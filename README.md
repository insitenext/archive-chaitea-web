# Chaitea-Web

## Requirements
### TypeWriter
    Automatic TypeScript template generation from C# source files (http://frhagn.github.io/Typewriter)

## Resource Reference

### Frameworks
* [AngularJS](https://angularjs.org/)
* [Bootstrap](http://getbootstrap.com/)

### UI Component & Utility Libraries

* [LoDash](http://www.lodash.com/)
* [Bootstrap-select](https://silviomoreto.github.io/bootstrap-select/)
* [Highcharts](http://www.highcharts.com/)
* [Moment.js](http://momentjs.com/)

### CSS Pre-Processing

* [Sass: Syntactically Awesome Style Sheets](http://sass-lang.com/)

### Icons

* [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
