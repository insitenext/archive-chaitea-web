var fs = require('fs');
var path = require('path');
var modelsFolder = 'ChaiTea/ChaiTea.Web/ClientAPI';
var path = path.resolve(__dirname, modelsFolder);
fs.readdir(path, function (err, items) {
	console.log('\nChaitea.Web API Models: \n')
	
    if (!items) {
		console.error('No files found in:')
        console.error(path + ' \n');
		console.error('Oh No! \n');
        console.error('error:::::', err);
		return;
    }
    var stream = fs.createWriteStream('index.ts');
    stream.once('open', function (fd) {

        for (var i = 0; i < items.length; i++) {
            if(!items[i].includes('.tst') && !items[i].includes('.js') && !items[i].includes('.NOT')){
                // remove trailing '.ts';
                items[i] = items[i].slice(0, (items[i].length - 3));
                stream.write("export * from './" + modelsFolder + '/' + items[i] + "';\n");
            }
        }
        stream.end();
        console.log('All Done! Added ' + items.length + ' items.');
    });
});


