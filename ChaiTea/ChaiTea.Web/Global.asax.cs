﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using AutoMapper;

using ChaiTea.BusinessLogic.AppMessaging.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Settings;
using ChaiTea.BusinessLogic.Entities.Utils.Translation;
using ChaiTea.BusinessLogic.Mappings;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Identity;

using log4net;
using log4net.Config;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using Newtonsoft.Json;

using NServiceBus;
using NServiceBus.Newtonsoft.Json;

[assembly: XmlConfigurator(Watch = true)]
namespace ChaiTea.Web
{
    public class MvcApplication : HttpApplication
    {
        public static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static AppEnvironment ParseEnvironment(string subject)
        {
            var strings = subject.Split(' ');
            var results = new List<string>();

            foreach (var s in strings)
            {
                results.Add($"{s.Substring(0, 1).ToUpper()}{s.Substring(1).ToLower()}");
            }

            var environment = string.Join(" ", results.ToArray());

            Enum.TryParse<AppEnvironment>(environment, out var result);

            return result;
        }

        public static AppEnvironment Environment = ParseEnvironment(EnvironmentSettings.ThisEnvironment);
        public static readonly Guid MvcApplicationInstanceIdentifier = Guid.NewGuid();

        private static readonly DateTime MvcApplicationStartTime = AppUtility.UtcNow();

        protected void Application_Start()
        {
            Logger.Info($"Chaitea.Web starting up instance {MvcApplicationInstanceIdentifier.ToString("N")} on Host DNS Name ({Dns.GetHostName()})");

            TypeDescriptor.AddAttributes(typeof(DateTime), new TypeConverterAttribute(typeof(UtcDateTimeConverter)));
            
            AreaRegistration.RegisterAllAreas();

            // Clears all view engines and only look for Razor.

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            GlobalConfiguration.Configure(ApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutoMapperWebConfiguration.Configure();

            ServiceBusConfiguration.Configure();

            UserContextService.CurrentUserIdGetter = () => HttpContext.Current?
                                                             .User?
                                                             .Identity?
                                                             .GetUserId<int>();

            UserContextService.UserGetter = (userId) => HttpContext.Current?
                                                    .GetOwinContext()?
                                                    .GetUserManager<ApplicationUserManager>()?
                                                    .FindById(userId);


            Language.ActiveLanguageFunc = () =>
                                          {
                                              var CurrentItemsKey = "Core.Entities.Util.Language.ActiveLanguage";

                                              if (HttpContext.Current == null)
                                              {
                                                  throw new ApplicationException("HttpContext not found to determine Active Language");
                                              }

                                              if (HttpContext.Current.Items.Contains(CurrentItemsKey))
                                              {
                                                  return Convert.ToInt32(HttpContext.Current.Items["Core.Entities.Util.Language.ActiveLanguage"]);
                                              }

                                              var fromRequest = HttpContext.Current.Request.Headers[UserContextService.Headers.LanguageId];

                                              if (!string.IsNullOrWhiteSpace(fromRequest))
                                              {
                                                  var langCode = Convert.ToInt32(fromRequest);
                                                  HttpContext.Current.Items[CurrentItemsKey] = langCode;

                                                  return langCode;
                                              }

                                              var ident = HttpContext.Current.User.Identity;

                                              if (ident is ClaimsIdentity)
                                              {
                                                  var langClaim = (ident as ClaimsIdentity).Claims.FirstOrDefault(t => t.Type == UserContextService.Claims.LanguageId);

                                                  if (langClaim != null)
                                                  {
                                                      HttpContext.Current.Items[CurrentItemsKey] = langClaim.Value;

                                                      return Convert.ToInt32(langClaim.Value);
                                                  }
                                              }

                                              var user = UserContextService.Current;

                                              if (user == null)
                                              {
                                                  throw new ApplicationException("Could not find current user to retrieve active language");
                                              }

                                              HttpContext.Current.Items[CurrentItemsKey] = user.ActiveLanguageId;

                                              return user.ActiveLanguageId;
                                          };
        }

        protected void Application_End()
        {
            Logger.Info($"Chaitea.Web shutting down instance {MvcApplicationInstanceIdentifier:N} on Host DNS Name ({Dns.GetHostName()}) started on {MvcApplicationStartTime} with uptime {AppUtility.UtcNow().Subtract(MvcApplicationStartTime).TotalMinutes} minutes.");
        }

        protected void Application_Error()
        {
            var e = Server.GetLastError();

            if (e != null)
            {
                if (LogicalThreadContext.Properties["requestGuid"] == null)
                {
                    LogicalThreadContext.Properties["requestGuid"] = Guid.NewGuid().ToString();
                }

                var additionalInfo = new Dictionary<string, string>
                                     {
                                         { "Url", Context.Request.Url.OriginalString },
                                         { "Principal Identity Name", Context.User?.Identity?.Name }
                                     };

                if (Context.Request.Headers.Count > 0)
                {
                    foreach (var header in Context.Request.Headers.Keys)
                    {
                        var headerName = Convert.ToString(header);

                        if (headerName.Equals("Cookie"))
                        {
                            foreach (var cookieHeader in Context.Request.Headers.GetValues(Convert.ToString(headerName)))
                            {
                                var cookies = cookieHeader.Split(';');
                                int i = 1;

                                foreach (var cookie in cookies)
                                {
                                    var cookieParts = cookie.Split('=');
                                    var cookieName = cookieParts[0];
                                    var cookieValue = cookieParts[1];
                                    var keyName = "Header-Cookie-" + cookieName?.Trim();

                                    if (additionalInfo.ContainsKey(keyName))
                                    {
                                        additionalInfo.Add(keyName + "-" + i, cookieValue);
                                        i++;
                                    }
                                    else
                                    {
                                        additionalInfo.Add(keyName, cookieValue);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var headerValue = Context.Request.Headers[headerName];
                            additionalInfo.Add("Header-" + headerName, headerValue);
                        }
                    }
                }

                var additionalAsJson = string.Empty;
                if (additionalInfo.Any())
                {
                    additionalAsJson = "{ \"" + string.Join("\"," + System.Environment.NewLine + "\"", 
                                                            additionalInfo.Select(kv => kv.Key?.Replace("\"", "'") + "\": \"" + 
                                                                                        kv.Value?.Replace("\"", "'"))) + "\"}";
                }

                var level = "ERROR";

                if (e is ArgumentNullException ||
                    e is ArgumentOutOfRangeException ||
                    e is ArgumentException ||
                    e is KeyNotFoundException ||
                    e is NotImplementedException)
                {
                    level = "INFO";
                }
                else if (e is UnauthorizedAccessException)
                {
                    level = "WARN";
                }
                else if (e is HttpException)
                {
                    var code = ((HttpException) e).GetHttpCode();
                    if (code == 404)
                    {
                        level = "INFO";
                    }
                    else if (code < 500)
                    {
                        level = "WARN";
                    }
                }

                switch (level)
                {
                    case "INFO":
                        Logger.Info($"MVC Exception: [{e.GetType().FullName}] Additional: {additionalAsJson}", e);
                        break;

                    case "WARN":
                        Logger.Warn($"MVC Exception: [{e.GetType().FullName}] Additional: {additionalAsJson}", e);
                        break;

                    default:
                        Logger.Error($"MVC Exception: [{e.GetType().FullName}] Additional: {additionalAsJson}", e);
                        break;
                }
            }
        }

        private static readonly string[] Redirector_Whitelist =
        {
            "/" + WebApiConfig.UrlPrefix.ToLower(),
            "/" + WebApiConfig.UrlPrefix_v1_1.ToLower(),
            "/" + WebApiConfig.UrlPrefix_v1_2.ToLower(),
            "/" + MobileApiConfig.UrlPrefix.ToLower(),
            "/personnel/safety/details",
            "/personnel/safety/entry",
            "/personnel/safety/entryview",
            "/training/course/index",
            "/training/course/entry",
            "/training/course/assign",
            "/training/course/unassign",
            "/admin/tenantmanagement",
            "/admin/knowledgecenter/coursetypes",
            "/admin/report",
            "/admin/client",
            "/admin/core",
            "/account",
            "/error",
            "/signalr",
            "/content",
            "/scripts",
            "/quality/customersurvey/survey",
            "/quality/customersurvey/surveyconfirm",
            "/quality/customersurvey/surveyforcustomer",
            "/quality/customersurvey/surveysubmit",
            "/swagger",
            JwtSettings.TokenUrl.ToLower()
        };

        protected void Application_BeginRequest()
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                // These headers are handling the "pre-flight" OPTIONS call sent by the browser

                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST, DELETE");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Credentials, Authorization, Content-Type, clientId, siteId, programId, languageId");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
                HttpContext.Current.Response.End();
            }

            var localPath = Request.Url.LocalPath.ToLower();
#if DEBUG
            localPath = localPath.Replace("/chaitea.web", string.Empty);
#endif
            if (!Redirector_Whitelist.Any(w => localPath.StartsWith(w)))
            {
                Response.Redirect(HttpContext.Current.Request.Url.Scheme + "://" + EnvironmentSettings.WebDomain, true);
            }

            LogicalThreadContext.Properties["requestGuid"] = Guid.NewGuid().ToString();

            if (localPath.StartsWith("/" + WebApiConfig.UrlPrefix.ToLower()) ||
                localPath.StartsWith("/" + WebApiConfig.UrlPrefix_v1_1.ToLower()) ||
                localPath.StartsWith("/" + WebApiConfig.UrlPrefix_v1_2.ToLower()) ||
                localPath.StartsWith("/" + MobileApiConfig.UrlPrefix.ToLower()) ||
                localPath.StartsWith(JwtSettings.TokenUrl.ToLower()))
            {
                Response.SuppressFormsAuthenticationRedirect = true;
            }
        }
    }

    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
                              {
                                  cfg.AddProfile(new ApplicationMapProfile());
                              });
        }
    }

    public static class ServiceBusConfiguration
    {
        public static void Configure()
        {
            MvcApplication.Logger
                          .Info("ServiceBusConfiguration: Configuring on application start.");

            BusMessenger.Configuration = new BusConfiguration();
#if DEBUG
            BusMessenger.Configuration
                        .UseTransport<MsmqTransport>()
                        .ConnectionString(ServiceBusTransports.DefaultConnection);
#else
            BusMessenger.Configuration
                        .UseTransport<SqsTransport>()
                        .ConnectionString(ServiceBusTransports.DefaultConnection);
#endif
            BusMessenger.Configuration
                        .UseSerialization<NewtonsoftSerializer>()
                        .Settings(new JsonSerializerSettings()
                                  {
                                      TypeNameHandling = TypeNameHandling.None
                                  });


            BusMessenger.SendAction = BusMessenger.DefaultSendOnlyAction;

            BusMessenger.SendOnlyBus = Bus.CreateSendOnly(BusMessenger.Configuration);

            MvcApplication.Logger.Info("ServiceBusConfiguration: created instance, ready to send");
        }
    }
}


public sealed class UtcDateTimeConverter : DateTimeConverter
{
    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) => ((DateTime)base.ConvertFrom(context, culture, value)).ToUniversalTime();
}

