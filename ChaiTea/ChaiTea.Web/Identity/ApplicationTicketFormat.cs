﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using Thinktecture.IdentityModel.Tokens;

namespace ChaiTea.Web.Identity
{
    public class ApplicationTicketFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly byte[] _keyByteArray;
        private readonly HmacSigningCredentials _signingKey;
        private readonly TokenValidationParameters _tokenValidationParameters;

        public ApplicationTicketFormat()
        {
            _keyByteArray = TextEncodings.Base64Url.Decode(JwtSettings.AudienceSecret);
            _signingKey = new HmacSigningCredentials(_keyByteArray);

            _tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey.SigningKey,

                ValidateIssuer = true,
                ValidIssuer = JwtSettings.IssuerUrl,

                ValidateAudience = true,
                ValidAudience = JwtSettings.AudienceId,

                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var issued = data.Properties.IssuedUtc;

            var expires = data.Properties.ExpiresUtc;
            return Protect(issued, expires, data.Identity.Claims);
        }

        public string Protect(DateTimeOffset? issued, DateTimeOffset? expires, IEnumerable<Claim> claims)
        { 
            var token = new JwtSecurityToken(_tokenValidationParameters.ValidIssuer,
                                             _tokenValidationParameters.ValidAudience,
                                             claims,
                                             issued.Value.UtcDateTime,
                                             expires.Value.UtcDateTime,
                                             _signingKey);

            var handler = new ApplicationSecurityTokenHandler();

            var tokenJSON = handler.WriteToken(token);

            return tokenJSON;
        }

        public AuthenticationTicket Unprotect(string protectedText) => Unprotect(protectedText, null);

        public AuthenticationTicket Unprotect(string protectedText, string purpose)
        {
            var handler = new ApplicationSecurityTokenHandler();
            ClaimsPrincipal principal = null;
            SecurityToken validToken = null;

            try
            {
                principal = handler.ValidateToken(protectedText, _tokenValidationParameters , out validToken);
            }
            catch(SecurityTokenExpiredException)
            {
                MvcApplication.Logger.Warn($"Token is expired: [{protectedText}]");
                return null;
            }
            catch (SecurityTokenValidationException)
            {
                MvcApplication.Logger.Warn("Token is not authenticated or cannot be authenticated by this security token authenticator.");
                return null;
            }
            catch (ArgumentException)
            {
                MvcApplication.Logger.Error("Token is null.");
                return null;
            }

            var ticket = new AuthenticationTicket(principal.Identity as ClaimsIdentity, new AuthenticationProperties());
            ticket.Properties.IssuedUtc = validToken.ValidFrom;
            ticket.Properties.ExpiresUtc = validToken.ValidTo;
            return ticket;
        }
    }
}
