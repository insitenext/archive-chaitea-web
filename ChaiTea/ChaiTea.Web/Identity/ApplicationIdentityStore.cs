﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Entities.AppMetaData;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ChaiTea.Web.Identity
{
    public class ApplicationIdentityStore : IUserPasswordStore<User, int>,
                                            IUserSecurityStampStore<User, int>,
                                            IUserEmailStore<User, int>,
                                            IUserLockoutStore<User, int>,
                                            IUserTwoFactorStore<User, int>,
                                            IUserPhoneNumberStore<User, int>,
                                            IUserRoleStore<User,int>
    {
        private ApplicationDbContext _context;

        public ApplicationIdentityStore(ApplicationDbContext context)
        {
            _context = context;
        }

        #region IUserStore<User, int>

        public async Task CreateAsync(User user)
        {
            _context.Set<User>().Add(user);            
            await _context.SaveChangesAsync();                        
        }

        public async Task DeleteAsync(User user)
        {
            _context.Set<User>().Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> FindByIdAsync(int userId)
        {
            var usr = await _context.Set<User>()
                           .Where(u => u.IsActive)
                           .Where(u => u.UserId == userId)
                           .FirstOrDefaultAsync();
            return usr;
        }

        public async Task<User> FindByNameAsync(string userName)
        { 
            var ua = await _context.Set<User>()
                           .Where(u => u.IsActive)
                           .Where(u => u.UserName.ToLower() == userName.ToLower())
                           .FirstOrDefaultAsync();
            return ua;
        }

        public async Task UpdateAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            if (_context != null)
            { _context.Dispose(); }
        }

        #endregion


        #region IUserPasswordStore<User, int>

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        #endregion

        #region IUserSecurityStampStore<User, int>

        public Task<string> GetSecurityStampAsync(User user)
        {
            return Task.FromResult(user.SecurityStamp);
        }

        public Task SetSecurityStampAsync(User user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }


        #endregion

        #region IUserEmailStore<User, int>

        public async Task<User> FindByEmailAsync(string email)
        {
            var au = await _context.Set<User>()
                                   .Where(u => u.IsActive)
                                   .Where(u => u.Email.ToLower().Equals(email.ToLower()))
                                   .FirstOrDefaultAsync();
            return au;
        }

        public Task<string> GetEmailAsync(User user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return Task.FromResult(user.IsEmailConfirmed);
        }

        public Task SetEmailAsync(User user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            user.IsEmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        #endregion

        #region IUserLockoutStore<User, int>

        public Task<int> GetAccessFailedCountAsync(User user)
        {
            return Task.FromResult(user.IsLockoutEnabled ? user.AccessFailedCount : 0);
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            return Task.FromResult(user.IsLockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return Task.FromResult(new DateTimeOffset(user.IsLockoutEnabled && user.LockoutEndDateUtc.HasValue ? user.LockoutEndDateUtc.Value : AppUtility.UtcNow()));
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            if (user.IsLockoutEnabled)
            {
                user.AccessFailedCount++;
                return Task.FromResult(user.AccessFailedCount);
            }
            return Task.FromResult(0);
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            user.IsLockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            if (user.IsLockoutEnabled)
            {
                user.LockoutEndDateUtc = new DateTime(lockoutEnd.Ticks);
            }
            return Task.FromResult(0);
        }

        #endregion

        #region IUserTwoFactorStore<User, int>

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            return Task.FromResult(user.IsTwoFactorEnabled);
        }

        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            user.IsTwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        #endregion

        #region IUserPhoneNumberStore<User, int>

        public Task<string> GetPhoneNumberAsync(User user)
        {
            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(User user)
        {
            return Task.FromResult(user.IsPhoneNumberConfirmed);
        }

        public Task SetPhoneNumberAsync(User user, string phoneNumber)
        {
            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
        {
            user.IsPhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task AddToRoleAsync(User user, string roleName)
        {
            throw new NotImplementedException("Roles are OrgUser Based; Dont use built in asp.net for this");
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            throw new NotImplementedException("Roles are OrgUser Based; Dont use built in asp.net for this");
        }

        public Task<IList<string>> GetRolesAsync(User user)
        {
            IList<string> result = new List<String>();
            return Task.FromResult(result);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            throw new NotImplementedException("Roles are OrgUser Based; Dont use built in asp.net for this");
        }

        #endregion

    }
}
