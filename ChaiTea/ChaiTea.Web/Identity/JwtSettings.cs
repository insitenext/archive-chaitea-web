﻿using System;

using ChaiTea.BusinessLogic.AppUtils.Settings;


namespace ChaiTea.Web.Identity
{
    /// <summary>
    /// Property getter class for system/environment settings related to the JWT (JSON Web Token) settings.
    /// The default is to read environment variables. If values not found within the environment, then
    /// settings will be read from the web.config file. If neither of those has the desired setting, then
    /// internal defaults will be used.
    /// </summary>
    public static class JwtSettings
    {
        private const string IssuerUrl_KeyName = "jwt_IssuerUrl";
        private static string IssuerUrl_Value;

        public static string IssuerUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(IssuerUrl_Value))
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{IssuerUrl_KeyName}");

                    if (!string.IsNullOrWhiteSpace(gev))
                    {
                        IssuerUrl_Value = gev;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[IssuerUrl_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv))
                        {
                            IssuerUrl_Value = asv;
                        }
                    }
                }

                return IssuerUrl_Value;
            }
        }

        private const string AudienceId_KeyName = "jwt_AudienceId";
        private static string AudienceId_Value;

        public static string AudienceId
        {
            get
            {
                if (string.IsNullOrWhiteSpace(AudienceId_Value))
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{AudienceId_KeyName}");
                    if (!string.IsNullOrWhiteSpace(gev))
                    {
                        AudienceId_Value = gev;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[AudienceId_KeyName];
                        if (!string.IsNullOrWhiteSpace(asv))
                        {
                            AudienceId_Value = asv;
                        }
                    }
                }

                return AudienceId_Value;
            }
        }

        private const string AudienceSecret_KeyName = "jwt_AudienceSecret";
        private static string AudienceSecret_Value;

        public static string AudienceSecret
        {
            get
            {
                if (string.IsNullOrWhiteSpace(AudienceSecret_Value))
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{AudienceSecret_KeyName}");

                    if (!string.IsNullOrWhiteSpace(gev))
                    {
                        AudienceSecret_Value = gev;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[AudienceSecret_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv))
                        {
                            AudienceSecret_Value = asv;
                        }
                    }
                }

                return AudienceSecret_Value;
            }
        }

        public const bool AllowInsecureConnectionDefault = false;

        private const string AllowInsecure_KeyName = "jwt_AllowInsecure";
        private static bool? AllowInsecure_Value;

        public static bool AllowInsecure
        {
            get
            {
                if (!AllowInsecure_Value.HasValue)
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{AllowInsecure_KeyName}");
                    bool allowInsecure;

                    if (!string.IsNullOrWhiteSpace(gev) &&
                        bool.TryParse(gev, out allowInsecure))
                    {
                        AllowInsecure_Value = allowInsecure;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[AllowInsecure_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv) &&
                            bool.TryParse(asv, out allowInsecure))
                        {
                            AllowInsecure_Value = allowInsecure;
                        }
                        else
                        {
                            AllowInsecure_Value = AllowInsecureConnectionDefault;
                        }
                    }
                }

                return AllowInsecure_Value.Value;
            }
        }

        public const bool ValidateTokenExpiryDefault = true;

        private const string ValidateTokenExpiry_KeyName = "jwt_ValidateTokenExpiry";
        private static bool? ValidateTokenExpiry_Value;

        public static bool ValidateTokenExpiry
        {
            get
            {
                if (!ValidateTokenExpiry_Value.HasValue)
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{ValidateTokenExpiry_KeyName}");
                    bool validateTokenExpiry;

                    if (!string.IsNullOrWhiteSpace(gev) &&
                        bool.TryParse(gev, out validateTokenExpiry))
                    {
                        ValidateTokenExpiry_Value = validateTokenExpiry;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[ValidateTokenExpiry_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv) &&
                            bool.TryParse(asv, out validateTokenExpiry))
                        {
                            ValidateTokenExpiry_Value = validateTokenExpiry;
                        }
                        else
                        {
                            ValidateTokenExpiry_Value = ValidateTokenExpiryDefault;
                        }
                    }
                }

                return ValidateTokenExpiry_Value.Value;
            }
        }

        public const string DefaultTokenUrl = "/oauth/token";

        private const string TokenUrl_KeyName = "jwt_TokenUrl";
        private static string TokenUrl_Value;

        public static string TokenUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(TokenUrl_Value))
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{TokenUrl_KeyName}");

                    if (!string.IsNullOrWhiteSpace(gev))
                    {
                        TokenUrl_Value = gev;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[TokenUrl_KeyName];

                        TokenUrl_Value = !string.IsNullOrWhiteSpace(asv) ? asv : DefaultTokenUrl;
                    }
                }

                return TokenUrl_Value;
            }
        }

        public static TimeSpan DefaultTokenExpirationMaxInMinutes = new TimeSpan(365, 0, 0, 0); // 1 year. 

        public const int DefaultTokenExpirationInMinutes = 1440; // 24 hrs (1440 minutes) = 24 * 60. 

        private const string TokenExpirationInMinutes_KeyName = "jwt_TokenExpirationInMinutes";
        private static int? TokenExpirationInMinutes_Value;

        public static int TokenExpirationInMinutes
        {
            get
            {
                if (!TokenExpirationInMinutes_Value.HasValue)
                {
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{TokenExpirationInMinutes_KeyName}");
                    int tokenExpirationMinutes;

                    if (!string.IsNullOrWhiteSpace(gev) &&
                        int.TryParse(gev, out tokenExpirationMinutes))
                    {
                        TokenExpirationInMinutes_Value = tokenExpirationMinutes;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[TokenExpirationInMinutes_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv) &&
                            int.TryParse(asv, out tokenExpirationMinutes))
                        {
                            TokenExpirationInMinutes_Value = tokenExpirationMinutes;
                        }
                        else
                        {
                            TokenExpirationInMinutes_Value = DefaultTokenExpirationInMinutes;
                        }
                    }
                }

                return TokenExpirationInMinutes_Value.Value;
            }
        }

        public const int DefaultAccountLockoutInMinutes = 5;

        private const string AccountLockoutInMinutes_KeyName = "jwt_AccountLockoutInMinutes";
        private static int? AccountLockoutInMinutes_Value;

        public static int AccountLockoutInMinutes
        {
            get
            {
                if (!AccountLockoutInMinutes_Value.HasValue)
                {
                    int lockoutMinutes;
                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{AccountLockoutInMinutes_KeyName}");

                    if (!string.IsNullOrWhiteSpace(gev) &&
                        int.TryParse(gev, out lockoutMinutes))
                    {
                        AccountLockoutInMinutes_Value = lockoutMinutes;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[AccountLockoutInMinutes_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv) &&
                            int.TryParse(asv, out lockoutMinutes))
                        {
                            AccountLockoutInMinutes_Value = lockoutMinutes;
                        }
                        else
                        {
                            AccountLockoutInMinutes_Value = DefaultAccountLockoutInMinutes;
                        }
                    }
                }

                return AccountLockoutInMinutes_Value.Value;
            }
        }

        public const int DefaultFailedAccessAttemptsBeforeLockout = 5;

        private const string FailedAccessAttemptsBeforeLockout_KeyName = "jwt_FailedAccessAttemptsBeforeLockout";
        private static int? FailedAccessAttemptsBeforeLockout_Value;

        public static int FailedAccessAttemptsBeforeLockout
        {
            get
            {
                if (!FailedAccessAttemptsBeforeLockout_Value.HasValue)
                {
                    int accessCount;

                    var gev = Environment.GetEnvironmentVariable($"{EnvironmentSettings.Prefix}{FailedAccessAttemptsBeforeLockout_KeyName}");

                    if (!string.IsNullOrWhiteSpace(gev) &&
                        int.TryParse(gev, out accessCount))
                    {
                        FailedAccessAttemptsBeforeLockout_Value = accessCount;
                    }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[FailedAccessAttemptsBeforeLockout_KeyName];

                        if (!string.IsNullOrWhiteSpace(asv) &&
                            int.TryParse(asv, out accessCount))
                        {
                            FailedAccessAttemptsBeforeLockout_Value = accessCount;
                        }
                        else
                        {
                            FailedAccessAttemptsBeforeLockout_Value = DefaultFailedAccessAttemptsBeforeLockout;
                        }
                    }
                }

                return FailedAccessAttemptsBeforeLockout_Value.Value;
            }
        }
    }
}