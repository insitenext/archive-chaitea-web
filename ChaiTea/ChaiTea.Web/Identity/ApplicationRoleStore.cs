﻿using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Entities.AppMetaData;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ChaiTea.Web.Identity
{
    public class ApplicationRoleStore : IRoleStore<Role, int>
    {
        private ApplicationDbContext _context;

        public ApplicationRoleStore(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Role role)
        {
            _context.Roles.Add(role);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Role role)
        {
            Role dbRole = await FindByIdAsync(role.RoleId);
            dbRole.IsActive = false;
            dbRole.UpdateBy = role.UpdateBy;
            dbRole.UpdateById = role.UpdateById;
            await _context.SaveChangesAsync();
        }

        public async Task<Role> FindByIdAsync(int roleId)
        {
            return await _context.Roles.FindAsync(roleId);
        }

        public async Task<Role> FindByNameAsync(string roleName)
        {
            return await _context.Roles
                                 .Where(r => r.Name.ToLower().Equals(roleName.ToLower()))
                                 .FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Role role)
        {
            var dbRole = await FindByIdAsync(role.RoleId);
            dbRole.Name = role.Name;
            dbRole.UpdateBy = role.UpdateBy;
            dbRole.UpdateById = role.UpdateById;
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            if (_context != null)
            { _context.Dispose(); }
        }
    }
}
