﻿using log4net;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChaiTea.Web.Identity
{
    /// <inheritdoc />
    /// <summary>
    /// JWT OAuth used by all of the API which is most of the Front-end
    /// Note: there is also a MVC account login controller for the current work still in mvc 
    /// </summary>
    /// <remarks>
    /// A new JWT token is requested (just once) each time the user logs in
    /// Previously, we would store thier Location Context (Client/Site/Program) in the token, but we no longer do that.
    /// Subsequent requests to the api use the JWT Token.
    /// When the BaseAPIController or MvcControllerBase build the UserContextService,
    /// it claims values first, then uses the header values second, then finally the values from the db
    /// </remarks>
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public static readonly ILog Logger = LogManager.GetLogger(nameof(ApplicationOAuthProvider));

        public const string AuthenticationType = "JWT";
        private IReadableStringCollection Parameters;

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // TODO: secure/lock-down to only requests presenting
            // TODO: a valid API key can use this jwt authentication/authorization mechanism

            Parameters = context.Parameters;
            context.Validated();

            return Task.CompletedTask;
        }

        private const string Auth_Key = "WWW-Authenticate";
        private const string Auth_Value = "OAuth";

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The username or password is incorrect.");

                Logger.Info($"Login Failed: The username or password is incorrect for username [{context.UserName}]");

                context.Response.Headers.Add(Auth_Key, new[] {Auth_Value});
                return;
            }

            if (!user.IsActive)
            {
                context.SetError("invalid_user", "The user is currently inactive.");

                Logger.Info($"Login Failed: The user [{user.UserName}:{user.UserId}] is currently inactive");

                context.Response.Headers.Add(Auth_Key, new[] {Auth_Value});
                return;
            }

            if (string.IsNullOrEmpty(user.SecurityStamp))
            {
                context.SetError("invalid_SecurityStamp",
                                 "The user successfully validated, but the Security Stamp does not correspond with the password. Reset Password.");

                Logger.Info($"Login Failed: The user [{user.UserName}:{user.UserId}] successfully validated, but the Security Stamp does not " +
                            $"correspond with the password. Reset Password.");

                context.Response.Headers.Add(Auth_Key, new[] {Auth_Value});
                return;
            }

            var oAuthIdentity = await userManager.CreateIdentityAsync(user, AuthenticationType);

            var ticket = new AuthenticationTicket(oAuthIdentity, null)
                         {
                             Properties =
                             {
                                 IssuedUtc = DateTime.UtcNow,
                                 ExpiresUtc = DateTime.UtcNow.Add(TimeSpan.FromMinutes(JwtSettings.TokenExpirationInMinutes))
                             }
                         };


            if (context.Request.Cookies.Any(c => c.Key == CookieSettings.CookieName))
            {
                context.Response.Cookies.Delete(CookieSettings.CookieName);
            }

            context.Validated(ticket);
        }
    }
}