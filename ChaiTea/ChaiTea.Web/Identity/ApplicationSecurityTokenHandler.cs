﻿using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Entities.AppMetaData;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;

namespace ChaiTea.Web.Identity
{
    public class ApplicationSecurityTokenHandler : JwtSecurityTokenHandler
    {
        public override ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            bool selfThrown = false;

            try
            {
                SecurityToken tkn;
                var princ = base.ValidateToken(securityToken, validationParameters, out tkn);

                if (princ.Claims.Any(c => c.Type == ClaimTypes.Role))
                {
                    // old token
                    // re write principal to move from OrgUserId to UserId

                    using (var db = new ApplicationDbContext())
                    {
                        var orgUserId = princ.Identity.GetUserId<int>();
                        var userIdEntity = (from ou in db.Set<OrgUser>()
                                            join u in db.Set<User>() on ou.UserId equals u.UserId
                                            where ou.OrgUserId == orgUserId
                                                  && ou.IsActive
                                            select new
                                                   {
                                                       u.UserId
                                                   })
                                           .ToList()
                                           .FirstOrDefault();

                        if (userIdEntity == null)
                        {
                            MvcApplication.Logger.Error($"JWT Token in old format; could not update as User Account for OrgUser was not " +
                                                        $"found for OrgUserId: [{orgUserId}]");
                            validatedToken = null;
                            selfThrown = true;

                            throw new SecurityTokenValidationException("User account for OrgUser not found");
                        }

                        var newClaims = princ.Claims.Where(nc => nc.Type != ClaimTypes.NameIdentifier)
                                             .Where(nc => nc.Type != ClaimTypes.Role)
                                             .Select(UserContextService.Claims.UpdateClaimType)
                                             .ToList();

                        newClaims.Add(new Claim(ClaimTypes.NameIdentifier, Convert.ToString(userIdEntity.UserId)));

                        var claimsIdentity = new ClaimsIdentity(newClaims, ApplicationOAuthProvider.AuthenticationType);


                        // todo: perhaps update the Security Token
                        /*
                        tkn = new JwtSecurityToken(securityToken)
                        {
                        };
                        */

                        princ = new ClaimsPrincipal(claimsIdentity);
                        MvcApplication.Logger.Info($"Updated JWT Token for OrgUserId: [{orgUserId}] to UserId: [{userIdEntity.UserId}]");
                    }
                }

                using (var db = new ApplicationDbContext())
                {
                    var userId = princ.Identity.GetUserId<int>();
                    var securityStamp = (princ.Identity as ClaimsIdentity).Claims
                                                                          .Where(c => c.Type == UserContextService.Claims.SecurityStamp)
                                                                          .Select(c => c.Value)
                                                                          .FirstOrDefault()
                                                                          .Trim();

                    if (string.IsNullOrEmpty(securityStamp))
                    {
                        MvcApplication.Logger.Warn($"JWT Token security stamp not found for UserId: [{userId}]");

                        validatedToken = null;
                        selfThrown = true;

                        throw new SecurityTokenValidationException("No Security Stamp found in token.");
                    }

                    // Get the user record from the Db (should *ALWAYS* be a list of *ONE* - there can be only one!).

                    var userRecord = db.Set<User>()
                                       .Where(u => u.IsActive)
                                       .FirstOrDefault(u => u.UserId == userId);

                    if (userRecord == null)
                    {
                        string msg = $"No active user record found for userId: [{userId}].";

                        MvcApplication.Logger.Warn(msg);

                        validatedToken = null;
                        selfThrown = true;

                        throw new SecurityTokenValidationException("Unable to authenticate. " + msg);
                    }

                    if (userRecord.SecurityStamp != securityStamp)
                    {
                        string msg = $"JWT Token security stamp mismatch for UserId: [{userId}] - " +
                                     $"User's stamp (from Db): [{userRecord.SecurityStamp}], JWT stamp: [{securityStamp}].";

                        MvcApplication.Logger.Warn(msg);

                        validatedToken = null;
                        selfThrown = true;

                        throw new SecurityTokenValidationException("The Security Stamp does not match. re-authenticate. " + msg);
                    }
                }

                validatedToken = tkn;

                return princ;
            }
            catch (SecurityTokenExpiredException expiredException)
            {
                MvcApplication.Logger.Warn($"Token expired exception caught validating JWT token. " +
                                           $"Message: {expiredException.Message}");
                throw;
            }
            catch (Exception ex)
            {
                // Don't log the exception twice, just pass it on.

                if (!selfThrown)
                {
                    MvcApplication.Logger.Warn($"Unexpected exception validating JWT token. " +
                                               $"Message: \"{ex.Message}\"");
                }

                throw;
            }
        }
    }
}