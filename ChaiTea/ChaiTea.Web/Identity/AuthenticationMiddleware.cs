using Microsoft.Owin;
using System.Threading.Tasks;

namespace ChaiTea.Web.Identity
{
    public class AuthenticationMiddleware : OwinMiddleware
    {
        public AuthenticationMiddleware(OwinMiddleware next) : base(next) { }

        public override async Task Invoke(IOwinContext context)
        {
            await Next.Invoke(context);

            if (context.Response.StatusCode == 400 && context.Response.Headers.ContainsKey("WWW-Authenticate"))
            {
                context.Response.StatusCode = 401;
            }
        }
    }
}