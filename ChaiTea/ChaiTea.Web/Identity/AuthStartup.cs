﻿using ChaiTea.BusinessLogic.DAL;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;

namespace ChaiTea.Web.Identity
{
    public static class AuthStartup
    {
        public static void ConfigureAuth(IAppBuilder app)
        {
            app.Use<AuthenticationMiddleware>();
            app.CreatePerOwinContext<ApplicationDbContext>(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            ConfigureOAuthTokenGeneration(app);
            ConfigureOAuthTokenConsumption(app);
        }

        private static void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            var OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = JwtSettings.AllowInsecure,
                TokenEndpointPath = new PathString(JwtSettings.TokenUrl),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(JwtSettings.TokenExpirationInMinutes),
                Provider = new ApplicationOAuthProvider(),
                AccessTokenFormat = new ApplicationTicketFormat()
            };

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
        }

        private static void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            byte[] audienceSecret = TextEncodings.Base64Url.Decode(JwtSettings.AudienceSecret);

            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { JwtSettings.AudienceId },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(JwtSettings.IssuerUrl, audienceSecret)
                    },
                    TokenHandler = new ApplicationSecurityTokenHandler()
                });
        }
    }
}
