﻿namespace ChaiTea.Web.Identity
{
    public class Roles
    {
        public const string CrazyProgrammers = "crazyprogrammers";
        public const string Admins = "admins";
        public const string Managers = "managers";
        public const string Users = "users";
        public const string Employees = "employees";
        public const string HrAdmins = "hr admins";
        public const string BillingViewers = "billing viewers";
        public const string TimeclockViewers = "timeclock viewers";
        public const string TrainingAdmins = "training admins";
        public const string Customers = "customers";
        public const string ProfileViewers = "profile viewers";
        public const string SepViewers = "sep viewers";
        public const string BonusViewers = "bonus viewers";
        public const string ManagerNotifications = "manager notifications";
        public const string CustomerNotifications = "customer notifications";
        public const string SuppliesViewers = "supplies viewers";
        public const string ToDoViewers = "to-do viewers";
        public const string TurnoverViewers = "turnover viewers";
        public const string EHSManagers = "ehs managers";
        public const string SurveyAdmins = "survey admins";
        public const string ClaimsManagers = "claims managers";
        public const string OrgStructureUser = "org structure user";
        public const string OrgChartViewer = "org chart viewer";
        public const string MessageBuilders = "message builder department";
        public const string MessageBuilderManager = "message builder manager";
        public const string AttendanceTrackerViewer = "attendance tracker viewer";
        public const string AttendanceTrackerManager = "attendance tracker manager";
        public const string RouteManager = "route manager";
        public const string RouteViewer = "route viewer";
        public const string DailyActivityViewer = "daily activity viewer";
        public const string DailyActivityManager = "daily activity manager";
        public const string ActiveSiteManager = "active site manager";
        public const string AttestationQuestionManager = "attestation question manager";
        public const string AttestationQuestionViewer = "attestation question viewer";
        public const string CostOverview = "cost overview";
        public const string HoursManager = "hours manager";
        public const string HoursApprover = "hours approver";
        public const string Mobile = "mobile";
        public const string TermsAndConditionsViewer = "terms and conditions viewer";
        public const string UnionRepresentative = "union representative";
        public const string QualityManager = "quality manager";
        public const string QualityDepartmentManager = "quality department manager";
        public const string SafetyDepartmentManager = "safety department manager";
        public const string GovernanceDepartmentManager = "governance department manager";


        public const string AllRoles = CrazyProgrammers + "," +
                                       Admins + "," +
                                       Managers + "," +
                                       Users + "," +
                                       Employees + "," +
                                       HrAdmins + "," +
                                       BillingViewers + "," +
                                       TimeclockViewers + "," +
                                       TrainingAdmins + "," +
                                       Customers + "," +
                                       ProfileViewers + "," +
                                       SepViewers + "," +
                                       MessageBuilders + "," +
                                       MessageBuilderManager + "," +
                                       BonusViewers + "," +
                                       ManagerNotifications + "," +
                                       CustomerNotifications + "," +
                                       SuppliesViewers + "," +
                                       ToDoViewers + "," +
                                       TurnoverViewers + "," +
                                       EHSManagers + "," +
                                       SurveyAdmins + "," +
                                       ClaimsManagers + "," +
                                       OrgStructureUser + "," +
                                       ActiveSiteManager + "," +
                                       OrgChartViewer + "," +
                                       AttendanceTrackerViewer + "," +
                                       AttendanceTrackerManager + "," +
                                       RouteManager + "," +
                                       RouteViewer + "," +
                                       DailyActivityViewer + "," +
                                       DailyActivityManager + "," +
                                       ActiveSiteManager + "," +
                                       AttestationQuestionManager + "," +
                                       AttestationQuestionViewer + "," +
                                       CostOverview + "," +
                                       HoursManager + "," +
                                       HoursApprover + "," +
                                       Mobile + "," +
                                       TermsAndConditionsViewer + "," +
                                       UnionRepresentative + "," +
                                       QualityManager + "," +
                                       QualityDepartmentManager + "," +
                                       SafetyDepartmentManager + "," +
                                       GovernanceDepartmentManager;

    }
}