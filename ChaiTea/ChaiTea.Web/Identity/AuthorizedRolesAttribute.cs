﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;

namespace ChaiTea.Web.Identity
{
    /// <summary>
    /// This attribute was created to override the default Mvc's authorize attribute's behavior of redirecting 
    /// users to the login page when a user is not authorize. The redirect should only occur if the user is 
    /// not authenticated. 
    /// See http://stackoverflow.com/questions/238437/why-does-authorizeattribute-redirect-to-the-login-page-for-authentication-and-au.
    /// </summary> 
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizedRolesAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var ident = httpContext?.User?.Identity as ClaimsIdentity;

            if (ident?.IsAuthenticated == true)
            {
                if (string.IsNullOrWhiteSpace(Roles))
                {
                    return true;
                }

                var baseRoles = Roles.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);

                var userHasRoleInClaims = ident.Claims.Any(c => c.Type == UserContextService.Claims.Role
                                                                && baseRoles.Any(br => c.Value.Equals(br, StringComparison.InvariantCultureIgnoreCase)));

                if (userHasRoleInClaims)
                {
                    return true;
                }

                int userId = UserContextService.CurrentUserId;
                IEnumerable<KeyValuePair<string, string>> headers = httpContext.Request
                                                                               .Headers
                                                                               .AllKeys
                                                                               .Select(t => new KeyValuePair<string, string>(t, httpContext.Request.Headers[t]));

                IUserContext uc = UserContextService.BuildFromRequest(false, userId, headers, ident.Claims);
                IEnumerable<string> userRoles = new UserContextService(uc).GetRolesForOrgUser();

                foreach (string r in userRoles)
                {
                    ident.AddClaim(new Claim(UserContextService.Claims.Role, r));
                }

                // Add them (as claims) to identity so we don't re-query in the same request.

                userHasRoleInClaims = ident.Claims.Any(c => c.Type == UserContextService.Claims.Role &&
                                                            baseRoles.Any(br => c.Value.Equals(br, StringComparison.InvariantCultureIgnoreCase)));

                if (userHasRoleInClaims)
                {
                    return true;
                }
            }

            return base.AuthorizeCore(httpContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}