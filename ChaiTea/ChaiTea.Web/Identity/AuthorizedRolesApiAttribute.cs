﻿using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Entities.AppMetaData;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ChaiTea.Web.Identity
{
    /// <summary>
    ///     This attribute was created to override the default Web Api's authorize attribute's behavior of redirecting users to the login page when a user
    ///     is not authorize. The redirect should only occur if the user is not authenticated. See http://stackoverflow.com/questions/238437/why-does-authorizeattribute-redirect-to-the-login-page-for-authentication-and-au.
    /// </summary> 
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizedRolesApiAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var ident = actionContext.RequestContext.Principal.Identity as ClaimsIdentity;
            if (ident?.IsAuthenticated == true)
            {
                if (String.IsNullOrWhiteSpace(base.Roles))
                { return true; }
                var baseRoles = base.Roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var userHasRoleInClaims = ident.Claims
                                               .Any(c => c.Type == UserContextService.Claims.Role
                                                      && baseRoles.Any(br => c.Value.Equals(br, StringComparison.InvariantCultureIgnoreCase)));

                if (userHasRoleInClaims)
                { return true; }


                var userId = UserContextService.CurrentUserId;
                var headers = actionContext.Request
                                           .Headers
                                           .Select(t => new System.Collections.Generic.KeyValuePair<string, string>(t.Key, t.Value.FirstOrDefault()));
                var uc = UserContextService.BuildFromRequest(false, userId, headers, ident.Claims);

                var Roles = new UserContextService(uc).GetRolesForOrgUser();

                foreach (var r in Roles)
                {
                    ident.AddClaim(new Claim(UserContextService.Claims.Role, r));
                }

                // Add them (as claims) to identity so we don't re-query in the same request.
                userHasRoleInClaims = ident.Claims
                                           .Any(c => c.Type == UserContextService.Claims.Role
                                                  && baseRoles.Any(br => c.Value.Equals(br, StringComparison.InvariantCultureIgnoreCase)));

                if (userHasRoleInClaims)
                { return true; }
            }


            return base.IsAuthorized(actionContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
        }
    }
}