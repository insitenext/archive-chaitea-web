﻿using ChaiTea.BusinessLogic.AppUtils.Settings;
using System;

namespace ChaiTea.Web.Identity
{
    public static class CookieSettings
    {
        private static bool loaded = false;
        private const string Prefix_KeyName = "CookieKeyPrefix";
        private static string Prefix_Value;
        public static string Prefix
        {
            get
            {
                if (!loaded)
                {
                    loaded = true;
                    var asv = System.Configuration.ConfigurationManager.AppSettings[Prefix_KeyName];
                    if (!string.IsNullOrWhiteSpace(asv))
                    { Prefix_Value = asv; }
                }
                return Prefix_Value;
            }
        }

        private const string CookieName_KeyName = "CookieName";
        private static string CookieName_Value;
        public static string CookieName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CookieName_Value))
                {
                    var gev = Environment.GetEnvironmentVariable(EnvironmentSettings.Prefix + CookieName_KeyName);
                    if (!string.IsNullOrWhiteSpace(gev))
                    { CookieName_Value = gev; }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[CookieName_KeyName];
                        if (!string.IsNullOrWhiteSpace(asv))
                        { CookieName_Value = asv; }
                        else
                        { CookieName_Value = "r2sbminsite"; }
                    }
                }
                return CookieName_Value;
            }
        }
    }
}
