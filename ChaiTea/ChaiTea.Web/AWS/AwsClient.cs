﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Settings;
using System;

namespace ChaiTea.Web.AWS
{
    public static class AwsClient
    {
        public static string GetUrl(string key)
        {
            //check for old keys
            var parts = key.Split('.');
            if (parts.Length == 1 || (parts.Length > 1 && parts[1] == null))
            {
                key = key + ".jpg";
            }

            return $"https://{AwsSettings.CloudfrontUrl}/{key}";
        }

        public static string GetUrlByName(string uniqueId, string folderName)
        {
            return GetUrl(folderName + "/" + uniqueId);
        }

        public static SessionAWSCredentials GetTemporaryCredentials(string accessKeyId, string secretAccessKeyId)
        {
            using (var stsClient = new AmazonSecurityTokenServiceClient(accessKeyId, secretAccessKeyId))
            {
                var credentials = stsClient.GetSessionToken(new GetSessionTokenRequest()
                {
                    DurationSeconds = 3600
                }).Credentials;

                return new SessionAWSCredentials(credentials.AccessKeyId, credentials.SecretAccessKey, credentials.SessionToken);
            }
        }

        public static string GeneratePreSignedURL(string bucketName, string objectKey)
        {
            using (var s3Client = new AmazonS3Client(Amazon.RegionEndpoint.GetBySystemName(AwsSettings.S3Region)))
            {
                return s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
                {
                    BucketName = bucketName,
                    Key = objectKey,
                    Expires = AppUtility.UtcNow().AddMinutes(60)
                });
            }

        }
    }
}
