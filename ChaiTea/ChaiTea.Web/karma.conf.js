// Karma configuration
// Generated on Thu Mar 17 2016 14:25:47 GMT-0700 (Pacific Daylight Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'Scripts/jquery-1.10.2.js',
        'Scripts/jquery.signalR-2.2.0.js',
        'Scripts/jquery.signalR-2.2.0.min.js',

        'Scripts/angular.js',
        'Scripts/angular-mocks.js',
        'Scripts/angular-resource.js',
        'Scripts/angular-animate.js',
        'Scripts/angular-sanitize.js',
        'Scripts/angular-translate.js',
        'Scripts/angular-translate-loader-url.js',
        'Scripts/angular-touch.js',
        'Scripts/angular-ui-router.js',
        'Scripts/angular-local-storage.js',
        'Scripts/angular-block-ui.js',
        'Scripts/angular-signalr-hub.js',
        'Scripts/angular-toggle-switch.min.js',
        'Scripts/angular-bootstrap-select.js',

        'Scripts/angular-bootstrap-calendar/*.js',
        'Scripts/angular-ui/*.js',
        'Scripts/file-upload/*.js',
        'Scripts/ng-*.js',

        'Scripts/moment.js',
        'Scripts/lodash.js',
        'Scripts/multiselect_edited.js',
        'Scripts/bootbox.js',
        'Scripts/highcharts.js',
        'Scripts/toastr.js',
        
        'Content/js/App.*.js',
        'Content/js/Core/**/*.js',
        'Content/js/Common/**/*.js',
        'Content/js/Services/**/*.js',
        'Content/Tests/**/*.Spec.js'
    ],


    // list of files to exclude
    exclude: [
        'Content/js/_libs.js',
        'Scripts/_references.js',
        'Content/Tests/Old/*.Spec.js',
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
