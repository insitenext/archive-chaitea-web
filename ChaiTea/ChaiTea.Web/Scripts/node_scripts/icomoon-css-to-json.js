﻿/*
 * @description Gets latest Icomoon stylesheet and creates JSON file from classes.
 */

var extract = require('extract-from-css');
var request = require('request');
var fs = require('fs');

var className = "CssToJson";

var path = "./Content/data/";

var _fileName = path + 'icon-classes.json';

var exisitingFile = function () {
    

    if (fs.statSync(_fileName)) {
        return fs.readFileSync(_fileName, 'utf8');
    } 

    return "";
}

exports.run = function () {
    return request('https://i.icomoon.io/public/20f175991c/ChaiTeaWeb/style.css', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var classes = extract.extractClasses(body);
            var jsonClasses = JSON.stringify(classes);

            fs.readFile(_fileName, function (err, data) {

                if (data == jsonClasses) {
                    console.info(className + " : File has not changed. No Need to update");
                    return;
                };

                //write new data to file
                fs.writeFile(_fileName, jsonClasses, function (err) {
                    if (err) throw err;
                    console.log(className + ' : It has been done.');
                });
            });
            //check to see if remote file has changed.
           

        } else if (error) {
            console.error(className + ' : Error getting icomoon file.', error);
        }
    });
}

//allows for run from terminal
if (!module.parent) {
    exports.run();
}