﻿using ChaiTea.BusinessLogic.Entities.AppMetaData;
using ChaiTea.Web.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Web;

using static Microsoft.AspNet.Identity.Owin.SecurityStampValidator;

namespace ChaiTea.Web
{
    public partial class Startup
    {
        /// <summary>
        /// Checks if the request is made to an API end point.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static bool IsApiRequest(IOwinRequest request)
        {
            string apiPath = VirtualPathUtility.ToAbsolute("~/api");

            return request.Uri.LocalPath.StartsWith(apiPath);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            AuthStartup.ConfigureAuth(app);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
                                        {
                                            AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                                            LoginPath = new PathString(VirtualPathUtility.ToAbsolute("~/Account/Login")),
                                            Provider = new CookieAuthenticationProvider
                                                       {
                                                           OnValidateIdentity = OnValidateIdentity<ApplicationUserManager, User, int>(TimeSpan.FromMinutes(30),
                                                                                                                                      async (manager, user) =>
                                                                                                                                              await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie),
                                                                                                                                      id => id.GetUserId<int>()
                                                                                                                                     ),
                                                           OnApplyRedirect = ctx =>
                                                                             {
                                                                                 if (!IsApiRequest(ctx.Request))
                                                                                 {
                                                                                     ctx.Response.Redirect(ctx.RedirectUri);
                                                                                 }
                                                                             }
                                                       },
                                            CookieHttpOnly = false,
                                            CookieSecure = JwtSettings.AllowInsecure ? CookieSecureOption.Never : CookieSecureOption.Always,
                                            TicketDataFormat = new ApplicationTicketFormat(),
                                            CookieName = CookieSettings.CookieName,
#if !DEBUG
                                            CookieDomain = ".sbminsite.com"
#endif
                                        });
        }
    }
}