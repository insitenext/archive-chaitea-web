﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;

using ChaiTea.Web.Helpers;

namespace ChaiTea.Web
{
    public static class ApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // API error filter

            config.Filters.Add(new ApiExceptionFilterAttribute());

            // API error logger

            config.Services.Add(typeof(IExceptionLogger), new Log4NetExceptionLogger());

            // API routes

            config.MapHttpAttributeRoutes();

            // Configure routing
            
            WebApiConfig.Register(config);
            MobileApiConfig.Register(config);
        }
    }
}
