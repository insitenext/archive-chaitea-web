﻿using System.Web.Optimization;
using WebGrease.Css.Extensions;

namespace ChaiTea.Web
{
    public class BundleConfig
    {
        private const string nodeModulesPath = "~/node_modules";

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterScripts(bundles);
            RegisterStyles(bundles);
        }

        private static void RegisterScripts(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            bundles.DirectoryFilter.Clear();

            bundles.Add(new ScriptBundle("~/scripts/jquery").Include(
                       "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/scripts/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/scripts/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/scripts/signalrJs").Include(
                    "~/Scripts/jquery.signalR.js"
                ));

            // Angular core and modules
            bundles.Add(new ScriptBundle("~/scripts/angular").Include(
                      "~/Scripts/npm-packages.js",
                      //nodeModulesPath + "/angular/angular.js",
                      "~/Scripts/angular-sanitize.js",
                      "~/Scripts/angular-translate.js",
                      "~/Scripts/angular-translate-loader-url.js",
                      "~/Scripts/angular-animate.js",
                      "~/Scripts/angular-resource.js",
                      "~/Scripts/angular-touch.js",
                      "~/Scripts/angular-local-storage.js",
                      "~/Scripts/angular-ui/event.js",
                      "~/Scripts/angular-ui-router.js",
                      "~/Scripts/angular-ui/ui-utils.js",
                      "~/Scripts/ng-infinite-scroll.js",
                      "~/Scripts/angular-block-ui.js",
                      "~/Scripts/file-upload/ng-file-upload.js",
                      "~/Scripts/file-upload/ng-file-upload-shim.js", // support for older browsers (IE8-9)
                      "~/Scripts/signature_pad.js",
                      "~/Scripts/ng-signature-pad.js",
                      "~/Scripts/aws-sdk.js",
                      "~/Scripts/async.js",
                      "~/Scripts/angular-bootstrap-select.js",
                      "~/Scripts/ng-tags-input.js",
                      "~/Scripts/image/tracking.min.js",
                      "~/Scripts/image/face.min.js",
                      "~/Scripts/angular-toggle-switch.min.js",
                      //nodeModulesPath+"/angular-wizard/dist/angular-wizard.js",
                      "~/Scripts/angular-wysiwyg.js",
                      //nodeModulesPath + "/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js",
                      "~/Scripts/jquery.stickytableheaders.min.js"
                      //nodeModulesPath + "/ngFlowGrid/src/ngFlowGrid.js"
                      ));

            // Config
            bundles.Add(new ScriptBundle("~/Scripts/chaitea-config").Include(
                   "~/Content/js/App.Config.js",
                   "~/Content/js/App.Run.js",
                   "~/Content/js/App.Values.js"));

            new[] {
                "Core",
                "Common",
                "Admin",
                "Dashboard",
                "Quality",
                "Training",
                "Settings",
                "Services",
                "User",
                "Personnel",
                "Components",
                "Financials",
                "Governance",
                "Contact"}.ForEach((string module) =>
            {
                bundles.Add(new ScriptBundle(string.Format("~/Scripts/chaitea-{0}", module)).IncludeDirectory(
                     string.Format("~/Content/js/{0}", module), "*.js", true));
            });

            // Third-party libs
            bundles.Add(new ScriptBundle("~/Scripts/libs").Include(
                //nodeModulesPath + "/lodash/index.js",
                //nodeModulesPath + "/bootbox/bootbox.js",
                //nodeModulesPath + "/toastr/toastr.js",
                //nodeModulesPath + "/moment/moment.js",
                "~/Scripts/multiselect_edited.js",
                "~/Scripts/highcharts.js",
                "~/Scripts/circle-progress.js",
                "~/Scripts/bootstrap-select.js",
                "~/Scripts/alasql.js",
                "~/Scripts/aws-sdk.js",
                "~/Scripts/angular-bootstrap-calendar/angular-bootstrap-calendar.js",
                "~/Scripts/angular-bootstrap-calendar/angular-bootstrap-calendar-tpls.js",
                "~/Scripts/image/image-crop.js",
                //nodeModulesPath + "/async/dist/async.min.js",
                //nodeModulesPath + "/angular-ui-grid/ui-grid.min.js",
                //nodeModulesPath + "/ui-select/dist/select.min.js",
                //nodeModulesPath + "/angular-busy/dist/angular-busy.js",
                "~/Scripts/image/image-crop.js",
                "~/Scripts/ngDraggable.js",
                "~/Scripts/ekko-lightbox.js",
                "~/Scripts/moment-timezone-with-data-2010-2020.js"
                ));
        }

        private static void RegisterStyles(BundleCollection bundles)
        {
            new[] {"Admin", "Quality", "Training", "Settings", "Personnel", "Financials", "Contact", "Dashboard","User", "Governance" }.ForEach((string module) =>
            {
                bundles.Add(new StyleBundle(string.Format("~/Content/styles/chaitea-{0}", module)).IncludeDirectory(
                     string.Format("~/Content/css/{0}", module), "*.css", true));
            });

            bundles.Add(new StyleBundle("~/Content/styles/angular-components").IncludeDirectory(
                     "~/Content/js/Components", "*.css", true));

            bundles.Add(new StyleBundle("~/Content/styles/core").Include(
                  "~/Content/css/Vendor/angular-block-ui.css",
                  "~/Content/css/Vendor/bootstrap-select.css",
                  "~/Content/css/Vendor/toastr.css",
                  "~/Content/css/Vendor/ng-tags-input.css",
                  "~/Content/css/Vendor/angular-bootstrap-calendar.css",
                  "~/Content/css/core.layout.css",
                  "~/Content/css/core.components.css",
                  "~/Content/css/Vendor/image-crop.css",
                  "~/Content/css/Vendor/image-crop.css",
                  "~/Content/css/Vendor/npm-packages.css",
                  "~/Content/css/Vendor/ekko-lightbox.css",
                  "~/Content/css/Vendor/flag-icons/flag-icon.min.css"
                  //nodeModulesPath + "/angular-wizard/dist/angular-wizard.css",
                  //nodeModulesPath + "/angular-bootstrap-colorpicker/css/colorpicker.css",
                  //nodeModulesPath + "/angular-ui-grid/ui-grid.min.css",
                  //nodeModulesPath + "/ui-select/dist/select.min.css",
                  //nodeModulesPath + "/angular-busy/dist/angular-busy.css"
                  ));

            // Super Experimental styles by Husam.
            bundles.Add(new StyleBundle("~/Content/styles/new").Include(
                "~/Content/css/components.css",
                "~/Content/css/style.css"));
        }
    }
}
