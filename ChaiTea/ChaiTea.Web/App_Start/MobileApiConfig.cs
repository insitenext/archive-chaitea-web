﻿using System.Web.Http;

namespace ChaiTea.Web
{
    public static class MobileApiConfig
    {
        public const string UrlPrefix = "api/mobile";

        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultMobileApi",
                                       UrlPrefix + "/{controller}/{id}",
                                       defaults: new
                                                 {
                                                     id = RouteParameter.Optional
                                                 });
        }
    }
}
