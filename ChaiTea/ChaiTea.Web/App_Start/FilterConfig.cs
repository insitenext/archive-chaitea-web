﻿using System.Web.Mvc;
using ChaiTea.Web.Helpers;

namespace ChaiTea.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorHandlerAttribute() { Order = 1 });

            filters.Add(new HandleAntiforgeryTokenErrorAttribute()
            {
                ExceptionType = typeof(HttpAntiForgeryException),
                Order = 2
            });
        }
    }
}
