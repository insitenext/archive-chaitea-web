﻿using System.Web.Http;

namespace ChaiTea.Web
{
    public static class WebApiConfig
    {
        public const string UrlPrefix = "api/Services";
        public const string UrlPrefix_v1_1 = "api/v1.1/Services";
        public const string UrlPrefix_v1_2 = "api/v1.2/Services";

        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultUserContextActionApi",
                                       UrlPrefix + "/User/{action}/{id}/{save}",
                                       defaults: new
                                       {
                                           area = "Services",
                                           controller = "User",
                                           id = RouteParameter.Optional,
                                           save = RouteParameter.Optional
                                       });

            config.Routes.MapHttpRoute("DefaultServiceApi",
                                       UrlPrefix + "/{controller}/{id}",
                                       defaults: new
                                       {
                                           id = RouteParameter.Optional
                                       });
        }
    }
}