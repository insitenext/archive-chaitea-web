﻿using System.Collections.Generic;

namespace ChaiTea.Web.Models
{
    public class NavigationVm
    {
        public string UserName { get; set; }
        public string ActiveTab { get; set; }
        public int AlertCount { get; set; }
        public int EscalationCount { get; set; }
        public int NotificationCount { get; set; }
        public int PassdownCount { get; set; }
    }

    public class MenuItem
    {
        public MenuItem(string name,  string url,string iconName = "", List<SubMenuItem> childItems = null,string[] roleNameRequired = null)
        {
            Name = name;
            IconName = iconName;
            Url = url;
            ChildrenItems = childItems ?? new List<SubMenuItem>();
            RoleNameRequired = roleNameRequired;
        }

        public string Name { get; set; }
        public string IconName { get; set; }
        public string Url { get; set; }
        public List<SubMenuItem> ChildrenItems { get; set; }
        public string[] RoleNameRequired { get; set; }
    }

    public class SubMenuItem
    {
        public SubMenuItem(string name,string url,string[] roleNameRequired = null)
        {
            Name = name;
            Url = url;
            RoleNameRequired = roleNameRequired;
        }

        public string Name { get; set; }
        public string Url { get; set; }
        public string[] RoleNameRequired { get; set; }
    }
}