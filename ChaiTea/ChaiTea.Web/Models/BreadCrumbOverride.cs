﻿namespace ChaiTea.Web.Models
{
    public class BreadCrumbOverride
    {
        public BreadCrumbOverride()
        {
            AreaName = string.Empty;
            AreaUrl = string.Empty;
            ControllerName = string.Empty;
            ControllerUrl = string.Empty;
            ActionName = string.Empty;
            ActionUrl = string.Empty;
        }
        private string _areaName { get; set; }
        private string _areaUrl { get; set; }
        private string _controllerName { get; set; }
        private string _controllerUrl { get; set; }
        private string _actionName { get; set; }
        private string _actionUrl { get; set; }

        public string AreaName
        {
            get
            {
                if (_areaName == string.Empty)
                    _areaName = null;
                return _areaName;
            }
            set
            {
                _areaName = value;
            }
        }
        public string AreaUrl
        {
            get
            {
                if (_areaUrl == string.Empty)
                    _areaUrl = null;
                return _areaUrl;
            }
            set
            {
                _areaUrl = value;
            }
        }
        public string ControllerName
        {
            get
            {
                if (_controllerName == string.Empty)
                    _controllerName = null;
                return _controllerName;
            }
            set
            {
                _controllerName = value;
            }
        }
        public string ControllerUrl
        {
            get
            {
                if (_controllerUrl == string.Empty)
                    _controllerUrl = null;
                return _controllerUrl;
            }
            set
            {
                _controllerUrl = value;
            }
        }

        public string ActionName
        {
            get
            {
                if (_actionName == string.Empty)
                    _actionName = null;
                return _actionName;
            }
            set
            {
                _actionName = value;
            }
        }
        public string ActionUrl
        {
            get
            {
                if (_actionUrl == string.Empty)
                    _actionUrl = null;
                return _actionUrl;
            }
            set
            {
                _actionUrl = value;
            }
        }
    }
}
