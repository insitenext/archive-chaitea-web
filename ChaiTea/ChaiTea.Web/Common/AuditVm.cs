﻿namespace ChaiTea.Web.Common
{
    public class AuditVm
    {
        public int? Id { get; set; }
    }

    public class ComplaintVm
    {
        public int? Id { get; set; }
    }

    public class ProfileEntryVm
    {
        public int? Id { get; set; }
    }

    public class ComplimentVm
    {
        public int? Id { get; set; }
    }

    /*
    public class EmployeeAuditVm
    {
       
    }
    */

    public class WorkOrderVm
    {
        public int? Id { get; set; }
        public int? WorkOrderSeriesId { get; set; }
        public WorkOrderMode State { get; set; }
    }

    public enum WorkOrderMode
    {
        Edit,
        Close
    }
}