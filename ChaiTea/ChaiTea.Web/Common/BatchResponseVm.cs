﻿using System.Collections.Generic;

namespace ChaiTea.Web.Common
{
    public class BatchResponseVm<T> : List<ResponseVm<T>>
    {
        public ResponseVm<T> AddSuccess(T body)
        {
            var response = new ResponseVm<T> { Status = ResponseStatus.OK, Body = body };
            Add(response);
            return response;
        }

        public ResponseVm<T> AddFailure(string message)
        {
            var response = new ResponseVm<T> { Status = ResponseStatus.EXCEPTION, Message =  message };
            Add(response);
            return response;
        }
    }

    public class ResponseVm<T>
    {
        public ResponseStatus Status { get; set; }
        public T Body { get; set; }
        public string Message { get; set; }
    }
    
    public enum ResponseStatus
    {
        OK = 1,
        EXCEPTION = 0
    }
}
