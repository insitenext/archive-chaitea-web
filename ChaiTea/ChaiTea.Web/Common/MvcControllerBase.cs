﻿using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.AppUtils.Settings;
using ChaiTea.BusinessLogic.Entities.Utils.Translation;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.Settings;
using ChaiTea.Web.Areas.Services.Models;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Identity;
using ChaiTea.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ChaiTea.Web.Common
{
    [AuthorizedRoles]
    public class MvcControllerBase : Controller
    {
        private IUserContext _usrContext;
        protected IUserContext UserCtx
        {
            get
            {
                if (_usrContext == null)
                {
                    IEnumerable<Claim> contextItemsFromClaims = null;
                    var identity = User?.Identity;
                    if (identity != null && identity is ClaimsIdentity)
                    {
                        contextItemsFromClaims = (identity as ClaimsIdentity).Claims;
                    }

                    var contextItemsFromHeader = Request.Headers
                                                        .AllKeys
                                                        .Select(t => new KeyValuePair<String, String>(t, Request.Headers[t]))
                                                        .ToList();
                    _usrContext = UserContextService.BuildFromRequest(true, UserContextService.CurrentUserId, contextItemsFromHeader, contextItemsFromClaims);
                }
                return _usrContext;
            }
        }

        public string DisplayName
        {
            get;
            private set;
        }

        private class Timezone {
            public string Site { get; set; }
            public string Client { get; set; }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                if (User.Identity.AuthenticationType == ApplicationOAuthProvider.AuthenticationType)
                { ViewBag.JwtAuthHeader = Request.Headers["Authorization"]; }

                var uC = UserCtx;
                if (uC != null)
                {
                    var contextService = new UserContextService(UserCtx);
                    ViewBag.UserContext = contextService.ToVm();
                    ViewBag.SiteOrBuilding = uC.SiteId.HasValue ? "Building" : "Site";
                    var isSecurityProgram = false;
                    if(uC.SiteId.HasValue && uC.ProgramId.HasValue)
                    {
                        var ProgramName = new ProgramService(uC).GetName(uC.ProgramId.Value);
                        if(ProgramName == "Security")
                        { isSecurityProgram = true; }
                    }
                    ViewBag.SiteOrBuilding = isSecurityProgram ? "Post" : ViewBag.SiteOrBuilding;
                    ViewBag.SitesOrBuildings = uC.SiteId.HasValue ? "BUILDINGS" : "SITES";
                    ViewBag.TypeOrProgram = uC.ProgramId.HasValue ? "Type" : "Program";
                    var usrFirstName = new UserInfoService(uC).GetFirstName(uC.UserId);
                    DisplayName = usrFirstName ?? string.Empty;
                    if (uC != null && uC.LanguageId != Language.Default)
                    {
                        string langCode = String.Empty;
                        if (string.IsNullOrEmpty(langCode))
                        {
                            var langId = uC.LanguageId;
                            using (var db = new BusinessLogic.DAL.ApplicationDbContext())
                            {
                                langCode = db.Set<Language>().Find(langId)?.Code;
                            }
                        }

                        if (string.IsNullOrEmpty(langCode))
                        { langCode = "en"; }

                        try
                        {
                            Thread.CurrentThread.CurrentCulture = new CultureInfo(langCode);
                            Thread.CurrentThread.CurrentUICulture = new CultureInfo(langCode);
                        }
                        catch (Exception cultureException)
                        {
                            MvcApplication.Logger.Warn($"Unable to set Thread Cultures based on LangCode [{langCode}] message [{cultureException.Message}]", cultureException);
                        }
                    }
                    ViewBag.MainRoles = typeof(Web.Identity.Roles)
                                        .GetFields()
                                        .Select(field => field.GetRawConstantValue().ToString())
                                        .ToDictionary(role => role.Replace(" ", ""),
                                                        role => contextService.IsInRole(role));
                    ViewBag.OrgUserId = contextService.GetOrgUserId();
                }

                var timezone = new Timezone() {
                    Site = null,
                    Client = null
                };

                var orgSetting = new OrgSettingService(UserCtx);
                if (UserCtx.SiteId.HasValue)
                {
                    var orgTz = orgSetting.Get(UserCtx.SiteId.Value);
                    if (orgTz != null)
                    {
                        timezone.Site = orgTz.Timezone;
                    }
                }
                if (UserCtx.ClientId.HasValue)
                {
                    var orgTz = orgSetting.Get(UserCtx.ClientId.Value);
                    if (orgTz != null)
                    {
                        timezone.Client = orgTz.Timezone;
                    }
                }
                ViewBag.Timezone = timezone;
            }

            ViewBag.PageNavigation = new NavigationVm()
            {
                UserName = DisplayName,
                AlertCount = 0,
                EscalationCount = 0,
                NotificationCount = 0,
                PassdownCount = 0
            };
            ViewBag.NavigationConfig = new HtmlString(AppUtility.Serialize(new WebNavigationVm
            {
                WebDomain = EnvironmentSettings.WebDomain,
                WebDomainRedirector = EnvironmentSettings.WebDomainRedirector
            }));
            ViewBag.AwsConfig = new HtmlString(AppUtility.Serialize(new EndpointVm
            {
                Bucket = AwsSettings.Bucket,
                Region = AwsSettings.S3Region,
                Cdn = AwsSettings.CloudfrontUrl
            }));
            ViewBag.BreadCrumbOverride = new BreadCrumbOverride();
            ViewBag.GoogleConfig = new GoogleVm()
            {
                Key = GoogleSettings.MapKey,
                Libraries = "places"
            };

            base.OnActionExecuting(filterContext);
        }
    }
}