/// <reference path="Content/js/Training/Templates/KnowledgeCenter/knowledgecenter.transcript.row.html" />
/// <binding ProjectOpened='watch' />
var gulp = require('gulp'),
    debug = require('gulp-debug'),

    gulpsass = require('gulp-sass'),
    ts = require('gulp-typescript'),
    gutil = require('gutil'),
    jasmine = require('gulp-jasmine'),
    karma = require('gulp-karma'),
    shell = require('gulp-shell'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat')
    prompt = require('gulp-prompt');
    merge = require('gulp-merge-json'),
    jsonTransform = require('gulp-json-transform');

var extractTranslate = require('gulp-angular-translate-extractor');

var Server = require('karma').Server;

var paths = {
    sass: {
        src: ['Content/css/**/*.scss',
            '!Content/css/Vendor/*'],
        dest: 'Content/css'
    },
    sassComponents: {
        src: ['Content/js/**/*.scss'],
        dest: 'Content/js/'
    },
    typescript: {
        src: ['Content/js/**/*.ts'],
        dest: 'Content/js/'
    }
}

function swallowError(error) {
    gutil.log(error);

    this.emit('end');
}

gulp.task('typescript', function () {
    gulp.src(paths.typescript.src)
    .pipe(ts({
    }))
    .pipe(gulp.dest(paths.typescript.dest));
});

gulp.task('build-css', function () {
    console.log('==== SCSS changed ==== \r\n ');
    gulp.src(paths.sass.src)
        .pipe(sourcemaps.init())
        .pipe(gulpsass()).on('error', swallowError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.sass.dest));
});

gulp.task('build-css-components', function () {
    console.log('==== Components SCSS changed ==== \r\n ');
    gulp.src(paths.sassComponents.src)
        .pipe(sourcemaps.init())
        .pipe(gulpsass()).on('error', swallowError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.sassComponents.dest));
});

gulp.task('watch', ['build-css', 'build-css-components'], function () {
    gulp.watch(paths.sass.src, ['build-css']);
    gulp.watch(paths.sassComponents.src, ['build-css-components']);
});

gulp.task('test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('clean:js', shell.task([
    'npm run clean'
]));

gulp.task('build-npm-js', function () {
    return gulp.src([
        './node_modules/angular/angular.js',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        './node_modules/angular-wizard/dist/angular-wizard.js',
        './node_modules/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
        './node_modules/ngFlowGrid/src/ngFlowGrid.js',
        './node_modules/lodash/index.js',
        './node_modules/bootbox/bootbox.js',
        './node_modules/toastr/toastr.js',
        './node_modules/moment/moment.js',
        './node_modules/async/dist/async.min.js',
        './node_modules/angular-ui-grid/ui-grid.min.js',
        './node_modules/ui-select/dist/select.min.js',
        './node_modules/angular-busy/dist/angular-busy.js',
        './node_modules/angular-simple-logger/dist/angular-simple-logger.js',
        './node_modules/angular-google-maps/dist/angular-google-maps.js',
        './node_modules/angular-signalr-hub/signalr-hub.js',
        './node_modules/numeral/numeral.js',
        './node_modules/angular-zendesk-widget/dist/angular-zendesk-widget.js',
        './node_modules/pdfjs-dist/build/pdf.combined.js',
        './node_modules/angular-pdf/dist/angular-pdf.js',
        './node_modules/zooming/dist/zooming.js'
    ])
    .pipe(concat('npm-packages.js'))
    .pipe(gulp.dest('./Scripts/'));
});

gulp.task('build-npm-css', ['copy-font-files'], function () {
    return gulp.src([
        './node_modules/angular-wizard/dist/angular-wizard.css',
        './node_modules/angular-bootstrap-colorpicker/css/colorpicker.css',
        './node_modules/angular-ui-grid/ui-grid.min.css',
        './node_modules/ui-select/dist/select.min.css',
        './node_modules/angular-busy/dist/angular-busy.css'
    ])
    .pipe(concat('npm-packages.css'))
    .pipe(gulp.dest('./Content/css/Vendor/'));
});

gulp.task('copy-font-files', function () {
    return gulp.src([
        './node_modules/angular-ui-grid/*.{svg,ttf,woff,eot}'
    ])
    .pipe(gulp.dest('./Content/css/Vendor/'));
})

/*
 * TRANSLATE GENERATION
 */
gulp.task('translate-files', function () {
    var i18nSrc = ['./Areas/Training/Views/KnowledgeCenter/*.cshtml'
                    , './Content/js/Training/Templates/KnowledgeCenter/*.html'
                    , './Content/js/Training/Templates/training.knowledgecenter.takequiz.tmpl.html'
                    , './Content/js/Training/Templates/Course/course.rating.entry.tmpl.html'
                    , './Areas/Training/Views/ServiceExcellence/Index.cshtml'
                    , './Areas/Personnel/Views/BoxScore/Index.cshtml'
                    , './Views/Shared/_LeftSideBar.cshtml'
                    , './Views/Home/Index.cshtml'
                    , './Areas/Quality/Views/Audit/Index.cshtml'
                    , './Areas/Quality/Views/Audit/Details.cshtml'
                    , './Areas/Quality/Views/Audit/Entry.cshtml'
                    , './Areas/Quality/Views/Audit/EntryView.cshtml'
                    , './Areas/Quality/Views/Complaint/Index.cshtml'
                    , './Areas/Quality/Views/Complaint/Details.cshtml'
                    , './Areas/Quality/Views/Complaint/Entry.cshtml'
                    , './Areas/Quality/Views/Complaint/EntryView.cshtml'
                    , './Areas/Quality/Views/Compliment/Index.cshtml'
                    , './Areas/Quality/Views/Compliment/Details.cshtml'
                    , './Areas/Quality/Views/Compliment/Entry.cshtml'
                    , './Areas/Quality/Views/Compliment/EntryView.cshtml'
                    , './Areas/Quality/Views/CustomerSurvey/Index.cshtml'
                    , './Areas/Quality/Views/CustomerSurvey/Details.cshtml'
                    , './Areas/Quality/Views/CustomerSurvey/Entry.cshtml'
                    , './Areas/Quality/Views/CustomerSurvey/SurveyResponse.cshtml'
                    , './Areas/Quality/Views/WorkOrder/Index.cshtml'
                    , './Areas/Quality/Views/WorkOrder/Details.cshtml'
                    , './Areas/Quality/Views/WorkOrder/Entry.cshtml'
                    , './Areas/Quality/Views/WorkOrder/EntryView.cshtml'
                    , './Areas/Quality/Views/WorkOrder/Upcoming.cshtml'
                    , './Content/js/Quality/Templates/quality.workorder.finalize.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.review.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.step1.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.step2.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.step3.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.step3m.tmpl.html'
                    , './Content/js/Quality/Templates/quality.workorder.submitmessage.tmpl.html'
                    , './Areas/Personnel/Views/Ownership/Index.cshtml'
                    , './Areas/Personnel/Views/Ownership/Details.cshtml'
                    , './Areas/Personnel/Views/Ownership/Entry.cshtml'
                    , './Areas/Personnel/Views/Ownership/EntryView.cshtml'
                    , './Content/js/Personnel/Templates/Ownership/accept-modal-tmpl.html'
                    , './Content/js/Personnel/Templates/Ownership/reject-modal-tmpl.html'
                    , './Areas/Settings/Views/MessagesReceived/Index.cshtml'
                    , './Content/js/Settings/Templates/ChaiTea.Settings.EmployeeMessages.tmpl.html'
    ];
    var i18nDest = './Content/i18n/';

    return gulp.src(i18nSrc)
        .pipe(extractTranslate({
            defaultLang: 'en',       // default language
            lang: ['en', 'es', 'fr', 'gu', 'ar'],   // array of languages
            dest: i18nDest,             // destination, default '.'
            prefix: 'locale-temp-',           // output filename prefix, default ''
            safeMode: true,            // do not delete old translations, true - contrariwise, default false
            //stringifyOptions: true,     // force json to be sorted, false - contrariwise, default false
        }))
        .pipe(gulp.dest(i18nDest));
})

gulp.task('merge', function () {
    var existingTranslationFilesPath = [
    './Content/i18n/locale-en.json',
    './Content/i18n/locale-es.json',
    './Content/i18n/locale-fr.json',
    './Content/i18n/locale-gu.json',
    './Content/i18n/locale-ar.json'
    ];

    var existingFileName = [
        'locale-en.json',
        'locale-es.json',
        'locale-fr.json',
        'locale-gu.json',
        'locale-ar.json'
    ]
    var newTranslationFilesPath = [
         './Content/i18n/locale-temp-en.json',
        './Content/i18n/locale-temp-es.json',
        './Content/i18n/locale-temp-fr.json',
        './Content/i18n/locale-temp-gu.json',
        './Content/i18n/locale-temp-ar.json'
    ]
    var i18nDest = './Content/i18n/';

    mergeFiles = function () {
        for (var i = 0; i < existingTranslationFilesPath.length; i++) {
            var filesToMerge = [];

            filesToMerge.push(newTranslationFilesPath[i]);
            filesToMerge.push(existingTranslationFilesPath[i]);

            gulp.src(filesToMerge)
            .pipe(merge(existingFileName[i]))
            .pipe(gulp.dest(i18nDest));
        }
    }

    return mergeFiles();
})

/*
 * @description Adds translation key and value (value adds to EN file only) to just the EN file or all json files in the ./Content/i18n folder.
 */
gulp.task('translation-add', function () {
    var folder = './Content/i18n/';

    var key = '',
        val = ''.
        addToAllFiles = false;

    return gulp.src(folder + "*.json")
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'key',
        message: 'Enter the Key ( ex: HELLO_WORLD )'
    },
    {
        type: 'input',
        name: 'val',
        message: 'Enter the Value? ( ex: Hello world! )'
    },
    {
        type: 'confirm',
        name: 'addToAll',
        message: "Do you want to add this property to all i18n files?"
    },
    {
        type: 'confirm',
        name:'confirm',
        message: function (res) {
            return 'Are you sure you want to add \r\n\r\n "{' + res.key + ':"' + res.val + '"}" \r\n\r\n to the json file(s) in the "'+folder+' folder? (value only gets set in EN file)"';
        }
    }
    ], function (res) {
        if (!res || !res.confirm) {
            return false;
        }
        addToAllFiles = res.addToAll;
        key = res.key;
        val = res.val;
    }
    ))
    .pipe(jsonTransform(function (data, file) {
        if (!addToAllFiles && (file.relative.indexOf('en') < 0)) {
            return data;
        }

        data[key] = (file.relative.indexOf('en') >= 0) ? val : '';
        return data;
    }, '\t'))
    .pipe(gulp.dest(folder))
})

/*
 * @description Remove translation by key from all json files in the ./Content/i18n folder.
 */
gulp.task('translation-remove', function () {
    var folder = './Content/i18n/';

    var key = '',
        val = '';

    return gulp.src(folder + "*.json")
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'key',
        message: 'Enter the Key to find for deletion ( ex: HELLO_WORLD )'
    },
    {
        type: 'confirm',
        name: 'confirm',
        message: function (res) {
            return 'Are you sure you want to remove \r\n\r\n "{' + res.key + '}" \r\n\r\n from all json files in the "' + folder + ' folder?';
        }
    }
    ], function (res) {
        if (!res || !res.confirm) {
            return false;
        }
        key = res.key;
    }
    ))
    .pipe(jsonTransform(function (data, file) {
        if (data[key] !== undefined) {
            delete data[key];
        }

        return data;
    }, '\t'))
    .pipe(gulp.dest(folder))
})