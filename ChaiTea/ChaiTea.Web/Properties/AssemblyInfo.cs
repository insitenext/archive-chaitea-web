﻿// Copyright (c) SBM Site Services, LLC.  All rights reserved.

using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("ChaiTea.Web")]
[assembly: AssemblyDescription("ChaiTea.Web.dll")]
[assembly: AssemblyDefaultAlias("ChaiTea.Web.dll")]
[assembly: InternalsVisibleTo("ChaiTea.UnitTests")]