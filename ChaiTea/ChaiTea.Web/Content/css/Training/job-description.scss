﻿@import "../Common/_globals";
@import "../Common/_bootstrap-overrides";

.job-description {
    .spinner-box{
        padding:50px 20px;
        .fa{
            font-size:16px;
        }
    }

    .position-profile-head{
        margin-top: 15px;
        margin-bottom: 10px;

        .my-profile-page-heading{
            padding: 10px 25px;
        }
        .my-profile-my-options{
            margin: 0;
        }

        
    }
    
    .job-description-header{
        background-color: $default;
        margin-bottom: 20px;
        padding-top: 10px;
        .table-head{
            background-color: $default;
            color: $warning !important;
            font-size: 16px;
            padding-left: 30px;
            font-weight: 600;
            line-height: 18px;
            @include breakpoint(tablet){
                  font-size: 18px;
                  line-height: 22px;
                  padding-left: 15px;
            }
        }
        .emp-sign {
			width: 200px;
     
            @include breakpoint(mobilephone){
               margin: 0 auto;
               text-align: center;
               // float:right;
            }
           
			img {
				height: 75px;
				width: auto;

                
			}
            .supervisor-img{
                    height: 50px !important;
                }

		}
        .profile-print{
            color: $base-dark;
            font-size: 30px;
            @include breakpoint(mobilephone){
                position: relative;
                top: -184px;
            }
        }

        .emp-config{
            color: $base-dark;
            font-size: 30px;
            padding-left: 8px;
             @include breakpoint(mobilephone){
                position: relative;
                top: -160px;
            }
        }
    }
	//padding: 10px 25px 20px 25px;
    .supervisor-width-align{
        
        @media only screen 
        and (min-device-width : 768px) 
        and (max-device-width : 1024px) 
        and (orientation : landscape) {
          padding-left: 0!important;
        padding-right: 0;
        margin-bottom: 10px !important;
        }
         @media only screen 
        and (min-device-width : 768px) 
        and (max-device-width : 1024px) 
        and (orientation : portrait) {
        margin-bottom: 10px !important;
        }
    }
    .handbook-width-align{
        @media only screen 
        and (min-device-width : 768px) 
        and (max-device-width : 1024px) 
        and (orientation : landscape) {
            padding-left: 0 !important;
            padding-right: 0;
            a{
                font-size: 13px !important;
                
            }
        }

    }
	.header {
		background-color: #fff;
		text-align: left;
		margin-bottom: 20px;
		padding: 10px;

		@include breakpoint(mobilephone) {
			background-color: transparent;
			text-align: center;
		}

		h3 {
			text-transform: capitalize;
			font-weight: inherit;
			color: inherit;
			font-size: 24px;
			display: inline-block;
			padding-left: 10px;

			@include breakpoint(mobilephone) {
				padding-left: 0;
				font-size: 20px;
				max-width: 160px;
			}
		}

		.profile-img {
			border: 3px solid $base;
			border-radius: 50%;
			height: 90px;
			width: 90px;
			overflow: hidden;
			padding: 0;

			img {
				width: 100%;
				height: 100%;
				vertical-align: middle;
			}
		}

		.profile-details {
			padding-top: 15px;
			padding-left: 10px;

			h3 {
				padding-bottom: 0;
				margin-bottom: 0;
			}
		}
	}

	@include breakpoint(mobilephone) {
		//padding: 10px 0 20px 0;
		.header {
			margin: 0 auto;

			.profile-details {
				padding-top: 10px;

				.position {
					color: $base-dark;
				}
			}

			h3 {
				line-height: 30px;
			}
		}

		.employee-list {

			p {
				font-size: 14px !important;
				max-width: 185px;
			}
		}
	}

	.position {

		h3 {
			display: inline-block;
			vertical-align: top;
			margin: 0;
		}
	}

	.description {
		background-color: #fff;
		margin-bottom: 20px;
		padding-bottom: 20px;

		h3 {
			margin-top: 0;
			margin-bottom: 10px;
            color: $primary;
			&.subtitle {
				margin-bottom: auto;
				font-size: 16px;
                font-weight: 600;
                text-transform: uppercase;
                color: $warning;
			}
		}

        .summary{
            font-size: 16px;
            line-height: 28px;
            font-weight: 300;
            color: $dark-dark;
        }

		.sub {
		}
        .duties-desc{
            font-size: 14px;
            line-height: 24px;
            font-weight: 400;
            color: $dark-dark;
        }
		.duties {
			padding-top: 10px;
			padding-left: 30px;
		}

        .tasks{
            font-size: 14px;
            line-height: 24px;
            font-weight: 400;
            color: $dark-dark;
        }

        .sub-text{
            font-size: 14px;
            line-height: 24px;
            font-weight: 400;
            color: $dark-dark;
        }

        .btn-accept{
            font-size: 14px;
            font-weight: 400;
            background-color: $success;
            border-color: $success;
        }
	}

	.sidebar {

		@include breakpoint(tablet) {
			padding-right: 0;
            padding-left: 0;
		}
        @include breakpoint(desktop){
            padding-left: 15px;
        }

		.panel-heading {
			cursor: pointer;
			border-bottom: none;
			background: white;
			position: relative;

			.icon {
				margin-top: 9px;
				margin-right: 23px;
                width: 0;
                height: 0;
                background-color: $default;
                color: $primary;
			}

			.panel-header:after {
				font-family: 'Glyphicons Halflings';
				content: "\e113";
				float: right;
				color: grey;
				padding-top: 5px;
				position: absolute;
				top: 15px;
				right: 15px;
			}

			.panel-header {

				&.collapsed:after {
					content: "\e114";
				}
			}

			h3 {
				line-height: 16px;
                color: $primary;
                font-weight: 600;
                margin-bottom: 0 !important;
				&.sub-title {
					color: rgb(110, 110, 110);
					font-size: 12px;
                    margin-top: 5px;
                    margin-bottom: 0;
                    font-weight: 400;
					@include breakpoint(mobilephone) {
						font-size: 11px;
						font-weight: normal;
					}
				}
			}

			.title {
				display: inline-block;
				width: 77%;
			}
		}

		.panel-body {
            min-height: 100px;
           
			a {
				margin-top: 10px;
			}

			h3 {
				margin: 0;
				padding: 0;
				line-height: 15px;
			}

			&.training {
				/*text-align: center;*/
				ul {
					text-align: left;
					list-style-type: none;
				}

				.counter {
					color: $success;
					font-size: 46px;
				}

                li {
                    .fa-square-o {
                        color: $danger;
                        font-weight: 600;
                    }
                    .fa-check-square-o {
                        color:$success;
                        font-weight: 600;
                    }
                }
			}


			.item {
				margin-bottom: 20px;

				.type {
					font-size: 28px;
					color: #aaaaaa;
				}

				.success {
					font-size: 28px;
					color: $success;
				}

				.failed {
					font-size: 28px;
					color: $danger;
				}

				&.kpi{
					h3{
						color:$warning !important
					}
				}
			}

			.collapse-text-toggle {
				color: $primary;
				font-weight: 600;
				transition: all 0.3s;
				cursor: pointer;
			}

			&.jd {
				h4 {
					color: $primary;
					padding-top: 10px;
					margin: 0;
					font-size: 14px;
					font-weight: 600;
				}
			}

			&.supervisor {
				margin-left: 10px;

				p {
					font-size: 14px;
				}
			}

			.locations {
				padding-bottom: 10px;
			}

			.supervisor-details {
				h4 {
					margin-top: 5px;
					margin-bottom: 0;
				}
			}

			.status {
				position: relative;

				.type {
					position: absolute;
					right: 0;
				}

				.status {
					position: absolute;
					left: 0;
				}
			}
		}
	}

	.config {

		h3 {
			line-height: 31px;

			a {
				font-size: 12px;
				text-transform: none;
			}
		}

		#emp-photo img {
			max-height: 350px;
		}

		#tasks {
			.well {
				background-color: #f9f9f9;
				border: 1px solid #ccc;

				input {
					cursor: default;
				}
			}

			
		}
	}


	.well.emp-profile-well.emp-profile-lg {
		position: relative;
		text-align: center;

		@include screen('tablet') {
			margin-bottom: 20px;
			text-align: left;
		}

		.circle-img {
			width: 90px;
			height: 90px;
			margin: auto;
			position: relative;

			@include screen('tablet') {
				float: left;
			}
		}

		.row-details {
			margin-left: 0;
			padding-left: 0;

			@include screen('tablet') {
				float: left;
				margin-left: 10px;
			}
		}

		.emp-name {
			font-size: 24px;
			font-weight: 300;
			margin-bottom: 10px;
			margin-top: 20px;
            color: $primary;
			@include screen('tablet') {
				font-size: 22px;
				margin-top: 0;
			}
		}

		.emp-title {
			font-size: 14px;
			color: $dark-dark;
			display: inline;
			font-style: normal;
			font-weight: 400;

			@include screen('tablet') {
				font-size: 16px;
				display: block;
			}
		}

		p.sm {
			font-size: 12px;
			color: $dark-dark;
			display:block;
			margin-left: 5px;
			font-style: italic;
			border-left: none;
			padding-left: 7px;
            font-weight: 400;

			@include screen('tablet') {
				font-size: 12px;
				margin-left: 0;
				padding-left: 0;
				border-left: none;
			}

			&:last-of-type {
				clear: both;
				display: block;
				border: none;
				padding-bottom: 10px;
			}
		}

		.btn.emp-config {
			position: absolute;
			right: 5px;
			top: -3px;
            color: $base-dark;
			i {
				font-size: 24px;
			}
		}

        .btn.profile-print{
            position: absolute;
            right: 35px;
            top: -326px;
            color: $base-dark;
            @include breakpoint(mobileonly){
                top: -344px;
            }
            @include breakpoint(desktop){
                top: -3px;
            }
            i{
                font-size: 22px;
            }
        }

        .supervisor-img{
            height: 50px !important;
            width: auto;
        }
        .acceptancedt{
            font-size: 14px;
            font-weight: 400;
            font-style : italic;
            color: $dark-dark;
        }
	}

	.full-jd {
		margin-top: 0;
		padding-top: 15px;
		// padding-bottom: 20px;
		position: relative;

		@include screen('tablet') {
			margin-top: 10px;
			padding: 20px;
		}

		.card-content {
			display: none;

			@include screen('tablet') {
				display: block;
			}

			p {
				font-size: 16px;
				line-height: 24px;
				font-weight: 300;
				margin: 5px 0 20px 0;

				&.lg {
					@include screen('tablet') {
						font-size: 20px;
						line-height: 34px;
					}
				}
			}
		}

		.iconed-title {
			cursor: pointer;
			margin-bottom: 25px;
		}

		.jd-title {
			text-transform: uppercase;
			color: $primary;
			font-size: 16px;
			font-weight: 600;
			margin-bottom: 3px;
			padding-top: 2px;
			margin: 5px 0;
			margin-bottom: 20px;
		}

		.jd-subtitle {
			color: $warning;
			text-transform: capitalize;
			margin-bottom: 10px;
			font-size: 14px;
		}




		ul {
			padding-left: 30px;
			list-style: initial;
			margin-bottom: 50px;
			padding-top: 10px;
			font-size: 16px;
			line-height: 26px;
			font-weight: 300;

			li {
				margin: 2px 0;
			}
		}
	}

	.card-content, .panel-body {
		margin-top: 20px;
		margin-bottom: 30px;
		padding-left: 40px;

		hr {
			margin: 10px auto;
		}

		b {
			font-size: 24px;
			line-height: 30px;
			font-weight: 300;
			color: $primary;
		}

		&.pri-block {

			p {
				font-size: 16px;
				line-height: 28px;
			}
		}

		ul.list-block {
			margin-bottom: 20px;

			li {
				border-bottom: 1px solid $border;
				line-height: 32px;
			}
		}

		ul.list-block-custom {
			list-style: none;
			padding-left: 0;
			margin-left: 0;
		}

		p {
			font-size: 16px;
			line-height: 28px;
			font-weight: 300;
            color: $dark-dark;

			&.sm {
				font-style: italic;
				font-weight: 400;
			}
		}
	}

	.training-card.card-content {
		h3 {
			font-size: 46px;
			color: $primary;
			float: left;
			padding-right: 15px;
			margin-right: 15px;
			margin-top: 0;
			line-height: 1;
			border-right: 1px solid $border;
		}
	}

	.actual-jd {
		@include breakpoint(mobilephone) {
			margin: 0;
			padding: 0;
			margin-bottom: 50px;
		}

		.well {
			padding: 10px;
		}

		.panel {
			border: 0;
			border-bottom: 1px solid #e4e4e4;
			box-shadow: none;
			margin-bottom: 0;

			&.no-border-bottom {
				border-bottom: none;

				@include breakpoint(mobilephone) {
					border-bottom: 1px solid #e4e4e4;
				}
			}

			.panel-heading {
			}

			&.has-error {
				.icon {
					background-color: $danger;
				}

				h3:not(.sub-title) {
					color: $danger;
				}
			}
		}

		#tasks{
			.list-block {
				li {
					position: relative;
				}
			}
		}
	}

	.supv-card {


		.supv {

			.supv-content {
				// padding-left: 50px;
				position: relative;
				margin-bottom: 10px;
				/*&:last-child {
						margin-left: 40px;
						position: relative;

						&:after, &:before {
							content: '';
							position: absolute;
							background: $primary;
							left: -22px;
						}

						&:after {
							width: 20px;
							height: 2px;
							top: 19px;
						}

						&:before {
							width: 2px;
							height: 55px;
							top: -35px;
						}
					}*/
			}

			h4 {
				margin: 0;
				color: $primary;
			}
		}
	}

	.supv-img {
		width: 38px;
		height: 38px;
		border-radius: 50%;
		overflow: hidden;
		margin-bottom: 20px;
		float: left;
		margin-right: 13px;

		img {
			width: 100%;
		}
	}


	.hand-book {
		a {
			font-size: 14px;
			font-weight: 300 !important;
			vertical-align: middle;

			i {
				font-size: 26px !important;
				
			}
		}
	}

    .fun-facts{
        position: relative;
        padding-left: 2px;
        bottom: 2px;
    }
}

// "Accept and Sign" modal
.jd-sign-terms {
	margin-top: 30px;
	border: none;

	.check-row {
		width: 30px;

		input {
			margin-top: 10px;
		}
	}

	label {
		color: inherit;
		display: block;

		b {
			text-transform: uppercase;
			font-size: 16px;
			color: $primary;

			@include screen('tablet') {
				font-size: 24px;
			}
		}

		p {
			font-size: 16px;
			font-weight: 400;
			color: inherit;
		}
	}
}

.sep-results {
	/*border-bottom: 2px solid #4699c9;*/
    @include breakpoint(mobilephone){
        padding-top:10px;
    }
}

@media only screen and (min-width: 990px) {
	.sep-results {
		margin-bottom: 10px;
		border-bottom: none;
	}
}

.sep-results [class^='col-'] {
	border: 1px solid #e4e4e4;
	margin-left: -1px;
	text-align: center;
	padding-top: 5px;
	padding-bottom: 10px;
	cursor: pointer;
}

.sep-results [class^='col-']:hover, .sep-results [class^='col-'].active-sep,.sep-results [class^='col-'].active {
	border-bottom: none;
	box-shadow: inset 0 -5px #4699c9;
}

.sep-results [class^='col-'].active-sep,.sep-results [class^='col-'].active {
	background: #f3f4f5;
}

.sep-results [class^='col-'] b {
	font-size: 20px;
	font-weight: 300;
}

@media only screen and (min-width: 990px) {
	.sep-results [class^='col-'] b {
		font-size: 40px;
	}
}

.sep-results [class^='col-'] p {
	font-size: 10px;
	text-transform: uppercase;
	font-weight: 700;
	line-height: 1.2;
}

@media only screen and (min-width: 990px) {
	.sep-results [class^='col-'] p {
		font-size: 12px;
	}
}

p.sep-status {
	margin: 0;
	margin-top: 3px;
	text-align: center;
	text-transform: uppercase;
	font-size: 10px;
	line-height: 18px;
	width: 60px;
}

@media only screen and (min-width: 990px) {
	p.sep-status {
		width: 90px;
	}
}

.update-padding {
    padding-top: 25px;
    font-size: 12px;
}

.pf-btn-accept{
            font-size: 14px;
            font-weight: 400;
            background-color: $success;
            border-color: $success;
}