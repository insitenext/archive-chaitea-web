﻿declare var module:any; //temporary to resolve TS error

describe('ChaiTea.Common.Directives tests->', function () {
    var _$compile;
    var _$scope;
    var _form;
    var _input;

    beforeEach(module("app"));

    describe('DateValidations->', () => {
        beforeEach(inject(function($rootScope, $compile) {
            _$scope = $rootScope.$new();
            _$compile = $compile;

            var element = angular.element(
                '<form name="form"><input name="dateField" type="text" data-ng-model="testModel" val-date-greater-than="{{ testDate }}"></form>');

            _$compile(element)(_$scope);
            _form = _$scope.form;
        }));

        it('should be invalid if the date model is before the set date on the val-date-greater-than custom attribute', () => {
            _form.dateField.$setViewValue(moment(new Date(12/1/2014)).format('MM/DD/YYYY'));
            _$scope.testDate = moment(new Date()).format('MM/DD/YYYY');
            _$scope.$digest();

            expect(_form.dateField.$error.valDateGreaterThan).toBe(true);
        });
    });

    describe('Components->', () => {
        beforeEach(inject(function ($rootScope, $compile) {
            _$scope = $rootScope.$new();
            _$compile = $compile;

            var element = angular.element(
                '<input type="number" value="test"  />');

            _$compile(element)(_$scope);
            console.log(angular.element(element).val());
            _input = element;
        }));

        it('should reject an input if entered is non-numeric value', () => {
            //var e = $.Event('keydown');
            //e.which = 50;
            //angular.element(_input).trigger(e);
            angular.element(_input).triggerHandler('keydown', { which: 50 });
           _$scope.$digest();
            console.log(angular.element(_input).prop('value'));
        });
    })
}) 