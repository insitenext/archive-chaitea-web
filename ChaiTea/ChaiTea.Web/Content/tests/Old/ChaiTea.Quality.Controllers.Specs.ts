﻿declare var module: any; //temporary to resolve TS error
describe('ChaiTea.Quality.Controllers tests->', () => {
    var _$scope;
    var _$controller;

    beforeEach(module('app'));

    describe('ComplaintIndexCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplaintIndexCtrl', { $scope: _$scope });
        }));

        it('should have initial start and end date range values', () => {
            expect(_$controller.dateRange.startDate).toBeTruthy();
            expect(_$controller.dateRange.endDate).toBeTruthy();
        });

        it('should have a defined chart config', () => {
            expect(_$controller.chartsConfig).toBeDefined();
        });

        it('should have a default start date of 6 months before in MM-DD-YYYY format', () => {
            var expectedDate = moment(moment(new Date()).subtract(5, 'months').startOf('month')).format('MM-DD-YYYY');
            expect(_$controller.dateRange.startDate).toBe(expectedDate);
        });

        it('should have a default end date of today in MM-DD-YYYY format', () => {
            expect(_$controller.dateRange.endDate).toBe(moment(new Date()).format('MM-DD-YYYY'));
        });
    });

    describe('ComplimentIndexCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplimentIndexCtrl', { $scope: _$scope });
        }));

        it('should have initial start and end date range values', () => {
            expect(_$controller.dateRange.startDate).toBeTruthy();
            expect(_$controller.dateRange.endDate).toBeTruthy();
        });

        it('should have a defined chart config', () => {
            expect(_$controller.chartsConfig).toBeDefined();
        });

        it('should have a default start date of 6 months before in MM-DD-YYYY format', () => {
            var expectedDate = moment(moment(new Date()).subtract(5, 'months').startOf('month')).format('MM-DD-YYYY');
            expect(_$controller.dateRange.startDate).toBe(expectedDate);
        });

        it('should have a default end date of today in MM-DD-YYYY format', () => {
            expect(_$controller.dateRange.endDate).toBe(moment(new Date()).format('MM-DD-YYYY'));
        });
    });

    describe('ComplaintDetailCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplaintDetailCtrl', { $scope: _$scope });
        }));

        it('should call bootbox confirm method when delete handler gets invoked', () => {
            spyOn(bootbox, 'confirm');

            _$controller.deleteComplaint(1);
            expect(bootbox.confirm).toHaveBeenCalled();
        });

        it('should reset the records to skip counter if getComplaintsReset gets invoked', () => {
            expect(_$controller.recordsToSkip).toBe(0);

            _$controller.recordsToSkip = 20;
            _$controller.getComplaintsReset();
            expect(_$controller.recordsToSkip).toBe(0);
        });
    });

    describe('ComplimentDetailCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplimentDetailCtrl', { $scope: _$scope });
        }));

        it('should call bootbox confirm method when delete handler gets invoked', () => {
            spyOn(bootbox, 'confirm');

            _$controller.deleteCompliment(1);
            expect(bootbox.confirm).toHaveBeenCalled();
        });

        it('should reset the records to skip counter if getComplimentsReset gets invoked', () => {
            expect(_$controller.recordsToSkip).toBe(0);

            _$controller.recordsToSkip = 20;
            _$controller.getComplimentsReset();
            expect(_$controller.recordsToSkip).toBe(0);
        });
    });

    describe('ComplaintEditCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplaintEditCtrl', { $scope: _$scope });
        }));

        it('should have all the defined default array options', () => {
            var options = ['complaintTypes', 'buildings', 'preventableStatuses', 'floors', 'classifications', 'employees'];

            _.each(options, (option) => {
                expect(_.has(_$controller.options, option)).toBe(true);
            });
        });

        it('should set the entry as disabled if complaintId is zero and either programId or siteId is zero on new complaints entry', () => {
            var isEntryDisabled = _$controller.checkIfEntryIsDisabled(0, 0, 2000);

            expect(isEntryDisabled).toBe(true);
        });

        it('should call and redirect to details page when cancel handler gets invoked', () => {
            spyOn(_$controller, 'redirectToDetails');

            _$controller.cancel();
            expect(_$controller.redirectToDetails).toHaveBeenCalled();
        });       
    });

    describe('ComplimentEditCtrl->', () => {
        beforeEach(inject(($rootScope, $controller) => {
            _$scope = $rootScope.$new();

            _$controller = $controller('ComplimentEditCtrl', { $scope: _$scope });
        }));

        it('should have all the defined default array options', () => {
            var options = ['complimentTypes', 'employees', 'buildings', 'floors'];

            _.each(options, (option) => {
                expect(_.has(_$controller.options, option)).toBe(true);
            });
        });

        it('should set the entry as disabled if complimentId is zero and either programId or siteId is zero on new compliments entry', () => {
            var isEntryDisabled = _$controller.checkIfEntryIsDisabled(0, 0, 2000);

            expect(isEntryDisabled).toBe(true);
        });

        it('should call and redirect to details page when cancel handler gets invoked', () => {
            spyOn(_$controller, 'redirectToDetails');

            _$controller.cancel();
            expect(_$controller.redirectToDetails).toHaveBeenCalled();
        });
    });
});

