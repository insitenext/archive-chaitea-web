﻿declare var module: any; //temporary to resolve TS error
describe('ChaiTea.Quality.Services tests->', () => {
    var _$controller;
    var _mockReportsSvc;
    var _$q;
    var _defer;

    beforeEach(module('app'));

    describe('Complaint Svc->', () => {
        beforeEach(inject(($rootScope, $q) => {
            _$q = $q;

            _mockReportsSvc = {
                getComplaintTrends: ()=> {
                    _defer = _$q.defer();
                    _defer.resolve([]);
                    return { $promise: _defer.promise };
                }
            }
        }));

        it('should call get complaint trends', () => {
            spyOn(_mockReportsSvc, 'getComplaintTrends');

            _mockReportsSvc.getComplaintTrends();
            expect(_mockReportsSvc.getComplaintTrends).toHaveBeenCalled();
        });
    });
}); 