﻿/// <reference path="../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Services Services.Base ->', () => {

    var baseSvc: ChaiTea.Common.Services.IApiBaseSvc;
    
    var mockEndpoint: string = 'http://localhost/ChaiTea.Web/';

    //var mockEmployee = {
    //    UserId: 1, FirstName: 'Sajjad', LastName: 'Choudhry', Email: 'schoudhry@sbmcorp.com'
    //};
    var mockEmployee = { "UserId": 1, "FirstName": "Sajjad", "LastName": "Choudhry", "Email": "schoudhry@sbmcorp.com", "PhoneNumber": null, "CultureId": 91, "CultureCode": "en-US", "UserAttachments": [{ "UserAttachmentId": 762, "UniqueId": "userprofile/gjmpnixpllolnqorercx", "Name": null, "Description": null, "UserAttachmentType": "ProfilePicture", "AttachmentType": "Image" }] };

    ngDescribe({
        modules: 'app',
        inject: [
            '$http',
            '$httpBackend',
            '$q',
            '$resource',
            'sitesettings',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.apibasesvc'],
        http: {
            get: {
                'http://localhost/ChaiTea.Web/api/Services/UserInfo': [202, 1]
            }
        },
        tests: function (deps) {

            beforeEach(function () {
                baseSvc = deps['chaitea.common.services.apibasesvc'];
            });

            //test service
            it('has awssvc', function () {
                expect(typeof deps['chaitea.common.services.apibasesvc']).toEqual('object');
            });
            //test functions
            it('expects setEndPoint to be a function', function () {
                expect(typeof baseSvc.setEndPoint).toBe('function');
            });
            it('expects getResource to be a function', function () {
                expect(typeof baseSvc.getResource).toBe('function');
            });
            it('expects get to be a function', function () {
                expect(typeof baseSvc.get).toBe('function');
            });
            it('expects getById to be a function', function () {
                expect(typeof baseSvc.getById).toBe('function');
            });
            it('expects getByOdata to be a function', function () {
                expect(typeof baseSvc.getByOdata).toBe('function');
            });
            it('expects getLookupList to be a function', function () {
                expect(typeof baseSvc.getLookupList).toBe('function');
            });
            it('expects query to be a function', function () {
                expect(typeof baseSvc.query).toBe('function');
            });
            it('expects save to be a function', function () {
                expect(typeof baseSvc.save).toBe('function');
            });
            it('expects update to be a function', function () {
                expect(typeof baseSvc.update).toBe('function');
            });
            it('expects updateAll to be a function', function () {
                expect(typeof baseSvc.updateAll).toBe('function');
            });
            it('expects delete to be a function', function () {
                expect(typeof baseSvc.delete).toBe('function');
            });

        }
    });

});
