﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services ImageSvc ->', () => {
    
    var imageSvc: ChaiTea.Common.Services.IImageSvc;
    var mockAwsAttachments = [{
        UserAttachmentType: 'ProfilePicture',
        UniqueId: 'imagetotest.png'
    }];

    angular.module('app').value('awsconfig', { Bucket: "sbm-insight-dev", Cdn: "d1h8kfd8pee7m7.cloudfront.net", Region: "us-west-1" });

    ngDescribe({
        modules: 'app',
        inject: [
            '$q',
            '$rootScope',
            '$log',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.utilssvc',
            'awsconfig',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.imagesvc'],
        tests: (deps) => {
            
            beforeEach(() => {
                imageSvc = deps['chaitea.common.services.imagesvc']; 
            });

            //test service
            it('has imageSvc', () => {
                expect(typeof imageSvc).toEqual('object');
            });
            //test functions
            it('compares get to "undefined"', () => {
                expect(typeof imageSvc.get).toBe('function'); 
            });
            it('compares getFromAttachments to "undefined"', () => {
                expect(typeof imageSvc.getFromAttachments).toBe('function'); 
            });
            it('compares getUserImageFromAttachments to "undefined"', () => {
                expect(typeof imageSvc.getUserImageFromAttachments).toBe('function'); 
            });
            it('compares save to "undefined"', () => {
                expect(typeof imageSvc.save).toBe('function'); 
            });
            //test get
            describe('ImageSvc -> get()', () => {
                it('should return a cloudfront path', () => {
                    expect(imageSvc.get('imagetotest.png')).toEqual('https://d1h8kfd8pee7m7.cloudfront.net/image/imagetotest.png');
                });
            });
            //test getFromAttachments
            describe('ImageSvc -> getFromAttachments()', () => {
                it('should return a cloudfront path by attachment type', () => {
                    expect(imageSvc.getFromAttachments(mockAwsAttachments, ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES.ProfilePicture, false)).toEqual('https://d1h8kfd8pee7m7.cloudfront.net/image/imagetotest.png');
                });
            });
            //test getUserImageFromAttachments
            describe('ImageSvc -> getUserImageFromAttachments()', () => {
                it('should return a cloudfront thumbnail path', () => {
                    expect(imageSvc.getUserImageFromAttachments(mockAwsAttachments, false)).toEqual('https://d1h8kfd8pee7m7.cloudfront.net/image/imagetotest.png');
                });
            });
            //test save

        }
    });

}); 
