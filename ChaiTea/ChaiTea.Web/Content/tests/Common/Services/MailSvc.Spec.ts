﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services MailSvc ->', () => {

    var mailSvc: ChaiTea.Common.Services.IMailSvc;

    angular.module('app').value('sitesettings', {
        fullDomain: "http://localhost",
        protocol: "http:",
        host: "localhost",
        basePath: "/ChaiTea.Web/",
        chartSettings: {
            barWidth: 30,
            groupPadding: 0.25
        },
        isDebuggingEnabled: true,
        isUat: false,
        isDev: false,
        userLanguage: "en",
        tempLanguage: null,
        defaultProfileImage: "/ChaiTea.Web/Content/img/user.jpg"
    });

    ngDescribe({
        modules: 'app',
        inject: [
            '$q',
            '$http',
            '$uibModal',
            'sitesettings',
            'chaitea.common.services.mailsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                mailSvc = deps['chaitea.common.services.mailsvc'];
            });

            //test service
            it('has awssvc', function () {
                expect(typeof mailSvc).toEqual('object');
            });

        }
    });

});
