﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services ReportHelperSvc ->', () => {

    var reportHelperSvc: ChaiTea.Common.Services.IReportSvc;

    angular.module('app').value('sitesettings', {
        fullDomain: "http://localhost",
        protocol: "http:",
        host: "localhost",
        basePath: "/ChaiTea.Web/",
        chartSettings: {
            barWidth: 30,
            groupPadding: 0.25
        },
        isDebuggingEnabled: true,
        isUat: false,
        isDev: false,
        userLanguage: "en",
        tempLanguage: null,
        defaultProfileImage: "/ChaiTea.Web/Content/img/user.jpg"
    });

    angular.module('app').value('userContext', { "UserId": 4705, "Settings": { "Clients": [{ "ID": 10001, "Name": "Apple Inc." }], "Sites": [{ "ID": 10025, "Name": "Elk Grove" }], "Programs": [] }, "Client": { "ID": 10001, "Name": "Apple Inc." }, "Site": { "ID": 10025, "Name": "Elk Grove" }, "Program": null });

    ngDescribe({
        modules: 'app',
        inject: [
            '$q',
            '$resource',
            'sitesettings',
            'userContext',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.reportsvc'],
        tests: function (deps) {

            beforeEach(function () {
                reportHelperSvc = deps['chaitea.common.services.reportsvc'];
            });

            it('has reporthelpersvc', function () {
                expect(typeof reportHelperSvc).toEqual('object');
            });

        }
    });

});