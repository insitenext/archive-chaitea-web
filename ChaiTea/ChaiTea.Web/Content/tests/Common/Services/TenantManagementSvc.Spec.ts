﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services TenantManagementSvc ->', () => {

    var tenantManagementSvc: ChaiTea.Common.Services.IAwsSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            'chaitea.common.services.tenantmanagementsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                tenantManagementSvc = deps['chaitea.common.services.tenantmanagementsvc'];
            });

            //test service
            it('has tenantmanagementsvc', function () {
                expect(typeof tenantManagementSvc).toEqual('object');
            });

        }
    });

});
