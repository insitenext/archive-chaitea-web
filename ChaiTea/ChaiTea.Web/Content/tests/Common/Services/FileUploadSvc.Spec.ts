﻿/// <reference path="../../../js/_libs.ts" />

//NOTE: THIS FILE WILL BE PHASED OUT SOON

declare var ngDescribe: any;

describe('ChaiTea.Common.Services FileUploadSvc ->', () => {

    var fileUploadSvc: ChaiTea.Common.Services.IAwsSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            '$q',
            'Upload',
            'chaitea.common.services.fileuploadsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                fileUploadSvc = deps['chaitea.common.services.fileuploadsvc'];
            });

            //test service
            it('has fileuploadsvc', function () {
                expect(typeof fileUploadSvc).toEqual('object');
            });

        }
    });

});
