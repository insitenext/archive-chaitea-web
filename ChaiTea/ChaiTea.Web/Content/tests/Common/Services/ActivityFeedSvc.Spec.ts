﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services ActivityFeedSvc ->', () => {

    var activityFeedSvc: ChaiTea.Common.Services.IActivityFeedSvc;

    angular.module('app').value('sitesettings', {
        fullDomain: "http://localhost",
        protocol: "http:",
        host: "localhost",
        basePath: "/ChaiTea.Web/",
        chartSettings: {
            barWidth: 30,
            groupPadding: 0.25
        },
        isDebuggingEnabled: true,
        isUat: false,
        isDev: false,
        userLanguage: "en",
        tempLanguage: null,
        defaultProfileImage: "/ChaiTea.Web/Content/img/user.jpg"
    });

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            'chaitea.common.services.activityfeedsvc'],
        tests: function (deps) {

            beforeEach(function () {
                activityFeedSvc = deps['chaitea.common.services.activityfeedsvc'];
            });

            //test service
            it('has activityfeedsvc', function () {
                expect(typeof activityFeedSvc).toEqual('object');
            });

        }
    });

});
