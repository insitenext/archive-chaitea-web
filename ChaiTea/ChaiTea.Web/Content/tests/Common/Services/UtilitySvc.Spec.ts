﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services UtilitySvc ->', () => {

    var utilitySvc: ChaiTea.Common.Services.IUtilitySvc;    

    ngDescribe({
        modules: 'app',
        inject: [
            '$q',
            '$rootScope',
            '$log',
            '$timeout',
            'chaitea.common.services.utilssvc'],
        tests: (deps) => {
            
            beforeEach(() => {
                utilitySvc = deps['chaitea.common.services.utilssvc'];
            });

            //test service
            it('has imageSvc', () => {
                expect(typeof utilitySvc).toEqual('object');
            });
            //test functions
            it('expects generateUniqueToken to be a function', () => {
                expect(typeof utilitySvc.generateUniqueToken).toBe('function');
            });
            it('expects utilitySvc to be a function', () => {
                expect(typeof utilitySvc.isJSONString).toBe('function');
            });

            describe('UtilitySvc -> generateUniqueToken()', () => {
                it('should return a random token', () => {
                    var token = utilitySvc.generateUniqueToken();
                    expect(utilitySvc.generateUniqueToken()).not.toBe(token);
                });

                it('should have length equal to 16', () => {
                    expect(utilitySvc.generateUniqueToken().length).toBe(36);
                });
            });

            describe('UtilitySvc -> isJSONString()', () => {
                it('should be valid json', () => {
                    expect(utilitySvc.isJSONString('{"foo": 1}')).toBe(true);
                });
                it('should be invalid json', () => {
                    expect(utilitySvc.isJSONString("Invalid json")).toBe(false);
                });
                   
            });

        }
    });

});
