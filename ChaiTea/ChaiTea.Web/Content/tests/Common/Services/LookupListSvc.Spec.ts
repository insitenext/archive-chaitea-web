﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services LookupListSvc ->', () => {

    var lookupListSvc: ChaiTea.Common.Services.ILookupListSvc;

    angular.module('app').value('sitesettings', {
        fullDomain: "http://localhost",
        protocol: "http:",
        host: "localhost",
        basePath: "/ChaiTea.Web/",
        chartSettings: {
            barWidth: 30,
            groupPadding: 0.25
        },
        isDebuggingEnabled: true,
        isUat: false,
        isDev: false,
        userLanguage: "en",
        tempLanguage: null,
        defaultProfileImage: "/ChaiTea.Web/Content/img/user.jpg"
    });

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            'chaitea.common.services.lookuplistsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                lookupListSvc = deps['chaitea.common.services.lookuplistsvc'];
            });

            //test service
            it('has lookuplistsvc', function () {
                expect(typeof lookupListSvc).toEqual('object');
            });

        }
    });

});
