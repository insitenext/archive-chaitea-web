﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services AwsSvc ->', () => {

    var awsSvc: ChaiTea.Common.Services.IAwsSvc;

    angular.module('app').value('awsconfig', { Bucket: "sbm-insight-dev", Cdn: "d1h8kfd8pee7m7.cloudfront.net", Region: "us-west-1" });

    ngDescribe({
        modules: 'app',
        inject: [
            '$log',
            '$q',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.utilssvc',
            'awsconfig',
            'chaitea.common.services.awssvc'],
        tests: function (deps) {

            beforeEach(function () {
                awsSvc = deps['chaitea.common.services.awssvc'];
            });

            //test service
            it('has awssvc', function () {
                expect(typeof awsSvc).toEqual('object');
            });

            /*
            getCredentials(creds: commonInterfaces.ISecurity): AWS.Credentials;
            s3MultipartUpload(file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, fileNameOverride?: string): angular.IPromise<any>;
            s3Upload(file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, isBase64?: boolean, fileNameOverride?: string): angular.IPromise<any>;
            encodeVideo(fileNameFull: string, creds: commonInterfaces.ISecurity, presets?: Array<string>): angular.IPromise<any>;
            getUrl(key: string): string;
            getSecureUrlByName(key: string, folder: commonInterfaces.S3Folders):angular.IPromise<any>;
            getUrlByName(fileName: string, folder: commonInterfaces.S3Folders): string;
            getSignedVideoUrl(videoName: string, videoSize?: number): angular.IPromise<commonInterfaces.IAwsVideoObject>;
            getUrlByAttachment(uniqueId: string, attachmentType: string): string;
            getFolderByType(fileType: string): commonInterfaces.S3Folders;
            getFolderByAttachmentType(attachmentType: string): commonInterfaces.S3Folders;
            getFileIcon(attachmentType: string): string;
            */

            it('should return valid AWS credentials', function () {

                var credentialSvc: ChaiTea.Services.ICredentialsSvc = deps['chaitea.services.credentials'];

                //

                //expect(typeof deps['chaitea.common.services.awssvc']).toEqual('object');
            });
            

            //this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {



        }
    });

});