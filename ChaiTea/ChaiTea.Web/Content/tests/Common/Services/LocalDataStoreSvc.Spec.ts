﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services LocalDataStoreSvc ->', () => {

    var localDataStoreSvc: ChaiTea.Common.Services.ILocalDataStoreSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            'localStorageService',
            '$q',
            'chaitea.common.services.utilssvc',
            'userContext',
            'chaitea.common.services.localdatastoresvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                localDataStoreSvc = deps['chaitea.common.services.localdatastoresvc'];
            });

            //test service
            it('has localdatastoresvc', function () {
                expect(typeof localDataStoreSvc).toEqual('object');
            });

        }
    });

});
