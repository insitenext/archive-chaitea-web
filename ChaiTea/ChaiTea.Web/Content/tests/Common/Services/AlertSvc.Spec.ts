﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services AlertSvc ->', () => {

    var alertSvc: ChaiTea.Common.Services.IAlertSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            'chaitea.common.services.alertsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                alertSvc = deps['chaitea.common.services.alertsvc'];
            });

            //test service
            it('has alertsvc', function () {
                expect(typeof alertSvc).toEqual('object');
            });

        }
    });

});
