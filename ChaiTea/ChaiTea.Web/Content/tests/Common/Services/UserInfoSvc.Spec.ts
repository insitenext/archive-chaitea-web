﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services UserInfoSvc ->', () => {

    var userInfoSvc: ChaiTea.Common.Services.IUserInfoSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            'chaitea.common.services.userinfosvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                userInfoSvc = deps['chaitea.common.services.userinfosvc'];
            });

            //test service
            it('has userinfosvc', function () {
                expect(typeof userInfoSvc).toEqual('object');
            });

        }
    });

});
