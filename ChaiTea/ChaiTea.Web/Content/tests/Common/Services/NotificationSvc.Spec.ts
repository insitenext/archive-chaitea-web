﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services NotificationSvc ->', () => {

    var notificationSvc: ChaiTea.Common.Services.INotificationSvc;
    
    angular.module('app').value('sitesettings', {
        fullDomain: "http://localhost",
        protocol: "http:",
        host: "localhost",
        basePath: "/ChaiTea.Web/",
        chartSettings: {
            barWidth: 30,
            groupPadding: 0.25
        },
        isDebuggingEnabled: true,
        isUat: false,
        isDev: false,
        userLanguage: "en",
        tempLanguage: null,
        defaultProfileImage: "/ChaiTea.Web/Content/img/user.jpg"
    });

    ngDescribe({
        modules: 'app',
        inject: [
            'Hub',
            'chaitea.common.services.utilssvc',
            'chaitea.core.services.messagebussvc',
            'sitesettings',
            'chaitea.common.services.activityfeedsvc',
            'chaitea.common.services.notificationsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                notificationSvc = deps['chaitea.common.services.notificationsvc'];
            });

            //test service
            it('has notificationsvc', function () {
                expect(typeof notificationSvc).toEqual('object');
            });

        }
    });

});
