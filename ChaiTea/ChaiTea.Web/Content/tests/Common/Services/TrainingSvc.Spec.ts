﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services TrainingSvc ->', () => {

    var trainingSvc: ChaiTea.Common.Services.ITrainingSvc; 

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            '$q',
            'chaitea.common.services.apibasesvc',
            'userContext',
            'chaitea.common.services.trainingsvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                trainingSvc = deps['chaitea.common.services.trainingsvc'];
            });

            //test service
            it('has trainingsvc', function () {
                expect(typeof deps['chaitea.common.services.trainingsvc']).toEqual('object');
            });

        }
    });

});
