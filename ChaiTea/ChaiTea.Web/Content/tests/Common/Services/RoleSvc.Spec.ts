﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services RoleSvc ->', () => {

    var roleSvc: ChaiTea.Common.Services.IAwsSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            'roleSvc'],
        tests: function (deps) {

            beforeEach(function () {
                roleSvc = deps['roleSvc'];
            });

            //test service
            it('has roleSvc', function () {
                expect(typeof roleSvc).toEqual('object');
            });

        }
    });

});
