﻿/// <reference path="../../../js/_libs.ts" />

declare var ngDescribe: any;

describe('ChaiTea.Common.Services CultureSvc ->', () => {

    var cultureSvc: ChaiTea.Common.Services.IAwsSvc;

    ngDescribe({
        modules: 'app',
        inject: [
            '$resource',
            'sitesettings',
            'chaitea.common.services.culturesvc'],
        tests: function (deps) {
            
            beforeEach(function () {
                cultureSvc = deps['chaitea.common.services.culturesvc'];
            });

            //test service
            it('has awssvc', function () {
                expect(typeof cultureSvc).toEqual('object');
            });

        }
    });

});
