﻿module ChaiTea.Dashboard.Controllers {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    const currentEmployeeIdQueryParam = Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString();
    import enums = Common.Enums;

    interface ILinkItem {
        title: string;
        subTitle?:string;
        icon: string;
        href:string;
    }
    interface IUserDashBoardWidget {
        UserDashboardWidgetId: number;
        UserId: number;
        DashboardWidgetId: number;
        IsVisible: boolean;
    }

    
    enum FileType { Undefined, ProfilePicture };

    class ConfigureModalCtrl implements ChaiTea.Common.Interfaces.INgController{
        staticDashboardTileInfo = [
            { id: enums.DashboardWidgets.TeamSafetyIncidents, name: 'TEAM_SAFETY', checked: false, dragging: false, editable: false},
            { id: enums.DashboardWidgets.ComplaintsTrend, name: 'COMPLAINTS_TREND', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.TodosStats, name: 'STATS_TODOS', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.AuditTrend, name: 'AUDIT_TREND', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.TeamHoursCurrentMonth, name: 'TEAM_HOURS', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.PositionProfilesCompletion, name: 'POSITION_PROFILE_PERCENT', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'COMPLIMENTS_RECENT', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.SurveysMostRecent, name: 'SURVEYS_RECENT', checked: false, dragging: false, editable: true},
            { id: enums.DashboardWidgets.MessagesDelivered, name: 'MESSAGES_DELIVERED', checked: false, dragging: false, editable: true },
            { id: enums.DashboardWidgets.FailedAuditsCurrentMonth, name: 'FAILED_AUDITS', checked: false, dragging: false, editable: true },
            { id: enums.DashboardWidgets.FailedInspectionItemTodosCurrentMonth , name: 'INSPECTION_TODOS', checked: false, dragging: false, editable: true },
            { id: enums.DashboardWidgets.ReAudits, name: 'REAUDITS', checked: false, dragging: false, editable: true },
            { id: enums.DashboardWidgets.Weather, name: 'WEATHER', checked: false, dragging: false, editable: true }
        ];
        dashboardInfo = [];
        dashboardId = Common.Enums.DashboardIds.Home;
        isClick: boolean = true;
        navigateToItem: number;
        widgets: enums.DashboardWidgets;
        isReAudit: boolean = false;
        static $inject = [
            '$scope',
            '$log',
            'userInfo',
            'userContext',
            'sitesettings',
            '$uibModal',
            '$window',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.notificationsvc',
            '$uibModalInstance',
            'dashBoardTileInfo'
        ];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $window: angular.IWindowService,
            private $timeout: angular.ITimeoutService,
            private apiSvc: Common.Services.IApiBaseSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private dashBoardTileInfo
        ) {
            this.getReAuditsConfig();
        }

        public initialize = () => {
        }

        private getReAuditsConfig = () => {
            this.apiSvc.query({}, 'AuditSetting', false, true).then((result) => {
                this.isReAudit = (result.length && _.any(result, { ReRunAuditsInternalAudit: true })) || false;
                if (!this.isReAudit) {
                    var indexStatic = _.findIndex(this.staticDashboardTileInfo, { id: enums.DashboardWidgets.ReAudits });
                    if (indexStatic != -1) {
                        this.staticDashboardTileInfo.splice(indexStatic, 1);
                    }
                    var indexTile = _.findIndex(this.dashBoardTileInfo, { id: enums.DashboardWidgets.ReAudits });
                    if (indexTile != -1) {
                        this.dashBoardTileInfo.splice(indexTile, 1);
                    }
                }
                this.getDashboardItems();
            }, this.onFailure);
        }

         /**
         * @description Make visible the items the user has already configured
         */
        private getDashboardItems = () => {
            var params = {
                userId: this.userContext.UserId,
                dashboardId: this.dashboardId
            }
            this.apiSvc.query(params, "UserDashboardWidget?userid=:userId&dashboardid=:dashboardId").then((result) => {
                this.dashboardInfo = result;
                this.checkUserTiles();
            }, this.onFailure);
        }

        /**
         * @description Make visible the items the user has already configured
         */
        private checkUserTiles = () => {
            angular.forEach(this.dashBoardTileInfo, (item, key) => {
                var obj = _.find(this.staticDashboardTileInfo, { 'id': item.id });
                if (obj && item.visible) {
                    obj.checked = true;
                }
                var oldIndex = _.findIndex(this.staticDashboardTileInfo, { 'id': item.id });
                if (key != oldIndex){
                    var obj = _.find(this.staticDashboardTileInfo, { 'id': item.id });
                    var oldObj = this.staticDashboardTileInfo[key];
                    this.staticDashboardTileInfo[key] = obj;
                    this.staticDashboardTileInfo[oldIndex] = oldObj;
                }
            });
        }

        /**
         * @description Make visible/invisible the items the user has selected
         */
        private changeItem = (index: number) => {
            if (this.staticDashboardTileInfo[index].editable) {
                this.staticDashboardTileInfo[index].checked = !this.staticDashboardTileInfo[index].checked;
                this.$timeout(() => {
                    this.staticDashboardTileInfo[index].dragging = false;
                }, 200);
                
            }
        }

        /**
         * @description Save dashboard Items
         */
        private saveItems = () => {
            var toBeUpdated = [];
            var toBeCreated = [];
            
            angular.forEach(this.staticDashboardTileInfo, (item) => {
                var obj = _.find(this.dashboardInfo, { 'DashboardWidgetId': item.id });
                if (!obj && item.checked) {
                    toBeCreated.push(item);
                }
            });
            angular.forEach(this.staticDashboardTileInfo, (item) => {
                var obj = <IUserDashBoardWidget> _.find(this.dashboardInfo, { 'DashboardWidgetId': item.id });
                if (obj && obj.IsVisible != item.checked) {
                    obj.IsVisible = item.checked;
                    toBeUpdated.push(obj);
                }
            });
            angular.forEach(toBeUpdated, (item) => {
                var params = {
                    UserDashboardWidgetId: item.UserDashboardWidgetId,
                    UserId: item.UserId,
                    DashboardWidgetId: item.DashboardWidgetId,
                    DashboardId: this.dashboardId,
                    IsVisible: item.IsVisible
                }
                this.apiSvc.update(item.UserDashboardWidgetId,params, "UserDashboardWidget").then((result) => {
                }, this.onFailure);
            });

            angular.forEach(toBeCreated, (item) => {
                var params = {
                    UserId: this.userContext.UserId,
                    DashboardWidgetId: item.id,
                    DashboardId: this.dashboardId,
                    IsVisible: item.checked
                }
                this.apiSvc.save(params, "UserDashboardWidget").then((result) => {
                }, this.onFailure);
            });
            var tileInfo = {
                staticDashboardTileInfo: this.staticDashboardTileInfo,
                isModified: true
            };
            this.$uibModalInstance.close(tileInfo);
        }

         /**
         * @description On mouse down
         */
        private checkDrag = (item) => {
            item.dragging = true;
            this.$timeout(() => {
                item.dragging = false;
            }, 1000);
        }

        /**
         * @description On dragging the options
         */
        private ondragStartOption = (id: number, item) => {
            this.navigateToItem = id;
            item.dragging = true;
        }

        /**
         * @description On dropping the option
         */
        private onDropCompleteOption = (index, item) => {
            this.staticDashboardTileInfo[index].dragging = false;
            if (item ) {
                this.toggleOptionTiles(index, item);
            }
        }

        /**
         * @description Set new index values
         */
         private toggleOptionTiles = ($index, item) => {
            this.isClick = true;
            var otherIndex = this.staticDashboardTileInfo.indexOf(item);
            this.staticDashboardTileInfo.splice($index, 0, item);
            if (otherIndex > $index) {
                this.staticDashboardTileInfo.splice(otherIndex + 1, 1);
            }
            else {
                this.staticDashboardTileInfo.splice(otherIndex, 1);
            }
        }

        /**
         * @description Dismiss the modal instance.
         */
         private close = (): void => {
             var tileInfo = {
                 staticDashboardTileInfo: this.staticDashboardTileInfo,
                 isModified: false
             };
            this.$uibModalInstance.close(tileInfo);
        };

         /**
         * @description log error and notify error to user
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

    }

    angular.module('app').controller('Dashboard.ConfigureModalCtrl', ConfigureModalCtrl);
    
} 
