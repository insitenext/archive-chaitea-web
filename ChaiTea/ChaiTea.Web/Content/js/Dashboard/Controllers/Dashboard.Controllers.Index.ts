﻿module ChaiTea.Dashboard.Controllers {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    const currentEmployeeIdQueryParam = Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString();
    import enums = Common.Enums;

    interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $select: string;
    }

    interface IDashboardItem {
        role:string;
        motivationalLine: string;
        linkSet:Array<ILinkItem>;
    }

    interface ILinkItem {
        title: string;
        subTitle?:string;
        icon: string;
        href:string;
    }
    interface ICountsAndTimeInfo {
        SafetyCount: number;
        ToDoCount: number;
        PastDueCount: number;
        PositionProfilePercentage: number;
        ProfileCompletedCount: number;
        TotalEmployees: number;
        ToDoClosureRate: number;
        LastAuditDate: Date;
        LastComplaintDate: Date;
        LastSafetyDate: Date;
        LastTeamHoursDate: Date;
        LastTodoDate: Date;
        LastTodoCloseOnTimeDate: Date;
        NextTodoPastDueDate: Date;
    }


    enum FileType { Undefined, ProfilePicture };

    enum AuditFiler {
        Fails = 1,
        FailedAudits = 2,
        FailsWithinAudits = 3
    }

    enum TodoFilter {
        Source = 1,
        FailedInspectionItem = 2,
        ReAudit = 3
    }

    class IndexCtrl implements ChaiTea.Common.Interfaces.INgController{
        dashboardItems:Array<IDashboardItem>;
        currentDashboardItem: IDashboardItem;

        coreColors: ICoreColors;
        employeeInfo: Training.Interfaces.ITrainingEmployee;
        isEmployee: boolean = false;
        userId: number;
        basePath: string;
        userProfilePath: string;
        modalInstance;
        isClick: boolean = true;
        timeClockViewer;
        goalLineWidth = 2;
        dashBoardInfo = {
            survey : {
                goal: 5,
                controlLimit: 0,
                latestScore: 0.0,
                latestDate: null
            },
            compliments: [],
            hours: {
                totalRegularHours: 0,
                totalSTEHours: 0,
                labels: [],
                regularHours: [],
                STEHours: []
            },
            audits: {
                categories: [],
                internalAuditData: [],
                jointAuditData: [],
                avgJointAuditsScore: 0,
                avgInternalAuditsScore: 0,
                control: 5,
                max: 5,
                failedAuditsCount: 0
            },
            complaints: {
                categories: [],
                inScope: [],
                outOfScope: [],
                inScopeCountAvg: 0,
                outOfScopeCountAvg: 0,
                control: 2
            },
            compliance: {
                totalEmployees: 0,
                totalCompliantEmployees: 0,
                compliancePercent: 0.00
            }
        };

        dashboardId = Common.Enums.DashboardIds.Home;
        navigateToViewName: string;
        dashBoardTileInfo = [
            { id: enums.DashboardWidgets.TeamSafetyIncidents, name: 'Safety', url: 'Personnel/Safety/', class: 'col-md-3 col-lg-3 col-xs-6 col-sm-6', visible: true },
            { id: enums.DashboardWidgets.ComplaintsTrend, name: 'Complaints', url: 'Quality/Complaint/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.TodosStats, name: 'Todo', url: 'Quality/ToDo/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.AuditTrend, name: 'Audits', url: 'Quality/Audit/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.TeamHoursCurrentMonth, name: 'Hours', url: 'Financials/TimeClock/', class: 'col-md-6 col-lg-6 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.PositionProfilesCompletion, name: 'PositionProfiles', url: 'Training/PositionProfile/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'Compliments', url: 'Quality/Compliment/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.SurveysMostRecent, name: 'Survey', url: 'Quality/CustomerSurvey/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.MessagesDelivered, name: 'MessagesDelivered', url: 'Settings/MessagesReceived/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.FailedAuditsCurrentMonth, name: 'FailedAudits', url: 'Quality/Audit/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.FailedInspectionItemTodosCurrentMonth , name: 'FailedInspectionTodos', url: 'Quality/Todo/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.ReAudits, name: 'ReAudits', url: 'Quality/ToDo/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.Weather, name: 'Weather', url: '', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false }
        ];
         
        mobileDashBoardTileInfo = [
            { id: enums.DashboardWidgets.TeamSafetyIncidents, name: 'Safety', url: 'Personnel/Safety/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: true },
            { id: enums.DashboardWidgets.ComplaintsTrend, name: 'Complaints', url: 'Quality/ToDo/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.TodosStats, name: 'Todo', url: 'Quality/Complaint/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.PositionProfilesCompletion, name: 'PositionProfiles', url: 'Training/PositionProfile/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.AuditTrend, name: 'Audits', url: 'Quality/Audit/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.TeamHoursCurrentMonth, name: 'Hours', url: 'Financials/TimeClock/', class: 'col-md-6 col-lg-6 col-xs-12 col-sm-12', visible: false },
            { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'Compliments', url: 'Quality/Compliment/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.SurveysMostRecent, name: 'Survey', url: 'Quality/CustomerSurvey/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.MessagesDelivered, name: 'MessagesDelivered', url: 'Settings/MessagesReceived/', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.FailedAuditsCurrentMonth, name: 'FailedAudits', url: 'Quality/Audit/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.FailedInspectionItemTodosCurrentMonth , name: 'FailedInspectionTodos', url: 'Quality/ToDo/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.ReAudits, name: 'ReAudits', url: 'Quality/ToDo/Details', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false },
            { id: enums.DashboardWidgets.Weather, name: 'Weather', url: '', class: 'col-md-3 col-lg-3 col-xs-12 col-sm-6', visible: false }
        ];
        
        countsAndTimeInfo: ICountsAndTimeInfo;

        dateRange: { startDate: string; endDate: string } = {
            startDate: this.dateFormatterSvc.formatDateShort(moment(new Date()).subtract(5, 'months').startOf('month')),
            endDate: this.dateFormatterSvc.formatDateShort(new Date())
        };
        
        reportsConfig = {
            hours: {},
            auditTrends: {},
            complaintTrends: {}
        };

        localStoreDashboardSettingsKey = "";
        dashboardInfo = [];
        reAuditsInfo = {
            ReAuditsCount: 0,
            LastModifiedDate: null,
            IsReAudit: false
        };
        failedInspectionTodos = {
            Open: 0,
            PastDue: 0,
            Closed: 0,
            ClosedOnTime: 0,
            ClosureRate: 0,
            LastOpenDate: null,
            LastClosedDate: null,
            OldestIncompleteDate: null
        };
        static $inject = [
            '$scope',
            '$log',
            '$q',
            'userInfo',
            'userContext',
            'sitesettings',
            '$uibModal',
            '$window',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.reportsvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService,
            private $q: angular.IQService,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $window: angular.IWindowService,
            private $timeout: angular.ITimeoutService,
            private apiSvc: Common.Services.IApiBaseSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private reportHelperSvc: commonSvc.IReportSvc) {
                
            this.basePath = this.sitesettings.basePath;
            
            this.localStoreDashboardSettingsKey = 'sbm-userdashboard-settings:' + this.userContext.UserId;

            //To-do - remove this in the next iteration
            this.$window.localStorage.removeItem('sbm-userdashboard-settings:' + this.userContext.UserId);

            this.isEmployee = userInfo.mainRoles.employees;
            this.userId = userInfo.userId;
            this.coreColors = sitesettings.colors.coreColors;

            this.userProfilePath = this.basePath + `User/Profile/Index?${currentEmployeeIdQueryParam}=${this.userId}`

            this.reportsConfig.hours = this.chartConfigBuilder('hours', {});
            this.reportsConfig.auditTrends = this.chartConfigBuilder('audits', {});
            this.reportsConfig.complaintTrends = this.chartConfigBuilder('complaints', {});

            var dashboardSettings = JSON.parse($window.localStorage.getItem(this.localStoreDashboardSettingsKey));
            
            var tileInfo = sitesettings.windowSizes.isNotDesktop ? this.mobileDashBoardTileInfo : this.dashBoardTileInfo;
            
            if (dashboardSettings) {
                if (tileInfo.length > dashboardSettings.length) {
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
                else if (dashboardSettings.length > tileInfo.length) {
                    dashboardSettings.forEach(((tile, index,object)=> {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index,1);
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                } else {
                    dashboardSettings.forEach(((tile, index, object) => {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index, 1);
                        }
                    }));
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
            }
            if (sitesettings.windowSizes.isNotDesktop && !dashboardSettings) {
                this.dashBoardTileInfo = this.mobileDashBoardTileInfo;
            }
            if (dashboardSettings && !_.some(dashboardSettings, (o) => _.isNull(o))) {
                this.dashBoardTileInfo = dashboardSettings;
            }

            //HACK to hide from TriParty Users
            if(this.sitesettings.isTriPartyHackUser()){
                _.remove(this.dashBoardTileInfo,(item) => {
                    return item.name == "Weather" ||
                        item.name == "Safety" ||
                        item.name == "Todo" ||
                        item.name == "Survey" ||
                        item.name == "PositionProfiles" ||
                        item.name == "Hours" ||
                        item.name == "ToDoClosure" ||
                        item.name == "MessagesDelivered";
                })

                _.remove(this.mobileDashBoardTileInfo,(item) => {
                    return item.name == "Weather",
                        item.name == "Safety" ||
                        item.name == "Todo" ||
                        item.name == "Survey" ||
                        item.name == "PositionProfiles" ||
                        item.name == "Hours" ||
                        item.name == "ToDoClosure" ||
                        item.name == "MessagesDelivered";
                })
            }
            //END HACK

        }

        public initialize = () => {
            this.timeClockViewer = this.CanViewAllEmployeesTimeClock();
            this.dashboardItems = [
                {
                    role: "managers",
                    motivationalLine: "Embrace productivity, guide your team, and serve your customer.",
                    linkSet: [
                        {
                            title: "Personnel",
                            icon: "fa-personnel",
                            href: this.sitesettings.basePath + "Personnel/BoxScore"
                        },
                        {
                            title: "Quality",
                            icon: "fa-quality",
                            href: this.sitesettings.basePath + "Quality/Audit"
                        },
                        {
                            title: "Financials",
                            icon: "fa-financials",
                            href: this.sitesettings.basePath + "Financials/BillingOverview"
                        },
                        {
                            title: "SEP",
                            icon: "fa-service-excellence",
                            href: this.sitesettings.basePath + "Training/ServiceExcellence"
                        },
                        {
                            title: "Safety",
                            icon: "fa-safety",
                            href: this.sitesettings.basePath + "Personnel/Safety"
                        },
                        {
                            title: "Knowledge Center",
                            icon: "fa-training",
                            href: this.sitesettings.basePath + "Training/KnowledgeCenter"
                        },
                    ]

                },
                {
                    role: "customers",
                    motivationalLine: "Thank you for allowing us to serve you.",
                    linkSet: [
                        {
                            title: "Personnel",
                            icon: "fa-personnel",
                            href: this.sitesettings.basePath + "Personnel/BoxScore"
                        },
                        {
                            title: "Quality",
                            icon: "fa-quality",
                            href: this.sitesettings.basePath + "Quality/Audit"
                        },
                        {
                            title: "Financials",
                            icon: "fa-financials",
                            href: this.sitesettings.basePath + "Financials/BillingOverview"
                        },
                        {
                            title: "SEP",
                            icon: "fa-service-excellence",
                            href: this.sitesettings.basePath + "Training/ServiceExcellence"
                        },
                        {
                            title: "Safety",
                            icon: "fa-safety",
                            href: this.sitesettings.basePath + "Personnel/Safety"
                        },
                        {
                            title: "Knowledge Center",
                            icon: "fa-training",
                            href: this.sitesettings.basePath + "Training/KnowledgeCenter"
                        },
                    ]

                }

            ]

            this.setCurrentDashboardItem();
            this.getReAuditsConfig();
            this.getEmployeeInfo(this.userInfo.userId);
        }

        ondragStart = (view: string) => {
            this.navigateToViewName = view;
            this.$timeout(() => {
                this.isClick = false;
            }, 200);
        }

        onDropComplete = (index, view) => {
            if (view && !this.isClick) {
                this.toggleTiles(index, view);
            }
            else {
                this.navigateToView(this.navigateToViewName)
            }
        }
        toggleTiles = ($index, view) => {
            this.isClick = true;
            var otherObj = this.dashBoardTileInfo[$index];
            var otherIndex = this.dashBoardTileInfo.indexOf(view);
            this.dashBoardTileInfo[$index] = view;
            this.dashBoardTileInfo[otherIndex] = otherObj;
            this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(this.dashBoardTileInfo));
        }
        navigateToView = (view: string) => {
            this.isClick = true;
            var navigateTo = _.find(this.dashBoardTileInfo, (o) => { return o.name == view });
            if (navigateTo) {
                if (navigateTo.id == enums.DashboardWidgets.FailedAuditsCurrentMonth) {
                    this.localDataStoreSvc.setObject('audit-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program, { filterType: AuditFiler.Fails, filterCriteria: AuditFiler.FailedAudits });
                }
                else if (navigateTo.id == enums.DashboardWidgets.ReAudits) {
                    this.localDataStoreSvc.setObject('todo-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program, { filterType: TodoFilter.Source, filterCriteria: TodoFilter.ReAudit });
                }
                else if (navigateTo.id == enums.DashboardWidgets.FailedInspectionItemTodosCurrentMonth) {
                    this.localDataStoreSvc.setObject('todo-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program, { filterType: TodoFilter.Source, filterCriteria: TodoFilter.FailedInspectionItem });
                }
                this.$window.location.href = this.basePath + navigateTo.url;
            }
        }
        /**
       * @description Returns whether the user is a timeclock viewer. 
       */
        private CanViewAllEmployeesTimeClock = (): boolean => {
            return this.userInfo.mainRoles.timeclockviewers;
        }

        private getEmployeeInfo = (employeeId) => {
            if(!this.userInfo.mainRoles.employees) return;
            
            this.apiSvc.getById(employeeId, "TrainingEmployee").then((employee: Training.Interfaces.ITrainingEmployee) => {
                if (employee) {
                    this.employeeInfo = employee;
                }
            }, this.onFailure);
        }

        public popQuickEntryModal = () => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Dashboard.QuickEntryLinksModalCtrl as vm',
                size: 'md'
            });
        }

        private setCurrentDashboardItem = () => {
            this.currentDashboardItem = _.find(this.dashboardItems, { role: 'employees' });

            if (this.userInfo.mainRoles.managers) {
                this.currentDashboardItem = _.find(this.dashboardItems, { role: 'managers' });
            } else if (this.userInfo.mainRoles.customers) {
                this.currentDashboardItem = _.find(this.dashboardItems, { role: 'customers' });
            }
        }

        private getReAuditsConfig = () => {
            this.apiSvc.query({}, 'AuditSetting', false, true).then((result) => {
                this.reAuditsInfo.IsReAudit = (result.length && _.any(result, { ReRunAuditsInternalAudit: true })) || false;
                if (!this.reAuditsInfo.IsReAudit) {
                    var obj = _.find(this.dashBoardTileInfo, { id: enums.DashboardWidgets.ReAudits });
                    obj.visible = false;
                }
                this.getDashboardItems();
            }, this.onFailure);
        }

        private getDashboardItems = () => {
            var params = {
                userId: this.userContext.UserId,
                dashboardId: this.dashboardId
            }
            this.apiSvc.query(params, "UserDashboardWidget").then((result) => {
                if (result.length > 0) {
                    angular.forEach(result, (item) => {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': item.DashboardWidgetId });
                        if (obj && obj.id == enums.DashboardWidgets.ReAudits && !this.reAuditsInfo.IsReAudit) {
                            obj.visible = false;
                        }
                        else if (obj && obj.visible != item.IsVisible) {
                            obj.visible = item.IsVisible;
                        }
                    });
                }
                else {
                    angular.forEach(this.dashBoardTileInfo, (item) => {
                        item.visible = true;
                        if (item.id == enums.DashboardWidgets.ReAudits && !this.reAuditsInfo.IsReAudit) {
                            item.visible = false;
                        }
                        var params = {
                            UserId: this.userContext.UserId,
                            DashboardWidgetId: item.id,
                            DashboardId: this.dashboardId,
                            IsVisible: item.visible
                        }
                        this.apiSvc.save(params, "UserDashboardWidget").then((result) => {
                        }, this.onFailure);
                    });
                }
                if (this.userInfo.mainRoles.managers || this.userInfo.mainRoles.customers) {
                    this.getDashBoardInfo();
                }
            }, this.onFailure);
        }

        private showConfigOptions = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Dashboard/Templates/dashboard.configure.options.tmpl.html',
                controller: 'Dashboard.ConfigureModalCtrl as vm',
                size: 'md',
                resolve: {
                    dashBoardTileInfo: () => {
                        return this.dashBoardTileInfo;
                    }
                }
            }).result.then(tileInfo => {
                if (tileInfo.isModified) {
                    angular.forEach(tileInfo.staticDashboardTileInfo, (item, key) => {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': item.id });
                        var objIndex = _.findIndex(this.dashBoardTileInfo, { 'id': item.id });
                        if (obj && obj.visible != item.checked) {
                            obj.visible = item.checked;
                        }
                        if (key != objIndex) {
                            this.dashBoardTileInfo.splice(key, 0, obj);
                            this.dashBoardTileInfo.splice(objIndex + 1, 1);
                        }
                    });
                    this.dashBoardTileInfo = _.sortBy(this.dashBoardTileInfo, (item) => {
                        return item.visible ? 0 : 1;
                    });
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(this.dashBoardTileInfo));
                    this.getDashBoardInfo();
                }
            });                
        }

        private getDashBoardInfo = () => {
            var today = new Date();
            async.series({
                setReportsConfig: (callback: any) => {
                    this.setReportsConfig().then((results) => {
                        callback(null, results);
                    });
                },

                getSurvey: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.SurveysMostRecent });
                    if (obj && obj.visible) {
                        this.getSurveyLatest();
                    }
                    callback(null);
                },
                getAuditTrends: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.AuditTrend });
                    if (obj && obj.visible) {
                        this.getAuditSixMonthTrend();
                    }
                    callback(null);
                },
                getComplaintTrends: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ComplaintsTrend });
                    if (obj && obj.visible) {
                        this.getComplaintsSixMonthTrend();
                    }
                    callback(null);
                },
                getFailedAuditsCount: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.FailedAuditsCurrentMonth });
                    if (obj && obj.visible) {
                        this.getFailedAuditsCount();
                    }
                    callback(null);
                },
                getReAuditsInfo: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ReAudits });
                    if (obj && obj.visible && this.reAuditsInfo.IsReAudit) {
                        this.getReAuditsInfo();
                    }
                    callback(null);
                }
            })
            async.auto({
                getCompliments: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ComplimentsMostRecent });
                    if (obj && obj.visible) {
                        this.getRecentCompliments().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getHours: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.TeamHoursCurrentMonth });
                    if (obj && obj.visible && (this.userInfo.mainRoles.timeclockviewers || this.userInfo.mainRoles.managers)) {
                        this.getHours().then((results) => {
                            callback(null, results);
                        });
                    }
                    else {
                        if (obj) {
                            obj.visible = false;
                        }
                    }
                },
                getToDo: (callback: any) => {
                    this.getDashBoardCounts().then((results) => {
                        callback(null, results);
                    });
                },
                getFailedInspectionTodosInfo: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.FailedInspectionItemTodosCurrentMonth });
                    if (obj && obj.visible) {
                        this.getFailedInspectionTodosInfo().then((results) => {
                            callback(null, results);
                        });
                    }
                }
            })
        }

        private getFailedInspectionTodosInfo = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add('day', 1)),
                filterByFailedInspectionItem: true
            };
            return this.apiSvc.query(params, "TodoItem/AllCounts", false, false).then((result) => {
                this.failedInspectionTodos = result;
                this.failedInspectionTodos.ClosureRate = 0;
                if ((result.Closed + result.PastDue) > 0) {
                    this.failedInspectionTodos.ClosureRate = (result.ClosedOnTime / (result.Closed + result.PastDue)) * 100;
                }
            }, this.onFailure);
        }

        private getReAuditsInfo = () => {
            return this.apiSvc.query({}, "TodoItem/ReAuditsInfo", false, false).then((result) => {
                this.reAuditsInfo.ReAuditsCount = result.ReAuditsCount;
                this.reAuditsInfo.LastModifiedDate = result.LastModifiedDate;
            }, this.onFailure);
        }

        private getFailedAuditsCount = () => {
            var param = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add('day', 1))
            };
            return this.apiSvc.query(param, 'ScoreCard/FailedAuditsCount', false, false).then((result) => {
                this.dashBoardInfo.audits.failedAuditsCount = result.FailedAuditsCount;
            }, this.onFailure);
        }

        private getSurveyLatest = (): angular.IPromise<any> => {
            return this.apiSvc.query({}, "Surveys/MostRecentDashboardWidget", false, false).then((result) => {
                this.dashBoardInfo.survey.latestScore = result.AverageScore;
                this.dashBoardInfo.survey.latestDate = result.ModifiedDate;
            }, this.onFailure);
        }

        private getRecentCompliments = (): angular.IPromise<any> => {
            this.dashBoardInfo.compliments = [];
            var params = <IODataPagingParamModel>{
                $top: 2,
                $orderby: 'CreateDate desc',
                $select: 'AccountableEmployees,Description,CreateDate,ComplimentId'
            };
            return this.apiSvc.getByOdata(params, "compliments").then((results) => {
                var employeeName = "";
                var employeeImg = "";
                angular.forEach(results, (obj) => {
                    if (obj.AccountableEmployees.length > 0) {
                        this.apiSvc.getById(obj.AccountableEmployees[0], "Employee").then((result) => {
                            employeeName = result.FirstName + " " + result.LastName;
                            employeeImg = this.imageSvc.getUserImageFromAttachments(result.UserAttachments, true);
                            this.dashBoardInfo.compliments.push({
                                employeeName: employeeName,
                                employeeImg: employeeImg,
                                createDate: obj.CreateDate,
                                description: obj.Description
                            });
                        }, this.onFailure);
                    }
                    else {
                        this.dashBoardInfo.compliments.push({
                            employeeName: employeeName,
                            employeeImg: employeeImg,
                            createDate: obj.CreateDate,
                            description: obj.Description
                        });
                    }
                });
            }, this.onFailure);
        }

        private getDashBoardCounts = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add('day', 1)),
            };
            return this.apiSvc.getByOdata(params, "Dashboard/DashboardCount", false, false).then((result) => {
                    this.countsAndTimeInfo = result;
                }, this.onFailure);
        }

        private setReportsConfig = (): angular.IPromise<any> => {
            var def = this.$q.defer();
            async.waterfall([
                (callback) => {
                    if (this.userContext.Site.ID) {
                        // Get Sites Report Settings
                        this.reportHelperSvc.getReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    } else {
                        // Get All Sites Combined Report Settings
                        this.reportHelperSvc.getAllSitesCombinedSiteReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    }
                },
                (res, callback) => {
                    if (res) {
                        this.dashBoardInfo.survey.controlLimit = res.SurveyControlLimit;
                        this.dashBoardInfo.complaints.control = res.ComplaintControlLimit;
                        this.dashBoardInfo.audits.control = res.ScoringProfileGoal;
                        this.dashBoardInfo.audits.max = res.ScoringProfileMaximumScore;
                    }
                    callback(null, res);
                }],
                (err, result) => {
                    if (err) {
                        this.onFailure(err);
                        def.reject(err);
                    }
                    def.resolve(result);
                });
            return def.promise;
        }

        private getAuditSixMonthTrend = (): angular.IPromise<any> => {
            return this.apiSvc.getByOdata({ startDate: this.dateRange.startDate, endDate: this.dateRange.endDate },
                "Report/AuditTrends", false, false).then((result) => {
                    var categories = [];
                    _.forEach(result.Categories, (category) => {
                        var cat = category.slice(0, 3);
                        categories.push(cat);
                    });
                    this.dashBoardInfo.audits.categories = categories
                    this.dashBoardInfo.audits.internalAuditData = result.InternalAuditData;
                    this.dashBoardInfo.audits.jointAuditData = result.JointAuditData;

                    var avgInternalAuditsScore =  _.reduce(this.dashBoardInfo.audits.internalAuditData, function (sum, n) {
                                                            return sum + n;
                                                            }, 0);
                    this.dashBoardInfo.audits.avgInternalAuditsScore = Math.round((avgInternalAuditsScore / result.InternalAuditData.length) * 100) / 100;

                    var avgJointAuditsScore = _.reduce(this.dashBoardInfo.audits.jointAuditData, function (sum, n) {
                                                                return sum + n;
                                                            }, 0);
                    this.dashBoardInfo.audits.avgJointAuditsScore = Math.round((avgJointAuditsScore / result.JointAuditData.length) * 100) / 100;
                    this.showAuditTrends();
                }, this.onFailure);
        }

        private getComplaintsSixMonthTrend = (): angular.IPromise<any> => {
            return this.apiSvc.getByOdata({ startDate: this.dateRange.startDate, endDate: this.dateRange.endDate },
                "Report/ComplaintTrends", false, false).then((result) => {
                    var categories = [];
                    _.forEach(result.Categories, (category) => {
                        var cat = category.slice(0, 3);
                        categories.push(cat);
                    });
                    this.dashBoardInfo.complaints.categories = categories;
                    this.dashBoardInfo.complaints.inScope = result.InScopeData;
                    this.dashBoardInfo.complaints.outOfScope = result.OutOfScopeData;
                    this.dashBoardInfo.complaints.inScopeCountAvg = Math.round((result.InScopeComplaintCount / result.InScopeData.length) * 100) / 100 ;
                    this.dashBoardInfo.complaints.outOfScopeCountAvg = Math.round((result.OutOfScopeComplaintCount / result.OutOfScopeData.length) * 100) / 100;

                    this.showComplaintTrends();
                }, this.onFailure);
        }

        private getHours = () : angular.IPromise<any>  => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment()),
                supervisorId: this.timeClockViewer ? '' : this.userId
            }
            return this.apiSvc.getByOdata(params, 'FinanceReport/HourMonthlyTrends', false, false).then((result) => {
                if (!result) return;
                this.dashBoardInfo.hours.labels = result.Labels;
                this.dashBoardInfo.hours.regularHours = result.RegularHours;
                this.dashBoardInfo.hours.STEHours = result.STEHours;
                this.dashBoardInfo.hours.totalSTEHours = result.TotalSTEHours;
                this.dashBoardInfo.hours.totalRegularHours = result.TotalRegularHours

                this.showHours();
            }, this.onFailure);
           
        }

        private showAuditTrends = () => {
            var goalLineWidth = this.dashBoardInfo.audits.control != 0 ? this.goalLineWidth : 0;
            // Build the chart
            this.reportsConfig.auditTrends = this.chartConfigBuilder('audits', {
                xAxis: {
                    tickColor: 'white',
                    categories: this.dashBoardInfo.audits.categories,
                    labels: {
                        enabled: true,
                        autoRotation: false
                    }
                },
                yAxis: {
                    minRange: this.dashBoardInfo.audits.control,
                    max: this.dashBoardInfo.audits.max,
                    min: 0,
                    labels: {
                        enabled: true
                    },
                    title: {
                        text: null
                    },
                    plotLines: [{
                        value: this.dashBoardInfo.audits.control,
                        color: this.coreColors.warning,
                        width: goalLineWidth,
                        zIndex: 4,
                        label: { text: '' }
                    }],
                },
                plotOptions: {
                    column: {
                        showInLegend: false
                    }
                },
                series: [
                    {
                        name: 'INTERNAL',
                        lineWidth: 3,
                        data: this.dashBoardInfo.audits.internalAuditData
                        
                    },
                    {
                        name: 'JOINT',
                        lineWidth: 3,
                        data: this.dashBoardInfo.audits.jointAuditData
                    },
                ],
                legend: {
                    enabled: false,
                    type: '',
                },
            });
        }
           
        private showComplaintTrends = () => {
            // Build the chart
            this.reportsConfig.complaintTrends = this.chartConfigBuilder('complaints', {
                xAxis: {
                    tickColor: 'white',
                    categories: this.dashBoardInfo.complaints.categories,
                    labels: {
                        enabled: true,
                        autoRotation: false
                    }
                },
                yAxis: {
                    minRange: this.dashBoardInfo.complaints.control,
                    min: 0,
                    allowDecimals: false,
                    labels: {
                        enabled: true
                    },
                    title: {
                        text: null
                    },
                    plotLines: [{
                        value: this.dashBoardInfo.complaints.control,
                        color: this.coreColors.warning,
                        width: this.goalLineWidth,
                        zIndex: 4,
                        label: { text: '' }
                    }],
                },
                plotOptions: {
                    column: {
                        showInLegend: false
                    }
                },
                series: [
                    {
                        name: 'IN SCOPE',
                        lineWidth: 3,
                        data: this.dashBoardInfo.complaints.inScope
                    },
                    {
                        name: 'OUT OF SCOPE',
                        lineWidth: 3,
                        data: this.dashBoardInfo.complaints.outOfScope
                    },
                ],
                legend: {
                    enabled: false,
                    type: '',
                },
            });
        }

        private showHours = () => {
            // Build the chart
            this.reportsConfig.hours = this.chartConfigBuilder('hours', {
                xAxis: {
                    tickColor: 'white',
                    categories: this.dashBoardInfo.hours.labels,
                    labels: {
                        enabled: true

                    }
                },
                yAxis: {
                    labels: {
                        enabled: false
                    }
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                categories: this.dashBoardInfo.hours.labels,
                series: [
                    {
                        name: 'REG HOURS',
                        lineWidth: 3,
                        data: this.dashBoardInfo.hours.regularHours
                    },
                    {
                        name: 'STE HOURS',
                        lineWidth: 3,
                        data: this.dashBoardInfo.hours.STEHours
                    },
                ],
                legend: {
                    enabled: false,
                    type: '',
                },
                height: 140
            });
        }

        private getComplianceStats = (): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(null, "EmployeeCourse/ComplianceStats",false,false).then((results) => {
                this.dashBoardInfo.compliance.totalEmployees = results.TotalEmployees;
                this.dashBoardInfo.compliance.totalCompliantEmployees = results.TotalCompliantEmployees;
                this.dashBoardInfo.compliance.compliancePercent = results.CourseCompliancePercentage;
            }, this.onFailure);
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private chartConfigBuilder = (name, options) => {
            var chartOptions = {

                // Column Chart
                audits: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        //min: 75,
                        //max: 100,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                value: 98,
                                color: '#6C9DC7',
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            color: '#edb326',
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                // Column Chart
                complaints: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        //min: 75,
                        //max: 100,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                value: 98,
                                color: '#6C9DC7',
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            color: '#edb326',
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                // line chart
                hours: {
                    chart: {
                        height: 140
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                    },
                    plotOptions: {
                        line: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            name: '',
                            data: []
                        }
                    ]
                },
            }
            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('Dashboard.Index', IndexCtrl);


    class QuickEntryLinksModal {

        basePath: string;
        
        quickEntryLinks = [];

        modalTitle: string = '';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc) {

            this.modalTitle = "Quick Entry Links";
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Dashboard/Templates/quick.entry.links.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            
            this.quickEntryLinks = [
                {
                    name: 'Audits',
                    link: this.basePath + 'Quality/Audit/Entry'
                },
                {
                    name: 'Complaint',
                    link: this.basePath + 'Quality/Complaint/Entry'
                },
                {
                    name: 'Compliment',
                    link: this.basePath + 'Quality/Compliment/Entry'
                },
                {
                    name: 'TO-DO',
                    link: this.basePath + 'Quality/Todo?createNew=True'
                },
                {
                    name: 'Work Orders',
                    link: this.basePath + 'Quality/WorkOrder/Entry'
                },
                {
                    name: 'Report It',
                    link: this.basePath + 'Personnel/Ownership/Entry'
                }
            ]
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

    }
    angular.module("app")
        .config(['blockUIConfig', (blockUIConfig) => {
            //disable Loading screen during all HTTP Requests
            blockUIConfig.autoBlock = false;
        }])
        .controller('Dashboard.QuickEntryLinksModalCtrl', QuickEntryLinksModal);
} 
