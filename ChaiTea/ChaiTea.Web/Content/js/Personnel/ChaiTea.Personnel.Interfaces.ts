﻿
module ChaiTea.Personnel.Interfaces {

    interface IUserAttachment extends Common.Interfaces.IAttachment {
        UserAttachmentId: number;
        UserAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    interface IEmployeeAttachment extends Common.Interfaces.IAttachment {
        EmployeeAttachmentId: number;
        EmployeeAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    enum USER_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    enum EMPLOYEE_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    export interface IEmployee {
        UserAttachments?: Array<IUserAttachment>;
        EmployeeId: number;
        HireDate?: string;
        JobDescription: string;
        Name: string;
        ProfileImage?: string;
    }

    export interface ISafetyIncidentItem {
        SafetyIncidentId: number;
        DateOfOccurrence: Date;
        Details: string;
        SafetyIncidentTypes: Array<ISafetyIncidentType>;
        OrgUserId: number;
    }

    export interface ISafetyIncidentType {
        SafetyIncidentTypeId: number;
        Name: string;
        isSelected: boolean;
    }
   
    export interface IOwnership {
        OwnershipId: number;
        BuildingId: number;
        Classifications: Array<any>;
        CustomerRepresentatives: Array<any>;
        FloorId: number;
        Description: string;
        EmployeeId: number;
        IsCritical: number;
        CreateDate: Date;
        ReportTypeId: number;
        RejectionTypeId: number;
        Status?: boolean;
        OwnershipAttachments: Array<IOwnershipAttachment>;
        IsCorrectiveActionTaken?: boolean;
        CorrectiveActionDescription: string;
        IsExempt: boolean;
    }
    export interface IOwnershipType {
        ClassificationId: number;
        Name: string;
    }
    export interface IConduct {
        ConductId: number;
        DateOfOccurrence: Date;
        Reason: string;
        ConductTypes: Array<IConductType>;
        OrgUserId: number;
        ConductAttachments: Array<IConductAttachment>;
        IsExempt: boolean;
    }

    export interface IConductType {
        ConductTypeId: number;
        Name: string;
        IsSelected: boolean;
    }

    //export interface IConductAttachmentType {
    //    ConductAttachmentTypeId: number;
    //    Name: string;
    //}
    export interface IAttendanceItem {
        IssueId: number;
        DateOfOccurrence: Date;
        Comment: string;
        IssueTypeId: number;
        IssueType: string;
        ReasonId: number;
        ReasonName: string;
        OrgUserId: number;
        IsExempt: boolean;
    }

    export interface IIssueType {
        IssueTypeId: number;
        Name: string;
        Reasons: IReason[]; 
    }

    export interface IConductAttachmentType {
        ConductAttachmentTypeId: number;
        Name: string;
    }

    export interface IConductAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        ConductAttachmentTypeId?: number;
        ConductAttachmentType: string;
        UniqueId: string;
    }
    
    export interface IOwnershipAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        OwnershipAttachmentTypeId?: number;
        OwnershipAttachmentType: string;
        UniqueId: string;
    }
    
    export interface IIssueTypeReason {
        IssueTypeReasonId: number;
        IssueTypeId: number;
        IssueType: IIssueType;
        ReasonId : number
        Reason: IReason;
    }

    export interface IReason {
        ReasonId: number;
        Name: string;
    }

    export interface IPersonnelEmployeesModalCtrl {
        cancel(): void;
        run(empId: number): void;
    }

    export interface ISafetyItem {
        SafetyId: number;
        EmployeeId: number;
        DateOfIncident: any;
        Detail: string;
        IsExempt: boolean;
        IncidentTypes: Array<number>;
        SafetyAttachments: Array<ISafetyAttachment>;
    }

    export interface ISafetyAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        UniqueId: string;
    }

    export interface IEmployeeAudit {
        EmployeeAuditId:number;
        EmployeeId:number;
        IsPass: boolean;
        IsEventBasedAudit: boolean;
        Comment:string;
        CheckBoxOptions: Array<any>; 
        CheckBoxSelections: Array<any>;
    }

    //my hours related
    export interface ITimeClock {
        TimeClockId: number
        EmployeeId: number
        Day: Date
        StartTime: Date
        EndTime: Date
        RegularTime: number
        OverTime: number
        DoubleTime: number
        ExtraTime: number
        TotalTime: number
        TotalPay: number
        WeekNo: number
        MonthNo: number
        EmployeeName: string
        EmployeeJobDesc: string
        EmployeeImage: string
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }
    export interface IWeek {
        weekNo: number;
        totalHours: number;
    }
    export interface IDayAggregate {
        day: Date;
        aggregateHours: number;
    }
    export interface ITimeClockEmployee {
        Day: Date;
        TotalHours: number;
        EmployeeId: number;
    }
    export interface ICalendarEvent {
        title: string; // The title of the event
        type: string; // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
        startsAt: Date; // A javascript date object for when the event starts
        ////endsAt: new Date(2015, 11, 5), // Optional - a javascript date object for when the event ends
        //editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
        //deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
        ////draggable: true, //Allow an event to be dragged and dropped
        //resizable: true, //Allow an event to be resizable
        incrementsBadgeTotal: boolean; //If set to false then will not count towards the badge total amount on the month and year view

        //#sreekanth added to customize for our needs with out changing actual directive functionality(modified tooltip template in actual angular-bootstrap-calendar-tpls.js file). do not set any value or ignore for default functionality.
        totalHours: number;
        aggregateHours: number;
        startTime: Date;
        endTime: Date;
        drillDown: boolean;
           
        //recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
        //cssClass: '' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
    }
} 