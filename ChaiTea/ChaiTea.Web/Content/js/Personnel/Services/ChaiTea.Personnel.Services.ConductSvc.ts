﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for Conduct.
 */
module ChaiTea.Personnel.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Conduct resource.
     */

    export interface IConductResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IConductSvc {

        saveConduct(params?: Object): angular.IPromise<any>;
        updateConduct(id: number, params?: Object): angular.IPromise<any>;
        getConducts(params?: IODataPagingParamModel);
        getConduct(id: number): angular.IPromise<any>;
    }

    class ConductSvc implements angular.IServiceProvider {
        private basePath: string;
        private ConductResource: IConductResource;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
            this.ConductResource = this.resource();
        }

        public $get(): IConductSvc {
            return {
                saveConduct: (params) => { return this.saveConduct(params); },
                updateConduct: (id, params) => { return this.updateConduct(id, params); },
                getConduct: (id) => { return this.getConduct(id) },
                getConducts: (params) => { return this.getConducts(params); }
            }
        }

        /**
        * @description returns all Conducts.
        * @param {Object} params
        */
        getConducts = (params?: IODataPagingParamModel) => {
            return this.ConductResource.getByOData(params || {}).$promise;
        }

        /**
        * @description Creates/saves an Conduct.
        * @param {Object} params
        */
        getConduct = (id: number): angular.IPromise<any> => {
            return this.ConductResource.get({ id: id }).$promise;
        }

        /**
         * @description Creates/saves an Conduct .
         * @param {Object} params
         */
        saveConduct = (params?: Object): angular.IPromise<any> => {
            return this.ConductResource.save(params || {}).$promise;
        }

        /**
         * @description Creates/saves an Conduct .
         * @param {Object} params
         */
        updateConduct = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.ConductResource.update(params).$promise;
        }

        /**
         * @description Resource object for audit profile.
         */
        private resource = (): IConductResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IConductResource>this.$resource(
                this.basePath + 'api/Services/Conduct/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/Conduct',
                        params: { skip: 0 }
                    }
                });
        }

    }

    angular.module('app').service('chaitea.personnel.services.conductsvc', ConductSvc);
}


