﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for attendance.
 */
module ChaiTea.Personnel.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to attendance resource.
     */

    export interface IAttendanceResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    export interface IAttendanceSvc {
        
        saveAttendance(params?: Object): angular.IPromise<any>;
        updateAttendance(id: number, params?: Object): angular.IPromise<any>;
        getAttendanceIssues(params?: IODataPagingParamModel);
        getAttendanceIssue(id: number): angular.IPromise<any>;
    }

    class AttendanceSvc implements angular.IServiceProvider {
        private basePath: string;
        private attendanceResource: IAttendanceResource;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
            this.attendanceResource = this.resource();
        }

        public $get(): IAttendanceSvc {
            return {
                saveAttendance: (params) => { return this.saveAttendance(params); },
                updateAttendance: (id, params) => { return this.updateAttendance(id, params); },
                getAttendanceIssue: (id) => { return this.getAttendanceIssue(id)},
                getAttendanceIssues: (params) => { return this.getAttendanceIssues(params);}
            }
        }

        /**
        * @description returns all attendance issues.
        * @param {Object} params
        */
        getAttendanceIssues = (params?: IODataPagingParamModel) => {
            return this.attendanceResource.getByOData(params || {}).$promise;
        }

        /**
        * @description Creates/saves an attendance issue.
        * @param {Object} params
        */
        getAttendanceIssue = (id: number): angular.IPromise<any> => {
            return this.attendanceResource.get({ id: id }).$promise;
        }

        /**
         * @description Creates/saves an attendance issue.
         * @param {Object} params
         */
        saveAttendance = (params?: Object): angular.IPromise<any> => {
            return this.attendanceResource.save(params || {}).$promise;
        }

        /**
         * @description Creates/saves an attendance issue.
         * @param {Object} params
         */
        updateAttendance = (id: number,params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.attendanceResource.update(params).$promise;
        }

        /**
         * @description Resource object for audit profile.
         */
        private resource = (): IAttendanceResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAttendanceResource>this.$resource(
                this.basePath + 'api/Services/Issue/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/Issue',
                        params: { skip: 0 }
                    }
                });
        }

    }

    angular.module('app').service('chaitea.personnel.services.attendancesvc', AttendanceSvc);
}


