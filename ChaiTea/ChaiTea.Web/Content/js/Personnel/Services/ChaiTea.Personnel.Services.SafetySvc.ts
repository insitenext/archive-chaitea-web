﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for SafetyIncident.
 */
module ChaiTea.Personnel.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to SafetyIncident resource.
     */

    export interface ISafetyResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface ISafetySvc {

        saveSafety(params?: Object): angular.IPromise<any>;
        updateSafety(id: number, params?: Object): angular.IPromise<any>;
        getSafety(params?: IODataPagingParamModel);
        getSafetyById(id: number): angular.IPromise<any>;
        deleteSafetyById(id: number): angular.IPromise<any>;
    }

    class SafetySvc implements angular.IServiceProvider {
        private basePath: string;
        private SafetyResource: ISafetyResource;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
            this.SafetyResource = this.resource();
        }

        public $get(): ISafetySvc {
            return {
                saveSafety: (params) => { return this.saveSafety(params); },
                updateSafety: (id, params) => { return this.updateSafety(id, params); },
                getSafetyById: (id) => { return this.getSafetyById(id) },
                getSafety: (params) => { return this.getSafety(params); },
                deleteSafetyById: (id) => { return this.deleteSafetyById(id) }
            }
        }

        /**
        * @description returns all SafetyIncidents.
        * @param {Object} params
        */
        getSafety = (params?: IODataPagingParamModel) => {
            return this.SafetyResource.getByOData(params || {}).$promise;
        }

        /**
        * @description Creates/saves an SafetyIncident.
        * @param {Object} params
        */
        getSafetyById = (id: number): angular.IPromise<any> => {
            return this.SafetyResource.get({ id: id }).$promise;
        }

        /**
        * @description Deletes a SafetyIncident.
        * @param {Object} params
        */
        deleteSafetyById = (id: number): angular.IPromise<any> => {
            return this.SafetyResource.delete({ id: id }).$promise;
        }

        /**
         * @description Creates/saves an SafetyIncident.
         * @param {Object} params
         */
        saveSafety = (params?: Object): angular.IPromise<any> => {
            return this.SafetyResource.save(params || {}).$promise;
        }

        /**
         * @description Creates/saves an SafetyIncident.
         * @param {Object} params
         */
        updateSafety = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.SafetyResource.update(params).$promise;
        }

        /**
         * @description Resource object for audit profile.
         */
        private resource = (): ISafetyResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ISafetyResource>this.$resource(
                this.basePath + 'api/Services/Personnel/Safety/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/Personnel/Safety',
                        params: { skip: 0 }
                    }
                });
        }

    }

    angular.module('app').service('chaitea.personnel.services.safetysvc', SafetySvc);
}
