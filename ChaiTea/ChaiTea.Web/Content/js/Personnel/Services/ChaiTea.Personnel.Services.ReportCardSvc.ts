﻿module ChaiTea.Personnel.Services {

    'use strict';

    import commonInterfaces = Common.Interfaces;

    export interface IReportCardSvc {
        getEmployeeKpi(year: number, month: number, empId: number): ng.IPromise<any>;
        getKpiComment(params?: Object): ng.IPromise<any>;
        getBonusOfEmployee(params?: Object): ng.IPromise<any>;
        getYearlyBonusOfEmployee(params?: Object): ng.IPromise<any>;
        getSiteBonus(params?: Object): ng.IPromise<any>;
        getMonthlyBonusOfEmployee(params?: Object): ng.IPromise<any>;
    }

    interface IReportCardResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
    }


    class ReportCardSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IReportCardSvc {
            return {
                getEmployeeKpi: (year, month, empId) => (this.getEmployeeKpi(year, month, empId)),
                getKpiComment: () => (this.getKpiComment()),
                getBonusOfEmployee: () => (this.getBonusOfEmployee()),
                getYearlyBonusOfEmployee: () => (this.getYearlyBonusOfEmployee()),
                getSiteBonus: () => (this.getSiteBonus()),
                getMonthlyBonusOfEmployee: () => (this.getMonthlyBonusOfEmployee())
            };
        }

        getEmployeeKpi = (year: number, month: number, empId: number) => {
            return this.ReportCardresource().get({ year: year, month: month, id: empId }).$promise;
        }

        getKpiComment = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/KpiComment/:year/:month').query(params).$promise;
        }

        getBonusOfEmployee = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/EmployeeBonus/:employeeId/:year/:month').get(params).$promise;
        }
        getYearlyBonusOfEmployee = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/YearlyBonus/:employeeId/:year/:month').get(params).$promise;
        }
        getSiteBonus = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/SiteBonus/:year/:month/').query(params).$promise;
        }

        getMonthlyBonusOfEmployee = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/EmployeeBonus/Employee/:year/:month').get(params).$promise;
        }
        /**
         * @description Resource object for training.
         */
        private ReportCardresource = (): IReportCardResource => {
            

            var url = this.basePath + 'api/Services/ServiceExcellenceForEmployee/';

            return <IReportCardResource>this.$resource(
                url + ':year/:month',
                { year: '@year', month: '@month' },
                {
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }
    }

    angular.module('app').service('chaitea.personnel.services.reportcard', ReportCardSvc);
} 