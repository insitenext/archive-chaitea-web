﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for SafetyIncident.
 */
module ChaiTea.Personnel.Services {
    'use strict';

    export interface ICommonSvc {
        getPersonnelAuditOptions(): angular.IPromise<any>;
    }

    class CommonSvc implements angular.IServiceProvider {
        private basePath: string;

        kpiOptions = {
            appearence: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        static $inject = [
            '$log',
            '$q',
            'sitesettings',
            'chaitea.services.base',
        ];
        constructor(
            private $log: ng.ILogService,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Services.IBaseSvc) {

        }
        
        public $get(): ICommonSvc {
            return {
                getPersonnelAuditOptions: () => { return this.getPersonnelAuditOptions(); },
            }
        }

        private getPersonnelAuditOptions = (): angular.IPromise<any> => {

            var def = this.$q.defer();

            this.apiSvc.get("LookupList/EmployeeAuditOptions").then((res) => {

                this.kpiOptions.appearence = _.where(res, { KpiOption: { Name: 'Appearance' } });
                this.kpiOptions.appearence["Description"] = "Employee wears an SBM tidy uniform and has good hygiene.";
                this.kpiOptions.appearence["Name"] = "Appearence";

                this.kpiOptions.attitude = _.where(res, { KpiOption: { Name: 'Attitude' } });
                this.kpiOptions.attitude["Description"] = "Employee displays a positive approachable demeanor, shows respect and is attentive.";
                this.kpiOptions.attitude["Name"] = "Attitude";

                this.kpiOptions.responsiveness = _.where(res, { KpiOption: { Name: 'Responsiveness' } });
                this.kpiOptions.responsiveness["Description"] = "Employee displays a quick response time and shows initiative.";
                this.kpiOptions.responsiveness["Name"] = "Responsiveness";

                this.kpiOptions.equipment = _.where(res, { KpiOption: { Name: 'Closet & Equipment' } });
                this.kpiOptions.equipment["Description"] = "Employee maintains proper care of equipment and a tidy closet without any hazardous conditions.";
                this.kpiOptions.equipment["Name"] = "Equipment & Closets";

                def.resolve(this.kpiOptions);

            }, (err) => {
                def.reject(err);
            });
                

            return def.promise;

        }

    }

    angular.module('app').service('chaitea.personnel.services.commonsvc', CommonSvc);
}


