﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Personnel.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IPersonnelReportsSvc {
        getOwnershipMonthlyTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getOwnershipsBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getOwnershipsByProgramOrType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getOwnershipTrendsByProgramOrType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getOwnershipsByEmployee(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getOwnershipsCount(params?: any): angular.IPromise<any>;
    }

    class PersonnelReportsSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {obj} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings) {

            this.basePath = sitesettings.basePath;
        }

        public $get(): IPersonnelReportsSvc {
            return {
                getOwnershipsCount: (params) => {
                    return this.getOwnershipsCount(params);
                },
                getOwnershipMonthlyTrends: (params) => {
                    return this.getOwnershipMonthlyTrends(params);
                },
                getOwnershipsBySite: (params) => {
                    return this.getOwnershipsBySite(params);
                },
                getOwnershipsByProgramOrType: (params) => {
                    return this.getOwnershipsByProgramOrType(params);
                },
                getOwnershipTrendsByProgramOrType: (params) => {
                    return this.getOwnershipTrendsByProgramOrType(params);
                },
                getOwnershipsByEmployee: (params) => {
                    return this.getOwnershipsByEmployee(params);
                }
            }
        }


        /**
         * @description Work Order Reports.
         */
        getOwnershipsCount = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/Count/:startDate/:endDate/:isEmployee').get(params).$promise;
        }

        getOwnershipMonthlyTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/MonthlyTrends/:startDate/:endDate').get(params).$promise;
        }

        getOwnershipsBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/TrendsBySite/:startDate/:endDate').get(params).$promise;
        }

        getOwnershipsByProgramOrType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/ByReportType/:startDate/:endDate').query(params).$promise;
        }

        getOwnershipTrendsByProgramOrType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/TrendsByReportType/:startDate/:endDate/:reportTypeId').get(params).$promise;
        }

        getOwnershipsByEmployee = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/TrainingReport/Ownership/ByEmployee/:startDate/:endDate').query(params).$promise;
        }

    }

    angular.module('app').service('chaitea.personnel.services.reportssvc', PersonnelReportsSvc);
}

 