﻿namespace ChaiTea.Personnel.MyProfile.Controllers {

    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import commonEnums = ChaiTea.Common.Enums;

    export interface IReportCardServiceExcellenceEmployee extends Training.Interfaces.ISETrainingEmployee {
        _monthName: string;
        _isLoading: boolean;
        _boxScores: any;
        _displayedBonus: number;
        IsClosed: boolean;
    }
    
    class ReportCardController implements ChaiTea.Common.Interfaces.INgController {
        currentMonth: Date;
        currentMonthKpi: IReportCardServiceExcellenceEmployee;
        siteBonus: Training.Interfaces.ISiteBonus;
        totalBonus: number = 0;
        starRating: number = 0;

        currentEmployeeId: number;
        columnItems: any[] = [
            {
                Name: "ATTENDANCE",
                Weight: 12.5,
                Goal: '2',
                icon: 'attendance'
            },
            {
                Name: "CONDUCT",
                Weight: 12.5,
                Goal: '0',
                icon: 'conduct'
            },
            {
                Name: "COMPLAINTS",
                Weight: 12.5,
                Goal: '0',
                icon: 'complaints'
            },
            {
                Name: "SAFETY",
                Weight: 12.5,
                Goal: '0',
                icon: 'safety'
            },
            {
                Name: "REPORT ITS",
                Weight: 12.5,
                Goal: '2',
                icon: 'proactive'
            },
            {
                Name: "PROFESSIONALISM",
                Weight: 12.5,
                Goal: 'PASS/FAIL',
                icon: 'professionalism'
            },
            {
                Name: "AUDITS",
                Weight: 12.5,
                Goal: '4',
                icon: 'audits'
            },
            {
                Name: "TRAINING",
                Weight: 12.5,
                Goal: '90%',
                icon: 'training'
            }
        ];

        kpisByMonth: IReportCardServiceExcellenceEmployee[] = [];
        skip: number = 0;
        lastItemDisplayed: boolean = false;
        isBusy: boolean;
        currentIsBusy: boolean;

        filteredKpisByMonth = [];
        isPageDisabled: boolean = false;
        modalInstance;
        employeeKpi: any;
        kpiIds = commonEnums.KPITYPE;
        kpiOrder = [
            { Id: commonEnums.KPITYPE.Attendance },
            { Id: commonEnums.KPITYPE.Conduct },
            { Id: commonEnums.KPITYPE.Complaints },
            { Id: commonEnums.KPITYPE.Safety },
            { Id: commonEnums.KPITYPE.Ownership },
            { Id: commonEnums.KPITYPE.Professionalism },
            { Id: commonEnums.KPITYPE.Quality },
            { Id: commonEnums.KPITYPE.Training }
            ]
        static $inject = [
            '$scope',
            'sitesettings',
            '$log',
            'blockUI',
            Common.Services.UtilitySvc.id,
            ChaiTea.Services.BaseSvc.id,
            'userInfo',
            'userContext',
            Common.Services.TrainingSvc.id,
            Core.Services.UserContextSvc.id,
            '$uibModal',
            Common.Services.ImageSvc.id,
            'chaitea.core.services.dateformattersvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private blockUI,
            private utilitySvc: ChaiTea.Common.Services.IUtilitySvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private trainingSvc: Common.Services.ITrainingSvc,
            private userContextSvc: Core.Services.IUserContextSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateFormatterSvc: Core.Services.IDateFormatterSvc
        ) {
            this.currentMonth = moment().toDate();
        }

        public initialize = () => {
            this.blockUI.start();
            var paramId = this.utilitySvc.getIdParamFromUrl(null, true);
            if (!paramId) {
                paramId = this.utilitySvc.getQueryParamFromUrl(Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString());
            }
            this.currentEmployeeId = (paramId || this.userInfo.userId);
            this.isBusy = this.currentIsBusy = true;

            this.getSiteBonus().then(() => {
                this.getTotalEmployeeBonus();
            });

            this.getEmployeesKpis(this.currentEmployeeId);
        }

        private showPopup = (kpiName: string, employeeInfo: IReportCardServiceExcellenceEmployee) => {
            kpiName = (kpiName == 'Ownership') ? 'report it' : kpiName;
            kpiName = (kpiName == 'Quality Audits') ? 'quality' : kpiName;
            var dateRange = {
                startDate: this.dateFormatterSvc.formatDateFull(moment().date(1).hours(0).minutes(0).seconds(0)),
                endDate: this.dateFormatterSvc.formatDateFull(moment())
            }
            employeeInfo.ProfileImage = this.imageSvc.getUserImageFromAttachments(employeeInfo.UserAttachments, true);
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/ServiceExcellence/sep.boxScoreModal.html',
                controller: 'Common.SEPBoxScoreModalCtrl as vm',
                size: 'md',
                resolve: {
                    employee: () => { return employeeInfo; },
                    module: () => { return kpiName.toLocaleLowerCase(); },
                    dateRange: () => { return dateRange; },
                    isReportCard: () => { return true; },
                    year: () => { return employeeInfo.Year; },
                    month: () => { return employeeInfo.Month; },
                    isScorecardFinalized: () => { return false; }
                }
            })
        }

        public loadNextSix = () => {
            if (this.lastItemDisplayed === true) return;
            this.skip = this.skip + 6;
            this.getEmployeesKpis();
        }

        private getEmployeesKpis = (employeeId?: number) => {
            if (!employeeId) {
                if (!this.currentEmployeeId) {
                    if (this.blockUI) this.blockUI.stop();
                    return this.$log.log("No employee");
                }
                employeeId = this.currentEmployeeId;
            }

            var self = this;
            this.isBusy = this.currentIsBusy = true;

            var params = {
                $filter: `EmployeeId eq ${this.currentEmployeeId}`,
                $orderby: 'Year desc, Month desc',
                $top: 6,
                $skip: this.skip
            }
            var asyncQueue = async.queue((task: any, callback) => {
                callback();
            }, 2);

            async.auto({
                getEmployeesMasterKpis: (callback: any) => {
                    this.apiSvc.getByOdata(params, 'EmployeeKpiMaster').then((results) => {
                        if (results.length < 6 || !results.length) {
                            this.lastItemDisplayed = true;
                        }
                        callback(null, results);
                    }, this.handleFailure);
                },
                getProcessMonths: ['getEmployeesMasterKpis', (callback, results) => {

                    angular.forEach(results.getEmployeesMasterKpis, (item: IReportCardServiceExcellenceEmployee) => {
                        item._isLoading = true;

                        asyncQueue.push({ name: 'getYearMonthKpi' }, (err) => {
                            self.getEmployeesYearMonthKpi(item);
                        });

                    });

                    callback(null, results);
                }]
            }, (err, results) => {
                if (err) this.$log.error(err);

                if (this.blockUI) this.blockUI.stop();

                if (!results.getEmployeesMasterKpis.length) {
                    this.lastItemDisplayed = true;
                    this.isBusy = false;
                }
            });

            asyncQueue.drain = () => {
                this.isBusy = false;

            }

        }

        private getEmployeesYearMonthKpi = (employeeMasterKpi) => {
            return this.apiSvc.getResource(`ServiceExcellence/${employeeMasterKpi.Year}/${employeeMasterKpi.Month}/${this.currentEmployeeId}`).get()
                .$promise.then((result: IReportCardServiceExcellenceEmployee) => {
                    var date = moment(result.Month + '-' + result.Year, "MM-yyyy");
                    let isCurrentMonth = false;
                    if ((moment(this.currentMonth).month()+1 === result.Month) && (moment(this.currentMonth).year() === result.Year)) {
                        isCurrentMonth = true;
                    }

                    result._monthName = date.format('MMM').toUpperCase();

                    //sort Kpis to match order of KPIOrder enum
                    var newKpiArray: any[] = angular.copy(result.EmployeeKpis);
                    angular.forEach(result.EmployeeKpis, (item, key) => {
                        if (item.PassingMetric % 1 != 0) {
                            item.PassingMetric = (item.PassingMetric * 100);
                        }

                        // index from ENUM that page requires
                        var index = _.findIndex(this.kpiOrder, { 'Id': item.Kpi.KpiId });
                        newKpiArray.splice(index, 1, item);
                    });

                    result.EmployeeKpis = newKpiArray;
                    result._isLoading = false;
                    result._displayedBonus = 0;
                    if (result.IsClosed && result.IsPass) {
                        result._displayedBonus = result.BonusAmount + this.siteBonus.BonusAmount;
                    }
                    if (isCurrentMonth) {
                        this.currentMonthKpi = result;
                    } else {
                        this.kpisByMonth.push(result);
                    }
                }, this.handleFailure);
        }

        private getSiteBonus = (): angular.IPromise<any> => {
            var params = {
                year: this.currentMonth.getFullYear(),
                month: this.currentMonth.getMonth()+1
            }
            return this.apiSvc.query(params, 'SiteBonus/:year/:month').then((res) => {
                if (!res) {
                    this.handleFailure("No SiteBonus returned");
                    return;
                }
                this.siteBonus = res[0] ? res[0] : 0 ;
            }, this.handleFailure);
        }

        public getTotalEmployeeBonus = () => {
            var params = {
                $filter: `EmployeeId eq ${this.currentEmployeeId}`,
                $select: 'BonusAmount,IsPass,IsClosed'
            }
            this.apiSvc.getByOdata(params, 'EmployeeKpiMaster').then((res) => {
                if (!res.length) {
                    return;
                }
                var isPassedBonuses = angular.copy(_.filter(res, (r: any) => { return (r.IsClosed && r.IsPass); }));
                this.totalBonus = (this.siteBonus.BonusAmount * isPassedBonuses.length) + _.sum(_.pluck(isPassedBonuses, 'BonusAmount'));
                this.starRating = (((res.length) / isPassedBonuses.length) / 5) * 100;
            }, this.handleFailure);
        }

        private handleFailure = (errorMessage: string) => {
            this.$log.error(errorMessage);
        }
    }

    angular.module('app')
        .config(['blockUIConfig', (blockUIConfig) => {
            //disable Loading screen during all HTTP Requests
            blockUIConfig.autoBlock = false;
        }])
        .controller('Personnel.MyProfileReportCardCtrl', ReportCardController);
}