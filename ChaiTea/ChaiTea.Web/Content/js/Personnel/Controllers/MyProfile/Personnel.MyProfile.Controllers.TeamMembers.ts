﻿namespace ChaiTea.Personnel.MyProfile.Controllers {

    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonSvc = ChaiTea.Common.Services;

    export interface ICorporateMember {
        Name: string;
        Title: string;
        Summary: string;
        Photo: string;
    }

    class TeamMembersController implements Common.Interfaces.INgController {
        awsBucketUrl: string;
        corporateMembers = [];
        employees = [];

        isMobile = false;

        static $inject = [
            '$scope',
            'sitesettings',
            '$log',
            '$http',
            '$uibModal',
            Common.Services.ApiBaseSvc.id,
            Common.Services.ImageSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $http: angular.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: Common.Services.IApiBaseSvc,
            private imageSvc: Common.Services.IImageSvc
        ) {
        }

        public initialize = () => {
            if (this.sitesettings.windowSizes.isNotDesktop) {
                this.isMobile = true;
            }

            this.apiSvc.get('TrainingEmployee').then((res) => {
                angular.forEach(res, (emp) => {
                    emp._profilePicture = this.imageSvc.getUserImageFromAttachments(emp.UserAttachments, true);
                });
                this.employees = res;
            },this.handleFailure);

            this.$http.get(this.sitesettings.basePath + 'Content/data/corporate-profiles.json').then((res) => {
                if (!res.data) return;
                var members = <Array<ICorporateMember>>res.data;
                angular.forEach(members, (val) => {
                    val.Photo = this.imageSvc.getByKey('public/image/sbm-corporate-profiles/names/' + val.Photo);
                });
                this.corporateMembers = members;
            },this.handleFailure);
        }

        public viewSummaryInModal = (person: ICorporateMember) => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + "Content/js/Personnel/Templates/personnel.team-members.person.modal.html",
                size: 'lg',
                controller: PersonModalController,
                controllerAs: '$ctrl',
                resolve: {
                    person: () => { return person; }
                }
            })
        }

        

        public back = ($event: angular.IAngularEvent) => {
            $event.preventDefault();
            window.history.back();
        }

        private handleFailure = (errorMessage: string) => {
            this.$log.error(errorMessage);
        }
    }

    angular.module('app')
        .controller('Personnel.MyProfileTeamMembersCtrl', TeamMembersController);


    class PersonModalController {
        static $inject = ['sitesettings','person'];
        constructor(private sitesettings: ISiteSettings, private person) {
        }
    }


}