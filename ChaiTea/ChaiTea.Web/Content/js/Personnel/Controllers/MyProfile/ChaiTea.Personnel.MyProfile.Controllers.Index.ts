﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.MyProfile.Controllers {
    'use strict';

    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    interface IImageSize {
        width: number;
        height: number;
    }

    enum FileType { Undefined, ProfilePicture };

    class MyProfileCtrl {
        
        basePath: string;    
        userId: number;
        employeeId: number;

        templateUrl: string;
        employee = {
            Id: 0,
            Name: '',
            JobDescription: '',
            HireDate: null,
            ProfilePicture: '',
            FunFacts: ''
        };

        funfact: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc
        ) {

            this.employeeId = userInfo.userId;
            this.userId = userContext.UserId;
            this.basePath = sitesettings.basePath;
            this.templateUrl = this.basePath + 'Content/js/Common/Templates/MyProfile/personnel.myProfile.myHours.tmpl.html';
        }

        initialize = (): void=> {

        }

        
    }

    class MyFinancialsCtrl {
        basePath: string;
        modalInstance;
        employeeId: number;

        monthlyBonus: number = 0;
        yearlyBonus: number = 0;
        siteBonusForMonth: number = 0;

        dateRange: { date: Date; month: number; year: number; startDate: Date; endDate: Date; } = {
            date: moment().toDate(),
            month: moment().month(),
            year: moment().year(),
            startDate: moment(new Date()).startOf('month').toDate(),
            endDate: moment(new Date()).endOf('month').toDate()
        };

        view = 'month';
        currentDay = new Date();
        minDate = moment();
        events = [];

        employeesWithAttachment = [];
        employees = [];
        employee = {};

        weeks = [];
        dayAggregates = []

        showStraightTimeEquivalent: boolean = false;
        showSTEBtnText = 'SHOW STE HOURS';

        totalWeeksInMonth: number;
        active: number;

        timeClock: personnelInterfaces.ITimeClock;
        timeClocks = [];
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.localdatastoresvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc) {

            this.basePath = sitesettings.basePath;
            this.employeeId = userInfo.userId;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope, (event, obj) => {
                var endDate = moment(obj.endDate);
                this.dateRange.endDate = endDate.toDate();
                this.dateRange.month = endDate.month();
                this.dateRange.year = endDate.year();

                this.currentDay = this.dateRange.endDate;
                this.totalWeeksInMonth = 0;
                this.getTimeClockData();
                this.getEmployeeBonus();
            });
        }

        initialize = (): void => {
            this.active = 0;

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate);
                dateSelection = moment(dateSelection.endDate);
                this.dateRange.month = dateSelection.month();
                this.dateRange.year = dateSelection.year();
                this.currentDay = dateSelection.toDate();
            }

            this.getTimeClockData();
            this.getEmployeeBonus();

            mixpanel.track("Viewed My Profile");
        }

        /**
        * @description Get yearly and monthly bonus for an emp,loyee.
        */
        private getEmployeeBonus = () => {
            this.siteBonusForMonth = 0;
            this.monthlyBonus = 0;
            this.apiSvc.query({ month: this.dateRange.month + 1, year: this.dateRange.year }, "SiteBonus/:year/:month/").then((data): any => {
                if (data.length) {
                    this.siteBonusForMonth = data[0].BonusAmount ? data[0].BonusAmount : 0;
                }
            });

            var params = {
                employeeId: this.employeeId,
                year: this.dateRange.year,
                month: this.dateRange.month + 1
            }

            this.apiSvc.getByOdata(params, "EmployeeBonus/:employeeId/:year/:month", false, false).then((result) => {
                if (result)
                    this.monthlyBonus = result.BonusAmount ? result.BonusAmount : 0;
            });

            var param = {
                employeeId: this.employeeId,
                year: this.dateRange.year,
                month: this.dateRange.month + 1
            }
            this.apiSvc.getByOdata(param, "FinanceReport/YearlyBonus/:employeeId/:year/:month", false, false).then((result) => {
                this.yearlyBonus = result.YearlyBonus;
            });
        }

        setCalendarEvents = () => {
            this.events = [];
            angular.forEach(this.timeClocks, (timeclock) => {
                this.events.push(<personnelInterfaces.ICalendarEvent>{
                    title: 'Working Hours',
                    type: 'success',
                    startsAt: moment(timeclock.Day).toDate(),
                    incrementsBadgeTotal: false,
                    totalHours: this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime,
                    aggregateHours: this.getAggregateDayHours(timeclock.Day),
                    startTime: timeclock.StartTime,
                    endTime: timeclock.EndTime,
                    drillDown: false
                });
            });
        }

        getTimeClockData = (): void => {
            var params = this.getPagingParams();

            this.apiSvc.getByOdata(params, 'TimeClock/Employee', false, true).then((data): any => {
                this.timeClocks = [];
                this.events = [];
                this.calculateAggregateDayHours(data);

                angular.forEach(data, (timeclock) => {
                    this.timeClock = timeclock;
                    this.timeClock.WeekNo = this.getMonthWeekOfDate(timeclock.Day);

                    this.timeClocks.push(this.timeClock);
                });
                
                this.setCalendarEvents();
                this.getWeeklyHours();
            }).finally(() => {
                mixpanel.track("Viewed My Hours");
            });
        }
        // gets the week of the month a given date falls in to 
        //and at the end we come to know total no of weeks that month has when we get week number for last date of month
        getMonthWeekOfDate = (timeClockDate: string) => {
            var date = moment(timeClockDate).toDate();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
            this.totalWeeksInMonth = Math.ceil((date.getDate() + firstDay) / 7);
            return this.totalWeeksInMonth;
        }

        //to calculate weekly total hours 
        getWeeklyHours = () => {
            this.weeks = [];
            for (var i = 1; i <= this.totalWeeksInMonth; i++) {
                var totalHrs = 0;
                angular.forEach(this.timeClocks, (timeclock) => {
                    if (i == timeclock.WeekNo) {
                        totalHrs = totalHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                    }
                });

                this.weeks.push(<personnelInterfaces.IWeek>{
                    totalHours: totalHrs,
                    weekNo: i
                });
            }
        }

        // to calculate aggregate total hours of each day
        calculateAggregateDayHours = (data: any) => {
            if (!data[0])
                return;

            var previousDate = data[0].Day;
            var aggregateHrs = 0;
            angular.forEach(data, (timeclock) => {
                if (timeclock.Day == previousDate) {
                    aggregateHrs = aggregateHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                }
                else {
                    this.dayAggregates.push(<personnelInterfaces.IDayAggregate>{
                        day: previousDate,
                        aggregateHours: aggregateHrs
                    });
                    previousDate = timeclock.Day;
                    aggregateHrs = this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime;
                }
            });
            //to push last date aggregate
            this.dayAggregates.push(<personnelInterfaces.IDayAggregate>{
                day: previousDate,
                aggregateHours: aggregateHrs
            });
        }

        //reset to show STE hours on toggle
        getStraightTimeEquivalentHours = () => {
            this.showStraightTimeEquivalent ? this.showStraightTimeEquivalent = false : this.showStraightTimeEquivalent = true;
            this.showStraightTimeEquivalent ? this.showSTEBtnText = 'SHOW REG HOURS' : this.showSTEBtnText = 'SHOW STE HOURS';
            this.calculateAggregateDayHours(this.timeClocks);
            this.getWeeklyHours();
            this.setCalendarEvents();
        }

        //to get aggregate total hours of a particular day
        getAggregateDayHours = (date: Date): number => {

            for (var i = 0; i < this.dayAggregates.length; i++) {
                if (this.dayAggregates[i].day == date) {
                    return this.dayAggregates[i].aggregateHours;
                }
            }
        }

        getPagingParams = () => {
            var startDay = 1;
            var endDay = moment(this.dateRange.endDate).endOf('month').date();
            var month = this.dateRange.month + 1;
            var year = this.dateRange.year;
            var params = <personnelInterfaces.IODataPagingParamModel>{
                $orderby: 'StartTime asc',
                $filter: 'day(Day) ge ' + startDay + ' and day(Day) le ' + endDay + ' and month(Day) eq ' + month + ' and year(Day) eq ' + year
            };
            return params;
        }


    }

    angular.module("app").controller("Personnel.MyFinancialsCtrl", MyFinancialsCtrl);

    angular.module("app").controller("Personnel.MyProfileIndex", MyProfileCtrl);

}