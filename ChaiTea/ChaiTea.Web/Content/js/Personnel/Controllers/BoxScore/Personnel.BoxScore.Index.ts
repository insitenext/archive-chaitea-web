﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IBoxScoreEmployee extends Training.Interfaces.ITrainingEmployee {
        BoxScores: any;
        _isLoading: boolean;
    }

    class BoxScoreIndex implements commonInterfaces.INgController {

        createNewItems = [];
        employees: Array<IBoxScoreEmployee> = [];
        trainingEmployees: Array<IBoxScoreEmployee> = [];
        clonedTrainingEmployees: Array<IBoxScoreEmployee> = [];
        sortInAscending: boolean = true;
        orderBy: string = 'Name asc';
        sortByColumn: string = 'profiles';
        endPoint: string = 'TrainingEmployee';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').format(),
            endDate: moment().format()
        }

        employeeLoopIndex = 0;

        reportsConfig = {
            control: 3,
            minScale: 0,
            scale: 5
        };
        counts = {
            Attendance: 0,
            Conduct: 0,
            Complaints: 0,
            Compliments: 0,
            Cafety: 0,
            Ownership: 0,
            Professionalism: 0,
            QualityAverage: 0.00,
            Training: 0,
            TotalEmployees: 0
        };
        positionProfilesCompleted: number = 0;
        trainingsCompleted: number = 0;

        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        top: number = 10;
        recordsToSkip: number = 0;
        columnIndexToShow: number;

        modalInstance;

        static $inject = [
            '$scope',
            '$log',
            'blockUI',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.trainingsvc',
            '$uibModal'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private blockUI,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private utilitySvc: Common.Services.IUtilitySvc,
            private imageSvc: Common.Services.IImageSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private dateFormatSvc: Core.Services.IDateFormatterSvc,
            private reportHelperSvc: Common.Services.IReportSvc,
            private trainingSvc: Common.Services.ITrainingSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.createNewItems = [
                {
                    link: this.sitesettings.basePath + 'Personnel/Attendance',
                    name: 'Attendance'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Conduct',
                    name: 'Conduct'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Complaint',
                    name: 'Complaint'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Compliment',
                    name: 'Compliment'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Safety',
                    name: 'Safety'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Ownership',
                    name: 'Report It'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Audit',
                    name: 'Quality'
                },
                {
                    paramsLink: this.sitesettings.basePath + 'Personnel/Audit?runAudit=true',
                    name: 'Professionalism'
                },
                // [Dw] Told to leave out 2/27/16
                //{
                //    link: this.sitesettings.basePath + 'Training/',
                //    name: 'Training'
                //}
            ];

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).add(1, 'days').format();

                this.getEmployeesReset();
            });
        }

        public initialize = (): void => {

            // Goal from Report settings 
            this.setReportsConfig();
            this.blockUI.start();

            this.tableColumnHide(false, 2);
        }

        private getAllBoxScoreCounts = () => {
            var tempCounts = {
                Attendance: 0,
                Conduct: 0,
                Complaints: 0,
                Compliments: 0,
                Cafety: 0,
                Ownership: 0,
                Professionalism: 0,
                QualityAverage: 0.00,
                TotalEmployees: 0,
                ProfileComplete: 0
            };
                //TODO GET all Training counts as well. Currently adding as it loads from getBoxScores().
                var params = {
                    startDate: this.dateFormatSvc.formatDateShort(this.dateRange.startDate),
                    endDate: this.dateFormatSvc.formatDateShort(this.dateRange.endDate)
                };
                return this.apiSvc.getResource(`BoxScore/All/:startDate/:endDate`).get(params)
                    .$promise.then((res) => {
                        this.counts = res;
                    });
        }
       
        public setSortingColumn = (column: number) => {
            this.sortInAscending = this.sortInAscending ? false : true;
            
            switch (column) {
                case 0:
                    this.sortByColumn = 'profiles';
                    this.endPoint = 'TrainingEmployee'
                    break;
                case 1:
                    this.sortByColumn = 'attendance';
                    this.endPoint = 'Issue/EmployeesIssueCount/:startDate/:endDate'
                    break;
                case 2:
                    this.sortByColumn = 'conduct';
                    this.endPoint = 'Conduct/EmployeesConductCount/:startDate/:endDate'
                    break;
                case 3:
                    this.sortByColumn = 'complaints';
                    this.endPoint = 'BoxScore/EmployeesComplaintsCount/:startDate/:endDate'
                    break;
                case 4:
                    this.sortByColumn = 'compliments';
                    this.endPoint = 'BoxScore/EmployeesComplimentsCount/:startDate/:endDate'
                    break;
                case 5:
                    this.sortByColumn = 'safety';
                    this.endPoint = 'Personnel/Safety/EmployeesSafetyCount/:startDate/:endDate'
                    break;
                case 6:
                    this.sortByColumn = 'reportIts';
                    this.endPoint = 'Ownership/EmployeesOwnershipsCount/:startDate/:endDate'
                    break;
                case 7:
                    this.sortByColumn = 'professionalism';
                    this.endPoint = 'EmployeeAudit/EmployeesAuditsIsPassCount/:startDate/:endDate'
                    break;
                case 8:
                    this.sortByColumn = 'audits';
                    this.endPoint = 'BoxScore/EmployeesAuditsAvgMetric/:startDate/:endDate'
                    break;
                case 9:
                    this.sortByColumn = 'training';
                    this.endPoint = 'BoxScore/EmployeesTrainingComplianceMetric/:startDate/:endDate'
                    break;
                default:
                    this.sortByColumn = 'attendance';
                    this.endPoint = 'Issue/EmployeesIssueCount/:startDate/:endDate'
            }

            this.orderBy = this.sortInAscending ? (this.sortByColumn == 'profiles' ? 'Name asc' : 'Metric asc') : (this.sortByColumn == 'profiles' ? 'Name desc' : 'Metric desc');
            this.resetEmployeesForSorting();
        }

        private resetEmployeesForSorting = () => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.employees = [];
            this.getBoxScores();
            this.clonedTrainingEmployees = _.clone(this.trainingEmployees);
        }

        private getEmployeesReset = () => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.employees = [];
            this.trainingEmployees = [];
            this.recordsToSkip = 0;
            this.getAllBoxScoreCounts();
            this.getAllEmployees().then(() => {
                this.clonedTrainingEmployees = _.clone(this.trainingEmployees);
                this.getBoxScores();
            });
            
        }

        private searchEmployees = () => {
            if (!this.isAllLoaded) {
                this.employees = this.trainingEmployees;
                this.isAllLoaded = true;
            }            
        }

        private getAllEmployees = () => {
            var params = {
                $orderby: 'Name asc',
            };
            return this.apiSvc.getByOdata(params, 'TrainingEmployee').then((result: Array<IBoxScoreEmployee>) => {
                if (!result.length) {
                    this.$log.error('no employees');
                    return;
                }
                angular.forEach(result, (employee) => {
                    employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
                    employee._isLoading = true;
                    this.trainingSvc.isPositionProfileComplete(employee);
                    this.trainingEmployees.push(employee);
                });

            });
        }

        public getBoxScores = () => {
            if (this.trainingEmployees.length > 0) {
                var self = this;
                
                if (this.isBusy || this.isAllLoaded) return;
                this.isBusy = true;

                var asyncQueue = async.queue((task: any, callback) => {
                    callback();
                }, 2);
                asyncQueue.drain = () => {  };

                async.auto({
                    getEmployees: (callback: any) => {
                        this.getEmployeesData().then((results) => {
                            callback(null, results);
                        });
                    },
                    processEmployeeData: ['getEmployees', (callback, results) => {
                        angular.forEach(results.getEmployees, (emp) => {
                            emp.IsLoading = true;
                            asyncQueue.push({ name: 'getEmpBoxScore '},  function (err) {
                                self.getEmployeesBoxScores(emp);
                            });
                        });

                        callback(null, results);

                    }]
                },
                    (err, results) => {
                        if (this.blockUI) this.blockUI.stop();

                        this.isBusy = false;
                        this.recordsToSkip += this.top;
                    })
            }
        }

        private getEmployeesData = (): angular.IPromise<any> => {
            var params = {
                $top: this.top,
                $skip: this.recordsToSkip,
                $orderby: this.orderBy,
                startDate: this.dateFormatSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatSvc.formatDateShort(this.dateRange.endDate)
            };
            return this.apiSvc.getByOdata(params, this.endPoint).then((result) => {
                if (result.length) {
                    var employeeIds = _.map(result, 'EmployeeId');
                    _.forEach(result, (r) => {
                        var emp = _.remove(this.clonedTrainingEmployees, (temp) => {
                                return temp.EmployeeId == r.EmployeeId;
                        })

                        if (emp[0]) {
                            this.employees.push(emp[0]);
                         }
                    })
                }
                else if (this.clonedTrainingEmployees.length) {
                    var i = 0;
                    _.forEach(this.clonedTrainingEmployees, (outerEmp) => {
                        if (i < 10) {
                            this.employees.push(
                                _.remove(this.clonedTrainingEmployees, (innerEmp) => {
                                    return outerEmp.EmployeeId == innerEmp.EmployeeId;
                                })[0]
                            );
                            i++
                        }
                    });
                }
                return this.employees;
                });


        }

        private getEmployeesBoxScores = (employee: IBoxScoreEmployee) => {
            var params = {
                employeeId: employee.EmployeeId,
                startDate: this.dateFormatSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatSvc.formatDateShort(this.dateRange.endDate)
            };

            return this.apiSvc.getResource(`BoxScore/:employeeId/:startDate/:endDate`).get(params)
                .$promise.then((res) => {
                    if (!res) return this.$log.log('No employee data.');

                    var emp = _.find(this.employees, (item) => (item.EmployeeId == employee.EmployeeId));

                    if (!emp) return this.$log.log('Cannot find employee with id', employee.EmployeeId);

                    emp.BoxScores = res;
                    emp.BoxScores.AuditAverage = res.AuditAverage ? res.AuditAverage : 0;
                    emp._isLoading = false;

                    //call tablecolumnhide on next 10 employees rows (only when we shift to mobile mode) 
                    if ($('.box-score-table thead tr.metrics th span').hasClass('prev')) {
                        this.tableColumnHide(false, this.columnIndexToShow);
                    }

                });
        }

        public goToPositionProfile = (employeeId: number) => {
            if (!employeeId) return this.$log.error('no emp id passes');
                window.location.href = this.sitesettings.basePath + 'Training/PositionProfile/EntryView/'+employeeId;
        }

        public tableColumnHide = (showAll: boolean = false, columnIndexToShow) => {
            this.columnIndexToShow = columnIndexToShow;
            var visibleClass = 'visible-md visible-lg';
            var allMetricsThs = $('.box-score-table thead tr.metrics th');
            var currentVisisbleEl = allMetricsThs.eq(columnIndexToShow);

            if (!currentVisisbleEl.length) {
                return this.$log.log('no more columns', columnIndexToShow);
            }

            $('.box-score-table thead th, .box-score-table tbody td').removeClass('visible');

            $('.box-score-table thead tr.metrics th').find('span.prev,span.next').unbind('click');

            currentVisisbleEl.addClass('visible').removeClass(visibleClass);

            //If no prev and next button; add them
            if (!(currentVisisbleEl.find('> span').length > 1) ) {
                var currentA = currentVisisbleEl.find('a');
                if ((columnIndexToShow) > 2) {
                    currentA.before('<span class="fa fa-angle-double-left prev"></span>');
                }
                if (columnIndexToShow > 2) {
                    if ((columnIndexToShow + 1) < (allMetricsThs.length / 2)) {
                        currentA.after('<span class="fa fa-angle-double-right next"></span>');
                    }
                }
            }

            //Bind on click to new span tags
            currentVisisbleEl.find('span.prev').on('click', (el) => {
                if ((columnIndexToShow - 1) == 1) {
                    return;
                }
                this.tableColumnHide(false, columnIndexToShow - 1);
            });

            currentVisisbleEl.find('span.next').on('click', (el) => {
                if ((columnIndexToShow + 1) >= (allMetricsThs.length)) {
                    return;
                }
                this.tableColumnHide(false, columnIndexToShow + 1);
            });

            //loop through and add {visibleClass} to all columns that are NOT index 0 or 1 and NOT currentCol
            $('.box-score-table thead tr, .box-score-table tbody tr').each((indx, el) => {
                var ths = $(el).find('th');
                var tds = $(el).find('td');

                if (!showAll) {
                    $(ths).each((indx, el) => {
                        if (indx > 1) {
                            if (columnIndexToShow != indx) {
                                $(el).addClass(visibleClass);
                            } else {
                                $(el).removeClass(visibleClass);
                            }
                        }
                    });
                    
                    $(tds).each((indx, el) => {
                        if (indx > 1) {
                            if (columnIndexToShow != indx) {
                                $(el).addClass(visibleClass);
                            } else {
                                $(el).removeClass(visibleClass);
                            }
                        }
                    });
                } else {
                    $(ths).removeClass(visibleClass);
                    $(tds).removeClass(visibleClass);
                }
            });
        }

        /**
         * @description Set global reports configuration.
         */
        private setReportsConfig = () => {
            return this.reportHelperSvc.getReportSettings().then((r) => {
                this.reportsConfig.control = r.ScoringProfileGoal;
                this.reportsConfig.minScale = r.ScoringProfileMinimumScore;
                this.reportsConfig.scale = r.ScoringProfileMaximumScore;
                
            });

        }

        public popModal = (module, employee) => {
            //WIP
            var dateRangeModified = {
                startDate: this.dateFormatSvc.formatDateFull(moment(this.dateRange.startDate).startOf('month')),
                endDate: this.dateFormatSvc.formatDateFull(moment(this.dateRange.endDate).subtract(1,'days').endOf('day'))
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.BoxScoresModalCtrl as vm',
                size: 'md',
                resolve: {
                    dateRange: () => { return dateRangeModified; },
                    employee: () => { return employee; },
                    module: () => { return module; }
                }
            });
        }
    }

    angular.module('app')
        .config(['blockUIConfig',(blockUIConfig) => {
            //disable Loading screen during all HTTP Requests
            blockUIConfig.autoBlock = false;
        }])
        .controller('Personnel.BoxScore.IndexCtrl', BoxScoreIndex);

    interface IItem {
        topLeftText: string;
        topRightText: string;
        topRightClass: string;
        bodyHeader: string;
        bodyText: string;
        bodyHtmlText: any;
        leftFooter: string;
        editButtonLink: string;
        viewButtonLink: string;
    }

    class BoxScoreModal {

        basePath: string;
        
        itemList: Array<IItem> = [];

        modalTitle: string = 'Attendance';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        myPromise = null;
        loaded: boolean = false;

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.boxscoresvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.personnel.services.conductsvc',
            'chaitea.personnel.services.safetysvc',
            'dateRange',
            'employee',
            'module',
            '$filter',
            'userContext'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private boxScoreSvc: ChaiTea.Common.Services.IBoxScoreSvc,
            private attendanceSvc: Personnel.Services.IAttendanceSvc,
            private conductSvc: Personnel.Services.IConductSvc,
            private safetySvc: Personnel.Services.ISafetySvc,
            private dateRange: Common.Interfaces.IDateRange,
            private employee: IBoxScoreEmployee,
            private module: string,
            private $filter: any,
            private userContext: IUserContext
        ) {

            this.modalTitle = 'REVIEW ' + this.module.toLocaleUpperCase();
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Personnel/Templates/personnel.boxScoreModal.html';
            this.getModuleByEmployeeId(dateRange,employee,module);

        }

        private getModuleByEmployeeId = (dateRange: Common.Interfaces.IDateRange, employee: IBoxScoreEmployee, module: string) => {

            this.loaded = false;
            //TODO Error with DateTimeOffset in attendance api call and 'gt'.  Finish styling of popup.
            switch (module){
                case 'attendance':
                    var params:any = {
                        $orderby: 'DateOfOccurrence desc',
                        $filter: `OrgUserId eq ${employee.EmployeeId} and DateOfOccurrence gt DateTime'${dateRange.startDate}' and DateOfOccurrence le DateTime'${dateRange.endDate}'`
                    };
                    this.myPromise = this.attendanceSvc.getAttendanceIssues(params).then((result) => {
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            var date = moment(item.DateOfOccurrence);
                            listItem.topLeftText =  date.format('MM.DD.YYYY');
                            listItem.topRightText = item.IssueTypeName;
                            listItem.bodyHeader = item.IssueTypeName + ': ' + this.$filter('displayTimezone')(this.$filter('date')(date.toDate(), 'h:mm a', this.userContext.Timezone.OffsetInHours));
                            listItem.bodyText = item.Comment || '-There are no comments-';
                            listItem.leftFooter = item.ReasonName || "No reason";
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Attendance/EntryView/' + item.IssueId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'conduct':
                    var params: any = {
                        $orderby: 'DateOfOccurrence desc',
                        $filter: `OrgUserId eq ${employee.EmployeeId} and DateOfOccurrence gt DateTime'${dateRange.startDate}' and DateOfOccurrence le DateTime'${dateRange.endDate}'`
                    };
                    this.myPromise = this.conductSvc.getConducts(params).then((result) => {
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = this.$filter('date')(item.DateOfOccurrence, 'MM.dd.yyyy', this.userContext.Timezone.OffsetInHours);
                            listItem.topRightText = 'CONDUCT OCCURRENCE';
                            listItem.bodyText = item.Reason || '-There are no comments-';
                            listItem.leftFooter = item.ConductTypes[0].Name;
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Conduct/EntryView/' + item.ConductId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'complaints':
                    var params: any = {
                        $orderby: 'CreateDate desc',
                        $filter: `FeedbackDate ge DateTime'${dateRange.startDate}' and FeedbackDate lt DateTime'${dateRange.endDate}' and AccountableEmployees/any(d:d eq ${employee.EmployeeId})`
                    };

                    this.myPromise = this.apiSvc.getByOdata(params, 'Complaints').then((result: Array<serviceInterfaces.IComplaint>) => {
                       
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            
                            listItem.topLeftText = moment(item.FeedbackDate).format('MM.DD.YYYY');
                            listItem.topRightText = 'COMPLAINT FILED';
                            listItem.bodyHeader = "Actions taken: " + item.Action;
                            listItem.bodyText = item.Description;
                            listItem.leftFooter = item.ClassificationName;
                            listItem.editButtonLink = this.sitesettings.basePath + 'Quality/Complaint/Entry/' + item.ComplaintId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'compliments':
                    var params: any = {
                        $orderby: 'CreateDate desc',
                        $filter: `FeedbackDate ge DateTime'${dateRange.startDate}' and FeedbackDate lt DateTime'${dateRange.endDate}' and AccountableEmployees/any(d:d eq ${employee.EmployeeId})`
                    };

                    this.myPromise = this.apiSvc.getByOdata(params, 'Compliments').then((result: Array<serviceInterfaces.ICompliment>) => {

                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};

                            listItem.topLeftText = moment(item.FeedbackDate).format('MM.DD.YYYY');
                            listItem.topRightText = 'COMPLIMENT ADDED';
                            listItem.bodyText = item.Description;
                            listItem.leftFooter = item.ComplimentTypeName;
                            listItem.editButtonLink = this.sitesettings.basePath + 'Quality/Compliment/Entry/' + item.ComplimentId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'safety':
                    var params: any = {
                        $orderby: 'DateOfIncident desc',
                        $filter: `EmployeeId eq ${employee.EmployeeId} and DateOfIncident gt DateTime'${dateRange.startDate}' and DateOfIncident le DateTime'${dateRange.endDate}'`
                    };
                    this.myPromise = this.safetySvc.getSafety(params).then((result: Array<serviceInterfaces.ISafetyItem>) => {
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            var date = moment(item.DateOfIncident);
                            listItem.topLeftText = date.format('MM.DD.YYYY');
                            listItem.topRightText = 'INCIDENT_OCCURRENCE';
                            listItem.bodyText = item.Detail || '-There are no details-';
                            listItem.leftFooter = item.SafetyAttachments.length ? item.SafetyAttachments.length + ' attachments' : '';
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Safety/EntryView/' + item.SafetyId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'report it':
                    var params: any = {
                        $orderby: 'StatusUpdateDate desc',
                        $filter: `EmployeeId eq ${employee.EmployeeId} and StatusUpdateDate gt DateTime'${dateRange.startDate}' and StatusUpdateDate le DateTime'${dateRange.endDate}' and Status eq '1'`
                    };
                    this.myPromise = this.apiSvc.getByOdata(params, 'Ownership').then((result) => {
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = moment(item.StatusUpdateDate).format('MM.DD.YYYY');
                            listItem.topRightText = item.Status == 1 ? 'Status : Accepted' : 'Status : Pending/Rejected';
                            listItem.bodyText = item.Description;
                            listItem.leftFooter = item.OwnershipAttachments.length ? item.OwnershipAttachments.length + ' attachments' : '';
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Ownership/Entry/' + item.OwnershipId;
                            this.itemList.push(listItem);
                        });
                        this.loaded = true;
                    }, this.onFailure);
                    break;
                case 'professionalism':

                    this.myPromise = this.boxScoreSvc.getProfessionalismModalContent(employee.EmployeeId, dateRange.startDate, dateRange.endDate).then((res) => {

                        angular.forEach(res[0], (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = moment(item.CreateDate).format('MM.DD.YYYY');
                            listItem.topRightText = item.IsPass ? 'Passed' : 'Failed';
                            listItem.topRightClass = item.IsPass ? 'text-success' : 'text-danger';
                            listItem.bodyText = this.boxScoreSvc.buildProfessionalismContent(item, res[1]);
                            listItem.leftFooter = item.IsEventBasedAudit ? 'Event based' : '';
                            listItem.viewButtonLink = this.sitesettings.basePath + 'Personnel/Audit/EntryView/' + item.EmployeeAuditId;
                            this.itemList.push(listItem);

                        });

                        this.loaded = true;
                        
                    }, this.onFailure);

                    break;
                case 'quality':
                    
                    break;
                default:
                    break;
            }
            
        }

        private onFailure = (response: any): void => {
            this.loaded = true;
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    
    angular.module('app').controller('Personnel.BoxScoresModalCtrl', BoxScoreModal);
    }