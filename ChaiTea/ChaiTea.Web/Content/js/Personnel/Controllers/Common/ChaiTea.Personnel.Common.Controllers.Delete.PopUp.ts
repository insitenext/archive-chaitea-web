﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;

    class DeletePopUpCtrl {
        Id: number = 0;
        basePath: string;
        downloadUrl: string;
        moduleIcon: string;
        moduleClass: string;
        endPoint: string;
        moduleName: string;
        translateOnPageLoad = {
            'WARNING': "CANNOT_DELETE_WARNING",
            "REASON": "CANNOT_DELETE_REASON"
        };
        static $inject = [
            '$scope',
            'sitesettings',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc',
            'id',
            'endpoint',
            'icon',
            'module',
            '$translate',
            '$uibModalInstance'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private id: number,
            private endpoint: string,
            private icon: string,
            private module: string,
            private $translate: angular.translate.ITranslateService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {

            this.Id = id;
            this.basePath = sitesettings.basePath;
            this.moduleIcon = icon;
            this.endPoint = endpoint;
            this.moduleName = module;
            this.translateOnPageLoad[this.moduleName] = this.moduleName;
            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
        }

      
        /**
         * @description Handle service failure
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }
         
        deleteItem = () => {
            this.apiSvc.delete(this.Id, this.endPoint).then((response) => {
                if (response.IsClosed) {
                    this.notificationSvc.infoToastMessage(this.translateOnPageLoad.WARNING + ' ' + this.translateOnPageLoad[this.moduleName] + ' ' + this.translateOnPageLoad.REASON );
                }
                else {
                    this.$uibModalInstance.close(true);
                }
            }, this.onFailure);
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

    }

    angular.module("app").controller("Personnel.DeletePopUpCtrl", DeletePopUpCtrl);
}
