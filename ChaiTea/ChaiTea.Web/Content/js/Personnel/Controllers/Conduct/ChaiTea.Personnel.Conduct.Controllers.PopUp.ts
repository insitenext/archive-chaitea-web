﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Conduct.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;

    enum FileType { Undefined, ProfilePicture };

    class ConductPopUpCtrl {
        conduct: any;
        conductId: number = 0;

        basePath: string;
        downloadUrl: string;
        moduleIcon: string;
        moduleClass: string;
        modalLeftSection: string;
        modalRightSection: string;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.notificationsvc',
            '$uibModal',
            'id',
            '$uibModalInstance'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {

            this.conductId = id;
            this.basePath = sitesettings.basePath;
            this.moduleClass = 'conduct';
            this.moduleIcon = 'star-o';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/Conduct/personnel.popup.conduct.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/Conduct/personnel.popup.conduct.rightsection.tmpl.html';
            this.getConductById();
        }
        
        /**
         * @description Get the conduct data by id
         */
        private getConductById = (): void=> {
            var params = {
                id: this.conductId,
            }
            this.apiSvc.getById(this.conductId, "Conduct/ConductById").then((result) => {
                this.conduct = result;
                this.conduct.ProfileImage = this.imageSvc.getUserImageFromAttachments(this.conduct.UserAttachments, true);
            }, this.onFailure);
        }

        /**
         * @description Handle service failure
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private deleteConduct = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.delete.popup.common.tmpl.html',
                controller: 'Personnel.DeletePopUpCtrl as vm',
                size: 'md',
                resolve: {
                    id: () => {
                        return this.conductId;
                    },
                    endpoint: () => {
                        return "Conduct"
                    },
                    icon: () => {
                        return "star-o"
                    },
                    module: () => {
                        return "CONDUCT"
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.$uibModalInstance.close(true);
                    this.notificationSvc.successToastMessage('Conduct deleted successfully');
                }
            });
        }
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        public editConduct = () => {
            window.location.replace(this.basePath + 'Personnel/Conduct/Entry/' + this.conduct.ConductId);
        }
        public redirectToEntry = () => {
            window.location.href = this.basePath + "Personnel/Conduct/Entry/" + this.conductId;
        }
    }

    angular.module("app").controller("Personnel.ConductPopUpCtrl", ConductPopUpCtrl);
}
