﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Conduct.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonInterfaces = Common.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    interface IConductType {
        conductTypeId: number;
        ConductTypeName: string;
    }

    class ConductIssueEntryViewCtrl {
        conduct: personnelInterfaces.IConduct;
        conductId: number = 0;

        employeeWithImage = {};
        conductType = {};
        basePath: string;
        downloadUrl: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.personnel.services.conductsvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private conductSvc: personnelSvc.IConductSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {

            this.basePath = sitesettings.basePath;
        }

        initialize = (conductId: number): void=> {
            this.conductId = conductId;
            this.getConductById(this.conductId);
        }

        /**
         * @description Get the conduct data by id
         */
        getConductById = (conductId: number): void=> {
            if (conductId > 0) {
                this.conductSvc.getConduct(conductId).then((result) => {
                    this.conduct = result;

                    this.getEmployeeWithImage(result.OrgUserId);
                    this.conductType = result.ConductTypes;
                    this.downloadUrl = (this.conduct.ConductAttachments.length ? this.awsSvc.getUrlByAttachment(this.conduct.ConductAttachments[0].UniqueId, this.conduct.ConductAttachments[0].AttachmentType) : '');

                }, this.onFailure);
            }
        }

        /**
         * @description Handle service failure
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        redirectToDetails = () => {
            window.location.replace(this.basePath + 'Personnel/Conduct/Details');
        }

        deleteConduct = (id: number) => {
            bootbox.confirm("Are you sure you want to delete this conduct entry?", (res) => {
                if (res) {
                    this.apiSvc.delete(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Conduct]).then((response) => {
                        if (response.IsClosed) {
                            bootbox.alert("You can not delete conduct of the month for which scorecard entry is disabled");
                        }
                        else {
                            this.redirectToDetails();
                        }
                    });
                }
            });
        }

        editConduct = () => {
            window.location.replace(this.basePath + 'Personnel/Conduct/Entry/' + this.conduct.ConductId);
        }
        /**
         * @description Get the employee and attach image
         */
        getEmployeeWithImage = (id: number): void=> {

            this.apiSvc.getById(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                this.employeeWithImage = <personnelInterfaces.IEmployee>{
                    Name: employee.Name,
                    EmployeeId: employee.EmployeeId,
                    UserAttachments: employee.UserAttachments,
                    HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                    JobDescription: employee.JobDescription.Name,
                    ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                }
            });
        }

    }

    angular.module("app").controller("Personnel.ConductIssueEntryViewCtrl", ConductIssueEntryViewCtrl);
}
