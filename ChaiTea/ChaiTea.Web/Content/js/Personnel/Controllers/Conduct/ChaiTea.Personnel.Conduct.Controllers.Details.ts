﻿/// <reference path="../../../_libs.ts" />

 
module ChaiTea.Personnel.Conduct.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    
    interface ITabs {
        AllCount: number;
        WarningCount: number;
        ActionTakenCount: number;
    }
    class ConductIssueDetailsCtrl {
    
        siteId: number = 0;
        basePath: string;
        showFilter: boolean = false;
        filterReasonId: number = 0;
        conductTypes = [];
        conductIssues = [];
        employees = [];
        filterEmployeeId: number = 0;
        filterConductTypeId: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        activeTab: number = 0;
        tabs: ITabs = {
            AllCount: 0,
            WarningCount: 0,
            ActionTakenCount: 0
        };
        top: number = 20;
        skip: number = 0;
        isFilterCollapsed: boolean = true;
        filterOptions = [
            { Key: 'EmployeeId', Value: 'Employee', Options: null, FieldType: 'select' },
            { Key: 'ConductTypeId', Value: 'Conduct Type', Options: null, FieldType: 'select' },
        ];
        filter = {
            filterType: null,
            filterCriteria: null
        };
        headerText: string = 'DATE_INCIDENT';
        search: string = '';
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.conductsvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            '$uibModal',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private conductSvc: personnelSvc.IConductSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.getTabCounts();
                // Reset the paging
                this.getConductIssueReset({
                    startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
                });
            });

            this.getFilterOptions();
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.tabFilterItems = [
                {
                    title: 'All',
                    onClickFn: () => { this.toggleActiveTab(0) },
                    count: this.tabs.AllCount
                },
                {
                    title: 'Warning',
                    onClickFn: () => { this.toggleActiveTab(1) },
                    count: this.tabs.WarningCount
                },
                {
                    title: 'Action Taken',
                    onClickFn: () => { this.toggleActiveTab(2) },
                    count: this.tabs.ActionTakenCount
                }
            ];

            this.$scope.$watch(() => (this.tabs), (newVal: any, oldVal: any) => {
                if (newVal === oldVal) return false;

                this.tabFilterItems[0].count = this.tabs.AllCount;
                this.tabFilterItems[1].count = this.tabs.WarningCount;
                this.tabFilterItems[2].count = this.tabs.ActionTakenCount;

            }, true);
        }
        private toggleActiveTab = (activeTab: number) => {
            this.activeTab = activeTab;
            this.conductIssues = [];
            this.isAllLoaded = false;
            this.skip = 0;
            this.getConductIssues({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private getConductIssueReset = (dateRange: commonInterfaces.IDateRange): void => {
            this.conductIssues = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.getTabCounts();
            this.getConductIssues(dateRange);
        }
        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

         /**
         * @description Get Tab counts
         */
        private getTabCounts = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                conductTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria : null
            };
            this.apiSvc.query(params, 'Conduct/ConductStats', false, false).then((result: ITabs) => {
                this.tabs = result;
            }, this.onFailure);
        }

        /**
         * @description Get attendance issues and push to array for infinite scroll.
         */
        private getConductIssues = (dateRange: commonInterfaces.IDateRange): void => {
           
            var params = {
                top: this.top,
                skip: this.skip,
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                getWarning: (this.activeTab == 1) ? true : false,
                getActionTaken: (this.activeTab == 2) ? true : false,
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                conductTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria: null
            }
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            this.apiSvc.query(params, "Conduct/ConductInfo").then((result) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                    this.isBusy = false;
                    return;
                }
                angular.forEach(result, (conduct) => {
                    conduct.ProfileImage = this.imageSvc.getUserImageFromAttachments(conduct.UserAttachments, true);
                    this.conductIssues.push(conduct);
                });
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure);
        }


        /**
         * @showing filter
         *
          */
        private getFilter = (): void => {
            this.showFilter = (this.showFilter ? false : true);
            this.filterEmployeeId = 0;
            this.filterConductTypeId = 0;
        }

         /**
         * @description on employee change in dropdown
         */
        private onEmployeeChange = (empId: number): void => {
            this.filterEmployeeId = empId;
        }

        /**
         * @description on conduct type change in dropdown
         */
        private onConductTypeChange = (conductTypeId: number): void => {
            this.filterConductTypeId = conductTypeId;
        }

        /**
         * @description Get Conducts based on filter selected
         */
        private getConductOnFilter = (): void => {
            this.getConductIssues({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }

        /**
         * @description Get filter options
         */
        private getFilterOptions = () => {
            this.employees = [];
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $select: 'EmployeeId,Name' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((result) => {
                angular.forEach(result, (employee) => {
                    this.employees.push({
                        Key: employee.EmployeeId,
                        Value: employee.Name
                    });
                });
                this.filterOptions[0].Options = this.employees;
            });
            this.serviceLookupListSvc.getConductTypes()
                .then((result) => {
                    angular.forEach(result, (item) => {
                        this.conductTypes.push({
                            Key: item.ConductTypeId,
                            Value: item.Name
                        });
                    });
                    this.filterOptions[1].Options = this.conductTypes;
                });
        }

        /**
         * @description Open details pop-up
         */
        public showEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.ConductPopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.conductIssues, { 'ConductId': id });
                    if (index != -1) {
                        this.conductIssues.splice(index, 1);
                        this.getTabCounts();
                    }
                }
            });
        }

        /** @description get attendance issue types and reasons related to types. */
        getConductTypes = (): void => {
            this.serviceLookupListSvc.getConductTypes()
                .then((result) => {
                this.conductTypes = result;
            });
        }

        /**
      * @description to export 
      */
        private export = (): void => {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var exportableConduct = [];
            var top = this.getExportableRecordCount();
            var params = {
                top: top,
                skip: 0,
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                getWarning: (this.activeTab == 1) ? true : false,
                getActionTaken: (this.activeTab == 2) ? true : false,
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                conductTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria : null
            }
           
            this.apiSvc.query(params, "Conduct/ConductInfo").then((result) => {
                angular.forEach(result, (conduct) => {
                    var types = "";
                    angular.forEach(conduct.ConductTypes, (type) => {
                        types = types + type.Name + " "
                    });
                    exportableConduct.push({
                        EmployeeName: conduct.EmployeeName,
                        Comment: conduct.Reason.replace(/\n/g, ' '),
                        Type: types,
                        Date: conduct.DateOfOccurrence
                    });
                });
                alasql('SELECT * INTO CSV("conduct-details.csv",?) FROM ?', [mystyle, exportableConduct]);
            }, this.onFailure);

        }

        /**
         * @description Gets the total record count for export functionality
         */
        private getExportableRecordCount = (): number => {
            var total = 0;
            switch (this.activeTab) {
                case 0:
                    total = this.tabs.AllCount;
                    break;
                case 1:
                    total = this.tabs.WarningCount;
                    break;
                case 2:
                    total = this.tabs.ActionTakenCount;
                    break;
            }
            return total;
        }

         /**
         * @description cancel the filter option
         */
        private cancel = (): void => {
            this.filterEmployeeId = 0;
            this.filterConductTypeId = 0;
            this.getConductOnFilter();
        }

        
    }

    angular.module('app').controller('Personnel.ConductIssueDetailsCtrl', ConductIssueDetailsCtrl);
} 