﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Conduct.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;

    enum ConductSortBy {
        Employee_Name = 1,
        Warning_Count = 2,
        Action_Taken_Count = 3,
        Total_Count = 4
    }
   
    class ConductIndexCtrl {
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        basePath: string = '';
        viewLink: string = '';
        isEmployee: boolean = false;
        conducts = [];
        noOfMonths: number = 0;
       
        charts = {
            counts: {
                monthlyTrend: {
                    pastDue: 0,
                    closedOnTime: 0,
                    count: 0
                }
            },
            ConductCount: 0,
            Occurrences: {},
            MonthlyTrend: {},
            MonthlyAverage: {}
        };
        lastModifiedDate: Date = null;
       
        employeeList = [];
        minDate: Date = null;
        headerText: string = 'DATE_INCIDENT';
        clientName: string = '';
        siteName: string = '';
        top: number = 10;
        skip: number = 0;
        isAllLoaded: boolean = false;
        isBusy: boolean = false;
        sort = [
            {
                SortById: ConductSortBy.Employee_Name,
                Asc: true
            },
            {
                SortById: ConductSortBy.Warning_Count,
                Asc: false
            },
            {
                SortById: ConductSortBy.Action_Taken_Count,
                Asc: false
            },
            {
                SortById: ConductSortBy.Total_Count,
                Asc: false
            }
        ];
        activeSortIndex: number = 0;
        static $inject = [
            '$scope',
            '$q',
            '$filter',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            '$timeout',
            'blockUI',
            '$location',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $timeout: angular.ITimeoutService,
            private blockUI,
            private $window: angular.IWindowService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc
        ) {
            this.basePath = sitesettings.basePath;
            this.siteName = userContext.Site.Name;
            this.clientName = userContext.Client.Name;
            this.isEmployee = this.userInfo.mainRoles.employees;
            this.minDate = new Date();
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refresh();
            });
        }

        private initialize = (): void => {
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private getLastModifiedDate = (): void => {
            this.apiSvc.query({}, "Conduct/LastModifiedDate", false, false).then((result) => {
                this.lastModifiedDate = result.LastModifiedDate;
            }, this.onFailure);
        }

        private toggleSort = (index: number) => {
            this.activeSortIndex = index;
            this.sort[index].Asc = !this.sort[index].Asc;
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.employeeList = [];
            this.getEmployeeInfo();
        }

        private getConductCount = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day'))
            }
            this.apiSvc.query(params, "Conduct/ConductCount", false, false).then((result) => {
                this.charts.ConductCount = result.ConductCount
            }, this.onFailure);
        }

        private getRecentConduct = (): void => {
            this.conducts = [];
            var params = {
                top: 5,
                skip: 0,
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day'))
            }
            this.apiSvc.query(params, "Conduct/ConductInfo").then((result) => {
                this.conducts = result;
                angular.forEach(this.conducts, (conduct) => {
                    conduct.ProfileImage = this.imageSvc.getUserImageFromAttachments(conduct.UserAttachments, true);
                });
            }, this.onFailure);
        }
        private getOccurrences = (): void => {
            var data = [];
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day'))
            }
            this.apiSvc.query(params, "Conduct/ConductTypeCount").then((result) => {
                if (result.length == 0) {
                    return;
                }
                data = result;
                this.charts.Occurrences = this.chartConfigBuilder('Occurrences', {
                    tooltip: {
                        pointFormat: '<b>{point.y}</b>'
                    },
                    series: [
                        {
                            type: 'pie',
                            name: 'Occurrences',
                            innerSize: '60%',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['ConductTypeName'],
                                    sliceY = d['Count'],
                                    sliceId = d['ConductTypeId']
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    id: sliceId,
                                    events: {
                                        click: ($event) => {
                                            this.getConductTypeTrends({
                                                id: sliceId,
                                                name: sliceName,
                                                color: $event.point.color
                                            })
                                        }
                                    },
                                    marker: {
                                    }
                                }
                            })
                        }
                    ]
                });
                this.getConductTypeTrends({
                    id: data[0]['ConductTypeId'],
                    name: data[0]['ConductTypeName']
                });
            }, this.onFailure);

          
            
        }

        private getConductTypeTrends = (selectedItem) => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                conductTypeId: selectedItem.id
            };
            this.apiSvc.query(params, "Conduct/MonthlyTrend").then((result) => {
                var categories = [];
                var ConductTypeCount = [];
                for (var i = moment(new Date(this.dateRange.startDate.toISOString())); i < moment(new Date(this.dateRange.endDate.toISOString())); i.add(1, 'month')) {
                    categories.push(i.format('MMM \'YY'));
                    var obj: any;
                    obj = _.find(result, { Month: (i.month() + 1), Year: i.year() });
                    if (obj) {
                        ConductTypeCount.push(obj.Count);
                    }
                    else {
                        ConductTypeCount.push(0);
                    }
                }
                this.charts.MonthlyTrend = this.chartConfigBuilder('MonthlyTrend', {
                    categories: categories,
                    series: [
                        {
                            color: selectedItem.color,
                            lineWidth: 3,
                            name: selectedItem.name,
                            data: ConductTypeCount
                        }
                    ]
                });
            }, this.onFailure);
        }

        

        private getEmployeeInfo = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: this.top,
                skip: this.skip,
                sortById: this.sort[this.activeSortIndex].SortById,
                asc: this.sort[this.activeSortIndex].Asc
            };
            this.apiSvc.query(params, "Conduct/ConductCountsByEmployee").then((result) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                    return;
                }
                angular.forEach(result, (employee) => {
                    employee.ProfileImage = employee.UserAttachments ? this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true) : SiteSettings.defaultProfileImage;
                    this.employeeList.push(employee);
                });
                if (this.employeeList.length < this.top) {
                    this.isAllLoaded = true;
                }
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure)
        }

        /**
         * @description Open details pop-up
         */
        public showEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.ConductPopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.refresh();
                }
            });
        }

        private refresh = (): void => {
            this.employeeList = [];
            this.skip = 0;
            this.isBusy = false;
            this.isAllLoaded = false;
            this.charts.MonthlyTrend = this.chartConfigBuilder('MonthlyTrend', {});
            this.charts.Occurrences = this.chartConfigBuilder('Occurrences', {});
            this.getRecentConduct();
            this.getLastModifiedDate();
            this.getEmployeeInfo();
            this.getConductCount();
            this.getOccurrences();
        }

        private chartConfigBuilder = (name, options) => {
            var chartOptions = {
                Occurrences: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },
                // Line Chart
                MonthlyTrend: {
                    legend: {
                        enabled: true,
                        labelFormatter: function () { return this.name.toUpperCase() },
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10
                    },
                    title: {
                        text: ''
                    },

                    categories: [],
                    xAxis: {
                        allowDecimals: false,
                        tickWidth: 0,
                        plotBands: [
                            {

                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        allowDecimals: false,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }

                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null // inherit from series
                            }
                        }
                    },
                    series: [
                        {
                            lineWidth: 3,
                            events: {
                                legendItemClick: () => (false)
                            }
                        }]

                }
            }
            return _.assign(chartOptions[name], options);
        }
    }
    angular.module('app').controller('Personnel.ConductIndexCtrl', ConductIndexCtrl);
}