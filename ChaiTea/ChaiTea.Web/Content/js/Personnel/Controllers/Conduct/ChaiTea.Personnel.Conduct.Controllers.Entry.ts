﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Conduct.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }
    
    class ConductIssueEntryCtrl {

        conductId: number;
        modalInstance;

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        employeesWithAttachment = [];
        employees = [];
        employee = {};
        employeeId: number = 0;
        ConductTypes = [];
        maxDate: Date = null;

        AttachmentName = '';

        //set defaults
        Conduct = <personnelInterfaces.IConduct>{
            ConductId: 0,
            DateOfOccurrence: <Date> null,
            Reason: '',
            ConductTypes: [],
            OrgUserId: 0,
            ConductAttachments: []
        }

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.common.services.awssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.conductsvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private ConductSvc: personnelSvc.IConductSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.IDateSvc
            ) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;

            this.modalInstance = this.$uibModal;
        }

        
        /** @description Initializer for the controller. */
        initialize = (conductId: number): void => {
            this.conductId = conductId;
            this.maxDate = new Date();
            this.Conduct.DateOfOccurrence = new Date();
            this.getConductTypes();
            
        }

        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void => {
            if (response.ConductId > 0) {
                mixpanel.track('Conduct ' + (this.conductId ? "Edited" : "Created"), { ConductId: response.ConductId });
                this.conductId = response.ConductId;
            }
            var forHistoricalPurposes = '';
            if (moment(this.Conduct.DateOfOccurrence) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                forHistoricalPurposes = '  This conduct is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
            }
            bootbox.alert('Conduct was saved successfully!' + forHistoricalPurposes, this.redirectToDetailsView);
        }

        /** @description get Conduct issue types and reasons related to types. */
        getConductTypes = (): void => {
            this.serviceLookupListSvc.getConductTypes()
                .then((result) => {
                angular.forEach(result,(item) => {
                    var conductType = <personnelInterfaces.IConductType>{
                        ConductTypeId: item.ConductTypeId,
                        Name: item.Name,
                        IsSelected: false
                    }
                    this.ConductTypes.push(conductType);
                });
                //  this.ConductIssueTypes = result;
                this.getEmployeeListWithAttachment();
            });
        }

        /** @description build ConductTypes object on click of ConductTypes. **/
        setConductType = (id: number): void => {

            var conductType = <personnelInterfaces.IConductType>{
                ConductTypeId: 0,
                Name: ''
            }
            angular.forEach(this.ConductTypes,(item) => {
                if (item.ConductTypeId == id)
                    conductType = <personnelInterfaces.IConductType>{
                        ConductTypeId: item.ConductTypeId,
                        Name: item.Name
                    }
            });

            if (!this.Conduct.ConductTypes.length) {
                this.Conduct.ConductTypes.push(conductType)
            }
            else {
                var index: number = 0;
                var checkFlag: boolean = false;

                angular.forEach(this.Conduct.ConductTypes,(item) => {

                    if (item.ConductTypeId == id) {
                        this.Conduct.ConductTypes.splice(index, 1);
                        checkFlag = true;
                    }
                    index++;
                });

                if (checkFlag == false) {
                    this.Conduct.ConductTypes.push(conductType);
                }
            }
        }

        updateConductTypes = (): void=> {
            angular.forEach(this.ConductTypes,(item) => {
                angular.forEach(this.Conduct.ConductTypes,(selectedItem) => {
                    if (item.ConductTypeId == selectedItem.ConductTypeId) {
                        item.IsSelected = true;
                    }
                });
            });
        };

        /** @description retrive conduct by id for editing**/

        getConductById = (id: number): void => {

            this.ConductSvc.getConduct(id).then((result) => {
                this.Conduct = result;
                this.getEmployeeById(this.Conduct.OrgUserId);
                this.updateConductTypes();

                if (this.Conduct.ConductAttachments.length) {
                    var uId = this.Conduct.ConductAttachments[0].UniqueId.toString();
                    if (this.Conduct.ConductAttachments[0].AttachmentType == 'Pdf') {
                        this.AttachmentName = 'Pdf File attached';
                    } else {
                        this.AttachmentName = 'Document attached';
                    }

                }
            }, this.onFailure);
        }

        /** @description Create/update a new Conduct. */

        saveConduct = ($event): void => {
            if ($event) $event.preventDefault();
            this.Conduct.DateOfOccurrence = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.Conduct.DateOfOccurrence.toString());
            if (this.Conduct.ConductTypes.length == 0) {
                bootbox.alert('Please select appropriate Conduct Type');
            } else {
                if (this.conductId > 0) {
                    // PUT
                    this.ConductSvc.updateConduct(this.conductId, this.Conduct).then(this.onSuccess, this.onFailure);

                } else {
                    // POST
                    var params = {
                        employeeId: this.Conduct.OrgUserId,
                        kpiId: Common.Enums.KPITYPE.Conduct

                    };
                    this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                        this.Conduct.IsExempt = false;
                        if (result.IsExempt != null) {
                            this.Conduct.IsExempt = result.IsExempt;
                        }
                        this.ConductSvc.saveConduct(this.Conduct).then(this.onSuccess, this.onFailure);
                    }, this.onFailure);
                }
            }
        }

        /**
      * @description Returns an employee by id.
      */
        private getEmployeeById = (empId: number): void => {
            this.employee = {};
            angular.forEach(this.employeesWithAttachment,(employee) => {
                if (employee.EmployeeId === empId) {
                    this.employee = employee;
                    this.Conduct.OrgUserId = empId;
                }
            });
        }

        /**
        * @description Returns an employee by id.
        */
        private getEmployeesList = (): void => {
            this.employees = [];

            angular.forEach(this.employeesWithAttachment,(employee) => {
                this.employees.push(<IEmployee>{
                    EmployeeId: employee.EmployeeId,
                    EmployeeName: employee.Name
                });
            });
        }

        /**
       * @description Returns a list of employees.
       */
        private getEmployeeListWithAttachment = (): void => {

            this.employeesWithAttachment = [];

            //get data from service
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {

                angular.forEach(employees,(employee) => {
                    this.employeesWithAttachment.push(<personnelInterfaces.IEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
                this.getEmployeesList();
                if (this.conductId == 0) {
                    this.showEmployees();
                }
                if (this.conductId > 0) {
                    this.getConductById(this.conductId);
                }
            });
        };

        /**
        * @description Helper to format filename before upload.
        * @param {string} fileName
        */
        getRandToken = () => {
            return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
        }

        /**
         * CONDUCT ATTACHMENT
         * @description Handler when a file is selected.
         */
        addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <personnelInterfaces.IConductAttachment>{
                UniqueId: null,
                AttachmentType: 'Image',
                ConductAttachmentType: commonInterfaces.CONDUCT_ATTACHMENT_TYPE[commonInterfaces.CONDUCT_ATTACHMENT_TYPE.Misc],
                ConductAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.AttachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.Conduct.ConductAttachments.length) {
                            this.Conduct.ConductAttachments[0] = tempAttachment;
                        } else {
                            this.Conduct.ConductAttachments.push(tempAttachment);
                        }

                    },(error: any) => {
                        //file.class = 'error';
                    },(progress: any) => {
                        //file.progress = progress;
                    });

                });

            }
        }

        /** @description Redirect to entry view page. */
        redirectToEntryView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Conduct/EntryView/' + this.conductId);
        }

        /** @description Redirect to details view page. */
        redirectToDetailsView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Conduct/Details/');
        }

        /**
     * @description Open the modal instance to show the employees avg score by area
     */
        showEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.siteSettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.ConductIssueEmployeesCtrl as vm',
                size: 'md',
                //backdrop: 'static',
                resolve: {
                    employee: () => { return this.employeesWithAttachment },
                    employeeId: () => { return this.employeeId }
                }
            }).result.then(returned => {
                if (returned)
                    this.getEmployeeById(returned);
                else
                    this.redirectToDetailsView();
            },() => {
                    this.redirectToDetailsView();
                });


        }
    }

    class ConductIssueEmployeesCtrl implements personnelInterfaces.IPersonnelEmployeesModalCtrl {
        basePath: string;
        modalTitle: string = 'SELECT EMPLOYEE';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        defaultImg = "~/Content/img/user.jpg";
        btnLabel = "RUN CONDUCT";
        static $inject = ['$scope', 'sitesettings', '$uibModalInstance', 'employee', 'employeeId'];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee: any[],
            private employeeId: number) {
            this.modalTitle = this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.body.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.footer.tmpl.html';
        }

        /**
        * @description Dismiss the modal instance.
        */
        cancel = (): void=> {
            this.$uibModalInstance.close(this.employeeId);
        };

        run = (empId: number): void => {
            this.employeeId = empId;
            this.$uibModalInstance.close(this.employeeId);

        }

    }


    angular.module('app').controller('Personnel.ConductIssueEntryCtrl', ConductIssueEntryCtrl);
    angular.module('app').controller('Personnel.ConductIssueEmployeesCtrl', ConductIssueEmployeesCtrl);

}