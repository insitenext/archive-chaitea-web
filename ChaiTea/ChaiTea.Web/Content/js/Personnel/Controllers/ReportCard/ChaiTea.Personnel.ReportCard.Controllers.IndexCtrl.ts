﻿ /// <reference path="../../../_libs.ts" />


module ChaiTea.Personnel.ReportCard.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonServices = ChaiTea.Common.Services;

    enum FileType { Undefined, ProfilePicture };

    export interface ISEFormOptions {
        name: string;
        displayOptions: {
            iconClass: string;
            descriptionText: string
        };
        formOptions: {
            passFail: boolean;
            comment: string;
            failWithOptions: any;
        };


    }

    interface IKpiComment {
        EmployeeKpiMasterId: number;
        Year: number;
        Month: number;
        Comment: string;
        EmployeeId: number;
        IsPass: boolean;
    }

    interface IKpiParams {
        year: number;
        month: number;
        employeeId: number;
    }

    interface ISEEntryScope extends ng.IScope {
        reportForm: ng.IFormController
    }

    class ReportCardIndexCtrl implements Common.Interfaces.INgController {
        reportCardItems: Array<any> = [];
        basePath;

        empId: number;

        employee: trainingInterfaces.ISETrainingEmployee;
        complaints: serviceInterfaces.IComplaint;
        auditScores;
        reportForm: ng.IFormController;
        endDate: Date;
        dateRange: { year: number; month: number; endDate: Date } = {
            year: moment().year(),
            month: moment().month() + 1,
            endDate: new Date()
        }

        isEntryDisabled: boolean = false;

        sepMonthlyEditCuttoffDay: number = 5;
        showAuditScores: boolean = false;
        modalInstance;
        floorName: string;

        kpiItem;
        safetyClaims = [];
        attendanceIssues = [];
        conductWarnings = [];
        ownershipWorkOrders = [];
        professionalismAudit = {};
        monthlyBonus: number = 0;
        yearlyBonus: number = 0;
        siteBonusForMonth: number = 0;

        overallComment: string;
        shouldToggle = [];
        isFail: boolean = false;

        start;
        end;

        static $inject = [
            '$scope',
            '$uibModal',
            'userContext',
            'chaitea.personnel.services.reportcard',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.printer',
            'chaitea.common.services.apibasesvc',
            'sitesettings',
            'userInfo',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.imagesvc'];

        constructor(private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private reportCardSvc: Services.IReportCardSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private dateFormatterSvc: Core.Services.IDateFormatterSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private printerSvc: ChaiTea.Core.Services.IPrinter,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private localDataStoreSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
            ) {
            
            this.basePath = sitesettings.basePath;
            this.empId = userInfo.userId;
           
        }

        public initialize = (sepMonthlyEditCuttoffDay: number = 5) => {
            this.sepMonthlyEditCuttoffDay = sepMonthlyEditCuttoffDay;
            //Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate)
            }
           
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.showAuditScores = false;
                this.isEntryDisabled = false;

                this.start = this.dateFormatterSvc.formatDateFull(moment(obj.endDate).startOf('month'));
                this.end = this.dateFormatterSvc.formatDateFull(moment(obj.endDate).endOf('day'));

                this.dateRange.month = moment(obj.endDate).month() + 1;
                this.dateRange.year = moment(obj.endDate).year();
                this.getEmployeeKpi(this.empId);

                this.enableEntryDateCheck();

                this.getEmployeeComplaints(this.empId);
                var start = new Date(this.dateRange.year, this.dateRange.month - 1, 1);

                var end = new Date(this.dateRange.year, this.dateRange.month, 0);
                this.dateRange.endDate = end;

                this.getSafetyClaims(this.empId);
                this.getAttendanceIssues(this.empId);
                this.getConductWarnings(this.empId);
                this.getProfessionalismAudit(this.empId);
                this.getEmployeeBonus(this.empId);
                this.getOwnership(this.empId);

                this.getEmployeeAuditScores({
                    startDate: this.dateFormatterSvc.formatDateShort(start),
                    endDate: this.dateFormatterSvc.formatDateShort(end),
                    employeeId: this.empId
                });

                this.getOverallComment({
                    year: this.dateRange.year,
                    month: this.dateRange.month,
                    employeeId: this.empId
                });
            });

        }

        /**
       * @description Get yearly and monthly bonus for an emp,loyee.
       */
        private getEmployeeBonus = (empId: number) => {

            this.reportCardSvc.getSiteBonus({ month: this.dateRange.month, year: this.dateRange.year }).then((data): any => {
                if (data.length) {
                    this.siteBonusForMonth = data[0].BonusAmount ? data[0].BonusAmount : 0;
                }
            });

            var paramsForEmp = {
                year: this.dateRange.year,
                month: this.dateRange.month
            }

            this.reportCardSvc.getMonthlyBonusOfEmployee(paramsForEmp).then((result) => {
                if (result)
                    this.monthlyBonus = result.BonusAmount ? result.BonusAmount : 0;
            }, this.onFailure);

            var params = {
                employeeId: empId,
                year: this.dateRange.year,
                month: this.dateRange.month
            }
            this.reportCardSvc.getYearlyBonusOfEmployee(params).then((result) => {
                this.yearlyBonus = result.YearlyBonus;
            }, this.onFailure);
        }

        /**
      * @description Get safety claims for an emp,loyee.
      */
        private getSafetyClaims = (empId: number) => {
            var params: any = {
                $orderby: 'DateOfIncident desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfIncident gt DateTime'${this.start}' and DateOfIncident lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Personnel/Safety/Employee").then((result) => {
                this.safetyClaims = result;
            }, this.onFailure);
        }

        /**
     * @description Get Ownership for an employee.
     */
        private getOwnership = (empId: number) => {
            var params: any = {
                $filter: `CreateDate gt DateTime'${this.start}' and CreateDate lt DateTime'${this.end}' and EmployeeId eq ${empId} and Status eq '1'`
            };
            this.apiSvc.getByOdata(params, "Ownership").then((result) => {
                this.ownershipWorkOrders = result;
            }, this.onFailure);
        }

        /**
       * @description Get attendance issues for an emp,loyee.
       */
        private getAttendanceIssues = (empId: number) => {
            var params: any = {
                $orderby: 'DateOfOccurrence desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfOccurrence gt DateTime'${this.start}' and DateOfOccurrence lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Issue/Employee").then((result) => {
                this.attendanceIssues = result;
            }, this.onFailure);
        }

        /**
       * @description Get conduct warnings for an emp,loyee.
       */
        private getConductWarnings = (empId: number) => {
            var params: any = {
                $orderby: 'DateOfOccurrence desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfOccurrence gt DateTime'${this.start }' and DateOfOccurrence lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Conduct/Employee").then((result) => {
                this.conductWarnings = result;
            }, this.onFailure);
        }

        /**
       * @description Get safety claims for an emp,loyee.
       */
        private getProfessionalismAudit = (empId: number) => {

            var params: any = {
                $orderby: 'UpdateDate desc',
                $top: 20,
                $skip: 0,
                $filter: `EmployeeId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "EmployeeAudit/Employee").then((result) => {
                this.professionalismAudit = result[0];
            }, this.onFailure);
        }

        private getEmployeeComplaints = (empId: number) => {

            this.floorName

            var params: any = {
                $orderby: 'CreateDate desc',
                $top: 20,
                $skip: 0,
                $filter: `FeedbackDate gt DateTime'${ this.start }' and FeedbackDate lt DateTime'${ this.end }' and AccountableEmployees/any(d:d eq ${empId})`
            };

            this.apiSvc.getByOdata(params, 'Complaints').then((res: serviceInterfaces.IComplaint) => {
                this.complaints = res;

            }, this.onFailure);

        }

        private getEmployeeKpi = (empId: number) => {
            if (!empId) return false;

            this.reportCardSvc.getEmployeeKpi(this.dateRange.year, this.dateRange.month, empId).then((employee) => {
                this.processEmployeeDataIntoView(employee);
            },this.onFailure);
        }


        /**
       * @description Get Audit Scores for an emp,loyee.
       */
        private getEmployeeAuditScores = (params: qualityInterfaces.IMyAuditsParams) => {
            this.apiSvc.query(params, "Report/EmployeeAuditsAvgByAreaByEmployee/:startDate/:endDate").then((result) => {
                this.auditScores = result;
                angular.forEach(result,(item) => {
                    if (item.AverageScoringProfileScore) {
                        this.showAuditScores = true;
                    }
                });
            });
        }

        /**
       * @description Get the overall comment for a particular month.
       */
        private getOverallComment = (params: IKpiParams) => {
            this.reportCardSvc.getKpiComment(params).then((result) => {
                this.kpiItem = result[0];
            });
        }



        public printScoreCard = ($event) => {
            var date = moment(this.dateRange.month.toString(), this.dateRange.year.toString());
            var employeeCopy = <any>angular.copy(this.employee);

            //TODO TEMP hide attendance and quality audits
            employeeCopy.EmployeeKpis[3].Kpi.Summary = "";
            employeeCopy.EmployeeKpis[5].Kpi.Summary = "";
            employeeCopy.evaluationStartDate = date.startOf('month').toDate();
            employeeCopy.evaluationEndDate = date.endOf('month').toDate();
            employeeCopy.site = this.userContext.Site;
            employeeCopy.bonusAmount = " ";

            var printObj = {
                scoreCardItems: [employeeCopy]
            }

            this.printerSvc.print(this.basePath + 'Content/js/Training/Templates/print.sep.scorecard.html', printObj);
        }


        private processEmployeeDataIntoView = (employee) => {
            this.employee = employee;
            this.hasData();
            this.employee.HireDate = (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A');

            //set false kpi blocks to open
            _.forEach(this.employee.EmployeeKpis,(kpi) => {
                kpi["showToggleBlock"] = false
            });
           
            // image
            this.employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
        }

        onFailure = (response: any) => {
            this.isFail = true;
        };


        /**
         * @description Logic to determine if the current month is selected or if the previous month is selected then the current day cannot be greater than the 5th
         */
        private enableEntryDateCheck = () => {
            var now: Date = new Date();
            var currentMonth = now.getMonth() + 1;
            var previousMonth = currentMonth - 1;
            var previousCutoffDate = this.sepMonthlyEditCuttoffDay;
            var kpiMonth = this.dateRange.month;

            this.isEntryDisabled = false;

            if ((kpiMonth == previousMonth && now.getDate() <= previousCutoffDate)) {
                this.isEntryDisabled = false
            } else if (kpiMonth != currentMonth ||
                this.dateRange.year != now.getFullYear()) {
                this.isEntryDisabled = true;
            }

        }

        //#region form stuff
        public toggleCheckBox = (kpi, item) => {
            if (this.isEntryDisabled) return false;

            if (_.contains(kpi.CheckBoxSelections, item.Key)) {
                kpi.CheckBoxSelections.slice(item.Key, 1);
                _.remove(kpi.CheckBoxSelections,(num) => (num == item.Key))
            } else {
                kpi.CheckBoxSelections.push(item.Key);
            }
        }

        public hasData = () => {
            var setVal: boolean = false;
            this.shouldToggle.splice(0, this.shouldToggle.length);
            angular.forEach(this.employee.EmployeeKpis,(kpiItem) => {
                setVal = false;
                angular.forEach(kpiItem.MetaData,(metadataItem) => {
                    if (metadataItem.Label == 'Complaints' || metadataItem.Label == 'Score') {
                        if (metadataItem.Value > 0 && metadataItem.Value != 'N/A')
                            setVal = true;
                    }
                    else if ((kpiItem.Kpi.Name == 'Attendance' && metadataItem.Label == 'Occurrences') || (kpiItem.Kpi.Name == 'Safety' && metadataItem.Label == 'Claims')
                        || (kpiItem.Kpi.Name == 'Conduct' && metadataItem.Label == 'Warnings') || (kpiItem.Kpi.Name == 'Ownership' && metadataItem.Label == 'Created')) {
                        if (metadataItem.Value != '0')
                            setVal = true;
                    }
                });

                if (kpiItem.Kpi.Name == 'Professionalism' && kpiItem.Kpi.Summary != "No audits done")
                    setVal = true;

                this.shouldToggle.push(setVal);
            });
        }


        /*
         * @description used for validation of many checkboxes
         */
        public someSelected = (object) => {
            return Object.keys(object).some(function (key) {
                return object[key].Selected;
            });
        }
        //#endregion


        public loadComplaintModal = (complaint: serviceInterfaces.IComplaint, $e) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.ComplaintModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    complaint: () => { return complaint; }
                }
            });

        }

        public loadProfessionalismAuditDetailsModal = (professionalismAuditId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.ProfessionalismAuditsDetailsModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    professionalismAuditId: () => { return professionalismAuditId; }
                }
            });

        }

        public loadSafetyIssueDetailsModal = (safetyId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.SafetyIssueDetailsModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    safetyId: () => { return safetyId; }
                }
            });

        }

        public loadConductWarningDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.ConductWarningDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    conductId: () => { return id; }
                }
            });

        }

        public loadAttendanceIssueDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.AttendanceIssueDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    attendanceIssueId: () => { return id; }
                }
            });

        }

      
        public loadOwnershipDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.OwnershipDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    ownershipId: () => { return id; }
                }
            });

        }

    }
    angular.module("app").controller('Personnel.ReportCardIndexCtrl', ReportCardIndexCtrl);



    class ComplaintModalCtrl {

        basePath: string;
        modalTitle: string = 'Complaints';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        floorName: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'title',
            'complaint'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private title: string,
            private complaint: serviceInterfaces.IComplaint) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Quality/quality.complaint.entry.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Quality/quality.complaint.footer.tmpl.html';

            if (complaint.FloorId > 0) {
                this.apiSvc.getById(complaint.FloorId, 'Floor').then((result) => {
                    this.floorName = result.Name;
                });
            }

        }
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


        /**
         * @description Dismiss the modal instance.
         */
        public goToComplaintDetail = ($e): void => {
            this.$uibModalInstance.dismiss('cancel');
            //Quality / Complaint / EntryView / 1012
            window.location.replace(this.basePath + 'Quality/Complaint/EntryView/' + this.complaint.ComplaintId);
        }
    }
    angular.module("app").controller('Personnel.ComplaintModalCtrl', ComplaintModalCtrl);

    class SafetyIssueDetailsModalCtrl {

        basePath: string;

        safety = {};

        employeeWithImage = {};
        //incidentType = {};
        downloadUrl: string = '';

        modalTitle: string = 'Safety Issue Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'safetyId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private safetyId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getSafetyById(safetyId);

        }

        getSafetyById = (safetyId: number): void=> {
            if (safetyId > 0) {
                this.apiSvc.getById(safetyId, "Personnel/Safety/Employee").then((result) => {
                    this.safety = result;
                    this.downloadUrl = (result.SafetyAttachments.length ? this.awsSvc.getUrlByAttachment(result.SafetyAttachments[0].UniqueId, result.SafetyAttachments[0].AttachmentType) : '');

                }, this.onFailure);
            }
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Personnel.SafetyIssueDetailsModalCtrl', SafetyIssueDetailsModalCtrl);

    class ConductWarningDetailsModal {

        basePath: string;

        conduct = {};
        //conductTypes = {};

        employeeWithImage = {};
        downloadUrl: string = '';

        modalTitle: string = 'Conduct Warning Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'conductId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private conductId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.conduct.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getConductById(conductId);

        }

        getConductById = (conductId: number): void=> {
            if (conductId > 0) {
                this.apiSvc.getById(conductId, "Conduct/Employee").then((result) => {
                    this.conduct = result;
                    this.downloadUrl = (result.ConductAttachments.length ? this.awsSvc.getUrlByAttachment(result.SafetyAttachments[0].UniqueId, result.SafetyAttachments[0].AttachmentType) : '');

                }, this.onFailure);
            }
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }

         /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Personnel.ConductWarningDetailsModal', ConductWarningDetailsModal);


    class AttendanceIssueDetailsModal {

        basePath: string;

        attendance = {};

        employeeWithImage = {};
        downloadUrl: string = '';

        modalTitle: string = 'Attendance Issue Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'attendanceIssueId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private attendanceIssueId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.attendance.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getAttendanceById(attendanceIssueId);

        }

        getAttendanceById = (attendanceIssueId: number): void=> {
            if (attendanceIssueId > 0) {
                this.apiSvc.getById(attendanceIssueId, "Issue/Employee").then((result) => {
                    this.attendance = result;
                }, this.onFailure);
            }
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Personnel.AttendanceIssueDetailsModal', AttendanceIssueDetailsModal);

    class ProfessionalismAuditsDetailsModalCtrl {

        basePath: string;

        audit: any = {};
        auditId: number = 0;

        employeeWithImage = {};

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        downloadUrl: string = '';

        modalTitle: string = 'Professionalism Audit Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'professionalismAuditId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private professionalismAuditId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.professionalismAudit.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getAudit(professionalismAuditId);

        }

        private getAudit = (auditId) => {
            this.apiSvc.getById(auditId, 'EmployeeAudit/Employee').then((audit) => {
                var audit = audit;
                audit.items = {};
                audit.items.appearance = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 1 } });
                audit.items.attitude = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 2 } });
                audit.items.equipment = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 3 } });
                audit.items.responsiveness = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 4 } });

                if (_.any(audit.items,(item) => (item == false))) {
                    audit.IsPass = false;
                }

                this.audit = audit;
            })
            return this.audit;
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }
       
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Personnel.ProfessionalismAuditsDetailsModalCtrl', ProfessionalismAuditsDetailsModalCtrl);


    class OwnershipDetailsModal {

        basePath: string;

        ownership = {};



        downloadUrl: string = '';

        modalTitle: string = 'Report It Created';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'ownershipId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private ownershipId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.ownership.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getOwnershipById(ownershipId);

        }

        getOwnershipById = (ownershipId: number): void=> {
            if (ownershipId > 0) {
                this.apiSvc.getById(ownershipId, "Ownership").then((result) => {
                    this.ownership = result;

                }, this.onFailure);
            }
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }

       
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Training.OwnershipDetailsModal', OwnershipDetailsModal);
}