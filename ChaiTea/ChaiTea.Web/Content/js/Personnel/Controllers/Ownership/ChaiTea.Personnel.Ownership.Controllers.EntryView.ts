﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Ownership.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonInterfaces = Common.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class OwnershipEntryViewModalCtrl {
        ownership: ChaiTea.Personnel.Interfaces.IOwnership;
        ownershipId: number = 0;
        status: string = '';
        employeeWithImage = {};
        basePath: string = '';
        ObjectUrl: string = '';
        programName: string = '';
        modalInstance;

        employeesWithImage = [];
        moduleIcon: string;
        moduleClass: string;
        customerRepresentativeTemplate: string;
        modalLeftSection: string;
        modalRightSection: string;
        selectedEmployeeText = "ASSIGNED_EMPLOYEES";
        isCritical: boolean = true;
        zoomScale: number;
        attachmentType = commonInterfaces.ATTACHMENT_TYPE;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.awssvc',
            '$uibModalInstance',
            'blockUI',
            'id',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private blockUI,
            private id: number,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {
            this.ownershipId = id;
            this.moduleClass = 'reportit';
            this.moduleIcon = 'proactive';
            this.basePath = sitesettings.basePath;
            this.zoomScale = sitesettings.windowSizes.isPhone ? 1 : 0.5;
            this.customerRepresentativeTemplate = this.basePath + "Content/js/Personnel/Templates/Ownership/manage-customer-representatives-modal-tmpl.html";
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.ownership.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.ownership.rightsection.tmpl.html';

            this.initialize();
        }

        initialize = (): void=> {
            this.getOwnershipById(this.ownershipId);
            this.programName = this.userContext.Program.Name;
        }

        private getOwnershipById = (ownershipId: number): void=> {
            if (ownershipId > 0) {
                this.blockUI.start('Loading...');
                this.apiSvc.getById(ownershipId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Ownership]).then((result) => {
                    this.ownership = result;
                    if (result.Status == null)
                        this.status = 'Pending';
                    else if (result.Status == 1)
                        this.status = 'Accepted';
                    else
                        this.status = 'Rejected';
                    result.CreateDate = this.dateSvc.getFromUtcDatetimeOffset(result.CreateDate, 'MM/DD/YYYY h:mm A');
                    this.ObjectUrl = this.getAttachment(result.OwnershipAttachments);
                    this.getEmployeeWithImage(result.EmployeeId).then(() => {
                        this.employeesWithImage.push(this.employeeWithImage);
                    });
                    if (this.blockUI) {
                        this.blockUI.stop();
                    }
                }, this.onFailure);
            }
        }

        edit = (): void => {
            window.location.replace(this.basePath + "Personnel/Ownership/Entry/" + this.ownershipId);
        }

        public close = (): void => {
                this.$uibModalInstance.dismiss('cancel');
        };

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private getEmployeeWithImage = (id: number): any => {

            return this.apiSvc.getById(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                this.employeeWithImage = <personnelInterfaces.IEmployee>{
                    Name: employee.Name,
                    EmployeeId: employee.EmployeeId,
                    UserAttachments: employee.UserAttachments,
                    HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                    JobDescription: employee.JobDescription.Name,
                    ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                }
            });
        }

        private getAttachment = (attachment: any): string => {
            var img: string;
            if (attachment.length > 0)
                img = this.awsSvc.getUrlByAttachment(attachment[0].UniqueId, attachment[0].AttachmentType);
            
            return img;
        }
        
        public accept = () => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Personnel/Templates/Ownership/accept-modal-tmpl.html',
                controller: 'Personnel.OwnershipAcceptPopup as vm',
                size: 'md',
                resolve: {
                    employee: () => { return this.ownership; }
                }
            });
        }

        public reject = () => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Personnel/Templates/Ownership/reject-modal-tmpl.html',
                controller: 'Personnel.OwnershipRejectPopup as vm',
                size: 'md',
                resolve: {
                    employee: () => { return this.ownership; }
                }
            });
        }

        public viewImage = (ownership: ChaiTea.Personnel.Interfaces.IOwnership, $event) => {

            $event.preventDefault();
            $event.stopPropagation();

            // Open the photo manage dialog
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'manageViewPhotoUploads.tmpl.html',
                controller: 'Personnel.OwnershipViewImageModalCtrl as vm',
                size: 'md',
                resolve: {
                    ownership: () => { return ownership; }
                }
            });

            this.modalInstance.result.then((attachment: ChaiTea.Personnel.Interfaces.IOwnershipAttachment) => {

                this.ownership.OwnershipAttachments[0].UniqueId = attachment.UniqueId;
                this.ObjectUrl = this.getAttachment(this.ownership.OwnershipAttachments);

            }, () => { });

        }

    }

    class OwnershipViewImageModalCtrl {
        currentImage: any;
        imageUniqueId: string;

        imageCss: any = {
            "-ms-transform": "rotate(0deg)",
            "-webkit-transform": "rotate(0deg)",
            "transform": "rotate(0deg)"
        };
        wrapperCss: any = { "display": "block" };

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'blockUI',
            'ownership',
            'localStorageService',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private blockUI,
            private ownership: ChaiTea.Personnel.Interfaces.IOwnership,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.currentImage = ownership.OwnershipAttachments[0];

        }

        /**
         * @description Set active image for view.
         */
        public rotateImage = (photo: any) => {

            this.blockUI.start();

            //fire off reuqest to actually rotate the image on s3
            var key = photo.UniqueId.replace('.', '_');
            this.apiSvc.query({}, 'attachment/' + photo.AttachmentId + '/rotate/90/key/' + key, false, false).then((res: any) => {
                this.blockUI.stop();
                this.currentImage.UniqueId = res.UniqueId;
            });

        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel = (): void => {
            this.$uibModalInstance.close(this.currentImage);
        };

    }
    
    angular.module('app').controller("Personnel.OwnershipViewImageModalCtrl", OwnershipViewImageModalCtrl);
    angular.module("app").controller("Personnel.OwnershipEntryViewModalCtrl", OwnershipEntryViewModalCtrl);
}