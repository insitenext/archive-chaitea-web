﻿/// <reference path="../../../_libs.ts" />

 
module ChaiTea.Personnel.Ownership.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    interface IOwnershipAttachment {
        AttachmentType: string;
        Description: string;
        Name: string;
        OwnershipAttachmentId: number;
        OwnershipAttachmentType: number;
        OwnershipAttachmentTypeId: number;
        UniqueId: string;
    }

    interface IOwnership {
        OwnershipId: number;
        BuildingId: number;
        BuildingName: string;
        Classifications: Array<any>;
        CustomerRepresentatives: Array<any>;
        FloorId: number;
        FloorName: string;
        Description: string;
        EmployeeId: number;
        EmployeeName: string;
        IsCritical: boolean;
        CreateDate: Date;        
        ReportTypeId: number;
        ReportTypeName: string;
        RejectionTypeId: number;
        RejectionTypeName: string;
        Status: any;
        OwnershipAttachment: Array<IOwnershipAttachment>;
        ObjectUrl: string;
        IsCorrectiveActionTaken?: boolean;
        CorrectiveActionDescription: string;        
    }

    enum Filters { Pending, Accepted, Rejected, Good_Catch };

    class OwnershipDetailsCtrl {

        siteId: number = 0;
        basePath: string;
        showFilter: boolean = false;
        isEmployee: boolean = false;
        isCustomer: boolean = false;
        isManager: boolean = false;

        siteName: string;
        programName: string;

        countFilter: number;
        ownershipTypes = [];
        ownership = [];
        ownershipObj = [];
        acceptedOwnerships = [];
        employeesHavingOwnership = [];
        employees = [];
        filterEmployeeId: number = 0;
        filterOwnershipTypeId: number = 0;

        modalInstance;
        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        workOrders = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };

        counts = {
                pending: 0,
                accepted: 0,
                rejected: 0,
                goodCatch: 0
        }

        isCardView: boolean = true;
        module: string = 'ownership';
        viewLink: string = '';
        headerText: string = 'REPORTED_ON';
        clientName: string = '';

        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.conductsvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.utilssvc',
            '$uibModal',
            'chaitea.common.services.awssvc',
            'chaitea.personnel.services.reportssvc',
            'chaitea.common.services.imagesvc',
            'blockUI',
            Common.Services.DateSvc.id];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private conductSvc: personnelSvc.IConductSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private chartsSvc: personnelSvc.IPersonnelReportsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private blockUI,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            this.clientName = userContext.Client.Name;
            if (this.userInfo.mainRoles.customers)
                this.isCustomer = true;
            if (this.userInfo.mainRoles.managers)
                this.isManager = true;
            
            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            if (this.userInfo.mainRoles.customers)
                this.countFilter = Filters.Accepted;
            else
                this.countFilter = Filters.Pending;

            if (!this.userInfo.mainRoles.managers && !this.userInfo.mainRoles.customers)
                this.isEmployee = true;
            else if (this.userInfo.mainRoles.managers)
                this.isEmployee = false;



            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                this.getOwnershipCounts();
                // Reset the paging
                this.getOwnershipReset({
                    startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
                });
            });

        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.programName = this.userContext.Program.Name;
            this.getOwnershipTypes();

            this.tabFilterItems = [
                {
                    title: 'Pending',
                    onClickFn: () => { this.changeCountFilter(null,0) },
                    count: 0
                },
                {
                    title: 'Accepted',
                    onClickFn: () => { this.changeCountFilter(null,1) },
                    count: 0
                },
                {
                    title: 'Rejected',
                    onClickFn: () => { this.changeCountFilter(null,2) },
                    count: 0
                },
                {
                    title: 'Good Catch',
                    onClickFn: () => { this.changeCountFilter(null,3) },
                    count: 0
                }
            ];

            this.$scope.$watch(() => (this.counts), (newVal: any, oldVal: any) => {
                if (newVal === oldVal) return false;

                this.tabFilterItems[0].count = this.counts.pending;
                this.tabFilterItems[1].count = this.counts.accepted;
                this.tabFilterItems[2].count = this.counts.rejected;
                this.tabFilterItems[3].count = this.counts.goodCatch;

            }, true);
        }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Personnel.OwnershipEntryViewModalCtrl as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            });
        }

        getOwnershipReset = (dateRange: commonInterfaces.IDateRange): void => {
            this.isBusy = false;
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.ownership = [];
            this.ownershipObj = [];
            this.getOwnerships(dateRange);
        }

        private getOwnershipCounts = (): void => {
            
            this.chartsSvc.getOwnershipsCount({
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                isEmployee: this.isEmployee
            }).then((result) => {
                this.counts.pending  = result.Pending;
                this.counts.accepted = result.Accepted;
                this.counts.rejected = result.Rejected;
                this.counts.goodCatch = result.GoodCatch;
               
            });
        }
        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        public changeCountFilter = ($e, filter): void => {

            this.countFilter = filter;
            this.getOwnershipReset({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }

        /**
         * @description Get ownershipts and push to array for infinite scroll.
         */
        getOwnerships = (dateRange: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Personnel/Ownership/EntryView/';
            this.siteName = this.userContext.Site.Name;
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };

            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            this.blockUI.start('Loading...');
            var params = this.getPagingParams(dateRange);

            this.apiSvc.getByOdata(params,'Ownership')
                .then((data) => {
                    if (data.length === 0 ) {
                        this.isAllLoaded = true;
                    }
                    angular.forEach(data, (obj) => {
                        if (obj.ReportedByAttachments) {
                            obj._createByProfilePicture = this.imageSvc.getUserImageFromAttachments(obj.ReportedByAttachments, true);
                        }

                        obj.CreateDate = this.dateSvc.getFromUtcDatetimeOffset(obj.CreateDate, 'MM/DD/YYYY h:mm A');
                        obj.ObjectUrl = this.getAttachment(obj.OwnershipAttachments);
                        this.ownership.push(obj);
                    });
                if (this.blockUI) {
                    this.blockUI.stop();
                }
                this.isBusy = false;
                this.recordsToSkip += this.top;
                var ownershipCopy = angular.copy(this.ownership);
                
                if (this.isCustomer)
                    this.countFilter = Filters.Accepted;

            }, this.onFailure);
        }

        getAttachment = (attachment: any): string => {
            var img: string;
            if (attachment.length == 0)
                img = this.basePath + "Content/img/no-photos.png";
            else {
                img = this.awsSvc.getUrlByAttachment(attachment[0].UniqueId, attachment[0].AttachmentType);
            }
            return img;
        }

        goToLink = (id: any) => {
            window.location.href = this.basePath + 'Personnel/Ownership/EntryView/' + id.OwnershipId;
        }

        /**
        * @description Return a paging param for the OData filter.
        */
        getPagingParams = (dateRange: commonInterfaces.IDateRange) => {
            var EndDate = moment(dateRange.endDate).endOf('day').format();
            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(dateRange.startDate);
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(EndDate);
            if (!this.userInfo.mainRoles.managers && !this.userInfo.mainRoles.customers)
                this.isEmployee = true;
            else if (this.userInfo.mainRoles.managers)
                this.isEmployee = false;
            


            var params = <personnelSvc.IODataPagingParamModel>{
                $orderby: 'CreateDate desc',
                $top: this.top,
                $skip: this.recordsToSkip
            };
            if (this.countFilter == Filters.Pending) {
                params['$filter'] = `CreateDate gt DateTime'${startDate}' and CreateDate le DateTime'${endDate}' and Status eq null`;
            }
            else if (this.countFilter == Filters.Accepted) {
                params['$filter'] = `CreateDate gt DateTime'${startDate}' and CreateDate le DateTime'${endDate}' and Status eq '1'`;
            }
            else if (this.countFilter == Filters.Rejected) {
                params['$filter'] = `CreateDate gt DateTime'${startDate}' and CreateDate le DateTime'${endDate}' and Status eq '0'`;
            }
            else if (this.countFilter == Filters.Good_Catch) {
                params['$filter'] = `CreateDate gt DateTime'${startDate}' and CreateDate le DateTime'${endDate}' and Status eq '1'
                                    and Classifications/any(c:c/Name eq 'Good Catch')`;
            }

            if (this.isEmployee)
                params['$filter'] += ` and EmployeeId eq ${this.userInfo.userId}  `; 


            if (this.filterEmployeeId > 0) {
                var filter = '';

                filter = `EmployeeId eq ${this.filterEmployeeId}`;

                if (!params['$filter'])
                    params['$filter'] = `${filter}`;
                else
                    params['$filter'] += `and ${filter}`;
            }

            if (this.filterOwnershipTypeId > 0) {
                var filter = '';

                filter = `OwnershipTypes/any(ownershipType: ownershipType/OwnershipTypeId eq ${this.filterOwnershipTypeId})`;
                if(!params['$filter'])
                    params['$filter'] = `${filter}`;
                else
                    params['$filter'] += `and ${filter}`;
            }

            return params;
        }

        /**
     * @description Returns a list of employees.
     */
        private getEmployeeListWithAttachment = (): void => {

            this.employeesHavingOwnership = [];

            //get data from service
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {

                angular.forEach(employees,(employee) => {
                    this.employeesHavingOwnership.push(<personnelInterfaces.IEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
                this.getEmployeesList();
            });
        }


        /**
        * @description Returns an employee by id.
        */
        private getEmployeesList = (): void => {
            this.employees = [];

            angular.forEach(this.employeesHavingOwnership,(employee) => {
                this.employees.push(<IEmployee>{
                    EmployeeId: employee.EmployeeId,
                    EmployeeName: employee.Name
                });
            });
        }

        /**
         * @showing filter
         *
          */
        getFilter = (): void => {
            this.showFilter = (this.showFilter ? false : true);
            this.filterEmployeeId = 0;
            this.filterOwnershipTypeId = 0;
        }

        onEmployeeChange = (empId: number): void => {
            this.filterEmployeeId = empId;
        }

        onOwnershipTypeChange = (ownershipTypeId: number): void => {
            this.filterOwnershipTypeId = ownershipTypeId;
        }

        getOwnershipOnFilter = (): void => {
            this.getOwnerships({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }

        /** @description get attendance issue types and reasons related to types. */
        getOwnershipTypes = (): void => {
            this.serviceLookupListSvc.getOwnershipTypes()
                .then((result) => {
                this.ownershipTypes = result;
                }, this.onFailure);
        }



        cancel = (): void => {
            this.filterEmployeeId = 0;
            this.filterOwnershipTypeId = 0;
            this.getOwnershipOnFilter();
        }

        /**
        * @description to export details to excel sheet 
        */
        export = (): void=> {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var ownerShipList = [];
            this.acceptedOwnerships = [];
            var dateRange =  {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };
            var params = <personnelSvc.IODataPagingParamModel>{
                $orderby: 'CreateDate desc',
                $filter: `CreateDate gt DateTime'${dateRange.startDate}' and CreateDate le DateTime'${dateRange.endDate}' and Status eq '1'`
            };
          

            this.apiSvc.getByOdata(params, 'Ownership').then((data) => {
                this.acceptedOwnerships = data;
                angular.forEach(this.acceptedOwnerships, (ownership) => {

                    ownerShipList.push({
                        "Ownership Id": ownership.OwnershipId,
                        "Employee Id": ownership.EmployeeId,
                        "Employee Name": ownership.EmployeeName,
                        "Status": 'Accepted',
                        "Report Type": ownership.ReportTypeName ? ownership.ReportTypeName : '',
                        "Critical": ownership.IsCritical == 0 ? "No" : "Yes",
                        "Created Date": ownership.CreateDate,
                        "Building": ownership.BuildingName,
                        "Floor": ownership.FloorName,
                        "Description": ownership.Description
                    });

                });
                alasql('SELECT * INTO CSV("ownerShipList.csv",?) FROM ?', [mystyle, ownerShipList]);
            }, this.onFailure);
                
        }

        public accept = (employee,$event) => {
            $event.stopPropagation();
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Personnel/Templates/Ownership/accept-modal-tmpl.html',
                controller: 'Personnel.OwnershipAcceptPopup as vm',
                size: 'md',
                resolve: {
                    employee: () => { return employee; }
                }
            });
        }

        public reject = (employee, $event) => {
            $event.stopPropagation();
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Personnel/Templates/Ownership/reject-modal-tmpl.html',
                controller: 'Personnel.OwnershipRejectPopup as vm',
                size: 'md',
                resolve: {
                    employee: () => { return employee; }
                }
            });
        }

    }

    angular.module('app').controller('Personnel.OwnershipDetailsCtrl', OwnershipDetailsCtrl);

    class OwnershipAcceptPopup {

        isCritical: number = 0;
        ClassificationTypes = [];
        ReportTypes = [];
        CustomerRepresentatives = [];
        modalInstance;
        basePath: string;
        report;
       
        Selected = {
            CustomerRepresentatives: []
        }

        Ownership = {
            OwnershipId: 0,
            Description: '',
            Classifications: [],
            BuildingId: 0,
            FloorId: 0,
            EmployeeId: 0,
            OwnershipAttachments: [],
            ReportTypeId: null, 
            CustomerRepresentatives: [],
            RejectionTypeId: 0,
            Status: 0,
            StatusUpdateById: 0,
            StatusUpdateDate: new Date(),
            IsCritical: 0,
            CreateDate: new Date(),
            IsCorrectiveActionTaken: null,
            CorrectiveActionDescription: '',
            IsExempt: null   
        }

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'employee',
            'chaitea.services.lookuplist',
            'userContext',
            'sitesettings',
            'blockUI',
            '$log',
            'chaitea.common.services.apibasesvc',
            '$q',
            '$uibModal',
            '$timeout',
            'chaitea.common.services.mailsvc'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private $q: angular.IQService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $timeout: angular.ITimeoutService,
            private mailSvc: commonSvc.IMailSvc
            ) {
            this.basePath = sitesettings.basePath;
            this.getClassificationTypes();
            this.getReportTypes();
            this.getCustomerRepresentatives();
        }

        setCriticalType = (id: number) => {
            this.isCritical = id;
            this.Ownership.IsCritical = id;
            if (this.Ownership.IsCritical == 0) {
                this.CustomerRepresentatives = [];
            }
        }

        getClassificationTypes = () => {
            this.serviceLookupListSvc.getOwnershipTypes().then((result) => {
                this.ClassificationTypes = result;
            });
        }

        setClassificationType = (id: number) => {
            var ownershipType = <personnelInterfaces.IOwnershipType>{
                ClassificationId: 0,
                Name: ''
            }
            angular.forEach(this.ClassificationTypes,(item) => {
                if (item.ClassificationId == id)
                    ownershipType = <personnelInterfaces.IOwnershipType>{
                        ClassificationId: item.ClassificationId,
                        Name: item.Name
                    }
            });

            if (!this.Ownership.Classifications.length) {
                this.Ownership.Classifications.push(ownershipType)
            }
            else {
                var index: number = 0;
                var checkFlag: boolean = false;

                angular.forEach(this.Ownership.Classifications,(item) => {

                    if (item.ClassificationId == id) {
                        this.Ownership.Classifications.splice(index, 1);
                        checkFlag = true;
                    }
                    index++;
                });

                if (checkFlag == false) {
                    this.Ownership.Classifications.push(ownershipType);
                }
            }
        }

        getReportTypes = () => {
            this.serviceLookupListSvc.getReportTypes().then((result) => {
                this.ReportTypes = result;
            });
        }

        addNewCustomerRepresentative = ($event) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Common/Templates/modal.addEditCustomerRepresentative.tmpl.html',
                controller: 'Quality.CustomerRepresentativeModalCtrl as vm',
                size: 'md',
                resolve: {
                    customerRepresentatives: () => { return this.CustomerRepresentatives; },
                    customerRepresentative: () => {
                        return <qualityInterfaces.ICustomerRepresentative>{
                            CustomerRepresentativeId: 0,
                            Name: '',
                            Email: '',
                            JobDescription: ''
                        }
                    }
                }
            });

            this.modalInstance.result.then((customerRepresentative: qualityInterfaces.ICustomerRepresentative) => {
                if (customerRepresentative.CustomerRepresentativeId == 0) {
                    this.apiSvc.save(customerRepresentative,'/OwnershipCustomerRepresentative')
                        .then((result) => {
                        customerRepresentative.CustomerRepresentativeId = result.CustomerRepresentativeId;
                        this.CustomerRepresentatives.unshift(customerRepresentative);
                    }, this.onFailure);
                }
            },() => { });
        }

        getCustomerRepresentatives = () => {
            return new this.$q((resolve, reject) => {
                if (this.CustomerRepresentatives.length == 0) {
                    this.apiSvc.get('/OwnershipCustomerRepresentative').then((data) => {
                        resolve(this.CustomerRepresentatives = data);                        
                    }, this.onFailure);
                } else {
                    resolve(this.CustomerRepresentatives);
                }
            });
        }

        onFailure = (response: any): void => {
            if (this.blockUI) this.blockUI.stop();

            this.$log.error(response);
        }

        onSuccess = (response: any): void=> {
            this.$uibModalInstance.close();
            if (this.blockUI) this.blockUI.stop();
            bootbox.alert('Report It Accepted successfully!', this.redirectToDetails);
        }

        redirectToDetails = () => {
            window.location.href = this.basePath + 'Personnel/Ownership/Details/';
        }
        cancel = () => {
            this.$uibModalInstance.close();
        }

        saveOwnership = () => {
            if (!this.isCritical && this.Ownership.Classifications.length == 0) {
                bootbox.alert("Please select one/more Classifications");
            }
            else if ((this.isCritical && this.Selected.CustomerRepresentatives.length > 0) ||
                (!this.isCritical && this.Ownership.Classifications.length > 0 && this.Ownership.ReportTypeId)) {
                var customerRepresentatives = [];
                var createDate: Date = new Date(this.employee.CreateDate);
                if (this.isCritical) {
                    angular.forEach(this.Selected.CustomerRepresentatives, (customerRepresentativeId) => {
                        var representative = _.find(this.CustomerRepresentatives, {
                            CustomerRepresentativeId: customerRepresentativeId
                        });
                        customerRepresentatives.push(representative);
                        var that = this;
                        that.$timeout(() => {

                            return that.mailSvc.sendMail({
                                FromName: 'SBMinsite',
                                FromEmail: 'SBMinsite@sbminsite.com',
                                ToName: representative.Name,
                                ToEmail: representative.Email,
                                Subject: 'Accepted Report It Notification',
                                Message: "<div>Hello!</div></br><div>This email is to notify you about an Accepted Report It.</br></br> Details of the Report It are as follows:</div></br>" +
                                "<div>Create Date: " + createDate.toLocaleDateString() + " </div></br>" +
                                "<div>Description: " + this.employee.Description + " </div></br>" +
                                "<div>Employee Name: " + this.employee.EmployeeName + " </div></br>" +
                                "<div>Building: " + this.employee.BuildingName + " </div></br>" +
                                "<div>Floor: " + this.employee.FloorName + " </div></br>"
                            }).then((result) => {

                            }, this.onFailure);
                        });
                    });
                    this.Ownership['CustomerRepresentatives'] = customerRepresentatives;
                }

                

                this.Ownership.OwnershipId = this.employee.OwnershipId;
                this.Ownership.BuildingId = this.employee.BuildingId;
                this.Ownership.FloorId = this.employee.FloorId;
                this.Ownership.RejectionTypeId = this.employee.RejectionTypeId;
                this.Ownership.Status = 1;
                this.Ownership.StatusUpdateById = this.userContext.UserId;
                this.Ownership.CreateDate = this.employee.CreateDate;
                this.Ownership.OwnershipAttachments = this.employee.OwnershipAttachments;
                this.Ownership.Description = this.employee.Description;
                this.Ownership.EmployeeId = this.employee.EmployeeId;
                this.Ownership.IsCorrectiveActionTaken = this.employee.CorrectiveActionDescription ? true : false;
                this.Ownership.CorrectiveActionDescription = this.employee.CorrectiveActionDescription;
                this.Ownership.IsExempt = this.employee.IsExempt;
                var params = {
                    employeeId: this.Ownership.EmployeeId,
                    kpiId: Common.Enums.KPITYPE.Ownership

                };
                this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                    this.Ownership.IsExempt = false;
                    if (result.IsExempt != null) {
                        this.Ownership.IsExempt = result.IsExempt;
                    }
                    this.apiSvc.update(this.Ownership.OwnershipId, this.Ownership, '/Ownership/').then(this.onSuccess, this.onFailure);
                }, this.onFailure);
            }
        }

    }

    angular.module('app').controller('Personnel.OwnershipAcceptPopup', OwnershipAcceptPopup);

    class CustomerRepresentativeModalCtrl {
        title = 'Add';
        static $inject = [
            '$scope',
            '$uibModalInstance',
            'customerRepresentatives',
            'customerRepresentative'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private customerRepresentatives: any[],
            private customerRepresentative: qualityInterfaces.ICustomerRepresentative) {

        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Create a new customer representative.
         */
        saveCustomerRepresentative = (): void => {
            this.$uibModalInstance.close(<qualityInterfaces.ICustomerRepresentative> this.customerRepresentative);
        }
    }

    angular.module('app').controller('Quality.CustomerRepresentativeModalCtrl', CustomerRepresentativeModalCtrl);

    class OwnershipRejectPopup {
        RejectTypes = [];
        modalInstance;
        basePath: string;

        Ownership = {
            OwnershipId: 0,
            Description: '',
            Classifications: [],
            BuildingId: 0,
            FloorId: 0,
            EmployeeId: 0,
            OwnershipAttachments: [],
            ReportTypeId: null,
            CustomerRepresentatives: [],
            RejectionTypeId: null,
            Status: 0,
            StatusUpdateById: 0,
            StatusUpdateDate: new Date(),
            IsCritical: null,
            CreateDate: new Date(),
            IsCorrectiveActionTaken: null,
            CorrectiveActionDescription: '',
            IsExempt: null 
        }

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'employee',
            'chaitea.services.lookuplist',
            'userContext',
            'sitesettings',
            'blockUI',
            '$log',
            'chaitea.common.services.apibasesvc',
            '$q',
            '$uibModal'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private userContext: IUserContext,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private $q: angular.IQService,
            private $uibModal: angular.ui.bootstrap.IModalService
            ) {
            this.basePath = sitesettings.basePath;
            this.getRejectTypes();
        }

        getRejectTypes = () => {
            this.serviceLookupListSvc.getRejectTypes().then((result) => {
                this.RejectTypes = result;
            }, this.onFailure);
        }

        onFailure = (response: any): void => {
            if (this.blockUI) this.blockUI.stop();

            this.$log.error(response);
        }

        saveOwnership = () => {
            if (this.Ownership.RejectionTypeId) {
                this.Ownership.OwnershipId = this.employee.OwnershipId;
                this.Ownership.BuildingId = this.employee.BuildingId;
                this.Ownership.CustomerRepresentatives = this.employee.CustomerRepresentatives;
                this.Ownership.FloorId = this.employee.FloorId;
                this.Ownership.Status = 0;
                this.Ownership.StatusUpdateById = this.userContext.UserId;
                this.Ownership.CreateDate = this.employee.CreateDate;
                this.Ownership.OwnershipAttachments = this.employee.OwnershipAttachments;
                this.Ownership.Description = this.employee.Description;
                this.Ownership.EmployeeId = this.employee.EmployeeId;
                this.Ownership.IsCorrectiveActionTaken = this.employee.CorrectiveActionDescription ? true : false;
                this.Ownership.CorrectiveActionDescription = this.employee.CorrectiveActionDescription;
                this.Ownership.IsExempt = this.employee.IsExempt;
                var params = {
                    employeeId: this.Ownership.EmployeeId,
                    kpiId: Common.Enums.KPITYPE.Ownership

                };
                this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                    this.Ownership.IsExempt = false;
                    if (result.IsExempt != null) {
                        this.Ownership.IsExempt = result.IsExempt;
                    }
                    this.apiSvc.update(this.Ownership.OwnershipId, this.Ownership, '/Ownership/').then(this.onSuccess, this.onFailure);
                }, this.onFailure);
            }
        }

        onSuccess = (response: any): void=> {
            this.$uibModalInstance.close();
            if (this.blockUI) this.blockUI.stop();
            bootbox.alert('Report It Rejected successfully!', this.redirectToDetails);
        }

        redirectToDetails = () => {
            window.location.href = this.basePath + 'Personnel/Ownership/Details/';
        }
        cancel = () => {
            this.$uibModalInstance.close();
        }

    }

    angular.module('app').controller('Personnel.OwnershipRejectPopup', OwnershipRejectPopup);



}  