﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.ownership.Controllers {
    'use strict';

    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    class OwnershipIndexCtrl {
        moment = moment;
        siteId: number = 0;
        failColor: string = '#EF5C48';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').format(),
            endDate: new Date()
        };
        basePath: string;
        noOfMonths: number = 0;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            'chaitea.personnel.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.apibasesvc',
            Common.Services.ImageSvc.id,
            '$uibModal',
            Common.Services.DateSvc.id
        ];

        valueFormatter = function (val) {
            return val.toFixed(0) + '%';
        };
        reportsConfig = {
            goalLine: 90,
            counts: {
                ownershipTrends: {
                    submitted: 0,
                    accepted: 0,
                    rejected: 0
                },
                ownershipTrendsBySiteOrBuilding: { count: 0 },
                ownershipTrendsByProgramOrType: { count: 0 },
                ownershipAccepted: { count: 0 },
                ownershipRejected: { count: 0 }
            },

            ownershipTrends: {},
            ownershipsBySiteOrBuilding: {},
            ownershipsByProgramOrType: {},
            ownershipTrendsByProgramOrType: {},
            ownershipTrendsByEmployee: [],
            recentOwnerships: []
        };
        viewLink: string = "";
        module: string = 'ownership';
        siteName: string;
        clientName: string = '';
        headerText: string = 'REPORTED_ON';

        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: personnelSvc.IPersonnelReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private imageSvc: Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.failColor = this.sitesettings.colors.coreColors.danger;
            this.clientName = userContext.Client.Name;

            this.reportsConfig.ownershipTrends = this.chartConfigBuilder('ownershipTrends', {});
            this.reportsConfig.ownershipsBySiteOrBuilding = this.chartConfigBuilder('ownershipsBySiteOrBuilding', {});
            this.reportsConfig.ownershipsByProgramOrType = this.chartConfigBuilder('ownershipsByProgramOrType', {});
            this.reportsConfig.ownershipTrendsByProgramOrType = this.chartConfigBuilder('ownershipTrendsByProgramOrType', {});

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });

                this.getCurrentMonthOwnershipTrend({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });
            
        }

        /** @description Initializer for the controller. */
        initialize = (): void => { }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Personnel.OwnershipEntryViewModalCtrl as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            });
        }
        /**
         * @description Get current month ownership trend
         */
        getCurrentMonthOwnershipTrend = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getOwnershipsCount({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                isEmployee: false
            }).then((result) => {
                if (!result) return;

                this.reportsConfig.counts.ownershipTrends.submitted = result.Submitted;
                this.reportsConfig.counts.ownershipTrends.accepted = result.Accepted;
            });
        }

        /**
         * @description Ownership Trends.
         */
        getOwnershipMonthlyTrends = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getOwnershipMonthlyTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result) return;

                var categories = result.Month,
                    accepted = angular.copy(result.Accepted),
                    rejected = angular.copy(result.Rejected);

                // Build the chart
                this.reportsConfig.ownershipTrends = this.chartConfigBuilder('ownershipTrends', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        tickInterval: result.Accepted.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'REJECTED',
                            lineWidth: 100,
                            data: rejected,
                            legendIndex: 1
                        },
                        {
                            name: 'ACCEPTED',
                            lineWidth: 100,
                            data: accepted,
                            legendIndex: 0
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize:'12px',
                            fontWeight: '600',
                        }
                    },
                });

                // Set past due work orders count
                this.reportsConfig.counts.ownershipAccepted.count = <number>_.reduce(accepted, function (sum, n) {
                    return <number>sum + <number>n;
                }) || 0;

                //Set onTime count
                this.reportsConfig.counts.ownershipRejected.count = <number>_.reduce(rejected, (sum, n) => (<number>sum + <number>n)) || 0;
            });
        }

        /**
         * @description By Site or Building.
         */
        getOwnershipsBySiteOrBuilding = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getOwnershipsBySite({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.Name) return;

                var categories = result.Name,
                    accepted = result.Accepted,
                    rejected = result.Rejected;

                // Build the chart
                this.reportsConfig.ownershipsBySiteOrBuilding = this.chartConfigBuilder('ownershipsBySiteOrBuilding', {
                    series: [
                        {
                            name: 'REJECTED',
                            legendIndex: 1,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: rejected[i]

                                }
                            })
                        }, {
                            name: 'ACCEPTED',
                            legendIndex: 0,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: accepted[i]

                                }
                            })
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    height: categories.length == 1 ? categories.length * 200 : categories.length * 100

                });

                // Set counts for total work orders by site or building.
                this.reportsConfig.counts.ownershipTrendsBySiteOrBuilding.count = <number>_.reduce(accepted.concat(rejected), function (sum, n) {
                    return <number>sum + <number>n;
                });

            });
        }

        /**
         * @description By Program or Type.
         */
        getOwnershipsByProgramOrType = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getOwnershipsByProgramOrType({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.length) return;

                var data = result;
                    
                // Build the chart
                this.reportsConfig.ownershipsByProgramOrType = this.chartConfigBuilder('ownershipsByProgramOrType', {
                    series: [
                        {
                            type: 'pie',
                            innerSize: '60%',
                            name: '',
                            data: _.map(data, (d, index) => {
                                var sliceId = d['Id'],
                                    sliceName = d['Name'],
                                    sliceY = d['Count'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    id: sliceId,
                                    // Attach click event to each slice to re-render the line chart
                                    events: {
                                        click: ($event) => {
                                            this.getOwnershipTrendsByProgramOrType(dateRange, {
                                                id: sliceId,
                                                name: sliceName.toUpperCase(),
                                                color: $event.point.color
                                            })
                                        }
                                    },
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                }); 
                    

                //Build the line chart based on the biggest slice of the pie on initial render
                this.getOwnershipTrendsByProgramOrType(dateRange, {
                    id: data[0]['Id'],
                    name: data[0]['Name'],
                });

            });
        }

        /**
         * @description Get trend chart based on legend click.
         */
        public loadTrend = (id: number, color: string, name: string): void => {
            var dateRange: commonInterfaces.IDateRange = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            }
            this.getOwnershipTrendsByProgramOrType(dateRange, {
                id: id,
                name: name.toUpperCase(),
                color: color
            });

        }

        /**
         * @description Get trends by program or type.
         */
        getOwnershipTrendsByProgramOrType = (dateRange: commonInterfaces.IDateRange, selectedItem) => {
            this.chartsSvc.getOwnershipTrendsByProgramOrType(<any>{
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                reportTypeId: selectedItem.id
            }).then((result) => {
                var categories = result.Months,
                    counts = result.Counts;

                this.reportsConfig.ownershipTrendsByProgramOrType = this.chartConfigBuilder('ownershipTrendsByProgramOrType', {
                    categories: categories,
                    series: [
                        {
                            color: selectedItem.color, //'#f26e54',
                            lineWidth: 3,
                            name: selectedItem.name,
                            data: counts
                        }
                    ]
                });
            });
        }

        /**
         * @description By Employees.
         */
        getOwnershipsByEmployees = (dateRange: commonInterfaces.IDateRange): void => {
            if (this.siteId == 0) return;

            this.chartsSvc.getOwnershipsByEmployee({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                this.reportsConfig.ownershipTrendsByEmployee = result;
            });
        }

        /**
         * @description Get 4 latest complaints.
         */
        public getRecentOwnerships = (dateRange: commonInterfaces.IDateRange): void => {
            var dates = {
                startDate: this.dateFormatterSvc.formatDateFull(moment(this.dateRange.startDate).toDate()),
                endDate: this.dateFormatterSvc.formatDateFull(moment(this.dateRange.endDate).endOf('day').toDate())
            };

            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(dates.startDate);
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(dates.endDate);
            this.viewLink = this.basePath + 'Personnel/Ownership/EntryView/';
            this.siteName = this.userContext.Site.Name;
            var filterQuery;

            filterQuery = `CreateDate ge DateTime'${startDate}' and CreateDate le DateTime'${endDate}' and Status eq '1'`;

            this.reportsConfig.recentOwnerships = [];
            var param = <any>{
                $filter: filterQuery,
                $orderby: 'StatusUpdateDate desc',
                $top: 5,
                $skip: 0
            };

            this.apiSvc.query(param, "Ownership")
                .then((data) => {
                    angular.forEach(data, (obj) => {
                        if (obj.ReportedByAttachments) {
                            obj._createByProfilePicture = this.imageSvc.getUserImageFromAttachments(obj.ReportedByAttachments, true);
                        }
                        obj.CreateDate = this.dateSvc.getFromUtcDatetimeOffset(obj.CreateDate, 'MM/DD/YYYY h:mm A');
                        this.reportsConfig.recentOwnerships.push(obj);
                    });
                }, this.onFailure);
        }

        private getAllInstanceInArray = (arr, val) => {
            var instances = [], i = -1;
            while ((i = arr.indexOf(val, i + 1)) != -1) {
                instances.push(i);
            }
            return instances;
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getOwnershipMonthlyTrends(dateRange);
            this.getOwnershipsBySiteOrBuilding(dateRange);
            this.getOwnershipsByProgramOrType(dateRange);
            this.getOwnershipsByEmployees(dateRange);
            this.getRecentOwnerships(dateRange);
        }

        /**
         * @description Defines overridable chart options.
         */
        chartConfigBuilder = (name, options) => {
            var chartOptions = {

                // Column Chart
                ownershipTrends: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        //min: 75,
                        //max: 100,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                // Bar Chart
                ownershipsBySiteOrBuilding: <HighchartsOptions>{
                    title: {
                        text: ''
                    },
                    //height: 40,
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        lineHeight: '10px',
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    yAxis: [
                        {
                            tickInterval: 25,
                            min: 0,
                            max: 100,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                            title: {
                                text: ''
                            }
                        }, {
                            linkedTo: 0,
                            opposite: true,
                            allowDecimals: false,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            }
                        }
                    ],
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'Late',
                            data: []
                        }, {
                            name: 'On Time',
                            data: []
                        }
                    ]
                },

                // Pie Chart
                ownershipsByProgramOrType: {
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []
                        }
                    ]
                },

                // Line Chart
                ownershipTrendsByProgramOrType: {
                    legend: {
                        enabled: true,
                        labelFormatter: function () { return this.name.toUpperCase() + " TREND"; },
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10
                    },
                    title: {
                        text: ''
                    },

                    categories: [],
                    xAxis: {
                        allowDecimals: false,
                        tickWidth: 0,
                        plotBands: [
                            {

                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        allowDecimals: false,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }

                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null // inherit from series
                            }
                        }
                    },
                    series: [
                        {
                            lineWidth: 3,
                            events: {
                                legendItemClick: () => (false)
                            }
                        }]

                }
            }
            return _.assign(chartOptions[name], options);
        }

    }

    angular.module('app').controller('Personnel.OwnershipIndexCtrl', OwnershipIndexCtrl);
}  
