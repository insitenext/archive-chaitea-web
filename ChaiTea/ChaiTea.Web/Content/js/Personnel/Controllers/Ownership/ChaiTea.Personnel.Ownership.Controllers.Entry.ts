﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Ownership.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    class OwnershipEntryCtrl {

        ownershipId: number;
        modalInstance;

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        buildings = [];
        floors = [];

        employeesWithAttachment = [];
        employees = [];
        employee = {};
        employeeId: number = 0;
        OwnershipTypes = [];
        maxDate: Date = null;

        AttachmentName = '';
        ReportTypes = [];
        correctiveActionNo: boolean = true;
        correctiveActionYes: boolean = false;

        //set defaults
        Ownership = <personnelInterfaces.IOwnership>{
            OwnershipId: 0,
            Description: '',
            Classifications: [],
            BuildingId: null,
            FloorId: null,
            EmployeeId: 0,
            OwnershipAttachments: [],
            ReportTypeId: null,
            CustomerRepresentatives: [],
            BuildingName: '',
            FloorName: '',
            ReportTypeName: '',
            RejectionTypeId:null,
            RejectionTypeName: '',
            Status: null,
            IsCritical: null,
            CreateDate: new Date(),
            IsCorrectiveActionTaken: null,
            CorrectiveActionDescription: '',
            IsExempt: null  
        }

        isEmployee: boolean = false;
        tempCorrectiveDescription: string = '';
        accepted: number = 1;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$uibModal',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.isEmployee = this.userInfo.mainRoles.employees;
            this.modalInstance = this.$uibModal;
        }

        
        /** @description Initializer for the controller. */
        initialize = (Id: number): void => {
            this.ownershipId = Id;
            this.maxDate = new Date();

            this.getBuildings();
            this.getEmployeeListWithAttachment();
        }

        private toggleActions = (check: boolean) => {
            this.correctiveActionNo = check ? false : true;
            this.correctiveActionYes = check ? true : false;
            this.Ownership.CorrectiveActionDescription = check ? this.tempCorrectiveDescription : '';
        }

        getBuildings = (): void => {
            this.apiSvc.getLookupList('LookupList/Buildings').then((data) => {
                this.buildings = data.Options;
            });
        }
        /** 
        * @description Get floors based on buildingId. 
        * @param {number} buildingId
        */
        onBuildingChange = (buildingId: number): void => {
            if (buildingId > 0) {
                this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'ParentId eq '+ buildingId }, 'Floor').then((res) => {
                    var floors = _.map(res,(floor) => {
                        return { Key: floor['Id'], Value: floor['Name'] }
                    });
                    this.floors = floors;
                    if (this.blockUI) this.blockUI.stop();
                });

            } else {
                this.floors = [];
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: personnelInterfaces.IOwnership): void => {
            if (this.ownershipId === 0) {
                mixpanel.track("Created Report-it", { ReportItId: response.OwnershipId || 0 });
            }

            if (response.OwnershipId > 0) {
                this.ownershipId = response.OwnershipId;
                
                //response.Status previously int? in VM. is being changed to bool?.
                mixpanel.track((response.Status ? "Accepted" : (response.Status === null ? "Rejected" : "Created")) + " Report It", { ReportItId: response.OwnershipId });

                //track IsCriticals created.
                if (response.IsCritical) mixpanel.track("Report It marked as Critical", { ReportItId: response.OwnershipId });
            }

            bootbox.alert('Report It saved successfully!', this.redirectToDetailsView);
        }

        updateOwnershipTypes = (): void=> {
            angular.forEach(this.OwnershipTypes,(item) => {
                angular.forEach(this.Ownership.Classifications,(selectedItem) => {
                    if (item.OwnershipTypeId == selectedItem.OwnershipTypeId) {
                        item.IsSelected = true;
                    }
                });
            });
        };
        /** @description retrive Ownership by id for editing**/

        getOwnershipById = (id: number): void => {

            this.apiSvc.getById(id, 'Ownership').then((result) => {
                this.Ownership = result;
                this.tempCorrectiveDescription = this.Ownership.CorrectiveActionDescription;
                this.toggleActions(this.Ownership.IsCorrectiveActionTaken);
                if (this.Ownership.Status == true) {
                    this.getReportTypes();
                }
                this.getEmployeeById(this.Ownership.EmployeeId);
                this.updateOwnershipTypes();
                this.onBuildingChange(this.Ownership.BuildingId);
            }, this.onFailure);
        }

        private getReportTypes = (): void => {
            this.serviceLookupListSvc.getReportTypes().then((result) => {
                this.ReportTypes = result;
            }, this.onFailure);
        }

        /** @description Create/update a new Ownership. */

        private saveOwnership = ($event, valid: boolean): void => {
            if ($event) $event.preventDefault();
            if (valid) {
                if (this.ownershipId > 0) {
                    this.Ownership.IsCorrectiveActionTaken =
                        this.Ownership.IsCorrectiveActionTaken == null || this.Ownership.Status == null ? this.Ownership.IsCorrectiveActionTaken :
                            (this.correctiveActionYes ? true : false);
                    // PUT
                    this.apiSvc.update(this.ownershipId, this.Ownership, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Ownership]).then(this.onSuccess, this.onFailure);

                } else {
                    this.isEmployee ? (this.Ownership.IsCorrectiveActionTaken = this.correctiveActionYes ? true : false) : null;
                    // POST
                    var params = {
                        employeeId: this.Ownership.EmployeeId,
                        kpiId: Common.Enums.KPITYPE.Ownership

                    };
                    this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                        this.Ownership.IsExempt = false;
                        if (result.IsExempt != null) {
                            this.Ownership.IsExempt = result.IsExempt;
                        }
                        this.apiSvc.save(this.Ownership, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Ownership]).then(this.onSuccess, this.onFailure);
                    }, this.onFailure);
                }
            }
        }

        /**
      * @description Returns an employee by id.
      */
        private getEmployeeById = (empId: number): void => {
            this.employee = {};
            angular.forEach(this.employeesWithAttachment,(employee) => {
                if (employee.EmployeeId === empId) {
                    this.employee = employee;
                    this.Ownership.EmployeeId = empId;
                }
            });
        }

        /**
        * @description set employee list array.
        */
        private getEmployeesList = (): void => {
            this.employees = [];

            angular.forEach(this.employeesWithAttachment,(employee) => {
                this.employees.push(<IEmployee>{
                    EmployeeId: employee.EmployeeId,
                    EmployeeName: employee.Name
                });
            });

            if (!this.userInfo.mainRoles.managers && !this.userInfo.mainRoles.customers)
                this.getEmployeeById(this.userInfo.userId);
        }

        /**
       * @description Returns a list of employees.
       */
        private getEmployeeListWithAttachment = (): void => {

            this.employeesWithAttachment = [];

            if (!this.userInfo.mainRoles.managers && !this.userInfo.mainRoles.customers) {
                var empId = this.userInfo.userId;
                this.apiSvc.getByOdata({ $filter: 'EmployeeId eq ' +empId }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {

                    angular.forEach(employees,(employee) => {
                        this.employeesWithAttachment.push(<personnelInterfaces.IEmployee>{
                            Name: employee.Name,
                            EmployeeId: employee.EmployeeId,
                            UserAttachments: employee.UserAttachments,
                            HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                            JobDescription: employee.JobDescription.Name,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                        });
                    });
                    this.getEmployeesList();
                 
                    if (this.ownershipId > 0) {
                        this.getOwnershipById(this.ownershipId);
                    }
                });
            }

            //get data from service
            else {
                this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {

                    angular.forEach(employees,(employee) => {
                        this.employeesWithAttachment.push(<personnelInterfaces.IEmployee>{
                            Name: employee.Name,
                            EmployeeId: employee.EmployeeId,
                            UserAttachments: employee.UserAttachments,
                            HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                            JobDescription: employee.JobDescription.Name,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                        });
                    });
                    this.getEmployeesList();
                    if (this.ownershipId == 0) {
                        this.showEmployees();
                    }
                    if (this.ownershipId > 0) {
                        this.getOwnershipById(this.ownershipId);
                    }
                });
            }
        };

        /** @description Redirect to entry view page. */
        redirectToEntryView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Ownership/EntryView/' + this.ownershipId);
        }

        /** @description Redirect to details view page. */
        redirectToDetailsView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Ownership/Details/');
        }

        /**
     * @description Open the modal instance to show the employees avg score by area
     */
        showEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.siteSettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.OwnershipEmployeesCtrl as vm',
                size: 'md',
                //backdrop: 'static',
                resolve: {
                    employee: () => { return this.employeesWithAttachment },
                    employeeId: () => { return this.employeeId }
                }
            }).result.then(returned => {
                if (returned)
                    this.getEmployeeById(returned);
                else
                    this.redirectToDetailsView();
            },() => {
                    this.redirectToDetailsView();
                });


        }

        addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <personnelInterfaces.IOwnershipAttachment>{
                UniqueId: null,
                AttachmentType: 'Image',
                OwnershipAttachmentType: commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE[commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE .Misc],
                OwnershipAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.AttachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, this.awsSvc.getFolderByAttachmentType(tempAttachment.AttachmentType), commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.Ownership.OwnershipAttachments.length) {
                            this.Ownership.OwnershipAttachments[0] = tempAttachment;
                        } else {
                            this.Ownership.OwnershipAttachments.push(tempAttachment);
                        }

                    },(error: any) => {
                            //file.class = 'error';
                        },(progress: any) => {
                            //file.progress = progress;
                        });

                });

            }
        }

        public viewImage = (ownership: ChaiTea.Personnel.Interfaces.IOwnership, $event) => {

            $event.preventDefault();
            $event.stopPropagation();

            // Open the photo manage dialog
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'managePhotoUploads.tmpl.html',
                controller: 'Personnel.OwnershipImageModalCtrl as vm',
                size: 'md',
                resolve: {
                    ownership: () => { return ownership; }
                }
            });

            this.modalInstance.result.then((attachment: any) => {

                if (!attachment) {
                    this.Ownership.OwnershipAttachments = [];
                } else {
                    this.Ownership.OwnershipAttachments[0] = attachment;
                }

            }, () => { });



        }


    }

    class OwnershipEmployeesCtrl implements personnelInterfaces.IPersonnelEmployeesModalCtrl {
        basePath: string;
        modalTitle: string = 'SELECT EMPLOYEE';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        defaultImg = "~/Content/img/user.jpg";
        btnLabel = "SELECT";
        static $inject = ['$scope', 'sitesettings', '$uibModalInstance', 'employee', 'employeeId'];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee: any[],
            private employeeId: number) {
            this.modalTitle = this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.body.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.footer.tmpl.html';
        }

        /**
        * @description Dismiss the modal instance.
        */
        cancel = (): void=> {
            this.$uibModalInstance.close(this.employeeId);
        };

        run = (empId: number): void => {
            this.employeeId = empId;
            this.$uibModalInstance.close(this.employeeId);

        }

    }

    class OwnershipImageModalCtrl {
        currentImage: any;
        imageUniqueId: string;

        imageCss: any = {
            "-ms-transform": "rotate(0deg)",
            "-webkit-transform": "rotate(0deg)",
            "transform": "rotate(0deg)"
        };
        wrapperCss: any = { "display": "block" };

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'blockUI',
            'ownership',
            'localStorageService',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private blockUI,
            private ownership: ChaiTea.Personnel.Interfaces.IOwnership,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.currentImage = ownership.OwnershipAttachments[0];

        }

        /**
         * @description Set active image for view.
         */
        public rotateImage = (photo: any) => {

            this.blockUI.start();

            //fire off reuqest to actually rotate the image on s3
            var key = photo.UniqueId.replace('.', '_');
            this.apiSvc.query({}, 'attachment/0/rotate/90/key/' + key, false, false).then((res: any) => {
                this.blockUI.stop();
                this.currentImage.UniqueId = res.UniqueId;
            });

        }

        /**
         * @description Delete the image.
         */
        public deleteImage = (photo: any) => {
            this.currentImage = null;
            this.cancel();
        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel = (): void => {
            this.$uibModalInstance.close(this.currentImage);
        };

    }


    angular.module('app').controller('Personnel.OwnershipEntryCtrl', OwnershipEntryCtrl);
    angular.module('app').controller('Personnel.OwnershipEmployeesCtrl', OwnershipEmployeesCtrl);
    angular.module('app').controller('Personnel.OwnershipImageModalCtrl', OwnershipImageModalCtrl);

}