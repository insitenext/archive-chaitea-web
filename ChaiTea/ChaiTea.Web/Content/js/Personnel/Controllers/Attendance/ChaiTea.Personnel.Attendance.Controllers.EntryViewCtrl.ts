﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Attendance.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonInterfaces = Common.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class AttendanceIssueEntryViewCtrl {
        attendance: personnelInterfaces.IAttendanceItem;
        attendanceId: number = 0;
        
        employeeWithImage = {};
        basePath: string;

        showActionButtons: boolean = true;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {

            this.basePath = sitesettings.basePath;

            if (!userInfo.mainRoles.customers && !userInfo.mainRoles.managers) {
                this.showActionButtons = false;
            }

        }

        initialize = (attendanceId: number): void=> {
            this.attendanceId = attendanceId;
            this.getAttendanceIssueById(this.attendanceId);
        }

        private getAttendanceIssueById = (attendanceId: number): void=> {
            if (attendanceId > 0) {
                this.attendanceSvc.getAttendanceIssue(attendanceId).then((result) => {
                    this.attendance = result;
                   
                    this.getEmployeeWithImage(result.OrgUserId);
                }, this.onFailure);
            }
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private redirectToDetails = () => {
            window.location.replace(this.basePath + 'Personnel/Attendance/Details');
        }

        private getEmployeeWithImage = (id: number): void => {

            this.apiSvc.getById(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                this.employeeWithImage = <personnelInterfaces.IEmployee>{
                    Name: employee.Name,
                    EmployeeId: employee.EmployeeId,
                    UserAttachments: employee.UserAttachments,
                    HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                    JobDescription: employee.JobDescription.Name,
                    ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                }
            });
        }

        public deleteAttendance = (id: number) => {
            bootbox.confirm("Are you sure you want to delete this attendance entry?", (res) => {
                if (res) {
                    this.apiSvc.delete(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Issue]).then((response) => {
                        if (response.IsClosed) {
                            bootbox.alert("You can not delete attendance issue of the month for which scorecard entry is disabled");
                        }
                        else {
                            this.redirectToDetails();
                        }
                    });
                }
            });
        }

        public editAttendance = () => {
            window.location.replace(this.basePath + 'Personnel/Attendance/Entry/' + this.attendance.IssueId);
        }
       
    }

    angular.module("app").controller("Personnel.AttendanceIssueEntryViewCtrl", AttendanceIssueEntryViewCtrl);
}