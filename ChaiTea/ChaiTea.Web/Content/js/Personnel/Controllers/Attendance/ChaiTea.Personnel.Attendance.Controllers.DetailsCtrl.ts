﻿/// <reference path="../../../_libs.ts" />
 
module ChaiTea.Personnel.Attendance.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    enum FileType { Undefined, ProfilePicture };
    enum Issuetype { Undefined, ArrivedLate, LeftEarly, Absent };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    interface ITabs {
        AbsentCount: number;
        PartialDayCount: number;
    }

    class AttendanceIssueDetailsCtrl {

        siteId: number = 0;
        basePath: string;
        showFilter: boolean = false;
        issueReason = [];
        attendanceIssueTypes = [];
        attendanceIssues = [];
        employeeIdsHavingIssues = [];
        employeesHavingIssues = [];
        employees = [];
        filterEmployeeId: number = 0;
        filterIssueTypeId: number = 0;
        filterReasonId: number = 0;

        isBusy: boolean = false;
        isAllLoaded: boolean = false;

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        activeTab: number = 0;
        tabs: ITabs = {
            AbsentCount: 0,
            PartialDayCount: 0
        };
        top: number = 20;
        skip: number = 0;
        isFilterCollapsed: boolean = true;
        filterOptions = [
            { Key: 'EmployeeId', Value: 'Employee', Options: null, FieldType: 'select' },
            { Key: 'IssueTypeId', Value: 'Issue Type', Options: null, FieldType: 'select' },
            { Key: 'ReasonId', Value: 'Reason', Options: null, FieldType: 'select' }
        ];
        filter = {
            filterType: null,
            filterCriteria: null
        };
        search: string = '';
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            '$uibModal',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc
        ) {
            
            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                // Reset the paging
                this.getAttendanceIssueReset();
            });
            this.getFilterOptions();
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.tabFilterItems = [
                {
                    title: 'All',
                    onClickFn: () => { this.toggleActiveTab(0) },
                    count: this.tabs.AbsentCount + this.tabs.PartialDayCount
                },
                {
                    title: 'Absent',
                    onClickFn: () => { this.toggleActiveTab(1) },
                    count: this.tabs.AbsentCount
                },
                {
                    title: 'Partial Day',
                    onClickFn: () => { this.toggleActiveTab(2) },
                    count: this.tabs.PartialDayCount
                }
            ];

            this.$scope.$watch(() => (this.tabs), (newVal: any, oldVal: any) => {
                if (newVal === oldVal) return false;

                this.tabFilterItems[0].count = this.tabs.AbsentCount + this.tabs.PartialDayCount;
                this.tabFilterItems[1].count = this.tabs.AbsentCount;
                this.tabFilterItems[2].count = this.tabs.PartialDayCount;

            }, true);
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private getTabCounts = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                issueTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria : null,
                reasonId: this.filter.filterType == this.filterOptions[2].Key ? this.filter.filterCriteria : null
            };
            this.apiSvc.query(params, 'Issue/AttendanceTabCounts', false, false).then((result: ITabs) => {
                this.tabs = result;
            }, this.onFailure);
        }

        private getAttendanceItems = () => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: this.top,
                skip: this.skip,
                getAbsent: (this.activeTab == 1) ? true : false,
                getPartial: (this.activeTab == 2) ? true : false,
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                issueTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria : null,
                reasonId: this.filter.filterType == this.filterOptions[2].Key ? this.filter.filterCriteria : null
            };

            this.apiSvc.query(params, 'Issue/AttendanceItems').then((result: any) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                }
                angular.forEach(result, (obj) => {
                    obj.ProfileImage = this.imageSvc.getUserImageFromAttachments(obj.UserAttachments, true);
                    this.attendanceIssues.push(obj);
                });
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure);
        }

        private toggleActiveTab = (activeTab: number) => {
            this.activeTab = activeTab;
            this.attendanceIssues = [];
            this.isAllLoaded = false;
            this.skip = 0;
            this.getAttendanceItems();
        }

        private getFilterOptions = () => {
            this.employees = [];
            this.attendanceIssueTypes = [];
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $select: 'EmployeeId,Name' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((result) => {
                angular.forEach(result, (employee) => {
                    this.employees.push({
                        Key: employee.EmployeeId,
                        Value: employee.Name
                    });
                });
                this.filterOptions[0].Options = this.employees;
            });

            this.serviceLookupListSvc.getAttendanceIssueTypes().then((result) => {
                angular.forEach(result, (item) => {
                    this.attendanceIssueTypes.push({
                        Key: item.IssueTypeId,
                        Value: item.Name
                    });
                });
                this.filterOptions[1].Options = this.attendanceIssueTypes;
            });

            var reasons = [];
            reasons.push(
                {
                    Key: 1,
                    Value: 'Sickness/Medical'
                },
                {
                    Key: 2,
                    Value: 'Vacation'
                },
                {
                    Key: 3,
                    Value: 'Other'
                }
            );
            this.filterOptions[2].Options = reasons;
        }

        private showEntryViewModal = (id): void => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.AttendancePopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.attendanceIssues, { 'IssueId': id });
                    if (index != -1) {
                        this.attendanceIssues.splice(index, 1);
                        this.getTabCounts();
                    }
                }
            });
        }

        getAttendanceIssueReset = (): void => {
            this.attendanceIssues = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.getTabCounts();
            this.getAttendanceItems();
        }

        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private getExportableRecordCount = (): number => {
            var total = 0;
            switch (this.activeTab) {
                case 0:
                    total = this.tabs.AbsentCount + this.tabs.PartialDayCount;
                    break;
                case 1:
                    total = this.tabs.AbsentCount;
                    break;
                case 2:
                    total = this.tabs.PartialDayCount;
                    break;
            }
            return total;
        }

        private export = (): void => {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var exportableAttendance = [];
            var top = this.getExportableRecordCount();
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: top,
                skip: 0,
                getAbsent: (this.activeTab == 1) ? true : false,
                getPartial: (this.activeTab == 2) ? true : false,
                employeeId: this.filter.filterType == this.filterOptions[0].Key ? this.filter.filterCriteria : null,
                issueTypeId: this.filter.filterType == this.filterOptions[1].Key ? this.filter.filterCriteria : null
            };

            this.apiSvc.query(params, "Issue/AttendanceItems").then((result) => {
                angular.forEach(result, (attendance) => {
                    exportableAttendance.push({
                        EmployeeName: attendance.EmployeeName,
                        Date: attendance.DateOfOccurrence,
                        Comment: attendance.Comment,
                        IssueType: attendance.IssueTypeName,
                        Reason: attendance.ReasonName
                    });
                });
                alasql('SELECT * INTO CSV("attendance-details.csv",?) FROM ?', [mystyle, exportableAttendance]);
            }, this.onFailure);

        }

    }

    angular.module('app').controller('Personnel.AttendanceIssueDetailsCtrl', AttendanceIssueDetailsCtrl);
}