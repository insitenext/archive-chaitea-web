﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Attendance.Controllers {
    "use strict";

    class AttendancePopUpCtrl {
        attendance: any;
        issueId: number = 0;

        basePath: string;
        downloadUrl: string;
        moduleIcon: string;
        modalLeftSection: string;
        modalRightSection: string;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.notificationsvc',
            '$uibModal',
            'id',
            '$uibModalInstance'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {

            this.issueId = id;
            this.basePath = sitesettings.basePath;
            this.moduleIcon = 'attendance';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/Attendance/personnel.popup.attendance.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/Attendance/personnel.popup.attendance.rightsection.tmpl.html';
            this.getAttendanceById();
        }
        
        private getAttendanceById = (): void => {
            var params = {
                id: this.issueId,
            }
            this.apiSvc.getById(this.issueId, "Issue/AttendanceById").then((result) => {
                this.attendance = result;
                this.attendance.ProfileImage = this.imageSvc.getUserImageFromAttachments(this.attendance.UserAttachments, true);
            }, this.onFailure);
        }

        /**
         * @description Handle service failure
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private deleteAttendance = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.delete.popup.common.tmpl.html',
                controller: 'Personnel.DeletePopUpCtrl as vm',
                size: 'md',
                resolve: {
                    id: () => {
                        return this.issueId;
                    },
                    endpoint: () => {
                        return "Issue"
                    },
                    icon: () => {
                        return "attendance"
                    },
                    module: () => {
                        return "ATTENDANCE"
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.$uibModalInstance.close(true);
                    this.notificationSvc.successToastMessage('Attendance deleted successfully');
                }
            });
        }
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        public editAttendance = () => {
            window.location.replace(this.basePath + 'Personnel/Attendance/Entry/' + this.attendance.IssueId);
        }
        public redirectToEntry = () => {
            window.location.href = this.basePath + "Personnel/Attendance/Entry/" + this.issueId;
        }
    }

    angular.module("app").controller("Personnel.AttendancePopUpCtrl", AttendancePopUpCtrl);
}
