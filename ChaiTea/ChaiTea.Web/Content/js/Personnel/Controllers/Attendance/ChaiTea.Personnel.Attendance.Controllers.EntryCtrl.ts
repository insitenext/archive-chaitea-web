﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Attendance.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    class AttendanceIssueEntryCtrl {

        issueId: number;
        modalInstance;

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        employeesWithAttachment = [];
        employees = [];
        employee = {};
        employeeId: number = 0;
        attendanceIssueTypes = [];
        issueFlag: number = 0;
        selected: string = "";
        time: Date = null;
        employeeSelected: boolean = false;
        isLate: boolean = false;
        isLeft: boolean = false;
        isAbsent: boolean = false;
        maxDate: Date = null;
        editIssueType: number;

        AttachmentName = '';

        //set defaults
        attendance = <personnelInterfaces.IAttendanceItem>{
            IssueId: 0,
            DateOfOccurrence: <Date> null,
            DateTimeOccurrence: <Date> null,
            Comment: '',
            IssueTypeId: 0,
            IssueType: '',
            ReasonId: null,
            ReasonName: null,
            OrgUserId: 0,
            IsExempt: null
        }
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.awssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.IDateSvc
            ) {
            
            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.modalInstance = this.$uibModal;
            
        }

        
        /** @description Initializer for the controller. */
        initialize = (issueId: number): void => {
            this.issueId = issueId;
            this.attendance.DateOfOccurrence = new Date();
            this.maxDate = new Date();
            
            this.getAttendanceIssueTypes();
            //this.getEmployeeListWithAttachment();
           
            if (this.issueId == 0) {
                this.isLate = true;
                this.time = new Date();
            }
        }

        getAttendanceIssueById = (issueId: number): void=> {
            if (issueId > 0) {
                this.attendanceSvc.getAttendanceIssue(issueId).then((result) => {
                    this.attendance = result;
                    var tempDate : string;
                    tempDate = result.DateOfOccurrence;
                    this.attendance.DateOfOccurrence = moment(tempDate).toDate();
                    this.attendance.IssueType = result.IssueTypeName;

                    this.time = new Date(this.attendance.DateOfOccurrence.toString());
                    this.editIssueType = this.attendance.IssueTypeId;
                    this.getEmployeeById(this.attendance.OrgUserId);
                    switch (this.attendance.IssueTypeId) {
                        case 1: this.isLate = true;
                            break;
                        case 2: this.isLeft = true;
                            break;
                        case 3: this.isAbsent = true;
                            break;
                    }
                    this.setIssueType(this.attendance.IssueTypeId - 1);
                }, this.onFailure);
            }
        }


        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            if (response.IssueId > 0) {
                mixpanel.track('Attendance ' + (this.issueId ? "Edited" : "Created"), { AttendanceId: response.IssueId });
                this.issueId = response.IssueId;
            }
            var forHistoricalPurposes = '';
            if (moment(this.attendance.DateOfOccurrence) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                forHistoricalPurposes = '  This issue is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
            }
            bootbox.alert('Issue was saved successfully!' + forHistoricalPurposes, this.redirectToDetailsView);
        }

        /** @description get attendance issue types and reasons related to types. */
        getAttendanceIssueTypes = (): void => {
            this.serviceLookupListSvc.getAttendanceIssueTypes().then((result) => {
                this.attendanceIssueTypes = result;
                if (this.issueId == 0) {
                    this.setIssueType(0);
                }

                this.getEmployeeListWithAttachment();
            });
        }

        setIssueType = (id: number): void => {
            this.attendance.IssueTypeId = this.attendanceIssueTypes[id].IssueTypeId;
            this.attendance.IssueType = this.attendanceIssueTypes[id].Name;
            this.issueFlag = id;
            
            if (this.attendance.IssueType != "Absent") {
                this.attendance.ReasonId = null;
                this.attendance.ReasonName = null;
            }
        }

        setReason = (id: number, issueId: number): void => {
            this.attendance.ReasonId = this.attendanceIssueTypes[issueId].Reasons[id].ReasonId;
            this.attendance.ReasonName = this.attendanceIssueTypes[issueId].Reasons[id].Name;
            this.selected = this.attendance.ReasonName;
        }

        /** @description Check if all the necessary information is available */
        checkValid = (): void => {
            if (this.attendance.ReasonId == 0) {
                bootbox.alert('Please select appropriate Reason');
            }
            else {
                this.saveIssue();
            }
        }

        /** @description Create/update a new attendance. */

        saveIssue = (): void => {
            if (this.attendance.IssueType == "Absent" && this.attendance.ReasonId == null) {
                bootbox.alert('Please select appropriate Reason');
            }
            else {

                this.attendance.DateOfOccurrence = this.getDateTime();
                this.attendance.DateOfOccurrence = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.attendance.DateOfOccurrence.toString());
                if (this.issueId > 0) {
                    // PUT
                    this.attendanceSvc.updateAttendance(this.issueId, this.attendance).then(this.onSuccess, this.onFailure);
                } else {
                    // POST
                    var params = {
                        employeeId: this.attendance.OrgUserId,
                        kpiId: Common.Enums.KPITYPE.Attendance

                    };
                    this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                        this.attendance.IsExempt = false;
                        if (result.IsExempt != null) {
                            this.attendance.IsExempt = result.IsExempt;
                        }
                        this.attendanceSvc.saveAttendance(this.attendance).then(this.onSuccess, this.onFailure);
                    }, this.onFailure);
                }
            }
        }

        private getDateTime = (): Date=> {
            var dateOfOccurrence = new Date(this.attendance.DateOfOccurrence.toString());
            if (this.time != null) {
                return new Date(
                    dateOfOccurrence.getFullYear(),
                    dateOfOccurrence.getMonth(),
                    dateOfOccurrence.getDate(),
                    this.time.getHours(),
                    this.time.getMinutes());
            }
            else {
                return new Date(
                    dateOfOccurrence.getFullYear(),
                    dateOfOccurrence.getMonth(),
                    dateOfOccurrence.getDate()
                    );
            }
        }

        /**
      * @description Returns an employee by id.
      */
        private getEmployeeById = (empId: number): void => {
            this.employee = {};
            angular.forEach(this.employeesWithAttachment,(employee) => {
                if (employee.EmployeeId === empId) {
                    this.employee = employee;
                    this.attendance.OrgUserId = empId;
                }
            });
        }

        /**
        * @description Returns an employee by id.
        */
        private getEmployeesList = (): void => {
            this.employees = [];

            angular.forEach(this.employeesWithAttachment,(employee) => {
                this.employees.push(<IEmployee>{
                    EmployeeId: employee.EmployeeId,
                    EmployeeName: employee.Name
                });
            });
        }

        /**
       * @description Returns a list of employees.
       */
        private getEmployeeListWithAttachment = (): void => {

            this.employeesWithAttachment = [];

            //get data from service
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {

                angular.forEach(employees,(employee) => {
                    this.employeesWithAttachment.push(<personnelInterfaces.IEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
                this.getEmployeesList();
                if (this.issueId == 0) {
                    this.showEmployees();
                }
                if (this.issueId > 0) {
                    this.getAttendanceIssueById(this.issueId);
                }
            });
        };

        /** @description Redirect to Entry View page. */
        redirectToEntryView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Attendance/EntryView/' + this.issueId);
        }

        /** @description Redirect to Details page. */
        redirectToDetailsView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Attendance/Details/');
        }

        /**
     * @description Open the modal instance to show the employees avg score by area
     */
        showEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.AttendanceIssueEmployeesCtrl as vm',
                size: 'md',
                resolve: {
                    employee: () => { return this.employeesWithAttachment },
                    employeeId: () => { return this.employeeId }
                }
            }).result.then(returned => {
                if (returned)
                    this.getEmployeeById(returned)
                else
                    this.redirectToDetailsView();
                },() => {
                    this.redirectToDetailsView();
            });
           

        }
    }

    class AttendanceIssueEmployeesCtrl implements personnelInterfaces.IPersonnelEmployeesModalCtrl{
        basePath: string;
        modalTitle: string = 'SELECT EMPLOYEE';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        defaultImg = "~/Content/img/user.jpg";
        btnLabel = "RUN ATTENDANCE";
        static $inject = ['$scope', 'sitesettings', '$uibModalInstance', 'employee', 'employeeId'];
        
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee: any[],
            private employeeId: number) {  

            this.modalTitle = this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.body.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.footer.tmpl.html';
        }

        /**
        * @description Dismiss the modal instance.
        */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        run = (empId: number): void => {
            this.employeeId = empId;
            this.$uibModalInstance.close(this.employeeId);
        }
        
    }


    angular.module('app').controller('Personnel.AttendanceIssueEntryCtrl', AttendanceIssueEntryCtrl);
    angular.module('app').controller('Personnel.AttendanceIssueEmployeesCtrl', AttendanceIssueEmployeesCtrl);

}