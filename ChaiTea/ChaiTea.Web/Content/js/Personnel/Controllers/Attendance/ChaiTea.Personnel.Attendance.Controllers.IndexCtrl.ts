﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Attendance.Controllers {

    interface IEmployeeAttendanceInfo {
        EmployeeId: number;
        EmployeeName: string;
        AbsentCount: number;
        PartialDayCount: number;
        Total: number;
        ProfileImage: string;
    }

    enum AttendanceSortBy {
        Employee_Name = 1,
        Absent_Count = 2,
        Partial_Day_Count = 3,
        Total_Count = 4
    }
    
    class AttendanceIndexCtrl {
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        charts = {
            absences: {},
            absentTypeMonthlyTrend: {}
        };
        totalAbsences: number = 0;
        employeeAttendanceInfo: Array<IEmployeeAttendanceInfo> = []; 
        attendanceIssues = [];
        lastModifiedDate: any;
        noOfMonths: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        top: number = 10;
        skip: number = 0;
        sort = [
            {
                SortById: AttendanceSortBy.Employee_Name,
                Asc: true
            },
            {
                SortById: AttendanceSortBy.Absent_Count,
                Asc: false
            },
            {
                SortById: AttendanceSortBy.Partial_Day_Count,
                Asc: false
            },
            {
                SortById: AttendanceSortBy.Total_Count,
                Asc: false
            }
        ];
        activeSortIndex: number = 0;
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            'chaitea.personnel.services.attendancesvc',
            '$uibModal',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Services.IBaseSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private attendanceSvc: ChaiTea.Personnel.Services.IAttendanceSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc
        ) {
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts();
            });
            this.getLastModifiedDate();
        }

        private getLastModifiedDate = () => {
            this.apiSvc.query({}, 'Issue/LastModifiedDate', false, false).then((result) => {
                this.lastModifiedDate = result.LastModifiedDate;
            }, this.onFailure);
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private toggleSort = (index: number) => {
            this.activeSortIndex = index;
            this.sort[index].Asc = !this.sort[index].Asc;
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.employeeAttendanceInfo = [];
            this.getAttendanceInfoPerEmployee();
        }

        private getRecentAttendanceItems = () => {
            this.attendanceIssues = [];
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: 5,
                skip: 0,
                getAbsent: false,
                getPartial: false
            };

            this.apiSvc.query(params, 'Issue/AttendanceItems').then((result: any) => {
                angular.forEach(result, (obj) => {
                    obj.ProfileImage = this.imageSvc.getUserImageFromAttachments(obj.UserAttachments, true);
                    this.attendanceIssues.push(obj);
                });
            }, this.onFailure);
        }

        private refreshCharts = () => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.totalAbsences = 0;
            this.employeeAttendanceInfo = [];
            this.charts.absences = this.chartConfigBuilder('absences', {});
            this.charts.absentTypeMonthlyTrend = this.chartConfigBuilder('absentTypeMonthlyTrend', {});
            this.getAbsences();
            this.getAttendanceInfoPerEmployee();
            this.getRecentAttendanceItems();
        }

        private getAbsences = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
            };

            this.apiSvc.query(params, 'Issue/AbsentTypesCounts', false, true).then((result) => {
                if (!result.length) {
                    return;
                }
                var data = [];
                angular.forEach(result, (item) => {
                    var obj: any = {};
                    obj.ReasonId = item.ReasonId;
                    obj.ReasonName = item.ReasonName;
                    obj.Count = item.Count;
                    data.push(obj);
                    this.totalAbsences += item.Count;
                });
                this.charts.absences = this.chartConfigBuilder('absences', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Absences',
                            innerSize: '60%',
                            events: {
                                click: (event) => {
                                    this.getAbsentTypeMonthlyTrend({
                                        id: event.point.id,
                                        color: event.point.color,
                                        name: event.point.name
                                    });
                                }
                            },
                            data: _.map(data, (d, index) => {
                                var sliceName = d['ReasonName'],
                                    sliceY = d['Count'],
                                    id = d['ReasonId'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    id: id
                                }
                            })
                        }],
                });
                this.getAbsentTypeMonthlyTrend({
                    id: data[0]['ReasonId'],
                    name: data[0]['ReasonName']
                });
            }, this.onFailure);
        }

        private getAbsentTypeMonthlyTrend = (absentTypeObj: any) => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                reasonId: absentTypeObj.id
            };

            this.apiSvc.query(params, 'Issue/AttendanceMonthlyTrendByReasonId', false, true).then((result) => {
                if (!result)
                    return;
                var categories = [];
                var AbsentTypeCount = [];
                for (var i = moment(new Date(this.dateRange.startDate.toISOString())); i < moment(new Date(this.dateRange.endDate.toISOString())); i.add(1, 'month')) {
                    categories.push(i.format('MMM \'YY'));
                    var obj: any;
                    obj = _.find(result, { Month: (i.month() + 1), Year: i.year() });
                    if (obj) {
                        AbsentTypeCount.push(obj.ReasonCount);
                    }
                    else {
                        AbsentTypeCount.push(0);
                    }
                }
                this.charts.absentTypeMonthlyTrend = this.chartConfigBuilder('absentTypeMonthlyTrend', {
                    categories: categories,
                    series: [
                        {
                            color: absentTypeObj.color,
                            name: absentTypeObj.name,
                            lineWidth: 3,
                            data: AbsentTypeCount
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'left',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -15,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });
            }, this.onFailure);
        }

        private getAttendanceInfoPerEmployee = () => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: this.top,
                skip: this.skip,
                sortById: this.sort[this.activeSortIndex].SortById,
                asc: this.sort[this.activeSortIndex].Asc
            };

            this.apiSvc.query(params, 'Issue/AttendanceInfoPerEmployee', false, true).then((result: any) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                }
                angular.forEach(result, (item) => {
                    if (item.UserAttachments.length == 0) {
                        item.ProfileImage = this.sitesettings.defaultProfileImage;
                    }
                    else {
                        item.ProfileImage = this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true);
                    }
                    this.employeeAttendanceInfo.push(item);
                });
                if (this.employeeAttendanceInfo.length < this.top) {
                    this.isAllLoaded = true;
                }
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure);
        }

        private showEntryViewModal = (id): void => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.AttendancePopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.refreshCharts();
                }
            });
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },
            });

            var chartOptions = {

                // Pie Chart
                absences: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },
                absentTypeMonthlyTrend: {
                    categories: [],
                    yAxis: {
                        min: 0,
                        plotLines: [{
                            zIndex: 4,
                            label: { text: '' },

                        }],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }

                    },
                    xAxis: {
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },

                    series: [{}, {}],
                    plotLines: [{}],
                    legend: {
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },

                }
            }
            return _.assign(chartOptions[name], options);
        }
    }
    angular.module('app').controller('Personnel.AttendanceIndexCtrl', AttendanceIndexCtrl);
}