﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    class PersonnelAuditEntryCtrl {
        basePath: string;
        templatePath: string;
        auditItemTemplatePath: string;

        auditId: number = 0;
        employeeId: number = 0;
        employee:Training.Interfaces.ITrainingEmployee;
        audit: Interfaces.IEmployeeAudit=<any>{
            CheckBoxSelections:[]
        };

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment:[]
        };
        
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {
            this.basePath = sitesettings.basePath

            this.basePath = sitesettings.basePath;
            this.templatePath = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.entry.tmpl.html';
            this.auditItemTemplatePath = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.auditItem.tmpl.html';
         
        }
        
        /** @description Initializer for the controller. */
        initialize = (employeeId): void => {
            if (!employeeId) window.history.back();
                
            this.employeeId = employeeId;
            this.apiSvc.getById(this.employeeId, "TrainingEmployee").then((employee: Training.Interfaces.ITrainingEmployee) => {
                employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
                this.employee = employee;
                this.$log.log(this.employee)
            });

            this.apiSvc.get("LookupList/EmployeeAuditOptions").then((res) => {

                this.kpiOptions.appearance = _.where(res, { KpiOption: { Name: 'Appearance' } });
                this.kpiOptions.appearance["Description"] = "Employee wears an SBM tidy uniform and has good hygiene.";
                this.kpiOptions.appearance["Name"] = "Appearance";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.appearance, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.appearance["IsPass"] = false;
                    }
                }

                this.kpiOptions.attitude = _.where(res, { KpiOption: { Name: 'Attitude' } });
                this.kpiOptions.attitude["Description"] = "Employee displays a positive approachable demeanor, shows respect and is attentive.";
                this.kpiOptions.attitude["Name"] = "Attitude";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.attitude, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.attitude["IsPass"] = false;
                    }
                }

                this.kpiOptions.responsiveness = _.where(res, { KpiOption: { Name: 'Responsiveness' } });
                this.kpiOptions.responsiveness["Description"] = "Employee displays a quick response time and shows initiative.";
                this.kpiOptions.responsiveness["Name"] = "Responsiveness";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.responsiveness, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.responsiveness["IsPass"] = false;
                    }
                }

                this.kpiOptions.equipment = _.where(res, { KpiOption: { Name: 'Closet & Equipment' } });
                this.kpiOptions.equipment["Description"] = "Employee maintains proper care of equipment and a tidy closet without any hazardous conditions.";
                this.kpiOptions.equipment["Name"] = "Equipment & Closets";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.equipment, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.equipment["IsPass"] = false;
                    }
                }

                this.$log.log('kpiOptions', this.kpiOptions);
            });

        }

        private getEmployeeAudit(auditId: number): angular.IPromise<any> {
            return this.apiSvc.getById(auditId,"EmployeeAudit");
        }

        public createUpdateAudit() {
            this.audit.EmployeeId = this.employeeId;
            //if (!this.auditId) {
            var self = this;

            //TEMPORARY need to change KpiSubOptions to Options
            angular.forEach(this.kpiOptions, (option: any) => {
                if (option.IsPass === false) {
                    this.audit.CheckBoxSelections.push(option[0].KpiSubOptionId);
                }
            });

            bootbox.confirm("Are you sure you want to submit?", (res) => {
                if (res) {
                    this.apiSvc.save(this.audit, 'EmployeeAudit').then((res) => {
                        mixpanel.track('Professionalism Audit Created', { ProfessionalismAuditId: res.EmployeeAuditId});
                        self.redirect();
                    }, this.handleError);
                } else {
                    this.audit.CheckBoxSelections = [];
                }
            });
            //} else {
            //    this.apiSvc.update(this.auditId, this.audit).then((res) => {
            //        self.notificationSvc.errorToastMessage("Audit has been updated successfully.");
            //        self.redirect();
            //    });
            //}
        }


        public toggleCheckBox = (kpi, value) => {
            if (_.contains(kpi.CheckBoxSelections, value)) {
                kpi.CheckBoxSelections.slice(value, 1);
                _.remove(kpi.CheckBoxSelections,(num) => (num ==value))
            } else {
                kpi.CheckBoxSelections.push(value);
            }

        }
        

        private redirect = () => {
            window.location.href = this.basePath + "Personnel/Audit/Details";
        }

        private handleError = (error) => {
            this.$log.error(error);
        }

    }

    angular.module('app').controller('Personnel.AuditEntryCtrl', PersonnelAuditEntryCtrl);

}