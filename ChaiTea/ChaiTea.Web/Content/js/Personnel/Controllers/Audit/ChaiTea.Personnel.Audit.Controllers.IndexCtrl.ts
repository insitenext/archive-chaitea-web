﻿/// <reference path="../../../_libs.ts" />
 
module ChaiTea.Personnel.Audit.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    interface IEmployeeAuditsPerEmployeeInfo {
        EmployeeId: number;
        EmployeeName: string;
        PassingCount: number;
        NotPassingCount: number;
        Total: number;
        ProfileImage: string;
    }

    enum EmployeeAuditSoryBy {
        Employee_Name = 1,
        Not_Passing_Count = 2,
        Passing_Count = 3
    }

    class AuditIndexCtrl {
        basePath: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        modalInstance: angular.ui.bootstrap.IModalSettings;
        employeeAudits = [];
        noOfMonths: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        top: number = 10;
        skip: number = 0;
        totalIncomplete: number = 0;
        employeeAuditsPerEmployeeInfo: Array<IEmployeeAuditsPerEmployeeInfo> = []; 
        charts = {
            monthlyTrendPieChart: {},
            monthlyTrendColumnChart: {}
        };
        pieColors: string = '';
        columnColors: string = '';
        employeesAudited = [];
        lastModifiedDate: any;
        sort = [
            {
                SortById: EmployeeAuditSoryBy.Employee_Name,
                Asc: true
            },
            {
                SortById: EmployeeAuditSoryBy.Not_Passing_Count,
                Asc: false
            },
            {
                SortById: EmployeeAuditSoryBy.Passing_Count,
                Asc: false
            }
        ];
        activeSortIndex: number = 0;
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',            
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc
            ) {

            this.basePath = sitesettings.basePath;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts();
            });
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.modalInstance = this.$uibModal;
            
            //Autofire todo modal from mainMenu
            if (window.location.search.indexOf('runAudit') > 0) {
                setTimeout(() => {
                    //this.runAudit();
                }, 500);
            }
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private refreshCharts = () => {
            this.employeeAuditsPerEmployeeInfo = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.totalIncomplete = 0;
            this.charts.monthlyTrendPieChart = this.chartConfigBuilder('monthlyTrendPieChart', {});
            this.charts.monthlyTrendColumnChart = this.chartConfigBuilder('monthlyTrendColumnChart', {});
            this.getMonthlyTrendPieChartData();
            this.getRecentEmployeeAudits();
            this.getMonthlyTrendColumnChartData();
            this.getEmployeeAuditsInfoPerEmployee();
        }

        private getRecentEmployeeAudits = () => {
            this.employeeAudits = [];
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: 5,
                skip: 0,
                getPassingData: false,
                getNotPassingData: false
            };

            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAudits').then((result: any) => {
                angular.forEach(result, (obj) => {
                    obj.ProfileImage = this.imageSvc.getUserImageFromAttachments(obj.UserAttachments, true);
                    this.employeeAudits.push(obj);
                });
            }, this.onFailure);
        }

        private toggleSort = (index: number) => {
            this.activeSortIndex = index;
            this.sort[index].Asc = !this.sort[index].Asc;
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.employeeAuditsPerEmployeeInfo = [];
            this.getEmployeeAuditsInfoPerEmployee();
        }

        private getMonthlyTrendPieChartData = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day')),
            };

            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAuditStats', false, false).then((result) => {
                this.totalIncomplete = result.IncompleteCount;
                this.lastModifiedDate = result.LastModifiedDate;
                var data = [];
                data.push(
                    {
                        Name: 'Passing',
                        Count: result.PassingCount
                    },
                    {
                        Name: 'Not Passing',
                        Count: result.NotPassingCount
                    },
                    {
                        Name: 'Incomplete',
                        Count: result.IncompleteCount
                    }
                );
                var colors = [this.sitesettings.colors.coreColors.success, this.sitesettings.colors.coreColors.dangerDark, this.sitesettings.colors.coreColors.base];
                this.pieColors = colors.join();
                this.charts.monthlyTrendPieChart = this.chartConfigBuilder('monthlyTrendPieChart', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Monthly trend',
                            innerSize: '60%',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Count'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });
            }, this.onFailure);
        }

        private getMonthlyTrendColumnChartData = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
            };

            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAuditsMontlyTrend', false, true).then((result) => {
                var categories = [];
                var passing = [];
                var notPassing = [];
                for (var i = moment(new Date(this.dateRange.startDate.toISOString())); i < moment(new Date(this.dateRange.endDate.toISOString())); i.add(1, 'month')) {
                    categories.push(i.format('MMM \'YY'));
                    var obj: any;
                    obj = _.find(result, { Month: (i.month() + 1), Year: i.year() });
                    if (obj) {
                        passing.push(obj.PassingCount);
                        notPassing.push(obj.NotPassingCount);
                    }
                    else {
                        passing.push(0);
                        notPassing.push(0);
                    }
                }
                var colors = [this.sitesettings.colors.coreColors.dangerDark, this.sitesettings.colors.coreColors.success];
                this.columnColors = colors.join();
                this.charts.monthlyTrendColumnChart = this.chartConfigBuilder('monthlyTrendColumnChart', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'Not Passing',
                            lineWidth: 100,
                            data: notPassing,
                            legendIndex: 1
                        },
                        {
                            name: 'Passing',
                            lineWidth: 100,
                            data: passing,
                            legendIndex: 0
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });
            }, this.onFailure);
        }

        private getEmployeeAuditsInfoPerEmployee = () => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: this.top,
                skip: this.skip,
                sortById: this.sort[this.activeSortIndex].SortById,
                asc: this.sort[this.activeSortIndex].Asc
            };

            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAuditsInfoPerEmployee', false, true).then((result: any) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                }
                angular.forEach(result, (item) => {
                    if (item.UserAttachments.length == 0) {
                        item.ProfileImage = this.sitesettings.defaultProfileImage;
                    }
                    else {
                        item.ProfileImage = this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true);
                    }
                    this.employeeAuditsPerEmployeeInfo.push(item);
                });
                if (this.employeeAuditsPerEmployeeInfo.length < this.top) {
                    this.isAllLoaded = true;
                }
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure);
        }

        private showEntryViewModal = (id): void => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.AuditPopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.refreshCharts();
                }
            });
        }

        private runAudit = ($event?) => {
            if($event) $event.preventDefault();
            var that = this;
            this.employeesAudited = [];
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {
                angular.forEach(employees, (employee) => {
                    this.employeesAudited.push(<personnelInterfaces.IEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription ? employee.JobDescription.Name : "N/A",
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
                this.modalInstance = this.$uibModal.open({
                    templateUrl: 'select-employee.html',
                    controller: 'Personnel.Audit.SelectEmployeeModal as vm',
                    size: 'md',
                    windowClass: 'no-padding',
                    resolve: {
                        employeeList: () => { return this.employeesAudited; }
                    }
                });
            }, this.onFailure);
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
       }

        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },
            });

            var chartOptions = {

                // Pie Chart
                monthlyTrendPieChart: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },
                monthlyTrendColumnChart: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },
            }
            return _.assign(chartOptions[name], options);
        }

    }

    class SelectEmployeeModal {
        static $inject = ['$scope','$uibModalInstance','employeeList'];
        constructor(
            private $scope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employeeList
        ) {
        }
    }

    angular.module('app').controller('Personnel.AuditIndexCtrl', AuditIndexCtrl);
    angular.module('app').controller('Personnel.Audit.SelectEmployeeModal', SelectEmployeeModal);
}