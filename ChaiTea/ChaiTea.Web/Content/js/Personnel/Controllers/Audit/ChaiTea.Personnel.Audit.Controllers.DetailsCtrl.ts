﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Audit.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    enum FileType { Undefined, ProfilePicture };
    enum Issuetype { Undefined, ArrivedLate, LeftEarly, Absent };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    interface ITabs {
        PassingCount: number;
        NotPassingCount: number;
    }

    class AuditDetailsCtrl {
        siteId: number = 0;
        basePath: string;
        employeeAudits = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        activeTab: number = 0;
        tabs: ITabs = {
            PassingCount: 0,
            NotPassingCount: 0
        };
        top: number = 20;
        skip: number = 0;
        employeesAudited = [];
        modalInstance: any;
        search: string = '';
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            '$uibModal',
            Common.Services.DateSvc.id,
            Common.Services.UtilitySvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private utilSvc: Common.Services.IUtilitySvc
        ) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                // Reset the paging
                this.reset();
            });
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.tabFilterItems = [
                {
                    title: 'All',
                    onClickFn: () => { this.toggleActiveTab(0) },
                    count: this.tabs.NotPassingCount + this.tabs.PassingCount
                },
                {
                    title: 'Passing',
                    onClickFn: () => { this.toggleActiveTab(1) },
                    count: this.tabs.PassingCount
                },
                {
                    title: 'Not Passing',
                    onClickFn: () => { this.toggleActiveTab(2) },
                    count: this.tabs.NotPassingCount
                }
            ];

            this.$scope.$watch(() => (this.tabs), (newVal: any, oldVal: any) => {
                if (newVal === oldVal) return false;

                this.tabFilterItems[0].count = this.tabs.NotPassingCount + this.tabs.PassingCount;
                this.tabFilterItems[1].count = this.tabs.PassingCount;
                this.tabFilterItems[2].count = this.tabs.NotPassingCount;

            }, true);
        }

        private goToReportCard = (employeeId: number) => {
            return this.utilSvc.getWebLink('/personnel/employee/' + employeeId + '/report-card');
        }

        private getTabCounts = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
            };
            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAuditsTabCounts', false, false).then((result: ITabs) => {
                this.tabs = result;
            }, this.onFailure);
        }

        private getEmployeeAudits = () => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: this.top,
                skip: this.skip,
                getPassingData: (this.activeTab == 1) ? true : false,
                getNotPassingData: (this.activeTab == 2) ? true : false,
            };

            this.apiSvc.query(params, 'EmployeeAudit/EmployeeAudits').then((result: any) => {
                if (!result.length) {
                    this.isAllLoaded = true;
                }
                angular.forEach(result, (obj) => {
                    obj.ProfileImage = this.imageSvc.getUserImageFromAttachments(obj.UserAttachments, true);
                    this.employeeAudits.push(obj);
                });
                this.isBusy = false;
                this.skip += this.top;
            }, this.onFailure);
        }

        private toggleActiveTab = (activeTab: number) => {
            this.activeTab = activeTab;
            this.employeeAudits = [];
            this.isAllLoaded = false;
            this.skip = 0;
            this.getEmployeeAudits();
        }

        private showEntryViewModal = (id): void => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.details.popup.common.tmpl.html',
                controller: 'Personnel.AuditPopUpCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.employeeAudits, { 'EmployeeAuditId': id });
                    if (index != -1) {
                        this.employeeAudits.splice(index, 1);
                        this.getTabCounts();
                    }
                }
            });
        }

        private reset = (): void => {
            this.employeeAudits = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.skip = 0;
            this.getTabCounts();
            this.getEmployeeAudits();
        }

        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private getExportableRecordCount = (): number => {
            var total = 0;
            switch (this.activeTab) {
                case 0:
                    total = this.tabs.PassingCount + this.tabs.NotPassingCount;
                    break;
                case 1:
                    total = this.tabs.PassingCount;
                    break;
                case 2:
                    total = this.tabs.NotPassingCount;
                    break;
            }
            return total;
        }

        private runAudit = ($event?) => {
            if ($event) $event.preventDefault();
            var that = this;
            this.employeesAudited = [];
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {
                angular.forEach(employees, (employee) => {
                    this.employeesAudited.push(<personnelInterfaces.IEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription ? employee.JobDescription.Name : "N/A",
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
                this.modalInstance = this.$uibModal.open({
                    templateUrl: 'select-employee.html',
                    controller: 'Personnel.Audit.SelectEmployeeModal as vm',
                    size: 'md',
                    windowClass: 'no-padding',
                    resolve: {
                        employeeList: () => { return this.employeesAudited; }
                    }
                });
            }, this.onFailure);
        }

        private export = (): void => {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var exportableEmployeeAudits = [];
            var top = this.getExportableRecordCount();
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                top: top,
                skip: 0,
                getPassingData: (this.activeTab == 1) ? true : false,
                getNotPassingData: (this.activeTab == 2) ? true : false,
            };

            this.apiSvc.query(params, "EmployeeAudit/EmployeeAudits").then((result) => {
                angular.forEach(result, (audit) => {
                    exportableEmployeeAudits.push({
                        EmployeeName: audit.EmployeeName,
                        AuditDate: audit.CreateDate,
                        Comment: audit.Comment,
                        EventBasedAudit: audit.IsEventBasedAudit ? 'Yes' : 'No',
                        Passed: audit.IsPass ? 'Yes' : 'No' 
                    });
                });
                alasql('SELECT * INTO CSV("employee-audit-details.csv",?) FROM ?', [mystyle, exportableEmployeeAudits]);
            }, this.onFailure);

        }

    }

    angular.module('app').controller('Personnel.AuditDetailsCtrl', AuditDetailsCtrl);
}