﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Audit.Controllers {
    "use strict";

    class AuditPopUpCtrl {
        employeeAudit: any;
        employeeAuditId: number = 0;

        basePath: string;
        downloadUrl: string;
        moduleIcon: string;
        modalLeftSection: string;
        modalRightSection: string;
        criterias = [
            {
                Id: 1,
                Name: 'Appearance',
                Passed: true
            },
            {
                Id: 2,
                Name: 'Attitude',
                Passed: true
            },
            {
                Id: 3,
                Name: 'Equipment',
                Passed: true
            },
            {
                Id: 4,
                Name: 'Responsiveness',
                Passed: true
            }
        ];
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.notificationsvc',
            '$uibModal',
            'id',
            '$uibModalInstance'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {

            this.employeeAuditId = id;
            this.basePath = sitesettings.basePath;
            this.moduleIcon = 'professionalism';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/ProfessionalismAudit/personnel.popup.audit.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Personnel/Templates/ProfessionalismAudit/personnel.popup.audit.rightsection.tmpl.html';
            this.getEmployeeAuditById();
        }

        private getEmployeeAuditById = (): void => {
            var params = {
                id: this.employeeAuditId,
            }
            this.apiSvc.getById(this.employeeAuditId, "EmployeeAudit/EmployeeAuditById").then((result) => {
                this.employeeAudit = result;
                this.employeeAudit.ProfileImage = this.imageSvc.getUserImageFromAttachments(this.employeeAudit.UserAttachments, true);
                angular.forEach(this.criterias, (item) => {
                    var index = _.findIndex(result.KpiOptions, { 'KpiOptionId': item.Id });
                    if (index != -1) {
                        item.Passed = false;
                    }
                });
            }, this.onFailure);
        }

        /**
         * @description Handle service failure
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private deleteEmployeeAudit = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Personnel/Templates/personnel.delete.popup.common.tmpl.html',
                controller: 'Personnel.DeletePopUpCtrl as vm',
                size: 'md',
                resolve: {
                    id: () => {
                        return this.employeeAuditId;
                    },
                    endpoint: () => {
                        return "EmployeeAudit"
                    },
                    icon: () => {
                        return "professionalism"
                    },
                    module: () => {
                        return "Audits"
                    }
                }
            }).result.then((res) => {
                if (res) {
                    this.$uibModalInstance.close(true);
                    this.notificationSvc.successToastMessage('Audit deleted successfully');
                }
            });
        }
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module("app").controller("Personnel.AuditPopUpCtrl", AuditPopUpCtrl);
}
