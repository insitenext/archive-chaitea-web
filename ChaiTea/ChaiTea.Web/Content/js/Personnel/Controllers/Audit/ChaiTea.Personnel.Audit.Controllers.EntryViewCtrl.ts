﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Attendance.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonInterfaces = Common.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    class AuditEntryViewCtrl {
        audit: any = {};
        auditId: number = 0;

        employeeWithImage = {};
        basePath: string;

        employeesWithAttachment = [];

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.attendancesvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private attendanceSvc: personnelSvc.IAttendanceSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {
            
            this.basePath = sitesettings.basePath;
        }

        initialize = (auditId: number): void=> {
            this.auditId = auditId;

            this.getEmployeeListWithAttachment();
        }

        /**
        * @description Returns a list of employees.
        */
        private getEmployeeListWithAttachment = (): void => {

            this.employeesWithAttachment = [];
            //get data from service
            this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'JobDescription ne null' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees) => {
                this.employeesWithAttachment = employees;
                this.getAudit();
            });
        }
        private getAudit = () => {
            this.apiSvc.getById(this.auditId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.EmployeeAudit]).then((audit) => {
                var audit = audit;
                audit.items = {};
                audit.items.appearance = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 1 } });
                audit.items.attitude = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 2 } });
                audit.items.equipment = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 3 } });
                audit.items.responsiveness = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 4 } });
                if (_.any(audit.items,(item)=>(item == false))) {
                    audit.IsPass = false;
                }
                angular.forEach(this.employeesWithAttachment,(emp) => {
                    if (audit.EmployeeId == emp.EmployeeId) {
                        audit.Employee = emp;
                        audit.ProfilePicture = this.imageSvc.getUserImageFromAttachments(emp.UserAttachments, true);
                    }
                })

                this.audit = audit;
            })
            return this.audit;
        }

        private deleteAudit = () => {
            if (!this.audit.EmployeeAuditId) return;

            bootbox.confirm("Are you sure you want to delete this Audit?", (res) => {
                if (res) {
                    this.apiSvc.delete(this.audit.EmployeeAuditId, "EmployeeAudit").then((response) => {
                        if (response.IsClosed){
                            bootbox.alert("You can not delete audits of the month for which scorecard entry is disabled");
                        }
                        else {
                            this.redirect();
                        }
                    });
                }
            });
        }

        private redirect = () => {
            window.location.href = this.basePath + "Personnel/Audit";
        }


    }

    angular.module("app").controller("Personnel.AuditEntryViewCtrl", AuditEntryViewCtrl);
}