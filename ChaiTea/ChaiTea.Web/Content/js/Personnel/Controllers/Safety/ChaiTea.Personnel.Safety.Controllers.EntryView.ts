﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Safety.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonInterfaces = Common.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class SafetyEntryViewCtrl {
        safety: personnelInterfaces.ISafetyItem;
        safetyId: number = 0;

        employeeWithImage = {};
        basePath: string;
        
        downloadUrl: string = '';

        static $inject = [
            '$scope',
            '$translate',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.personnel.services.safetysvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.notificationsvc',
            Common.Services.DateSvc.id
        ];

        constructor(
            private $scope,
            private $translate,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private safetySvc: personnelSvc.ISafetySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc
        ) {
            this.basePath = sitesettings.basePath;
        }

        initialize = (safetyId: number): void => {
            this.safetyId = safetyId;
            if (this.safetyId > 0) {
                this.safetySvc.getSafetyById(this.safetyId)
                              .then((result) => {
                                  this.safety = result;
                                  this.safety.DateOfIncident = this.dateSvc.getFromUtcDatetimeOffset(result.DateOfIncident, 'MMM D, Y');
                    this.downloadUrl = '';
                    if (this.safety.SafetyAttachments.length) {
                        this.downloadUrl = this.awsSvc.getUrlByAttachment(this.safety.SafetyAttachments[0].UniqueId, this.safety.SafetyAttachments[0].AttachmentType);
                    }

                    this.apiSvc.getById(result.EmployeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee])
                        .then((employee) => {
                            this.employeeWithImage = {
                                Name: employee.Name,
                                JobDescription: employee.JobDescription.Name,
                                HireDate: moment.tz(employee.HireDate, 'utc').format('MMM D, Y'),
                                ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                            };
                        }, this.onFailure);
                }, this.onFailure);
            }
        }

        private getIncidentTypeCode = (id: number): string => {
            var name: string = 'Unknown';
            angular.forEach(ChaiTea.Common.Safety.Enums.StaticIncidentTypes.AsList, (incidentType) => {
                if (incidentType.IncidentTypeId == id) {
                    name = incidentType.Code;
                }
            });
            return name;
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        private redirectToDetails = () => {
            window.location.replace(this.basePath + 'Personnel/Safety/Details');
        }

        private redirectToEdit = () => {
            window.location.replace(this.basePath + 'Personnel/Safety/Entry/' + this.safety.SafetyId);
        }

        private deleteSafety = (id: number) => {
            this.$translate("SAFETY_DELETECONFIRM").then((translation) => {
                this.alertSvc.confirmWithCallback(translation, (res) => {
                    if (res) {
                        this.safetySvc.deleteSafetyById(id)
                                      .then((response) => {
                            if (response.IsClosed) {
                                this.$translate("SAFETY_DELETEDISABLED").then((translation) => {
                                    this.alertSvc.alert({ message: translation });
                                });
                            }
                            else {
                                this.redirectToDetails();
                            }
                        }, this.onFailure);
                    }
                });
            });
        }
    }
    angular.module("app").controller("Personnel.SafetyEntryViewCtrl", SafetyEntryViewCtrl);
}