﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Personnel.Safety.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    };
  
    class SafetyEntryCtrl {

        safetyId: number;
        modalInstance;

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        employeesWithAttachment = [];
        employee = {};
        employeeId: number = 0;
        IncidentTypes = ChaiTea.Common.Safety.Enums.StaticIncidentTypes.AsList;
        maxDate: Date = null;

        AttachmentName = 'Select Attachment';
        //set defaults
        Safety = <personnelInterfaces.ISafetyItem>{
            SafetyId: 0,
            DateOfIncident: <Date> null,
            Detail: '',
            IncidentTypes: [],
            EmployeeId: 0,
            SafetyAttachments: []
        }

        static $inject = [
            '$scope',
            '$translate',
            '$q',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.common.services.awssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.safetysvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.alertsvc',
            Common.Services.DateSvc.id,
            'chaitea.common.services.notificationsvc'
        ];
        constructor(
            private $scope,
            private $translate,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private SafetySvc: personnelSvc.ISafetySvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private serviceLookupListSvc: serviceLookupListSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private notificaitonSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;

            this.modalInstance = this.$uibModal;
        }

        
        /** @description Initializer for the controller. */
        initialize = (safetyId: number): void => {
            this.safetyId = safetyId;
            this.maxDate = new Date();

            if (this.safetyId == 0) {
                this.employeesWithAttachment = [];

                this.apiSvc.getByOdata({
                    $orderby: 'Name asc',
                    $filter: 'JobDescription ne null'
                }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee])
                    .then((employees) => {
                    angular.forEach(employees, (employee) => {
                        this.employeesWithAttachment.push(<personnelInterfaces.IEmployee>{
                            Name: employee.Name,
                            EmployeeId: employee.EmployeeId,
                            JobDescription: employee.JobDescription.Name,
                            HireDate: moment.tz(employee.HireDate, 'utc').format('MMM D, Y'),
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                        });
                    });
                });
            
                this.showEmployees();

            } else if (this.safetyId > 0) {
                this.SafetySvc.getSafetyById(this.safetyId).then((result) => {
                    this.Safety = result;
                    this.Safety.DateOfIncident = this.dateSvc.getFromUtcDatetimeOffset(result.DateOfIncident, 'M/D/YY');
                    if (this.Safety.SafetyAttachments.length) {
                        this.$translate("DOCUMENT_ATTACHED").then((translation) => {
                            this.AttachmentName = translation;
                        });
                    }
                    this.apiSvc.getById(this.Safety.EmployeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee])
                        .then((employee) => {
                            this.employee = {
                                Name: employee.Name,
                                EmployeeId: employee.EmployeeId,
                                JobDescription: employee.JobDescription.Name,
                                HireDate: moment.tz(employee.HireDate, 'utc').format('MMM D, Y'),
                                ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                            };
                        }, this.onFailure);
                }, this.onFailure);
            }
        }

       
        private onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) {
                this.blockUI.stop();
            }
            this.notificaitonSvc.errorToastMessage();
        }

        private onSuccess = (response: any): void=> {
            if (response.SafetyId > 0) {
                mixpanel.track('Safety ' + (this.safetyId ? "Edited" : "Created"), { SafetyId: response.SafetyId });
                this.safetyId = response.SafetyId;
            }
            this.Safety.DateOfIncident = this.dateSvc.getFromUtcDatetimeOffset(this.Safety.DateOfIncident, 'M/D/YY');
            this.$translate("SAFETY_SAVED").then((translation) => {
                if (moment.tz(this.Safety.DateOfIncident, 'utc') < moment().startOf('month')) {
                    this.$translate("SAFETY_SAVEDFORHISTORICAL").then((AdditionalTranslation) => {
                        this.alertSvc.alertWithCallback(translation + ' ' + AdditionalTranslation, this.redirectToEntryView);
                    });
                }
                else {
                    this.alertSvc.alertWithCallback(translation, this.redirectToEntryView);
                }
            });
        }

        /** @description checks to see if IncidentType object is selected when initializing. **/
        private isIncidentSelected = (id: number): boolean => {
            var foundIncident: boolean = false;
            angular.forEach(this.Safety.IncidentTypes, (incidentType) => {
                if (incidentType == id) {
                    foundIncident = true;
                }
            });
            return foundIncident;
        }

        /** @description toggles whether IncidentType object is selected when that object is clicked. **/
        private toggleIncidentType = (id: number): void => {
            var index: number = 0;
            var foundIncident: boolean = false;

            angular.forEach(this.Safety.IncidentTypes,(incidentType) => {
                if (incidentType == id) {
                    this.Safety.IncidentTypes.splice(index, 1);
                    foundIncident = true;
                }
                index++;
            });
            if (!foundIncident) {
                this.Safety.IncidentTypes.push(id);
            }
        }

        /** @description Create/update a new Safety. */
        private saveSafety = ($event): void => {
            if ($event) {
                $event.preventDefault();
            }
            if (this.Safety.IncidentTypes.length == 0) {
                this.$translate('SAFETY_SELECTINCIDENT').then((translation) => {
                    this.alertSvc.alert({ message: translation });
                });
            }
            else {
                this.Safety.DateOfIncident = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.Safety.DateOfIncident);
                if (this.safetyId > 0) {
                    this.SafetySvc.updateSafety(this.safetyId, this.Safety).then(this.onSuccess, this.onFailure);
                } else {
                    var params = {
                        employeeId: this.Safety.EmployeeId,
                        kpiId: Common.Enums.KPITYPE.Safety

                    };
                    this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                        this.Safety.IsExempt = false;
                        if (result.IsExempt != null) {
                            this.Safety.IsExempt = result.IsExempt;
                        }
                        this.SafetySvc.saveSafety(this.Safety).then(this.onSuccess, this.onFailure);
                    }, this.onFailure);
                }
            }
        }

        private addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <personnelInterfaces.ISafetyAttachment>{
                UniqueId: null,
                AttachmentType: 'Image',
                SafetyAttachmentTypeId: 1
            }

            if (files && files.length) {
                this.AttachmentName = files[0].name;
                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) {
                        this.notificaitonSvc.errorToastMessage('Could not upload file. Internal permissions Error');
                        return;
                    }

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead)
                               .then((data: commonInterfaces.IAwsObjectDetails) => {
                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.Safety.SafetyAttachments.length) {
                            this.Safety.SafetyAttachments[0] = tempAttachment;
                        } else {
                            this.Safety.SafetyAttachments.push(tempAttachment);
                        }
                    },(error: any) => {
                        this.onFailure(error);
                    },(progress: any) => {
                        //file.progress = progress;
                    });
                });
            }
        }

        private redirectToEntryView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Safety/EntryView/' + this.safetyId);
        }

        private redirectToDetailsView = (): void=> {
            window.location.replace(this.basePath + 'Personnel/Safety/Details/');
        }

        /** @description Open the modal instance to show the employees avg score by area */
        private showEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Personnel.SafetyEmployeesCtrl as vm',
                size: 'md',
                resolve: {
                    employee: () => { return this.employeesWithAttachment },
                    employeeId: () => { return this.employeeId }
                }
            }).result.then(returned => {
                if (returned) {
                    this.Safety.EmployeeId = returned;
                    this.employee = _.find(this.employeesWithAttachment, "EmployeeId", returned);
                } else {
                    this.redirectToDetailsView();
                }
            },() => {
                this.redirectToDetailsView();
            });
        }
    }

    class SafetyEmployeesCtrl implements personnelInterfaces.IPersonnelEmployeesModalCtrl {

        basePath: string;
        modalTitle: string = 'SELECT EMPLOYEE';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        defaultImg = "~/Content/img/user.jpg";
        btnLabel = "SAFETY_REPORTINCIDENT";
        static $inject = ['$scope', 'sitesettings', '$uibModalInstance', 'employee', 'employeeId', '$translate'];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee: any[],
            private employeeId: number,
            private $translate) {
            this.basePath = sitesettings.basePath;
            this.$translate(this.modalTitle).then((translation) => {
                this.modalTitle = translation;
            });
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.body.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.selectEmployee.footer.tmpl.html';
        }

        cancel = (): void=> {
            this.$uibModalInstance.close(this.employeeId);
        };

        run = (empId: number): void => {
            this.employeeId = empId;
            this.$uibModalInstance.close(this.employeeId);
        }
    }
    angular.module('app').controller('Personnel.SafetyEntryCtrl', SafetyEntryCtrl);
    angular.module('app').controller('Personnel.SafetyEmployeesCtrl', SafetyEmployeesCtrl);
} 