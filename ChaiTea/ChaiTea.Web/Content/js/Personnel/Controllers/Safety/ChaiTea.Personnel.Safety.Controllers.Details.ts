﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Personnel.Safety.Controllers {

    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import personnelSvc = ChaiTea.Personnel.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;

    interface IEmployee {
        EmployeeId: number;
        EmployeeName: string;
    }

    class SafetyDetailsCtrl {

        siteId: number = 0;
        basePath: string;
        
        recordsToSkip: number = 0;
        top: number = 20;

        incidentTypes = ChaiTea.Common.Safety.Enums.StaticIncidentTypes.AsList;
        safety = [];
        employees = [];
        showFilter: boolean = false;
        filterEmployeeId: number = 0;
        filterIncidentTypeId: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.personnel.services.safetysvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id,
            'chaitea.common.services.notificationsvc'];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private safetySvc: personnelSvc.ISafetySvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.HeaderDateChange, this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                this.getSafetyReset({
                    startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
                });
            });
        }

        /** @description Initializer for the controller. */
        public initialize = (): void => {
            mixpanel.track("Viewed Safety");
            this.getEmployeesList();
        }

        private getEmployeesList = () => {
            this.apiSvc.getLookupList('LookupList/Employees', true).then((result) => {
                this.employees = result.Options;
            }, this.onFailure);
        }

        private getSafetyReset = (dateRange: commonInterfaces.IDateRange): void => {
            this.safety = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.getSafety(dateRange);
        }


        /**
         * @description Get attendance issues and push to array for infinite scroll.
         */
        private getSafety = (dateRange: commonInterfaces.IDateRange): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };
            const sDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(dateRange.startDate));
            const eDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(dateRange.endDate));
            var params = <personnelSvc.IODataPagingParamModel>{
                $orderby: 'DateOfIncident desc',
                $top: this.top,
                $skip: this.recordsToSkip,
                $filter: `DateOfIncident gt DateTime'${sDate}' and DateOfIncident le DateTime'${eDate}'`
            };

            if (this.filterEmployeeId > 0) {
                params['$filter'] += `and EmployeeId eq ${this.filterEmployeeId} `;
            }

            if (this.filterIncidentTypeId > 0) {
                params['$filter'] += `and IncidentTypes/any(incidentTypeId: incidentTypeId eq ${this.filterIncidentTypeId}) `;
            }


            this.safetySvc.getSafety(params)
                .then((data) => {
                    if (!data.length) {
                        this.isAllLoaded = true;
                    }
                    angular.forEach(data, (incident: any) => {
                        incident.Employee = {
                            EmployeeId: incident.EmployeeId,
                            Name: incident.EmployeeName,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(incident.UserAttachments, true)
                        };
                        incident.DateOfIncident = this.dateSvc.getFromUtcDatetimeOffset(incident.DateOfIncident, 'MMM D, Y');
                        this.safety.push(incident);
                    }, this.onFailure);
                    this.isBusy = false;
                    this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        private getIncidentTypeCode = (id: number): string => {
            var code: string = 'Unknown';
            var elem = _.find(ChaiTea.Common.Safety.Enums.StaticIncidentTypes.AsList, (f) => { return f.IncidentTypeId == id; });
            if (elem) {
                code = elem.Code;
            }
            return code;
        }

        private toggleFilter = (): void => {
            this.showFilter = !this.showFilter;
            this.filterEmployeeId = 0;
            this.filterIncidentTypeId = 0;
        }

        private onEmployeeChange = (empId: number): void => {
            this.filterEmployeeId = empId;
        }

        private onIncidentTypeChange = (typeId: number): void => {
            this.filterIncidentTypeId = typeId;
        }

        private getSafetyOnFilter = (): void => {
            this.getSafetyReset({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }

        private cancel = (): void => {
            this.filterEmployeeId = 0;
            this.filterIncidentTypeId = 0;
            this.getSafetyOnFilter();
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }
    }

    angular.module('app').controller('Personnel.SafetyDetailsCtrl', SafetyDetailsCtrl);
} 