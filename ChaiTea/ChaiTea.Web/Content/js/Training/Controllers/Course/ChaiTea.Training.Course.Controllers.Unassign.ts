﻿module ChaiTea.Training.Course.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;

    interface IJobCoursePayloadVm {
        CliendId: number; SiteId: number; JobIds: Array<number>; CourseIds: Array<number>; IsUnassign: boolean
    }

    interface IPositionEmployeeItems {
        name: string;
        items: Array<any>;
        changed: boolean;
    }

    class CourseUnassign implements commonInterfaces.INgController {
        tabs: any = [
            { title: 'By Position' },
            { title: 'By Employee'},
           
        ];

        positionEmployeeItems: Array<IPositionEmployeeItems>;

        courses = {
            items: [],
            selectedCourse:null,
            types: [],
            selectedType: null,
            assigned:[]
        };

        employeeCourses = {
            items: [],
            courses:[],
            selectedEmployee:null,
            filter: null,
            assigned:[]
        };

        MasterSelection;

        isTrainingManager = true;

        lastViewableColumnIndex;
        selectedVm: IAssignCourseSelectedVm = {
            clients: <any>[],
            sites: <any>[],
            positions: <any>[],
            employees: <any>[],
            courses: <any>[]
        };
        payloadVm;


        isSelection: boolean = false;

        hasFormChanged;

        refreshIsBusy: boolean = false;

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$q',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc',
            'blockUI'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: ng.IWindowService,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private blockUI) {}

        public initialize = (): void => {
            this.getCourseTypes();

            this.$scope.$watch(()=>{return this.courses.items},(newVal, oldVal) => {
                if (newVal == oldVal) return;
            })
        }

        public toggleCourseType = (courseType) => {
            this.courses.selectedType = (courseType != this.courses.selectedType) ? courseType : null;
            this.courses.selectedCourse = null;
        }

        private getCourseTypes = () => {
            if (this.courses.assigned && this.courses.assigned.length) return;

            var params = {
                $select: 'CourseType',
                $expand:'CourseType'
            }

            this.blockUI.start('Loading Course Categories...');

            this.apiSvc.getByOdata({}, 'JobCourse/ConciseJobCourse')
                .then((res) => {
                    if (!res) return;
                    
                    this.courses.items = _.uniq(_.pluck(res, 'Course'),(item)=>(item.CourseId));
                    this.courses.types = _.uniq(_.pluck(this.courses.items, 'CourseType'),(item)=>(item.CourseTypeId));
                    this.courses.assigned = res;

                    if (this.blockUI) this.blockUI.stop();
                }, this.handleFailure);
        }

        public removeJobAssignment = ($index, item) => {
            if (!item) return;

            bootbox.confirm("Are you sure you want to remove this assignment?",(res) => {
                if (!res) return;

                var _item = item;
                this.apiSvc.delete(item.JobCourseId, "JobCourse").then((res) => {
                    if (!res) this.handleFailure("Something failed.");
                    _.remove(this.courses.assigned, (e) => (e.JobCourseId == _item.JobCourseId));
                    this.notificationSvc.successToastMessage('Course assignment removed.');
                    this.refreshIsBusy = false;
                });
            });
        }

        public refreshJobCourses = () => {
            if (this.refreshIsBusy) return;

            this.courses.assigned = null;
            this.getCourseTypes();
        }

        private getEmployeeCourses = () => {
            if (this.employeeCourses.assigned && this.employeeCourses.assigned.length) return;
            var params = {};

            this.blockUI.start('Loading Employee Courses...');

            this.apiSvc.getByOdata(params, "EmployeeCourse/ConciseEmployeeCourse").then((res) => {
                if (!res) return;

                //add unique records that contain courses
                this.employeeCourses.items = _.uniq(_.where(res,(item) => (item.Course != null)),(item: any) => (item.OrgUserId));

                this.employeeCourses.assigned = res;

                if (this.blockUI) this.blockUI.stop();
            },this.handleFailure);
        }

        public removeEmployeeAssignment = ($index, item) => {
            if (!item) return;

            bootbox.confirm(`Are you sure you want to remove this assignment?`, (res) => {
                if (!res) return;

                var _item = item;
                this.apiSvc.delete(item.EmployeeCourseId, "EmployeeCourse").then((res) => {
                    if (!res) this.handleFailure("Something failed.");
                    _.remove(this.employeeCourses.assigned, (e) => (e.EmployeeCourseId == _item.EmployeeCourseId));
                    this.notificationSvc.successToastMessage('Course assignment removed.');
                }, this.handleFailure);
            });

        }

        public refreshEmployeeCourses = () => {
            this.employeeCourses.assigned = null;
            this.getEmployeeCourses();
        }


        private handleFailure = (reason) => {
            if (this.blockUI) this.blockUI.stop();
            this.$log.error(reason);
        }
    }

    angular.module('app').controller('Training.CourseUnassign', CourseUnassign);


    angular.module('app').filter('assignedHeading',() => {
        return (input) => {
            if (!input) return input;
            var name = `${input.Client.Name} > ${input.Site.Name}`;
            return name;
        }
    })
} 