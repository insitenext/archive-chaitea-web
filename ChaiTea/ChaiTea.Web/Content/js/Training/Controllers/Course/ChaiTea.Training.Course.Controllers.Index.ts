﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    class CourseIndex implements commonInterfaces.INgController {

        basePath: string;
        courses: Array<serviceInterfaces.ICourse> = [];

        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            'chaitea.common.services.apibasesvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
        }

        public initialize = (): void => {

            this.getAllCourses();

        }

        public rowClick = (index: number): void => {

            window.location.replace(this.basePath + 'Training/Course/Entry/' + this.courses[index].CourseId);

        }

        private getAllCourses = (): void => {

            this.apiSvc.get(serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Course]).then((courses: Array<serviceInterfaces.ICourse>) => {
                
                angular.forEach(courses,(course) => {
                    
                    if (course.Overview.length > 150) {
                        course.Overview = course.Overview.slice(0, 150) + ' ...';
                    }

                    var trainings = _.filter(course.CourseAttachments, { CourseAttachmentType: 'Content' });
                    course.TotalCourseTrainings = trainings.length;

                    this.courses.push(course);

                });

            });

        }

    }

    angular.module('app').controller('Training.CourseIndex', CourseIndex);
} 