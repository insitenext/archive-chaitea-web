﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    export enum CourseAttachmentTypes { Content, Cover };

    class CourseEntry implements commonInterfaces.INgController {

        completed: boolean = false;
        basePath: string;
        courseForm: ng.IFormController;
        activeTab: number = 0;
        tabsDisabled: Array<boolean> = [false, true, true, true];

        courseDetailsForm: ng.IFormController;
        attachmentsConfirmed: boolean = false;
        coverImage: string;

        course: serviceInterfaces.ICourse = {
            Name: '',
            Overview: '',
            DeadlineHire: 0,
            DeadlineHireType: 'd',
            RetakeAfter: 0,
            RetakeAfterType: 'd',
            OrgType: '',
            EmployeeId: null,
            CourseAttachments: [],
            Tags: [],
            CourseTypeId: null,
            IsFeatured: false,
            coverImage: '',
            AverageRating: 0
        };
        //{TagId: 0, Name: "testing"}

        files = [];
        creds: commonInterfaces.ISecurity;

        employeesSbm: Array<serviceInterfaces.IEmployee> = [];
        employees: Array<serviceInterfaces.IEmployee> = [];
        employee: serviceInterfaces.IEmployee;

        newAttachmentQuestion: serviceInterfaces.ICourseAttachmentQuestion;
        newAttachmentQuestionOption: Array<serviceInterfaces.ICourseAttachmentQuestionOption>;

        courseType: serviceInterfaces.ICourseType;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];

        languages: Array<serviceInterfaces.ILanguage>;

        client: serviceInterfaces.ILookupListOption;
        clients: Array<serviceInterfaces.ILookupListOption>;

        site: serviceInterfaces.ILookupListOption;
        sites: Array<serviceInterfaces.ILookupListOption>;

        selectedAttachment: serviceInterfaces.ICourseAttachment;
        selectedAttachmentIndex: number;

        isCollapsed: Array<boolean> = [];
        tags: Array<any> = [
            { text: 'just' },
            { text: 'some' },
            { text: 'cool' },
            { text: 'tags' }
        ];
        modalInstance;

        static $inject = [
            '$scope',
            '$log',
            '$uibModal',
            '$timeout',
            '$q',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.trainingsvc',
            Common.Services.NotificationSvc.id
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $timeout: ng.ITimeoutService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private lookupListSvc: ChaiTea.Services.ILookupListSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilsSvc: ChaiTea.Common.Services.IUtilitySvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private notificationSvc: Common.Services.INotificationSvc) {

            this.apiSvc.setEndPoint('Course');
            this.basePath = sitesettings.basePath;

        }

        public initialize = (courseId?: number): void => {

            this.loadData(courseId);

        }

        /**
        * @description Load all data needed to build course.
        */
        private loadData = (courseId): void => {

            async.parallel([
                //course
                (callback) => {
                    if (courseId) {
                        //load course data
                        this.apiSvc.getById(courseId).then((course: serviceInterfaces.ICourse) => {

                            this.course = course;
                            callback(null, course);

                        });
                    } else {
                        callback(null, null);
                    }
                },
                //employees
                //load SBM by default
                (callback) => {
                    //TODO: (JG) Add endpoint to lookup SBM clients
                    this.apiSvc.get("Employee/10009/10034").then((data) => {

                        this.employees = data;
                        this.employeesSbm = data;
                        callback(null, data);

                    });
                },
                //course types
                (callback) => {
                    this.apiSvc.get("Lookuplist/CourseTypes").then((data) => {

                        //check client and filter out types
                        data = this.trainingSvc.filterCourseTypes(this.userContext.Client, data);

                        this.courseTypes = data;
                        callback(null, data);

                    });
                },
                //languages
                (callback) => {
                    this.apiSvc.get("Language").then((data) => {

                        this.languages = data;
                        callback(null, data);

                    });
                },
                (callback) => {
                    //TODO: already pulled in header, should pull from cached version

                    this.apiSvc.getLookupList("User/Clients").then((data) => {

                        this.clients = data.Clients.Options;

                        callback(null, data);

                    });
                }

            ], (err, res) => {

                if (err) return this.$log.error(err);

                if (courseId) {

                    this.attachmentsConfirmed = true;

                    //set values
                    this.courseType = this.courseTypes[_.findIndex(this.courseTypes, { CourseTypeId: this.course.CourseTypeId })];
                    this.client = this.clients[_.findIndex(this.clients, { Key: this.course.OrgClient.Id })];

                    if ((this.course.OrgType == commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Site]) || (this.course.OrgType == commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Client])) {
                        this.course.OrgType = commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Client];
                    }

                    //load files
                    this.updateFiles();

                    //unlock tabs
                    this.tabsDisabled = [false, false, false, false]; 

                    //load sites
                    if (this.course.OrgType != commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Organization]) {
                        this.apiSvc.getLookupList("User/Sites/" + this.course.OrgClient.Id + "/false").then((data) => {

                            this.sites = data.Sites.Options;
                            
                            var siteIndex = _.findIndex(this.sites, { Key: this.course.OrgSite.Id });
                            var index = (siteIndex > -1 ? siteIndex : 0);
                            this.site = this.sites[index];

                            this.loadEmployeesBySite(this.client.Key, this.site.Key);

                        });
                    } else {
                        this.employee = this.employees[_.findIndex(this.employees, { EmployeeId: this.course.EmployeeId })];
                    }

                    //angular.forEach(this.course.CourseQuizQuestions,(question: serviceInterfaces.ICourseQuizQuestion, key: number) => {
                    //    this.isCollapsed[key] = true;
                    //});
                }

            });

        }

        /**
        * @description Maps attachments to temp file array
        */
        public updateFiles = (): void => {

            //this.files = [];
            var counter: number = 0;

            this.course.CourseAttachments = _.sortBy(this.course.CourseAttachments, 'OrderPriority');

            angular.forEach(this.course.CourseAttachments,(file: any) => {

                if (file.CourseAttachmentType == CourseAttachmentTypes[CourseAttachmentTypes.Cover]) {
                    this.coverImage = this.awsSvc.getUrlByAttachment(file.Attachment.UniqueId, file.Attachment.AttachmentType);
                }    

                if (file.CourseAttachmentType == CourseAttachmentTypes[CourseAttachmentTypes.Content]) {

                    if (file.originalUniqueId) {
                        var index = _.findIndex(this.files, { originalUniqueId: file.originalUniqueId });
                    } else {
                        var index = _.findIndex(this.files, { Attachment: { UniqueId: file.Attachment.UniqueId } });
                    }

                    if (index > -1) {
                        this.files[index].Attachment.Name = file.Attachment.Name;
                        this.files[index].Attachment.AttachmentType = file.Attachment.AttachmentType;
                        this.files[index].Attachment.AttachmentTypeId = file.Attachment.AttachmentTypeId;
                        this.files[index].Attachment.UniqueId = file.Attachment.UniqueId;
                        this.files[index].OrderPriority = counter;

                        this.files[index].CourseSubAttachments = this.updateSubFiles(file.CourseSubAttachments);
                        
                    } else {

                        //set custom/file props
                        file.exists = true;
                        file.uploaded = true;
                        file.class = 'success';
                        file.cdn = this.awsSvc.getUrlByAttachment(file.Attachment.UniqueId, file.Attachment.AttachmentType);
                        file.icon = this.awsSvc.getFileIcon(file.Attachment.AttachmentType);
                        file.OrderPriority = counter;
                        file.originalUniqueId = null;
                        file.CourseSubAttachments = this.updateSubFiles(file.CourseSubAttachments);

                        this.files.push(file);
                        
                    }

                    counter++;
                }

                file.hasQuiz = file.CourseAttachmentQuestions.length ? true : false;
            });

        }

        public updateSubFiles = (attachments: Array<serviceInterfaces.ICourseAttachment>): Array<any> => {

            var subCounter: number = 0;
            var subFiles = [];

            attachments = _.sortBy(attachments, 'OrderPriority');

            angular.forEach(attachments, (subAttachment: any) => {

                subAttachment.icon = this.awsSvc.getFileIcon(subAttachment.Attachment.AttachmentType);
                subAttachment.uploaded = true;
                subAttachment.OrderPriority = subCounter;

                subFiles.push(subAttachment);
                subCounter++;

            });

            return subFiles;

        }

        /**
        * @description Fires autocomplete on keyup.
        */
        public loadTags = (query): any => {

            var param: Object = {
                $filter: "startswith(Name,'" + query + "')"
            };
            return this.apiSvc.getByOdata(param, "Tag");
        }

        /**
        * @description Fires on tab change.
        */
        public changeTab = (index: number, direction: string): void => {

            try {

                if (direction) {

                    this.selectedAttachment = null;
                    
                    var contentAttachments = _.filter(this.course.CourseAttachments, { CourseAttachmentType: CourseAttachmentTypes[CourseAttachmentTypes.Content] });

                    if (index == 0 && !this.courseDetailsForm.$valid) throw 'Please complete all required fields for continuing.';

                    if (index == 0 && !this.coverImage) throw 'Please upload a course cover image.';

                    if (index == 1 && !contentAttachments.length) throw 'Please upload course content before continuing.';

                    if (index == 1 && !this.attachmentsConfirmed) throw 'Please confirm that you have verified all attachments.';

                    if (index == 2) {
                        angular.forEach(contentAttachments, (attachment) => {
                            if (attachment.hasQuiz) {
                                if (!attachment.CourseAttachmentQuestions || attachment.CourseAttachmentQuestions.length < 3) throw 'All Course material must have at least three questions attached or select NO QUIZ if you do not wish to add quiz ';

                                angular.forEach(attachment.CourseAttachmentQuestions, (question) => {

                                    if (!question.CourseAttachmentQuestionOptions || question.CourseAttachmentQuestionOptions.length < 2) throw 'Course material quiz questions must have at least two answers attached.';

                                });
                            }
                            else {
                                attachment.CourseAttachmentQuestions = [];
                            }

                        });
                    }
  
                }

                if (direction && direction == 'next') {
                    index += 1;
                }

                if (direction && direction == 'back') {
                    index -= 1;
                }

                this.activeTab = index;
                this.tabsDisabled[index] = false;

            } catch (error) {
                this.notificationSvc.errorToastMessage(error);

             }

        }

        /**
        * @description Fires on type change.
        */
        public typeChange = (): void => {
            
            if (this.course.OrgType == commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Organization]) {
                this.employees = this.employeesSbm;
            }

        }

        /**
        * @description Fires on client change.
        */
        public clientChange = (): void => {
            //http://localhost/ChaiTea.Web/api/Services/User/Sites/10001/false
            //LookupList/Employees/10001/10025

            this.employees = [];

            this.apiSvc.getLookupList("User/Sites/" + this.client.Key + "/false").then((data) => {

                this.sites = data.Sites.Options;

            });

        }

        /**
        * @description Fires on site change.
        */
        public siteChange = (): void => {

            var siteId = (this.site ? this.site.Key : null);
            this.loadEmployeesBySite(this.client.Key, siteId);

        }

        /**
        * @description Deletes course.
        */
        public delete = (): void => {

            this.alertSvc.confirmWithCallback('Are you sure you want to delete this course? All employees must be unassigned from the course.',(res: boolean) => {

                if (res) {

                    //make sure all employees are unassigned
                    this.apiSvc.get('EmployeeCourse/AllByCourseId/' + this.course.CourseId).then((employeeCourses: Array<serviceInterfaces.IEmployeeCourse>) => {

                        if (employeeCourses.length > 0) return this.notificationSvc.errorToastMessage('Please remove all employees before deleting course.');

                        //delete me
                        this.apiSvc.delete(this.course.CourseId, 'Course').then((res: any) => {

                            this.notificationSvc.successToastMessage('Course deleted.');

                            this.$timeout(() => {
                                window.location.replace(this.basePath + 'Training/Course/');
                            }, 2000);

                        });

                    });



                }

            });

        }

        /**
        * @description Uploads file to s3.
        */
        public transferFiles = (): void => {
            //upload files and fire off encoding, if video
            angular.forEach(this.files,(file: any) => {

                if (!file.uploaded) {

                    file.icon = this.awsSvc.getFileIcon(null);
                    
                    this.awsSvc.s3MultipartUpload(file, this.creds).then((data: commonInterfaces.IAwsObjectDetails) => {
                        
                        if (data.Folder === 'video') {
                            this.awsSvc.encodeVideo(data.Name, this.creds);
                        }

                        //make file name pretty
                        var fileName = file.name.split('.').shift();
                        fileName = fileName.charAt(0).toUpperCase() + fileName.slice(1);

                        file.OrderPriority = this.course.CourseAttachments.length;
                        file.Attachment = {
                            Name: fileName,
                            UniqueId: data.Name,
                            AttachmentType: data.AttachmentType,
                            AttachmentTypeId: data.AttachmentTypeId
                        };

                        file.class = 'success'
                        file.icon = this.awsSvc.getFileIcon(data.AttachmentType);
                        file.uploaded = true;

                        this.addAttachment(fileName, data.Name, data.AttachmentType, data.AttachmentTypeId, 'Content', false);

                        this.attachmentsConfirmed = false;

                    },(error: any) => {

                            file.class = 'error';

                        },(progress: any) => {

                            file.progress = progress;

                        });

                }

            });
        }

        /**
        * @description Gets s3 creds and starts file transfer.
        */
        public upload = ($files, $event): void => {
            
            if ($files && $files.length) {
                
                this.getCreds().then((creds: commonInterfaces.ISecurity) => {
                    this.transferFiles();
                });

            }

        }

        /**
        * @description Gets s3 creds and uploads the cover image.
        */
        public uploadCoverImage = ($file, $event): void => {

            if ($file) {
                
                this.readFile($file).then((img: any) => {

                    if (img.width != 780 || img.height != 470) {
                        this.notificationSvc.errorToastMessage("Cover Image must be 780x470");
                        return;
                    }

                    //TODO: update using the base64

                    this.getCreds().then((creds: commonInterfaces.ISecurity) => {

                        this.awsSvc.s3Upload($file, this.creds, commonInterfaces.S3Folders.image, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                            //make file name pretty
                            var fileName = $file.name.split('.').shift();
                            fileName = fileName.charAt(0).toUpperCase() + fileName.slice(1);

                            this.addAttachment(fileName, data.Name, data.AttachmentType, data.AttachmentTypeId, 'Cover', true);

                            this.coverImage = data.Url;

                        }, (error: any) => {

                            $file.class = 'error';

                        });

                    });

                });
                
            }

        }

        setNoQuizFlag = () => {
            if (this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions.length) {
                bootbox.confirm("You will lose the existing quiz and all its questions for this training, if you choose 'NO QUIZ'", (res) => {
                    if (res) {
                        this.course.CourseAttachments[this.selectedAttachmentIndex].hasQuiz = !this.course.CourseAttachments[this.selectedAttachmentIndex].hasQuiz;
                        this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions = [];
                        this.$scope.$apply();
                    }
                });
            } else {
                this.course.CourseAttachments[this.selectedAttachmentIndex].hasQuiz = !this.course.CourseAttachments[this.selectedAttachmentIndex].hasQuiz;
            }
            
        }

        /**
        * @description Adds questions to selected attachment .
        */
        public attachmentChange = (): void => {
            this.selectedAttachmentIndex = this.getSelectedIndex();
        }

        /**
        * @description Adds questions to selected attachment .
        */
        public addAttachmentQuestion = ($e): void => {

            if (!this.newAttachmentQuestion.Question || !this.selectedAttachment) return;

            //find attachment
            this.selectedAttachment = this.course.CourseAttachments[this.selectedAttachmentIndex];

            this.newAttachmentQuestion.OrderPriority = this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions.length + 1;

            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions.push(_.clone(this.newAttachmentQuestion, true));

            //doesn't work
            //angular.element('#new-answer-' + (this.course.CourseQuizQuestions.length - 1)).focus();
            this.newAttachmentQuestion.Question = '';

        }

        /**
        * @description Adds option to question.
        */
        public addAttachmentOption = (index: number, $e): void => {

            if (!this.newAttachmentQuestionOption[index].Option && !this.selectedAttachmentIndex) return;            
    
            if (!this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[index].CourseAttachmentQuestionOptions) {
                this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[index].CourseAttachmentQuestionOptions = [];
                this.newAttachmentQuestionOption[index].IsAnswer = true;
            }

            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[index].CourseAttachmentQuestionOptions.push(_.clone(this.newAttachmentQuestionOption[index]));

            this.newAttachmentQuestionOption[index].IsAnswer = false;
            this.newAttachmentQuestionOption[index].Option = '';

        }

        /**
        * @description Removes options.
        */
        public removeAttachmentOption = (parentIndex: number, index: number, $e): void => {
            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[parentIndex].CourseAttachmentQuestionOptions.splice(index, 1);
        }

        /**
        * @description Updates selected question on change
        */
        public questionChange = (index, $e): void => {
            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[index] = this.selectedAttachment.CourseAttachmentQuestions[index];
        }

        /**
        * @description Updates selected question option on change
        */
        public questionOptionChange = (parentIndex: number, index: number, $e): void => {
            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[parentIndex].CourseAttachmentQuestionOptions[index] = this.selectedAttachment.CourseAttachmentQuestions[parentIndex].CourseAttachmentQuestionOptions[index];
        }

        /**
        * @description Updates answer.
        */
        public answerChange = (parentIndex: number, index: number, $e): void => {
            _.map(this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[parentIndex].CourseAttachmentQuestionOptions,(answer: serviceInterfaces.ICourseAttachmentQuestionOption) => {
                answer.IsAnswer = false;
            });
            this.course.CourseAttachments[this.selectedAttachmentIndex].CourseAttachmentQuestions[parentIndex].CourseAttachmentQuestionOptions[index].IsAnswer = true;

        }

        private getSelectedIndex = (): number => {
            
            if (!this.selectedAttachment) return null;

            return _.findIndex(this.course.CourseAttachments, { Attachment: { UniqueId: this.selectedAttachment.Attachment.UniqueId } });
        }

        /**
        * @description Removes file.
        */
        public removeItemFromList($e: any, attachmentUniqueId: string) {
            //$e.preventDefault();

            var $self = this;
            this.alertSvc.confirmWithCallback('Are you sure you want to delete this uploaded course file?',(res: boolean) => {

                this.$timeout(function () {

                    if (res) {
                        
                        var attachmentInex = _.findIndex($self.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId }});
                        $self.course.CourseAttachments.splice(attachmentInex, 1);

                        var fileIndex = _.findIndex($self.files, { Attachment: { UniqueId: attachmentUniqueId } });
                        $self.files.splice(fileIndex, 1);

                        $self.$scope.$apply();
                        
                    }
                }, 1000);

            });

        }

        /**
        * @description Removes file from sub attachment array.
        */
        public removeSubItemFromList($e: any, attachmentUniqueId: string, subUniqueId: string) {
            
            var $self = this;
            this.alertSvc.confirmWithCallback('Are you sure you want to delete this uploaded course file?', (res: boolean) => {
                
                if (res) {

                    var attachmentIndex: number = _.findIndex($self.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId } });
                    var attachmentSubIndex: number = _.findIndex($self.course.CourseAttachments[attachmentIndex].CourseSubAttachments, { Attachment: { UniqueId: subUniqueId } });
                    $self.course.CourseAttachments[attachmentIndex].CourseSubAttachments.splice(attachmentSubIndex, 1);
                    $self.$scope.$apply();

                }
            });
        }

        /**
        * @description Moves array index up and down.
        */
        public move = ($e, index: number, parentUniqueId: string): void => {

            var direction: number = 0;
            var tmpList: Array<serviceInterfaces.ICourseAttachment> = _.cloneDeep(this.course.CourseAttachments);
            var contentList: Array<serviceInterfaces.ICourseAttachment> = _.filter(tmpList, { CourseAttachmentType: CourseAttachmentTypes[CourseAttachmentTypes.Content] });
            var current: any = contentList[index];

            if (parentUniqueId) {
                var parent = _.find(contentList, { Attachment: { UniqueId: parentUniqueId} });
                contentList = parent.CourseSubAttachments;
                current = contentList[index];
            }

            //can't sort while uploading
            var lock: boolean = false;
            angular.forEach(this.files, (file) => {
                if (!file.uploaded) {
                    return lock = true;
                }
                angular.forEach(file.CourseSubAttachments,(subFile) => {
                    if (!subFile.uploaded) {
                        return lock = true;
                    }
                });
            });

            if (!lock) {

                //if down
                if ($e.offsetY > 10 && ((index + 1) <= (contentList.length - 1))) {
                    direction = 1;
                }

                //if up
                if ($e.offsetY <= 10 && index > 0) {
                    direction = -1;
                }

                //swap places
                if (direction != 0) {
                    contentList[index] = contentList[index + direction];
                    contentList[index + direction] = current

                    //update order
                    var counter: number = 0;
                    _.map(contentList, (item) => {

                        //update master list
                        var masterItem: serviceInterfaces.ICourseAttachment = null;
                        if (parentUniqueId) {
                            var masterParent = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: parentUniqueId } });
                            masterItem = _.find(masterParent.CourseSubAttachments, { Attachment: { UniqueId: item.Attachment.UniqueId } });
                        } else {
                            masterItem = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: item.Attachment.UniqueId } })
                        }

                        masterItem.OrderPriority = counter;
                        counter++;
                    });

                    this.updateFiles();

                }
            }

        }

        /**
        * @description Saves course.
        */
        public saveCourse = ($event: any): void => {
            var self = this;
            var tags: Array<serviceInterfaces.ITag> = [];

            //update payload
            this.course.EmployeeId = this.employee.EmployeeId;
            this.course.CourseTypeId = this.courseType.CourseTypeId;
            
            //add client, if selected
            if (this.client && this.client.Key) {
                this.course.OrgClient = { Id: this.client.Key, Name: this.client.Value };
            } 
            
            if (this.site && this.site.Key) {
                this.course.OrgType = commonInterfaces.OrgTypes[commonInterfaces.OrgTypes.Site];
                this.course.OrgSite = { Id: this.site.Key, Name: this.site.Value };
            }  
            
            //validate questions

            //save new tags
            if (this.course.Tags.length) {
                async.map(this.course.Tags,(tag, callback) => {
                    if (!tag.TagId) {
                        //save new tags
                        this.apiSvc.save(tag, "Tag").then((tag: serviceInterfaces.ITag) => {
                            callback(null, tag);
                        });
                    } else {
                        callback(null, tag);
                    }
                },
                    (err, res: Array<serviceInterfaces.ITag>) => {

                        if (err) {
                            return this.$log.error(err);
                        }
                
                        //load new tags
                        this.course.Tags = res;

                        //all done save everything
                        this.saveFullCourse();

                    });
            } else {
                this.saveFullCourse();
            }
                       
        }

        /**
        * @description Plays selected video in a modal.
        */
        public playVideo = (attachmentUniqueId: string, subUniqueId: string) => {

            if (subUniqueId != null) {
                var parent = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId }});
                this.selectedAttachment = _.find(parent.CourseSubAttachments, { Attachment: { UniqueId: subUniqueId }});
            } else {
                this.selectedAttachment = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId }});
            }

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.PlayModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    attachment: () => { return this.selectedAttachment; }
                }
            });

        }

        /**
        * @description Loads the attachment edit modal.
        */
        public loadAttachmentModal = (attachmentUniqueId: string, subUniqueId: string) => {

            if (subUniqueId != null) {
                var parent = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId }});
                this.selectedAttachment = _.find(parent.CourseSubAttachments, { Attachment: { UniqueId: subUniqueId } } );
            } else {
                this.selectedAttachment = _.find(this.course.CourseAttachments, { Attachment: { UniqueId: attachmentUniqueId }});
            }


            
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.AttachmentModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    subUniqueId: () => { return subUniqueId; },
                    attachment: () => { return this.selectedAttachment; },
                    courseAttachments: () => { return this.course.CourseAttachments; },
                    languages: () => { return this.languages; }
                }
            });

            this.modalInstance.result.then((updatedAttachment: serviceInterfaces.ICourseAttachment) => {

                if (!updatedAttachment) return;

                this.selectedAttachment = updatedAttachment;

                this.updateFiles();

            }, () => {
                
            });
        }

        /**
        * @description Fires off course save.
        */
        private saveFullCourse = (): void => {

            if (this.course.CourseId) {
                this.apiSvc.update(this.course.CourseId, this.course, "Course").then((res: any) => {
                    //show success msg
                    this.notificationSvc.successToastMessage('Course updated successfully.');
                    this.completed = true;
                }, (error) => { this.$log.error(error); });
            } else {
                this.apiSvc.save(this.course, "Course").then((res: any) => {
                    //show success msg
                    this.notificationSvc.successToastMessage('Course saved successfully.');
                    this.completed = true;
                }, (error) => { this.$log.error(error); });
            }

        }

        /**
        * @description Gets list of employee's by site and client.
        */
        private loadEmployeesBySite = (clientId: number, siteId: number): void => {
        
            if (siteId) {

                this.apiSvc.get('Employee/' + this.client.Key + '/' + siteId).then((res) => {

                    this.employees = res;
                    this.employee = this.employees[_.findIndex(this.employees, { EmployeeId: this.course.EmployeeId })];

                });

            } else {
                //all selected
                //get employees from all sites
                async.map(this.sites, (site, callback) => {
                    if (!site.Key) callback(null, []);

                    this.apiSvc.get('Employee/' + this.client.Key + '/' + site.Key).then((res) => {
                        callback(null, res);
                    });   

                }, (err, res) => {
                    
                    if (err) return this.$log.error(err);

                    var combined: Array<any> = [];
                    angular.forEach(res, (siteEmployees: Array<serviceInterfaces.IEmployee>) => {
                        
                        if (siteEmployees.length) {
                            //this.employees = _.merge(this.employees, siteEmployees);
                            combined = _.union(combined, siteEmployees);
                        }
                        
                    });

                    this.employees = <Array<serviceInterfaces.IEmployee>>_.sortByAll(combined, ['FirstName']);
                    this.employee = this.employees[_.findIndex(this.employees, { EmployeeId: this.course.EmployeeId })];

                });

            }

        }

        /**
        * @description Adds new attachment to the course object.
        */
        private addAttachment = (fileName: string, uniqueId: string, attachmentType: string, attachmentTypeId: number, courseAttachmentType: string, replace?: boolean): void => {
            
            var attachment: serviceInterfaces.ICourseAttachment = {
                Attachment: {
                    UniqueId: uniqueId,
                    Name: fileName,
                    Description: '',
                    AttachmentType: attachmentType,
                    AttachmentTypeId: attachmentTypeId
                },
                CourseAttachmentType: courseAttachmentType,
                CourseAttachmentQuestions: [],
                CourseSubAttachments: [],
                OrderPriority: this.course.CourseAttachments.length,
                Language: this.languages[0],
                hasQuiz: (courseAttachmentType == 'Cover' ? false : true)
            };

            if (replace) {
                //remove all attachments with the same type
                var foundAttachments = _.filter(this.course.CourseAttachments, { CourseAttachmentType: courseAttachmentType });
                angular.forEach(foundAttachments, (foundAttachment: serviceInterfaces.ICourseAttachment) => {
                    var index = _.findIndex(this.course.CourseAttachments, { Attachment: { UniqueId: foundAttachment.Attachment.UniqueId } });
                    this.course.CourseAttachments.splice(index, 1)
                });
            }

            this.course.CourseAttachments.push(attachment);

        }

        /**
        * @description Returns temp creds needed to upload to AWS.
        */
        private getCreds = (): angular.IPromise<any> => {
            var def = this.$q.defer();

            if (!this.creds) {

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.creds = creds;
                    def.resolve(creds);

                });

            } else {
                def.resolve(this.creds);
            }

            return def.promise;

        }

        private readFile = (file: any): angular.IPromise<any> => {
            var def = this.$q.defer();
            var fr = new FileReader;

            fr.onload = function () {
                var img = new Image;

                img.onload = function () {
                    def.resolve(img);
                };

                img.src = fr.result;
            };

            fr.readAsDataURL(file);

            return def.promise;
        }



    }

    angular.module('app').controller('Training.CourseEntry', CourseEntry);

    class PlayModalCtrl {
        
        basePath: string;
        modalTitle: string = 'View Video Attachment';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        selectedAttachment: serviceInterfaces.ICourseAttachment;
        videoThumbnail: string = '';
        videoUrl: string = '';

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'attachment'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private attachment: serviceInterfaces.ICourseAttachment) {

            this.selectedAttachment = attachment;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Training/Templates/Course/course.video.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Training/Templates/Course/course.video.footer.tmpl.html';
            this.modalTitle = attachment.Attachment.Name;


            //get orig video url
            this.awsSvc.getSignedVideoUrl(this.attachment.Attachment.UniqueId).then((res) => {
                this.videoThumbnail = res.Thumbnails[0];
                this.videoUrl = res.Url;
            });

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.close();
        };

    }

    angular.module('app').controller('Training.PlayModalCtrl', PlayModalCtrl);

    class AttachmentModalCtrl {

        basePath: string;
        modalTitle: string = 'Edit course attachment';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        files = [];
        subFiles = [];
        creds: commonInterfaces.ISecurity; 
        selectedAttachment: serviceInterfaces.ICourseAttachment;
        isParent = true;
        languages: Array<serviceInterfaces.ILanguage>;
        englishCourseAttachments: Array<serviceInterfaces.ICourseAttachment>;

        courseAttachmentParentUniqueId: string = '';

        retakeQuiz: boolean = false;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            '$timeout',
            'sitesettings',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.alertsvc',
            'subUniqueId',
            'attachment',
            'courseAttachments',
            'languages'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $timeout: ng.ITimeoutService,
            private sitesettings: ISiteSettings,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private subUniqueId: string,
            private attachment: serviceInterfaces.ICourseAttachment,
            private allAttachments: Array<serviceInterfaces.ICourseAttachment>,
            private globalLanguages: Array<serviceInterfaces.ILanguage>) {

            this.isParent = (subUniqueId != null ? false : true);
            this.selectedAttachment = attachment;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Training/Templates/Course/course.attachment.entry.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Training/Templates/Course/course.attachment.footer.tmpl.html';
            this.languages = globalLanguages;

            if (!this.selectedAttachment.Language) {
                this.selectedAttachment.Language = this.languages[0];
            } else {
                this.selectedAttachment.Language = _.find(this.languages, { LanguageId: this.selectedAttachment.Language.LanguageId });
            }

            this.englishCourseAttachments = _.filter(this.allAttachments, (a) => {
                return (a.Language.LanguageId == 1 && a.Attachment.UniqueId != this.selectedAttachment.Attachment.UniqueId && a.CourseAttachmentType == 'Content');
            });

            if (this.isParent) {
                this.updateFiles();
            }

        }

        /**
        * @description Maps attachment array to temp file array.
        */
        public languageChange = (): void => {
            if (this.selectedAttachment.Language.Code == 'en') {
                this.selectedAttachment.CourseAttachmentParentUniqueId = "";
            }
        }

        /**
        * @description Maps attachment array to temp file array.
        */
        public updateFiles = (): void => {

            angular.forEach(this.selectedAttachment.CourseSubAttachments, (file: any) => {

                var index = _.findIndex(this.subFiles, { Attachment: { UniqueId: file.UniqueId } }); 

                if (index == -1) {
                    file.exists = true;
                    file.class = 'success';
                    file.cdn = this.awsSvc.getUrlByAttachment(file.Attachment.UniqueId, file.Attachment.AttachmentType);
                    file.icon = this.awsSvc.getFileIcon(file.Attachment.AttachmentType);
                    this.subFiles.push(file);
                }  

            });

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.close(this.selectedAttachment);
        };

        /**
        * @description Gets s3 creds needed to file upload.
        */
        public upload = ($files, $file, $event, isParentAttachment): void => {

            if ($files && $files.length) {

                if (!this.creds) {
                    this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                        if (!creds.AccessKey || !creds.AccessSecret) return;

                        this.creds = creds;
                        this.transferFiles(isParentAttachment);

                    });
                } else {
                    this.transferFiles(isParentAttachment);
                }

            }

        }

        /**
        * @description Uploads file to s3.
        */
        public transferFiles = (isParentAttachment): void => {

            var currentFiles = this.subFiles;
            if (isParentAttachment) {
                currentFiles = this.files;
            } 

            //upload files and fire off encoding, if video
            angular.forEach(currentFiles,(file: any) => {

                if (!file.uploaded) {

                    this.awsSvc.s3MultipartUpload(file, this.creds).then((data: commonInterfaces.IAwsObjectDetails) => {

                        if (data.Folder === 'video') {
                            this.awsSvc.encodeVideo(data.Name, this.creds);
                        }

                        //make file name pretty
                        var fileName = file.name.split('.').shift();
                        fileName = fileName.charAt(0).toUpperCase() + fileName.slice(1);

                        file.class = 'success';
                        file.icon = this.awsSvc.getFileIcon(data.AttachmentType);
                        file.uploaded = true;
                        file.Attachment = {
                            AttachmentTypeId: data.AttachmentTypeId,
                            AttachmentType: data.AttachmentType,
                            Name: fileName,
                            Description: '',
                            UniqueId: data.Name,
                        };

                        if (isParentAttachment) {
                            //replace current file
                            this.selectedAttachment.originalUniqueId = this.selectedAttachment.Attachment.UniqueId;
                            this.selectedAttachment.Attachment.UniqueId = data.Name;
                            this.selectedAttachment.Attachment.AttachmentType = data.AttachmentType;
                            this.selectedAttachment.retakeQuiz = this.retakeQuiz;
                        } else {
                            var attachment: serviceInterfaces.ICourseSubAttachment = {
                                Attachment: {
                                    AttachmentTypeId: data.AttachmentTypeId,
                                    AttachmentType: data.AttachmentType,
                                    Name: fileName,
                                    Description: '',
                                    UniqueId: data.Name,
                                },
                                CourseAttachmentType: 'Content',
                                OrderPriority: this.selectedAttachment.CourseSubAttachments.length,
                                LanguageId: this.selectedAttachment.Language.LanguageId,
                                Language: this.selectedAttachment.Language
                            };

                            this.selectedAttachment.CourseSubAttachments.push(attachment);
                        }

                    },(error: any) => {

                        file.class = 'error';

                    },(progress: any) => {

                        file.progress = progress;

                    });
                }

            });
        }

        /**
        * @description Removes file.
        */
        public removeItemFromList($e: any, attachmentUniqueId: string) {
            //$e.preventDefault();

            var $self = this;
            this.alertSvc.confirmWithCallback('Are you sure you want to delete this uploaded course file?', (res: boolean) => {

                this.$timeout(function () {

                    if (res) {

                        var attachmentInex = _.findIndex($self.selectedAttachment.CourseSubAttachments, { UniqueId: attachmentUniqueId });
                        $self.selectedAttachment.CourseSubAttachments.splice(attachmentInex, 1);

                        var fileIndex = _.findIndex($self.files, { UniqueId: attachmentUniqueId });
                        $self.files.splice(fileIndex, 1);

                        $self.$scope.$apply();

                    }

                }, 1000);

            });

        }
    }

    angular.module('app').controller('Training.AttachmentModalCtrl', AttachmentModalCtrl);




    
} 