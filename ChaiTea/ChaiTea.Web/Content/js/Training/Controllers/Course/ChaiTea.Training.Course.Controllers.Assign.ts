﻿module ChaiTea.Training.Course.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;

    export interface IAssignCourseSelectedVm {
        clients: Array<any>;
        sites: Array<any>;
        positions: Array<any>;
        employees: Array<any>;
        courses: any;
    }

    //interface ISelectedVmItem {
    //    name: string;
    //    displaySelected?: { Key: number; Value: string };
    //    selected: Array<any>;
    //    items?: Array<any>;
    //}

    interface IJobCoursePayloadVm {
        CliendId: number; SiteId: number; JobIds: Array<number>; CourseIds: Array<number>; IsUnassign: boolean
    }

    interface IPositionEmployeeItems {
        name: string;
        items?: Array<any>;
        changed: boolean;
    }

    class CourseAssign implements commonInterfaces.INgController {
        tabs: any = [
            { title: 'Clients', active: true, disabled: true, hideSelectAll:true },
            { title: 'Sites', disabled: true, hideSelectAll: true },
            { title: 'Positions', disabled: true, hideSelectAll: true },
            { title: 'Employees', disabled: true },
            { title: 'Select Courses', disabled: true },
            { title: 'Summary', disabled: true },
        ];
        activeTab: number = 0;
        positionEmployeeItems: Array<IPositionEmployeeItems>;

        courses = {
            items: [],
            types: [],
            selectedType: null,
            selectAll: false,
            showFilter: false,
            filter: ''
        };

        MasterSelection;

        isTrainingManager = true;

        lastViewableColumnIndex;
        selectedVm: IAssignCourseSelectedVm = {
            clients: <any>[],
            sites: <any>[],
            positions: <any>[],
            employees: <any>[],
            courses: <any>[]
        };
        payloadVm;


        isSelection: boolean = false;

        hasFormChanged;
        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$q',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc',
            'blockUI'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: ng.IWindowService,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private blockUI) {

        }

        public initialize = (): void => {
            this.initPositionEmployeeItems();
            this.initSelectVm();
            this.initPayloadVm();



            this.$scope.$watch(() => { return this.selectedVm },(newVal, oldVal) => {
                if (newVal == oldVal) return;
                this.hasFormChanged = true;


            }, true);


            this.$scope.$watch(() => { return this.courses.items },(newVal, oldVal) => {
                if (newVal == oldVal) return;

                this.tabs[4].disabled = !this.anyCoursesSelected()


            }, true);


        }


        public getColumn = (arrIndex, column) => {

            if (this.positionEmployeeItems[arrIndex].changed == false) return;

            var clients = _.find(this.positionEmployeeItems, { name: 'clients' }),
                sites = _.find(this.positionEmployeeItems, { name: 'sites' }),
                positions = _.find(this.positionEmployeeItems, { name: 'positions' }),
                employees = _.find(this.positionEmployeeItems, { name: 'employees' })

            this.resetFollowingSelections(arrIndex);

            switch (this.positionEmployeeItems[arrIndex].name) {
                case 'clients':
                    this.apiSvc.getLookupList('User/Clients').then((res) => {
                        if (!res.Clients.Options) return this.handleFailure('No Clients to show.');

                        _.find(this.positionEmployeeItems, { name: 'clients' }).items = res.Clients.Options;

                        this.positionEmployeeItems[arrIndex].changed = false;
                    }, this.handleFailure);
                    break;
                case 'sites':

                    this.apiSvc.getResource('User/Sites/:siteId/:save')
                        .get({ siteId: this.selectedVm['clients'][0].Key, save: false }).$promise.then((res: any) => {
                        if (!res) return this.handleFailure("There are no sites to show.");

                        positions.items = [];
                        employees.items = [];

                        sites.items = _.reject(res.Sites.Options, { 'Key': null });
                        this.positionEmployeeItems[arrIndex].changed = false;
                    }, this.handleFailure);
                    break;
                case 'positions':
                    this.apiSvc.getResource('LookupList/Jobs/BySite/:clientId/:siteId')
                        .query({ clientId: this.selectedVm['clients'][0].Key, siteId: this.selectedVm['sites'][0].Key }).$promise.then((res) => {
                        if (!res) return this.handleFailure("There are no positions to show.");

                        employees.items = [];

                        positions.items = res;
                        this.positionEmployeeItems[arrIndex].changed = false;
                    }, this.handleFailure);

                    break;
                case 'employees':

                    this.apiSvc.getResource('LookupList/Employees/ByPosition/:clientId/:siteId/:positionId')
                        .query({ clientId: this.selectedVm['clients'][0].Key, siteId: this.selectedVm['sites'][0].Key, positionId: this.selectedVm['positions'][0].Key }).$promise.then((res) => {
                        if (!res.length) return this.handleFailure("There are no Employees to show.");

                        employees.items = res;
                        this.positionEmployeeItems[arrIndex].changed = false;
                    }, this.handleFailure);
                    break;
                default:
            }
        }

        private resetFollowingSelections = (indx) => {
            var selArray = ["clients", "sites", "positions", "employees"];

            angular.forEach(selArray,(val, key) => {
                if (key >= indx) {
                    this.selectedVm[selArray[key]] = [];
                }
            });
        }


        //#region Step 1 : ClientSitePostionEmployee Functions
       

        private scrollToSection = (arrIndex) => {
            if (this.sitesettings.windowSizes.isTiny) {
                setTimeout(() => {
                    angular.element(document.body).animate({
                        'scrollTop': angular.element('#' + this.positionEmployeeItems[arrIndex + 1].name).offset().top - 110
                    }, 200)
                }, 100);
            }
        }


        public clearDisplaySelections = () => {
            bootbox.confirm("Are you sure? You will lose any selections.",(res) => {
                if (res) {
                    this.reset();
                    this.activeTab = 0;
                    this.$scope.$apply();
                }
            });
        }


        public selectAllCoursesInColumn = (courses: any) => {
            var filteredCourses = this.handleSelectAllCourses(courses);
            _.each(filteredCourses, function (item: any) {
                item.selected = courses.selectAll;
            });
        }

        private handleSelectAllCourses = (courses: any): any => {
            var filteredCourses: any;
            if (courses.selectedType || courses.showFilter) {
                if (courses.selectedType && courses.showFilter) {
                    filteredCourses = _.filter(courses.items, (item: any) => {
                        return (item.CourseTypeId == courses.selectedType.CourseTypeId) && (item.Value.toLowerCase().includes(courses.filter.toLowerCase()));
                    });
                } else if (courses.selectedType) {
                    filteredCourses = _.filter(courses.items, (item: any) => {
                        return (item.CourseTypeId == courses.selectedType.CourseTypeId);
                    });
                }
                else if (courses.showFilter) {
                    filteredCourses = _.filter(courses.items, (item: any) => {
                        return (item.CourseTypeId == (item.Value.toLowerCase().includes(courses.filter.toLowerCase())));
                    });
                }
            }
            else {
                filteredCourses = courses.items;
            }
            return filteredCourses;
        }

        public selectAllInColumn = (column) => {
            this.selectedVm[column.name] = [];
            angular.forEach(column.items,(item) => {
                if (column.selectAll) {
                    this.selectedVm[column.name].push(item);
                    item.selected = item.Key;
                } else {
                    delete item.selected;
                }
            });


        }

        private addRemoveItem = ($parent, item) => {
            var column = $parent.$parent.column;
            var existItem = _.find(this.selectedVm[column.name], item);
            if (existItem) {
                _.pull(this.selectedVm[column.name], existItem);
            } else {
                this.selectedVm[column.name].push(item);
            }
           
            //Mark as changed so this.getColumn pulls new data
            if ($parent.$parent.$index != this.positionEmployeeItems.length - 1) {
                var indx = $parent.$parent.$index + 1;
                this.resetFollowingSelections(indx);
                this.positionEmployeeItems[indx].changed = true;

            }

        }



       

        //#endregion

        //#region Step 2 : Course Picker
        public coursesTabSelected = () => {

            this.getCourses();

        }

        private getCourses = () => {

            if (this.hasFormChanged == false) {
                return;
            }

            this.courses.items = [];

            var odataParams: any = {
                $select: 'CourseId, Name, CourseType, OrgClient',
                $expand: 'CourseType, OrgClient'
            };

            if (this.selectedVm.clients.length == 1) {
                odataParams.$filter = `OrgClient/Id eq ${this.selectedVm.clients[0].Key} or substringof(OrgClient/Name,'SBM')`;
            } else {
                odataParams.$filter = `substringof(OrgClient/Name,'SBM')`;
            }

            this.apiSvc.getByOdata(odataParams, 'Course')
                .then((res) => {
                if (!res) this.handleFailure("No courses available.");

                angular.forEach(res,(item) => {
                    var isSbmCourse = item.OrgClient.Name.toLowerCase().indexOf('sbm') > -1;
                    this.courses.items.push({
                        Key: item.CourseId,
                        Value: item.Name,
                        CourseTypeId: item.CourseType.CourseTypeId,
                        isSbmCourse: isSbmCourse
                    });
                });

                this.courses.types = _.uniq(_.pluck(res, 'CourseType'),(item) => (item.CourseTypeId));

                this.hasFormChanged = false;
                this.courses.selectAll = false;
            }, this.handleFailure);


        }

        public toggleCourseType = (courseType) => {
            this.courses.selectedType = (courseType != this.courses.selectedType) ? courseType : null;
            var filteredCourses = this.handleSelectAllCourses(this.courses);
            var selectedCourses = _.filter(filteredCourses, (item: any) => {
                return item.selected;
            });
            if (selectedCourses.length == filteredCourses.length) {
                this.courses.selectAll = true;
            } else {
                this.courses.selectAll = false;
            }
        }


        //#region Step 3 : Summary
        public summaryTabSelected = () => {
            this.processCourses();
            this.hasFormChanged = false;
        }

        private processCourses = () => {
            this.selectedVm.courses = _.where(this.courses.items, { selected: true });
        }

        public finishAssign = () => {
            if (!this.selectedVm) return this.handleFailure("SelectedVm is falsy");

            this.payloadVm.ClientIds = this.processDataForPayload("clients");
            this.payloadVm.SiteIds = this.processDataForPayload("sites");
            this.payloadVm.JobIds = this.processDataForPayload("positions");
            this.payloadVm.EmployeeIds = this.processDataForPayload("employees");
            this.payloadVm.CourseIds = this.processDataForPayload("courses");
            
            //check if is an assign to a position or an assign to employees
            if (this.payloadVm.EmployeeIds) { //IS EMPLOYEE ASSIGN
                this.blockUI.start('Assigning Courses to Employees. Please wait...');

                this.apiSvc.save(this.payloadVm, 'TrainingEmployee/UpdateEmployeeCourses').then(() => {
                    this.notificationSvc.successToastMessage("Course assignments have been successfully saved for "+this.payloadVm.EmployeeIds.length+" employees.");
                    this.reset();
                }, this.handleFailure);
            } else { //IS POSITION ASSIGN
                this.blockUI.start('Assigning Courses. Please wait...');

                this.apiSvc.save(this.payloadVm, 'Job/UpdateMultipleJobsCourses').then(() => {
                    this.notificationSvc.successToastMessage("Course assignments have been successfully saved for "+this.payloadVm.JobIds.length+" positions.");
                    this.reset();
                }, this.handleFailure);
            }

            
        }
        //#endregion 
        
        private processDataForPayload = (objName: string): Array<number> => {
            var item = this.selectedVm[objName];
            if (!item.length) {
                this.handleFailure("objName:" + objName + " is falsy");
                return;
            }

            var clientArray = [];


            clientArray = _.pluck(item, "Key");


            return clientArray;
        }

        private toggleSelectedAll = (isItemSelected: boolean) => {
            if (!isItemSelected) {
                this.courses.selectAll = false;
            }
        }

        private anyCoursesSelected = () => {
            return _.any(this.courses.items,(item) => { return item.selected; });
        }

        //#region Batch Processing (Not currently in use)
        public applySelection = () => {
            this.processSelections();
        }

        private processSelections = () => {
            
            //var indexOfFirstSet = null;

            //find first set of non empty items
            //angular.forEach(selected,(value, key) => {
            //    //if (!value[Object.keys(value)[0]].length) delete selectedVm[key];
            //    if (value.item.selected.length && indexOfFirstSet == null)
            //        indexOfFirstSet = key;


            //});
            // indexOfFirstSet = this.lastViewableColumnIndex;


            //first index is 2
            var clients = angular.copy(_.find(this.positionEmployeeItems, { name: 'clients' })),
                sites = angular.copy(_.find(this.positionEmployeeItems, { name: 'sites' })),
                positions = angular.copy(_.find(this.positionEmployeeItems, { name: 'positions' })),
                employees = angular.copy(_.find(this.positionEmployeeItems, { name: 'employees' }));

        

            //angular.forEach(selected, (value, key) => {
            //    if (key > indexOfFirstSet) return;


            //    this.selectedVm.push({
            //        SectionTitle: value.name,
            //        Name: value.item.displaySelected.Value,
            //        selected: value.item.selected
            //    })
            //});
            
            return this.MasterSelection;
        }
        //#endregion

        private initPositionEmployeeItems = () => {
            this.positionEmployeeItems = [
                {
                    name: 'clients',
                    //items: ,
                    changed: true
                }, {
                    name: 'sites',
                    items: [],
                    changed: true
                }, {
                    name: 'positions',
                    items: [],
                    changed: true
                }, {
                    name: 'employees',
                    items: [],
                    changed: true
                }
            ]
        }

        private initSelectVm = () => {
            this.selectedVm = {
                clients: <any>[],
                sites: <any>[],
                positions: <any>[],
                employees: <any>[],
                courses: <any>[]
            };
        }
        private initPayloadVm = () => {
            this.payloadVm = {};
        }

        private reset = () => {
            this.initPositionEmployeeItems();
            this.initSelectVm();
            this.initPayloadVm();
            this.courses = { items: [], types: [], selectedType: null, selectAll: false, showFilter: false, filter: '' };

            if (this.blockUI) this.blockUI.stop();

            this.activeTab = 0;
        }

        private handleFailure = (reason) => {
            if (this.blockUI) this.blockUI.stop();
            this.$log.error(reason);
        }
    }

    angular.module('app').controller('Training.CourseAssign', CourseAssign);
} 