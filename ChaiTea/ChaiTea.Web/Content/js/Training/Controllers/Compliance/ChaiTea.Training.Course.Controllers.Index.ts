﻿module ChaiTea.Training.Compliance.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;

    class ComplianceIndex implements commonInterfaces.INgController {

        basePath: string = '';
        modalInstance;
        employees: Array<serviceInterfaces.ITrainingEmployeeWithCourse> = [];
        totalPercent: number = 0;
        totalCompleted: number = 0;
        totalExpected: number = 0;
        compliantMsg: string = 'COMPLIANCE';
        compliantControl: number = 80;

        static $inject = [
            '$scope',
            '$log',
            '$uibModal',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo',
            'chaitea.common.services.imagesvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
        ) {

            this.basePath = sitesettings.basePath;
        }

        public initialize = (): void => {

            this.getEmployeeList();

        }

        /**
         * @description Returns a list of employees.
         */
        private getEmployeeList = (): void => {

            this.employees = [];
            var endpoint = serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee] + '/WithCourse';
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, endpoint)
                .then((employees: Array<serviceInterfaces.ITrainingEmployeeWithCourse>) => {

                    angular.forEach(employees, (employee: serviceInterfaces.ITrainingEmployeeWithCourse) => {

                        //set profile image
                        employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);

                        this.employees.push(employee);
                    });

                    this.calculateCourseData();


            });
            
        }


        private getSingleEmployeesCourses = (employee: serviceInterfaces.ITrainingEmployeeWithCourse) => {
            return this.apiSvc.get('EmployeeCourse/AllByEmployeeId/' + employee.OrgUserId).then((employeeCourses: Array<serviceInterfaces.IEmployeeCourse>) => {
                employee.EmployeeCourses = employeeCourses;
            });
        }

        /**
         * @description Calculate course compliance
         */
        private calculateCourseData = (): void => {

            var now = moment();

            angular.forEach(this.employees,(employee) => {

                employee.HireDate = moment(employee.HireDate).format('MM/DD/YYYY');
                employee.CoursesPercentCompleted = 0;

                var metrics = this.trainingSvc.calculateCourseComplianceMetrics(employee.EmployeeCourses);

                angular.forEach(employee.EmployeeCourses,(employeeCourse) => {

                    var compliance = this.trainingSvc.calculateCourseCompliance(employeeCourse);
                    employeeCourse.IsSelfInitiated = compliance.IsSelfInitiated;
                    employeeCourse.IsCompliant = compliance.IsCompliant;
                    employeeCourse.TotalCompleted = compliance.TotalCompleted;
                    employeeCourse.CompletedDate = compliance.CompletedDate;

                });

                var assignedCourses: Array<serviceInterfaces.IEmployeeCourse> = _.filter(employee.EmployeeCourses, { IsSelfInitiated: false });

                employee.CoursesExpected = assignedCourses.length;
                employee.CoursesCompleted = _.where(assignedCourses, { IsCompliant: true }).length;

                if (employee.EmployeeCourses.length) {
                    employee.CoursesPercentCompleted = metrics.CompliancePercent;
                }

                this.totalCompleted += employee.CoursesCompleted;
                this.totalExpected += employee.CoursesExpected;

            });
            
            this.totalPercent = Math.round((this.totalCompleted / this.totalExpected) * 100);

        }

        /**
         * @description Redirect to users transcript
         */
        public loadTranscript = (employee: serviceInterfaces.ITrainingEmployeeWithCourse) => {
            window.location.replace(this.basePath + 'Training/Transcript/EntryView/' + employee.OrgUserId);
        }

    }
    angular.module('app').controller('Training.ComplianceIndex', ComplianceIndex);

} 