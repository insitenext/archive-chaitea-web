﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    class KnowledgeCenterEntryView implements commonInterfaces.INgController {
        courseTypeId;

        employeeId: number;
        basePath: string;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];
        courses: Array<serviceInterfaces.ICourse> = [];
        featuredCourse: Array<serviceInterfaces.ICourse> = [];
        counts = {
            siteSpecific: 0,
            courses: 0
        }
        defaultCoverImage: string;
        courseData: any;
        modalInstance;
        courseIcon: string;

        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo',
            '$http',
            '$uibModal',
            'localStorageService',
            'chaitea.common.services.awssvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $http: ng.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc) {

            this.employeeId = userInfo.userId;
            this.basePath = sitesettings.basePath;

            this.defaultCoverImage = this.basePath + 'Content/img/no-course-image-available.png';
        }

        public initialize = (courseTypeId): void => {
            this.courseTypeId = courseTypeId;

            //get course types

            //get all employee courses

            //group courses by type

            //check all courses for due dates and completion
            this.getCourseIcon(courseTypeId);
            this.loadData();

            mixpanel.track('User is browsing Knowledge Center');
        }

        public loadData = (): void => {
            async.series([
                //courses
                (callback) => {
                    
                    //filter by client and site
                    var allSites = this.localStorageService.get('sites');
                    var filterStr: string = `CourseTypeId eq ${this.courseTypeId} and (OrgType eq 'Organization' or OrgClient/Id eq ${this.userContext.Client.ID})`;

                    if (this.userContext.Site && this.userContext.Site.ID && (allSites && allSites.length > 1)) {
                        filterStr = `CourseTypeId eq ${this.courseTypeId} and (OrgType eq 'Organization' or OrgSite/Id eq ${this.userContext.Site.ID}`;
                    }

                    var params = {
                        $filter: filterStr
                    }
                    this.apiSvc.getByOdata(params, "Course").then((data) => {
                        callback(null, data);
                    });
                }
            ], (err, res) => {

                if (err) return this.$log.error(err);

                angular.forEach(res[0], (course: serviceInterfaces.ICourse) => {
                    var courseCover = _.find(course.CourseAttachments, { CourseAttachmentType: 'Cover' });
                    //assigning default image to show when no cover image was uploaded
                    course.coverImage = this.defaultCoverImage;

                    course = this.trainingSvc.getComplianceCourse(course);
                    if (courseCover) {
                        course.coverImage = this.awsSvc.getUrlByAttachment(courseCover.Attachment.UniqueId, courseCover.Attachment.AttachmentType);
                    }
                    if (course.Overview.length > 100) {
                        course.Overview = course.Overview.slice(0, 100) + ' ...';
                    }

                    angular.forEach(course.CourseAttachments, (attachment: serviceInterfaces.ICourseAttachment) => {
                        if (!course['Views']) course['Views'] = 0;

                        course['Views'] += attachment.Views;
                    });

                    this.courses.push(course);
                });
                var featuredCourseIndex = -1;
                featuredCourseIndex = _.findIndex(this.courses, function (n) {
                    return n.IsFeatured == true;
                });
                if (featuredCourseIndex != -1) {
                    this.featuredCourse = this.courses.splice(featuredCourseIndex, 1);
                }
                console.log(this.featuredCourse);
                this.counts.courses = this.courses.length + this.featuredCourse.length;

            });
        }

        /**
        * @description Loads the attachment edit modal.
        */
        public loadCourseModal = (course) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'coursePopup.tmpl.html',
                controller: 'Training.KnowledgeCenterCoursePopup as vm',
                size: 'sm',
                resolve: {
                    course: () => { return course; },
                    courseIcon: () => { return this.courseIcon; }
                }
            });
        }

        public getCourseIcon = (courseTypeId) => {
            this.apiSvc.getById(courseTypeId, 'CourseType').then((res) => {
                this.courseIcon = res.Icon;
            });
        }

        public leaveNote = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.leavenote.tmpl.html',
                controller: 'Training.LeaveNoteCtrl as vm',
                size: 'sm',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return this.employeeId; }
                }
            });
        }
    }

    angular.module('app').controller('Training.KnowledgeCenterEntryView', KnowledgeCenterEntryView);

    class KnowledgeCenterCoursePopup {

        basePath: string;
        employeeId: number;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'userInfo',
            'chaitea.common.services.trainingsvc',
            'chaitea.common.services.awssvc',
            'course',
            'courseIcon'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private userInfo: IUserInfo,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private course,
            private courseIcon
        ) {
            this.employeeId = userInfo.userId;
            //update attachments by perferrred language
            this.course = trainingSvc.getComplianceCourse(this.course);

            angular.forEach(this.course.CourseAttachments, (attachment: any) => {
                attachment.icon = this.awsSvc.getFileIcon(attachment.AttachmentType);
            });
        }
        /**
        * @description Dismiss the modal instance.
        */
        close = (): void=> {
            this.$uibModalInstance.close();
        };

    }

    angular.module('app').controller('Training.KnowledgeCenterCoursePopup', KnowledgeCenterCoursePopup);

    class LeaveNoteCtrl {

        modalInstance;

        siteSettings: ISiteSettings;

        basePath: string;
        sites = [];

        messageBuilder = {
            Jobs: [],
            Text: '',
            Sites: [],
            Department: {},
            IsCustomer: false,
            IsTrainingAdmin: true,
            TrainingMessageId: 0
        };

        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'employeeId',
            '$uibModal',
            'userContext',
            'userInfo'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private employeeId: number,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private userInfo: IUserInfo) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.getSites();
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        public getSites = (): void => {
            if (this.userContext.Site && this.userContext.Site.ID) {
                this.sites.push({
                    Id: this.userContext.Site.ID,
                    Name: this.userContext.Site.Name
                });
                this.messageBuilder.Sites = this.sites;
            }
            else {
                var filterStr: string = `ParentId eq  ${this.userContext.Client.ID}`;
                var params = {
                    $filter: filterStr
                };
                this.apiSvc.getByOdata(params, "Site").then((result) => {
                    angular.forEach(result, (site) => {
                        this.sites.push({
                            Id: site.Id,
                            Name: site.Name
                        });
                    });
                    this.messageBuilder.Sites = this.sites;
                });
            }
        }

        /**
        * @description Send Notice to the training admins.
        */
        public sendNotice = (): void => {


            this.messageBuilder.Department = { DepartmentId: 1, Name: 'General' };
            var userName = this.userInfo.firstName + ' ' + this.userInfo.lastName;
            var userEmail = this.userInfo.userEmail;
            this.messageBuilder.Text = userName + ' (Email:' + userEmail + ') has an issue with Training';

            this.apiSvc.save(this.messageBuilder, 'TrainingMessage').then((res) => {
                this.$uibModalInstance.dismiss('cancel');
                this.modalInstance = this.$uibModal.open({
                    templateUrl: this.siteSettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.notificationsent.tmpl.html',
                    size: 'sm',
                    controller: 'Training.SentNotificationCtrl as vm'
                });
            });
        }

    }
    angular.module("app").controller('Training.LeaveNoteCtrl', LeaveNoteCtrl);

    class SentNotificationCtrl {

        modalInstance;
        static $inject = [
            '$scope',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            '$uibModal'
        ];
        constructor(
            private $scope,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService
        ) {

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };
    }
    angular.module("app").controller('Training.SentNotificationCtrl', SentNotificationCtrl);
}