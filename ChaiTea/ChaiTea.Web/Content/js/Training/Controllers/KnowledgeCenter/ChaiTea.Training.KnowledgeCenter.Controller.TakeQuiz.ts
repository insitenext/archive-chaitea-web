﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface ICurrentQuestion extends serviceInterfaces.ICourseAttachmentQuestion {
        index?: number;
        selectedOption?: serviceInterfaces.ICourseAttachmentQuestionOption;
    }

    class KnowledgeCenterTakeQuiz implements commonInterfaces.INgController {
        courseAttachmentId:number;
        employeeCourseId: number;

        employeeCourse: serviceInterfaces.IEmployeeCourse;

        employeeId: number;
        basePath: string;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];
        
        courseAttachment: serviceInterfaces.ICourseAttachment;
       
        currentQuestion: ICurrentQuestion;

        snapshotEmployeeCoursePayload:serviceInterfaces.ISnapshotEmployeeCourse;

        questionLetters: Array<string> = ['A','B','C','D','E','F','G','H','I','J','K'];
        
        answers: Array<string> = [];

        static $inject = [
            '$scope',
            '$uibModal',
            '$log',
            '$window',
            '$q',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo'
        ];

        constructor(
            private $scope: ng.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $log: ng.ILogService,
            private $window: ng.IWindowService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo) {

            this.employeeId = userInfo.userId;
            this.basePath = sitesettings.basePath;

            
        }

        public initialize = (employeeCourseId, courseAttachmentId): void => {
            this.employeeCourseId = employeeCourseId;
            this.courseAttachmentId = courseAttachmentId;

            this.loadData();
        }

        public loadData = (): void => {
            async.series([
                //courses
                (callback) => {
                   
                    //bring down employeeCourse 
                    this.apiSvc.getById(this.employeeCourseId, "EmployeeCourse").then((res) => {
                        this.employeeCourse = res;

                        this.courseAttachment = _.find(this.employeeCourse.Course.CourseAttachments,(e) => {
                            return e.CourseAttachmentId == this.courseAttachmentId;
                        });

                        if (!this.courseAttachment) throw this.handleError('No course attachment found by id ' + this.courseAttachmentId);

                        this.currentQuestion = this.courseAttachment.CourseAttachmentQuestions[0];

                        this.currentQuestion.index = 0;

                    });

                }
            ],(err, res) => {
                if (err) return this.$log.error(err);
            });
        }

        public selectAnswer = (answer) => {

            this.currentQuestion.selectedOption = answer;

            this.answers.push(answer.Option);


            //Fail
            if (!this.currentQuestion.selectedOption.IsAnswer) {

                this.trainingSvc.saveSnapshot(this.employeeCourse, this.courseAttachment, false, this.answers).then((res) => {
                    mixpanel.track("Course Quiz Failed", { CourseId: this.courseAttachment.CourseId, QuizId: this.courseAttachment.CourseAttachmentId });
                    bootbox.dialog({
                        message: "Let's try again",
                        title: "Quiz: Oops!",
                        closeButton: false,
                        buttons: {
                            success: {
                                label: 'Back to Course',
                                className: 'btn-success',
                                callback: () => {
                                    return this.backToCourse();
                                }
                            },
                            main: {
                                label: 'Try Again',
                                className: 'btn-primary',
                                callback: () => { return this.startOver(); }
                            }
                        }
                    });

                });

                
            }
        }

        private backToCourse = () => {
            this.$window.location.href = this.sitesettings.basePath + "Training/KnowledgeCenter/TakeCourse/" + this.employeeCourseId;
        }

        private startOver = () => {
            //scramble questions and answers
            angular.forEach(this.courseAttachment.CourseAttachmentQuestions,(item) => {
                if(item['selectedOption'])
                    delete item['selectedOption'];
                item.CourseAttachmentQuestionOptions = _.shuffle(item.CourseAttachmentQuestionOptions);
            });
            
            if(this.currentQuestion.selectedOption)
                delete this.currentQuestion.selectedOption;
            
            this.currentQuestion.CourseAttachmentQuestionOptions = _.shuffle(this.currentQuestion.CourseAttachmentQuestionOptions);

            this.$scope.$apply()
            //remove all "selectedOption"s in this.courseAttachments


        }

        public goToNextQuestion = () => {
            var currentIndex = this.currentQuestion.index;

            this.courseAttachment.CourseAttachmentQuestions[currentIndex] = this.currentQuestion;

            this.currentQuestion = this.courseAttachment.CourseAttachmentQuestions[currentIndex + 1];
            this.currentQuestion.index = currentIndex + 1;
        }
        
        public cancelQuiz = () => {
            bootbox.confirm('<strong>Are you sure you want to cancel?</strong><br/>Your answers will not be stored.',(res) => {
                if (!res) return;

                this.backToCourse();
            });
        }

        public finishQuiz = () => {
            console.log('finish quiz', this.courseAttachment);
            
            //save historical data
            this.trainingSvc.saveSnapshot(this.employeeCourse, this.courseAttachment, true, this.answers).then((res) => {

               this.$uibModal.open({
                    templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/Course/course.rating.entry.tmpl.html',
                    controller: 'Training.KnowledgeCenter.CourseRating as vm',
                    size: 'sm',
                    resolve: {
                        employeeCourseId: () => { return this.employeeCourseId },
                        courseId: () => { return this.courseAttachment.CourseId },
                        courseAttachmentId: () => { return this.courseAttachment.CourseAttachmentId },
                        courseType: () => { return this.employeeCourse.Course.CourseType },
                        hasQuiz: () => { return true; }
                    }
                });
            });

        }

        public handleError = (errorMessage) => {
            this.$log.error(errorMessage);
        }
    }

    class CourseRatingModalCtrl {
        ratingScore: number;
        static $inject = [
            '$scope',
            '$uibModalInstance',
            '$window',
            'sitesettings',
            'userInfo',
            'chaitea.common.services.apibasesvc',
            'employeeCourseId',
            'courseId',
            'courseAttachmentId',
            'courseType',
            'hasQuiz'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $window: angular.IWindowService,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private employeeCourseId: number,
            private courseId: number,
            private courseAttachmentId: number,
            private courseType: serviceInterfaces.ICourseType,
            private hasQuiz: boolean) { }

        /**
         * @description Dismiss the modal instance.
         */
        goBackToCourse = (): void=> {
            var param = {
                CourseId: this.courseId,
                CourseAttachmentId: this.courseAttachmentId,
                OrgUserId: this.userInfo.userId,
                Rating: this.ratingScore,
            }
            this.apiSvc.save(param, 'CourseRating').then((res) => {
                mixpanel.track("Course Quiz Passed", { CourseId: this.courseId, QuizId: this.courseAttachmentId });

                this.$uibModalInstance.dismiss();
                this.backToCourse();
            }); 
        };
        private backToCourse = () => {
            console.log(this.employeeCourseId);
            this.$window.location.href = this.sitesettings.basePath + "Training/KnowledgeCenter/TakeCourse/" + this.courseId + "?employeeId=" + this.userInfo.userId + "&pageTitle=" + this.courseType.Name + "&courseTypeId=" + this.courseType.CourseTypeId;
        }

    }
    angular.module('app').controller('Training.KnowledgeCenter.CourseRating', CourseRatingModalCtrl);
    angular.module('app').controller('Training.KnowledgeCenter.TakeQuiz', KnowledgeCenterTakeQuiz);
} 