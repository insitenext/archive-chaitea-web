﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    class KnowledgeCenterIndex implements commonInterfaces.INgController {

        employeeId: number;
        basePath: string;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];
        //courses: Array<serviceInterfaces.ICourse> = [];

        counts={
            siteSpecific: 0,
            courses:0
        }

        courseData: any;
        courses: Array<serviceInterfaces.ICourse> = [];
        courseDataFiltered: any;
        courseViewCounts: any = {};
        modalInstance;
        orgSetting: Common.Interfaces.IOrgSetting;
        backgroundImgUrl: string = '';
        profileImgUrl: string = '';
        showTranscript: boolean = false;
        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo',
            '$uibModal',
            '$http',
            'chaitea.common.services.awssvc',
            'localStorageService',
            'awsconfig'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: ng.IHttpService,
            private awsSvc: Common.Services.IAwsSvc,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private awsConfig: commonInterfaces.IEndpoint) {

            this.employeeId = userInfo.userId;
            this.basePath = sitesettings.basePath;
        }

        public initialize = (employeeId): void => {

            //get course types

            //get all employee courses

            //group courses by type

            //check all courses for due dates and completion
            this.getOrgSetting(this.userContext.Client.ID);
            this.profileImgUrl = this.awsSvc.getUrl('public/image/sbm-logo-square.png');
            this.backgroundImgUrl = this.awsSvc.getUrl('public/image/knowledge.png');
            this.loadData();
        }

        private getOrgSetting = (clientId: number) => {
            var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Org];
            var params = {
                clientId: clientId
            }
            this.apiSvc.query(params, "Org/Clients/:clientId", false, true).then((resultantOrgSetting: any) => {
                if (resultantOrgSetting.length != 0) {
                    this.orgSetting = resultantOrgSetting[0];
                    if (this.orgSetting.ProfileImgAttachmentId) {
                        this.apiSvc.getById(this.orgSetting.ProfileImgAttachmentId, 'Attachment').then((result) => {
                            this.profileImgUrl = this.awsSvc.getUrlByAttachment(result.UniqueId, commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image]);
                        }, this.handleFailure);
                    }
                    if (this.orgSetting.BackgroundImgAttachmentId) {
                        this.apiSvc.getById(this.orgSetting.BackgroundImgAttachmentId, 'Attachment').then((result) => {
                            this.backgroundImgUrl = this.awsSvc.getUrlByAttachment(result.UniqueId, commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image]);
                        }, this.handleFailure);
                    }
                }
            }, this.handleFailure);
        }

        public loadData = (): void => {
            async.series([
                //course meta data (icons to use, colors etc)
                (callback) => {
                    this.courseData = this.courseDataJsonObject();
                    this.courseDataFiltered = [];
                    callback(null, this.courseData);
                },
                //courses
                (callback) => {
                    
                    //filter by client and site
                    var allSites = this.localStorageService.get('sites');
                    var filterStr: string = `OrgType eq 'Organization' or OrgClient/Id eq ${this.userContext.Client.ID}`;

                    if (this.userContext.Site && this.userContext.Site.ID && (allSites && allSites.length > 1)) {
                        filterStr = `OrgType eq 'Organization' or OrgSite/Id eq ${this.userContext.Site.ID}`;
                    }

                    var params = {
                        $filter: filterStr
                    };
                    this.apiSvc.getByOdata(params, "Course").then((res) => {

                        //check client and filter
                        res = this.trainingSvc.filterCourses(this.userContext.Client, res);
                       
                        angular.forEach(res, (course: serviceInterfaces.ICourse) => {

                            course = this.trainingSvc.getComplianceCourse(course);

                            angular.forEach(course.CourseAttachments, (attachment) => {
                                
                                if (!this.courseViewCounts[course.CourseType.Name]) this.courseViewCounts[course.CourseType.Name] = 0;

                                this.courseViewCounts[course.CourseType.Name] += attachment.Views;

                            });

                            this.courses.push(course);

                        });

                        callback(null, this.courses);

                    });
                },
                //course types
                (callback) => {
                    
                    this.apiSvc.get("Lookuplist/CourseTypes").then((data) => {
                        this.courseTypes = [];
                        var totalCourses = 0;
                        angular.forEach(data, (courseType: serviceInterfaces.ICourseType) => {
                            //filter out types not used
                            var course = _.find(this.courses, { CourseType: { Name: courseType.Name } });
                            if (course) {
                                courseType.Color = courseType.Icon.toLowerCase();
                                this.courseTypes.push(courseType);
                            }
                        });

                        angular.forEach(this.courseTypes, (courseType) => {
                            courseType['count'] = <any>_.size(_.where(this.courses, { 'CourseTypeId': courseType.CourseTypeId }));
                            courseType['viewCount'] = (this.courseViewCounts[courseType.Name] ? this.courseViewCounts[courseType.Name] : 0);
                            totalCourses = totalCourses + courseType['count'];
                        });

                        //
                        //TODO [JG]: filter by client and site
                        //

                        this.counts.siteSpecific = _.filter(this.courses, (item: serviceInterfaces.ICourse) => (item.OrgClient.Name.toLowerCase() != 'sbm')).length;
                        this.counts.courses = totalCourses;
                        callback(null, data);

                    });
                }
                
            ],(err, res) => {

                if (err) return this.$log.error(err);

            });
        }

        private handleFailure = (error) => {
            this.$log.error(error);
        }

        private courseDataJsonObject = () => {
            return [
                {

                    "subject": "Custodial",
                    "icon": "custodial2",

                    "color": "pinkish"

                },
                {

                    "subject": "Floor",
                    "icon": "custodial",

                    "color": "warning"

                },
                {

                    "subject": "EHS",
                    "icon": "safety",

                    "color": "blue"

                },
                {

                    "subject": "Human Resources",
                    "icon": "legal",

                    "color": "pink"
                },
                {

                    "subject": "Service Excellence",
                    "icon": "KPIs",

                    "color": "green"
                },
                {

                    "subject": "Glasswash",
                    "icon": "glassware",

                    "color": "sea"
                },
                {

                    "subject": "MAC",
                    "icon": "MAC",

                    "color": "pink"
                },
                {

                    "subject": "Operations",
                    "icon": "operations",

                    "color": "to"
                },
                {

                    "subject": "Pest Control",
                    "icon": "pest",

                    "color": "pest"
                },
                {

                    "subject": "Lamp",
                    "icon": "lamp",

                    "color": "green"

                },
                {

                    "subject": "Landscaping",
                    "icon": "landscaping",

                    "color": "pink"
                },

                {

                    "subject": "Plant Services",
                    "icon": "plant-services",

                    "color": "plum"
                },
                {

                    "subject": "Cleanroom",
                    "icon": "cleanroom",

                    "color": "primary"
                },
                {

                    "subject": "MAC",
                    "icon": "MAC",

                    "color": "pink"
                },
                {

                    "subject": "GMP",
                    "icon": "GMP",

                    "color": "blue"
                },
                {
                    "subject": "EVS",
                    "icon": "EVS",
                    "color": "to"
                },
                {
                    "subject": "Recycling",
                    "icon": "recycling",
                    "color": "green"
                },
                {
                    "subject": "Lab Services",
                    "icon": "lab-services",
                    "color": "plum"
                },
                {
                    "subject": "SBM",
                    "icon": "sbm",
                    "color": "orange"
                },
                {
                    "subject": "Floor Care",
                    "icon": "floor-care",
                    "color": "to"
                },
                {
                    "subject": "Professionalism Development",
                    "icon": "leadership-dev",
                    "color": "green"
                }
            ]
        }

        public leaveNote = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.leavenote.tmpl.html',
                controller: 'Training.LeaveNoteCtrl as vm',
                size: 'sm',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return this.employeeId; }
                }
            });
        }

        public assignUnassign = (isAssign: boolean): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.assignUnassign.tmpl.html',
                controller: 'Training.AssignUnassignCtrl as vm',
                size: 'sm',
                resolve: {
                    title: () => { return ''; },
                    isAssign: () => { return isAssign; }
                }
            });
        }
    }

    angular.module('app').controller('Training.KnowledgeCenterIndex', KnowledgeCenterIndex);

    class LeaveNoteCtrl {
       
        modalInstance;

        siteSettings: ISiteSettings;
       
        basePath: string;
        sites = [];

        messageBuilder = {
            Jobs: [],
            Text: '',
            Sites: [],
            Department: {},
            IsCustomer: false,
            IsTrainingAdmin: true,
            TrainingMessageId: 0
        };
        
        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'employeeId',
            '$uibModal',
            'userContext',
            'userInfo'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private employeeId: number,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private userInfo: IUserInfo) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.getSites();
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        public getSites = (): void => {
            if (this.userContext.Site && this.userContext.Site.ID) {
                this.sites.push({
                    Id: this.userContext.Site.ID,
                    Name: this.userContext.Site.Name
                });
                this.messageBuilder.Sites = this.sites;
            }
            else {
                var filterStr: string = `ParentId eq  ${this.userContext.Client.ID}`;
                var params = {
                    $filter: filterStr
                };
                this.apiSvc.getByOdata(params, "Site").then((result) => {
                    angular.forEach(result, (site) => {
                        this.sites.push({
                            Id: site.Id,
                            Name: site.Name
                        });
                    });
                    this.messageBuilder.Sites = this.sites;
                });
            }
        }

        /**
        * @description Send Notice to the training admins.
        */
        public sendNotice = (): void => {
            
            
            this.messageBuilder.Department = { DepartmentId: 1, Name: 'General' };
            var userName = this.userInfo.firstName + ' ' + this.userInfo.lastName;
            var userEmail = this.userInfo.userEmail;
            this.messageBuilder.Text = userName +  ' (Email:' + userEmail + ') has an issue with Training';

            this.apiSvc.save(this.messageBuilder, 'TrainingMessage').then((res) => {
            this.$uibModalInstance.dismiss('cancel');
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.siteSettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.notificationsent.tmpl.html',
                    size: 'sm',
                    controller: 'Training.SentNotificationCtrl as vm'
                 });
            });
        }

    }
    angular.module("app").controller('Training.LeaveNoteCtrl', LeaveNoteCtrl);

    class SentNotificationCtrl {

        modalInstance;
        static $inject = [
            '$scope',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            '$uibModal'
        ];
        constructor(
            private $scope,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService
            ) {

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };
    }
    angular.module("app").controller('Training.SentNotificationCtrl', SentNotificationCtrl);

    class AssignUnassignCtrl {

        modalInstance;

        siteSettings: ISiteSettings;

        basePath: string;
        sites = [];

        messageBuilder = {
            Jobs: [],
            Text: '',
            Sites: [],
            Department: {},
            IsCustomer: false,
            IsTrainingAdmin: true,
            TrainingMessageId: 0
        };
        btnAction: string = "";
        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'isAssign',
            '$uibModal',
            'userContext',
            'userInfo'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private isAssign: boolean,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private userInfo: IUserInfo) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.btnAction = this.isAssign ? 'ASSIGN' : 'UNASSIGN';
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

    }
    angular.module("app").controller('Training.AssignUnassignCtrl', AssignUnassignCtrl);
} 