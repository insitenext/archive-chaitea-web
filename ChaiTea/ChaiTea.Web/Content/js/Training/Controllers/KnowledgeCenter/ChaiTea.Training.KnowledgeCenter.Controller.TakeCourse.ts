﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum AttachmentTypes { Video, Pdf, File };

    class KnowledgeCenterTakeCourse implements commonInterfaces.INgController {
        employeeCourseId: number;
        courseAttachmentId: number;
        employeeId: number;
        basePath: string;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];

        employeeCourse: serviceInterfaces.IEmployeeCourse;
        fullCourse: serviceInterfaces.ICourse;
        compliance: any;
        modalInstance;
        selectedTraining;
        showSubAttachments: boolean = false;

        pdfTempUrl: string = '/ChaiTea.Web/Content/js/Training/Templates/KnowledgeCenter/pdf.tmpl.html';

        attachmentTypes = AttachmentTypes;
        hasQuiz: boolean = true;

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModal',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo',
            Common.Services.NotificationSvc.id
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: ng.IWindowService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private notificationSvc: Common.Services.INotificationSvc) {

            this.employeeId = userInfo.userId;
            this.basePath = sitesettings.basePath

        }

        public initialize = (courseId: number, employeeId: number): void => {
            this.employeeCourseId = courseId;

            //an employeeId present means that this is from 
            //KnowledgeCenter EntryView and needs to create EmployeeCourse assignment.
            if (employeeId) {
                
                var employeeCourseVm = <serviceInterfaces.ICourseEmployeeAssign>{
                    CourseIds: [courseId],
                    EmployeeIds: [employeeId]
                };

                this.assignCourseToEmployee(employeeCourseVm).then((res) => {
                    if (!res) throw Error;

                    var params = {
                        $filter: `CourseId eq ${courseId}`
                    }

                    this.apiSvc.getByOdata(params, 'EmployeeCourse/AllByEmployeeId/' + employeeId).then((res: any) => {

                        //find currently selected course
                        var employeeCourse: any = _.find(res, { Course: { CourseId: courseId } });

                        this.processCourse(employeeCourse);
                        
                        mixpanel.track("Course has been taken", {CourseId:courseId});
                    }, this.handleError);

                }, this.handleError);

            } else {
                this.getEmployeeCourse();
            }
        }

        public getEmployeeCourse = (): void => {

            this.apiSvc.getById(this.employeeCourseId, "EmployeeCourse/WithSnapshots").then((data) => {

                this.processCourse(data);

            });

        }

        public processCourse = (employeeCourse: serviceInterfaces.IEmployeeCourse) => {
            
            employeeCourse.Course.CreateDate = moment(employeeCourse.Course.CreateDate).format('MM/DD/YYYY');

            this.employeeCourse = employeeCourse;
            this.employeeCourseId = employeeCourse.EmployeeCourseId;

            var employeeCourseNoFilter = _.clone(employeeCourse, true);

            //save unaltered for saving
            this.fullCourse = _.cloneDeep(this.employeeCourse.Course);

            this.employeeCourse.Course = this.trainingSvc.getComplianceCourse(this.employeeCourse.Course);

            angular.forEach(this.employeeCourse.Course.CourseAttachments, (item: any, key) => {
                this.courseAttachmentId = item.CourseAttachmentId;
                this.apiSvc.getByOdata({ CourseId: item.CourseId, CourseAttachmentId: item.CourseAttachmentId }, 'CourseRating/AvgByCourseAttachment/:CourseId/:CourseAttachmentId', false, false).then((res) => {
                    item.AverageRating = res.Average;
                });

                //Retrieve secure file location for 
                if (item.AttachmentType == AttachmentTypes[AttachmentTypes.Video]) {
                    this.awsSvc.getSignedVideoUrl(item.Attachment.UniqueId, 720).then((data) => {
                        item.cdn = data.Url;
                        item.thumbUrl = data.Thumbnails[0];
                    });
                } else {
                    var folder = this.awsSvc.getFolderByAttachmentType(item.Attachment.AttachmentType);
                    this.awsSvc.getSecureUrlByName(item.Attachment.UniqueId, folder).then((data) => {
                        item.cdn = data.Url;
                    });
                }

                item.icon = this.awsSvc.getFileIcon(item.Attachment.AttachmentType);
                item.Description = item.Description || "Training " + (key + 1);
                item.isQuizTakeable = false;
                this.hasQuiz = item.CourseAttachmentQuestions.length ? true : false;

                //Retrieve secure file location for each of the subattachment files
                angular.forEach(item.CourseSubAttachments,(sub) => {
                    var folder = this.awsSvc.getFolderByAttachmentType(sub.Attachment.AttachmentType);
                    this.awsSvc.getSecureUrlByName(sub.Attachment.UniqueId, folder).then((data) => {
                        sub.cdn = data.Url;
                    });
                    sub.icon = this.awsSvc.getFileIcon(item.Attachment.AttachmentType);
                });

                //check is the training has been passed
                item.isPassed = false;
                if (employeeCourse.SnapshotEmployeeCourses.length) {

                    var snapshots = _.where(employeeCourse.SnapshotEmployeeCourses, { CourseAttachmentId: item.CourseAttachmentId });

                    if (snapshots.length && snapshots[0].Passed) {
                        item.isPassed = true;
                    }

                }

            });

            if (this.employeeCourse.Course.CourseAttachments.length) {
                this.selectedTraining = this.employeeCourse.Course.CourseAttachments[0];
            }

            this.compliance = this.trainingSvc.calculateCourseCompliance(employeeCourseNoFilter);

        }

        /**
         * @description Assure that user has made it to the end of the video before taking the quiz.
         */
        public onVideoEnded = ($event) => {
            if ($event) {
                if (this.userInfo.userId == this.employeeCourse.OrgUserId) {
                    this.selectedTraining.isQuizTakeable = true;
                }
            }
        }

        /**
         * @description Incrament view count on video view.
         */
        public onTimeUpdate = ($event) => {

            var video: any = angular.element('#main-video');
            
            if (!video) return;

            if ((video[0].currentTime > 15 || video[0].currentTime == video[0].duration)) {
                this.trackView();
            }

        }

        /**
         * @description Save view incrament.
         */
        public trackView = () => {

            if (this.userInfo.userId == this.employeeCourse.OrgUserId) {
                this.selectedTraining.isQuizTakeable = true;
            }

            //find attachment
            var attachmentIndex: number = _.findIndex(this.employeeCourse.Course.CourseAttachments, { Attachment: { UniqueId: this.selectedTraining.Attachment.UniqueId } });
            var savedAttachmentIndex: number = _.findIndex(this.fullCourse.CourseAttachments, { Attachment: { UniqueId: this.selectedTraining.Attachment.UniqueId } });

            if (!this.employeeCourse.Course.CourseAttachments[attachmentIndex]['tracked']) this.employeeCourse.Course.CourseAttachments[attachmentIndex]['tracked'] = false;

            if (!this.employeeCourse.Course.CourseAttachments[attachmentIndex]['tracked']) {

                //update view counts
                this.employeeCourse.Course.CourseAttachments[attachmentIndex].Views++; 
                this.fullCourse.CourseAttachments[savedAttachmentIndex].Views++; 
                //save
                this.apiSvc.update(this.employeeCourse.Course.CourseId, this.fullCourse, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Course]);
                //only fire once
                this.employeeCourse.Course.CourseAttachments[attachmentIndex]['tracked'] = true;

            }
        }

        public takeQuiz = () => {

            if (this.hasQuiz) {
                this.modalInstance = this.$uibModal.open({
                    templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/training.knowledgecenter.takequiz.tmpl.html',
                    controller: 'Training.TakeQuizCtrl as vm',
                    size: 'sm',
                    resolve: {
                        employeeCourseId: () => { return this.employeeCourseId; },
                        courseAttachmentId: () => { return this.selectedTraining.CourseAttachmentId; }
                    }
                });
            }
            else {
                var courseAttachment = _.find(this.employeeCourse.Course.CourseAttachments, (e) => {
                    return e.CourseAttachmentId == this.courseAttachmentId;
                });

                if (!courseAttachment) throw this.handleError('No course attachment found by id ' + this.courseAttachmentId);


                //save historical data
                this.trainingSvc.saveSnapshot(this.employeeCourse, courseAttachment, true, []).then((res) => {

                    this.$uibModal.open({
                        templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/Course/course.rating.entry.tmpl.html',
                        controller: 'Training.KnowledgeCenter.CourseRating as vm',
                        size: 'sm',
                        resolve: {
                            employeeCourseId: () => { return this.employeeCourseId },
                            courseId: () => { return courseAttachment.CourseId },
                            courseAttachmentId: () => { return courseAttachment.CourseAttachmentId },
                            courseType: () => { return this.employeeCourse.Course.CourseType },
                            hasQuiz: () => { return false; }
                        }
                    });
                }, this.onFailure);
            }
        
        }

        onFailure = (response: any) => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
         * @description Parse data from Course to generate a user friendly name of the file.
         * @todo Need to change file name from AWS or Server side.
         */
        public generateDownloadName = (item: Object, stringToAppend: string = "") => {
            if (_.isEmpty(item)) this.handleError('No item to parse.');
            var result = "";
            //get course name

            //apend Attachment name or description
            if (item['Name']) {
                result = item['Name'];
            } else if (item['Description']) {

            }

            //append stringToAppend
            if (!_.isEmpty(stringToAppend)) {
                result = result + "-" + stringToAppend;
            }

            //attach file extension
            

            result = result.replace(' ', '-').toLowerCase();

            return result;
        }

        private assignCourseToEmployee = (courseEmployeeAssignVm: serviceInterfaces.ICourseEmployeeAssign) => {
            return this.apiSvc.save(courseEmployeeAssignVm, 'TrainingEmployee/UpdateEmployeeCourses');
        }


        private handleError = (error) => {
            this.$log.error(error);
            return false;
        }



    }

    angular.module('app').controller('Training.KnowledgeCenterTakeCourse', KnowledgeCenterTakeCourse);

    class TakeQuizCtrl {
        selectedTraining;
        modalInstance;
        siteSettings: ISiteSettings;
    
        basePath: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'employeeCourseId',
            'courseAttachmentId',
            '$uibModal',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private employeeCourseId: number,
            private courseAttachmentId: number,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        public openQuiz = () => {
            this.$window.location.href = this.siteSettings.basePath
                    + "Training/KnowledgeCenter/TakeQuiz/" + this.employeeCourseId
                + "?courseAttachmentId=" + this.courseAttachmentId;
        };
    }
    angular.module("app").controller('Training.TakeQuizCtrl', TakeQuizCtrl);
} 