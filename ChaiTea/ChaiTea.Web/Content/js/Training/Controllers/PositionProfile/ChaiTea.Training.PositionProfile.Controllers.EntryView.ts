﻿module ChaiTea.Training.Controllers {
    'use strict';

    import trainingSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonEnums = ChaiTea.Common.Enums;

    interface IContractVm extends trainingInterfaces.IContract {
        EmployeeSignatureURL?: string;
        SupervisorSignatureURL?: string;
    }

    enum FileType { Undefined, ProfilePicture };
    enum Filters { Assigned, Missing };
    
    class PositionProfileIndex {
        isPageDisabled:boolean = true;
        basePath:string="";

        isManagerRole: boolean = false;
        employees: Array<trainingInterfaces.ITrainingEmployee> = [];
        employeesFiltered: Array<trainingInterfaces.ITrainingEmployee> = [];
        counts={
            employees: 0,
            assigned: 0,
            missing: 0
        }

        hideLocation: boolean = false;
        defaultProfileImage: string = '';
        maxStringLength: number = 200;

        /* name from AWS */
        handbookFilenameEN: string = "";
        handbookFilenameES: string = "";

        location: any = {
            clientName: '',
            siteName: '',
            buildings: [],
            floors: [],
            routes: []
        };
        jd: trainingInterfaces.IJobDescriptionExtended = <any>{
            Contract: {}
        };
        supervisor: trainingInterfaces.ISupervisor;
        FunFacts: string;
        EmployeeSignatureURL: string;

        currentContract: IContractVm;
        isContractCurrent: boolean = false;
        
        appliedFilter:number = 0;

        modalInstance;

        pageLoaded: boolean = false;

        /*
        * @description Used to display loading indicator.
        */
        loadedItems = {
            jobDescription: false,
            locationAndRoutes: false,
            courses: false
        }
        

        kpiIndicators = [];

        employeeCourses: Array<serviceInterfaces.IEmployeeCourse> = [];

        isForTeam: boolean = false;
        hideNavigation: boolean = false;
        employeeId: number;
        unionContract = {
            file: "",
            fileName: "",
            UniqueId: ""
        }

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.trainingemployee',
            'chaitea.common.services.awssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.printer',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.trainingsvc',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            'blockUI'
        ];

        constructor(
            private $scope: ng.IScope,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: ng.ILogService,
            private trainingEmployeeSvc: trainingSvc.ITrainingEmployeeSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private baseSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private printerSvc: ChaiTea.Core.Services.IPrinter,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private $timeout: ng.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: Common.Services.IApiBaseSvc,
            private blockUI
            ) {
            //this.isPageDisabled = (userContext.Site.ID === 0);
            this.basePath = sitesettings.basePath;

            this.modalInstance = this.$uibModal;
        }

        initialize = (positionProfileId: number, isForTeam: boolean): void => {
            this.employeeId = positionProfileId;
            this.isForTeam = isForTeam;
            if (positionProfileId != this.userInfo.userId) {
                this.hideNavigation = true;
            }
            this.getPositionProfileById(positionProfileId);
            this.getFunFacts();
            this.isManagerRole = this.canManageEmployeePositionProfile();
            
            // handbook from AWS
            this.handbookFilenameEN = this.awsSvc.getUrl("file/sbm_ee_handbook_7_12_en.pdf");
            this.handbookFilenameES = this.awsSvc.getUrl("file/sbm_ee_handbook_7_12_es.pdf");

            this.messageBusSvc.onMessage('components.headerpanel:languagechange',this.$scope, (language) => {
                this.getPositionProfileById(positionProfileId);
            });

        }

        private getFunFacts = (): void => {
            this.apiSvc.getById(this.employeeId, `Employee/User`).then((result) => {
                if (result) {

                    this.apiSvc.getById(result.UserId, 'UserInfo').then((user) => {
                        this.FunFacts = user.FunFacts;
                    }, this.onFailure);
                }
            }, this.onFailure);
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Show Accept & Sign modal
         */
        public acceptAndSign = ($event) => {
            $event.preventDefault();
            var that = this;
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/positionprofile.acceptandsign.tmpl.html',
                controller: 'Training.PositionProfile.AcceptAndSignModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    jobDescription: () => { return this.jd; },
                    currentContract: () => { return this.currentContract; }
                }
            });
            this.modalInstance.result.then((result) => {
                if (this.blockUI) this.blockUI.stop();
                if (result) {
                    this.processContract(result)
                    _.defer(() => {
                        that.$scope.$apply()
                    });
                }
            })
        }

        /**
        * @description Returns whether the user is a manager. 
        */
        private canManageEmployeePositionProfile = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Returns the job secription object by employee id.
         */
        private getPositionProfileById = (employeeId: number): void => {

            async.auto({
                get_position_profile: (callback) => {
                    console.log('lookup training employee');
                    this.trainingEmployeeSvc.getPositionProfileById(employeeId).then((positionProfile: trainingInterfaces.IJobDescriptionExtended) => {
                        if (!positionProfile.EmployeeId) {
                            //window.location.replace(this.basePath + 'Training/PositionProfile');
                            var params = '';
                            params = `?myteam=true`;
                            window.location.href = this.basePath + 'Training/PositionProfile/Index/' + params;
                        }

                        //set profile image
                        positionProfile.ProfileImage = this.imageSvc.getUserImageFromAttachments(positionProfile.UserAttachments, true);

                        //compile job description
                        positionProfile.FullDescription = this.buildPositionProfile(positionProfile.JobDescription);

                        //TEMP Hide JobDescription.Purpose
                        positionProfile.JobDescription.Purpose = "";

                        this.jd = positionProfile;

                        this.loadedItems.jobDescription = true;
                        callback(null, positionProfile);
                    });
                },
                get_other: ['get_position_profile', (callback) => {

                        //Set Client JobPurpose if available
                    this.getClientJobPurpose(this.jd.JobDescription.JobId).then((purposeText: string) => {
                        this.jd.JobDescription.Purpose = purposeText || "";
                        });
                        this.baseSvc.getByOdata({}, "Kpi").then((kpis) => {
                            this.kpiIndicators = kpis;
                        });

                        //get supervisor job data
                        this.supervisor = this.jd.Supervisor;
                        if (this.supervisor != null) {
                            this.supervisor.ProfileImage = this.defaultProfileImage;
                            if (this.supervisor.ProfilePictureUniqueId != null)
                                this.supervisor.ProfileImage = this.imageSvc.get(this.supervisor.ProfilePictureUniqueId);
                            if (this.supervisor.Manager != null) {
                                this.supervisor.Manager.ProfileImage = this.defaultProfileImage;
                                if (this.supervisor.Manager.ProfilePictureUniqueId != null)
                                    this.supervisor.Manager.ProfileImage = this.imageSvc.get(this.supervisor.Manager.ProfilePictureUniqueId);
                            }
                        }

                        //process contract
                    this.processContract(this.jd.JobContract);

                    callback(null);
                }],
                get_location_and_route: (callback) => {
                    console.log('lookup by location');
                    this.trainingEmployeeSvc.getPositionProfileLocationAndRoute(employeeId).then((locations) => {

                        if (locations.length) {
                            this.location.clientName = locations[0].ClientName;
                            this.location.siteName = locations[0].SiteName;

                            //compile location data
                            angular.forEach(locations,(l) => {
                                //TODO GET PROGRAM NAMES
                                if (_.indexOf(this.location.buildings, l.BuildingName) < 0) {
                                    this.location.buildings.push(l.BuildingName);
                                }
                                if (_.indexOf(this.location.floors, l.FloorName) < 0) {
                                    this.location.floors.push(l.FloorName);
                                }
                                if (_.indexOf(this.location.routes, l.AreaName) < 0) {
                                    this.location.routes.push(l.AreaName);
                                }
                            });
                        }

                        this.loadedItems.locationAndRoutes = true;
                        callback(null, locations);
                    });
                },
                get_course_list: (callback) => {
                    //trainings
                    this.trainingSvc.getEmployeeCourseList(employeeId).then((employeeCourses: Array<serviceInterfaces.IEmployeeCourse>) => {
                        
                        //set list detail
                        angular.forEach(employeeCourses, (employeeCourse: any) => {
                            employeeCourse.Name = employeeCourse.Course.Name;
                            employeeCourse.DetailStyle = (employeeCourse.IsCompleted ? 'fa-check2 text-success' : 'fa-minus text-danger' );
                        });

                        this.employeeCourses = employeeCourses;

                        this.loadedItems.courses = true;
                        callback(null, employeeCourses);
                    });

                },
                
                get_uinionContract: (callback) => {
                    var params = {
                        employeeId: employeeId,
                        attributeId: commonEnums.ATTRIBUTE_CODE.UNION_CODE
                    };
                    this.apiSvc.query(params, "TrainingEmployee/EmployeeAttachment/:employeeId/:attributeId", false,false).then((result) => {
                        if (result.UniqueId) {
                            this.unionContract.UniqueId = result.UniqueId;
                            this.unionContract.fileName = result.Name || "";
                            this.unionContract.file = this.awsSvc.getUrlByAttachment(result.UniqueId, result.AttachmentType.toLowerCase());
                        }
                    });
                }
            },(err, res):any => {

                if (err) return this.$log.error(err); 

                this.pageLoaded = true;

            });

        }

        private getClientJobPurpose=(jobId:number):angular.IPromise<any> => {
            var jobPurposeParams = {
                ClientId: this.userContext.Client.ID,
                JobId: jobId
            };
            return this.baseSvc.query(jobPurposeParams, 'JobPurpose/:ClientId/:JobId', false, false).then((res: trainingInterfaces.IJobPurpose) => {
                if (!res) {
                    return this.$log.error('No JobPurpose found.');
                }
                return this.jd.JobDescription.Purpose = res.Description;
            });
        }

        /**
         * @description Get contract images
         */
        private processContract = (contract: IContractVm): void => {

            if (!this.checkIsContractCurrent(contract)) return;
        
            this.currentContract = contract;

            var imageUrlEmployee = this.getEmployeeImageFromAttachments(this.currentContract.ContractAttachments);
            if (imageUrlEmployee )
                this.currentContract.EmployeeSignatureURL = this.awsSvc.getUrl("image/" + imageUrlEmployee);
            else
                this.currentContract.EmployeeSignatureURL = "";

            var imageUrlSupervisor = this.getSupervisorSignImageFromAttachments(this.currentContract.ContractAttachments);
            if (imageUrlSupervisor)
                this.currentContract.SupervisorSignatureURL = this.awsSvc.getUrl("image/" + imageUrlSupervisor);
            else
                this.currentContract.SupervisorSignatureURL = "";
        }

        /**
         * @description Get image signature from attachment array
         */
        private getEmployeeImageFromAttachments = (contractAttachments: any) => {
            return _.pluck(
                _.where(contractAttachments, { 'ContractAttachmentType': trainingInterfaces.IContractAttachmentType[trainingInterfaces.IContractAttachmentType.EmployeeSignature] }),
                'UniqueId')[0];
        }

        /**
        * @description Get image of supervisor signature from attachment array
        */
        private getSupervisorSignImageFromAttachments = (contractAttachments: any) => {
            return _.pluck(
                _.where(contractAttachments, { 'ContractAttachmentType': trainingInterfaces.IContractAttachmentType[trainingInterfaces.IContractAttachmentType.SupervisorSignature] }),
                'UniqueId')[0];
        }

        /**
         * @description Check (and sets this.isContractCurrent) to see if Job Description has been updated AFTER the contract has been signed
         * @param {Training.Services.IContract} 
         */
        private checkIsContractCurrent = (contract: trainingInterfaces.IContract): boolean => {
            this.isContractCurrent = true; //set to True for same NOTE below. Change to FALSE when below is uncommented.;

            if (!contract) return;

            /* NOTE: [11/23/15] Constraint temporarily removed due to Biz Rules not being defined correctly
            *   if (moment(contract.UpdateDate).isAfter(this.jd.JobDescription.UpdateDate) || moment(contract.UpdateDate).isAfter(this.jd.UpdateDate)) {
            *       this.isContractCurrent = (contract.ContractAttachments.length ? true : false);
            *   }
            */
            return this.isContractCurrent;
        }

        /**
         * @description Combines the job desc data and returns html
         */
        private buildPositionProfile = (positionProfile: any): string => {

            var combined: string = "";

            if (positionProfile) {
                combined += (positionProfile.Summary ? positionProfile.Summary : '');

                //duties
                if (positionProfile.DutySummary || positionProfile.JobDuties.length) {
                    combined += '<h4>ESSENTIAL DUTIES & RESPONSIBILITIES</h4>';
                    combined += '<p>' + (positionProfile.DutySummary ? positionProfile.DutySummary : '') + '</p>';
                    combined += '<ul>';
                    angular.forEach(positionProfile.JobDuties,(duty) => {
                        combined += '<li>' + duty.Description + '</li>';
                    });
                    combined += '</ul>';
                }

                //tasks
                if (positionProfile.SampleTaskSummary || positionProfile.SampleTasks.length) {
                    combined += '<h4>ESSENTIAL DUTIES & RESPONSIBILITIES</h4>';
                    combined += '<p>' + (positionProfile.SampleTaskSummary ? positionProfile.SampleTaskSummary : '') + '</p>';
                    combined += '<ul>';
                    angular.forEach(positionProfile.SampleTasks,(task) => {
                        combined += '<li>' + task.Name + '</li>';
                    });
                    combined += '</ul>';
                }
                //legalDescriptions
                if (positionProfile.LegalDescriptions.length) {
                    angular.forEach(positionProfile.LegalDescriptions,(desc) => {
                        combined += `
                        <div>
                            <h3 class="subtitle">${desc.Name}</h3>
                            <div class="content sub-text">${desc.Description}</div>
                        </div>
                    `;
                    })
                }
            }

            return combined;

        }

        public fullFromElement = (id: any) => {
            if (!id) return;
            var content = angular.element('#' + id).html();
            return content;
        }

        public printPositionProfile = ($event,includeJobDesc:boolean) => {

            var printObj = {
                vm: this,
                includeJobDesc: includeJobDesc
    }
            this.printerSvc.print(this.basePath + 'Content/js/Training/Templates/print.positionprofile.profile.html', printObj);
        }
    }

    angular.module('app').config(['blockUIConfig', (blockUIConfig) => {
        //disable Loading screen during all HTTP Requests
        blockUIConfig.autoBlock = false;
    }]).controller('Training.PositionProfileEntryView', PositionProfileIndex);

    interface IacceptAndSignModalScope extends angular.ui.bootstrap.IModalScope {
        acceptandsignForm: angular.IFormController;
    }
    class AcceptAndSignModalCtrl {
        isSignPadVisible: boolean = false;
        signersName = "";
        isSupervisor = false;
        employeeSignatureDone = false;

        static $inject = [
            '$scope',
            '$compile',
            '$uibModalInstance',
            'jobDescription',
            'currentContract',
            'chaitea.common.services.awssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.apibasesvc',
            '$log',
            'blockUI'];
        constructor(
            private $scope: IacceptAndSignModalScope,
            private $compile: angular.ICompileService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private jobDescription: trainingInterfaces.IJobDescriptionExtended,
            private currentContract: IContractVm,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private baseSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $log: angular.ILogService,
            private blockUI
            ) {
            this.$scope.acceptandsignForm = jobDescription.JobDescription["AcceptanceForm"] || {};
            
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss();
        };

        showSignPad = (isSupervisor = false) => {
            this.isSupervisor = isSupervisor;
            this.signersName = isSupervisor ? this.jobDescription.SupervisorName : this.jobDescription.Name;
            this.isSignPadVisible = true;
        }

        closeSignPad = (signature: any,forceClose = false) => {
            var that = this;
            if (this.blockUI) this.blockUI.stop();
            if (signature && !signature.isEmpty() && !forceClose) {
                if (confirm("Are you sure want to close? You will lose the signature.")) {
                    that.isSignPadVisible = false;
                    signature.clear();
                    _.defer(() => {
                        that.$scope.$apply()
                    });
                };
            } else {
                that.isSignPadVisible = false;
                signature.clear();
                _.defer(() => {
                    that.$scope.$apply()
                });
            }
        }

        clearSignPad = (signature: any) => {
            signature.clear();
        }

        submitSignPad = (signature: any,isSupervisor = false) => {
            if (signature._isEmpty) {
                alert("Please sign your name to submit.");
                return false;
            }
            this.blockUI.start();
            this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {
                //TODO: upload to cloudiary or S3
                //then save create EmployeeAttachment and add to DB
                var that = this;

                this.awsSvc.s3Upload(signature.toDataURL(), creds, commonInterfaces.S3Folders.image, commonInterfaces.S3ACL.publicRead, true).then((res: commonInterfaces.IAwsObjectDetails) => {
                    that.jobDescription.JobDescription["AcceptanceForm"] = that.$scope.acceptandsignForm;
                    
                    if (res) {
                        var contract: trainingInterfaces.IContract;

                        if (that.isSupervisor && this.currentContract) {
                            contract = this.currentContract;
                        } else {
                            //build new contract
                            contract = <trainingInterfaces.IContract>{
                                ContractAttachments: <Array<trainingInterfaces.IContractAttachments>>[],
                                Employee: {
                                    EmployeeId: that.jobDescription.EmployeeId
                                }
                            }
                        }

                        var attachment = <any>{
                            UniqueId: res.Name,
                            AttachmentType: commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image],
                        };

                        if (that.isSupervisor) {
                            attachment.ContractAttachmentType = trainingInterfaces.IContractAttachmentType[trainingInterfaces.IContractAttachmentType.SupervisorSignature];
                            contract.ContractAttachments.push(attachment);

                            this.baseSvc.update(contract.ContractId, contract, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Contract]).then((data) => {
                                that.currentContract = data;
                                this.$uibModalInstance.close(data);
                            });
                        } else {
                            attachment.ContractAttachmentType = trainingInterfaces.IContractAttachmentType[trainingInterfaces.IContractAttachmentType.EmployeeSignature];
                            contract.ContractAttachments.push(attachment);

                            //Create new contract for employee
                            this.baseSvc.save(contract, 'Contract').then((data) => {
                                that.currentContract = data;
                                that.employeeSignatureDone = true;
                                that.closeSignPad(signature,true);
                            });
                        }

                    }

                },(error) => {
                    this.$log.error(error);
                });

            });

        }

    }

    angular.module('app').controller('Training.PositionProfile.AcceptAndSignModalCtrl', AcceptAndSignModalCtrl);
}