﻿module ChaiTea.Training.Controllers {
    'use strict';

    import trainingSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;

    enum FileType { Undefined, ProfilePicture };
    enum Filters { Undefined, Assigned, Missing, NewHires };

    class PositionProfileIndex {
        isPageDisabled:boolean = true;

        isManagerRole: boolean = false;
        employees: Array<trainingInterfaces.ITrainingEmployee> = [];
        employeesFiltered: Array<trainingInterfaces.ITrainingEmployee> = [];
        counts={
            employees: 0,
            assigned: 0,
            missing: 0,
            newHires : 0
        }
        isPositionProfileViewer: boolean = false;
        isNewHire: boolean = false;
        hideLocation: boolean = false;
        defaultProfileImage: string = '';
        maxStringLength: number = 200;

        location: any = {
            clientName: '',
            siteName: '',
            buildings: [],
            floors: [],
            routes: []
        };
        jd: trainingInterfaces.IJobDescriptionExtended;
        superviser: trainingInterfaces.IJobDescriptionExtended;

        appliedFilter: number = 1;
        isForTeam: boolean = false;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.trainingemployee',
            'chaitea.common.services.trainingsvc',
            'chaitea.common.services.apibasesvc',
            '$timeout',
            'chaitea.common.services.imagesvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: ng.ILogService,
            private trainingEmployeeSvc: trainingSvc.ITrainingEmployeeSvc,
            private trainingSvc: Common.Services.ITrainingSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $timeout: ng.ITimeoutService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
            ) {
            this.isPageDisabled = (userContext.Site.ID === 0);
        }

        initialize = (isForTeam: boolean): void => {
            this.isForTeam = isForTeam;
            this.CanConfigureEmployeesBonus();
            this.getEmployeeList();

            this.isManagerRole = this.canManageEmployeePositionProfile();
            
        }

         /**
         * @description Returns whether the user can configure bonus. 
         */
        private CanConfigureEmployeesBonus = (): void => {
            this.isPositionProfileViewer = this.userInfo.mainRoles.profileviewers;
        }

        /**
        * @description Gets filter type and applies filter to list.
        */
        public applyFilter = ($e,value): void => {
            $e.preventDefault();
            var filter: number = value !== null ? value : parseInt($($e.target).find('input').val());

            //Set to null to deselect selected filter
            this.appliedFilter = (this.appliedFilter === filter) ? null : filter;

            this.filterEmployees(this.appliedFilter);
        }

        /**
        * @description Filters employee list by filter type.
        */
        private filterEmployees = (filter: number): void => {
            if (this.isForTeam) {
                this.employeesFiltered = this.employees;
                _.remove(this.employeesFiltered, (emp: trainingInterfaces.ITrainingEmployee) => {
                    return emp.EmployeeId == this.userInfo.userId
                });
                return;
            }
            else if (filter === null) {
                this.employeesFiltered = this.employees;
                return;
            }

            this.employeesFiltered = _.filter(this.employees,(emp: trainingInterfaces.ITrainingEmployee) => {
                return (filter === Filters.Assigned ? emp.IsComplete : (filter === Filters.NewHires ? emp.isNewHire  : !emp.IsComplete))
            });

        }

        private isWithInWeek = (hireDate: string): boolean => {
            var oneWeekOldDate = moment().subtract(7, 'days').startOf('day');

            return moment(hireDate).isAfter(oneWeekOldDate);
        }
        /**
        * @description Returns whether the user is a manager.
        */
        private canManageEmployeePositionProfile = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Returns a list of employees.
         */
        private getEmployeeList = (): void => {

            this.employees = [];

            //get data from service
            this.apiSvc.getByOdata({ $orderby: 'Name asc'}, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees: Array<trainingInterfaces.ITrainingEmployee>) => {

                angular.forEach(employees,(employee) => {

                    employee.isNewHire = this.isWithInWeek(employee.HireDate);
                    //format
                    employee.HireDate = (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A');
                    
                    //set pfoile image
                    employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);

                    //check if job description is complete
                    this.trainingSvc.isPositionProfileComplete(employee);

                    this.employees.push(employee);

                });
                

                this.counts.employees = employees.length;
                this.counts.assigned = _.filter(this.employees,(emp: trainingInterfaces.ITrainingEmployee) => { return emp.IsComplete }).length;
                this.counts.missing = _.filter(this.employees,(emp: trainingInterfaces.ITrainingEmployee) => { return !emp.IsComplete }).length;
                this.counts.newHires = _.filter(this.employees,(emp: trainingInterfaces.ITrainingEmployee) => { return emp.isNewHire }).length;

                //set default filter
                this.filterEmployees(null);

            });
        }

    }

    angular.module('app').controller('Training.PositionProfileIndex', PositionProfileIndex);
}