﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.JobDescription.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    

    enum FileType { Undefined, ProfilePicture };

    class ConfigCtrl implements commonInterfaces.INgController{  
        
        employeeId;
        employee;
        files = [];
        currentPicture;
        phoneNumber;
        allPositions = [];
        allTasks = [];
        allEquipments = [];

        taskSelectLoading: boolean = false;
        taskInput: string = "";
        defaultProfileImage: string = '';
        equipmentSelectLoading: boolean = false;
        equipmentInput: string = "";
        deletePhoto: boolean = false;
        phoneNumberInput: string = "";
        funFactsInput: string = "";

        sectionChangedStates={
            photo: false,
            position: false,
            tasks: false,
            equipments: false,
            deletePhoto: false,
            phoneNumber: false,
            funfacts: false
        };

        modalInstance;

        static $inject = [
            '$scope',
            '$log',
            '$uibModal',
            'sitesettings',
            "chaitea.common.services.trainingemployee",
            'chaitea.common.services.jobs',
            'chaitea.common.services.imagesvc',
            Common.Services.NotificationSvc.id
        ];
        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings,
            private trainingSvc: ChaiTea.Common.Services.ITrainingEmployeeSvc,
            private jobSvc: ChaiTea.Common.Services.IJobSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private notificationSvc: Common.Services.INotificationSvc) { }

        public initialize = (id: number): void => {
            this.getEmployeeById(id);
            this.getAllPositions();
            this.getAllTasks();
            this.getAllEquipments();
        }

        /**
         * @description function to save image
         * @param {string} fileName
         */
        public uploadImage = (employee, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length) {
                var file = files[0];

                this.imageSvc.readImage(file).then((data: any) => {

                    //error check the image here

                    this.cropImage(data.dataUrl);
                });
               
                
            }
        }


        public cropImage = (file: any) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Common.ImageCropCtrl as vm',
                size: 'md',
                resolve: {
                    file: () => { return file; },
                    needDescription: () => { return false; }
                },
                backdrop: 'static'
            });

            this.modalInstance.result.then((modalRes: any) => {

                if (!modalRes) return;

                this.imageSvc.save(modalRes.originalImage, false, true).then((res: commonInterfaces.IAwsObjectDetails) => {

                    var tempAttach = {
                        UniqueId: res.Name,
                        UserAttachmentType: 'ProfilePicture',
                        AttachmentType: 'Image'
                    }

                    this.employee.UserAttachments = [];
                    this.employee.UserAttachments.push(tempAttach);

                    this.imageSvc.save(modalRes.croppedImage, false, true, this.imageSvc.getThumbnailName(res.Guid)).then((res: commonInterfaces.IAwsObjectDetails) => {

                        this.trainingSvc.updateTrainingEmployee(this.employee.EmployeeId, this.employee).then(() => {
                            this.notificationSvc.successToastMessage('Image saved successfully.');
                            this.currentPicture = res.Url;
                        });

                    }, (error) => {
                        this.notificationSvc.errorToastMessage(error);
                    });

                }, (error) => {
                    this.notificationSvc.errorToastMessage(error);
                });

            }, () => {

            });

        }

        public removePhoto = () => {
            
            bootbox.confirm('Are you sure you want to delete the photo?',(result) => {
                if (result) {
                    this.remove();
                    this.saveEmployee();
                }
            });  
              
        }

        private remove = () => {
            var index: number;
            index = this.getAttachmentIndex(this.employee.UserAttachments);
            if (index != -1) {
                this.employee.UserAttachments.splice(index, 1)
                this.currentPicture = this.imageSvc.getUserImageFromAttachments(this.employee.UserAttachments, true); 
            }
            this.onSectionChange('deletePhoto');
            
        }

        private getAttachmentIndex = (attachments: any[]): number => {

            var attachmentIndex: number = -1;
            var num: number = 0;
            angular.forEach(attachments,(attachment) => {
                if (attachment.UserAttachmentType === FileType[FileType.ProfilePicture]) {
                    attachmentIndex = num;
                    num++;
                }
            });

            return attachmentIndex;
        }

        public saveEmployee = () => {
            this.trainingSvc.updateTrainingEmployee(this.employee.EmployeeId, this.employee).then((data) => {
                this.notificationSvc.successToastMessage("Employee has been updated.");
                this.resetSectionStates();
                
            });
        }
        
        private resetSectionStates = () => {
            this.sectionChangedStates = {
                photo: false,
                position: false,
                tasks: false,
                equipments: false,
                deletePhoto: false,
                phoneNumber: false,
                funfacts: false
            };
        }
        //#region Position

      
        //#endregion


        //#region Tasks
        public addNewTaskItemKeyPress = ($event) => {
            if ($event.which === 13) {
                this.addNewTaskItem($event);
            }
        }

        public savePhoneNumber = ($event) => {
            var phoneNumFromInput: any = this.phoneNumberInput;

            if (_.isObject(this.phoneNumberInput))
                phoneNumFromInput = this.phoneNumberInput['phoneNumber'];

            this.employee.PhoneNumber = phoneNumFromInput;
            //this.phoneNumberInput = "";
            this.onSectionChange('phoneNumber');
            this.saveEmployee();   
        }

        public saveFunFacts = ($event) => {
            var funFactsFromInput: any = this.funFactsInput;
            if (_.isObject(this.funFactsInput))
                funFactsFromInput = this.funFactsInput['funfacts'];
            this.employee.FunFacts = funFactsFromInput;
            this.onSectionChange('funfacts');
            this.saveEmployee();
        }

        public addNewTaskItem = ($event) => {
            var taskNameFromInput:any = this.taskInput;

            if (_.isObject(this.taskInput))
                taskNameFromInput = this.taskInput['Name'];

            if (!taskNameFromInput.length || taskNameFromInput === "") {
                 return false;
            }

            this.taskSelectLoading = true;
            if (!_.any(this.allTasks, { 'Name': taskNameFromInput })) {
                var newJobTask = {
                    "Name": taskNameFromInput
                };
                this.jobSvc.saveNewJobTask(newJobTask).then((data) => {
                    this.allTasks.push(data);
                    this.employee.EmployeeTasks.push(data);
                    this.taskSelectLoading = false;
                    this.taskInput = "";
                    this.onSectionChange('tasks');
                })
            } else {
                if (!_.any(this.employee.EmployeeTasks, this.taskInput)) {
                    this.employee.EmployeeTasks.push(this.taskInput);
                    this.onSectionChange('tasks');
                }
                else {
                    var message: string = this.taskInput['Name'] + " task has already been assigned";
               
                    bootbox.alert(message);
                }
                this.taskInput = "";
                
            };
        }

        public removeNewTaskItem = (index) => {
            this.employee.EmployeeTasks.splice(index, 1);
            this.onSectionChange('tasks');
        }

        public getTasksForView = (current) => {
            var tasksCopy = angular.copy(this.allTasks);
            if (current) {
               // tasksCopy.unshift(current);
            }
            return tasksCopy;
        }

        public onTaskSelection = (item, model, label) => {
            

            
        }
        //#endregion

        //#region Equipment and Supplies
        public addNewEquipmentItemKeyPress = ($event) => {
            if ($event.which === 13) {
                this.addNewEquipmentItem($event);
            }
        }

        public addNewEquipmentItem = ($event) => {

            var equipmentNameFromInput: any = this.equipmentInput;
           
            if (_.isObject(this.equipmentInput)) equipmentNameFromInput = this.equipmentInput['Name'];


            if (!equipmentNameFromInput.length || equipmentNameFromInput === "") {
                return false;
            }
           
            this.equipmentSelectLoading = true;
            if (!_.any(this.allEquipments, { 'Name': equipmentNameFromInput })) {

                var newJobEquipment = {
                    "Name": equipmentNameFromInput
                };
                this.jobSvc.saveNewJobEquipment(newJobEquipment).then((data) => {
                    this.allEquipments.push(data);
                    this.employee.EmployeeEquipment.push(data);
                    this.equipmentSelectLoading = false;
                    this.equipmentInput = "";
                    this.onSectionChange('equipments');                 
                })

            } else {

                //get obj from all list
                var exisitng: any = _.find(this.allEquipments, { 'Name': equipmentNameFromInput })

                if (!_.any(this.employee.EmployeeEquipment, exisitng)) {
                    this.employee.EmployeeEquipment.push(exisitng);
                    this.onSectionChange('equipments');
                } else {
                    var message: string = this.equipmentInput['Name'] + " equipment has already been assigned";
                    bootbox.alert(message);
                }
                this.equipmentInput = "";
                
            };
        }

        public removeNewEquipmentItem = (index) => {
            this.employee.EmployeeEquipment.splice(index, 1);
            this.onSectionChange('equipments');
        }
     
        public getEquipmentsForView = (current) => {
            var equipmentsCopy = angular.copy(this.allEquipments);
            if (current) {
                // equipmentsCopy.unshift(current);
            }
            return equipmentsCopy;
        }

        public onEquipmentSelection = (item, model, label) => {



        }
        //#endregion

        public onSectionChange(sectionName: string) {
            this.sectionChangedStates[sectionName] = true;
        }

        /**
         * @description Returns the job description object by employee id.
         */
        private getEmployeeById = (employeeId: number): void => {

            //get data from service
            this.trainingSvc.getPositionProfileById(employeeId).then((jobDescription: trainingInterfaces.IJobDescriptionExtended) => {
                if (!jobDescription.EmployeeId) return false;

                this.employee = jobDescription;
                
                this.employeeId = jobDescription.EmployeeId;

                jobDescription.ProfileImage = this.imageSvc.getUserImageFromAttachments(jobDescription.UserAttachments, true);
                this.currentPicture = jobDescription.ProfileImage;
                this.phoneNumberInput = jobDescription.PhoneNumber;
                this.funFactsInput = jobDescription.FunFacts;
            });
        }

        private getAllPositions = () => {
            this.jobSvc.getAllPositions().then((data) => {
                angular.forEach(data,(item) => {
                    this.allPositions.push(item);
                })
            })
        }

        private getAllTasks = () => {
            this.jobSvc.getAllTasks().then((data) => {
                angular.forEach(data,(item) => {
                    this.allTasks.push(item);
                })
            })
        }

        private getAllEquipments = () => {
            this.jobSvc.getAllEquipments().then((data) => {
                angular.forEach(data,(item) => {
                    this.allEquipments.push(item);
                })
            })
        }

    }

    angular.module('app').controller('Training.JobDescriptionConfig', ConfigCtrl);
}