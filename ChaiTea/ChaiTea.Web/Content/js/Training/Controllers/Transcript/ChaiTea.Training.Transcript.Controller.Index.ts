﻿module ChaiTea.Training.Course.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IImageSize {
        width: number;
        height: number;
    }

    enum FileType { Undefined, ProfilePicture };

    class TranscriptIndex implements commonInterfaces.INgController {
        userId: number;
        employeeId: number;
        employeeUserId: number;
        employee = {
            Id:   0,
            Name:   '',
            JobDescription: '',
            HireDate: null,
            ProfilePicture: ''
        };
        isManager: boolean = false;
        //selectedEmployeeUserId: number;

        basePath: string;
        courseTypes: Array<serviceInterfaces.ICourseType> = [];

        counts = {
            assigned: {
                courses: 0,
                pastDue: 0,
                completed: 0,
                compliancePercentage: 0,
                pageCount: 0
            },
            self_initiated: {
                self_assigned: 0,
                completed: 0,
                pageCount: 0
            }
            
        }

        trainings: Array<{ isPassed: boolean; passedDate: Date; }> = [];
        control: number = 80;
        hideNavigation: boolean = false;
        selectedEmployeeCourse: serviceInterfaces.IEmployeeCourse;

        assignedEmployeeCourses: Array<serviceInterfaces.IEmployeeCourse> = [];
        selfEmployeeCourses: Array<serviceInterfaces.IEmployeeCourse> = [];

        filters: any = {
            assigned: 'all',
            self: 'all',
            sort: 'duedate',
            view: 'all'
        };

        rowTemplate: string = '';
        rowTemplateMobile: string = '';
        showAcknowledgeButton: boolean = false;
        countsFilter: number = 0;
        assignedFilter: boolean = false;
        assignedSort: boolean = false;
        initiatedFilter: boolean = false;

        datePickerTracker: Array<any> = [];

        currentAssignedCoursesPage: number = 1;
        currentSelfAssignedCoursesPage: number = 1;
        top: number = 10;
        params = {
            employeeId: null,
            employeeUserId: null,
            orderbyDueDate: true,
            top: 10,
            skip: 0,
            filterByCourseCompletion: null,
            filterBySelfAssigned: false
        };
        employeeCourseToUpdate: serviceInterfaces.IEmployeeCourse;

        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.trainingsvc',
            'userContext',
            'userInfo',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.awssvc',
            Common.Services.UtilitySvc.id,
            Common.Services.NotificationSvc.id,
            Common.Services.UserInfoSvc.id
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: Common.Services.IUtilitySvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private userInfoSvc: ChaiTea.Common.Services.IUserInfoSvc) {
            this.employeeId = userInfo.userId;
            this.userId = userContext.UserId;
            this.basePath = sitesettings.basePath;
            this.isManager = this.userInfo.mainRoles.managers;

            this.rowTemplate = this.basePath + 'Content/js/Training/Templates/KnowledgeCenter/knowledgecenter.transcript.row.html';
            this.rowTemplateMobile = this.basePath + 'Content/js/Training/Templates/KnowledgeCenter/knowledgecenter.transcript.row.mobile.html';

            //check for any chages and update
            this.$scope.$watch('vm.employeeCourseToUpdate', (newValue: serviceInterfaces.IEmployeeCourse, oldValue: serviceInterfaces.IEmployeeCourse) => {

                if (oldValue && !moment(oldValue.DueDate).isSame(newValue.DueDate)) {

                    //update compliance
                    newValue.IsCompliant = moment(newValue.DueDate).isAfter(moment());
                    this.setStatusIcon(newValue);

                    var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.EmployeeCourse];
                    this.apiSvc.update(newValue.EmployeeCourseId, newValue, endpoint).then((res) => {
                        this.getEmployeeComplianceStats();
                    }, this.onFailure);

                }

            }, true);
        }

        public initialize = (): void => {
            var paramId = this.utilSvc.getIdParamFromUrl(null, true);
            if (!paramId) {
                paramId = this.utilSvc.getQueryParamFromUrl(Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString());
            }
            this.employeeId = (paramId || this.userInfo.userId);

            if (this.employeeId != this.userInfo.userId) {
                this.hideNavigation = true;
                this.userInfoSvc.getUserInfoByOrgUserId(this.employeeId).then(result => {
                    this.employeeUserId = result.userId;
                    this.getCourseTypes();
                    this.getEmployeeComplianceStats();
                    this.getAssignedCourses();
                    this.getSelfInitializedCourses();
                });
            } else {
                this.employeeUserId = this.userId;
                this.getCourseTypes();
                this.getEmployeeComplianceStats();
                this.getAssignedCourses();
                this.getSelfInitializedCourses();
            }
        }

        /**
         * @description Get User Image
         */
        private getUserImage = (attachments: any, size: IImageSize): string => {
            var userImg: string = '';
            //TODO [DW] IMAGESVC: Might need to get larger size 120x120
            userImg = this.imageSvc.getUserImageFromAttachments(attachments, true);
            return userImg;
        }

        getCourseTypes = () => {
            this.apiSvc.get("Lookuplist/CourseTypes").then((data) => {
                this.courseTypes = data;
            }, this.onFailure);
        }

        getSelfInitializedCourses = () => {
            this.selfEmployeeCourses = [];
            this.params.filterBySelfAssigned = true;
            this.params.skip = (this.currentSelfAssignedCoursesPage - 1) * this.top;
            this.loadData(this.params);
        }

        getAssignedCourses = () => {
            this.assignedEmployeeCourses = []; 
            this.params.filterBySelfAssigned = false;
            this.params.skip = (this.currentAssignedCoursesPage - 1) * this.top;
            this.loadData(this.params);
        }
        /**
         * @description Load course data and calculate course compliance
         */
        private loadData = (params): void => {
            this.params.employeeId = this.employeeId;
            this.params.employeeUserId = this.employeeUserId;

            // This call passes The Top, Skip, Sort and other params as querysting as well
            this.apiSvc.query(params, "EmployeeCourse/AllByEmployeeId").then((employeeCourses: Array<serviceInterfaces.IEmployeeCourse>) => {
               
                angular.forEach(employeeCourses,(employeeCourse: serviceInterfaces.IEmployeeCourse) => {
                    employeeCourse.Views = 0;
                            
                    angular.forEach(employeeCourse.Course.CourseAttachments, (file: any) => {

                        if (file.CourseAttachmentType == CourseAttachmentTypes[CourseAttachmentTypes.Cover]) {
                            employeeCourse.FeaturedImage = this.awsSvc.getUrlByAttachment(file.Attachment.UniqueId, file.Attachment.AttachmentType);
                        }

                        employeeCourse.Views += file.Views;

                    });

                    employeeCourse.FeaturedImage = (employeeCourse.FeaturedImage ? employeeCourse.FeaturedImage : this.basePath + 'Content/img/no-course-image-available.png');

                    employeeCourse.CompletedDate = moment(employeeCourse.CompletedDate).format('MM/DD/YYYY');
                    employeeCourse.IsActive = true;

                    employeeCourse.CanAcknowledge = (employeeCourse.IsSelfInitiated ? false : true);
                    employeeCourse.CanDelete = false;
                    employeeCourse.ShowAcknowledgeButton = false;

                    if (!employeeCourse.IsCompleted && employeeCourse.IsSelfInitiated) {
                        employeeCourse.CanDelete = true;
                    }

                    this.setStatusIcon(employeeCourse);

                    //add ts for better sorting
                    var temp: any = _.cloneDeep(employeeCourse);
                    temp.timestamp = moment(temp.DueDate).unix();

                    //sort courses by assign and self
                    if (employeeCourse.IsSelfInitiated) {
                        this.selfEmployeeCourses.push(temp);
                    } else {
                        this.assignedEmployeeCourses.push(temp);
                    }

                    this.datePickerTracker[temp.EmployeeCourseId] = { id: temp.EmployeeCourseId, open: false };

                });

            }, this.onFailure);
        }

        /**
        * @description Gets transcript metrics
        */
        getEmployeeComplianceStats = () => {
            var employeeId = this.userInfo.userId;
            if (this.isManager) {
                employeeId = this.employeeId;
            } 
            this.apiSvc.query({ employeeId: employeeId }, 'EmployeeCourse/EmployeeComplianceStats/:employeeId', false, false).then((response) => {
                this.counts.assigned.compliancePercentage = _.round(response.CompliancePercentage);
                this.counts.assigned.courses = response.TotalAssignedCourses;
                this.counts.assigned.completed = response.TotalCompliant;
                this.counts.self_initiated.self_assigned = response.TotalSelfAssignedCourses;
                this.counts.assigned.pageCount = response.TotalAssignedCourses;
                this.counts.self_initiated.pageCount = response.TotalSelfAssignedCourses;
            }, this.onFailure);
        }

        /**
        * @description Toggle course status
        */
        public toggleCourseCompletion = ($event: any, employeeCourse: serviceInterfaces.IEmployeeCourse, isPassed?: boolean): void => {
            
            $event.preventDefault();    

            if (isPassed == null) {
                isPassed = (employeeCourse.IsCompleted ? false : true);
            }

            employeeCourse.IsCompleted = isPassed;
            var msg = (employeeCourse.IsCompleted ? 'Course marked as acknowledged.' : 'Course marked as unacknowledged.');

            this.setStatusIcon(employeeCourse);

            //manually create snapshots for all trainings
            angular.forEach(employeeCourse.Course.CourseAttachments, (attachment: serviceInterfaces.ICourseAttachment) => {
                this.trainingSvc.saveSnapshot(employeeCourse, attachment, isPassed, [], this.userInfo.userId).then((res) => {
                    employeeCourse.IsCompleted = isPassed;
                    employeeCourse.CompletedDate = moment().format('MM/DD/YYYY');
                    this.getEmployeeComplianceStats();
                });
            });

            this.notificationSvc.successToastMessage(msg);
        }

        /**
        * @description Sets employee course to inactive
        */
        public deleteEmployeeCourse = ($event: any, employeeCourse: serviceInterfaces.IEmployeeCourse): void => {
            $event.preventDefault();  

            this.alertSvc.confirmWithCallback('Are you sure you want to delete this course from your transcript?', (res: boolean) => {
                
                if (res) {
                    
                    employeeCourse.IsActive = false;
                    this.$scope.$apply();

                    this.apiSvc.delete(employeeCourse.EmployeeCourseId, 'EmployeeCourse').then((data) => {
                        this.notificationSvc.successToastMessage('Course deleted');
                    });

                }

            });

        }

        /**
         * @description Apply filter and filter employee course lists
         */
        public applyFilter = (type: string, filter: string): void => {

            if (filter == 'completed') {
                this.params.filterByCourseCompletion = true;
                if (type == 'assigned') {
                    this.counts.assigned.pageCount = this.counts.assigned.completed;
                    this.getAssignedCourses();
                } else {
                    this.counts.self_initiated.pageCount = this.counts.self_initiated.completed;
                    this.getSelfInitializedCourses();
                }
                
                
            } else if (filter == 'incomplete') {
                this.params.filterByCourseCompletion = false;
                if (type == 'assigned') {
                    this.counts.assigned.pageCount = this.counts.assigned.courses - this.counts.assigned.completed;
                    this.getAssignedCourses();
                } else {
                    this.counts.self_initiated.pageCount = this.counts.self_initiated.self_assigned - this.counts.self_initiated.completed;
                    this.getSelfInitializedCourses();
                }
            } else {
                this.params.filterByCourseCompletion = null;
                if (type == 'assigned') {
                    this.counts.assigned.pageCount = this.counts.assigned.courses;
                    this.getAssignedCourses();
                } else {
                    this.counts.self_initiated.pageCount = this.counts.self_initiated.self_assigned;
                    this.getSelfInitializedCourses();
                }
            }

        }

        /**
         * @description Handle due date button click
         */
        private changeDueDate = ($event, employeeCourse: serviceInterfaces.IEmployeeCourse): void => {
            $event.preventDefault();
            $event.stopPropagation();
            this.datePickerTracker[employeeCourse.EmployeeCourseId].open = true;
            this.employeeCourseToUpdate = employeeCourse;
        }

        /**
         * @description Apply date filter
         */
        private applySortFilter = (type: string, filter: string): void => {
            this.params.orderbyDueDate = (filter != 'pastdue');
            if (type == 'assigned') {
                this.getAssignedCourses()
            }
            else {
                this.getSelfInitializedCourses()
            }
        }

        /**
         * @description Set icon by employee course status
         */
        private setStatusIcon = (employeeCourse: serviceInterfaces.IEmployeeCourse): void => {
            employeeCourse.StatusIcon = 'fa fa-fail';
            employeeCourse.StatusClass = 'failing';
            if (employeeCourse.IsCompliant && !employeeCourse.IsCompleted) {
                employeeCourse.StatusIcon = 'fa fa-incomplete';
                employeeCourse.StatusClass = 'incomplete';
            }
            if (employeeCourse.IsCompleted) {
                employeeCourse.StatusIcon = 'fa fa-check22';
                employeeCourse.StatusClass = 'complete';
            }
        }

        /**
         * @description On filter change
         */
        public filterChange = ($event: any, type: string): void => {
            this.currentAssignedCoursesPage = 1;
            this.currentSelfAssignedCoursesPage = 1;
            this.applyFilter(type, this.filters[type]);
        }

        /**
         * @description On sort change
         */
        public sortChange = ($event: any, type: string): void => {
            this.applySortFilter(type, this.filters.sort);
        }

        /**
         * @description On filter change mobile
         */
        public filterChangeMobile = ($event: any, type: string, filter: string): void => {
            this.currentAssignedCoursesPage = 1;
            this.currentSelfAssignedCoursesPage = 1;
            this.applyFilter(type, filter);
        }

        /**
         * @description On sort change mobile
         */
        public sortChangeMobile = ($event: any, type: string, filter: string): void => {
            this.applySortFilter(type, filter);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }
    }

    angular.module('app').controller('Training.TranscriptIndex', TranscriptIndex);
} 