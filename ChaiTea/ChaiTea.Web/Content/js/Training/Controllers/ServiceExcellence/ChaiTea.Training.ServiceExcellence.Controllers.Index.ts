﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;

    export interface IPrintSEPScoreCards {
        scoreCardItems: Array<IPrintScoreCardItem>;
    }
    export interface IPrintScoreCardItem {
        employee: trainingInterfaces.ISETrainingEmployee;
        evaluationDate: Date;
        site: ISite;
        bonusAmount: string;
    }

    interface BoxScoreHeaderCounts {
        Attendance: number;
        Conduct: number;
        Complaints: number;
        Safety: number;
        ReportIt: number;
        Training: number;
        Quality: number;
        Professionalism: number;
        Comments: number;
    }

    interface StatisticsCounts {
        All: number;
        Incomplete: number;
        Failing: number;
    }

    enum FileType { Undefined, ProfilePicture };
    enum Filters { Incomplete, Failing, All };

    class SEPIndexCtrl implements Common.Interfaces.INgController {
        basePath: string;
        isPageDisabled: boolean = true;
        createNewItems = [];
        siteBonus: number = 0;
        isManager: boolean = false;

        employees: Array<trainingInterfaces.IEmployeeInfo> = [];
        getEmployees: Array<trainingInterfaces.IEmployeeInfo> = [];

        modalInstance;

        counts = {
            stats: <StatisticsCounts>{
                All: 0,
                Incomplete: 0,
                Failing: 0
            },
            header: <BoxScoreHeaderCounts>{
                Attendance: 0,
                Conduct: 0,
                Complaints: 0,
                Safety: 0,
                ReportIt: 0,
                Training: 0,
                Quality: 0,
                Professionalism: 0,
                Comments: 0
            }
        }
        
       individualCounts = <BoxScoreHeaderCounts>{
            Attendance: 0,
            Conduct: 0,
            Complaints: 0,
            Safety: 0,
            ReportIt: 0,
            Training: 0,
            Quality: 0,
            Professionalism: 0,
            Comments: 0
         }

       KPIs = <BoxScoreHeaderCounts>{
           Attendance: Common.Interfaces.KPIType.Attendance,
           Conduct: Common.Interfaces.KPIType.Conduct,
           Complaints: Common.Interfaces.KPIType.Complaints,
           Safety: Common.Interfaces.KPIType.Safety,
           ReportIt: Common.Interfaces.KPIType.Ownership,
           Training: Common.Interfaces.KPIType.Training,
           Quality: Common.Interfaces.KPIType.Quality,
           Professionalism: Common.Interfaces.KPIType.Professionalism
       }
     
        dateRange: { month: number; year: number; endDate: Date} = {
            month: moment().month() + 1,
            year: moment().year(),
            endDate: new Date()
        };

        defaultProfileImage: string = '';

        appliedFilter: number = 0;

        filter: number = 2;
        countFilter: number = 0;

        recordsToSkip: number = 0;
        top: number = 5;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        check: boolean = true;

        isFinalized: boolean = false;
        scorecardStatus: trainingInterfaces.IScorecardStatus;
        employeeWithImage: commonInterfaces.IEmployeeWithAttachment;
        dataPoints = {
            totalBonusAmount: 0,
            passedCount: 0,
            failedCount: 0
        }
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.trainingemployee',
            'chaitea.training.services.serviceexcellence',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.core.services.printer',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$timeout',
            '$q',
            'blockUI',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.training.services.SEPBonusConfig',
            '$uibModal',
            'chaitea.core.services.usercontextsvc',
            Common.Services.AlertSvc.id
        ];
        constructor(
            private $scope: ng.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: ng.ILogService,
            private trainingEmployeeSvc: trainingSvc.ITrainingEmployeeSvc,
            private serviceExcellenceSvc: Services.IServiceExcellenceSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private printerSvc: ChaiTea.Core.Services.IPrinter,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $timeout: ng.ITimeoutService,
            private $q: ng.IQService,
            private blockUI,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private sepBonusSvc: trainingServices.ISEPBonusConfigSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContextSvc: Core.Services.IUserContextSvc,
            private alertSvc: Common.Services.IAlertSvc
            ) {
            this.isPageDisabled = (userContext.Site.ID === 0);
            this.basePath = sitesettings.basePath;
            this.filter = Filters.Incomplete;
            this.countFilter = Filters.Incomplete;
            this.isManager = this.userInfo.mainRoles.managers;

            this.createNewItems = [
                {
                    link: this.sitesettings.basePath + 'Personnel/Attendance',
                    name: 'Attendance'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Conduct',
                    name: 'Conduct'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Complaint',
                    name: 'Complaint'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Compliment',
                    name: 'Compliment'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Safety',
                    name: 'Safety'
                },
                {
                    link: this.sitesettings.basePath + 'Personnel/Ownership',
                    name: 'Report It'
                },
                {
                    link: this.sitesettings.basePath + 'Quality/Audit',
                    name: 'Quality'
                },
                {
                    paramsLink: this.sitesettings.basePath + 'Personnel/Audit?runAudit=true',
                    name: 'Professionalism'
                }
            ];
        }

        public initialize = (): void => {

            if (this.isPageDisabled) {
                this.userContextSvc.OpenContextPicker(true);
                return;
            }

            this.tabFilterItems = [
                {
                    title: 'Incomplete',
                    onClickFn: () => { this.changeFilter(null, Filters.Incomplete) },
                    count: 0
                },
                {
                    title: 'Failing',
                    onClickFn: () => { this.changeFilter(null, Filters.Failing) },
                    count: 0
                },
                {
                    title: 'All',
                    onClickFn: () => { this.changeFilter(null, Filters.All) },
                    count: 0
                }
            ];

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate);
                this.dateRange.month = moment(this.dateRange.endDate).month() + 1;
                this.dateRange.year = moment(this.dateRange.endDate).year();
            }

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.dateRange.month = moment(obj.endDate).month() + 1;
                this.dateRange.year = moment(obj.endDate).year();
                
                this.dateRange.endDate = new Date(this.dateRange.year, this.dateRange.month, 0);
                this.check = true;
                this.getScorecardStatus();
                this.getSiteBonus();
            });
            this.$scope.$watch(() => (this.counts), (newVal: any, oldVal: any) => {
                if (newVal === oldVal) return false;

                this.tabFilterItems[0].count = this.counts.stats.Incomplete;
                this.tabFilterItems[1].count = this.counts.stats.Failing;
                this.tabFilterItems[2].count = this.counts.stats.All;

            }, true);
            mixpanel.track("Viewed SEP Scorecard");
        }

        private getUserInfo = (): void => {
            this.apiSvc.getById(this.scorecardStatus.CreateById, 'TrainingEmployee').then((employee) => {

                this.employeeWithImage = <commonInterfaces.IEmployeeWithAttachment>{
                    Name: employee.Name,
                    EmployeeId: employee.EmployeeId,
                    HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                    JobDescription: employee.JobDescription.Name,
                    ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                }

            }, this.onFailure);
        }

        private calculateDataPoints = () => {
            this.dataPoints.totalBonusAmount = 0;
            _.forEach(this.employees, (emp) => {
                if (!emp.IsExempt && emp.IsPass == true) {
                    this.dataPoints.totalBonusAmount += emp.BonusAmount;
                }
            });

            this.dataPoints.failedCount = _.where(this.employees, { 'IsPass': false }).length;
            this.dataPoints.passedCount = _.where(this.employees, { 'IsPass': true }).length;
        }

        private getScorecardStatus = () => {
            this.isFinalized = false;
            var params = {
                month: this.dateRange.month,
                year: this.dateRange.year
            }
            this.apiSvc.query(params, 'ServiceExcellence/ScorecardStatus/:month/:year', false, false).then((result) => {
                if (result.IsFinalized) {
                    this.filter = 2;    // reset Filter to "ALL" for closed periods.
                    this.scorecardStatus = result;
                    this.isFinalized = this.scorecardStatus.IsFinalized ? true : false;
                    this.getUserInfo();
                }
            }, this.onFailure);
        }
        public getEmployeeList = (): any => {

            this.serviceExcellenceSvc.getEmployeeList(this.dateRange.year, this.dateRange.month).then((employees) => {

                this.getEmployees = employees;

                angular.forEach(this.getEmployees, (employee) => {
                    employee.BonusAmount += this.siteBonus;
                    employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
                });
                
                //emit message to Monthly Datepicker directive to hide or show the back button
                this.messageBusSvc.emitMessage('components.headerpanel:monthly-date:toggle-backbutton',
                    { show: (this.getEmployees.length ? true : false) });

                this.getStatsCounts();
                
                this.getInfo();
                if (this.isFinalized) {
                    this.calculateDataPoints();
                }
                
            });
        }

        public getStatsCounts = (): any => {
            this.counts.stats.Failing = _.where(this.getEmployees, { 'IsPass': false }).length;
            this.counts.stats.Incomplete = _.where(this.getEmployees, { 'IsPass': null }).length;
            this.counts.stats.All = this.getEmployees.length;
        }

        public getSiteBonus = (): any => {
            this.siteBonus = 0;
            this.sepBonusSvc.getSiteBonus({ month: this.dateRange.month, year: this.dateRange.year }).then((result): any => {
                this.getEmployeeList();
                if (result.length) {
                    this.siteBonus = result[0].BonusAmount ? result[0].BonusAmount : 0;
                }
            });
        }

        public rowClick = (EmployeeId: number): void => {

            window.location.href = this.basePath + 'Training/ServiceExcellence/Entry/' + EmployeeId;

        }

        public changeFilter = ($event, filterId): void => {
            this.filter = filterId;
            this.getInfo();
        }

        public getInfo = (): any => {
            this.employees = [];

            for (var item in this.counts.header) {
                this.counts.header[item] = 0;
            }

          
            this.employees = this.filter == Filters.Incomplete ? _.where(this.getEmployees, { 'IsPass': null }) : this.filter == Filters.Failing ? _.where(this.getEmployees, { 'IsPass': false }) : this.getEmployees;

            angular.forEach(this.employees, (employee) => {
                if (employee.EmployeeKpis.length > 0) {
                    this.counts.header.Attendance += !employee.EmployeeKpis[this.KPIs.Attendance].IsExempt && employee.EmployeeKpis[this.KPIs.Attendance].IsPass == false ? 1 : 0;
                    this.counts.header.Conduct += employee.EmployeeKpis[this.KPIs.Conduct].IsPass == false ? 1 : 0;
                    this.counts.header.Complaints += employee.EmployeeKpis[this.KPIs.Complaints].IsPass == false ? 1 : 0;
                    this.counts.header.Safety += employee.EmployeeKpis[this.KPIs.Safety].IsPass == false ? 1 : 0;
                    this.counts.header.ReportIt += employee.EmployeeKpis[this.KPIs.ReportIt].IsPass == false ? 1 : 0;
                    this.counts.header.Training += employee.EmployeeKpis[this.KPIs.Training].IsPass == false ? 1 : 0;
                    this.counts.header.Quality += employee.EmployeeKpis[this.KPIs.Quality].IsPass == null ? 1 : 0;
                    this.counts.header.Professionalism += employee.EmployeeKpis[this.KPIs.Professionalism].IsPass == null ? 1 : 0;
                    this.counts.header.Comments += employee.Comment == null || employee.Comment == '' ? 1 : 0;
                }
            });
            this.check = false;
            this.individualCounts = this.counts.header;

        }

        /**
         * @description Compiles list of employees and sends to Core.Services.Printer
         */
        public printAllScoreCards = ($event) => {

            this.blockUI.start('Loading...');

            this.buildScorecardEmployee().then((employees) => {
                //compile object + list of employees for print
               
                var printObj = {
                    scoreCardItems: employees
                }

                this.$timeout(() => {
                    if (this.blockUI) this.blockUI.stop();
                }, 1000);

                this.printerSvc.print(this.basePath + 'Content/js/Training/Templates/print.sep.scorecard.html', printObj);
            })

        }

        buildScorecardEmployee = (): angular.IPromise<any> => {
            var def = this.$q.defer();
            var promises = [];

            var employeesCopy = angular.copy(this.getEmployees);

            angular.forEach(employeesCopy,(employeeCopy, i) => {
                promises.push(
                    this.serviceExcellenceSvc.getEmployeeKpi(this.dateRange.year, this.dateRange.month, employeeCopy.EmployeeId).then((employee) => {
                        var date = moment(this.dateRange.endDate); 
                        employeesCopy[i]['evaluationStartDate'] = date.startOf('month').toDate();
                        employeesCopy[i]['evaluationEndDate'] = date.endOf('month').toDate();
                        employeesCopy[i]['site'] = this.userContext.Site;
                        employeesCopy[i]['bonusAmount'] = " ";

                        //TODO TEMP hide attendance and quality audits
                        if (employee.EmployeeKpis.length > 0) {
                            employee.EmployeeKpis[this.KPIs.Attendance].Summary = "";
                            employee.EmployeeKpis[this.KPIs.Quality].Summary = "";
                        }

                        employeesCopy[i]['EmployeeKpis'] = employee.EmployeeKpis;
                    }))
            })

            this.$q.all(promises).then((cb) => {
                def.resolve(employeesCopy);
            });

            return def.promise;
        }


        /**
         * @description Compiles list of employees and sends to Core.Services.Printer
         */
        public printBonusList = ($event) => {
            //compile object + list of employees for print
            var printObj = {
                Client: this.userContext.Client.Name,
                Site: this.userContext.Site.Name,
                Program: this.userContext.Program.Name,
                employees: _.where(this.getEmployees, { 'IsPass': true })
            }
            this.printerSvc.print(this.basePath + 'Content/js/Training/Templates/print.sep.bonuslist.html', printObj);
        }
        

        /**
        * @description to export bonus details to excel sheet 
        */
        export = (): void=> {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Bold: "1" } } },
                };
                var bonusListToExport = [];
                angular.forEach(this.employees, (emp) => {
                    if (emp.IsPass) {
                        bonusListToExport.push({
                            "Employee Name": emp.Name,
                            "Pass/Fail": emp.IsPass != null ? (emp.IsPass ? 'Pass' : 'Fail') : 'Incomplete',
                            "Employee Number": emp.ExternalEmployeeId,
                            "BLANK": '',
                            "Pay Code": 430,
                            "BLANK1": '',
                            "BLANK2": '',
                            "Work Date": '',
                            "LS Amount": emp.IsPass != null ? (emp.IsPass ? emp.BonusAmount : 0) : 0,
                            "BLANK3": ''
                        });
                    }
                });
                alasql('SELECT * INTO CSV("BonusList.csv",?) FROM ?', [mystyle, bonusListToExport]);
        }

        public createKpi = (employeeId: number): any => {
            this.apiSvc.getByOdata({ year: this.dateRange.year, month: this.dateRange.month, id: employeeId }, "ServiceExcellence/:year/:month/:id", false, false).then((result) => {
            }, this.onFailure);
            window.location.reload();
        }

        public popModal = (module, employee) => {
            var dateRange = {
                year: this.dateRange.year,
                month: this.dateRange.month
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/ServiceExcellence/sep.boxScoreModal.html',
                controller: 'Common.SEPBoxScoreModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    dateRange: () => { return dateRange; },
                    employee: () => { return employee; },
                    module: () => { return module; },
                    isScorecardFinalized: () => { return this.isFinalized; },
                    isReportCard: () => { return false; },
                    year: () => { return 0; },
                    month: () => { return 0; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    window.location.reload();
                }
            });
        }

        public finalizeScorecard = () => {
            var allowFinalize = true;
            if (this.counts.stats.Incomplete) {
                angular.forEach(this.getEmployees, (employee) => {
                    var empKpis = _.filter(employee.EmployeeKpis, function (kpi) {
                        return kpi.Kpi.KpiId != ChaiTea.Common.Enums.KPITYPE.Professionalism && kpi.Kpi.KpiId != ChaiTea.Common.Enums.KPITYPE.Ownership && kpi.IsPass == null
                    });
                    if (empKpis && empKpis.length > 0) {
                        allowFinalize = false;
                        return;
                    }
                });
            }
            if (allowFinalize) {
                this.openFinalizeModal();
            }
            else {
                this.alertSvc.alertWithCallback("All employees scorecard needs to be complete before finalizing scorecard");
            }
        }

        public openFinalizeModal = () => {
            this.calculateDataPoints();
            var dateRange = {
                year: this.dateRange.year,
                month: this.dateRange.month
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Training/Templates/ServiceExcellence/sep.finalizeModal.html',
                controller: 'Training.SEPFinalizeModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    dateRange: () => { return dateRange; },
                    employeesScorecardInfo: () => { return this.employees },
                    finalizedDataPoints: () => { return this.dataPoints }
                }
            }).result.then((isFinalized) => {
                if (isFinalized) {
                    window.location.reload();
                }
            });
        }

        private goToTranscriptPage = (employeeId: number) => {
            window.location.href = this.basePath + "Training/Transcript/Index/" + employeeId;
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

    }

    angular.module("app").controller('Training.SEPIndexCtrl', SEPIndexCtrl);

}