﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;



    class SEDetailsCtrl implements Common.Interfaces.INgController {

        basePath: string;
        isPageDisabled: boolean = true;
        kpiId: number;

        title: string = '';

        employees: Array<trainingInterfaces.ISETrainingEmployee> = [];
        employeesByKpi: Array<Interfaces.ISETrainingEmployee> = [];
        employeeSEPs: Array<Interfaces.ISETrainingEmployee> = [];

        dateRange: { month: number; year: number } = {
            month: moment().month() + 1,
            year: moment().year()
        };

        counts = {
            employees: 0,
            audits: {
                incomplete: 0,
                failed: 0,
                passed: 0
            }
        }

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.trainingemployee',
            'chaitea.training.services.serviceexcellence',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.core.services.printer',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$timeout',
            '$q'
        ];
        constructor(
            private $scope: ng.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: ng.ILogService,
            private trainingEmployeeSvc: trainingSvc.ITrainingEmployeeSvc,
            private serviceExcellenceSvc: Services.IServiceExcellenceSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private printerSvc: ChaiTea.Core.Services.IPrinter,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $timeout: ng.ITimeoutService,
            private $q: ng.IQService
            ) {

            this.isPageDisabled = (userContext.Site.ID === 0);
            this.basePath = sitesettings.basePath;

        }

        public initialize = (kpiId: number): void => {

            this.kpiId = kpiId;
            this.title = this.setTitle();

            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.month = moment(obj.endDate).month() + 1;
                this.dateRange.year = moment(obj.endDate).year();

                this.getKPIsEmployees();
            });

        }

        private setTitle = (): any => {

            var titles: Array<string> = ['', 'Safety', 'Complaints', 'Conduct', 'Attendance', 'Professionalism', 'Quality Audits', 'Training', 'Ownership']

            return titles[this.kpiId]; 
        }

        /**
        * @description Refreshes list by filter.
        */
        private getKPIsEmployees = (): any => {
            this.employees = [];

            this.serviceExcellenceSvc.getEmployeeList(this.dateRange.year, this.dateRange.month).then((employees) => {
                //emit message to Monthly Datepicker directive to hide or show the back button
                this.messageBusSvc.emitMessage('components.headerpanel:monthly-date:toggle-backbutton',
                    { show: (employees.length ? true : false) });

                angular.forEach(employees,(employee) => {

                    //format
                    employee.HireDate = (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A');

                    //set profile image
                    employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);

                    switch (employee.IsPass) {
                        case true:
                            employee.resultClass = "pass";
                            employee.resultText = "Passed";
                            break;
                        case false:
                            employee.resultClass = "fail";
                            employee.resultText = "Failed";
                            break;
                        default:
                            employee.resultClass = "incomplete";
                            employee.resultText = "Incomplete";
                            break;
                    }

                    this.employees.push(employee);

                });

                this.calculateKpis();


            });
        }

        private calculateKpis = (): any => {

            angular.forEach(this.employees,(employee) => {

                angular.forEach(employee.EmployeeKpis, (kpi) => {
                    if (kpi.Kpi.KpiId == this.kpiId) {
                        if ((kpi.Kpi.Name == 'Professionalism' && kpi.Metric == null)
                            || (!kpi.IsPass && (kpi.Kpi.Name != 'Professionalism'))) {
                            this.employeesByKpi.push(employee);
                        }
                    }
                    
                 
                });

            });

        }

    }
    angular.module("app").controller('Training.SEDetailsCtrl', SEDetailsCtrl);
}