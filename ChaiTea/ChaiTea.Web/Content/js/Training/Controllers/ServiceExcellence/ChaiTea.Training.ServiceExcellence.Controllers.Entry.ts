﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import trainingServices = ChaiTea.Training.Services;

    export interface ISEFormOptions {
        name: string;
        displayOptions: {
            iconClass: string;
            descriptionText: string
        };
        formOptions: {
            passFail: boolean;
            comment: string;
            failWithOptions: any;
        };


    }

    interface IKpiComment {
        EmployeeKpiMasterId: number;
        Year: number;
        Month: number;
        Comment: string;
        EmployeeId: number;
        IsPass: boolean;
    }

    interface IKpiParams {
        year: number;
        month: number;
        employeeId: number;
    }

    interface ISEEntryScope extends ng.IScope {
        reportForm: ng.IFormController
    }

    enum FileType { Undefined, ProfilePicture };

    class SEEntryCtrl implements Common.Interfaces.INgController {
        reportCardItems: Array<any> = [];
        basePath;
        employeeId: number;

        employee: Interfaces.ISETrainingEmployee;
        complaints: serviceInterfaces.IComplaint;
        auditScores;
        safetyClaims = [];
        attendanceIssues = [];
        conductWarnings = [];
        ownershipWorkOrders = [];
        professionalismAudit = {};
        monthlyBonus: number = 0;
        yearlyBonus: number = 0;
        siteBonusForMonth: number = 0;

        reportForm: ng.IFormController;
        endDate: Date;
        dateRange: { year: number; month: number; endDate: Date} = {
            year: moment().year(),
            month: moment().month() + 1,
            endDate: new Date()
        }

        isEntryDisabled: boolean = false;

        showAuditScores: boolean = false;
        modalInstance;
        floorName: string;

        kpiItem;

        overallComment: string;
        shouldToggle = [];
        
        start;
        end;

        static $inject = [
            '$scope',
            '$uibModal',
            'userContext',
            'chaitea.training.services.serviceexcellence',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.printer',
            'chaitea.common.services.apibasesvc',
            'sitesettings',
            'chaitea.training.services.SEPBonusConfig',
            'chaitea.common.services.imagesvc'
        ];

        constructor(private $scope: ISEEntryScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private serviceExcellenceSvc: Services.IServiceExcellenceSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private dateFormatterSvc: Core.Services.IDateFormatterSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private printerSvc: ChaiTea.Core.Services.IPrinter,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private sitesettings: ISiteSettings,
            private sepBonusSvc: trainingServices.ISEPBonusConfigSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
            ) {
            this.basePath = sitesettings.basePath;
        }

        public initialize = (empId: number, sepMonthlyEditCuttoffDay: number = 5) =>{
            this.employeeId = empId;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.showAuditScores = false;
                this.isEntryDisabled = false;
                this.start = this.dateFormatterSvc.formatDateFull(moment(obj.endDate).startOf('month'));
                this.end = this.dateFormatterSvc.formatDateFull(moment(obj.endDate).endOf('day'));

                this.dateRange.month = moment(obj.endDate).month() + 1;
                this.dateRange.year = moment(obj.endDate).year();
                this.getEmployeeKpi();

                this.getEmployeeComplaints(empId);
                var start = new Date(this.dateRange.year, this.dateRange.month-1, 1);
                
                var end = new Date(this.dateRange.year, this.dateRange.month , 0);
                this.dateRange.endDate = end;

                this.getEmployeeAuditScores({
                    startDate: this.dateFormatterSvc.formatDateShort(start),
                    endDate: this.dateFormatterSvc.formatDateShort(end),
                    employeeId: empId
                });

                this.getSafetyClaims(empId);
                this.getAttendanceIssues(empId);
                this.getConductWarnings(empId);
                this.getProfessionalismAudit(empId);
                this.getEmployeeBonus(empId);
                this.getOwnership(empId);

                this.getOverallComment({
                    year: this.dateRange.year,
                    month: this.dateRange.month,
                    employeeId: empId
                });
            });

            

        }

        private getEmployeeComplaints = (empId: number) => {
            
            var params: any = {
                $orderby: 'CreateDate desc',
                $top: 20,
                $skip: 0,
                $filter: `FeedbackDate ge DateTime'${this.start }' and FeedbackDate lt DateTime'${this.end}' and AccountableEmployees/any(d:d eq ${empId})`
            };

            this.apiSvc.getByOdata(params, 'Complaints').then((res: serviceInterfaces.IComplaint) => {
                this.complaints = res;

            });

        }

         /**
        * @description Get Audit Scores for an emp,loyee.
        */
        private getEmployeeAuditScores = (params: qualityInterfaces.IMyAuditsParams) => {
            this.apiSvc.query(params, "Report/EmployeeAuditsAvgByAreaByEmployee/:startDate/:endDate/:employeeId").then((result) => {
                this.auditScores = result;
                angular.forEach(result,(item) => {
                    if (item.AverageScoringProfileScore) {
                        this.showAuditScores = true;
                    }
                });
            });
        }
        /**
      * @description Get yearly and monthly bonus for an emp,loyee.
      */
        private getEmployeeBonus = (empId: number) => {
           this.siteBonusForMonth = 0;
           this.monthlyBonus = 0;
           
           this.sepBonusSvc.getSiteBonus({ month: this.dateRange.month, year: this.dateRange.year }).then((data): any => {
               if (data.length) {
                   this.siteBonusForMonth = data[0].BonusAmount ? data[0].BonusAmount : 0;
               }
           });

           var params = {
               employeeId: empId,
               year: this.dateRange.year,
               month: this.dateRange.month
           }

           this.sepBonusSvc.getBonusOfEmployee(params).then((result) => {
               if (result)
                   this.monthlyBonus = result.BonusAmount ? result.BonusAmount : 0;
           });
           
           var param = {
               employeeId: empId,
               year: this.dateRange.year,
               month: this.dateRange.month
           }
           this.sepBonusSvc.getYearlyBonusOfEmployee(param).then((result) => {
               this.yearlyBonus = result.YearlyBonus;
           });
        }
        /**
      * @description Get safety claims for an emp,loyee.
      */
        private getSafetyClaims = (empId: number) => {

            var params: any = {
                $orderby: 'DateOfIncident desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfIncident ge DateTime'${this.start}' and DateOfIncident lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Personnel/Safety").then((result) => {
                this.safetyClaims = result;
            });
        }

         /**
      * @description Get Ownership for an employee.
      */
        private getOwnership = (empId: number) => {

            var params: any = {
               
                $filter: `CreateDate ge DateTime'${this.start }' and CreateDate lt DateTime'${this.end}' and EmployeeId eq ${empId} and Status eq '1'`
            };
            this.apiSvc.getByOdata(params, "Ownership").then((result) => {
                this.ownershipWorkOrders = result;
            });
        }

        /**
       * @description Get attendance issues for an emp,loyee.
       */
        private getAttendanceIssues = (empId: number) => {

            var params: any = {
                $orderby: 'DateOfOccurrence desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfOccurrence ge DateTime'${this.start }' and DateOfOccurrence lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Issue").then((result) => {
                this.attendanceIssues = result;
            });
        }

        /**
       * @description Get conduct warnings for an emp,loyee.
       */
        private getConductWarnings = (empId: number) => {

            var params: any = {
                $orderby: 'DateOfOccurrence desc',
                $top: 20,
                $skip: 0,
                $filter: `DateOfOccurrence ge DateTime'${this.start }' and DateOfOccurrence lt DateTime'${this.end}' and OrgUserId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "Conduct").then((result) => {
                this.conductWarnings = result;
            });
        }

        /**
       * @description Get safety claims for an emp,loyee.
       */
        private getProfessionalismAudit = (empId: number) => {

            var params: any = {
                $orderby: 'UpdateDate desc',
                $top: 20,
                $skip: 0,
                $filter: `CreateDate ge DateTime'${this.start}' and CreateDate lt DateTime'${this.end}' and EmployeeId eq ${empId}`
            };
            this.apiSvc.getByOdata(params, "EmployeeAudit").then((result) => {
                this.professionalismAudit = result[0];
            });
        }

         /**
        * @description Get the overall comment for a particular month.
        */
        private getOverallComment = (params: IKpiParams) => {
            this.serviceExcellenceSvc.getKpiComment(params).then((result) => {
                this.kpiItem = result;
            });
        }

         /**
        * @description Update Comment.
        */
        public updateOverallComment = () => {
            this.serviceExcellenceSvc.updateKpiComment(this.kpiItem.EmployeeKpiMasterId, this.kpiItem).then(this.onSuccess);
        }

        private getEmployeeKpi = () => {
            if (!this.employeeId) return false;

            this.serviceExcellenceSvc.getEmployeeKpi(this.dateRange.year, this.dateRange.month, this.employeeId).then((employee) => {
                this.processEmployeeDataIntoView(employee);
            });
        }

        public printScoreCard = ($event) => {
            var date = moment(this.dateRange.month.toString(), this.dateRange.year.toString());
            var employeeCopy = <any>angular.copy(this.employee);

            //TODO TEMP hide attendance and quality audits
            employeeCopy.EmployeeKpis[3].Kpi.Summary = "";
            employeeCopy.EmployeeKpis[5].Kpi.Summary = "";
            employeeCopy.evaluationStartDate = date.startOf('month').toDate();
            employeeCopy.evaluationEndDate = date.endOf('month').toDate();
            employeeCopy.site = this.userContext.Site;
            employeeCopy.bonusAmount = " ";

            var printObj = {
                scoreCardItems: [employeeCopy]
            }

            this.printerSvc.print(this.basePath + 'Content/js/Training/Templates/print.sep.scorecard.html', printObj);
        }


        private processEmployeeDataIntoView = (employee) => {
            this.employee = employee;
            this.isEntryDisabled = employee.IsClosed;
            this.hasData();
            this.employee.HireDate = (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A');

            //set false kpi blocks to open
            _.forEach(this.employee.EmployeeKpis,(kpi) => {
                kpi["showToggleBlock"] = false
            });
           
            // image
            this.employee.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
        }    

        /**
        * @description Success callback.
        */
        onSuccess = (response: any): void=> {
            bootbox.alert('Comment was saved successfully!');
        }

        //#region form stuff
        public toggleCheckBox = (kpi, item) => {
            if (this.isEntryDisabled) return false;

            if (_.contains(kpi.CheckBoxSelections, item.Key)) {
                kpi.CheckBoxSelections.slice(item.Key, 1);
                _.remove(kpi.CheckBoxSelections,(num) => (num == item.Key))
            } else {
                kpi.CheckBoxSelections.push(item.Key);
            }
        }

        public saveForm = ($event) => {
            $event.preventDefault();
        }

        public cancelForm = ($event, redirectUrl) => {
            $event.preventDefault();

            if (this.reportForm.$pristine) {
                window.location.href = redirectUrl;
            }

            bootbox.confirm("Are you sure you want to cancel? <p>All changes will be lost.</p>",(cb) => {
                if (cb) {
                    window.location.href = redirectUrl;
                }
            });
        }

        /*
         * @description used for validation of many checkboxes
         */
        public someSelected = (object) => {
            return Object.keys(object).some(function (key) {
                return object[key].Selected;
            });
        }
        //#endregion

        public hasData = () => {
            var setVal: boolean = false;
            this.shouldToggle.splice(0, this.shouldToggle.length);
            angular.forEach(this.employee.EmployeeKpis,(kpiItem) => {
                setVal = false;
                angular.forEach(kpiItem.MetaData,(metadataItem) => {
                    if (metadataItem.Label == 'Complaints' || metadataItem.Label == 'Score') {
                        if (metadataItem.Value > 0 && metadataItem.Value != 'N/A')
                            setVal = true;
                    } 
                    else if ((kpiItem.Kpi.Name == 'Attendance' && metadataItem.Label == 'Occurrences') || (kpiItem.Kpi.Name == 'Safety' && metadataItem.Label == 'Claims')
                        || (kpiItem.Kpi.Name == 'Conduct' && metadataItem.Label == 'Warnings') || (kpiItem.Kpi.Name == 'Ownership' && metadataItem.Label == 'Created')) {
                        if (metadataItem.Value != '0')
                            setVal = true;
                    }
                });

                if (kpiItem.Kpi.Name == 'Professionalism' && kpiItem.Kpi.Summary != "No audits done")
                    setVal = true;

                this.shouldToggle.push(setVal);
            });

            console.log(this.shouldToggle);
        }

        public onFailClick = (event, employeeId: number, item: any) => { 
            event.stopPropagation(); 

            switch (item.Kpi.Name) {
                case 'Conduct':
                    this.loadConductModal(employeeId);
                    break;
                case 'Safety':
                    this.loadSaftyModal(employeeId);
                    break;
                case 'Attendance':
                    this.loadAttendenceModal(employeeId);
                    break;
                case 'Professionalism':
                    this.loadAuditModal(employeeId);
                    break;
                case 'Complaints':
                    window.location.href = this.sitesettings.basePath + 'Quality/Complaint/Entry';
                    break;
                case 'Ownership':
                    this.loadOwnershipModal(employeeId);
                    break;
                default:
                    return;
            }

        }

        public loadProfessionalismAuditDetailsModal = (professionalismAuditId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.ProfessionalismAuditsDetailsModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    professionalismAuditId: () => { return professionalismAuditId; }
                }
            });

        }

        public loadOwnershipModal = (employeeId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.OwnershipModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return employeeId; }
                }
            });

        }

        public loadSaftyModal = (employeeId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.SafetyModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return employeeId; }
                }
            });

        }

        public loadSafetyIssueDetailsModal = (safetyId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.SafetyIssueDetailsModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    safetyId: () => { return safetyId; }
                }
            });

        }

        public loadComplaintModal = (employeeId: number, complaint: serviceInterfaces.IComplaint, $e) => {

            console.log('load modal');

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.ComplaintModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    complaint: () => { return complaint; },
                    employeeId: () => { return employeeId; }
                }
            });

        }

        public loadConductModal = (employeeId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.ConductModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return employeeId; }
                }
            });

        }

        public loadConductWarningDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.ConductWarningDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    conductId: () => { return id; }
                }
            });

        }

        public loadAttendenceModal = (employeeId: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.AttendanceModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return employeeId; }
                }
            });

        }

        public loadAttendanceIssueDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.AttendanceIssueDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    attendanceIssueId: () => { return id; }
                }
            });

        }

        public loadAuditModal = (employeeId: number) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.AuditModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    employeeId: () => { return employeeId; }
                }
            })
        }

        public loadOwnershipDetailsModal = (id: number) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Training.OwnershipDetailsModal as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    ownershipId: () => { return id; }
                }
            });

        }


    }
    angular.module("app").controller('Training.SEEntryCtrl', SEEntryCtrl);


    

    //
    //
    //TODO: move to common modal folder
    //
    //

    class ProfessionalismAuditsDetailsModalCtrl {

        basePath: string;

        audit: any = {};
        auditId: number = 0;

        employeeWithImage = {};

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        downloadUrl: string = '';

        modalTitle: string = 'Professionalism Audit Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'professionalismAuditId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private title: string,
            private professionalismAuditId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.professionalismAudit.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getAudit(professionalismAuditId);

        }

        private getAudit = (auditId) => {
            this.apiSvc.getById(auditId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.EmployeeAudit]).then((audit) => {
                var audit = audit;
                audit.items = {};
                audit.items.appearance = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 1 } });
                audit.items.attitude = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 2 } });
                audit.items.equipment = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 3 } });
                audit.items.responsiveness = !_.any(audit.CheckBoxOptions, { KpiOption: { KpiOptionId: 4 } });

                if (_.any(audit.items,(item) => (item == false))) {
                    audit.IsPass = false;
                }

                this.audit = audit;
            })
            return this.audit;
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }
       
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Training.ProfessionalismAuditsDetailsModalCtrl', ProfessionalismAuditsDetailsModalCtrl);


    class OwnershipModalCtrl {
        ownershipId: number;
        modalInstance;

        modalTitle: string = 'Ownership';
        modalBodyTemplate: string = '';

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        buildings = [];
        floors = [];

        employeesWithAttachment = [];
        employees = [];
        employee = {};
        OwnershipTypes = [];
        maxDate: Date = null;

        AttachmentName = '';

        //set defaults
        Ownership = <serviceInterfaces.IOwnership>{
            OwnershipId: 0,
            Description: '',
            Classifications: [],
            BuildingId: 0,
            FloorId: 0,
            EmployeeId: 0,
            OwnershipAttachments: [],
            ReportTypeId: null,
            CustomerRepresentatives: [],
            BuildingName: '',
            FloorName: '',
            ReportTypeName: '',
            RejectionTypeId: null,
            RejectionTypeName: '',
            Status: null,
            IsCritical: null,
            CreateDate: new Date(),
            IsExempt: false
        }

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$window',
            '$interval',
            '$timeout',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.common.services.utilssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'employeeId'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private employeeId: number) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.Ownership.EmployeeId = this.employeeId;

            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.ownership.entry.tmpl.html';

            this.getBuildings();
        }

        getBuildings = (): void => {
            this.apiSvc.getLookupList('LookupList/Buildings').then((data) => {
                this.buildings = data.Options;
            });
        }
        /** 
        * @description Get floors based on buildingId. 
        * @param {number} buildingId
        */
        onBuildingChange = (buildingId: number): void => {
            if (buildingId > 0) {
                this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'ParentId eq ' + buildingId }, 'Floor').then((res) => {
                    var floors = _.map(res,(floor) => {
                        return { Key: floor['Id'], Value: floor['Name'] }
                    });
                    this.floors = floors;
                    if (this.blockUI) this.blockUI.stop();
                });

            } else {
                this.floors = [];
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            bootbox.alert('Ownership was saved successfully!');
            this.close();
            this.$window.location.reload();
        }

        /**
        * @description Dismiss the modal instance.
        */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /** @description Create/update a new Ownership. */

        save = ($event): void => {
            if ($event) $event.preventDefault();           
                // POST
                this.apiSvc.save(this.Ownership, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Ownership]).then(this.onSuccess, this.onFailure);
        }

        addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.IOwnershipAttachment>{
                UniqueId: null,
                AttachmentType: 'Image',
                OwnershipAttachmentType: commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE[commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE.Misc],
                OwnershipAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.AttachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, this.awsSvc.getFolderByAttachmentType(tempAttachment.AttachmentType), commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.Ownership.OwnershipAttachments.length) {
                            this.Ownership.OwnershipAttachments[0] = tempAttachment;
                        } else {
                            this.Ownership.OwnershipAttachments.push(tempAttachment);
                        }

                    },(error: any) => {
                            //file.class = 'error';
                        },(progress: any) => {
                            //file.progress = progress;
                        });

                });

            }
        }

    }
    angular.module("app").controller('Training.OwnershipModalCtrl', OwnershipModalCtrl);

    class OwnershipDetailsModal {

        basePath: string = '';

        ownership = {};
       

       
        downloadUrl: string = '';

        modalTitle: string = 'Report It Created';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'ownershipId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private title: string,
            private ownershipId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.ownership.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getOwnershipById(ownershipId);

        }

        getOwnershipById = (ownershipId: number): void=> {
            if (ownershipId > 0) {
                this.apiSvc.getById(ownershipId, "Ownership").then((result) => {
                    this.ownership = result;
                    
                }, this.onFailure);
            }
        }



        onFailure = (response: any): void => {
            this.$log.error(response);
        }

       
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Training.OwnershipDetailsModal', OwnershipDetailsModal);

    class SafetyModalCtrl {

        basePath: string;

        modalTitle: string = 'Safety';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        //set defaults
        safety = <serviceInterfaces.ISafetyItem>{
            SafetyId: 0,
            DateOfIncident: <Date> null,
            Detail: '',
            IncidentTypes: [],
            EmployeeId: 0,
            SafetyAttachments: []
        }
        incidentTypes: Array<serviceInterfaces.IIncidentType> = [];
        maxDate: Date = null;
        attachmentName: string;

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'title',
            'employeeId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private employeeId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.entry.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';

            this.maxDate = new Date();
            this.safety.EmployeeId = employeeId;
            this.safety.DateOfIncident = new Date();
        }

        /** @description checks to see if IncidentType object is selected when initializing. **/
        private isIncidentSelected = (id: number): boolean => {
            var foundIncident: boolean = false;
            angular.forEach(this.safety.IncidentTypes, (incidentType) => {
                if (incidentType == id) {
                    foundIncident = true;
                }
            });
            return foundIncident;
        }

        /** @description toggles whether IncidentType object is selected when that object is clicked. **/
        private toggleIncidentType = (id: number): void => {
            var index: number = 0;
            var foundIncident: boolean = false;

            angular.forEach(this.safety.IncidentTypes, (incidentType) => {
                if (incidentType == id) {
                    this.safety.IncidentTypes.splice(index, 1);
                    foundIncident = true;
                }
                index++;
            });
            if (!foundIncident) {
                this.safety.IncidentTypes.push(id);
            }
        }
        /**
         * CONDUCT ATTACHMENT
         * @description Handler when a file is selected.
         */
        addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.ISafetyAttachment>{
                UniqueId: null,
                AttachmentType: 'File',
                SafetyAttachmentType: commonInterfaces.CONDUCT_ATTACHMENT_TYPE[commonInterfaces.CONDUCT_ATTACHMENT_TYPE.Misc],
                SafetyAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.attachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.safety.SafetyAttachments.length) {
                            this.safety.SafetyAttachments[0] = tempAttachment;
                        } else {
                            this.safety.SafetyAttachments.push(tempAttachment);
                        }

                    },(error: any) => {
                            //throw error
                        },(progress: any) => {
                            //update progress bar
                        });

                });

            }
        }
        
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new safety object
         */
        save = ($e: any): void=> {

            if (this.safety.IncidentTypes.length == 0) {
                return bootbox.alert('Please select appropriate Incident Type');
            }

            this.apiSvc.save(this.safety, "Personnel/Safety").then((res) => {
                this.$uibModalInstance.dismiss('cancel');
                this.$window.location.reload();
            },(err) => { this.$log.error(err) });

        };

    }
    angular.module("app").controller('Training.SafetyModalCtrl', SafetyModalCtrl);

    class SafetyIssueDetailsModalCtrl {

        basePath: string;

        safety = {};

        employeeWithImage = {};
        //incidentType = {};
        downloadUrl: string = '';

        modalTitle: string = 'Safety Issue Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'safetyId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private title: string,
            private safetyId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getSafetyById(safetyId);

        }

        getSafetyById = (safetyId: number): void=> {
            if (safetyId > 0) {
                this.apiSvc.getById(safetyId, "Personnel/Safety").then((result) => {
                    this.safety = result;
                    this.downloadUrl = (result.SafetyAttachments.length ? this.awsSvc.getUrlByAttachment(result.SafetyAttachments[0].UniqueId, result.SafetyAttachments[0].AttachmentType) : '');

                }, this.onFailure);
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

       
    }
    angular.module("app").controller('Training.SafetyIssueDetailsModalCtrl', SafetyIssueDetailsModalCtrl);


    class ComplaintModalCtrl {

        basePath: string;
        modalTitle: string = 'Complaints';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        floorName: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'title',
            'complaint',
            'employeeId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private title: string,
            private complaint: serviceInterfaces.IComplaint,
            private employeeId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Quality/quality.complaint.entry.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Quality/quality.complaint.footer.tmpl.html';

            if (complaint.FloorId > 0) {
                this.apiSvc.getById(complaint.FloorId, 'Floor').then((result) => {
                    this.floorName = result.Name;
                });
            }

        }
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


        /**
         * @description Dismiss the modal instance.
         */
        public goToComplaintDetail = ($e): void => {
            this.$uibModalInstance.dismiss('cancel');
            //Quality / Complaint / EntryView / 1012
            window.location.replace(this.basePath + 'Quality/Complaint/EntryView/' + this.complaint.ComplaintId);
        }
    }
    angular.module("app").controller('Training.ComplaintModalCtrl', ComplaintModalCtrl);

    class ConductModalCtrl {

        basePath: string;

        modalTitle: string = 'Conduct';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        attachmentName: string = '';
        maxDate: Date = null;
        conductTypes: Array<serviceInterfaces.IConductType> = [];
        conduct = <serviceInterfaces.IConduct>{
            ConductId: 0,
            DateOfOccurrence: <Date> null,
            Reason: '',
            ConductTypes: [],
            OrgUserId: 0,
            ConductAttachments: []
        }

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'title',
            'employeeId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private employeeId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.conduct.entry.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.conduct.footer.tmpl.html';
            this.conduct.OrgUserId = employeeId;

            this.getConductTypes();
            this.maxDate = new Date();
            this.conduct.DateOfOccurrence = new Date();
        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Dismiss the modal instance.
         */
        public save = ($e): void=> {

            if (this.conduct.ConductTypes.length == 0) {
                return bootbox.alert('Please select appropriate Conduct Type');
            }

            this.apiSvc.save(this.conduct, "Conduct").then((res) => {
                this.$uibModalInstance.dismiss('cancel');
                bootbox.alert('Conduct was saved successfully!');
                this.$window.location.reload();
            },(err) => { this.$log.error(err) });

        };

        /**
         * @description Dismiss the modal instance.
         */
        public attachDocument = ($e): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Dismiss the modal instance.
         */
        public getConductTypes = (): void => {
            this.serviceLookupListSvc.getConductTypes().then((result: Array<serviceInterfaces.IConductType>) => {
                angular.forEach(result,(item) => {
                    item.IsSelected = false;
                });
                this.conductTypes = result;
            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        public setConductType = (id: number): void => {

            var conductType: serviceInterfaces.IConductType = _.find(this.conductTypes, { 'ConductTypeId': id });

            if (!conductType) return;

            if (!_.any(this.conduct.ConductTypes, { 'ConductTypeId': id })) {
                this.conduct.ConductTypes.push(conductType);
            } else {
                this.conduct.ConductTypes.splice(_.findIndex(this.conduct.ConductTypes, { 'ConductTypeId': id }), 1);
            }

        }

        /**
         * @description Dismiss the modal instance.
         */
        public addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.IConductAttachment>{
                UniqueId: null,
                AttachmentType: 'File',
                ConductAttachmentType: commonInterfaces.CONDUCT_ATTACHMENT_TYPE[commonInterfaces.CONDUCT_ATTACHMENT_TYPE.Misc],
                ConductAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.attachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.conduct.ConductAttachments.length) {
                            this.conduct.ConductAttachments[0] = tempAttachment;
                        } else {
                            this.conduct.ConductAttachments.push(tempAttachment);
                        }

                    },(error: any) => {
                            //throw error
                        },(progress: any) => {
                            //update progress bar
                        });

                });

            }
        }

    }
    angular.module("app").controller('Training.ConductModalCtrl', ConductModalCtrl);

    class ConductWarningDetailsModal {

        basePath: string;

        conduct = {};

        employeeWithImage = {};
        downloadUrl: string = '';

        modalTitle: string = 'Conduct Warning Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'conductId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private title: string,
            private conductId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.conduct.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getConductById(conductId);

        }

        getConductById = (conductId: number): void=> {
            if (conductId > 0) {
                this.apiSvc.getById(conductId, "Conduct").then((result) => {
                    this.conduct = result;
                    this.downloadUrl = (result.ConductAttachments.length ? this.awsSvc.getUrlByAttachment(result.SafetyAttachments[0].UniqueId, result.SafetyAttachments[0].AttachmentType) : '');

                }, this.onFailure);
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }
       
        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Training.ConductWarningDetailsModal', ConductWarningDetailsModal);

    class AttendanceModalCtrl {

        basePath: string;
        modalTitle: string = 'Attendance';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        maxDate: Date = null;
        attendanceIssueTypes: Array<serviceInterfaces.IAttendanceType> = [];
        attendance = <serviceInterfaces.IAttendanceItem>{
            IssueId: 0,
            DateOfOccurrence: <Date> null,
            DateTimeOccurrence: <Date> null,
            Comment: '',
            IssueTypeId: 0,
            IssueType: '',
            ReasonId: null,
            ReasonName: null,
            OrgUserId: 0
        }
        isLate: boolean = false;
        time: Date = null;
        issueId: number = 0;
        issueFlag: number = 0;
        selected: string = "";

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'title',
            'employeeId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private title: string,
            private employeeId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.attendance.entry.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.attendance.footer.tmpl.html';

            this.attendance.OrgUserId = employeeId;
            this.getAttendanceIssueTypes();
            this.isLate = true;
            this.time = new Date();
            this.maxDate = new Date();
            this.attendance.DateOfOccurrence = new Date();
        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Dismiss the modal instance.
         */
        public save = ($e): void=> {
            this.apiSvc.save(this.attendance, "Issue").then((res) => {
                this.$uibModalInstance.dismiss('cancel');
                bootbox.alert('Attendance was saved successfully!');
                this.$window.location.reload();
            },(err) => { this.$log.error(err) });
        };

        /**
         * @description Dismiss the modal instance.
         */
        getAttendanceIssueTypes = (): void => {
            this.serviceLookupListSvc.getAttendanceIssueTypes().then((result) => {
                this.attendanceIssueTypes = result;
                this.setIssueType(0);
            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        setIssueType = (id: number): void => {
            this.attendance.IssueTypeId = this.attendanceIssueTypes[id].IssueTypeId;
            this.attendance.IssueType = this.attendanceIssueTypes[id].Name;
            this.issueFlag = id;
            //this.attendance.ReasonId = 0;
            if (this.issueId == 0) {
                //this.attendance.DateOfOccurrence = new Date();
            }

            if (this.attendance.IssueType == "Absent") {
                this.time = null;
            } else {
                this.attendance.ReasonId = null;
                this.attendance.ReasonName = null;
            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        setReason = (id: number, issueId: number): void => {
            this.attendance.ReasonId = this.attendanceIssueTypes[issueId].Reasons[id].ReasonId;
            this.attendance.ReasonName = this.attendanceIssueTypes[issueId].Reasons[id].Name;
            this.selected = this.attendance.ReasonName;
        }

    }
    angular.module("app").controller('Training.AttendanceModalCtrl', AttendanceModalCtrl);

    class AttendanceIssueDetailsModal {

        basePath: string;

        attendance = {};

        employeeWithImage = {};
        downloadUrl: string = '';

        modalTitle: string = 'Attendance Issue Details';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'title',
            'attendanceIssueId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private title: string,
            private attendanceIssueId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.attendance.entryView.tmpl.html';
            //this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.footer.tmpl.html';
            this.getAttendanceById(attendanceIssueId);

        }

        getAttendanceById = (attendanceIssueId: number): void=> {
            if (attendanceIssueId > 0) {
                this.apiSvc.getById(attendanceIssueId, "Issue").then((result) => {
                    this.attendance = result;
                }, this.onFailure);
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        close = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };


    }
    angular.module("app").controller('Training.AttendanceIssueDetailsModal', AttendanceIssueDetailsModal);

    class AuditModalCtrl {

        basePath: string;
        modalTitle: string = 'Professionalism Audit';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        auditItemTemplatePath: string = '';

        auditId: number = 0;

        audit: Personnel.Interfaces.IEmployeeAudit = <any>{
            CheckBoxSelections: []
        };

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'title',
            'employeeId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private title: string,
            private employeeId: number) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.entry.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.entry.footer.tmpl.html';
            this.auditItemTemplatePath = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.auditItem.tmpl.html';

            this.employeeId = employeeId;

            this.apiSvc.get("LookupList/EmployeeAuditOptions").then((res) => {

                this.kpiOptions.appearance = _.where(res, { KpiOption: { Name: 'Appearance' } });
                this.kpiOptions.appearance["Description"] = "Employee wears an SBM tidy uniform and has good hygiene.";
                this.kpiOptions.appearance["Name"] = "Appearance";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.appearance, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.appearance["IsPass"] = false;
                    }
                }

                this.kpiOptions.attitude = _.where(res, { KpiOption: { Name: 'Attitude' } });
                this.kpiOptions.attitude["Description"] = "Employee displays a positive approachable demeanor, shows respect and is attentive.";
                this.kpiOptions.attitude["Name"] = "Attitude";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.attitude, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.attitude["IsPass"] = false;
                    }
                }

                this.kpiOptions.responsiveness = _.where(res, { KpiOption: { Name: 'Responsiveness' } });
                this.kpiOptions.responsiveness["Description"] = "Employee displays a quick response time and shows initiative.";
                this.kpiOptions.responsiveness["Name"] = "Responsiveness";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.responsiveness, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.responsiveness["IsPass"] = false;
                    }
                }

                this.kpiOptions.equipment = _.where(res, { KpiOption: { Name: 'Closet & Equipment' } });
                this.kpiOptions.equipment["Description"] = "Employee maintains proper care of equipment and a tidy closet without any hazardous conditions.";
                this.kpiOptions.equipment["Name"] = "Equipment & Closets";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.equipment, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.equipment["IsPass"] = false;
                    }
                }

                this.$log.log('kpiOptions', this.kpiOptions);
            });

        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save
         */
        public save = ($e): void=> {
            this.audit.EmployeeId = this.employeeId;

            var self = this;

            //TEMPORARY need to change KpiSubOptions to Options
            angular.forEach(this.kpiOptions, (option: any) => {
                if (option.IsPass === false) {
                    this.audit.CheckBoxSelections.push(option[0].KpiSubOptionId);
                }
            });

            this.apiSvc.save(this.audit, "EmployeeAudit").then((res) => {
                this.$uibModalInstance.dismiss('cancel');
                bootbox.alert('Professionalism Audit was saved successfully!');
                this.$window.location.reload();
            },(err) => { this.$log.error(err) });

        };



        public toggleCheckBox = (kpi, value) => {
            if (_.contains(kpi.CheckBoxSelections, value)) {
                kpi.CheckBoxSelections.slice(value, 1);
                _.remove(kpi.CheckBoxSelections,(num) => (num == value))
            } else {
                kpi.CheckBoxSelections.push(value);
            }

        }

    }
    angular.module("app").controller('Training.AuditModalCtrl', AuditModalCtrl);

}