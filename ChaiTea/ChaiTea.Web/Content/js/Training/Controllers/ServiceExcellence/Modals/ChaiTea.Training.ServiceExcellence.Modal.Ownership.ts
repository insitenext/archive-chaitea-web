﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;

    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
        IsKpiExempted: boolean;
    }

    class OwnershipModalCtrl {

        ownershipId: number;
        modalInstance;

        modalTitle: string = 'Report It';
        modalBodyTemplate: string = '';

        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        basePath: string;

        buildings = [];
        floors = [];

        AttachmentName = '';

        //set defaults
        Ownership = <serviceInterfaces.IOwnership>{
            OwnershipId: 0,
            Description: '',
            Classifications: [],
            BuildingId: null,
            FloorId: null,
            EmployeeId: 0,
            OwnershipAttachments: [],
            ReportTypeId: null,
            CustomerRepresentatives: [],
            BuildingName: '',
            FloorName: '',
            ReportTypeName: '',
            RejectionTypeId: null,
            RejectionTypeName: '',
            Status: null,
            IsCritical: null,
            CreateDate: new Date(),
            IsExempt: false
        }

        toUpdate: boolean = false;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'blockUI',
            '$log',
            '$window',
            '$interval',
            '$timeout',
            '$uibModalInstance',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.common.services.utilssvc',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'employee'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private employee: IEmployeeAdd) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.Ownership.EmployeeId = this.employee.EmployeeId;

            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.ownership.entry.tmpl.html';

            this.getBuildings();
        }

        public getBuildings = (): void => {
            this.apiSvc.getLookupList('LookupList/Buildings').then((data) => {
                this.buildings = data.Options;
            }, this.onFailure);
        }
        /** 
        * @description Get floors based on buildingId. 
        * @param {number} buildingId
        */
        public onBuildingChange = (buildingId: number): void => {
            if (buildingId > 0) {
                this.apiSvc.getByOdata({ $orderby: 'Name asc', $filter: 'ParentId eq ' + buildingId }, 'Floor').then((res) => {
                    var floors = _.map(res, (floor) => {
                        return { Key: floor['Id'], Value: floor['Name'] }
                    });
                    this.floors = floors;
                    if (this.blockUI) this.blockUI.stop();
                }, this.onFailure);

            } else {
                this.floors = [];
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = ($e): void => {
            this.$uibModalInstance.close(this.toUpdate);
        };

        /** @description Create/update a new Ownership. */

        save = ($event): void => {
            if ($event) $event.preventDefault();
            if (this.$scope.ownershipForm.$valid) {
                // POST
                this.Ownership.IsExempt = this.employee.IsKpiExempted;
                this.apiSvc.save(this.Ownership, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Ownership]).then((res) => {
                    this.toUpdate = false;
                    this.$uibModalInstance.close(this.toUpdate);
                    bootbox.alert('Ownership was saved successfully!');
                }, this.onFailure);
            }
        }

        public addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.IOwnershipAttachment>{
                UniqueId: null,
                AttachmentType: 'Image',
                OwnershipAttachmentType: commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE[commonInterfaces.OWNERSHIP_ATTACHMENT_TYPE.Misc],
                OwnershipAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.AttachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, this.awsSvc.getFolderByAttachmentType(tempAttachment.AttachmentType), commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.Ownership.OwnershipAttachments.length) {
                            this.Ownership.OwnershipAttachments[0] = tempAttachment;
                        } else {
                            this.Ownership.OwnershipAttachments.push(tempAttachment);
                        }

                    }, (error: any) => {
                    }, (progress: any) => {
                    });

                });

            }
        }

    }
    angular.module("app").controller('Training.OwnershipModalCtrl', OwnershipModalCtrl);

}