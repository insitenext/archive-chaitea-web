﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;

    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
    }

    class AuditModalCtrl {

        basePath: string;
        modalTitle: string = 'Professionalism Audit';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        auditItemTemplatePath: string = '';

        auditId: number = 0;
        isEventBased: boolean = false;
        toUpdate: boolean = false;

        audit: Personnel.Interfaces.IEmployeeAudit = <any>{
            CheckBoxSelections: []
        };

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'title',
            'employee'];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private title: string,
            private employee: IEmployeeAdd) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.auditItemTemplatePath = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.audit.auditItem.tmpl.html';

            this.setEventBased(0);
            this.apiSvc.get("LookupList/EmployeeAuditOptions").then((res) => {

                this.kpiOptions.appearance = _.where(res, { KpiOption: { Name: 'Appearance' } });
                this.kpiOptions.appearance["Description"] = "Employee wears an SBM tidy uniform and has good hygiene.";
                this.kpiOptions.appearance["Name"] = "Appearance";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.appearance, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.appearance["IsPass"] = false;
                    }
                }

                this.kpiOptions.attitude = _.where(res, { KpiOption: { Name: 'Attitude' } });
                this.kpiOptions.attitude["Description"] = "Employee displays a positive approachable demeanor, shows respect and is attentive.";
                this.kpiOptions.attitude["Name"] = "Attitude";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.attitude, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.attitude["IsPass"] = false;
                    }
                }

                this.kpiOptions.responsiveness = _.where(res, { KpiOption: { Name: 'Responsiveness' } });
                this.kpiOptions.responsiveness["Description"] = "Employee displays a quick response time and shows initiative.";
                this.kpiOptions.responsiveness["Name"] = "Responsiveness";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.responsiveness, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.responsiveness["IsPass"] = false;
                    }
                }

                this.kpiOptions.equipment = _.where(res, { KpiOption: { Name: 'Closet & Equipment' } });
                this.kpiOptions.equipment["Description"] = "Employee maintains proper care of equipment and a tidy closet without any hazardous conditions.";
                this.kpiOptions.equipment["Name"] = "Equipment & Closets";
                if (this.audit) {
                    if (_.contains(this.kpiOptions.equipment, this.audit.CheckBoxSelections)) {
                        this.kpiOptions.equipment["IsPass"] = false;
                    }
                }

                this.$log.log('kpiOptions', this.kpiOptions);
            }, this.onFailure);

        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Save
         */
        public save = ($e): void => {
            if (this.$scope.auditForm.$valid) {
                this.audit.EmployeeId = this.employee.EmployeeId;

                var self = this;

                //TEMPORARY need to change KpiSubOptions to Options
                angular.forEach(this.kpiOptions, (option: any) => {
                    if (option.IsPass === false) {
                        this.audit.CheckBoxSelections.push(option[0].KpiSubOptionId);
                    }
                });

                this.apiSvc.save(this.audit, "EmployeeAudit").then((res) => {
                    this.toUpdate = true;
                    this.$uibModalInstance.close(this.toUpdate);
                    bootbox.alert('Professionalism Audit was saved successfully!');
                }, this.onFailure);
            }
        }


        public setEventBased = (eventBased: number) => {
            this.audit.IsEventBasedAudit = eventBased == 1 ? true : false;
        }

        public toggleCheckBox = (kpi, value) => {
            if (_.contains(kpi.CheckBoxSelections, value)) {
                kpi.CheckBoxSelections.slice(value, 1);
                _.remove(kpi.CheckBoxSelections, (num) => (num == value))
            } else {
                kpi.CheckBoxSelections.push(value);
            }

        }

    }
    angular.module("app").controller('Training.AuditModalCtrl', AuditModalCtrl);

}