﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;

    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
        IsKpiExempted: boolean;
    }

    interface IAttendanceItem {
        IssueId: number;
        DateOfOccurrence: Date;
        Comment: string;
        IssueTypeId: number;
        IssueType: string;
        ReasonId: number;
        ReasonName: string;
        OrgUserId: number;
        IsExempt: boolean;
    }

    class AttendanceModalCtrl {

        basePath: string;
        modalTitle: string = 'NEW ATTENDANCE ENTRY';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        toUpdate: boolean = false;

        maxDate: Date = null;
        attendanceIssueTypes: Array<serviceInterfaces.IAttendanceType> = [];
        attendance: IAttendanceItem = {
            IssueId: 0,
            DateOfOccurrence: <Date>null,
            Comment: '',
            IssueTypeId: 0,
            IssueType: '',
            ReasonId: null,
            ReasonName: null,
            OrgUserId: 0,
            IsExempt: null
        }
        isLate: boolean = false;
        time: Date = null;
        issueId: number = 0;
        issueFlag: number = 0;
        selected: string = "";

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'title',
            'employee'];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private title: string,
            private employee: IEmployeeAdd) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.attendance.OrgUserId = employee.EmployeeId;
            this.getAttendanceIssueTypes();
            this.isLate = true;
            this.time = new Date();
            this.maxDate = new Date();
            this.attendance.DateOfOccurrence = new Date();
        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void => {
            this.$uibModalInstance.close(this.toUpdate);
        };

        /**
         * @description Dismiss the modal instance.
         */
        public save = ($e): void => {
            if (this.$scope.attendanceForm.$valid) {
                this.attendance.DateOfOccurrence = this.getDateTime();
                this.attendance.IsExempt = this.employee.IsKpiExempted; 
                this.apiSvc.save(this.attendance, "Issue").then((res) => {
                    this.toUpdate = true;
                    this.$uibModalInstance.close(this.toUpdate);
                    var forHistoricalPurposes = '';
                    if (moment(this.attendance.DateOfOccurrence) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                        forHistoricalPurposes = '  This attendance is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
                    }
                    bootbox.alert('Attendance was saved successfully!' + forHistoricalPurposes);
                }, (err) => { this.$log.error(err) });
            }
        };

        /**
         * @description Dismiss the modal instance.
         */
        public getAttendanceIssueTypes = (): void => {
            this.serviceLookupListSvc.getAttendanceIssueTypes().then((result) => {
                this.attendanceIssueTypes = result;
                this.setIssueType(0);
            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        public setIssueType = (id: number): void => {
            this.attendance.IssueTypeId = this.attendanceIssueTypes[id].IssueTypeId;
            this.attendance.IssueType = this.attendanceIssueTypes[id].Name;
            this.issueFlag = id;
            if (this.attendance.IssueType == "Absent") {
                this.time = null;
            } else {
                this.attendance.ReasonId = null;
                this.attendance.ReasonName = null;
            }
        }

        private getDateTime = (): Date => {
            var dateOfOccurrence = new Date(this.attendance.DateOfOccurrence.toString());
            if (this.time != null) {
                return new Date(
                    dateOfOccurrence.getFullYear(),
                    dateOfOccurrence.getMonth(),
                    dateOfOccurrence.getDate(),
                    this.time.getHours(),
                    this.time.getMinutes());
            }
            else {
                return new Date(
                    dateOfOccurrence.getFullYear(),
                    dateOfOccurrence.getMonth(),
                    dateOfOccurrence.getDate()
                );
            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        public setReason = (id: number, issueId: number): void => {
            this.attendance.ReasonId = this.attendanceIssueTypes[issueId].Reasons[id].ReasonId;
            this.attendance.ReasonName = this.attendanceIssueTypes[issueId].Reasons[id].Name;
            this.selected = this.attendance.ReasonName;
        }

    }
    angular.module("app").controller('Training.AttendanceModalCtrl', AttendanceModalCtrl);
}