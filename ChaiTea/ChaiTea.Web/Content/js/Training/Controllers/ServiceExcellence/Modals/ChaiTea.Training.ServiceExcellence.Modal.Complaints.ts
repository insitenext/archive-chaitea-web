﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
   
    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
    }
   
    class ComplaintsNewModalCtrl {
        complaintId: number = 0;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;

        options = {
            complaintTypes: [],
            buildings: [],
            preventableStatuses: [],
            floors: [],
            classifications: [],
            employees: []
        };

        maxDate: Date = new Date();
        basePath: string;
        isEntryDisabled: boolean = false;
        complaint = {
            IsRepeatComplaint: false,
            AccountableEmployees: [],
            FeedbackDate: null,
            Description: '',
            Note: '',
            Action: ''
        };
        selectedEmployees: any[] = [];

        toUpdate: boolean = false;
        modalInstance;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            '$filter',
            '$uibModalInstance',
            '$uibModal',
            'blockUI',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.apibasesvc',
            'employee'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: any,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private blockUI,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private employee: IEmployeeAdd) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.getAllLookuplist();
            this.complaint = {
                IsRepeatComplaint: false,
                FeedbackDate: this.dateFormatterSvc.formatDateShort(new Date(), 'MM/DD/YYYY'),
                AccountableEmployees: [],
                Description: '',
                Note: '',
                Action: ''
            };
        }

        public closeComplaint = (): any => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: this.employee.EmployeeId,
                Name: this.employee.Name,
                JobDescription: this.employee.JobDescription,
                ProfileImage: this.employee.ProfileImage
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Common/Templates/Personnel/personnel.complaintsClose.entry.tmpl.html',
                controller: 'Training.ComplaintsCloseModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    employee: () => { return sendEmployee; },
                    complaint: () => { return this.complaint }
                }
            }).result.then(result => {
                if (result.toUpdate) {
                    this.toUpdate = true;
                    this.complaint = result.complaint;
                }
            });
        }

        public close = ($e): void => {
            this.$uibModalInstance.close(this.toUpdate);
        };

        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }


        public getAllLookuplist = (): void => {
            this.apiSvc.getByOdata({ id: this.programId }, 'LookupList/ComplaintTypes/Program/:id', false, false).then((data) => {
                this.options.complaintTypes = data.Options;
            }, this.onFailure);

            this.apiSvc.getByOdata(null, 'LookupList/Buildings', false, false).then((data) => {
                this.options.buildings = data.Options;
            }, this.onFailure);

            this.apiSvc.getByOdata(null, 'LookupList/Classifications', false, false).then((data) => {
                this.options.classifications = data.Options;
            }, this.onFailure);

        }

        public getFloorsByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                this.blockUI.start('Loading Floors...');
                this.apiSvc.getByOdata({ id: buildingId }, 'LookupList/Floors/Building/:id', false, false).then((data) => {
                    this.options.floors = data.Options;
                    if (this.blockUI) this.blockUI.stop();
                }, this.onFailure);
            }
        }

        onBuildingChange = this.getFloorsByBuildingId;

        public save = (): void => {
            if (this.$scope.complaintsNewForm.$valid) {
                this.complaint.Description = this.$filter('htmlToPlaintext')(this.complaint.Description);
                this.complaint.Note = this.$filter('htmlToPlaintext')(this.complaint.Note);
                this.complaint.Action = this.$filter('htmlToPlaintext')(this.complaint.Action);
                this.selectedEmployees.push(this.employee.EmployeeId);
                this.complaint.AccountableEmployees = this.selectedEmployees;
                this.apiSvc.save(this.complaint, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Complaints]).then((result) => {
                    this.toUpdate = true;
                    this.$uibModalInstance.close(this.toUpdate);
                    var forHistoricalPurposes = '';
                    if (moment(this.complaint.FeedbackDate) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                        forHistoricalPurposes = '  This complaint is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
                    }
                    bootbox.alert('Complaint was saved successfully!' + forHistoricalPurposes);
                }, this.onFailure);
            }
        };

    }
    angular.module("app").controller('Training.ComplaintsNewModalCtrl', ComplaintsNewModalCtrl);

    class ComplaintsCloseModalCtrl {
        complaintId: number = 0;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;

        options = {
            complaintTypes: [],
            buildings: [],
            preventableStatuses: [],
            floors: [],
            classifications: [],
            employees: []
        };

        maxDate: Date = new Date();
        basePath: string;
        isEntryDisabled: boolean = false;

        selectedEmployees: any[] = [];

        toUpdate: boolean = false;
        modalInstance;
        multiSelect: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            '$filter',
            '$uibModalInstance',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'blockUI',
            'employee',
            'complaint'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: any,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private blockUI,
            private employee: IEmployeeAdd,
            private complaint) {

            this.basePath = sitesettings.basePath;
            this.multiSelect = this.basePath + 'Content/js/Common/Templates/multiselect.tmpl.html';
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.getAllLookuplist();
        }

        public getAllLookuplist = (): void => {

            this.apiSvc.getByOdata(null, 'LookupList/PreventableStatuses', false, false).then((data) => {
                this.options.preventableStatuses = data.Options;
            }, this.onFailure);
        }

        public updateComplaint = (): any => {
            if (this.$scope.complaintsCloseForm.$valid) {
                this.toUpdate = true;
                var compaintReturnObj = {
                    toUpdate: this.toUpdate,
                    complaint: this.complaint
                };
                this.$uibModalInstance.close(compaintReturnObj);
            }
        }

        public close = ($e): void => {
            var compaintReturnObj = {
                toUpdate: this.toUpdate,
                complaint: this.complaint
            };
            this.$uibModalInstance.close(compaintReturnObj);
        };

        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }
    }
    angular.module("app").controller('Training.ComplaintsCloseModalCtrl', ComplaintsCloseModalCtrl)
}