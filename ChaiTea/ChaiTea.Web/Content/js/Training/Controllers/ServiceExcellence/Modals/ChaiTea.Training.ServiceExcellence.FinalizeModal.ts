﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import commonInterfaces = Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;

    class SEPFinalizeModalCtrl {

        basePath: string;
        currentDate: Date;
        userId: number;
        employeesInfo: Array<trainingInterfaces.IEmployeeInfo> = [];
        employeeWithImage: commonInterfaces.IEmployeeWithAttachment;

        employeesToExempt: Array<trainingInterfaces.IEmployeeInfo> = [];
        dataPoints = {
            totalBonusAmount: 0,
            passedCount: 0,
            failedCount: 0
        }
        isFinalized: boolean = false;

        static $inject = [
            '$scope',
            'userInfo',
            'userContext',
            '$log',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            'dateRange',
            'employeesScorecardInfo',
            'finalizedDataPoints'
        ];
        constructor(
            private $scope,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private $log: ng.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateRange,
            private employeesScorecardInfo: any,
            private finalizedDataPoints: any) {

            this.basePath = sitesettings.basePath;
            this.userId = userInfo.userId;
            this.getUserInfo();
            this.currentDate = moment().toDate();
            this.employeesInfo = employeesScorecardInfo;
            this.dataPoints = finalizedDataPoints;
        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = (): void => {
            this.$uibModalInstance.close(this.isFinalized);
        };

        
        private getUserInfo = (): void => {
            this.apiSvc.getById(this.userId, 'TrainingEmployee').then((employee) => {

                this.employeeWithImage = <commonInterfaces.IEmployeeWithAttachment>{
                    Name: employee.Name,
                    EmployeeId: employee.EmployeeId,
                    HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                    JobDescription: employee.JobDescription.Name,
                    ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                }
            }, this.onFailure);
        }


        public setEmployeesToExempt = (employee: trainingInterfaces.IEmployeeInfo) => {
            var toggleFlag = _.some(this.employeesToExempt, function (emp) {
                return emp.EmployeeId == employee.EmployeeId
            });
            if (toggleFlag) {
                _.remove(this.employeesToExempt, function (emp) {
                    return emp.EmployeeId == employee.EmployeeId
                });
                this.dataPoints.totalBonusAmount = this.dataPoints.totalBonusAmount + employee.BonusAmount;
            }
            else {
                this.dataPoints.totalBonusAmount = this.dataPoints.totalBonusAmount - employee.BonusAmount;
                this.employeesToExempt.push(employee);
            }
        }

        private exemptEmployeeFromScorecard = (employee: trainingInterfaces.IEmployeeInfo) => {
            this.apiSvc.update(employee.EmployeeKpiMasterId, null, 'ServiceExcellence/ExemptEmployee').then((reuslt) => {
            }, this.onFailure);
        }

        private finalizeScoreCard = () => {
            var params = {
                SiteId: this.userContext.Site.ID,
                year: this.dateRange.year,
                month: this.dateRange.month,
            }
            this.apiSvc.save(params, 'ServiceExcellence/ScorecardStatus').then((result) => {
                this.isFinalized = true;
                this.close();
            }, this.onFailure);
        }
        /**
         * @description Dismiss the modal instance.
         */
        public submit = ($e): void => {
            if (this.employeesToExempt.length) {
                _.forEach(this.employeesToExempt, (emp) => {
                    this.exemptEmployeeFromScorecard(emp);
                });
                this.finalizeScoreCard();
            }
            else {
                this.finalizeScoreCard();
            }
        };

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }
    }
    angular.module("app").controller('Training.SEPFinalizeModalCtrl', SEPFinalizeModalCtrl);
}