﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;

    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
        IsKpiExempted: boolean;
    }

    class ConductModalCtrl {

        basePath: string;
        toUpdate: boolean = false;
        modalTitle: string = 'Conduct';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        attachmentName: string = '';
        maxDate: Date = null;
        conductTypes: Array<serviceInterfaces.IConductType> = [];
        conduct = <serviceInterfaces.IConduct>{
            ConductId: 0,
            DateOfOccurrence: <Date>null,
            Reason: '',
            ConductTypes: [],
            OrgUserId: 0,
            ConductAttachments: [],
            IsExempt: null
        }

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'title',
            'employee'];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private employee: IEmployeeAdd) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.conduct.OrgUserId = employee.EmployeeId;

            this.getConductTypes();
            this.maxDate = new Date();
            this.conduct.DateOfOccurrence = new Date();
        }
        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void => {
            this.$uibModalInstance.dismiss(this.toUpdate);
        };

        /**
         * @description Dismiss the modal instance.
         */
        public save = ($e): void => {

            if (this.$scope.conductForm.$valid) {
                this.conduct.IsExempt = this.employee.IsKpiExempted;
                this.apiSvc.save(this.conduct, "Conduct").then((res) => {
                    this.toUpdate = true;
                    this.$uibModalInstance.close(this.toUpdate);
                    var forHistoricalPurposes = '';
                    if (moment(this.conduct.DateOfOccurrence) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                        forHistoricalPurposes = '  This conduct is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
                    }
                    bootbox.alert('Conduct was saved successfully!' + forHistoricalPurposes);
                }, this.onFailure);
            }
        };

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Dismiss the modal instance.
         */
        public attachDocument = ($e): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Dismiss the modal instance.
         */
        public getConductTypes = (): void => {
            this.serviceLookupListSvc.getConductTypes().then((result: Array<serviceInterfaces.IConductType>) => {
                angular.forEach(result, (item) => {
                    item.IsSelected = false;
                });
                this.conductTypes = result;
                this.conductTypes[0].IsSelected = true;
                this.setConductType(this.conductTypes[0].ConductTypeId);
            }, this.onFailure);
        }

        /**
         * @description Dismiss the modal instance.
         */
        public setConductType = (id: number): void => {

            var conductType: serviceInterfaces.IConductType = _.find(this.conductTypes, { 'ConductTypeId': id });

            if (!conductType) return;

            if (!_.any(this.conduct.ConductTypes, { 'ConductTypeId': id })) {
                this.conduct.ConductTypes.push(conductType);
            } else {
                this.conduct.ConductTypes.splice(_.findIndex(this.conduct.ConductTypes, { 'ConductTypeId': id }), 1);
            }

        }

        /**
         * @description Dismiss the modal instance.
         */
        public addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.IConductAttachment>{
                UniqueId: null,
                AttachmentType: 'File',
                ConductAttachmentType: commonInterfaces.CONDUCT_ATTACHMENT_TYPE[commonInterfaces.CONDUCT_ATTACHMENT_TYPE.Misc],
                ConductAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.attachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.conduct.ConductAttachments.length) {
                            this.conduct.ConductAttachments[0] = tempAttachment;
                        } else {
                            this.conduct.ConductAttachments.push(tempAttachment);
                        }

                    }, (error: any) => {
                        //throw error
                    }, (progress: any) => {
                        //update progress bar
                    });

                });

            }
        }

    }
    angular.module("app").controller('Training.ConductModalCtrl', ConductModalCtrl);
}