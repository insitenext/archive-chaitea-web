﻿/// <reference path="../../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import safetyEnums = ChaiTea.Common.Safety.Enums;
    
    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
        IsKpiExempted: boolean;
    }

    class SafetyModalCtrl {

        basePath: string;

        modalTitle: string = 'Safety';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        //set defaults
        safety = <serviceInterfaces.ISafetyItem>{
            SafetyId: 0,
            DateOfIncident: <Date>null,
            Detail: '',
            IncidentTypes: [],
            EmployeeId: 0,
            SafetyAttachments: [],
            IsExempt: null
        }
        incidentTypes = safetyEnums.StaticIncidentTypes.AsList;
        maxDate: Date = null;
        attachmentName: string;

        toUpdate: boolean = true;

        static $inject = [
            '$scope',
            '$log',
            '$window',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'title',
            'employee'];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private title: string,
            private employee: IEmployeeAdd) {

            this.modalTitle = this.title || this.modalTitle;
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.entry.tmpl.html';
            this.maxDate = new Date();
            this.safety.EmployeeId = this.employee.EmployeeId;
            this.safety.DateOfIncident = new Date();
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /** @description checks to see if IncidentType object is selected when initializing. **/
        private isIncidentSelected = (id: number): boolean => {
            var foundIncident: boolean = false;
            angular.forEach(this.safety.IncidentTypes, (incidentType) => {
                if (incidentType == id) {
                    foundIncident = true;
                }
            });
            return foundIncident;
        }

        /** @description toggles whether IncidentType object is selected when that object is clicked. **/
        private toggleIncidentType = (id: number): void => {
            var index: number = 0;
            var foundIncident: boolean = false;

            angular.forEach(this.safety.IncidentTypes, (incidentType) => {
                if (incidentType == id) {
                    this.safety.IncidentTypes.splice(index, 1);
                    foundIncident = true;
                }
                index++;
            });
            if (!foundIncident) {
                this.safety.IncidentTypes.push(id);
            }
        }

        /**
         * CONDUCT ATTACHMENT
         * @description Handler when a file is selected.
         */
        public addFile = (files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var tempAttachment = <serviceInterfaces.ISafetyAttachment>{
                UniqueId: null,
                AttachmentType: 'File',
                SafetyAttachmentType: commonInterfaces.CONDUCT_ATTACHMENT_TYPE[commonInterfaces.CONDUCT_ATTACHMENT_TYPE.Misc],
                SafetyAttachmentTypeId: 1
            }

            if (files && files.length) {

                this.attachmentName = files[0].name;

                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                    if (!creds.AccessKey || !creds.AccessSecret) return;

                    this.awsSvc.s3Upload(files[0], creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.publicRead).then((data: commonInterfaces.IAwsObjectDetails) => {

                        tempAttachment.UniqueId = data.Name;
                        tempAttachment.AttachmentType = data.AttachmentType;

                        //as of now it supports only one attachments
                        if (this.safety.SafetyAttachments.length) {
                            this.safety.SafetyAttachments[0] = tempAttachment;
                        } else {
                            this.safety.SafetyAttachments.push(tempAttachment);
                        }

                    }, (error: any) => {
                        //throw error
                    }, (progress: any) => {
                        //update progress bar
                    });

                });

            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        public close = ($e): void => {
            this.$uibModalInstance.close(this.toUpdate);
        };

        /**
         * @description Save the new safety object
         */
        save = ($e: any): void => {

            if (this.$scope.safetyForm.$valid && this.safety.IncidentTypes.length != 0) {
                this.safety.IsExempt = this.employee.IsKpiExempted;
                this.apiSvc.save(this.safety, "Personnel/Safety").then((res) => {
                    this.toUpdate = true;
                    this.$uibModalInstance.close(this.toUpdate);
                    var forHistoricalPurposes = '';
                    if (moment(this.safety.DateOfIncident) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                        forHistoricalPurposes = '  This safety is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
                    }
                    bootbox.alert('Safety was saved successfully!' + forHistoricalPurposes);
                }, this.onFailure);
            }
        };
    }
    angular.module("app").controller('Training.SafetyModalCtrl', SafetyModalCtrl);
}