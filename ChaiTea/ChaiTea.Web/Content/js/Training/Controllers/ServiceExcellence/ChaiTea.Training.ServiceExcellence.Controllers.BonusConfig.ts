﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Training.ServiceExcellence.Controllers {
    "use strict";

    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualitySvc = ChaiTea.Quality.Services;
    import trainingInterface = ChaiTea.Training.Interfaces;
    import trainingServices = ChaiTea.Training.Services;
    import commonSvc = ChaiTea.Common.Services;


    enum FileType { Undefined, ProfilePicture };

    class BonusConfigCtrl {
        
        basePath;
        userId;
        employeesWithAttachment = [];
        employeesWithBonus = [];
        
        isBonusConfigViewer: boolean;
        canEdit = false;

        dateRange: { year: number; month: number; endDate: Date } = {
            year: moment().year(),
            month: moment().month() + 1,
            endDate: new Date()
        }

        siteBonus = <trainingInterface.ISiteBonus>{
            BonusAmount: 0,
            Month: this.dateRange.month,
            SiteId: 0,
            Year: this.dateRange.year
        };
        origBonusAmmount = 0;
        saveDisabled = true;
        siteBonusId: number = 0;

        totalBonus: number;
        disableEditing: boolean;

        isPageDisabled: boolean = false;
        static $inject = [
            '$log',
            '$scope',
            '$timeout',
            'blockUI',
            'userContext',
            'userInfo',
            'chaitea.core.services.usercontextsvc',
            'chaitea.training.services.serviceexcellence',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.apibasesvc',
            'sitesettings',
            'chaitea.common.services.imagesvc',
            'chaitea.training.services.SEPBonusConfig',
            'chaitea.common.services.localdatastoresvc',
            Common.Services.UtilitySvc.id
        ];

        constructor(
            private $log: angular.ILogService,
            private $scope: angular.IScope,
            private $timeout: angular.ITimeoutService,
            private blockUI,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private userContextSvc: Core.Services.IUserContextSvc,
            private serviceExcellenceSvc: Services.IServiceExcellenceSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private dateFormatterSvc: Core.Services.IDateFormatterSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private sitesettings: ISiteSettings,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private sepBonusSvc: trainingServices.ISEPBonusConfigSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private utilSvc: Common.Services.IUtilitySvc
            ) {
            this.basePath = sitesettings.basePath;
            this.userId = userInfo.userId;
        }

        public initialize = () => {
            
            this.siteBonus.SiteId = this.userContext.Site.ID;
            this.isPageDisabled = this.siteBonus.SiteId <= 0;

            if (this.isPageDisabled) {
                this.userContextSvc.OpenContextPicker(true);
                return;
            }

            if ((this.dateRange.month < (moment().month() + 1) && moment().date() > 5) || this.dateRange.month < (moment().month()))
                this.disableEditing = true;

            this.CanConfigureEmployeesBonus();
            this.getEmployees();

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate);
            }

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.dateRange.month = moment(obj.endDate).month()+1;
                this.dateRange.year = moment(obj.endDate).year();
                this.dateRange.endDate = moment(obj.endDate).endOf('month').toDate();

                if ((this.dateRange.month < (moment().month() + 1) && moment().date() > 5) || this.dateRange.month < (moment().month())) {
                    this.disableEditing = true;
                    this.canEdit = false;
                }
                else
                    this.disableEditing = false;

                this.siteBonus.Year = this.dateRange.year;
                this.siteBonus.Month = this.dateRange.month;

                this.getSiteBonus();
                this.getAllEmployeesBonus();
            });
            
        }

        private goToServiceExcellence = () => {
            return this.utilSvc.getWebLink('/personnel/sep');
        }

        /**
         * @description Returns whether the user can configure bonus. 
         */
        private CanConfigureEmployeesBonus = (): void => {
            this.isBonusConfigViewer = this.userInfo.mainRoles.bonusviewers;
        }

        /**
         * @description Handle edit event
         */
        public onEdit = (cancel?): void => {
            
            if (cancel) {
                this.siteBonus.BonusAmount = this.origBonusAmmount;
                this.canEdit = false;
                return;
            }

            this.canEdit = !this.canEdit;
        }

        /**
         * @description Handle save event
         */
        public save = (): void => {

            if (!this.canEdit) return;

            this.saveDisabled = true;

            if (this.siteBonusId) {
                this.sepBonusSvc.updateSiteBonus(this.siteBonusId, this.siteBonus).then((res): any => {

                    this.origBonusAmmount = this.siteBonus.BonusAmount;

                    if (this.employeesWithBonus.length) {
                        this.sepBonusSvc.updateBonusOfAllEmployees(this.employeesWithBonus).then((res): any => {
                            this.canEdit = false;
                            this.calculateTotalBonus();
                            this.notificationSvc.successToastMessage('Bonus amounts updated');
                        }, this.onFailure);
                    } else {
                        this.notificationSvc.successToastMessage('Bonus base amount updated');
                    }

                }, this.onFailure);
            } else {
                this.sepBonusSvc.saveSiteBonus(this.siteBonus).then((res): any => {

                    this.siteBonusId = res.SiteBonusId;
                    this.origBonusAmmount = this.siteBonus.BonusAmount;

                    if (this.employeesWithBonus.length) {
                        this.sepBonusSvc.updateBonusOfAllEmployees(this.employeesWithBonus).then((res): any => {
                            this.canEdit = false;
                            this.calculateTotalBonus();
                            this.notificationSvc.successToastMessage('Bonus amounts updated');
                        }, this.onFailure);
                    } else {
                        this.notificationSvc.successToastMessage('Bonus base amount updated');
                    }

                }, this.onFailure);
            }

        }

        /**
         * @description Get bonuses
         */
        private getAllEmployeesBonus = (): void => {
            this.employeesWithBonus = [];

            this.sepBonusSvc.getBonusOfAllEmployees({ month: this.dateRange.month, year: this.dateRange.year }).then((data): any => {
                this.employeesWithBonus = data;
                
                this.calculateTotalBonus();
            }, this.onFailure);
        }

        /**
         * @description Gets SiteBonus
         */
        private getSiteBonus = (): void => {
            this.sepBonusSvc.getSiteBonus({ month: this.dateRange.month , year: this.dateRange.year }).then((data): any => {
                if (data.length) {
                    this.siteBonusId = data[0].SiteBonusId;
                    this.siteBonus = data[0];
                    this.origBonusAmmount = this.siteBonus.BonusAmount;
                }
            }, this.onFailure);
        }

        /**
         * @description Calculate total bonus
         */
        private calculateTotalBonus = (): void => {
            this.totalBonus = 0;
            angular.forEach(this.employeesWithBonus,(emp) => {
                this.totalBonus += (emp.BonusAmount + this.siteBonus.BonusAmount)
            });
        }

        /**
        * @description get all supervisor employees
        */
        private getEmployees = (): void => {

            var params = { $orderby: 'Name asc', $filter: 'JobDescription ne null'};

            this.blockUI.start();

            this.apiSvc.getByOdata(params, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees): any => {

                this.blockUI.stop();

                angular.forEach(employees,(employee) => {
                    this.employeesWithAttachment.push(<trainingInterface.ITrainingEmployee>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
            }, this.onFailure);
        }      

        /**
        * @description Handle api failure
        */
        private onFailure = (response: any): void => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(response);
        }
       
    }
    angular.module("app").controller('BonusConfigCtrl', BonusConfigCtrl);

} 