﻿module ChaiTea.Training.Interfaces {

    export interface IKeyPerformanceIndicator {
        Name: string;
        Icon: string;
        Desc: string;
        Status: boolean;
    }

    export interface IEmployeeTask {
        EmployeeTaskId: number;
        EmployeeId: number;
        JobTaskId: number;
    }

    export interface ITrainingEmployee {
        Email?: string;
        UserAttachments?: Array<IUserAttachment>;
        EmployeeId: number;
        EmployeeTasks?: Array<IJobTask>;
        EmployeeEquipment?: Array<IJobEquipment>;
        HireDate?: string;
        JobDescription: IJobDescription;
        Name: string;
        PhoneNumber?: string;
        SupervisorId?: number;
        SupervisorPhoneNumber?: string;
        SupervisorEmail?: string;
        ProfileImage?: string;
        IsComplete?: boolean;
        AreaCount?: number;
        Status?: string;
        Supervisor?: ISupervisor;
        isNewHire?: boolean;
        ExternalEmployeeId?: string;
    }

    export interface ISiteBonus {
        BonusAmount: number;
        Month: number;
        Year: number;
        SiteId: number;
    }
    /*
     * @description ServiceExcellence Employee
     */
    export interface ISETrainingEmployee extends ITrainingEmployee {
        IsPass?: boolean;
        EmployeeKpis?: Array<IEmployeeKpis>;
        Month: number;
        Year: number;
        //---VM variables---
        showToggleBlock?: boolean;
        BonusAmount: number;
    }

    

    interface IUserAttachment extends Common.Interfaces.IAttachment {
        UserAttachmentId: number;
        UserAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    interface IEmployeeAttachment extends Common.Interfaces.IAttachment {
        EmployeeAttachmentId: number;
        EmployeeAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    enum USER_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    enum EMPLOYEE_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    /*
     * @description AKA "Position"
     */
    export interface IJobDescription {
        DutySumarry: string;
        JobDuties: Array<any>;
        JobId: number;
        LegalDescriptionSummary: string;
        LegalDescriptions: Array<any>;
        Name: string;
        Purpose: string;
        SampleTaskSummary: string;
        SampleTasks: Array<any>;
        Summary:string;
        WorkEnvironment: string;
        UpdateDate: Date;
        UpdateBy:string;
    }

    export interface IManager {
        ManagerId: number;
        Name: string;
        JobDescription: string;
        PhoneNumber: string;
        Email?: string;
        ProfileImage?: string;
        ProfilePictureUniqueId?: string;        
    }

    export interface ISupervisor {
        SupervisorId: number;
        Name: string;
        JobDescription: string;
        PhoneNumber: string;
        Email?: string;
        ProfilePictureUniqueId?: string;
        ProfileImage?: string;
        Manager?: IManager;       
    }

    export interface IJobDescriptionExtended {
        Email: string;
        UserAttachments: Array<IUserAttachment>;
        EmployeeId: number;
        EmployeeTasks: Array<IJobTask>;
        HireDate: Date;
        JobDescription: IJobDescription;
        Name: string;
        PhoneNumber: string;
        ProfileImage?: string;
        FullDescription?: string;
        SupervisorId?: number;
        SupervisorName?: string;
        SupervisorPhone?: string;
        Supervisor?: any;
        JobContract: IContract;
        UpdateById: string;
        UpdateDate: Date;
        FunFacts?: string;   
    }

    export interface IJobTask {
        JobTaskId?: number;
        Name: string;
    }

    export interface IJobEquipment {
        JobEquipmentId?: number;
        Name: string;
    }

    export interface IJobPurpose {
        JobPurposeId?: number;
        JobId: number;
        ClientId: number;
        Description:string;
    }

    export interface IContract {
        ContractId: number;
        Employee: ITrainingEmployee;
        ContractAttachments: Array<IContractAttachments>;
        UpdateDate: Date;
        
    }

    export interface IContractAttachments extends ChaiTea.Common.Interfaces.IAttachment {
        ContractAttachmentId: number;
        ContractAttachmentType: IContractAttachmentType;
        CreateDate?: Date;
        UpdateDate?: Date;

    }

    export enum IContractAttachmentType {
        EmployeeSignature = 1,
        SupervisorSignature = 2
    }

    

    export interface IEmployeeInfo {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        HireDate: Date;
        IsPass: boolean;
        BonusAmount: number;
        EmployeeKpis: Array<IEmployeeKpis>;
        IsClosed: boolean;
        Comment: string;
        UserAttachments: Array<IUserAttachments>;
        ProfileImage: string;
        ExternalEmployeeId: number;
        EmployeeKpiMasterId: number;
        IsExempt: boolean;
    }

    export interface IKpi {
        KpiId: number;
        Name: string;
        Summary?: string;
        Description?: string;
        Icon?: string;
    }

    export interface IEmployeeKpis {
        IsPass: boolean;
        Kpi: IKpi;
        Metric?: number;
        Comment?: string;
        PositiveComment?: string;
        ProgramName?: string;
        CheckBoxOptions?: any[];
        MetaData?: any[];
        IsPassingData?: boolean;
        PassingMetric?: number;
        EmployeeMonthlyKpiId: number;
        IsExempt: boolean;
    }

    export interface IUserAttachments {
        AttachmentType: string;
        UniqueId: string;
        UserAttachmentType: string;
    }
    export interface IScorecardStatus {
        ScorecardStatusId: number;
        SiteId: number;
        year: number;
        month: number;
        IsFinalized: boolean;
        CreateDate: Date;
        CreateById: number;
    }
}