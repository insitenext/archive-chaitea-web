﻿module ChaiTea.Training.Services {
    'use strict';

    export interface ISEPBonusConfigSvc {
        getBonusOfAllEmployees(params?: Object): ng.IPromise<any>;
        getBonusOfAllEmployeesForAdmin(params?: Object): ng.IPromise<any>;
        getSiteBonus(params?: Object): ng.IPromise<any>;
        updateBonusOfAllEmployees(params?: Object): ng.IPromise<any>;
        updateSiteBonus(id : number,params?: Object): ng.IPromise<any>;
        saveSiteBonus(params?: Object): ng.IPromise<any>;
        getBonusOfEmployee(params?: Object): ng.IPromise<any>;
        getYearlyBonusOfEmployee(params?: Object): ng.IPromise<any>;
    }

    interface ISEPBonusConfigResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    class SEPBonusConfigSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ISEPBonusConfigSvc {
            return {
                getBonusOfAllEmployees: () => (this.getBonusOfAllEmployees()),
                getBonusOfAllEmployeesForAdmin: () => (this.getBonusOfAllEmployeesForAdmin()),
                getSiteBonus: () => (this.getSiteBonus()),
                updateBonusOfAllEmployees: (params?: any) => (this.updateBonusOfAllEmployees(params)),
                updateSiteBonus: (id,params) => (this.updateSiteBonus(id,params)),
                saveSiteBonus: (params) => (this.saveSiteBonus(params)),
                getBonusOfEmployee: () => (this.getBonusOfEmployee()),
                getYearlyBonusOfEmployee: () => (this.getYearlyBonusOfEmployee()),
            };
        }

        getBonusOfAllEmployees = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/EmployeeBonus/:year/:month/').query(params).$promise;
        }
        getBonusOfEmployee = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/EmployeeBonus/:employeeId/:year/:month').get(params).$promise;
        }
        getYearlyBonusOfEmployee = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/YearlyBonus/:employeeId/:year/:month').get(params).$promise;
        }
        getBonusOfAllEmployeesForAdmin = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/EmployeeBonus/Admin/:year/:month/').query(params).$promise;
        }
        updateBonusOfAllEmployees = (params?) => {
            return this.SEPEmployeeBonusConfigResource().update(params).$promise;
        }

        getSiteBonus = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/SiteBonus/:year/:month/').query(params).$promise;
        }
        updateSiteBonus = (id, params?) => {
            params = angular.extend(params || {}, { id: id });
            return this.SEPSiteBonusConfigResource().update(params).$promise;
        }
        saveSiteBonus = (params?) => {
            return this.SEPSiteBonusConfigResource().save(params).$promise;
        }
       
        /**
         * @description Resource object for sep employee bonus config.
         */
        private SEPEmployeeBonusConfigResource = (): ISEPBonusConfigResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: true
            };
           
            var url = this.basePath + 'api/Services/EmployeeBonuses';



            return <ISEPBonusConfigResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    },
                });
        }


        /**
         * @description Resource object for sep site bonus config.
         */
        private SEPSiteBonusConfigResource = (): ISEPBonusConfigResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/SiteBonus/';

            return <ISEPBonusConfigResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

       
    }


    angular.module('app').service('chaitea.training.services.SEPBonusConfig', SEPBonusConfigSvc);
}