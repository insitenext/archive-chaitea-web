﻿module ChaiTea.Training.Services {
    'use strict';
    
    import commonInterfaces = Common.Interfaces;

    export interface IServiceExcellenceSvc {
        getEmployeeList(year: number, month: number,  params?: commonInterfaces.IODataPagingParamModel):ng.IPromise<any>;
        getEmployeeKpi( year: number, month: number,  empId: number):ng.IPromise<any>;
        updateEmployeeKpi(id: number, params?: any): ng.IPromise<any>;
        getKpiComment(params?: Object): ng.IPromise<any>;
        updateKpiComment(id: number, params?: any): ng.IPromise<any>;
    }

    export interface IKpiCommentSvc {
        getKpiComment(year: number, month: number, employeeId: number): ng.IPromise<any>;
        updateKpiComment(id: number, params?: any): ng.IPromise<any>;
    }


    interface IServiceExcellenceResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    interface IKpiCommentResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }



    class ServiceExcellenceSvc implements angular.IServiceProvider{
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IServiceExcellenceSvc {
            return {
                getEmployeeList: ( year,month, params?: commonInterfaces.IODataPagingParamModel) => (this.getEmployeeList(year,month,  params)),
                getEmployeeKpi: (year,month,  empId) => (this.getEmployeeKpi(year,month,empId)),
                updateEmployeeKpi: (id: number, params?: any) => (this.updateEmployeeKpi(id, params)),
                getKpiComment: () => (this.getKpiComment()),
                updateKpiComment: (id: number, params?: any) => (this.updateKpiComment(id, params))
            };
        }

        getEmployeeList = (year: number, month: number, params?: commonInterfaces.IODataPagingParamModel) => {
            params = angular.extend(params || {}, { year: year, month: month})
            return this.ServiceExcellenceResource().query(params).$promise;
        }

        getEmployeeKpi = ( year: number,month: number, empId: number) => {
            return this.ServiceExcellenceResource().get({year:year,month:month, id: empId }).$promise;
        }

        getKpiComment = (params?) => {
            return this.$resource(this.basePath +
                'api/Services/KpiComment/:year/:month/:employeeId').get(params).$promise;
        }

        updateEmployeeKpi = (id: number, params) => {
            params = angular.extend(params || {}, { id: id });
            return this.ServiceExcellenceResource().update(params).$promise;
        }

        updateKpiComment = (id: number, params) => {
            params = angular.extend(params || {}, { id: id });
            return this.KpiCommentResource().update(params).$promise;
        }

        /**
         * @description Resource object for training.
         */
        private ServiceExcellenceResource = (): IServiceExcellenceResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/ServiceExcellence/';

            return <IServiceExcellenceResource>this.$resource(
                url + ':year/:month/:id',
                { year:'@year',month:'@month',id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

        private KpiCommentResource = (): IServiceExcellenceResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false,
            };

            var url = this.basePath + 'api/Services/KpiComment/';
            var url1 = this.basePath + 'api/Services/KpiComment/';
            return <IServiceExcellenceResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }


       
    }


    angular.module('app').service('chaitea.training.services.serviceexcellence', ServiceExcellenceSvc);
}