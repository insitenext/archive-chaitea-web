﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Quality.Directives {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    import commonInterfaces = Common.Interfaces;

    /**
    * @description Expands a block of text
    */
    export interface IExpandTextScope extends ng.IScope {
        collapsed: boolean;
        maxLength: number;
        startHidden: boolean;
        startExpanded: boolean;
        moreText: string;
        lessText: string;
        model: any;
        toggle(): void;
    }

    expandText.$inject = ['$log', '$compile'];
    function expandText(
        $log: angular.ILogService,
        $compile: angular.ICompileService): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            //require: 'ngModel',
            scope: {
                model: '=ngModel',
                dataMax: '=dataMax',
                dataText: '=dataText',
                id: '@',

            },
            template: '<div class="summary">{{model}}</div>',
            link: ($scope: IExpandTextScope, $element, $attrs: any): void => {
                
                $scope.collapsed = false;

                //add toggle function
                $scope.toggle = () => {
                    $scope.collapsed = !$scope.collapsed;
                };

                //look for text change
                $attrs.$observe('text',(text: string): void => {
  
                    //get attrs and set defaults
                    $scope.maxLength = $attrs.max || 0;
                    $scope.startExpanded = $attrs.startExpanded || false;
                    $scope.startHidden = ($scope.maxLength > 0 ? false : true);
                    $scope.moreText = $attrs.moreText || 'More';
                    $scope.lessText = $attrs.lessText || 'Less';

                    if (text.length > $scope.maxLength || $scope.startHidden) {

                        //get parts
                        var firstPart: string = text.substr(0, $scope.maxLength);
                        var secondPart: string = ($scope.startHidden ? text : text.substr($scope.maxLength, text.length));

                        //build html
                        var firstSpan: any = $compile('<span>' + firstPart + '</span>')($scope);
                        var secondSpan: any = $compile('<span ng-if="collapsed">' + secondPart + '</span>')($scope);
                        var moreIndicatorSpan: any = $compile('<span ng-if="!collapsed">... </span>')($scope);
                        var lineBreak: any = $compile('<br ng-if="collapsed">')($scope);
                        var toggleButton: any = $compile('<div class="collapse-text-toggle" ng-click="toggle()">{{collapsed ? "' + $scope.lessText + '" : "' + $scope.moreText + '"}} <i ng-if="collapsed" class="fa-chevron-left" /><i ng-if="!collapsed" class="fa-chevron-right" /></div>')($scope);

                        //empty and add elems
                        $element.empty();
                        if (!$scope.startHidden) {
                            $element.append(firstSpan);
                        }
                        $element.append(secondSpan);
                        if (!$scope.startHidden) {
                            $element.append(moreIndicatorSpan);
                            $element.append(lineBreak);
                        }
                        $element.append(toggleButton);

                    } else {
                        $element.empty();
                        $element.append(text);
                    }

                });

            }
           
        };

    }
    angular.module('app').directive('expandText', expandText);

    /**
    * @description Toggles a list
    */
    export interface IExpandListScope extends ng.IScope {
        moreText: string;
        lessText: string;
        list: Array<any>;
        toggle(): void;
        collapsed: boolean;  
        limitToOrig: number;
        limitTo: number;      
    }

    expandList.$inject = ['$log', '$compile'];
    function expandList(
        $log: angular.ILogService,
        $compile: angular.ICompileService): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                id: '@',
                listClass: '@',
                listItemClass: '@'
            },  
            template: `<div>
                            <ul class="{{listClass || 'list-group'}}">
                                <li class="{{listItemClass}}" ng-repeat="item in model | limitTo:limitTo">
                                {{item.Name}} <div class="detail pull-right"><icon class="{{item.DetailStyle}}"></icon></div>
                                </li>
                            </ul>
                            <div class="collapse-text-toggle" ng-click="toggle()">{{!collapsed ? ('SHOWLESS' | translate) : ('SHOWMORE'|translate)}} <i ng-if="!collapsed" class="fa-chevron-left" /><i ng-if="collapsed" class="fa-chevron-right" /></div>
                        </div>
                        `,
            link: ($scope: IExpandListScope, $element:ng.IAugmentedJQuery, $attrs): void => {

                $scope.$watch('model',(list: Array<any>): void => {
                    $scope.list = $scope['model'] || [];
                });

                $scope.toggle = () => {
                    $scope.limitTo = ($scope.collapsed ? $scope.list.length : $scope.limitToOrig);
                    $scope.collapsed = !$scope.collapsed;
                };

                //defaults
                $scope.collapsed = true;
                $scope.limitToOrig = $attrs['limitTo'] || 5;
                $scope.limitTo = $scope.limitToOrig;
                $scope.list = $scope['model'] || [];
            }

        };
    }
    angular.module('app').directive('expandList', expandList);
}