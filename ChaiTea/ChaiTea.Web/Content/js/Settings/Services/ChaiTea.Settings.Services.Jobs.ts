﻿module ChaiTea.Settings.Services {
    'use strict';

    import commonInterfaces = Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;

    export interface IJobSvc {
        saveNewJob(data?: Object): ng.IPromise<any>;
        updateJob(id: number, data?: Object): ng.IPromise<any>;
        saveNewSampleTask(data: settingsInterfaces.ISampeTask): ng.IPromise<any>;
        getAllPositions(params?: commonInterfaces.IODataPagingParamModel): ng.IPromise<any>;
        getAllExemptionTypeIds(params?: commonInterfaces.IODataPagingParamModel): ng.IPromise<any>;
        getAllSampleTasks(params?: commonInterfaces.IODataPagingParamModel): ng.IPromise<any>;
        getAllLegalDescriptions(params?: commonInterfaces.IODataPagingParamModel): ng.IPromise<any>;
        getAllDuties(params?: commonInterfaces.IODataPagingParamModel): ng.IPromise<any>;
    }

    interface IJobResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    class JobSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IJobSvc {
            return {
                saveNewJob: (data?: any) => (this.saveNewJob(data)),
                updateJob: (id: number, data?: any) => (this.updateJob(id, data)),
                saveNewSampleTask: (data: settingsInterfaces.ISampeTask) => (this.saveNewSampleTask(data)),
                getAllPositions: (params?: commonInterfaces.IODataPagingParamModel) => (this.getAllPositions(params)),
                getAllExemptionTypeIds: (params?: commonInterfaces.IODataPagingParamModel) => (this.getAllExemptionTypeIds(params)),
                getAllSampleTasks: (params?: commonInterfaces.IODataPagingParamModel) => (this.getAllSampleTasks(params)),
                getAllLegalDescriptions: (params?: commonInterfaces.IODataPagingParamModel) => (this.getAllLegalDescriptions(params)),
                getAllDuties: (params?: commonInterfaces.IODataPagingParamModel) => (this.getAllDuties(params))

            };
        }

        saveNewJob = (data: Object) => {
            return this.jobResource().save(data).$promise;
        }

        updateJob = (id: number, data: Object) => {
            data = angular.extend(data || {}, { id: id });
            return this.jobResource().update(data).$promise;
        }

        saveNewSampleTask = (data: settingsInterfaces.ISampeTask) => {
            return this.sampleTaskResource().save(data).$promise;
        }

        getAllPositions = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.jobResource().getByOData(params).$promise;
        }

        getAllExemptionTypeIds = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ExemptionTypes').get(params).$promise;
        }

        getAllSampleTasks = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.sampleTaskResource().getByOData(params).$promise;
        }

        getAllLegalDescriptions = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.legalDescriptionResource().getByOData(params).$promise;
        }

        getAllDuties = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.dutyResource().getByOData(params).$promise;
        }


        /**
         * @description Resource object for training.
         */
        private jobResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/Job/';

            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for training.
         */
        private sampleTaskResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/SampleTask/';

            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for training.
         */
        private legalDescriptionResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/LegalDescription/'; 

            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for training.
         */
        private dutyResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/Duty/';

            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

    }


    angular.module('app').service('chaitea.settings.services.jobs', JobSvc);
}