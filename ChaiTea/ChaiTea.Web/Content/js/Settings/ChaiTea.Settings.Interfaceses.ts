﻿module ChaiTea.Settings.Interfaces {

    export interface IJobDescription {
        DutySumarry: string;
        JobDuties: Array<IDuty>;
        JobId: number;
        LegalDescriptionSummary: string;
        LegalDescriptions: Array<ILegalDescription>;
        Name: string;
        Abbreviation: string;
        Purpose: string;
        SampleTaskSummary: string;
        SampleTasks: Array<ISampeTask>;
        Summary: string;
        WorkEnvironment: string;
    }
        
    export interface IJobTask {
        JobTaskId?: number;
        Name: string;
    }

    export interface ISampeTask {
        SampleTaskId?: number;
        Name: string;
        JobId: number;
    }

    export interface ILegalDescription {
        LegalDescriptionId?: number;
        Name: string;
        Description: string;
    }

    export interface IDuty {
        DutyId?: number;
        Description: string;
    }

    export interface IMessageBuilder {
        TrainingMessageId: number;
        Title: string;
        Text: string;
        Jobs: Array<IJob>;
        Sites: Array<{}>;
        Department: {};
        IsCustomer: boolean;
    }
    export interface IJob {
        JobId: number;
        Name: string;
    }

    export interface IDepartment {
        DepartmentId: number;
        Name: string;
    }
    export interface IJobs {
        JobId: number;
        Name: string;
    }
}