﻿module ChaiTea.Settings.MessagesReceived.Controllers {
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;

    interface IStaffMessage {
        Id: number;
        ProfileImage: string;
        DepartmentName: string;
        To: string;
        Details: string;
        Title: string;
        Site: string;
        When: string;
        LabelColor: string;
        SentDate: Date
    }
    interface IAssociateMessageStats {
        EmployeeId: number;
        EmployeeName: string;
        ProfileImage: string;
        JobName: string;
        HireDate: Date;
        TotalMessagesReceived: number;
        UserAttachments: any
        OpenedMessagesCount: number;
        OpenedMessagePercentage: number;
    }
    interface IAssociateMessage {
        DepartmentId: number;
        DepartmentName: string;
        ProfileImage: string;
        LabelColor: string;
        MessageTitle: string;
        Message: string;
        SentTo: Array<settingsInterfaces.IJob>,
        SentToNameLength: number,
        ReceivedDate: Date;
        When: string;
    }

    interface IResult {
        TotalRecipients: number,
        Opened: number,
        NotOpened: number
    }

    class MessagesReceivedIndexCtrl {
        staffMessages: Array<IStaffMessage> = [];
        associateMessagesStats: Array<IAssociateMessageStats> = [];
        modalInstance: any;
        count: number;
        departments: Array<settingsInterfaces.IDepartment> = []
        selectedDepartment: settingsInterfaces.IDepartment;
        jobs: Array<settingsInterfaces.IJobs> = []
        selectedJobId: number = 0;
        showFilter: boolean = false;
        recordsToSkip: number = 0;
        top: number = 10;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        interimCount: number = 0;
        activeTab: number = 0;
        messagesSentCount: number = 0;
        messagesReceivedCount: number = 0;
        startDate: string = '';
        endDate: string = '';
        location: string = '';
        totalMessagesSentOut: string = '';
        openedMessagesCount: string = '';
        totalDepartments: string = '';
        charts = {
            messagesReceivedChart: {},
            messagesSentChart: {},
            messagesMonthlyChart: {}
        }
        jobsCount: number = 0;
        toggleBtnFlag: boolean = false;
        noOfMonths: number = 6;
        associateMessagesTabCounts = {
            totalMessages: 0,
            totalEmployees: 0
        }
        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc',
            'sitesettings',
            '$uibModal',
            'chaitea.core.services.dateformattersvc',
            'userContext',
            Common.Services.ImageSvc.id
        ];
        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private userContext: IUserContext,
            private imageSvc: Common.Services.IImageSvc
        ) {
            this.location = this.userContext.Client.Name + (this.userContext.Site.ID ? ',' : '') + (this.userContext.Site.Name || '');
            this.charts.messagesReceivedChart = this.chartConfigBuilder('messagesReceivedChart', {});
            this.charts.messagesSentChart = this.chartConfigBuilder('messagesSentChart', {});
            this.charts.messagesMonthlyChart = this.chartConfigBuilder('messagesMonthlyChart', {});
            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.HeaderDateChange, this.$scope, (event, obj) => {
                this.startDate = obj.startDate;
                this.endDate = obj.endDate;
                this.getMessagesReset();
                this.updateTab();
                this.getTabCounts();
                this.getMessageCount();
            });
        }

        public initialize = (): void => {
            this.selectedDepartment = {
                DepartmentId: null,
                Name: null
            };
            this.getDepartments();
            this.getAllJobsCount();
            this.getPositions();
        }

        public getMessagesReset = (): void => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.staffMessages = [];
            this.associateMessagesStats = [];
            this.interimCount = 0;
            this.getMessages();
            this.getMessageCount();
            this.getAssociateMessagesTabCounts();
        }

        private getAllJobsCount = () => {
            this.apiSvc.query({}, 'MessagesReceived/AllJobsCount', false, false).then((result): any => {
                this.jobsCount = result.JobsCount;
            }, this.onFailure);
        }

        private getTabCounts = (): any => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.endDate)
            };
            this.apiSvc.query(params, "MessagesReceived/TabCounts/:startDate/:endDate", false, false).then((result) => {
                this.messagesSentCount = result.MessagesSentCount;
                this.messagesReceivedCount = result.MessagesReceivedCount;
            }, this.onFailure);
        }

        public getMessageCount = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.endDate),
                departmentId: this.selectedDepartment.DepartmentId
            };
            this.apiSvc.query(params, 'MessagesReceived/SentMessagesCount/:startDate/:endDate/:departmentId', false, false).then((result) => {
                if (!result) return this.$log.log('No Messages.');
                this.count = result.SentMessagesCount;
            }, this.onFailure);
        }

        public getMessages = () => {
            if (this.toggleBtnFlag) {
                this.getAssociateMessages();
            } else {
                this.getStaffMessages();
            }
        }

        public getAssociateMessagesTabCounts = () => {
            
            var param = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.endDate).endOf('day')),
                positionId: this.selectedJobId ? this.selectedJobId : 0
            };
            this.apiSvc.query(param, 'MessagesReceived/ByEmployeesCounts/:startDate/:endDate/:positionId', false, false).then((data) => {
                this.associateMessagesTabCounts.totalEmployees = data.EmployeeCount;
                this.associateMessagesTabCounts.totalMessages = data.MessageCount;
            }, this.onFailure);
        }

        public getAssociateMessages = () => {
            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;

            var param = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.endDate).endOf('day')),
                positionId: this.selectedJobId ? this.selectedJobId : 0,
                top: this.top,
                skip: this.recordsToSkip
            };
            this.apiSvc.query(param, 'MessagesReceived/StatsByEmployee/:startDate/:endDate/:positionId/:top/:skip', false, true).then((data) => {
                if (data.length === 0) {
                    this.isAllLoaded = true;
                }
                this.interimCount += data.length;
                angular.forEach(data, (item: IAssociateMessageStats) => {
                    item.EmployeeName = item.EmployeeName.toLowerCase();
                    item.ProfileImage = this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true);
                    item.OpenedMessagePercentage = (item.OpenedMessagesCount * 100) / item.TotalMessagesReceived;
                    this.associateMessagesStats.push(item);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        public getStaffMessages = (): void => {
            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;
            var startDate = this.dateFormatterSvc.formatDateFull(this.startDate);
            var endDate = this.dateFormatterSvc.formatDateFull(moment(this.endDate).endOf('day'));
            var params = {
                $orderby: 'CreateDate desc',
                $top: this.top,
                $skip: this.recordsToSkip,
                $filter: `CreateDate ge DateTime'${startDate}' and CreateDate lt DateTime'${endDate}'`
            }
            if (this.selectedDepartment.DepartmentId) {
                params['$filter'] += `and Department/DepartmentId eq ${this.selectedDepartment.DepartmentId}`;
            }
            this.apiSvc.getByOdata(params, 'MessagesReceived/Messages').then((data) => {
                if (data.length === 0) {
                    this.isAllLoaded = true;
                }
                this.interimCount += data.length;
                angular.forEach(data, (item) => {
                    // Filter out any messages sent to training admins
                    if (item.Jobs.length > 0) {
                        item = this.processMessage(item);
                        this.staffMessages.push(item);
                    }                    
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        private processMessage = (item: any) => {
            var sentTo: string = "";
            if (this.jobsCount == 0) {
                sentTo = "Pending...";
            } else if (item.Jobs.length == this.jobsCount) {
                sentTo = "All Positions ";
            }
            else {
                angular.forEach(item.Jobs, (job) => {
                    sentTo = sentTo + job.Name + ",";
                });
            }
            if (item.IsCustomer) {
                sentTo += " ,Customer"
            }
            else {
                sentTo = sentTo.slice(0, -1);
            }
            item.sentTo = sentTo;
            var sites: string = "";
            angular.forEach(item.Sites, (site) => {
                sites = sites + site.Name + " ,";
            });
            item.sites = sites.slice(0, -1);
            item.when = "";
            if (moment().isSame(item.CreateDate, 'days')) {
                item.when = "Today @ " + moment(item.CreateDate).format('h:mm A');
            }
            else {
                item.when = moment(item.CreateDate).format('MMM DD, YYYY @ h:mm A');
            }
            return {
                Id: item.TrainingMessageId,
                ProfileImage: this.sitesettings.defaultProfileImage,
                DepartmentName: item.Department.Name,
                To: item.sentTo,
                Details: item.Text,
                Title: item.Title,
                Site: item.sites,
                When: item.when,
                LabelColor: _.kebabCase(item.Department.Name),
                SentDate: item.CreateDate,
                Opened: item.Opened,
                TotalRecipients: item.TotalRecipients,
                OpenedPercentage: item.TotalRecipients != 0 ? (item.Opened * 100) / item.TotalRecipients : 0
            };
        }

        private getDepartments = (): void => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Department]).then((departments): any => {
                if (!departments.length) return this.$log.log('No departments available.');
                this.departments = [];
                this.departments = departments;
            }, this.onFailure);
        }

        private getPositions = (): void => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Job]).then((jobs): any => {
                if (!jobs.length) return this.$log.log('No jobs available.');
                this.jobs = [];
                this.jobs = jobs;
            }, this.onFailure);
        }

        private toggleTabs = (activeTab: number): any => {
            this.activeTab = activeTab;
            this.updateTab();
        };

        public toggleFilter = (): any => {
            this.showFilter = !this.showFilter;
        }

        private updateTab = () => {
            if (this.activeTab == 0) {
                this.getMessagesReceivedChart();
            }else if (this.activeTab == 1) {
                this.getMessagesSentChart();
            } else if (this.activeTab == 2) {
                this.getMonthlyTrendOfMessagesSentByDepartment();
            }
        }

        public loadStatsForMessage = (message: IStaffMessage): void => {
            var params = {
                messageId: message.Id
            };
            this.apiSvc.query(params, 'MessagesReceived/MessageStatsById/:messageId', false, false).then((result: IResult) => {
                if (result.TotalRecipients != 0) {
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Settings/messagebuilder.messagestats.tmpl.html',
                        controller: 'Common.MessageStatsCtrl as vm',
                        size: 'md',
                        resolve: {
                            message: () => { return message; },
                            result: () => { return result; },
                            fromMessagesReceived: () => { return true; }
                        }
                    });
                }
                else {
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Settings/messagebuilder.noStats.tmpl.html',
                        controller: 'Common.SendMessageCtrl as vm',
                        size: 'sm',
                        resolve: {
                            message: () => { return message; },
                            fromMessagesReceived: () => { return true; }
                        }
                    });
                }
            }, this.onFailure);

        }

        public ShowEmployeeMessages = (employeeId: number, employeeName: string): void => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Settings/Templates/ChaiTea.Settings.EmployeeMessages.tmpl.html',
                controller: 'Settings.EmployeeMessages as vm',
                size: 'md',
                resolve: {
                    employee: () => { return { employeeId: employeeId, name: employeeName }; },
                    dateRange: () => { return { startDate: this.startDate, endDate: this.endDate }; }
                }
            });
        }

        private getMessagesReceivedChart = (): any => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.endDate)
            };
            this.apiSvc.getByOdata(params, 'MessagesReceived/MessagesReceivedInfoByJob/:startDate/:endDate').then((result) => {
                if (!result) return;
                var categories = [];
                var allMessages = [];
                var openedMessages = [];
                var notOpenedMessages = [];
                var openedColors = [SiteSettings.colors.secondaryColors.teal,
                    SiteSettings.colors.secondaryColors.blue,
                    SiteSettings.colors.secondaryColors.lime,
                    SiteSettings.colors.secondaryColors.lavendar,
                    SiteSettings.colors.secondaryColors.orange,
                    SiteSettings.colors.secondaryColors.blueSteel,
                    SiteSettings.colors.secondaryColors.pink
                ];
                var notOpenedColors = [
                    SiteSettings.colors.secondaryColors.tealDark,
                    SiteSettings.colors.secondaryColors.blueDark,
                    SiteSettings.colors.secondaryColors.limeDark,
                    SiteSettings.colors.secondaryColors.lavendarDark,
                    SiteSettings.colors.secondaryColors.orangeDark,
                    SiteSettings.colors.secondaryColors.blueSteelDark,
                    SiteSettings.colors.secondaryColors.pinkDark
                ];
                var i = 0;
                angular.forEach(result, (item) => {
                    categories.push(item.JobDescription);
                    allMessages.push(item.AllMessagesCount);
                    openedMessages.push({ y: item.OpenedMessagesCount, color: openedColors[i] });
                    notOpenedMessages.push({ y: (item.AllMessagesCount - item.OpenedMessagesCount), color: notOpenedColors[i] });
                    i++;
                    i = i == 7 ? 0 : i;
                });
                var openMsgsCount = openedMessages.reduce(function (m, num) {
                    return m + num.y;
                }, 0);
                this.openedMessagesCount = openMsgsCount.toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 });
                this.charts.messagesReceivedChart = this.chartConfigBuilder('messagesReceivedChart', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        stackLabels: {
                            enabled: true,
                            style: {
                                color: 'black'
                            }
                        },
                        title: null,
                        tickInterval: allMessages.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        },
                        column: {
                            colorByPoint: false
                        }
                    },
                    series: [
                        {
                            name: 'Not Opened',
                            lineWidth: 100,
                            data: notOpenedMessages
                        },
                        {
                            name: 'Opened',
                            lineWidth: 100,
                            data: openedMessages
                        }
                    ],
                    legend: {
                        enabled: false,
                    },
                });
            }, this.onFailure);
        }

        private getMessagesSentChart = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.endDate)
            };
            this.apiSvc.query(params, "MessagesReceived/MessagesSentInfoByDepartment/:startDate/:endDate").then((result) => {
                var data = [];
                data = result;
                var msgsSent = data.reduce(function (m, num) {
                                                        return m + num.MessageCount;
                }, 0);
                this.totalMessagesSentOut = msgsSent.toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 });
                this.totalDepartments = data ? data.length.toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 }) : '00';
                this.charts.messagesSentChart = this.chartConfigBuilder('messagesSentChart', {
                    series: [
                        {
                            type: 'pie',
                            name: 'messagesSentChart',
                            innerSize: '60%',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['DepartmentName'],
                                    sliceY = d['MessageCount'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                }
                            })
                        }],
                });
            }, this.onFailure);
        }

        public getMonthlyTrendOfMessagesSentByDepartment = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.endDate)
            };
            this.apiSvc.query(params, "MessagesReceived/MonthlyTrendByDepartment/:startDate/:endDate").then((result) => {

                if (!result.length) return;

                var dataSeries = [];
                var dataCategories = [];

                //to get xAxis values
                this.noOfMonths = Math.abs(moment(this.startDate).diff(moment(this.endDate), 'months')) + 1;
                var date = moment(_.clone(this.startDate));
                for (var i = 0; i < this.noOfMonths; i++) {
                    dataCategories.push(date.format('MMM \'YY'));
                    date.add('month', 1);
                }

                var tempObj = [];
                angular.forEach(result, (item: any) => {
                    item.xAxis = moment().month(item.Month - 1).year(item.Year).format('MMM \'YY');
                    tempObj.push(item);
                });

               var data = _.chain(tempObj)
                    .groupBy('DepartmentId')
                    .map((value, key) => {
                        var data = [];
                        angular.forEach(dataCategories, (item: any) => {
                            var valueForThisMOnth = _.find(value, (v: any) => { return v.xAxis == item; });

                            if (valueForThisMOnth) {
                                data.push(valueForThisMOnth.MessageCount);
                            } else {
                                data.push(0);
                            }

                        });

                        return {
                            Id: key,
                            Name: value[0].DepartmentName,
                            Data: data,
                            Total: _.sum(_.pluck(value, 'MessageCount'))
                        };
                    }).value();

               data = _.sortByOrder(data, ['Total'], ['desc'])

               var categoryLength = data.length - 1;
               angular.forEach(data, (category) => {
                   dataSeries.push({ data: category.Data, name: category.Name, amount: category.Total, index: categoryLength })
                   categoryLength -= 1;
               });

                // Build the chart
                this.charts.messagesMonthlyChart = this.chartConfigBuilder('messagesMonthlyChart', {
                    xAxis: {
                        tickColor: 'white',
                        categories: dataCategories,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: dataSeries,
                    legend: {
                        enabled: false
                    }
                });
            }, this.onFailure);
        }

        private onFailure = (response: any): void => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(response);
        }

        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                }
            });

            var chartOptions = {

                // Pie Chart
                messagesSentChart: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },
                // Column Chart
                messagesReceivedChart: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                messagesMonthlyChart: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },
            }
            return _.assign(chartOptions[name], options);
        }
    }

    class EmployeeMessages {

        recordsToSkip: number = 0;
        top: number = 10;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        interimCount: number = 0;
        activeTab: number = 0;
        associateMessages: Array<IAssociateMessage> = [];
        selectedMessage: IAssociateMessage;
        messageCountsByStatus: any;
        isListView: boolean = true;
        isNew: boolean = null;
        static $inject = [
            '$scope',
            '$log',
            'chaitea.common.services.notificationsvc',
            '$uibModalInstance',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            'chaitea.core.services.dateformattersvc',
            'sitesettings',
            'employee',
            'dateRange'
        ];
        constructor(
            private $scope,
            private $log: ng.ILogService,
            private notificationSvc: commonSvc.INotificationSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private sitesettings: ISiteSettings,
            private employee: any,
            private dateRange: any) {

            this.getEmployeeMessagesStatusCount();
            this.getEmployeeMessages();
        }

        public getEmployeeMessagesStatusCount = () => {
            var param = {
                employeeId: this.employee.employeeId,
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).endOf('day')),
            };
            this.apiSvc.query(param, 'MessagesReceived/MessagesReceivedByEmployeeStatusCounts/:employeeId/:startDate/:endDate', false, false).then((data) => {
                this.messageCountsByStatus = data;
            }, this.onFailure);
        }

        public resetMessages = (isNew, tab) => {
            this.associateMessages = [];
            this.isNew = isNew;
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.activeTab = tab;
            this.getEmployeeMessages();
        }

        public getEmployeeMessages = () => {
            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;

            var param = {
                employeeId: this.employee.employeeId,
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).endOf('day')),
                top: this.top,
                skip: this.recordsToSkip,
                isNew: this.isNew
            };
            this.apiSvc.query(param, 'MessagesReceived/ByEmployee/:employeeId/:startDate/:endDate/:top/:skip/:isNew', false, true).then((data) => {
                if (data.length === 0) {
                    this.isAllLoaded = true;
                }
                angular.forEach(data, (item: IAssociateMessage) => {
                    item.ProfileImage = this.sitesettings.defaultProfileImage;
                    item.LabelColor = _.kebabCase(item.DepartmentName);
                    item.When = "";
                    if (moment().isSame(item.ReceivedDate, 'days')) {
                        item.When = "Today @ " + moment(item.ReceivedDate).format('h:mm A');
                    }
                    else {
                        item.When = moment(item.ReceivedDate).format('MMM DD, YYYY @ h:mm A');
                    }
                    var name = [];
                    name = _.map(item.SentTo, 'Name');
                    item.SentToNameLength = name.reduce(function (sum, n) {
                        return sum + n.length
                    }, 0);
                    this.associateMessages.push(item);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        public showMessageDetails = (message) => {
            this.isListView = false;
            this.selectedMessage = message;
        }

        public goBackToList = () => {
            this.isListView = true;
        }

        private close = () => {
            this.$uibModalInstance.dismiss();
        }

        private onFailure = (response: any): void => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(response);
        }

    }
    angular.module('app').controller('Settings.EmployeeMessages', EmployeeMessages);
    angular.module('app').controller('Settings.MessagesReceivedIndexCtrl', MessagesReceivedIndexCtrl);
}