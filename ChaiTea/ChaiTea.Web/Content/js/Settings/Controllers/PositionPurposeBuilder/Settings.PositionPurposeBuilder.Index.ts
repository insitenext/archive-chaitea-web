﻿module ChaiTea.Settings.PositionPurposeBuilder.Controllers {
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    interface IFormScope extends angular.IScope {
        mainForm: any;
    }
    interface IJobPurpose {
        JobPurposeId: number;
        JobId:number;
        ClientId: number;
        Description: string;
    }

    class PositionPurposeBuilder implements Common.Interfaces.INgController {
        clients: Array<commonInterfaces.IKeyValue> = [];
        client: commonInterfaces.IKeyValue;
        position: commonInterfaces.IKeyValue;
        positions: Array<commonInterfaces.IKeyValue> = [];
        purpose: IJobPurpose;

        origEnPurpose: IJobPurpose;
        origEnPositionIndex: number = 1;

        //A clone of the current selected position in the selected language for Translation purposes.
        curLangPurposeClone: IJobPurpose;
        currentAltLang: string = "";

        static $inject = [
            '$scope',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: IFormScope,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.INotificationSvc) {

        }

        public initialize = (): void => {
            this.getClients();
            this.messageBusSvc.emitMessage('components.headerpanel:languagehide', true);

            this.messageBusSvc.onMessage('components.headerpanel:languagechange', this.$scope, (scope, language) => {
                if (!this.purpose.JobPurposeId) {
                    this.notificationSvc.warningToastMessage('Language cannot be changed on non-saved Position Purposes.');
                    this.handleFailure('Cannot get translated fields with no Purpose selected.');
                    return;
                }

                if (language == 'en') {
                    this.purpose = this.origEnPurpose;
                    this.currentAltLang = 'en';
                } else {
                    this.apiSvc.getById(this.purpose.JobPurposeId, 'JobPurpose')
                        .then((purpose): any => {
                        if (!purpose) {
                            this.handleFailure('No purpose was found in the language: '+ language);
                            return;
                        }

                        this.curLangPurposeClone = this.purpose = purpose;
                        this.currentAltLang = language;
                    });
                }
            });

            this.$scope.$watch(() => { return this.purpose }, (newVal, oldVal) => {
                if (newVal == oldVal) return;
                
                var isHidden = newVal && !newVal.JobPurposeId ? true : false;
                this.messageBusSvc.emitMessage('components.headerpanel:languagehide', isHidden);
            });
        }

        public getClients = () => {
            this.apiSvc.getLookupList('User/Clients').then((res) => {
                if (!res.Clients.Options) {
                    this.handleFailure('No Clients to show.');
                    return;
                }

                this.clients = res.Clients.Options;
                
            }, this.handleFailure);
        }

        public onClientChange = () => {
            this.getPositions(this.client);
        }

        /**
        * @description Gets and sets list of positions
        */
        private getPositions = (client: any, jobId?: number): void => {
            if (!client) {
                this.positions = this.$scope.mainForm.position = [];
                this.handleFailure('No Client selected.');
                return;
            }

            this.apiSvc.getResource('LookupList/Jobs/ByClient/:clientId').query({ clientId: client.Key }).$promise.then((res) => {
                if (!res) return this.handleFailure("There are no sites to show.");

                this.positions = res;
                                
                this.position = this.positions[0];

                this.getJobPurposeByClientAndJob(client.Key, this.position.Key);

            }, this.handleFailure);

        }

        /**
       * @description Fires on dropdown change
       */
        public onPositionChange = ($e): void => {
            this.getJobPurposeByClientAndJob(this.client.Key, this.position.Key);

            if (this.currentAltLang != 'en') {
                this.messageBusSvc.emitMessage('components.headerpanel:languagechange', "en");
            }
        }

        public getJobPurposeByClientAndJob = (clientId: number, jobId: number): void => {
            this.apiSvc.getResource(`JobPurpose/${clientId}/${jobId}`).get().$promise.then((res) => {
                if (!res) {
                    this.handleFailure("There is no JobPurpose to show.");
                    return;
                }

                this.purpose = res;
                this.purpose.JobId = this.position.Key;
                this.purpose.ClientId = this.client.Key;
                this.purpose.JobPurposeId = this.purpose.JobPurposeId || 0;
                if (!this.currentAltLang || this.currentAltLang == 'en') {
                    this.origEnPurpose = this.purpose;
                }
            }, this.handleFailure);
        }

        public saveJobPurpose = (): void => {
            if (this.$scope.mainForm.$invalid || !this.purpose.JobId || !this.purpose.ClientId) {
                this.handleFailure('Form is invalid');
                return;
            }

            if (this.purpose.JobPurposeId) {
                if (this.currentAltLang && this.currentAltLang != "en") {
                    bootbox.confirm(`Are you sure you want to update this Position Purpose's <strong>${this.currentAltLang.toUpperCase()}</strong> translation?`, (res) => {
                        if (res) {
                            this.apiSvc.save(this.purpose, "JobPurpose/UpdateLocalizable").then((res:IJobPurpose) => {
                                this.notificationSvc.successToastMessage(`Position Purpose for the <strong>${this.currentAltLang.toUpperCase()}</strong> version of <strong>${this.position.Value}</strong> has been updated.`);
                            },this.handleFailure);
                        }
                    })
                } else {
                    this.apiSvc.update(this.purpose.JobPurposeId, this.purpose, 'JobPurpose').then((res) => {
                        this.notificationSvc.successToastMessage(`Position Purpose for <strong>${this.position.Value}</strong> has been updated.`);
                    },this.handleFailure);
                }
            } else {
                this.apiSvc.save(this.purpose, 'JobPurpose').then((res) => {
                    this.notificationSvc.successToastMessage(`Position Purpose for <strong>${this.position.Value}</strong> has been created.`);

                    this.purpose = this.origEnPurpose = res;

                    this.messageBusSvc.emitMessage('components.headerpanel:languagehide', false);
                },this.handleFailure);
            }
        }

        private getPositionIndex = (jobId: number): number=> {

            var positionIndex = 0;
            var keepGoing = true

            angular.forEach(this.positions, (position, key) => {
                if (keepGoing) {
                    if (position.Key == jobId) {
                        positionIndex = key;
                        keepGoing = false;
                    }
                }
            });

            return positionIndex;
        }

        private handleFailure = (error: string) => {
            this.$log.error(error);
            return false;
        }
    }

    angular.module('app').controller('Settings.PositionPurposeBuilder', PositionPurposeBuilder);
}