﻿module ChaiTea.Settings.JobDescriptionBuilder.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    class JobDescriptionBuilderCtrl implements commonInterfaces.INgController {

        positions: Array<settingsInterfaces.IJobDescription>;
        position: settingsInterfaces.IJobDescription;


        origEnPosition: settingsInterfaces.IJobDescription;
        origEnPositionIndex:number = 1;
        //A clone of the current selected position in the selected language for Translation purposes.
        curLangPositionClone:settingsInterfaces.IJobDescription;
        currentAltLang: string = "";

        sampleTaskList: Array<settingsInterfaces.ISampeTask>;
        taskSelectLoading: boolean = false;

        legalDescriptionList: Array<settingsInterfaces.ILegalDescription>;
        legalSelectLoading: boolean = false;

        dutyList: Array<settingsInterfaces.IDuty>;
        dutySelectLoading: boolean = false;

        newPositionName: string = '';
        showNewPosition: boolean = false;

        isHideJobDuties: boolean = false;
        isHideSampleTasks: boolean = false;
        

        hideJobDutiesBtnText: string = 'Hide Items';
        hideSampleTasksBtnText: string = 'Hide Items';

        ExemptionTypes = [];

        newJobDuty: settingsInterfaces.IDuty;
        newSampleTask: settingsInterfaces.ISampeTask;
        newLegalDescription: settingsInterfaces.ILegalDescription = {
            Name: '',
            Description: ''
        };


        jobDescriptionForm: ng.IFormController;

        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.settings.services.jobs',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private jobSvc: jobSvc.IJobSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.INotificationSvc) {
        }

        public initialize = (): void => {
            this.getPositions();
            this.getExemptionTypeIds();

            this.messageBusSvc.onMessage('components.headerpanel:languagechange', this.$scope,(scope,language) => {
                if (!this.position.JobId) {
                    this.notificationSvc.warningToastMessage('Language cannot be changed on non-saved positions.');
                    return this.$log.error('Cannot get translated fields with no position selected.');
                }

                if (language == 'en') {
                    this.position = this.origEnPosition || this.positions[this.origEnPositionIndex];
                    this.currentAltLang = "en";
                } else {
                    this.apiSvc.getById(this.position.JobId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Job]).then((position): any => {
                        if (!position) return this.$log.error('No position was found in the language: ', language);

                        this.curLangPositionClone = this.position = position;
                        this.currentAltLang = language;
                    });
                }
            });

            this.$scope.$watch(() => { return this.position }, (newVal, oldVal) => {
                if (newVal == oldVal) return;

                var isHidden = !newVal.JobId ? true : false;
                this.messageBusSvc.emitMessage('components.headerpanel:languagehide', isHidden);
            });
        }

        getExemptionTypeIds = (): void => {
            this.jobSvc.getAllExemptionTypeIds().then((result): any => {
                this.ExemptionTypes = result.Options;
            });
        }
        /**
        * @description Saves job description
        */
        public saveJobDescription = ($e): void => {
            $e.preventDefault();
            if (this.jobDescriptionForm.$invalid) return;

            this.position.Name = this.newPositionName;

            if (this.position.JobId) {
                if (this.currentAltLang && this.currentAltLang != "en") {
                    bootbox.confirm(`Are you sure you want to update this position profiles ${this.currentAltLang == 'es'?'Spanish':'English'} translation?`,(res) => {
                        if (res) {
                            this.apiSvc.save(this.position, "Job/UpdateLocalizable");
                        }
                    })
                } else {
                    this.jobSvc.updateJob(this.position.JobId, this.position).then((res) => {
                        this.getPositions(this.position.JobId);
                        this.notificationSvc.successToastMessage("Job has been saved.");
                    });
                }
            } else {
                this.jobSvc.saveNewJob(this.position).then((res) => {
                    this.notificationSvc.successToastMessage("Job has been saved.");
                    this.positions.push(this.position);
                    this.clearPosition();
                    this.getPositions(res.JobId);
                });
            }
        }

        /**
        * @description Fires on dropdown change
        */
        public onPositionChange = ($e): void => {
            if (this.positions.length) {
                if (this.position.Name === 'New Position') {
                    this.position.JobId = 0;
                    this.showNewPosition = true;
                    this.newPositionName = '';
                } else {
                    this.newPositionName = this.position.Name;
                }

                this.origEnPosition = angular.copy(this.position);

                if (this.currentAltLang != 'en')
                    this.messageBusSvc.emitMessage('components.headerpanel:languagechange', "en");
            } 
        }

        private getPositionIndex = (jobId: number): number=> {

            var positionIndex = 0;
            var keepGoing = true

            angular.forEach(this.positions,(position, key) => {
                if (keepGoing) {
                    if (position.JobId == jobId) {
                        positionIndex = key;
                        keepGoing = false;
                    }
                }
            });

            return positionIndex;
        }

        /**
        * @description Add new duty to list
        */
        public addDuty = ($e): void => {
            $e.preventDefault();

            if (!this.newJobDuty.Description) return;

            if (!this.position.JobDuties) this.position.JobDuties = [];

            if (!_.any(this.position.JobDuties, { 'Description': this.newJobDuty.Description })) {
                this.position.JobDuties.unshift(this.newJobDuty)
            }

            this.newJobDuty = <settingsInterfaces.IDuty>{ Description: '' };

        }

        /**
        * @description Add new task to list
        */
        public addTask = ($e): void => {
            $e.preventDefault();

            if (!this.newSampleTask.Name) return;

            if (!this.position.SampleTasks) this.position.SampleTasks = [];

            if (!_.any(this.position.SampleTasks, { 'Name': this.newSampleTask.Name })) {
                this.position.SampleTasks.unshift(this.newSampleTask)
            }

            this.newSampleTask = <settingsInterfaces.ISampeTask>{ Name: '' };

        }

        /**
        * @description Add new legal description to list
        */
        public addLegalDescription = ($e): void => {
            $e.preventDefault();

            if (!this.newLegalDescription.Name || !this.newLegalDescription.Description) {
                this.notificationSvc.warningToastMessage('Legal name and description is required');
                return;
            }

            if (!this.position.LegalDescriptions) this.position.LegalDescriptions = [];

            if (!_.any(this.position.LegalDescriptions, { 'Name': this.newLegalDescription.Name })) {
                this.position.LegalDescriptions.unshift(this.newLegalDescription)
            }

            this.newLegalDescription = <settingsInterfaces.ILegalDescription>{ Name: '', Description: '' };

        }

        /**
        * @description Removes item from list by index
        */
        public removeItemFromList = ($e: any, index: number, listName: string): void => {
            $e.preventDefault();
            this.position[listName].splice(index, 1);
        }

        /**
        * @description hide button text toggle functionality
        */
        public hide = ($e: any, toHide: string): void => {
            $e.preventDefault();
            if (toHide == 'JobDuties') {
                if (!this.isHideJobDuties) {
                    this.isHideJobDuties = true;
                    this.hideJobDutiesBtnText = 'Show Items';
                }
                else {
                    this.isHideJobDuties = false;
                    this.hideJobDutiesBtnText = 'Hide Items';
                }
            }

            if (toHide == 'SampleTasks') {
                if (!this.isHideSampleTasks) {
                    this.isHideSampleTasks = true;
                    this.hideSampleTasksBtnText = 'Show Items';
                }
                else {
                    this.isHideSampleTasks = false;
                    this.hideSampleTasksBtnText = 'Hide Items';
                }
            }
        }
        /**
        * @description Returns all duties for autocomplete
        */
        public getDutiesForView = (current) => {
            var tasksCopy = angular.copy(this.dutyList);
            if (current) {
                // tasksCopy.unshift(current);
            }
            return tasksCopy;
        }

        /**
        * @description Returns all tasks for autocomplete
        */
        public getTasksForView = (current) => {
            var tasksCopy = angular.copy(this.sampleTaskList);
            if (current) {
                // tasksCopy.unshift(current);
            }
            return tasksCopy;
        }

        /**
        * @description Returns all legal descriptions for autocomplete
        */
        public getLegalDescriptionsForView = (current) => {
            var tasksCopy = angular.copy(this.legalDescriptionList);
            if (current) {
                // tasksCopy.unshift(current);
            }
            return tasksCopy;
        }

        /**
        * @description Clears new position field
        */
        public clearPosition = () => {
            this.showNewPosition = false;
            this.position = this.positions[1];
            this.newPositionName = this.position.Name;
        }

        /**
        * @description Gets and sets list of positions
        */
        private getPositions = (jobId?:number): void => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Job]).then((positions): any => {
                if (!positions.length) return this.$log.log('No positions available.');

                this.positions = [];

                this.positions = positions;
                //add extra item to trigger an add event 
                this.positions.unshift(<settingsInterfaces.IJobDescription>{ JobId: 0, Name: 'New Position' });

                if (!this.position) {
                    this.position = positions[1];
                } else {
                    this.origEnPositionIndex = this.getPositionIndex(jobId || this.position.JobId);
                    this.position = positions[this.origEnPositionIndex];
                }
                this.newPositionName = this.position.Name;

                if (this.currentAltLang == 'en') {
                    this.origEnPosition = angular.copy(this.position);
                }
            });
        }

        /**
        * @description Gets and sets list of tasks 
        */
        private getSampleTasks = (jobId?: number): void => {

            var params = <commonInterfaces.IODataPagingParamModel>{ $filter: (jobId ? 'JobId eq ' + jobId : 'JobId gt 0') };

            this.jobSvc.getAllSampleTasks().then((tasks): any => {

                this.sampleTaskList = tasks;

            });

        }

        /**
        * @description Gets and sets list of legal descriptions
        */
        private getLegalDescriptions = (jobId?: number): void => {

            var params = <commonInterfaces.IODataPagingParamModel>{ $filter: (jobId ? 'JobId eq ' + jobId : 'JobId gt 0') };

            this.jobSvc.getAllLegalDescriptions().then((tasks): any => {

                this.legalDescriptionList = tasks;

            });

        }

        /**
        * @description Gets and sets list of duties
        */
        private getDuties = (jobId?: number): void => {

            var params = <commonInterfaces.IODataPagingParamModel>{ $filter: (jobId ? 'JobId eq ' + jobId : 'JobId gt 0') };

            this.jobSvc.getAllDuties().then((tasks): any => {

                this.dutyList = tasks;

            });

        }

    }

    angular.module('app').controller('Settings.JobDescriptionBuilderIndex', JobDescriptionBuilderCtrl);
} 