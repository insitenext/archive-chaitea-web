﻿module ChaiTea.Settings.MessageBuilder.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    class MessageBuilderCtrl {

        messageBuilder= <settingsInterfaces.IMessageBuilder>{
            Jobs: [],
            Text: '',
            Sites: [],
            Department: {},
            IsCustomer: false,
            TrainingMessageId: 0
        };
        job: settingsInterfaces.IJob;
        sites = [];
        selectedSites = [];
        positions = [];
        selectedPositions = [];
        selectAll: boolean = false;
        selectAllPositions: boolean = false;
        departments = [];
        department: settingsInterfaces.IDepartment;
        toCustomer: boolean = false;

        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.settings.services.jobs',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private jobSvc: jobSvc.IJobSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.INotificationSvc) {
        }

        public initialize = (): void => {
            this.getPositions();
            this.getClientSite();
            this.getDepartments();
        }

        /**
        * @description Gets and sets list of positions
        */
        private getPositions = (jobId?: number): void => {
           
            this.apiSvc.getByOdata({ $select: 'JobId,Name' , $orderby: 'Name asc'}, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Job]).then((positions): any => {
                if (!positions.length) return this.$log.log('No positions available.');

                this.positions = [];

                this.positions = positions;
            });
        }

         /**
        * @description Gets and sets list of client sites
        */
        private getClientSite = (): void => {
            var siteName;
            this.apiSvc.getByOdata({ $filter: `Type eq 'Site'`, $orderby: 'Name asc' }, 'client/orgs').then((res) => {
                this.sites = res;
            });
        }

         /**
        * @description Add or remove item on check or uncheck
        */
        private addRemoveItem = (item) => {
           
            var existItem = _.find(this.selectedSites, item);
            if (existItem) {
                _.pull(this.selectedSites, existItem);
            } else {
                this.selectedSites.push(item);
            }
        }

        /**
       * @description Add or remove position on check or uncheck
       */
        private addRemovePosition = (item) => {

            var existItem = _.find(this.selectedPositions, item);
            if (existItem) {
                _.pull(this.selectedPositions, existItem);
            } else {
                this.selectedPositions.push(item);
            }
        }

         /**
        * @description Select all sites
        */
        checkAll = () => {
            this.selectedSites = [];
            if (this.selectAll)
                this.selectAll = true;
            else
                this.selectAll = false;

            angular.forEach(this.sites, (site) => {
                site.selected = this.selectAll;
                if (this.selectAll) {
                    this.selectedSites.push(site);
                }
            });

        }

        /**
       * @description Select all positions
       */
        checkAllPositions = () => {
            this.selectedPositions = [];
            if (this.selectAllPositions)
                this.selectAllPositions = true;
            else
                this.selectAllPositions = false;

            angular.forEach(this.positions, (position) => {
                position.selected = this.selectAllPositions;
                if (this.selectAllPositions) {
                    this.selectedPositions.push(position);
                }
            });

        }

        /**
        * @description Gets and sets list of departments
        */
        private getDepartments = () => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Department]).then((departments): any => {
                if (!departments.length) return this.$log.log('No departments available.');

                this.departments = [];

                this.departments = departments;
            });
        }

        /**
       * @description to save message and position
       */
        send = (): void => {
            this.messageBuilder.Jobs = [];
            this.messageBuilder.Jobs = this.selectedPositions;
            this.messageBuilder.Sites = this.selectedSites;
            this.messageBuilder.Department = this.department;
          
            if (!this.messageBuilder.Text || this.selectedSites.length == 0 || !this.department) {
                bootbox.alert("Provide all required fields");
            }
            else if (this.selectedPositions.length == 0 && !this.messageBuilder.IsCustomer) {
                bootbox.alert("Select either a postion or check the Send to Customer option");
            }
            else {
                this.apiSvc.save(this.messageBuilder, 'TrainingMessage').then((res) => {
                    bootbox.alert('message successfully sent');
                });
            }
        }

    }

    angular.module('app').controller('Settings.MessageBuilderCtrl', MessageBuilderCtrl);
} 