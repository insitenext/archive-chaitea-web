﻿module ChaiTea.Settings.MessageBuilder.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import coreSvc = ChaiTea.Core.Services;

    interface IChartDetails {
        Title: string;
        SentDate: Date;
        Percent: number;
        Recipients: number;
    }

    interface ITotalRecipientsChart {
        JobDescription: string,
        AllMessagesCount: number,
        OpenedMessagesCount
    }

    class MessageBuilderStatsCtrl {

        messages = [];
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        messageStatsChart = {
            Opened: {},
            MostOpened: {},
            LeastOpened: {},
            MessagesSent: {},
            TotalRecipients: {}
        };
        mostOpenedDetails: IChartDetails = {
            Title: "",
            SentDate: null,
            Percent: 0,
            Recipients: 0
        };

        leastOpenedDetails: IChartDetails = {
            Title: "",
            SentDate: null,
            Percent: 0,
            Recipients: 0
        };
        opened: IChartDetails = {
            Title: "",
            SentDate: null,
            Percent: 0,
            Recipients: 0
        };
        openedColors: any;
        totalMessages: number = 0;
        activeTab: number = 0;
        openedMessagesCount: number = 0;
        sentMessageCount: number = 0;
        recipientMessageCount: number = 0;
        customMenu = [
                ['bold', 'italic', 'underline', 'ordered-list', 'unordered-list', 'link']
            ]
        

        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.dateformattersvc',
            'sitesettings'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            sitesettings : ISiteSettings) {
            
            this.messageStatsChart.Opened = this.chartConfigBuilder('messageStatsOpened', {});
            this.messageStatsChart.MostOpened = this.chartConfigBuilder('messageStats', {});
            this.messageStatsChart.LeastOpened = this.chartConfigBuilder('messageStats', {});
            this.messageStatsChart.MessagesSent = this.chartConfigBuilder('messageSent', {});
            this.messageStatsChart.TotalRecipients = this.chartConfigBuilder('messageStatsTotalRecipients', {});

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).add(1, 'day').endOf('day');

                this.getAllCounts();
                this.refreshCharts();
            });
        }

        public initialize = (): void => {
            
        }

        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private getAllCounts = (): any => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            };

            this.apiSvc.getByOdata(params, "MessageStats/MessageCounts/:startDate/:endDate", false, false).then((result) => {
                this.openedMessagesCount = result.Opened;
                this.sentMessageCount = result.SentMessages;
                this.recipientMessageCount = result.TotalRecipients;
            }, this.onFailure);
        }

        private refreshCharts = (): void => {
            this.getMostOpenedChart();
            this.getLeastOpenedChart();
            this.activeTab == 0 ? this.getMessagesSentChart() : this.activeTab == 1 ? this.getTotalRecipientsChart() : this.getOpenedChart(); 
        }

        private getOpenedChart = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
            };

            this.apiSvc.getByOdata(params, "MessageStats/MessageStats/:startDate/:endDate", false, false).then((result) => {
                if (!result.TotalRecipients) {
                    this.opened = {
                        Title: "",
                        SentDate: null,
                        Percent: 0,
                        Recipients: 0
                    }
                    return;
                }                        
                this.totalMessages = result.NotOpened + result.Opened;
                this.opened = {
                    Title: "",
                    SentDate: null,
                        Percent: result.Percent,
                        Recipients: result.TotalRecipients
                    };
                var data = [];
                data.push({
                    name: 'OPENED MESSAGES', y: result.Opened
                });
                data.push({
                    name: 'NOT OPENED MESSAGES', y: result.NotOpened
                });

                // Build the chart
                this.messageStatsChart.Opened = this.chartConfigBuilder('messageStatsOpened', {
                    series: [
                        {
                            type: 'pie',
                            name: 'messageStatsOpened',
                            innerSize: '60%',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['name'],
                                    sliceY = d['y'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                }
                            })
                        }],
                });
            }, this.onFailure);
        }

        private getMostOpenedChart = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                $orderby: 'Percent desc',
                $top: 1,
            };

            this.apiSvc.getByOdata(params, "MessageStats/Messages/:startDate/:endDate").then((result) => {
                if (!result.length) {
                    this.mostOpenedDetails = {
                        Title: "",
                        SentDate: null,
                        Percent: 0,
                        Recipients: 0
                    };
                    return;
                }                        
                this.apiSvc.getByOdata({ TrainingMessageId: result[0].MessageId }, "TrainingMessage/:TrainingMessageId", false, false).then((res) => {
                    this.mostOpenedDetails = {
                        Title: res.Title,
                        SentDate: res.CreateDate,
                        Percent: result[0].Percent,
                        Recipients: result[0].TotalRecipients
                    };
                    var data = [];
                    data.push({
                        name: 'OPENED MESSAGES', y: result[0].Opened
                    });
                    data.push({
                        name: 'NOT OPENED MESSAGES', y: result[0].NotOpened
                    });

                    // Build the chart
                    this.messageStatsChart.MostOpened = this.chartConfigBuilder('messageStats', {
                        series: [
                            {
                                type: 'pie',
                                name: 'messageStats',
                                innerSize: '60%',
                                data: _.map(data, (d, index) => {
                                    var sliceName = d['name'],
                                        sliceY = d['y'];
                                    return {
                                        name: sliceName,
                                        y: sliceY,
                                    }
                                })
                            }],
                        height: 220,

                    });
                }, this.onFailure);
            }, this.onFailure);
        }

        private getLeastOpenedChart = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                $orderby: 'Percent',
                $top: 1,
            }
            this.apiSvc.getByOdata(params, "MessageStats/Messages/:startDate/:endDate").then((result) => {
                if (!result.length) {
                    this.leastOpenedDetails = {
                        Title: "",
                        SentDate: null,
                        Percent: 0,
                        Recipients: 0
                    };
                    return;
                }
                this.apiSvc.getByOdata({ TrainingMessageId: result[0].MessageId }, "TrainingMessage/:TrainingMessageId", false, false).then((res) => {
                    this.leastOpenedDetails = {
                        Title: res.Title,
                        SentDate: res.CreateDate,
                        Percent: result[0].Percent,
                        Recipients: result[0].TotalRecipients
                    }
                    var data = [];
                    data.push({
                        name: 'OPENED MESSAGES', y: result[0].Opened
                    });
                    data.push({
                        name: 'NOT OPENED MESSAGES', y: result[0].NotOpened
                    });
                    // Build the chart
                    this.messageStatsChart.LeastOpened = this.chartConfigBuilder('messageStats', {
                        series: [
                            {
                                type: 'pie',
                                name: 'messageStats',
                                innerSize: '60%',
                                data: _.map(data, (d, index) => {
                                    var sliceName = d['name'],
                                        sliceY = d['y'];
                                    return {
                                        name: sliceName,
                                        y: sliceY,
                                    }
                                })
                            }],
                    });
                }, this.onFailure);
            }, this.onFailure);
        }

        private getMessagesSentChart = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateFormatterSvc.formatDateFull(this.dateRange.startDate)),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)),
            };
            this.apiSvc.getByOdata(params, "MessageStats/MessagesSentMonthly/:startDate/:endDate", false, false).then((result) => {

                if (!result) return;

                var categories = result.Labels;


                var dataSeries = [];
                var dataCategories = [];

                angular.forEach(result.Categories, function (value, key) {
                    var total: number = 0;
                    total = _.sum(value);
                    dataCategories.push({ data: value, name: key, total: total })
                });

                dataCategories.sort(function (a, b) {
                    return b.total - a.total
                });

                var categoryLength = dataCategories.length - 1;
                angular.forEach(dataCategories, (category) => {
                    dataSeries.push({ data: category.data, name: category.name, amount: category.total, index: categoryLength })
                    categoryLength -= 1;
                });

                // Build the chart
                this.messageStatsChart.MessagesSent = this.chartConfigBuilder('messageSent', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: dataSeries,
                    legend: {
                        enabled: false
                    }
                });
            }, this.onFailure);
        }

        private getTotalRecipientsChart = (): any => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            };

            this.apiSvc.getByOdata(params, 'MessageStats/TotalRecipients/:startDate/:endDate').then((result: Array<ITotalRecipientsChart>) => {
                if (!result) return;
                var categories = [];
                var allMessages = [];
                var openedMessages = [];
                var notOpenedMessages = [];

                var openedColors = [SiteSettings.colors.secondaryColors.teal,
                    SiteSettings.colors.secondaryColors.blue,
                    SiteSettings.colors.secondaryColors.lime,
                    SiteSettings.colors.secondaryColors.lavendar,
                    SiteSettings.colors.secondaryColors.orange,
                    SiteSettings.colors.secondaryColors.blueSteel,
                    SiteSettings.colors.secondaryColors.pink
                ];

                var notOpenedColors = [
                    SiteSettings.colors.secondaryColors.tealDark,
                    SiteSettings.colors.secondaryColors.blueDark,
                    SiteSettings.colors.secondaryColors.limeDark,
                    SiteSettings.colors.secondaryColors.lavendarDark,
                    SiteSettings.colors.secondaryColors.orangeDark,
                    SiteSettings.colors.secondaryColors.blueSteelDark,
                    SiteSettings.colors.secondaryColors.pinkDark
                ];

                var i = 0;
                angular.forEach(result, (item) => {
                    categories.push(item.JobDescription);
                    allMessages.push(item.AllMessagesCount);
                    openedMessages.push({ y: item.OpenedMessagesCount, color: openedColors[i] });                    
                    notOpenedMessages.push({ y: (item.AllMessagesCount - item.OpenedMessagesCount), color: notOpenedColors[i] });
                    i++;
                    i = i == 7 ? 0 : i;
                });

                // Build the chart
                this.messageStatsChart.TotalRecipients = this.chartConfigBuilder('messageStatsTotalRecipients', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        stackLabels: {
                            enabled: true,
                            style: {
                                color: 'black'
                            }
                        },
                        title: null,
                        tickInterval: allMessages.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        },
                        column: {
                            colorByPoint: false
                        }
                    },
                    series: [
                        {
                            name: 'Not Opened',
                            lineWidth: 100,
                            data: notOpenedMessages
                        },
                        {
                            name: 'Opened',
                            lineWidth: 100,
                            data: openedMessages
                        }
                    ],
                    legend: {
                        enabled: false,
                    },
                });
            }, this.onFailure);
        }

        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },


            });

            var chartOptions = {

                // Pie Chart
                messageStats: {
                    chart: {
                        height: 220
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },

                messageStatsOpened: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },

                // Column Chart
                messageStatsTotalRecipients: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },
                // Column Chart
                messageSent: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.darkDark,
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

            }
            return _.assign(chartOptions[name], options);
        }

        private toggleTabs = (activeTab: number): any => {
            this.activeTab = activeTab;
            this.activeTab == 0 ? this.getMessagesSentChart() : this.activeTab == 1 ? this.getTotalRecipientsChart() : this.getOpenedChart(); 
        };
    }

    angular.module('app').controller('Settings.MessageBuilderStatsCtrl', MessageBuilderStatsCtrl);
} 