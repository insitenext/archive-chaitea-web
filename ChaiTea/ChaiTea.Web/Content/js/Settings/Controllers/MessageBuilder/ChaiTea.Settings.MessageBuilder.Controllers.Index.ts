﻿module ChaiTea.Settings.MessageBuilder.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    interface IMessage {
        Id: number;
        ProfileImage: string;
        DepartmentName: string;
        To: string;
        Details: string;
        Title: string;
        Site: string;
        When: string;
        LabelColor: string;
        SentDate: Date
    }

    interface IResult {
        TotalRecipients: number,
        Opened: number,
        NotOpened: number
    }

    class MessageBuilderDetailsCtrl {

        messages: Array<IMessage> = [];
        modalInstance: any;
        count: number;
        departments: Array<settingsInterfaces.IDepartment> = []
        selectedDepartment: settingsInterfaces.IDepartment;
        showFilter: boolean = false;

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 10;
        
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        interimCount: number = 0;

        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.notificationsvc',
            'sitesettings',
            '$uibModal'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.messages = [];
        }

        public initialize = (): void => {
            this.selectedDepartment = {
                DepartmentId: null,
                Name: null
            };
            this.getMessageCount();
            this.getDepartments();
            this.getMessages();

            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.MessageBuilderCreated, this.$scope, (event, message:IMessage) => {
                if (message) {
                    var item = this.processMessage(message);
                    this.messages.unshift(item);
                }
            })
        }

        public getMessageCount = (): void => {
            this.apiSvc.query({},'TrainingMessage/AllCounts',false,false).then((result) => {
                if (!result) return this.$log.log('No Messages.');

                this.count = result.Count;
            }, this.onFailure);
        }

        /**
        * @description Gets and sets list of departments
        */
        private getDepartments = (): void => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Department]).then((departments): any => {
                if (!departments.length) return this.$log.log('No departments available.');

                this.departments = [];

                this.departments = departments;
            }, this.onFailure);
        }

        public getMessagesReset = (): void => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.messages = [];
            this.interimCount = 0;
            this.getMessages();
        }
        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
       * @description to save message and position
       */
        public getMessages = (): void => {
            

            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;

            var params = {
                $orderby: 'CreateDate desc',
                $top: this.top,
                $skip: this.recordsToSkip
            }
            if (this.selectedDepartment.DepartmentId) {
                params['$filter'] = `Department/DepartmentId eq ${this.selectedDepartment.DepartmentId}`;
                var departmentParams = {
                    departmentId: this.selectedDepartment.DepartmentId
                };
                this.apiSvc.query(departmentParams, '/TrainingMessage/DepartmentCounts/:departmentId', false, false).then((result) => {
                    this.count = result.Count;
                }, this.onFailure);
            }
            else {
                this.getMessageCount();
            }
            this.apiSvc.getByOdata(params, 'TrainingMessage').then((data) => {
                if (data.length === 0) {
                    this.isAllLoaded = true;
                }
                this.interimCount += data.length;
                this.apiSvc.getByOdata({ $select: 'JobId'}, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Job]).then((positions): any => {
                    if (!positions.length) return this.$log.log('No positions available.');

                    angular.forEach(data, (item) => {
                        // Filter out any messages sent to training admins
                        if (item.Jobs.length > 0) {
                            item = this.processMessage(item, positions);
                            this.messages.push(item);
                        }
                    });
                    this.isBusy = false;
                    this.recordsToSkip += this.top;

                }, this.onFailure);
                

            }, this.onFailure);
        }

        public toggleFilter = (): any => {
            this.showFilter = (this.showFilter ? false : true);
        }

        /*
        * @description process and maps variables from INotification to IMessage
        */
        private processMessage = (item, positions?) => {
            var sentTo: string = "";

            if (!positions) { 
                sentTo = "Pending...";
            }else if (item.Jobs.length == positions.length) {
                sentTo = "All Positions ";
            }
            else {
                angular.forEach(item.Jobs, (job) => {
                    sentTo = sentTo + job.Name + ",";
                });
            }
            if (item.IsCustomer) {
                sentTo += " ,Customer"
            }
            else {
                sentTo = sentTo.slice(0, -1);
            }
            item.sentTo = sentTo;
            var sites: string = "";

            angular.forEach(item.Sites, (site) => {
                sites = sites + site.Name + " ,";
            });
            item.sites = sites.slice(0, -1);


            item.when = "";
            if (moment().isSame(item.CreateDate, 'days')) {

                item.when = "Today @ " + moment(item.CreateDate).format('h:mm A');
            }
            else {
                item.when = moment(item.CreateDate).format('MMM DD @ h:mm A');
            }

            return {
                Id: item.TrainingMessageId,
                ProfileImage: this.sitesettings.defaultProfileImage,
                DepartmentName: item.Department.Name,
                To: item.sentTo,
                Details: item.Text,
                Title: item.Title,
                Site: item.sites,
                When: item.when,
                LabelColor: _.kebabCase(item.Department.Name),
                SentDate: item.CreateDate
            };
        }

        public loadsendMessage = (message: IMessage): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Settings/Templates/settings.messagebuilder.sendMessage.tmpl.html',
                controller: 'Settings.SendMessageCtrl as vm',
                size: 'sm',
                resolve: {
                    title: () => { return ''; },
                    message: () => { return message; }
                }
            });
        }

        public loadStatsForMessage = (message: IMessage): void => {
            this.apiSvc.getByOdata({ id: message.Id }, 'MessageStats/MessageStats/:id', false, false).then((result: IResult) => {
                if (result.TotalRecipients != 0) {
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Settings/messagebuilder.messagestats.tmpl.html',
                        controller: 'Common.MessageStatsCtrl as vm',
                        size: 'md',
                        resolve: {
                            message: () => { return message; },
                            result: () => { return result; },
                            fromMessagesReceived: () => { return false; }
                        }
                    });
                }
                else {
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Settings/messagebuilder.noStats.tmpl.html',
                        controller: 'Common.SendMessageCtrl as vm',
                        size: 'sm',
                        resolve: {
                            message: () => { return message; },
                            fromMessagesReceived: () => { return false; }
                        }
                    });
                }
            }, this.onFailure);
            
        }

    }

    angular.module('app').controller('Settings.MessageBuilderDetailsCtrl', MessageBuilderDetailsCtrl);
} 