﻿module ChaiTea.Settings.KPIBuilder.Controllers {

    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import settingsInterfaces = ChaiTea.Settings.Interfaces;
    import jobSvc = ChaiTea.Settings.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    
    interface IKPIResult {
        DefaultPassingMetric: number;
        IsActive: boolean;
        JobId: number;
        KpiId: number;
        Name: string;
        Description: string;
        PassingMetric: number;
        ProgramId: number;
        SiteId: number;
        SiteJobProgramKpiId: number;
        Scale: string;
        Frequency: string;
        SortOrder: number;
        MetricChanged: boolean;
    }

    interface IKpiItem {
        KpiId: number;
        Name: string;
        Summary: string;
        Description: string;
    }

    class KPIBuilderIndexCtrl {
        kpiConfiguration = [];
        positions = [];
        kpis: Array<IKpiItem> = [];
        selectedPosition: settingsInterfaces.IJob;
        hasMetrics: boolean = true;
        saveFailed: boolean = false;
        isPageDisabled: boolean = false;

        static $inject = [
            '$scope',
            '$log',
            "$window",
            'chaitea.common.services.apibasesvc',
            'sitesettings',
            'chaitea.settings.services.jobs',
            'userContext',
            'chaitea.core.services.usercontextsvc',
            '$q'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private sitesettings: ISiteSettings,
            private jobSvc: ChaiTea.Settings.Services.IJobSvc,
            private userContext: IUserContext,
            private userContextSvc: Core.Services.IUserContextSvc,
            private $q: angular.IQService) {
        }

        public initialize = (): void => {
            if (this.userContext.Site.ID) {
                this.getJobs();
                this.getKpis();
                this.selectedPosition = {
                    JobId: null,
                    Name: null
                };
            }
            else {
                this.isPageDisabled = true;
                this.userContextSvc.OpenContextPicker(true);
                return;
            }
        }

        /**
        * @description Get Jobs
        */
        public getJobs = (): void => {
            this.positions = [];
            var params = {
                clientId: this.userContext.Client.ID,
                siteId: this.userContext.Site.ID
            }
            this.apiSvc.query(params, 'LookupList/Jobs/BySite/:clientId/:siteId')
                .then((positions): any => {
                    if (positions.length == 0) return this.$log.log('No positions available.');

                    this.positions = positions;
                }, this.onFailure);
        }

        /**
        * @description Get KPIs
        */
        public getKpis = (): void => {
            this.kpis = [];
            var params = {
            }
            this.apiSvc.query(params, 'Kpi')
                .then((result): any => {
                    if (result.length == 0) return this.$log.log('No Kpis available.');

                    this.kpis = result;
                }, this.onFailure);
        }


        /**
        * @description Get the KPI Metrics
        */
        public getKPIMetrics = (): void => {
            this.kpiConfiguration = [];
            var params = {
                jobId: this.selectedPosition.JobId
            }
            this.apiSvc.query(params, "KpiSetting/KPIMetrics/:jobId").then((result) => {
                var kpiItem = <IKPIResult>{};
                    
                var kpiItems: Array<IKPIResult> = [];
                if (result.length > 0) {
                    kpiItems = result;
                    this.hasMetrics = true;
                }
                else {
                    this.hasMetrics = false;
                    for (var i = 0; i < this.kpis.length; ++i) {
                        kpiItem = <IKPIResult>{
                            DefaultPassingMetric: 0,
                            IsActive: true,
                            JobId: this.selectedPosition.JobId,
                            KpiId: null,
                            Name: null,
                            Description: '',
                            PassingMetric: 0,
                            ProgramId: null,
                            SiteId: this.userContext.Site.ID,
                            SiteJobProgramKpiId: 0,
                            Scale: '',
                            Frequency: 'Monthly',
                            SortOrder: null,
                            MetricChanged: false
                        };
                        kpiItem.KpiId = this.kpis[i].KpiId;
                        kpiItem.Name = this.kpis[i].Name;
                        kpiItems[i] = kpiItem;
                    }
                }
                angular.forEach(kpiItems, (item) => {

                    switch (item.Name) {
                        case 'Attendance':
                            kpiItem = item;
                            kpiItem.SortOrder = 1;
                            kpiItem.Description = 'Being at work on time when scheduled';
                            kpiItem.Scale = 'Total';
                            kpiItem.Frequency = 'Monthly';
                            kpiItem.MetricChanged = false;
                            break;

                        case 'Conduct':
                            kpiItem = item;
                            kpiItem.SortOrder = 2;
                            kpiItem.Description = 'Behavior & actions for a positive work environment';
                            kpiItem.Scale = 'Total';
                            kpiItem.Frequency = 'Monthly';
                            kpiItem.MetricChanged = false;
                            break;

                        case 'Complaints':
                            kpiItem = item;
                            kpiItem.SortOrder = 3;
                            kpiItem.Description = 'Customer Feedback about a service failure';
                            kpiItem.Scale = 'Total';
                            kpiItem.Frequency = 'Monthly';
                                kpiItem.MetricChanged = false;
                            break;

                        case 'Safety':
                            kpiItem = item;
                            kpiItem.SortOrder = 4;
                            kpiItem.Description = 'Work without getting hurt or hurting others';
                            kpiItem.Scale = 'Total';
                            kpiItem.Frequency = 'Monthly';
                                kpiItem.MetricChanged = false;
                            break;
                        case 'Ownership':
                            kpiItem = item;
                            kpiItem.Name = 'Report It';
                            kpiItem.SortOrder = 5;
                            kpiItem.Description = 'Taking responsibility & action for things that happen at work';
                            kpiItem.Scale = 'Total';
                            kpiItem.MetricChanged = false;
                            kpiItem.Frequency = 'Monthly';
                            break;

                        case 'Training':
                            kpiItem = item;
                            kpiItem.SortOrder = 6;
                            kpiItem.Description = 'Learing to increase job knowledge & skills';
                            if (item.SiteJobProgramKpiId == 0) {
                                item.PassingMetric = 0.8;
                            }
                            kpiItem.Scale = 'Percentage';
                            kpiItem.Frequency = 'Monthly';
                            kpiItem.MetricChanged = false;
                            break;
                        case 'Quality Audits':
                            kpiItem = item;
                            kpiItem.SortOrder = 7;
                            kpiItem.Description = 'Inspection to match customer expectations with service delivered';
                            kpiItem.Scale = 'Average';
                            kpiItem.Frequency = 'Monthly';
                            kpiItem.MetricChanged = false;
                            break;

                        case 'Professionalism':
                            kpiItem = item;
                            kpiItem.SortOrder = 8;
                            kpiItem.Description = 'Appearance, attitude, responsiveness & closets and equipment';
                            kpiItem.Scale = 'PASS/FAIL';
                            kpiItem.Frequency = 'Monthly';
                            kpiItem.MetricChanged = false;
                            break;
                    }
                    this.kpiConfiguration.push(kpiItem);
                });
                
               
                this.kpiConfiguration.sort(function (a, b) { return a.SortOrder - b.SortOrder });
            }, this.onFailure);
        }

        /**
        * @description Increment Metric value.
        */
        public incrementGoal = (index: number): void => {
            var increment: number = 1
            if (this.kpiConfiguration[index].Name == 'Quality Audits') {
                increment = 0.1;
            }
            else if (this.kpiConfiguration[index].Name == 'Training') {
                increment = 0.01;
            }
            if (this.kpiConfiguration[index].PassingMetric <= 100) {
                this.kpiConfiguration[index].PassingMetric += increment;
            }
            this.kpiConfiguration[index].MetricChanged = true;
        }

        /**
        * @description Decrement Metric value.
        */
        public decrementGoal = (index: number): void => {
            var decrement: number = 1
            if (this.kpiConfiguration[index].Name == 'Quality Audits') {
                decrement = 0.1;
            }
            else if (this.kpiConfiguration[index].Name == 'Training') {
                decrement = 0.01;
            }
            if (this.kpiConfiguration[index].PassingMetric > 0) {
                this.kpiConfiguration[index].PassingMetric -= decrement;
                if (this.kpiConfiguration[index].PassingMetric < 0) {
                    this.kpiConfiguration[index].PassingMetric = 0;
                }
            }
            this.kpiConfiguration[index].MetricChanged = true;
        }

        /**
        * @description Save kpi metrics.
        */
        public save = (): void => {
            var allPromises = [];
            var kpiConfigurationCopy = [];
            this.saveFailed = false;
            bootbox.confirm("Are you sure you want to set the metrics?", (res) => {
                if (res) {
                    kpiConfigurationCopy = this.kpiConfiguration.slice(0);
                    kpiConfigurationCopy.sort(function (a, b) { return a.KpiId - b.KpiId });
                    angular.forEach(kpiConfigurationCopy, (kpiItem) => {
                        if (kpiItem.MetricChanged || kpiItem.SiteJobProgramKpiId == 0) {
                            if (kpiItem.SiteJobProgramKpiId > 0) {
                                var httpPromise = this.apiSvc.update(kpiItem.SiteJobProgramKpiId, kpiItem, 'KpiSetting').then(() => { }, this.onSaveFailure);
                                allPromises.push(httpPromise);
                            }
                            else {
                                var httpPromise = this.apiSvc.save(kpiItem, 'KpiSetting').then(() => {}, this.onSaveFailure);
                                allPromises.push(httpPromise);
                            }

                        }

                    });
                    this.$q.all(allPromises).then(() => {
                        if (!this.saveFailed) {
                            this.hasMetrics = true;
                            bootbox.alert('KPI Metrics set successfully');
                        }
                    }, this.onFailure);
                }
            });
        }

        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }
        
        /**
        * @description Handle errors.
        */
        private onSaveFailure = (response: any): void => {
            this.saveFailed = true;
            this.$log.error(response);
        }
    }

    angular.module('app').controller('Settings.KPIBuilderIndexCtrl', KPIBuilderIndexCtrl);

} 