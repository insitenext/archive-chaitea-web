﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Settings.CourseAdmin.Controllers {
    'use strict';

    interface SelectSteps {
        Number: number;
        Name: string;
        Text: string;
    }

    interface Data {
        Id: number;
        Name: string;
        Image: string;
        InfoId: number;
        MoreInfo: string;
        Selected: boolean;
    }
    export enum Steps {
        SelectEmployeePosition = 1,
        SelectCourse = 2
    }
   
    interface ISummaryContent {
        Title: string;
        Content: Array<Data>;
        DisplayContent: boolean;
        NoOfContentItems: number;
    }

    class AssignCtrl {

        basePath: string = '';
        showSummary: boolean = true;
        currentStep: number = 1;
        steps: Array<SelectSteps> = [];
        assignByEmployee: boolean = true;
        isAssign: boolean = true;
        employeeJob: Array<Data> = [];
        employeeCourse: Array<Data> = [];
        dataItems: Array<Data> = [];
        EmployeeIds: Array<number> = [];
        JobIds: Array<number> = [];
        CourseIds: Array<number> = [];
        courses: Array<Data> = [];
        noOfSteps: number = 0;
        ClientIds: Array<number> = [];
        SiteIds: Array<number> = [];
        employeeCourseData: Array<any> = [];
        payLoad = {
            ClientIds: [],
            EmployeeIds: [],
            SiteIds: [],
            JobIds: [],
            CourseIds: []
        }
        positions: Array<Data> = [];
        selectedPositionId: number;
        enumSteps = {
            SelectEmployee: Steps.SelectEmployeePosition,
            SelectCourse: Steps.SelectCourse
        };
        selectedEmployee: any;

        summary: Array<ISummaryContent> = [];
        noOfContentItems: number = 8;
        translateOnPageLoad = [
            'ASSIGNING_COURSES_TO_EMPLOYEES_BLOCKUI',
            'ASSIGNING_COURSES_BLOCKUI',
            'COURSE_ASSIGNMENT_EMPLOYEES',
            'COURSE_ASSIGNMENT_POSITIONS',
            'SURE_REMOVE_COURSES',
            'UNASSIGNING_COURSES_TO_EMPLOYEES',
            'COURSES_REMOVED_SUCCESSFULLY'
        ];
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            '$http',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc',
            'userContext',
            'userInfo',
            Common.Services.ImageSvc.id,
            'blockUI',
            '$translate'
        ];

        constructor(
            private $scope: ng.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private imageSvc: Common.Services.IImageSvc,
            private blockUI,
            private $translate: angular.translate.ITranslateService) {
            this.basePath = sitesettings.basePath;
            if (!this.sitesettings.windowSizes.isDesktop) {
                this.showSummary = false;
            }
            var i = 0;
            angular.forEach(this.translateOnPageLoad, (data) => {
                this.$translate(data).then((translated) => {
                    data = translated;
                    this.translateOnPageLoad[i] = data;
                    i++;
                });
            });
        }

        initialize = (isAssign: boolean, isByEmployee: boolean) => {
            this.assignByEmployee = isByEmployee;
            this.isAssign = isAssign;
            this.steps = [{ Number: 1, Name: this.assignByEmployee ? 'STEP_NAME_EMPLOYEE' : 'STEP_NAME_POSITION', Text: this.assignByEmployee ? 'STEP_TEXT_EMPLOYEE' : 'STEP_TEXT_POSITION' },
                { Number: 2, Name: 'STEP_NAME_COURSE', Text: 'STEP_TEXT_COURSE' }];
            this.noOfSteps = this.steps.length;
            if (this.assignByEmployee && this.isAssign) {
                this.getEmployee();
            }
            else if (!this.isAssign) {
                this.getEmployeeCourse();
            }
            this.getPositions();
            this.summary.push({
                Title: this.assignByEmployee ? 'EMPLOYEES' : 'POSITIONS',
                Content: this.assignByEmployee ? this.employeeJob : this.positions,
                DisplayContent: true,
                NoOfContentItems: 5
            });
            this.summary.push({
                Title: 'COURSES_SUMMARY',
                Content: this.courses,
                DisplayContent: true,
                NoOfContentItems: 4
            });
        }

        public move = (isNext: boolean, selectedCount: number) => {
            if (isNext) {
                if (selectedCount > 0) {
                    this.currentStep ++;
                    this.dataItems = [];
                }
            }
            else {
                if (this.currentStep != Steps.SelectEmployeePosition) {
                    this.currentStep --;
                    this.dataItems = [];
                }
            }

            if (this.currentStep == Steps.SelectCourse) {

                if (this.isAssign) {
                    if (this.courses.length == 0) {
                        this.getCourses();
                    }
                }
                else {
                    this.courses = [];
                    this.getAssignedCourses();
                }

                this.dataItems = this.courses;
            }
            else if (this.currentStep == Steps.SelectEmployeePosition) {
                    this.dataItems = this.assignByEmployee ? this.employeeJob : this.positions;
                if (this.selectedPositionId) {
                    this.dataItems = _.where(this.dataItems, { 'InfoId': this.selectedPositionId });
                }
            }

        }

        public save = () => {
            if (this.isAssign) {
                this.saveFinish();
            }
            else {
                this.remove();
            }
        }

        public checkLastStep = (): boolean => {
            if (_.where(this.dataItems, { 'Selected': true }).length == 0) return true;
            var returnVal = false;
            returnVal = _.some(this.summary, function (item) {
                return _.where(item.Content, { 'Selected': true }).length == 0
            });
            
            return returnVal;
        }

        public getEmployee = () => {
            this.dataItems = [];
            this.apiSvc.query({}, 'TrainingEmployee/EmployeeJob').then((result) => {
                angular.forEach(result, (item) => {
                    this.employeeJob.push({
                        Id: item.OrgUserId,
                        Name: item.LastName + ', ' + item.FirstName,
                        Image: this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true),
                        InfoId: item.JobId,
                        MoreInfo: item.JobName,
                        Selected: false
                    });
                });
                this.dataItems = this.employeeJob;
            }, this.onFailure);
        }

        public getEmployeeCourse = () => {
            this.dataItems = [];
            this.apiSvc.query({}, 'EmployeeCourse/EmployeeCourseInfo').then((result) => {
                this.employeeCourseData = result;
                var uniqueEmployees = _.uniq(_.where(result, (item) => (item.Course != null)), (item: any) => (item.OrgUserId));
                angular.forEach(uniqueEmployees, (item) => {
                    this.employeeJob.push({
                        Id: item.OrgUserId,
                        Name: item.LastName + ', ' + item.FirstName,
                        Image: this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true),
                        InfoId: item.JobId,
                        MoreInfo: item.JobName,
                        Selected: false
                    });
                });
                this.dataItems = this.employeeJob;
            }, this.onFailure);
        }

        public getAssignedCourses = () => {
            var coursesByEmployee = _.where(this.employeeCourseData, { 'OrgUserId': this.selectedEmployee.Id });

            angular.forEach(coursesByEmployee, (item) => {
                this.courses.push({
                    Id: item.CourseId,
                    Name: item.CourseName,
                    Image: null,
                    InfoId: item.EmployeeCourseId,
                    MoreInfo: null,
                    Selected: false
                });
            });
            this.summary[_.findIndex(this.summary, { 'Title': 'COURSES_SUMMARY' })].Content = this.courses;
        }

        public getCourses = () => {
            var odataParams: any = {
                $select: 'CourseId, Name, CourseType, OrgClient',
                $expand: 'CourseType, OrgClient'
            };
            odataParams.$filter = `OrgClient/Id eq ${this.userContext.Client.ID} or substringof(OrgClient/Name,'SBM')`;
            this.apiSvc.getByOdata(odataParams, 'Course')
                .then((res) => {
                    if (!res) this.onFailure("No courses available.");
                    angular.forEach(res, (item) => {
                        this.courses.push({
                            Id: item.CourseId,
                            Name: item.Name,
                            Image: null,
                            InfoId: null,
                            MoreInfo: null,
                            Selected: false
                        });
                    });
                }, this.onFailure);
        }

        public getPositions = () => {
            var params = {
                clientId: this.userContext.Client.ID,
                siteId: this.userContext.Site.ID
            };
            this.apiSvc.query(params, "LookupList/Jobs/BySite/:clientId/:siteId").then((result) => {
                angular.forEach(result, (item) => {
                    this.positions.push({
                        Id: item.Key,
                        Name: item.Value,
                        Image: null,
                        InfoId: null,
                        MoreInfo: null,
                        Selected: false
                    });
                });
                if (!this.assignByEmployee) {
                    this.dataItems = [];
                    this.dataItems = this.positions;
                }
            }, this.onFailure);
        }

        public changePosition = () => {
            this.dataItems = [];
            this.dataItems = this.employeeJob;
            if (this.selectedPositionId) {
                this.dataItems = _.where(this.dataItems, { 'InfoId': this.selectedPositionId });
            }
        }

        public saveFinish = () => {
            var returnVal = false;
            returnVal = _.some(this.summary, function (item) {
                return _.where(item.Content, { 'Selected': true }).length == 0
            });
            if (!returnVal) {
                this.payLoad.ClientIds.push(this.userContext.Client.ID);
                this.payLoad.SiteIds.push(this.userContext.Site.ID);
                if (this.assignByEmployee) {
                    this.payLoad.JobIds = _.pluck(_.where(this.employeeJob, { 'Selected': true }), 'InfoId');
                }
                else {
                    this.payLoad.JobIds = _.pluck(_.where(this.positions, { 'Selected': true }), 'Id');
                }
                this.payLoad.EmployeeIds = _.pluck(_.where(this.employeeJob, { 'Selected': true }), 'Id');
                this.payLoad.CourseIds = _.pluck(_.where(this.courses, { 'Selected': true }), 'Id');

                //check if is an assign to a position or an assign to employees
                if (this.assignByEmployee) { //IS EMPLOYEE ASSIGN
                    this.blockUI.start(this.translateOnPageLoad[0]);

                    this.apiSvc.save(this.payLoad, 'TrainingEmployee/UpdateEmployeeCourses').then(() => {
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad[2]);
                        if (this.blockUI) this.blockUI.stop();
                        window.location.href = this.basePath + "Training/KnowledgeCenter";
                    }, this.onFailure);
                }
                else {
                    this.blockUI.start(this.translateOnPageLoad[1]);

                    this.apiSvc.save(this.payLoad, 'Job/UpdateMultipleJobsCourses').then(() => {
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad[3]);
                        if (this.blockUI) this.blockUI.stop();
                        window.location.href = this.basePath + "Training/KnowledgeCenter";
                    }, this.onFailure);
                }
            }
        }

        public remove = () => {
            var returnVal = false;
            returnVal = _.some(this.summary, function (item) {
                return _.where(item.Content, { 'Selected': true }).length == 0
            });
            if (!returnVal) {
                var allPromises = [];

                bootbox.confirm(this.translateOnPageLoad[4], (res) => {
                    if (res) {
                        this.blockUI.start(this.translateOnPageLoad[5]);
                        var employeeCourses = _.where(this.courses, { 'Selected': true });
                        angular.forEach(employeeCourses, (item) => {

                            var httpPromise = this.apiSvc.delete(item.InfoId, 'EmployeeCourse').then(() => {
                                this.summary[_.findIndex(this.summary, { 'Title': 'COURSES_SUMMARY' })].Content = [];
                                this.employeeCourseData.splice(_.findIndex(this.employeeCourseData, { 'EmployeeCourseId': item.InfoId }), 1);
                            }, this.onFailure);
                            allPromises.push(httpPromise);
                        });
                        this.$q.all(allPromises).then(() => {
                            if (this.blockUI) this.blockUI.stop();
                            this.notificationSvc.successToastMessage(this.translateOnPageLoad[6]);
                            this.move(false, 0);
                        }, this.onFailure);
                    }
                });
            }
        }

        public selectAll = (allSelected: boolean) => {
            if (!allSelected) {
                _.map(this.dataItems, (item) => {
                    item.Selected = true;
                    return item;
                });
            }
            else {
                _.map(this.dataItems, (item) => {
                    item.Selected = false;
                    return item;
                });
            }
        }

        public selectItem = (step: number, $index: number) => {
            if (!this.isAssign && this.currentStep == Steps.SelectEmployeePosition) {
                _.map(this.dataItems, (item) => {
                    if (item.Id != this.dataItems[$index].Id) {
                        item.Selected = false;
                        return item;
                    }
                });
                this.selectedEmployee = this.dataItems[$index];
            }
            this.dataItems[$index].Selected = !this.dataItems[$index].Selected;
        }

        public onFailure = (reason) => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(reason);
        }

        private toggleSummary = () => {
            this.showSummary = !this.showSummary;
        }

        private toggleContent = ($index) => {
            this.summary[$index].DisplayContent = !this.summary[$index].DisplayContent;
        }

        private removeDataFromContent = (parentIndex: number, childIndex: number, $event, id: number) => {
            $event.stopPropagation();
            var index = _.findIndex(this.summary[parentIndex].Content, { 'Id': id });
            this.summary[parentIndex].Content[index].Selected = false;
        }

        private showAllContent = (parentIndex: number, childIndex: number, $event) => {
            $event.stopPropagation();
            this.summary[parentIndex].NoOfContentItems = _.where(this.summary[parentIndex].Content, {'Selected' : true }).length;
        }
    }

    angular.module('app').controller('Settings.AssignCtrl', AssignCtrl);


}