﻿module ChaiTea.Quality.Interfaces {
    /**
     * DB models interfaces
     */
    export interface ICustomerRepresentative {
        CustomerRepresentativeId: number;
        Name: string;
        Email: string;
        JobDescription: string;
    }

    export interface IAuditInspectionTodo {
        TodoItemId: number;
        ScorecardDetailId: number;
        OrgUserId: number;
        CustomerRepresentativeId: number;
        Comment: string;
        DueDate: string;
        IsActive: boolean;
        FloorId: number;
    }

    export interface IAuditDetail {
        AuditDate: Date;
        AuditorId: number;
        AuditorName: string;
        Average: number;
        BuildingId: number;
        BuildingName: string;
        Comment: string;
        CustomerRepresentatives: any[];
        FloorId: number;
        FloorName: string;
        InProgress: boolean;
        IsJointAudit: boolean;
        MaximumScore: number;
        MinimumScore: number;
        ProgramId: number;
        ProgramName: string;
        ResponsibleUserId: number;
        ResponsibleUsername: string;
        ScorecardId: number;
        ScorecardSections: any[];
        SiteId: number;
        SiteName: string;
    }

    export interface IAuditProfile {
        AuditProfileId: number;
        SiteId: number;
        SiteName: string;
        BuildingId: number;
        BuildingName: string;
        FloorId: number;
        FloorName: string;
        ScoringProfileId: number;
        ScoringProfileDescription: string;
        ScoringProfileMinScore: number;
        ScoringProfileMaxScore: number;
        ResponsibleUserId: number;
        ResponsibleUsername: string;
        ProgramId: number;
        ProgramName: string;
        ProfileSectionCount: number;
    }

    export interface IAuditProfileSection {
        AuditProfileSectionId: number;
        AuditProfileId: number;
        AreaId: number;
        AreaName: string;
        AreaClassificationId: number;
        AreaClassificationName: string;
        AssignedEmployees: number[];
        InspectionItems: number[];
        Profile: IAuditProfile;
    }

    export interface IScorecardDetail {
        AuditInspectionItemName: string;
        Comment: string;
        Score?: number;
    }

    export interface IScorecardDetailTodo {
        TodoItemId?: number;
        OrgUserId?: number;
        OrgUserName?: string;
        DueDate: string;
        ScorecardDetailId: number;
        Comment: string;
        BuildingName?: string;
        FloorName?: string;
        FloorId?: number;
        IsComplete: boolean;
        ScorecardId?: number;
        CreateDate: string;
        BuildingId?: number;
        CompletedDate: string;
        IsDueDatePassed: boolean;
        ProfileImage: string;
        IsEmployeeActive: boolean;
        UserAttachment: any;
        LastSeenDate?: any
    }

    export interface IScorecardDetailImage {
        ImageId: number;
        PublicId: string;
        ScorecardDetailId: number;
    }

    export interface ILookupListOption {
        Key: number;
        Value: string;
    }

    export interface ISelectedProfile {
        newProfileType: string;
        selectedProfile: number;
        responsibleUserId: number;
    }

    export interface IAuditProfileManagerModalCtrl {
        cancel(): void;
        createNewProfile(): void;
    }

    export interface IAuditProfileSectionEmployeesModalCtrl {
        cancel(): void;
    }

    export interface ISelectPicker extends angular.IAugmentedJQuery {
        selectpicker();
    }

    export interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    export interface IKeyValuePair {
        Key: number;
        Value: string;
    }

    export interface IMyAuditsParams {
        startDate?: string;
        endDate?: string;
        employeeId?: number;
    }

    export interface IFeedbackEmployee {
        FeedbackEmployeeId?: number;
        FeedbackId: number;
        EmployeeId: number;
        IsExempt: boolean;
    }

    export interface IReRunAuditTodo {
        TodoItemId: number;
        ScorecardId: number;
        OrgUserId: number;
        Comment: string;
        DueDate: string;
        FloorId: number;
    }
} 