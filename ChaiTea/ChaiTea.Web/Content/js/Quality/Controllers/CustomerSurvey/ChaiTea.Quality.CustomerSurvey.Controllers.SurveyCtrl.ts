﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;

    class CustomerSurveySurveyCtrl {
        isSubmitSuccess: boolean = false;
        isSubmitting: boolean = false;
   
        token: string = '';
        ratingScale: number = 5;
        basePath: string;

        customerSurvey = {
            InvitationId: null,
            Questions: []
        };

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
        }

        /** @description Initializer for the controller. */
        initialize = (token: string): void => {
            this.token = token;

            if (!this.token) {
                return;
            }

            this.getSurveyQuestionsByToken(this.token);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.isSubmitting = false;
            this.notificationSvc.errorToastMessage();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.isSubmitting = false;
            this.isSubmitSuccess = true;
        }

        /**
         * @description Get the customer survey questions by token.
         * @param {string} token
         */
        getSurveyQuestionsByToken = (tokenValue: string) => {
            this.apiSvc.query({ token: tokenValue }, 'Surveys', false, false).then((result) => {
                this.customerSurvey = result;

                angular.forEach(this.customerSurvey.Questions, function (question) {
                    question.Value = null;
                });
            }, this.onFailure);
        }

        /** 
         * @description Save the customer survey response.
         */
        saveSurveyResponse = ($event): void => {
            if ($event) {
                $event.preventDefault();
            }
            var surveyResponse = {
                Token: this.token,
                Responses: _.map(this.customerSurvey.Questions, function (q) {
                    return {
                        QuestionId: q.QuestionId,
                        Value: q.Value
                    }
                })
            };
            this.isSubmitting = true;
            this.apiSvc.save(surveyResponse, 'Surveys/Responses', false).then(this.onSuccess, this.onFailure);
        };

    }
    angular.module('app').controller('Quality.CustomerSurveySurveyCtrl', CustomerSurveySurveyCtrl);
}  