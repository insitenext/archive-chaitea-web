﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;

    class CustomerSurveyEntryCtrl {
        id: number = 0;
        clientId: number = 0;
        clientName: string = '';
        siteId: number = 0;
        siteName: string = '';
        programId: number = 0;
        programName: string = '';

        basePath: string;
        isEntryDisabled: boolean = false;
        customerSurvey = [ ];

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            '$log',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;
            this.clientName = userContext.Client.Name;
            this.siteId = userContext.Site.ID;
            this.siteName = userContext.Site.Name;
            this.programId = userContext.Program.ID;
            this.programName = userContext.Program.Name;

        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.isEntryDisabled = (this.programId == 0 || this.siteId == 0);

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker').trigger('click');
                }, 500);
                return
            }

            this.apiSvc.query({}, 'Surveys/Template', false, false).then((result) => {
                this.customerSurvey = result;
            }, this.onFailure);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            mixpanel.track('Survey edited', { SurveyProgramId: this.programId });
            bootbox.alert('Customer survey questions was saved successfully!', this.redirectToDetails);
        }

        /** 
         * @description Save the customer survey.
         */
        saveSurvey = ($event): void => {
            if ($event) {
                $event.preventDefault();
            }
            this.apiSvc.update(this.programId, this.customerSurvey, 'Surveys/TemplateQuestions', false).then(this.onSuccess, this.onFailure);
        };

        /** 
         * EVENT HANDLERS
         * @description Cancel the data entry. 
         */
        cancel = ($event): void=> {
            if ($event) {
                $event.preventDefault();
            }
            this.redirectToDetails();
        }

        /** 
         * EVENT HANDLERS
         * @description Redirect to details page. 
         */
        redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/CustomerSurvey/Details');
        }
    }
    angular.module('app').controller('Quality.CustomerSurveyEntryCtrl', CustomerSurveyEntryCtrl);
} 