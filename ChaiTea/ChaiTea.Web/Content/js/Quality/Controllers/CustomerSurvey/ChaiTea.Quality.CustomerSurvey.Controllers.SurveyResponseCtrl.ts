﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonSvc = ChaiTea.Common.Services;

    class CustomerSurveyResponseModalCtrl {
        basePath: string;
        goal: number;
        invitationResponse;
        scoringScaleArray = [
            {
                icon: 'fa-bad-emoji',
                class: 'text-danger',
                value: 1,
                text: 'bad'
            },
            {
                icon: 'fa-subpar-emoji',
                class: 'text-danger',
                value: 2,
                text: 'subpar'
            },
            {
                icon: 'fa-ok-emoji',
                class: 'text-warning',
                value: 3,
                text: 'okay'
            },
            {
                icon: 'fa-good-emoji',
                class: 'text-success',
                value: 4,
                text: 'good'
            },
            {
                icon: 'fa-great-emoji',
                class: 'text-success',
                value: 5,
                text: 'great'
            },
        ]
        scoringScale;
        moduleIcon: string;
        moduleClass: string;
        modalLeftSection: string;
        modalRightSection: string;
        maxDate: Date = new Date();
        translateOnPageLoad = {
            'CONFIRM_RESEND': 'SURE_RESEND_SURVEY',
            'SUCCESS_RESEND': 'SUCCESS_RESEND_MESSAGE',
            'CONFIRM_DELETE': 'SURE_DELETE_SURVEY',
            'SUCCESS_DELETE': 'SUCCESS_DELETE_MESSAGE',
            'SUCCESS_UPDATE': 'UPDATED'
        };
   
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.reportsvc',
            '$uibModalInstance',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.alertsvc',
            '$translate',
            'id'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private alertSvc: commonSvc.IAlertSvc,
            private $translate: angular.translate.ITranslateService,
            private id: number) {

            this.basePath = sitesettings.basePath;
            this.moduleClass = 'survey';
            this.moduleIcon = 'quiz';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.survey.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.survey.rightsection.tmpl.html';

            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
            this.getControlLimit();
            if (id) {
                this.apiSvc.query({ RecipientInvitationId: id }, 'Surveys/Invitations/:RecipientInvitationId', false, false).then((result: any) => {
                    this.invitationResponse = result;
                    if (this.invitationResponse.SentDate) {
                        this.invitationResponse.SentDate = moment.utc(this.invitationResponse.SentDate).tz(userContext.Timezone.TimezoneName).format("YYYY-MM-DDTHH:mm:ss.sss");
                    }
                    this.scoringScale = this.scoringScaleArray[Math.floor(this.invitationResponse.AverageScore - 1)];
                }, this.onFailure);
            }
        }

        public close = (): void => {
            this.$uibModalInstance.close({ deleted: false, inv: this.invitationResponse });
        };

        private getControlLimit = () => {
            this.reportHelperSvc.getReportSettings().then((d) => {
                this.goal = d.SurveyControlLimit;
            });
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        private resendSurvey = () => {
            this.alertSvc.confirmWithCallback(this.translateOnPageLoad.CONFIRM_RESEND, (result) => {
                if (result) {
                    this.apiSvc.save({ recipientInvitationId: this.id, surveyUrl: `${this.sitesettings.fullDomain}/Quality/CustomerSurvey/Survey/` }, "Surveys/Invitations", false).then((response) => {
                        this.$uibModalInstance.close({ deleted: false, inv: this.invitationResponse });
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad.SUCCESS_RESEND);
                    }, this.onFailure);
                }
            });
        }

        /**
         * @description Delete Survey
         */
        private deleteSurvey = () => {
            this.alertSvc.confirmWithCallback(this.translateOnPageLoad.CONFIRM_DELETE, (result) => {
                if (result) {
                    this.apiSvc.delete(this.id, "Surveys/Invitations").then((response) => {
                        this.$uibModalInstance.close({ deleted: true, inv: null });
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad.SUCCESS_DELETE);
                    }, this.onFailure);
                }
            });
        }

        /**
         * @description Save Reporting Date
         */
        private saveReportingDate = () => {
            if (this.invitationResponse.editReportingDate != null) {
                if (this.invitationResponse.editReportingDate != this.invitationResponse.ReportingDate) {
                    var params = {
                        recipientInvitationId: this.id,
                        reportingDate: moment(this.invitationResponse.editReportingDate).format("YYYY-MM-DD")
                    };
                    this.apiSvc.update(this.id, params, "Surveys/Invitations", false).then((response) => {
                        this.invitationResponse.ReportingDate = this.invitationResponse.editReportingDate;
                        this.invitationResponse.editReportingDate = null;
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad.SUCCESS_UPDATE);
                    }, this.onFailure);
                }
            }
        }

        /** 
         * EVENT HANDLERS
         * @description Cancel the data entry. 
         */
        private cancel = ($event): void=> {
            if ($event) {
                $event.preventDefault();
            }
            this.redirectToDetails();
        }

        /** 
         * EVENT HANDLERS
         * @description Redirect to details page. 
         */
        private redirectToDetailsView = (): void => {
            window.location.replace(this.basePath + 'Quality/CustomerSurvey/Details');
        }

        /** 
         * EVENT HANDLERS
         * @description Redirect to details page. 
         */
        private redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/CustomerSurvey/');
        }
    }
    angular.module('app').controller('Quality.CustomerSurveyResponseModalCtrl', CustomerSurveyResponseModalCtrl);
} 