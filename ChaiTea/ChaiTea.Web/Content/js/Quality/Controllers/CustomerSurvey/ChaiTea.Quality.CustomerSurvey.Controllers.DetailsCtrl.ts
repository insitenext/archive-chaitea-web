﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.CustomerSurvey.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;


    class CustomerSurveyDetailCtrl {
        siteId: number = 0;
        isEntryDisabled: boolean = false;

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;

        goal;
        surveyList = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: new Date()
        };

        // Filter variables
        filterOptions = [
            { Key: 'filterByName', Value: 'Recipient Name', Options: null, FieldType: 'text' }
        ];

        filter = {
            filterType: null,
            filterCriteria: null
        };

        currentTabName: string = 'All';

        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[] = [
            {
                title: 'All',
                onClickFn: () => { this.currentTabName = 'All'; this.getSurveysReset(); }
            },
            {
                title: 'Completed',
                onClickFn: () => { this.currentTabName = 'Completed'; this.getSurveysReset(); }
            },
            {
                title: 'Pending',
                onClickFn: () => { this.currentTabName = 'Pending'; this.getSurveysReset(); }
            }
        ];

        isFilterCollapsed: boolean = true;
        localStorageSurveyFilterKey: string;
        
        viewLink: string = "";
        module: string = "surveys";
        headerText: string = "SURVEY_DATE";
        clientName: string = '';

        static $inject = [
            '$scope',
            '$rootScope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            'userInfo',
            '$log',
            '$uibModal',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $rootScope: angular.IRootScopeService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day').toDate();
               
                // Reset the paging
                this.getSurveysReset({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });

            this.localStorageSurveyFilterKey = 'survey-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;

            //setting filters from session storage.
            var filterObj = this.localDataStoreSvc.getObject(this.localStorageSurveyFilterKey);
            if (filterObj) {
                this.filter = filterObj;
                this.isFilterCollapsed = false;
            }
        }

        /** @description Initializer for the controller. */
        initialize = (): void => { }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.CustomerSurveyResponseModalCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res.deleted) {
                    var index = _.findIndex(this.surveyList, { 'RecipientInvitationId': id });
                    if (index != -1) {
                        this.surveyList.splice(index, 1);
                    }
                } else {
                    var index = _.findIndex(this.surveyList, { 'RecipientInvitationId': id });
                    if (index != -1) {
                        if (res.inv.AverageScore) {
                            res.inv.AverageScore = res.inv.AverageScore.toFixed(2);
                        }
                        var comment = _.filter(res.inv.Responses, { 'AnswerType': 'TEXT' });
                        if (comment) {
                            res.inv.Comment = comment[0];
                        }
                        this.surveyList[index] = res.inv;
                    }
                }
            });
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging.
         */
        getSurveysReset = (dateRange?: commonInterfaces.IDateRange) => {
            this.surveyList = [];
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.getControlLimit();
            this.getSurveys(dateRange);            
        }

        private getControlLimit = () => {
            this.reportHelperSvc.getReportSettings().then((d) => {
                this.goal = d.SurveyControlLimit;
            });
        }

        /**
         * @description Get data and push to array for infinite scroll.
         */
        private getSurveys = (dateRange?: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Quality/CustomerSurvey/SurveyResponse/';
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            };
            
            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;

            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                top: this.top,
                skip: this.recordsToSkip,
                filterByName: '',
                filterByCompletion: null
            };

            

            if (this.filter && this.filter.filterCriteria) {
                params.filterByName = this.filter.filterCriteria
            }

            if (this.currentTabName == 'Completed') {
                params.filterByCompletion = true;
            }
            else if (this.currentTabName == 'Pending') {
                params.filterByCompletion = false;
            }

            this.apiSvc.query(params,'Surveys', false, true).then((data) => {
                if (data.length == 0) {
                    this.isAllLoaded = true;
                }

                angular.forEach(data, (obj) => {
                    if (obj.AverageScore) {
                        obj.AverageScore = obj.AverageScore.toFixed(2);
                    }
                    var comment = _.filter(obj.Responses, { 'AnswerType': 'TEXT' });
                    obj.Comment = comment[0];
                    this.surveyList.push(obj);
                });
                this.recordsToSkip += this.top;
                this.isBusy = false;
            }, this.onFailure);
        }

        /**
        * @description to export customer surveys details to excel sheet 
        */
        public export = (): void => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.startDate)),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'days')),
                filterByName: '',
                filterByCompletion: null
            };

            if (this.filter && this.filter.filterCriteria) {
                params.filterByName = this.filter.filterCriteria
            }

            if (this.currentTabName == 'Completed') {
                params.filterByCompletion = true;
            }
            else if (this.currentTabName == 'Pending') {
                params.filterByCompletion = false;
            }

            this.apiSvc.query(params, 'Surveys', false, true).then((exports) => {                
                if (exports.length == 0) {                    
                    return;
                }
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Bold: "1" } } },
                };
                var surveysToExport = [];
                for (var i = 0; i < exports.length; i++) {
                    var surveyDate = exports[i].ReportingDate ? moment(exports[i].ReportingDate) : null;
                    var sentDate = exports[i].SentDate ? moment(exports[i].SentDate) : null;
                    var completedDate = exports[i].SubmittedDate ? moment(exports[i].SubmittedDate) : null;
                    surveysToExport.push({
                        "RecipientName": exports[i].Recipient.Name,
                        "Client": exports[i].Recipient.Survey.ClientName,
                        "Site": exports[i].Recipient.Survey.SiteName,
                        "Program": exports[i].Recipient.Survey.ProgramName,
                        "SurveyDate": surveyDate ? surveyDate.format('MMM D') + "," + surveyDate.format('YYYY HH:mm:ss') : '--',
                        "SentDate": sentDate ? sentDate.format('MMM D') + "," + sentDate.format('YYYY HH:mm:ss') : '--',
                        "CompletedDate": completedDate ? completedDate.format('MMM D') + "," + completedDate.format('YYYY HH:mm:ss') : '--',
                        "Comments": exports[i].Responses.length > 0 ? exports[i].Responses[4].Value.replace(/\r?\n|\r/g, " ") : '',
                        "Score": exports[i].AverageScore ? Math.round(exports[i].AverageScore * 100) / 100 : 'N/A'
                    });
                }

                alasql('SELECT * INTO CSV("surveydetails.csv",?) FROM ?', [mystyle, surveysToExport]);

            }, this.onFailure);            
        }
    }

    angular.module('app').controller('Quality.CustomerSurveyDetailCtrl', CustomerSurveyDetailCtrl);
}  