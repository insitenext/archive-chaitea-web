﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.CustomerSurvey.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;

    class CustomerSurveyIndexCtrl {
        clientName: string = '';
        siteId: number = 0;
        programId: number = 0;
        goal: number;
        currentDate = new Date();
        failColor: string = '#48c101';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: new Date()
        };
        basePath: string;
        chartSettings: IChartSettings;

        valueFormatter = val => val.toFixed(2);

        valueFormatterInt = val => val.toFixed(0);

        reportsConfig = {
            control: 0,
            scale: 5,
            counts: {
                surveyTrendsCurrentMonth: { averages: 0, count: 0, outOf: 5, control: 0 },
                surveyTrends: { count: 0, outOf: 5, control: 0 },
                surveyTrendsBySite: { count: 0, outOf: 0, control: 0 },
                surveyTrendsByProgram: { count: 0, outOf: 0, control: 0 }
            },

            surveyTrends: {},
            surveyTrendsBySite: {},
            surveyTrendsByProgram: {},
            recentSurveys: []
        };
        noOfMonths: number = 0;
        pendingCount: number = 0;
        checkSiteValue: boolean = true;
        checkProgramValue: boolean = true;

        module: string = "surveys";
        viewLink: string = "";
        headerText: string = 'SURVEY_DATE';
        
        static $inject = [
            '$scope',
            '$filter',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            '$uibModal',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private lookuplistSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private notificationSvc: commonSvc.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.clientName = userContext.Client.Name;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.reportHelperSvc.getReportSettings().then((d) => {

                    this.reportsConfig.control = d.SurveyControlLimit;

                    //set defaults
                    this.reportsConfig.surveyTrends = this.chartConfigBuilder('surveyTrends', {});
                    this.reportsConfig.surveyTrendsBySite = this.chartConfigBuilder('surveyTrendsBySite', {});
                    this.reportsConfig.surveyTrendsByProgram = this.chartConfigBuilder('surveyTrendsByProgram', {});

                    //get chart data
                    this.refreshCharts({
                        startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                        endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                    });

                });

            });

        }

        /** @description Initializer for the controller. */
        public initialize = (): void => { 

        }

        public ShowEntryViewModal = (id: number): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.CustomerSurveyResponseModalCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res.deleted) {
                    var index = _.findIndex(this.reportsConfig.recentSurveys, { 'RecipientInvitationId': id });
                    if (index != -1) {
                        this.reportsConfig.recentSurveys.splice(index, 1);
                    }
                } else {
                    if (res.inv.AverageScore) {
                        res.inv.AverageScore = res.inv.AverageScore.toFixed(2);
                    }
                    var index = _.findIndex(this.reportsConfig.recentSurveys, { 'RecipientInvitationId': id });
                    if (index != -1) {
                        this.reportsConfig.recentSurveys[index] = res.inv;
                    }
                }
            });;
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        private getControlLimit = () => {
            this.reportHelperSvc.getReportSettings().then((d) => {
                this.goal = d.SurveyControlLimit;
            }, this.onFailure);
        }
        
        private getPendingCount = (dateRange: commonInterfaces.IDateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1,'days').endOf('day'))
            };
            this.apiSvc.query(params, 'Surveys/PendingCount', false, false).then((result) => {
                this.pendingCount = result.PendingCount;
            }, this.onFailure);
        }

        /**
         * @description Survey Trends
         */
        private getSurveyTrends = (dateRange: commonInterfaces.IDateRange) => {
            this.apiSvc.query({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }, 'Surveys/SurveyTrends', false, false).then((result) => {
                if (!result && result.Categories.length == 0) {
                    return;
                }
                var control = this.reportsConfig.control,
                    categories = result.Categories,
                    averages = result.Averages,
                    totalSurveyCount = result.TotalSurveyCount;

                var tempAvg = _.without(_.slice(averages), null);
                var avg = 0;
                if (tempAvg.length > 0){
                    avg = _.reduce(tempAvg, function (memo: number, num: number) {
                        return memo + num;
                    }, 0) / tempAvg.length;
                }

                this.reportsConfig.counts.surveyTrendsCurrentMonth.count = totalSurveyCount;
                this.reportsConfig.counts.surveyTrendsCurrentMonth.control = control;
                this.reportsConfig.counts.surveyTrendsCurrentMonth.outOf = 5;
                this.reportsConfig.counts.surveyTrendsCurrentMonth.averages = avg;

                var maxForFloor = <number>_.max(result.Categories) + 1;
                if (maxForFloor < this.reportsConfig.control) {
                    maxForFloor = this.reportsConfig.control + 1;
                }
                this.reportsConfig.surveyTrends = this.chartConfigBuilder('surveyTrends', {
                    yAxis: {
                        minRange: maxForFloor,
                        min: 0,
                        max: this.reportsConfig.scale,
                        title: '',
                        tickInterval: 1,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        plotLines: [{
                            value: control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: 4,
                            zIndex: 4,
                            label: { text: ''}
                        }],
                    },
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [{
                            name: 'Survey Score',
                            lineWidth: 100,
                            data: averages
                            }, {
                            name: 'Goal',
                            marker: {
                                symbol: 'circle'
                            },
                            type: 'scatter'
                        }],
                    legend: { enabled: false }
                });

                // Set counts
                this.reportsConfig.counts.surveyTrends.count = totalSurveyCount;
                this.reportsConfig.counts.surveyTrends.control = control;
                this.reportsConfig.counts.surveyTrends.outOf = 5;

            }, this.onFailure);
        }

        /**
         * @description Site Surveys
         */
        private getSurveysBySite = (dateRange: commonInterfaces.IDateRange) => {

            this.apiSvc.query({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }, 'Surveys/SurveyTrendsBySite', false, false).then((result) => {
                if (!result.Categories.length) {
                    return;
                }
                var tempAvg = result.Averages.slice();
                var averages = result.Averages,
                    categories = result.Categories,
                    control = this.reportsConfig.control;

                var temp = _.pull(tempAvg, null);
                this.checkSiteValue = temp.length == 0 ? false : true;

                this.reportsConfig.surveyTrendsBySite = this.chartConfigBuilder('surveyTrendsBySite', {
                    xAxis: {
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                    },
                    yAxis: [
                        {
                            tickInterval: 1,
                            min: 0,
                            max: 5,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                            title: {
                                text: ''
                            },
                            plotLines: [{
                                value: control,
                                color: this.sitesettings.colors.coreColors.warning,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }],
                        }, {
                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    series: [{
                        name: 'Survey Score',
                        data: _.map(categories,(obj, i) => {
                            return {
                                name: obj,
                                y: averages[i],
                                color: averages[i] < control ? this.failColor : this.sitesettings.colors.secondaryColors.teal
                            }
                        })
                    }],
                    height: categories.length == 3 ? categories.length * 100 : categories.length * 50
                });

    
                // Set counts
                this.reportsConfig.counts.surveyTrendsBySite.outOf = categories.length;
                this.reportsConfig.counts.surveyTrendsBySite.control = control;
            });
        }

        /**
         * @description Program Surveys
         */
        private getSurveysByProgram = (dateRange: commonInterfaces.IDateRange) => {

            this.apiSvc.query({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }, 'Surveys/SurveyTrendsByProgram', false, false).then((result) => {
                if (!result) {
                    return;
                }
                var tempAvg = result.Averages.slice();
                var temp = _.pull(tempAvg, null);
                this.checkProgramValue = temp.length == 0 ? false : true;

                var control = this.reportsConfig.control,
                    categories = result.Categories,
                    averages = result.Averages;

                this.reportsConfig.surveyTrendsByProgram = this.chartConfigBuilder('surveyTrendsByProgram', {
                    xAxis: {
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                    },
                    yAxis: [
                        {
                            tickInterval: 1,
                            min: 0,
                            max: 5,
                            title: {
                                text: ''
                            },
                            plotLines: [{
                                value: control,
                                color: this.sitesettings.colors.coreColors.warning,
                                width: 4,
                                zIndex: 4,
                                label: {
                                    text: '',
                                    style: {
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }
                            }],
                        }, {

                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    series: [{
                        name: 'Survey Score',
                        data: _.map(categories,(obj, i) => {
                            return {
                                name: obj,
                                y: averages[i],
                                color: averages[i] < control ? this.failColor : this.sitesettings.colors.secondaryColors.teal
                            }
                        })
                    }],
                    height: categories.length == 3 ? categories.length * 100 : categories.length * 50
                });

                // Set counts
                this.reportsConfig.counts.surveyTrendsByProgram.outOf = categories.length;
                this.reportsConfig.counts.surveyTrendsByProgram.control = control;


            });
        }

        /**
         * @description Get 4 latest survey responses.
         */
        private getRecentSurveyResponses = (): void => {
            this.reportsConfig.recentSurveys = [];
            this.viewLink = this.basePath + "Quality/CustomerSurvey/SurveyResponse/";

            var sDate = this.dateFormatterSvc.formatDateFull(this.dateRange.startDate);
            var nextDay = new Date(this.dateFormatterSvc.formatDateFull(this.dateRange.endDate));
            nextDay.setDate(nextDay.getDate() + 1);
            var eDate = this.dateFormatterSvc.formatDateFull(nextDay.setHours(0, 0, 0, 0));

            var param = {
                startDate: sDate,
                endDate: eDate,
                top: 5,
                skip: 0,
                filterByName: '',
                filterByCompletion: true
            };

            this.apiSvc.query(param, 'Surveys', false, true).then((data) => {
                angular.forEach(data, (obj) => {
                    if (obj.AverageScore) {
                        obj.AverageScore = obj.AverageScore.toFixed(2);
                    }
                    var comment = _.filter(obj.Answers, { 'AnswerType': 'TEXT' });
                    obj.Comment = comment[0];
                    this.reportsConfig.recentSurveys.push(obj);
                });
            }, this.onFailure);
            
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getSurveyTrends(dateRange);
            this.getSurveysBySite(dateRange);
            this.getSurveysByProgram(dateRange);
            this.getRecentSurveyResponses();
            this.getPendingCount(dateRange);
            this.getControlLimit();
        }

        /**
         * @description Defines overridable chart options.
         */
        private chartConfigBuilder = (name, options) => {
            var chartOptions = {
                surveyTrends: {
                    title: {
                        text: '',
                    },

                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },

                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {

                        series: {
                            width: 20,
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 1,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: 4,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                    },
                    xAxis: {
                        tickColor: 'white',
                        categories: []
                    },
                    series: [{
                        name: 'Survey Score',

                        lineWidth: 100,
                        data: []
                    }, {
                            name: 'Goal',
                            marker: {
                                symbol: 'circle'
                            },

                            type: 'scatter',
                        }]
                },
                surveyTrendsBySite: {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: [],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },

                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        //itemWidth: 250,
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',

                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }

                    },

                    yAxis: [{
                        tickInterval: 1,
                        min: 0,
                        max: 5,

                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },

                        lineWidth: 1,
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: 4,
                            zIndex: 4,
                            label: { text: '' }
                        }],


                    }, {

                            linkedTo: 0,
                            opposite: true,
                            allowDecimals: false,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            }
                        }],
                    plotOptions: {
                        min: 80,
                        max: 100,
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth

                        }
                    },
                    series: [{
                        name: 'Surveys Score',
                        data: []
                    }]
                },
                surveyTrendsByProgram: {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: [],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },

                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }

                    },

                    yAxis: [{
                        tickInterval: 1,
                        min: 0,
                        max: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: 4,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                    }, {

                            linkedTo: 0,
                            opposite: true,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            }
                        }],
                    plotOptions: {
                        min: 80,
                        max: 100,
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth
                        }
                    },
                    series: [{
                        name: 'Survey Score',
                        data: []
                    }]
                }
            }
            return _.assign(chartOptions[name], options);
        }
    }
    angular.module('app').controller('Quality.CustomerSurveyIndexCtrl', CustomerSurveyIndexCtrl);
}  
