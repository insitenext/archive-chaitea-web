﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.ToDo.Controllers{
    'use strict';

    import coreSvc = Core.Services;
    import qualitySvc = Quality.Services;
    import qualityInterfaces = Quality.Interfaces;

    interface ITodoTabCounts {
        Open: number;
        Closed: number;
        PastDue: number;
        ClosedOnTime: number;
    }

    interface EmployeeInfo extends ITodoTabCounts{
        OrgUserId: number;
        OrgUserName: string;
        ProfileImage: string;
        IsOrgUserActive: boolean;
    }

    class ToDoIndexCtrl {
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        basePath: string = '';
        viewLink: string = '';
        isEmployee: boolean = false;
        isSiteDisabled: boolean = true;
        todos = [];
        tempTodos = [];
        noOfMonths: number = 0;
        tabCounts = <ITodoTabCounts>{
            Open: 0,
            Closed: 0,
            PastDue: 0,
            ClosedOnTime: 0
        };
        todoCharts = {
            counts: {
                monthlyTrend: {
                    pastDue: 0,
                    closedOnTime: 0,
                    count: 0
                }
            },
            MonthlyAverage: {},
            MonthlyTrend: {}
        };
        employeeInfo: EmployeeInfo = {
            OrgUserId: 0,
            OrgUserName: '',
            Open: 0,
            Closed: 0,
            ClosedOnTime: 0,
            PastDue: 0,
            ProfileImage: '',
            IsOrgUserActive: true
        };
        employeeList: Array<EmployeeInfo> = [];
        minDate: Date = null;
        headerText: string = 'ASSIGNED';
        clientName: string = '';
        siteName: string = '';

        static $inject = [
            '$scope',
            '$q',
            '$filter',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.todosvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            '$timeout',
            'blockUI',
            '$location',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private toDoSvc: qualitySvc.IToDoSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $timeout: angular.ITimeoutService,
            private blockUI,
            private $window: angular.IWindowService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc
        ) {
            this.basePath = sitesettings.basePath;
            this.siteName = userContext.Site.Name;
            this.clientName = userContext.Client.Name;
            this.isEmployee = this.userInfo.mainRoles.employees;
            this.isSiteDisabled = !(userContext.Site && userContext.Site.ID > 0);
            this.todoCharts.MonthlyAverage = this.chartConfigBuilder('todoMonthlyAverage', {});
            this.todoCharts.MonthlyTrend = this.chartConfigBuilder('todoMonthlyTrend', {});
            this.minDate = new Date();
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.getRecentToDos();
                this.getTabCounts();
                this.getEmployeeInfo();
                this.geMonthlyTrends();
            });
        }

        private initialize = (): void => {
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ToDoEntryViewModalCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            });
        }

        private getRecentToDos = (): void => {
            this.tempTodos = [];
            this.todos = [];
            this.viewLink = this.basePath + 'Quality/ToDo/EntryView/?id=';
            this.blockUI.start('Loading...');

            var endPoint = "TodoItem/ByEmployee";
            if (!this.isEmployee) {
                endPoint = "ToDoItem"
            }
            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dateRange.startDate));
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dateRange.endDate));
            var params = {
                $top: 5,
                $skip: 0,
                $orderby: 'CreateDate desc',
                $filter: `CreateDate ge DateTime'${startDate}' and CreateDate le DateTime'${endDate}'`
            };
            this.apiSvc.getByOdata(params, endPoint).then((result): any => {
                angular.forEach(result, (todo) => {
                    var newTodo: qualityInterfaces.IScorecardDetailTodo = todo;
                    if (todo.IsComplete) {
                        newTodo.IsDueDatePassed = new Date(todo.DueDate).getTime() < new Date(todo.UpdateDate).getTime();
                    }
                    newTodo.DueDate = this.dateSvc.getFromUtcDatetimeOffset(todo.DueDate, 'MM/DD/YYYY h:mm A');
                    newTodo.CreateDate = this.dateSvc.getFromUtcDatetimeOffset(todo.CreateDate, 'MM/DD/YYYY h:mm A');
                    newTodo.CompletedDate = this.dateSvc.getFromUtcDatetimeOffset(todo.UpdateDate, 'MM/DD/YYYY h:mm A');
                    if (todo.LastSeenDate) {
                        newTodo.LastSeenDate = this.dateSvc.getFromUtcDatetimeOffset(todo.LastSeenDate, 'MM/DD/YYYY h:mm A');
                    }
                    newTodo.ProfileImage = todo.UserAttachment ? this.imageSvc.getUserImageFromAttachments(todo.UserAttachment, true) : SiteSettings.defaultProfileImage;
                    this.tempTodos.push(newTodo);
                });

                //Sort by CompletedDate for Closed todos 
                this.todos = this.tempTodos.sort(function (a, b) {
                    return new Date(b.CreateDate).getTime() - new Date(a.CreateDate).getTime()
                });

                this.$timeout(() => {
                    if (this.blockUI) this.blockUI.stop();
                }, 1000);

            }, this.onFailure);
        } 

        private getTabCounts = () => {
            if (this.isEmployee) {
                var employeeParams = {
                    startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                    employeeId: this.userInfo.userId
                };
                this.apiSvc.query(employeeParams, 'TodoItem/EmployeeCounts', false, false).then((result: ITodoTabCounts) => {
                    this.tabCounts = result;
                    this.getMonthlyAverageChart();
                }, this.onFailure);
            }
            else {
                var params = {
                    startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day'))
                };
                this.apiSvc.query(params, 'TodoItem/AllCounts', false, false).then((result: ITodoTabCounts) => {
                    this.tabCounts = result;
                    this.getMonthlyAverageChart();
                }, this.onFailure);
            }
        }

        private getMonthlyAverageChart = (): void => {
                var data = [];
                data.push({
                    name: 'Closed on Time', y: this.tabCounts.ClosedOnTime
                });
                data.push({
                    name: 'Past Due', y: this.tabCounts.PastDue + (this.tabCounts.Closed - this.tabCounts.ClosedOnTime)
                });

                // Build the chart
                this.todoCharts.MonthlyAverage = this.chartConfigBuilder('todoMonthlyAverage', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Montlhy Average',
                            innerSize: '60%',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['name'],
                                    sliceY = d['y'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                }
                            })
                        }],
                });
        }

        private geMonthlyTrends = () => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
            };
            this.apiSvc.query(params, "TodoItem/MonthlyTrend/:startDate/:endDate").then((result) => {
                if (!result) return;

                var categories = [];
                var closedOnTime = [];
                var pastDue = [];
                _.forEach(result, (item: any) => {
                    categories.push(moment().month(item.Month - 1).year(item.Year).format('MMM \'YY'));
                    closedOnTime.push(item.ClosedOnTime);
                    pastDue.push(item.PastDue);
                });
                //Build the chart
                this.todoCharts.MonthlyTrend = this.chartConfigBuilder('todoMonthlyTrend', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'PAST DUE',
                            lineWidth: 100,
                            data: pastDue,
                            legendIndex: 1,
                            color: this.sitesettings.colors.secondaryColors.blue
                        },
                        {
                            name: 'CLOSED ON TIME',
                            lineWidth: 100,
                            data: closedOnTime,
                            legendIndex: 0,
                            color: this.sitesettings.colors.secondaryColors.teal

                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });

                //Set onTime count
                this.todoCharts.counts.monthlyTrend.closedOnTime = <number>_.reduce(closedOnTime, function (sum, n) {
                    return <number>sum + <number>n;
                }) || 0;

                //Set onTime count
                this.todoCharts.counts.monthlyTrend.pastDue = <number>_.reduce(pastDue, function (sum, n) {
                    return <number>sum + <number>n;
                }) || 0;
                this.todoCharts.counts.monthlyTrend.count = this.todoCharts.counts.monthlyTrend.closedOnTime + this.todoCharts.counts.monthlyTrend.pastDue;
            }, this.onFailure);
        }

        private getEmployeeInfo = (): void => {
            this.employeeList = [];
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day'))
            };
            this.apiSvc.query(params, 'TodoItem/EmployeeInfo/:startDate/:endDate').then((result) => {
                angular.forEach(result, (item) => {
                    this.employeeInfo = item;
                    this.employeeInfo.ProfileImage = item.UserAttachment ? this.imageSvc.getUserImageFromAttachments(item.UserAttachment, true) : SiteSettings.defaultProfileImage;
                    this.employeeList.push(this.employeeInfo);
                });
            }, this.onFailure);
        }


        private chartConfigBuilder = (name, options) => {
            var chartOptions = {
                todoMonthlyAverage: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%'
                        }
                    ]
                },
                todoMonthlyTrend: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: '',
                            lineWidth: 100
                        }
                    ]
                },
            }
            return _.assign(chartOptions[name], options);
        }
    }
    angular.module('app').controller('Quality.ToDoIndexCtrl', ToDoIndexCtrl);
}