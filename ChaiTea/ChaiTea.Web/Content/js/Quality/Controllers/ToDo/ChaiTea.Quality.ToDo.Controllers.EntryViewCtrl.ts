﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import qualitySvc = Quality.Services;
    import coreSvc = Core.Services;
    import commonInterfaces = Common.Interfaces;
    import commonSvc = Common.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import qualityInterfaces = Quality.Interfaces;

    interface IEmployeeWithImage {
        Name: string;
        ProfileImage: string;
        JobDescription: string;
    }

    interface IUserAttachment {
        UserAttachmentType: string;
        UniqueId: string;
    }

    enum FileType { Undefined, ProfilePicture };

    class ToDoEntryViewModalCtrl {
        todoItemId: number = 1;
        createdDate: any;
        dueDate: any;
        updateDate: any;
        buildingName: string;
        floorName: string;
        comment: string;
        employeeName: string;
        employeeId: number;
        employeeImage: string;
        todo: qualityInterfaces.IScorecardDetailTodo;
        top: number = 20;
        recordsToSkip: number = 0;
        bbRes: boolean;
        createByName: string;
        LastSeenDate: any;
        basePath: string;
        filter = {
            filterType: null,
            filterCriteria: null
        };
        modalInstance;
        isEmployee: boolean = false;

        employeeWithImage: IEmployeeWithImage;
        employeesWithImage = [];
        selectedEmployeeText = '';
        moduleIcon: string;
        moduleClass: string;
        modalLeftSection: string;
        modalRightSection: string;
        translateOnPageLoad = {
            'ToDo': 'TO_DO_STATIC',
            'CONFIRM': 'DELETE_CONFIRMATION',
            'SUCCESS': 'DELETE_SUCCESS',
            'CUSTOMERREP': 'REPRESENTATIVE',
            'ASSIGNEDTO': 'Assigned To'
        };

        IsComplete: boolean = false;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            '$uibModal',
            "chaitea.quality.services.employeesvc",
            'chaitea.quality.services.complaintssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.quality.services.todosvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.notificationsvc',
            '$uibModalInstance',
            'chaitea.common.services.awssvc',
            '$translate',
            Common.Services.DateSvc.id,
            'id'
        ];

        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private complaintsSvc: qualitySvc.IComplaintsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private toDoSvc: qualitySvc.IToDoSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private $translate: angular.translate.ITranslateService,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private id: number) {
            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;
            this.isEmployee = this.userInfo.mainRoles.employees;

            
            this.moduleClass = 'todo';
            this.moduleIcon = 'tasks2';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.todo.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.todo.rightsection.tmpl.html';

            this.todoItemId = id;
            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
            this.selectedEmployeeText = this.translateOnPageLoad.ASSIGNEDTO;
            this.getTodoById(this.todoItemId);
            mixpanel.track("Viewed a ToDo", { TodoItemId: id });
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        getTodoById = (todoItemId: number): void=> {
                if (todoItemId > 0) {
                    var endPoint = "TodoItem/ByEmployee";
                    if (!this.isEmployee) {
                        endPoint = "ToDoItem"
                    }
                    this.apiSvc.getById(todoItemId, endPoint).then((result) => {
                        this.todo = result;
                        this.createdDate = this.dateSvc.getFromUtcDatetimeOffset(result.CreateDate, 'MM/DD/YYYY h:mm A');
                        this.dueDate = this.dateSvc.getFromUtcDatetimeOffset(result.DueDate, 'MM/DD/YYYY h:mm A');
                        this.updateDate = this.dateSvc.getFromUtcDatetimeOffset(result.UpdateDate, 'MM/DD/YYYY h:mm A');
                        this.buildingName = result.BuildingName;
                        this.floorName = result.FloorName;
                        this.employeeName = result.OrgUserName;
                        this.employeeId = result.OrgUserId;
                        this.comment = result.Comment;
                        this.createByName = result.CreateByName;
                        if (result.LastSeenDate) {
                            this.LastSeenDate = this.dateSvc.getFromUtcDatetimeOffset(result.LastSeenDate, 'MM/DD/YYYY h:mm A');
                        }
                        this.IsComplete = result.IsComplete;
                        var defaultImg = "~/Content/img/user.jpg";
                        this.employeeWithImage = <IEmployeeWithImage>{
                            Name: result.OrgUserName,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(result.UserAttachment, true)
                        }
                        if (result.IsOrgUserActive) {
                            this.getEmployeeWithImage(result.OrgUserId).then(() => {
                                this.employeesWithImage.push(this.employeeWithImage);
                            });
                        }
                        else {
                            this.employeesWithImage.push(this.employeeWithImage);
                        }

                    }, this.onFailure);
                }

        }

        private getEmployeeWithImage = (id: number): any => {

            return this.apiSvc.getById(id, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                this.employeeWithImage.JobDescription = employee.JobDescription.Name;
                
            }, (error) => {
                if (error.status == Common.Enums.HTTP_STATUS_CODES.NOT_FOUND) {
                    this.employeeWithImage.JobDescription = this.translateOnPageLoad.CUSTOMERREP
                }
                else {
                    this.onFailure(error);
                }
            });
        }

        private getAttachment = (attachment: any): string => {
            var img: string;
            if (attachment.length > 0)
                img = this.awsSvc.getUrlByAttachment(attachment[0].UniqueId, attachment[0].AttachmentType);

            return img;
        }

        private reRunAudit = (): void => {
            this.$uibModalInstance.dismiss('cancel');
            this.apiSvc.save({ FloorId: this.todo.FloorId, InProgress: true, ParentScorecardId: this.todo.ScorecardId }, 'ScoreCard', false).then((result) => {
                window.location.href = this.basePath + 'Quality/Audit/Entry/' + result.ScorecardId;
            });
        }

        editToDo = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + "Content/js/Common/Templates/modal.tmpl.html",
                controller: 'EditToDoCtrl as vm',
                size: 'md',
                resolve: {
                    todo: () => { return this.todo }
                }
            });
        }

        public closeToDo = (): void => {
            this.alertSvc.confirmWithCallback('Are you sure you want to close the to-do?',(bbRes): void => {
                if (bbRes) {
                        this.todo.IsComplete = bbRes;

                        var todo = this.todo;
                        if (this.isEmployee) {
                            this.apiSvc.update(todo.TodoItemId, todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem] + '/PutByEmployee').then((res) => {
                                this.gotoDetailsPage();
                                mixpanel.track("Closed Todo by employee", { TodoItemId: todo.TodoItemId });
                            }, this.onFailure);
                        } else {
                            this.apiSvc.update(todo.TodoItemId, todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem]).then((res) => {
                                this.gotoDetailsPage();
                                mixpanel.track("Closed Todo", { TodoItemId: todo.TodoItemId });
                            }, this.onFailure);
                        }
                    }
            });
        }

        deleteToDo = (): void => {
            this.alertSvc.confirmWithCallback(this.translateOnPageLoad.CONFIRM + ' ' + this.translateOnPageLoad.ToDo + '?', (bbRes): void => {
                if (bbRes) {
                        this.apiSvc.delete(this.todoItemId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem]).then((result) => {
                            this.$uibModalInstance.close(true);
                            this.notificationSvc.successToastMessage(this.translateOnPageLoad.ToDo + ' ' + this.translateOnPageLoad.SUCCESS);
                        }, this.onFailure);
                    }
            });

        }

        gotoDetailsPage = (): void => {
            window.location.replace(this.basePath + 'Quality/Todo/');
        }

        private getPagingParams = () => {
            var params: commonInterfaces.IODataPagingParamModel = {
                $top: this.top,
                $skip: this.recordsToSkip,
                $orderby: 'DueDate desc',
                $filter: 'TodoItemId eq ' + this.todoItemId
            };

            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                var filter = `${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                params['$filter'] = filter;
            }

            return params;
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

    }

    class EditToDoCtrl {
        modalTitle: string = "Edit To-Do";
        modalBodyTemplate: string;
        modalFooterTemplate: string;

        static $inject = ['$scope',
                          'sitesettings',
                          '$uibModalInstance',
                          'chaitea.common.services.apibasesvc',
                          '$window',
                          'todo',
                          'chaitea.quality.services.todosvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $window: angular.IWindowService,
            private todo: qualityInterfaces.IScorecardDetailTodo,
            private toDoSvc: qualitySvc.IToDoSvc) {

            this.modalBodyTemplate = this.sitesettings.basePath + "Content/js/Quality/Templates/ToDo/quality.todo.edit.modal.body.tmpl.html";
            this.modalFooterTemplate = this.sitesettings.basePath + "Content/js/Quality/Templates/ToDo/quality.todo.edit.modal.footer.tmpl.html";
        }

        saveToDo = (): void => {
                this.apiSvc.update(this.todo.TodoItemId, this.todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem]).then((result) => {
                    this.cancel();
                    this.$window.location.reload();
                });
            }
        cancel = (): void => {
            this.$uibModalInstance.dismiss();
        }

    }

    angular.module('app').controller('ToDoEntryViewModalCtrl', ToDoEntryViewModalCtrl);
    angular.module('app').controller('EditToDoCtrl', EditToDoCtrl);
}
