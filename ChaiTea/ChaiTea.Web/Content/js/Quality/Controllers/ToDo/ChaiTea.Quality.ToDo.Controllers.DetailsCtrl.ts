﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.ToDo.Controllers {
    'use strict';

    import qualitySvc = Quality.Services;
    import coreSvc = Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import qualityInterfaces = Quality.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface ITodoTabCounts {
        Open: number;
        Closed: number;
        PastDue: number;
    }

    enum Tabs {
        Open,
        Closed,
        PastDue
    }

    enum TodoFilter {
        Source = 1,
        FailedInspectionItem = 2,
        ReAudit = 3
    }

    class ToDoDetailsCtrl {

        toDos: Array<qualityInterfaces.IScorecardDetailTodo> = [];                
        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        isSiteDisabled: boolean = true;
        siteId: number = 0;

        sortBy: string = 'desc';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };


        filterByOptions = [
            { Key: 'BuildingId', Value: 'Building', Options: null, FieldType: 'select' },
            {
                Key: TodoFilter.Source,
                Value: 'Source',
                Options: [
                    {
                        Key: TodoFilter.FailedInspectionItem,
                        Value: 'Failed Inspection Items'
                    }
                ],
                FieldType: 'select'
            }
        ];

        basePath: string;
        boolFilter: boolean = true;
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[];

        filter = {
            filterType: null,
            filterCriteria: null
        };
        isFilterCollapsed: boolean = true;
        localStorageTodoFilterKey: string;

        modalInstance;
        isEmployee: boolean = false;

        activeTab: number = 0;
        tabCounts =  <ITodoTabCounts>{
            Open: 0,
            Closed: 0,
            PastDue: 0
        };
        filterActiveEmployee: boolean = false;
        filterActiveBuilding: boolean = false;

        isCardView: boolean = true;
        module: string = 'toDos';
        viewLink: string = '';
        headerText: string = 'ASSIGNED';
        clientName: string = '';
        siteName: string = '';
        isReAudit: boolean = null;
        filterObj: any;
        static $inject = [
            '$scope',
            '$q',
            '$filter',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.todosvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.apibasesvc',
            '$uibModal',
            '$timeout',
            'chaitea.quality.services.lookuplistsvc',
            'blockUI',
            '$location',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private toDoSvc: qualitySvc.IToDoSvc,
            private lookuplistSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $timeout: angular.ITimeoutService,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private blockUI,
            private $window: angular.IWindowService,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private imageSvc: commonSvc.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            this.modalInstance = $uibModal;
            this.siteId = userContext.Site.ID;
            this.basePath = sitesettings.basePath;
            this.isEmployee = this.userInfo.mainRoles.employees;
            this.isSiteDisabled = !(userContext.Site && userContext.Site.ID > 0);
            this.siteName = userContext.Site.Name;
            this.clientName = userContext.Client.Name;
            
            if (!this.isEmployee) {
                this.filterByOptions.unshift({ Key: 'OrgUserId', Value: 'Employee', Options: null, FieldType: 'select' });
            }
            this.localStorageTodoFilterKey = 'todo-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;
            this.filterObj = this.localDataStoreSvc.getObject(this.localStorageTodoFilterKey);
            if (this.filterObj) {
                this.filter = this.filterObj;
                this.isFilterCollapsed = false;
            }
        }

        public initialize = (): void => {
            //Autofire todo modal from mainMenu
            if (window.location.search.indexOf('createNew') > 0) {
                setTimeout(() => {
                    angular.element('[to-do-list]').first().trigger('click');
                }, 500);
            }

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                // Reset the paging
                if (this.isReAudit == null) {
                    this.addReRunAuditFilter();
                }
                else {
                    this.getTodosReset();
                }
            });
            this.getFilterOptions();

            this.tabFilterItems = [
                {
                    title: 'Open',
                    onClickFn: () => { this.boolFilter = false; this.toggleActiveTab(0) },
                    count: 0
                },
                {
                    title: 'Closed',
                    onClickFn: () => { this.boolFilter = false; this.toggleActiveTab(1) },
                    count: 0
                },
                {
                    title: 'Open Past Due',
                    onClickFn: () => { this.boolFilter = false; this.toggleActiveTab(2) },
                    count: 0
                }
            ];

            this.$scope.$watch(()=>(this.tabCounts), (newVal:any, oldVal:any) => {
                if (newVal === oldVal) return false;
                
                this.tabFilterItems[0].count = this.tabCounts['Open'];
                this.tabFilterItems[1].count = this.tabCounts['Closed'];
                this.tabFilterItems[2].count = this.tabCounts['PastDue'];

            },true);

        }

        private addReRunAuditFilter = () => {
            this.apiSvc.query({}, 'AuditSetting', false, true).then((result) => {
                this.isReAudit = (result.length && _.any(result, { ReRunAuditsInternalAudit: true })) || false;
                if (this.isReAudit) {
                    var obj = _.find(this.filterByOptions, { 'Key': TodoFilter.Source });
                    obj.Options.push({
                        Key: TodoFilter.ReAudit,
                        Value: 'Re-Audits'
                    });
                }
                this.checkFilters();
                this.getTodosReset();
            }, this.onFailure);
        }

        private checkFilters = () => {
            if (this.filterObj && this.filterObj.filterCriteria == TodoFilter.ReAudit && !this.isReAudit) {
                this.filter.filterType = null;
                this.filter.filterCriteria = null;
            }
        }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ToDoEntryViewModalCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.toDos, { 'TodoItemId': id });
                    if (index != -1) {
                        this.toDos.splice(index, 1);
                        if (this.activeTab == Tabs.Open) {
                            this.tabCounts.Open--;
                        }
                        else if (this.activeTab == Tabs.PastDue) {
                            this.tabCounts.PastDue--;
                        }
                    }
                }
            });
        }

        private getTabCounts = () => {
            if (this.isEmployee || this.filterActiveEmployee) {
                var employeeParams = {
                    startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                    employeeId: this.filterActiveEmployee ? this.filter.filterCriteria : this.userInfo.userId,
                    filterByFailedInspectionItem: this.filter.filterCriteria == TodoFilter.FailedInspectionItem ? true : false,
                    buildingId: this.filterActiveBuilding ? this.filter.filterCriteria : null,
                    filterByReAudit: this.filter.filterCriteria == TodoFilter.ReAudit ? true : false
                };
                this.apiSvc.query(employeeParams, 'TodoItem/EmployeeCounts',false,false).then((result: ITodoTabCounts) => {
                    this.tabCounts = result;
                    this.filterActiveBuilding = false;
                    this.filterActiveEmployee = false;
                }, this.onFailure);
            }
            else {
                var params = {
                    startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'day')),
                    filterByFailedInspectionItem: this.filter.filterCriteria == TodoFilter.FailedInspectionItem ? true : false,
                    buildingId: this.filterActiveBuilding ? this.filter.filterCriteria : null,
                    filterByReAudit: this.filter.filterCriteria == TodoFilter.ReAudit ? true : false
                };
                this.apiSvc.query(params, 'TodoItem/AllCounts',false,false).then((result: ITodoTabCounts) => {
                    this.tabCounts = result;
                    this.filterActiveBuilding = false;
                    this.filterActiveEmployee = false;
                }, this.onFailure);
            }

        }

        /**
         * @description Retrieves todos from service
         */
        public getTodos = (): void => {
            this.viewLink = this.basePath + 'Quality/ToDo/EntryView/?id=';
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            this.blockUI.start('Loading...');

            var param = this.getPagingParams(this.top, this.recordsToSkip);
            var endPoint = "TodoItem/ByEmployee";
            if (!this.isEmployee) {
                endPoint = "ToDoItem/ToDos"
            }

            this.apiSvc.getByOdata(param, endPoint).then((result): any => {
                angular.forEach(result, (todo) => {
                    var newTodo: qualityInterfaces.IScorecardDetailTodo = todo;
                    if (todo.IsComplete) {
                        newTodo.IsDueDatePassed = new Date(todo.DueDate).getTime() < new Date(todo.UpdateDate).getTime();
                    }
                    newTodo.DueDate = this.dateSvc.getFromUtcDatetimeOffset(todo.DueDate, 'MM/DD/YYYY h:mm A');
                    newTodo.CreateDate = this.dateSvc.getFromUtcDatetimeOffset(todo.CreateDate, 'MM/DD/YYYY h:mm A');
                    newTodo.CompletedDate = this.dateSvc.getFromUtcDatetimeOffset(todo.UpdateDate, 'MM/DD/YYYY h:mm A');
                    if (todo.LastSeenDate) {
                        newTodo.LastSeenDate = this.dateSvc.getFromUtcDatetimeOffset(todo.LastSeenDate, 'MM/DD/YYYY h:mm A');
                    }
                    newTodo.ProfileImage = todo.UserAttachment ? this.imageSvc.getUserImageFromAttachments(todo.UserAttachment, true) : SiteSettings.defaultProfileImage;
                    this.toDos.push(newTodo);
                });

                if (!result.length) {
                    this.isAllLoaded = true;
                }
                
                this.isBusy = false;
                this.recordsToSkip += this.top;

                this.$timeout(() => {
                    if (this.blockUI) this.blockUI.stop();
                }, 1000);
            }, this.onFailure);
        }

        /**
         * @description Gets the total record count for export functionality
         */
        public getExportableRecordCount = (): number => {
            var total = 0;
            switch (this.activeTab) {
                case 0:
                    total = this.tabCounts.Open;
                    break;
                case 1:
                    total = this.tabCounts.Closed;
                    break;
                case 2:
                    total = this.tabCounts.PastDue;
                    break;
            }
            return total;
        }

        private updateActiveFilter = () => {
            if (this.filter.filterCriteria && this.filter.filterType && this.filter.filterCriteria != TodoFilter.FailedInspectionItem) {
                if (this.filter.filterType == 'BuildingId') {
                    this.filterActiveBuilding = true;
                }
                else if (this.filter.filterType == 'OrgUserId'){
                    this.filterActiveEmployee = true;
                }
            }
        }

        /**
         * @description Pops modal to confirm action
         */
        public closeToDo = ($e: UIEvent, index: number, todo: qualityInterfaces.IScorecardDetailTodo): void => {
            bootbox.confirm('Are you sure you want to close the to-do?', (bbRes): void => {

                if (bbRes) {
                    if (!todo.ScorecardDetailId) {
                        todo.IsComplete = bbRes;
                        //Update if an Employee
                        if (this.isEmployee) {
                            this.apiSvc.update(todo.TodoItemId, todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem] + '/PutByEmployee').then((res) => {
                                this.toDos.splice(index, 1);
                                mixpanel.track("Closed Todo by employee", { TodoItemId: todo.TodoItemId });
                                this.updateActiveFilter();
                                this.getTabCounts();
                            }, this.onFailure);
                        } else {
                            this.apiSvc.update(todo.TodoItemId, todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem]).then((res) => {
                                this.toDos.splice(index, 1);
                                mixpanel.track("Closed Todo", { TodoItemId: todo.TodoItemId });
                                this.updateActiveFilter();
                                this.getTabCounts();
                            }, this.onFailure);
                        }
                    }
                    else {
                        todo.IsComplete = bbRes;
                        this.toDoSvc.updateToDo(todo.TodoItemId, todo).then((res) => {
                            this.toDos.splice(index, 1);
                            mixpanel.track("Closed Audit Todo", { TodoItemId: todo.TodoItemId });
                            this.updateActiveFilter();
                            this.getTabCounts();
                        }, this.onFailure);
                    }
                }
            });
        }

        /**
        * @description Return a paging param for the OData filter.
        */
        private getPagingParams = (top, skip) => {
            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dateRange.startDate));
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dateRange.endDate));
            var todaysDate = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.dateFormatterSvc.formatDateFull(moment()));

            var orderBy = '';
            if (!this.boolFilter)
                orderBy = 'UpdateDate desc';
            else
                orderBy = 'DueDate asc';
            var params: any = {
                filterByFailedInspectionItem: this.filter.filterCriteria == TodoFilter.FailedInspectionItem ? true : false,
                $top: top,
                $skip: skip,
                $orderby: orderBy,
                $filter: `IsComplete ne ${this.boolFilter} and CreateDate ge DateTime'${startDate}' and CreateDate le DateTime'${endDate}'` +
                         (this.activeTab == 1 ? `` :
                    (this.activeTab == 0 ? ` and DueDate ge DateTime'${todaysDate}'`
                                         : ` and DueDate lt DateTime'${todaysDate}'`))
            };

            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                if (this.filter.filterCriteria != TodoFilter.FailedInspectionItem) {
                    var filter = '';
                    if (this.filter.filterCriteria === TodoFilter.ReAudit) {
                        filter = ` and ScorecardId ne null`;
                    }
                    else {
                        filter = ` and ${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                    }
                    params['$filter'] += filter;
                }
                //saving filter in session storage
                this.localDataStoreSvc.setObject(this.localStorageTodoFilterKey, this.filter);
            }
            else {
                //remove filter from session storage
                this.localDataStoreSvc.setObject(this.localStorageTodoFilterKey, '');
                this.isFilterCollapsed = true;
            }

            //Filter for Employee tasks only
            if (this.isEmployee) {
                params['$filter'] = params['$filter'] + ` and OrgUserId eq ${this.userInfo.userId} `;
            }

            return params;
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
       * @description to export complaint details to excel sheet 
       */
        private export = (): void => {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
                     
            var top = this.getExportableRecordCount();
            var param = this.getPagingParams(top, 0);
            var endPoint = "TodoItem/ByEmployee";
            if (!this.isEmployee) {
                    endPoint = "ToDoItem"
            }

            this.apiSvc.getByOdata(param, endPoint).then((result): any => {
                var exportableToDos = [];
                var tempArray = [];
                    angular.forEach(result, (todo) => {
                        var newTodo: qualityInterfaces.IScorecardDetailTodo = todo;
                        newTodo.DueDate = this.dateSvc.getFromUtcDatetimeOffset(todo.DueDate);
                        newTodo.IsDueDatePassed = new Date(todo.DueDate).getTime() < new Date(todo.UpdateDate).getTime();
                        newTodo.CompletedDate = todo.UpdateDate;
                        newTodo.ProfileImage = todo.UserAttachment ? this.imageSvc.getUserImageFromAttachments(todo.UserAttachment, true) : SiteSettings.defaultProfileImage;
                        exportableToDos.push(newTodo);
                    });

                    //Sort by CompletedDate for Closed todos 
                    if (!this.boolFilter) {
                        tempArray = exportableToDos.sort(function (a, b) {
                            return new Date(b.CompletedDate).getTime() - new Date(a.CompletedDate).getTime()
                        });
                    }
                    else if (this.sortBy == 'asc') {
                        tempArray = exportableToDos.sort(function (a, b) {
                            return new Date(a.DueDate).getTime() - new Date(b.DueDate).getTime()
                        });
                    }
                    else {
                        tempArray = exportableToDos.sort(function (a, b) {
                            return new Date(b.DueDate).getTime() - new Date(a.DueDate).getTime()
                        });
                    }
                    this.isBusy = false;
                    this.$timeout(() => {
                        if (this.blockUI) this.blockUI.stop();
                    }, 1000);

                    var recordsToExport = [];
                    for (var i = 0; i < tempArray.length; i++) {
                        recordsToExport.push({
                            "Name": tempArray[i].OrgUserName, "Created Date": tempArray[i].CreateDate, "Description": tempArray[i].Comment.replace(/\r?\n|\r/g, " "),
                            "Building": tempArray[i].BuildingName, "Floor": tempArray[i].FloorName, "Due Date": tempArray[i].DueDate
                        });
                    }
                    alasql('SELECT * INTO CSV("to-do-details.csv",?) FROM ?', [mystyle, recordsToExport]);                    
            }, this.onFailure);                                                
        }

        private getTodosReset = () => {
            this.isBusy = false;
            this.toDos = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.updateActiveFilter();
            this.getTabCounts();
            this.getTodos();
        }

        private getFilterOptions = (): angular.IPromise<any> => {
            var lookupList = [
                this.apiSvc.getLookupList('LookupList/Employees', true)
            ];

            //Add building to filterOptions if a site has been chosen.
            if (this.siteId) {
                lookupList.push(this.apiSvc.getLookupList('LookupList/Buildings', true));
            }

            return this.$q.all(lookupList).then((result: Array<any>) => {
                if (!this.isEmployee) {
                    this.filterByOptions[0].Options = result[0].Options;

                    if (this.siteId)
                        this.filterByOptions[1].Options = result[1].Options;
                }
                else {
                    this.filterByOptions[0].Options = result[1].Options;
                }
            });
        }

        private getTodoScroll = () => {
            this.getTodos();
        }

        private toggleActiveTab = (activeTab: number) => {
            this.activeTab = activeTab;
            this.boolFilter = this.activeTab == 0 || this.activeTab == 2 ? true : false; 
            this.toDos = [];
            this.isAllLoaded = false;
            this.recordsToSkip = 0;            
            this.getTodos();
        }
    }

    angular.module('app').controller('Quality.ToDoDetailsCtrl', ToDoDetailsCtrl);
}