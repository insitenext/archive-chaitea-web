﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import qualitySvc = Quality.Services;
    import coreSvc = Core.Services;
    import commonInterfaces = Common.Interfaces;
    import commonSvc = Common.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    class ToDoViewCtrl {

        isEmployee = true;
        minDate: Date = new Date();
        options = {
            buildings: [],
            floors: [],
            employees: []
        };
        datepickers = {
            auditDate: false
        };
        todo: qualityInterfaces.IScorecardDetailTodo;

       
        static $inject = [
            '$scope',
            '$q',
            '$filter',
            'blockUI',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.reportssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.quality.services.todosvc',
            '$log',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $filter: angular.IFilterService,
            private blockUI,
            sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: qualitySvc.IReportsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private todoSvc: qualitySvc.IToDoSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private notificationSvc: commonSvc.INotificationSvc) {

        }

        public initialize = (): void => {
            this.getAllLookuplist();
        }

        /**
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): void => {
            this.lookupListSvc.getBuildings().then((data) => {
                this.options.buildings = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getEmployees().then((data) => {
                this.options.employees = data.Options;
            }, this.onFailure);
        } 

        /** 
         * @description Get floors based on buildingId. 
         * @param {number} buildingId
         */
        getFloorsByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                this.blockUI.start('Loading Floors...');
                this.lookupListSvc.getFloors({ id: buildingId }).then((data) => {
                    this.options.floors = data.Options;
                    if (this.blockUI) this.blockUI.stop();
                }, this.onFailure);
            }
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(response);
        }

        /**
         * @description Open date picker.
         */
        openDatePicker = ($event, which): void => {
            $event.preventDefault();
            $event.stopPropagation();
            this.datepickers[which] = true;
        }
    }

    angular.module('app').controller('Quality.ToDoViewCtrl', ToDoViewCtrl);
} 