﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Complaint.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    //export interface IComplaintIndexCtrl {
    //    initialize(): void;
    //    onFailure(response: any): void;
    //    getComplaintTrends(dateRange: commonInterfaces.IDateRange): void;
    //    getComplaintsByType(dateRange: commonInterfaces.IDateRange): void;
    //    getComplaintTrendsBySite(dateRange: commonInterfaces.IDateRange): void;
    //    refreshCharts(dateRange: commonInterfaces.IDateRange): void;
    //}

    interface ISiteResult {
        category: string;
        data: number;
    }

    class ComplaintIndexCtrl {
        failColor: string = '#EF5C48';
        currentDate = new Date();

        clientId: number = 0;
        programId: number = 0;
        siteId: number = 0;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        basePath: string;

        employeesWithAttachment = [];

        chartSettings: IChartSettings;

        goalLineWidth = 0;

        valueFormatter = val => val.toFixed(0);

        /**
         * Define chart data object format for all the charts.
         */
        chartsConfig = {
            control: 0, // goalLine
            minScale: 0,
            scale: 100,

            //controlLimit: 50,
            repeatComplaintsInScope: 0,
            repeatComplaintsOutOfScope: 0,
            openInScopeComplaintCount: 0,
            totalInScopeComplaintCount: 0,
            totalComplaints: 0,

            counts: {
                complaintTrends: { total: 0, inScope: 0, outScope: 0, open: 0 }
            },


            complaintTrends: {},
            complaintsByType: {
                series: []
            },
            complaintTrendsBySite: {},
            complaintTrendsByType: null,
            recentComplaints: []
        };
        noOfMonths: number = 0;
        module: string = "complaint";
        headerText: string = "COMPLAINT_DATE";
        viewLink: string = "";
        clientName: string = "";

        static $inject = [
            '$scope',
            '$filter',
            'userInfo',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.complaintssvc',
            'chaitea.quality.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$uibModal',
            'chaitea.core.services.usercontextsvc'];
        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private complaintsSvc: qualitySvc.IComplaintsSvc,
            private chartsSvc: qualitySvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContextSvc: Core.Services.IUserContextSvc) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.clientId = userContext.Client.ID;
            this.programId = userContext.Program.ID;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;

            //Show goal line if a Site is chosen
            //if (userContext.Site.ID) {
                this.goalLineWidth = 4;
            //}

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.setReportsConfig().then(() => {
                    this.refreshCharts({
                        startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                        endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                    });
                });
            });

        }

        /** @description Initializer for the controller. */
        initialize = (): void => {

        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
       * @description get all supervisor employees
       */
        private getEmployees = (): void => {
            this.apiSvc.get(serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees): any => {

                angular.forEach(employees,(employee) => {
                    this.employeesWithAttachment.push(<commonInterfaces.IEmployeeWithAttachment>{
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                });
            });
        }

        /**
         * @description Set global reports configuration.
         */
        private setReportsConfig = (): angular.IPromise<any> => {

            //All Sites: combine goal lines
            if (!this.siteId) {
                return this.reportHelperSvc.getAllSitesCombinedSiteReportSettings()
                    .then((d: commonInterfaces.IReportSettings) => {
                            return this.chartsConfig.control = d.ComplaintControlLimit;
                    }, this.handleFailure);
            } else {
                return this.reportHelperSvc.getReportSettings().then((d) => {
                    this.chartsConfig.control = d.ComplaintControlLimit;
                }, this.handleFailure);
            }
        }

        /**
         * @description Get complaint trends and update the complaint trends model.
         */
        private getComplaintTrends = (dateRange: commonInterfaces.IDateRange): void=> {
            this.chartsSvc.getComplaintTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var totalCombinedComplaints = 0;
                var inScopeTotalCurrentMonth = (result.InScopeData.length) ? result.InScopeData[result.InScopeData.length - 1] : 0;
                var outOfScopeTotalCurrentMonth = (result.OutOfScopeData.length) ? result.OutOfScopeData[result.OutOfScopeData.length - 1] : 0;

                //get highest value and make sure enough room for Goal Line
                var combinedData = _.union(result.InScopeData, result.OutOfScopeData);
                var maxForFloor = <number>_.max(combinedData) + 2;
                if (maxForFloor < this.chartsConfig.control)
                    maxForFloor = this.chartsConfig.control + 2;

                this.chartsConfig.complaintTrends = this.chartConfigBuilder('complaintTrends', {
                    totalComplaints: <number>_.reduce(result.InScopeData.concat(result.OutOfScopeData), function (sum, n) {
                        return <number>sum + <number>n;
                    }),
                    totalInScopeComplaints: inScopeTotalCurrentMonth,
                    totalOutofScopeComplaints: outOfScopeTotalCurrentMonth,
                    repeatInScopeComplaints: 0,
                    repeatOutOfScopeComplaints: 0,
                    inScopeTotalForDateRange: result.InScopeComplaintCount,
                    outOfScopeTotalForDateRange: result.OutOfScopeComplaintCount,
                    categories: result.Categories,
                    yAxis: {
                        minRange: maxForFloor,
                        plotLines: [{
                            value: this.chartsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: {
                                text: '',
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        }],
                    },
                    xAxis: {
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'IN SCOPE',
                            lineWidth: 3,
                            data: result.InScopeData
                        },
                        {
                            name: 'OUT OF SCOPE',
                            lineWidth: 3,
                            data: result.OutOfScopeData,
                        }
                    ],

                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -15,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });

                this.chartsConfig.counts.complaintTrends.total = <number>_.reduce(result.InScopeData.concat(result.OutOfScopeData), function (sum, n) {
                    return <number>sum + <number>n;
                });
                this.chartsConfig.counts.complaintTrends.open = <number>_.reduce(result.OpenInScopeData.concat(result.OpenOutOfScopeData), function (sum, n) {
                    return <number>sum + <number>n;
                });
                this.chartsConfig.counts.complaintTrends.outScope = result.OutOfScopeComplaintCount;
            });
        }

        /**
         * Get in scope and out of scope repeat complaints for current month.
         */
        private getTotalRepeatComplaintsForCurrentMonth = (): void => {
            this.chartsSvc.getComplaintTrends({
                startDate: this.dateFormatterSvc.formatDateShort(moment(new Date()).startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(new Date())
            }).then((complaintsResult) => {
                var repeatInScopeData = complaintsResult.RepeatInScopeData[0];
                var repeatOutOfScopeData = complaintsResult.RepeatOutOfScopeData[0];
                var openInScopeComplaintCount = complaintsResult.OpenInScopeData[0];

                this.chartsConfig.repeatComplaintsInScope = repeatInScopeData;
                this.chartsConfig.repeatComplaintsOutOfScope = repeatOutOfScopeData;
                this.chartsConfig.openInScopeComplaintCount = openInScopeComplaintCount;
                this.chartsConfig.totalInScopeComplaintCount = complaintsResult.InScopeComplaintCount;
                this.chartsConfig.totalComplaints = this.chartsConfig.totalInScopeComplaintCount + complaintsResult.OutOfScopeComplaintCount;

            }, this.onFailure);
        }

        /**
         * @description Get complaints by type and update the complaints by type model.
         */
        private getComplaintsByType = (dateRange: commonInterfaces.IDateRange): void=> {
            var $trendsDiv = $("#complaintTrendsByType").closest("div.parent-container");
            $trendsDiv.hide();
            this.chartsSvc.getComplaintsByType({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                result.Data = result.Data.sort(function (a, b) {
                    var nameA = a.name.toLowerCase()
                    var nameB = b.name.toLowerCase()
                    if (nameA < nameB)
                        return -1;
                    if (nameA > nameB)
                        return 1;
                    return 0;
                });

                if (result.Data.length > 0) {
                    var maxElement = result.Data[0];

                    /* need to correct and refactor */
                    this.chartsSvc.getComplaintTrendsByTypeOrProgram({
                        trendId: maxElement.id,
                        startDate: dateRange.startDate,
                        endDate: dateRange.endDate
                    }).then((result) => {
                        if ($trendsDiv.is(":hidden")) {
                            $trendsDiv.show();
                        }

                        this.chartsConfig.complaintTrendsByType = {
                            totalComplaints: result.InScopeComplaintCount,
                            categories: result.Categories,
                            series: [
                                {
                                    name: maxElement.name.toUpperCase() + ' TREND',
                                    lineWidth: 3,
                                    data: result.InScopeData,
                                    events: {
                                        legendItemClick: () => (false)
                                    }
                                }
                            ],
                            isLegendEnabled: true,
                            legend: {
                                enabled: true,
                                align: 'right',
                                borderWidth: 0,
                                verticalAlign: 'top',
                                floating: true,
                                x: 0,
                                y: -10,
                                itemStyle: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                        };
                    });

                    $.each(result.Data,(idx, elt) => {
                        if (idx == 0) {
                            elt.sliced = true;
                            elt.selected = true;
                        }
                        elt.marker = {
                            enabled: true,
                            symbol: 'circle'
                        };
                        elt.events = {
                            click: (arg) => {
                                this.chartsSvc.getComplaintTrendsByTypeOrProgram({
                                    trendId: elt.id,
                                    startDate: dateRange.startDate,
                                    endDate: dateRange.endDate
                                }).then((result) => {
                                    if ($trendsDiv.is(":hidden")) {
                                        $trendsDiv.show();
                                    }

                                    this.chartsConfig.complaintTrendsByType = {
                                        totalComplaints: result.InScopeComplaintCount,
                                        categories: result.Categories,
                                        series: [
                                            {
                                                color:$(arg.srcElement).attr("fill"),
                                                name: elt.name.toUpperCase() + ' TREND',
                                                lineWidth: 3,
                                                data: result.InScopeData,
                                                events: {
                                                    legendItemClick: () => (false)
                                                }
                                            }
                                        ],
                                        isLegendEnabled: true,
                                        legend: {
                                            enabled: true,
                                            align: 'right',
                                            borderWidth: 0,
                                            verticalAlign: 'top',
                                            floating: true,
                                            x: 0,
                                            y: -10,
                                            itemStyle: {
                                                fontSize: '12px',
                                                fontWeight: '600',
                                            }
                                        },
                                    };
                                });
                            }
                        };
                    });
                    this.chartsConfig.complaintsByType = {
                        series: [
                            {
                                type: 'pie',
                                innerSize: '60%',
                                name: 'Complaint type share',
                                data: result.Data,
                            }
                        ]
                    };
                } else {
                    //DW: fix to update visibility of 'Complaints by Program' charts and legend without refresh.
                    this.chartsConfig.complaintsByType.series = [];
                }
            }, this.onFailure);
        }

        /**
         * @description Get trend chart based on legend click.
         */
        public loadTrend = (id: number, color: string, name: string): void => {
            var $trendsDiv = $("#complaintTrendsByType").closest("div.parent-container");
            $trendsDiv.hide();
            var startDate = this.dateFormatterSvc.formatDateShort(this.dateRange.startDate);
            var endDate = this.dateFormatterSvc.formatDateShort(this.dateRange.endDate);
            this.chartsSvc.getComplaintTrendsByTypeOrProgram({
                trendId: id,
                startDate: startDate,
                endDate: endDate
            }).then((result) => {
                if ($trendsDiv.is(":hidden")) {
                    $trendsDiv.show();
                }

                this.chartsConfig.complaintTrendsByType = {
                    totalComplaints: result.InScopeComplaintCount,
                    categories: result.Categories,
                    series: [
                        {
                            color: color,
                            name: name.toUpperCase() + ' TREND',
                            lineWidth: 3,
                            data: result.InScopeData,
                            events: {
                                legendItemClick: () => (false)
                            }
                        }
                    ],
                    isLegendEnabled: true,
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                };
            });
        }

        /**
         * @description Get complaint trends by site and update the complaint trends by site model.
         */
        private getComplaintTrendsBySite = (dateRange: commonInterfaces.IDateRange): void=> {
            this.chartsSvc.getComplaintTrendsBySite({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var totalZeroComplaints = 0;
                if (result.Data.length) {

                    var inScope = { Keys: [], Categories: [], Data: [] };
                    var all = { Keys: [], Categories: [], Data: [] };
                    var resultant: Array<ISiteResult> = [];

                   
                    var i: number;
                    for (i = 0; i < result.Data.length; ++i){
                        resultant.push({ category: result.Categories[i], data: result.Data[i] });
                    }
                    resultant = this.sortArrayDescending(resultant);
                  
                    inScope.Categories = this.getInScopeCategories(resultant);
                    inScope.Data = this.getInScopeData(resultant);
                    all.Categories = this.getAllCategories(resultant);
                    all.Data = this.getAllData(resultant);

                    var totalZero = this.getAllInstanceInArray(result.Data, 0);
                    totalZeroComplaints = totalZero.length;

                    //get highest value and make sure enough room for Goal Line
                    //var combinedData = _.union(result.InScopeData, result.OutOfScopeData);

                    var maxForFloor = <number>_.max(result.Data) + 5;
                    if (maxForFloor < this.chartsConfig.control)
                        maxForFloor = this.chartsConfig.control + 5;

                    if (maxForFloor < 10) maxForFloor = 10;

                    //USES chartConfigBuilder
                    this.chartsConfig.complaintTrendsBySite = this.chartConfigBuilder('complaintTrendsBySite', {
                        totalObjects: <number>result.Data.length, // either number of sites or buildings depending on the view
                        totalZeroComplaints: totalZeroComplaints,
                        displayChart: totalZeroComplaints != <number>result.Data.length,
                        xAxis: {
                            categories: <any[]>inScope.Categories,
                            labels: {
                                useHTML: true,
                                formatter: function () {
                                    var labelArr = this.value.split(';');
                                    return "<span>" + labelArr[0] + "</span>";
                                },
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                        },
                        yAxis: {
                            floor: 0,
                            minRange: maxForFloor,
                            plotLines: [
                                {
                                    value: this.chartsConfig.control,
                                    color: this.sitesettings.colors.coreColors.warning,
                                    width: this.goalLineWidth,
                                    zIndex: 4,
                                    label: { text: '' }
                                }
                            ],
                            title: null,
                            allowDecimals: false,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        },
                        series: [
                            {
                                name: 'IN SCOPE',
                                data: <any[]>inScope.Data,
                                color: this.sitesettings.colors.secondaryColors.teal,
                                dataLabels: {
                                    enabled: true,
                                    color: '#fff',
                                    inside: true,
                                    shadow: false
                                },
                                events: {
                                    legendItemClick: function () {
                                        var seriesIndex = this.index;
                                        var series = this.chart.series;

                                        series[0].chart.axes[0].categories = <any[]>inScope.Categories;

                                        for (var i = 0; i < series.length; i++) {
                                            if (series[i].index === seriesIndex) {
                                                if (!series[i].visible) series[i].setVisible(true, false);
                                            } else {
                                                if (series[i].visible) series[i].setVisible(false, false);
                                            }
                                        }

                                        this.chart.chartHeight = inScope.Data.length <= 2 ? inScope.Data.length * 110 : inScope.Data.length * 75;
                                        this.chart.setSize(this.chart.chartWidth, this.chart.chartHeight);
                                        this.chart.redraw();
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "ALL",
                                data: <any[]>all.Data,
                                color: this.sitesettings.colors.secondaryColors.teal,
                                dataLabels: {
                                    enabled: true,
                                    color: '#fff',
                                    inside: true,
                                    shadow: false,
                                    style: {
                                        fontWeight:'600'
                                    }
                                },
                                visible: false,
                                events: {
                                    legendItemClick: function () {
                                        var seriesIndex = this.index;
                                        var series = this.chart.series;

                                        series[1].chart.axes[0].categories = <any[]>all.Categories;

                                        for (var i = 0; i < series.length; i++) {
                                            if (series[i].index === seriesIndex) {
                                                if (!series[i].visible) series[i].setVisible(true, false);
                                            } else {
                                                if (series[i].visible) series[i].setVisible(false, false);
                                            }
                                        }

                                        this.chart.chartHeight = all.Data.length <= 2 ? all.Data.length * 110 : all.Data.length * 75;
                                        this.chart.redraw();
                                        this.chart.setSize(this.chart.chartWidth, this.chart.chartHeight);
                                        this.chart.redraw();
                                        return false;
                                    }
                                }
                            }
                        ],
                        legend: {
                            enabled: true,
                            align: 'right',
                            borderWidth: 0,
                            verticalAlign: 'top',
                            floating: true,
                            x: 0,
                            y: -10,
                            itemStyle: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        height: inScope.Data.length <= 2 ? inScope.Data.length * 110 : inScope.Data.length * 75
                    });
                }
            }, this.onFailure);
        }

        /**
         * @description Get keys for the data point which has more than 0 complaints
         */
        private getInScopeKeys = (resultant: Array<ISiteResult>) => {

            var inScopeKeys = [];

            var data: Array<ISiteResult> = [];
            data = this.sortArrayDescending(resultant);

            angular.forEach(data,(item, key) => {
                if (item.data > 0) {
                    inScopeKeys.push(key);
                }
            });

            return inScopeKeys;
        }

        /**
         * @description Get categories based on inScope.Keys
         */
        private getInScopeCategories = (resultant: Array<ISiteResult>) => {

            var inScopeCategories = [];

            angular.forEach(resultant,(item) => {
                if (item.data > 0) {
                    inScopeCategories.push(item.category);
                }
            });

            return inScopeCategories;
        };

        /**
         * @description Get data points based on inScope.Keys
         */
        private getInScopeData = (resultant: Array<ISiteResult>) => {

            var inScopeData = [];

            angular.forEach(resultant,(item) => {
                if (item.data > 0) {
                    inScopeData.push(item.data);
                }
            });

            return inScopeData;
        };

        /**
         * @description Get all categories
         */
        private getAllCategories = (resultant: Array<ISiteResult>) => {

            var allCategories = [];

            angular.forEach(resultant,(item) => {
                    allCategories.push(item.category);
            });

            return allCategories;
        };

        /**
         * @description Get data points based on inScope.Keys
         */
        private getAllData = (resultant: Array<ISiteResult>) => {

            var allData = [];

            angular.forEach(resultant,(item) => {
                    allData.push(item.data);
            });

            return allData;
        };

        private sortArrayDescending = (resultant: Array<ISiteResult>): Array<ISiteResult>=> {
            var sortedData: Array<ISiteResult> = resultant.sort((a, b) => { return b.data - a.data; });
            return sortedData;
        }

        public getImage = (userattachments: any) => string => {
        }

        /**
         * @description Get 5 latest complaints.
         */
        private getRecentComplaints = (): void => {
            this.viewLink = this.basePath + 'Quality/Complaint/EntryView/';
            var sDate = this.dateFormatterSvc.formatDateFull(this.dateRange.startDate);
            var eDate = this.dateFormatterSvc.formatDateFull(this.dateRange.endDate);

            var filterQuery;

            if (this.programId > 0) {
                filterQuery = `FeedbackDate ge DateTime'${sDate}' and FeedbackDate le DateTime'${eDate}' and ProgramId eq ${this.programId}`;
            }
            else {
                filterQuery = `FeedbackDate ge DateTime'${sDate}' and FeedbackDate le DateTime'${eDate}'`;
            }

            this.chartsConfig.recentComplaints = [];
            var param = <qualitySvc.IODataPagingParamModel>{
                $filter: filterQuery,
                $orderby: 'FeedbackDate desc',
                $top: 5,
                $skip: 0
            };

            this.complaintsSvc.getComplaints(param)
                .then((data) => {

                    angular.forEach(data, (obj) => {
                        angular.forEach(obj.AccountableEmployeesInformation, (empObj) => {
                            empObj.ProfileImage = this.imageSvc.getUserImageFromAttachments(empObj.UserAttachments, true);
                        });
                        this.chartsConfig.recentComplaints.push(obj);
                });

            }, this.onFailure);
        }

        /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ComplaintEntryViewCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.chartsConfig.recentComplaints, { 'ComplaintId': id });
                    if (index != -1) {
                        this.chartsConfig.recentComplaints.splice(index, 1);
                    }
                }
            });
        }

        private getAllInstanceInArray = (arr, val) => {
            var instances = [], i = -1;
            while ((i = arr.indexOf(val, i + 1)) != -1) {
                instances.push(i);
            }
            return instances;
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {

            this.getComplaintTrends(dateRange);
            this.getComplaintsByType(dateRange);
            this.getComplaintTrendsBySite(dateRange);
            this.getTotalRepeatComplaintsForCurrentMonth();
            this.getRecentComplaints();
        }

        /**
        * @description Defines overridable chart options.
        */
        private chartConfigBuilder = (name, options) => {
            var chartOptions = {
                complaintTrends: <HighchartsOptions>{
                    totalComplaints: 0,
                    totalInScopeComplaints: 0,
                    totalOutofScopeComplaints: 0,
                    categories: [],
                    yAxis: {
                        minRange: null,

                        plotLines: [{
                            value: this.chartsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }

                    },
                    xAxis: {
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [{}, {}],
                    plotLines: [{}],
                    legend: {
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },

                },
                complaintTrendsBySite: <HighchartsOptions>{
                    backgroundColor: '#ffffff',
                    totalObjects: 0,
                    totalZeroComplaints: 0,
                    displayChart: false,
                    series: [{
                        name: 'Complaints',
                        data: [],
                        dataLabels: {
                            enabled: true,
                            color: '#fff',
                            //inside: true
                        }

                    }],
                    //height: 400,
                    xAxis: {
                        categories: [],
                        labels: {
                            useHTML: true,
                            formatter: function () {
                                var labelArr = this.value.split(';');
                                return "<span>" + labelArr[0] + "</span>";
                            },
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                    },
                    yAxis: [
                        {
                            title: {
                                text: ''
                            },
                            allowDecimals: false,
                        }, {
                            linkedTo: 0,
                            allowDecimals: false,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        },
                        
                    ],

                    tooltip: {
                        useHTML: true,
                        formatter: function () {
                            var labelArr = this.x.split(';');
                            var html = "<span>Complaints for period:</span><span style='display:inline-block;margin-left: 10px;font-weight:bold;'>" + this.y + "</span><br />";
                            html += "<span>Complaints for current month:</span><span style='display:inline-block;margin-left: 10px;font-weight:bold;'>" + labelArr[1] + "</span>";
                            return html;
                        }
                    },
                    isLegendEnabled: true,
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            events: {
                                legendItemClick: function () {
                                    return false;
                                }
                            },
                            dataLabels: {
                                enabled: true,
                                //inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            
                        },
                        bar: {
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth
                        }
                    }
                }

            }
            return _.assign(chartOptions[name], options);
        }


        private handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }
    }

    angular.module('app').controller('ComplaintIndexCtrl', ComplaintIndexCtrl);
}  
