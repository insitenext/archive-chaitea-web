﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Complaint.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;

    class ComplaintEditCtrl {
        complaintId: number = 0;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;

        options = {
            complaintTypes: [],
            buildings: [],
            preventableStatuses: [],
            floors: [],
            classifications: [],
            employees: []
        };

        maxDate: Date = new Date();
        basePath: string;
        isEntryDisabled: boolean = false;
        complaint = {
            IsRepeatComplaint: false,
            AccountableEmployees: [],
            FeedbackDate: null,
            Description: '',
            Note: '',
            Action: '',
            CompletedDate: null
        };
        selectedEmployees: any[] = [];
        completedDate: any;
        feedbackDate: any;
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            '$filter',
            'chaitea.quality.services.complaintssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            Common.Services.DateSvc.id,
            'blockUI',
            'chaitea.common.services.apibasesvc',
            '$q'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: any,
            private complaintsSvc: qualitySvc.IComplaintsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private dateSvc: ChaiTea.Common.Services.IDateSvc,
            private blockUI,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $q: angular.IQService
            ) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
        }

        /** @description Initializer for the controller. */
        initialize = (complaintId: number): void => {
            this.complaintId = complaintId;
            this.isEntryDisabled = this.complaintId > 0 ? false : this.checkIfEntryIsDisabled(this.complaintId, this.siteId, this.programId);

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return
            }
            this.getAllLookuplist();
            this.getComplaintById(this.complaintId);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            if (response.FeedbackId > 0) {
                this.complaintId = response.FeedbackId;

                mixpanel.track((response.CompleteDate ? "Closed" : "Opened") + " Complaint", { ComplaintId: response.FeedbackId });
            }
            var forHistoricalPurposes = '';
            if (moment(this.complaint.FeedbackDate) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                forHistoricalPurposes = '  This complaint is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
            }
            bootbox.alert('Complaint was saved successfully!' + forHistoricalPurposes, this.redirectToDetails);
        }

        /** 
         * @description Get complaint object by complaintId. 
         * @param {int} complaintId
         */
        getComplaintById = (complaintId: number): void => {
            if (complaintId > 0) {
                this.complaintsSvc.getComplaint(complaintId).then((result) => {
                    result.FeedbackDate = moment(result.FeedbackDate).toDate();
                    this.complaint = result;
                    this.complaint.IsRepeatComplaint = result.IsRepeatComplaint.toString();
                    this.selectedEmployees = [];
                    angular.forEach(this.complaint.AccountableEmployees, (employee) => {
                        this.selectedEmployees.push(employee.EmployeeId);
                    });
                    this.getFloorsByBuildingId(result.BuildingId);
                    this.feedbackDate = new Date(result.FeedbackDate);
                    if (result.CompletedDate) {
                        this.completedDate = new Date(result.CompletedDate);
                    }
                }, this.onFailure);
            } else {
                this.feedbackDate = this.dateFormatterSvc.formatDateShort(new Date(), 'MM/DD/YYYY');
                this.complaint = {
                    IsRepeatComplaint: false,
                    FeedbackDate: this.dateFormatterSvc.formatDateShort(new Date(), 'MM/DD/YYYY'),
                    AccountableEmployees: [],
                    Description: '',
                    Note: '',
                    Action: '',
                    CompletedDate: null
                };
            }

        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} complaintId
         * @param {number} siteId
         * @param {number} programId
         */
        checkIfEntryIsDisabled = (complaintId: number, siteId: number, programId: number): boolean => {
            return complaintId == 0 && (programId == 0 || siteId == 0);
        }

        /**
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): void => {
            if (this.complaintId) {
                this.lookupListSvc.getComplaintTypesForComplaint({ id: this.complaintId }).then((data) => {
                    this.options.complaintTypes = data.Options;
                }, this.onFailure);
            } else {
                this.lookupListSvc.getComplaintTypesForProgram({ id: this.programId }).then((data) => {
                    this.options.complaintTypes = data.Options;
                }, this.onFailure);
            }

            this.lookupListSvc.getBuildingsForComplaint({ id: this.complaintId }).then((data) => {
                this.options.buildings = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getPreventableStatuses({}).then((data) => {
                this.options.preventableStatuses = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getClassifications({}).then((data) => {
                this.options.classifications = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getEmployeesForComplaint({ id: this.complaintId }).then((data) => {
                this.options.employees = data.Options;
            }, this.onFailure);
        }

        /** 
         * @description Get floors based on buildingId. 
         * @param {number} buildingId
         */
        getFloorsByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                this.blockUI.start('Loading Floors...');
                this.lookupListSvc.getFloors({ id: buildingId }).then((data) => {
                    this.options.floors = data.Options;
                    if (this.blockUI) this.blockUI.stop();
                }, this.onFailure);
            }
        }

        /** @description On building field change event. */
        onBuildingChange = this.getFloorsByBuildingId;

        /** @description Create/update a new complaint. */
        saveComplaint = (): void => {
            if (this.$scope.complaintForm.$valid) {
                this.complaint.Description = this.$filter('htmlToPlaintext')(this.complaint.Description);
                this.complaint.Note = this.$filter('htmlToPlaintext')(this.complaint.Note);
                this.complaint.Action = this.$filter('htmlToPlaintext')(this.complaint.Action);
                var tempDate = new Date();
                this.complaint.FeedbackDate = this.dateFormatterSvc.formatDateFull(new Date(this.feedbackDate.getFullYear(), this.feedbackDate.getMonth(), this.feedbackDate.getDate(),
                            tempDate.getHours(), tempDate.getMinutes(), tempDate.getSeconds()));
                this.complaint.FeedbackDate = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.complaint.FeedbackDate);
                
                if (this.completedDate) {
                       this.complaint.CompletedDate = this.dateFormatterSvc.formatDateFull(new Date(this.completedDate.getFullYear(), this.completedDate.getMonth(), this.completedDate.getDate(), tempDate.getHours(), tempDate.getMinutes(), tempDate.getSeconds()));
                    this.complaint.CompletedDate = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.complaint.CompletedDate);
                }
                var promises = [];
                var promise: any;
                this.complaint.AccountableEmployees = [];
                angular.forEach(this.selectedEmployees, (employee) => {
                    var params = {
                        employeeId: employee,
                        kpiId: Common.Enums.KPITYPE.Complaints

                    };
                    promise = this.apiSvc.query(params, 'EmployeeKpiMaster/CheckIfKpiExemptedByEmployeeId/:employeeId/:kpiId', false, false).then((result) => {
                        this.complaint.AccountableEmployees.push({
                            EmployeeId: employee,
                            IsExempt: result.IsExempt == null ? false : result.IsExempt
                        });
                    }, this.onFailure);
                    promises.push(promise);
                });
                // [SS 6/2/2015]: selected employees aren't being pushed to the server
                if (this.complaintId > 0) {
                    // PUT
                    this.$q.all(promises).then(() => {
                        this.complaintsSvc.updateComplaint(this.complaintId, this.complaint).then(this.onSuccess, this.onFailure);
                    });

                } else {
                    // POST
                    this.$q.all(promises).then(() => {
                        this.complaintsSvc.saveComplaint(this.complaint).then(this.onSuccess, this.onFailure);
                    });
                }
            }
        };

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            window.location.replace(this.basePath + 'Quality/Complaint/');
        }

        /** @description Redirect to details page. */
        redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/Complaint/Details');
        }
    }

    angular.module('app').controller('ComplaintEditCtrl', ComplaintEditCtrl);
}