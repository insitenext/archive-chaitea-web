﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Complaint.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    

    class ComplaintDetailCtrl {
        siteId: number = 0;
        isEntryDisabled: boolean = false;

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        complaints = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: new Date()
        };

        // Filter variables
        filterOptions = [
            { Key: 'ComplaintTypeId', Value: 'Type', Options: null, FieldType: 'select' },
            { Key: 'BuildingId', Value: 'Building', Options: null, FieldType: 'select' },
            { Key: 'CustomerName', Value: 'From', Options: null, FieldType: 'text' },
            {
                Key: 'IsRepeatComplaint', Value: 'Repeat', Options: [
                    { Key: 'yes', Value: 'Yes' },
                    { Key: 'no', Value: 'No'}
                ], FieldType: 'select'
            }
        ];

        filter = {
            filterType: null,
            filterCriteria: null
        };

        boolFilter: boolean = true;
        isFilterCollapsed: boolean = true;

        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[] = [
            {
                title: 'Open',
                onClickFn: () => { this.boolFilter = true; this.getComplaintsReset() }
            },
            {
                title: 'Closed',
                onClickFn: () => { this.boolFilter = false; this.getComplaintsReset() }
            }
        ];

        localStorageComplaintFilterKey: string;

        isCardView: boolean = true;
        module: string = 'complaints';
        viewLink: string = '';
        headerText: string = 'COMPLAINT_DATE';
        clientName: string = "";
        static $inject = [
            '$scope',
            '$q',
            'userInfo',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.quality.services.complaintssvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.imagesvc',
            Common.Services.DateSvc.id,
            '$uibModal'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private servicecomplaints: qualitySvc.IComplaintsSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;

                // Reset the paging
                this.getComplaintsReset({
                    startDate: this.dateFormatterSvc.formatDateFull(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(obj.endDate)
                });
            });
            this.localStorageComplaintFilterKey = 'complaint-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;
            //setting filters from session storage.
            var filterObj = this.localDataStoreSvc.getObject(this.localStorageComplaintFilterKey);
            if (filterObj) {
                this.filter = filterObj;
                this.isFilterCollapsed = false;
            }

            this.getFilterOptions();
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {

            //Remove building from filterOptions if a site has been chosen.
            if (!this.siteId) {
                _.remove(this.filterOptions,(data) => (data['Key'] == 'BuildingId'));
            }

        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging and get complaints.
         */
        getComplaintsReset = (dateRange?: commonInterfaces.IDateRange) => {
            this.isBusy = false;
            this.complaints = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getComplaints(dateRange);
        }

        /**
         * @description Get the filter options.
         */
        getFilterOptions = (): angular.IPromise<any> => {
            var lookupList = [
                this.lookupListSvc.getComplaintTypes()
            ];

            //Add building to filterOptions if a site has been chosen.
            if (this.siteId) {
                lookupList.push(this.lookupListSvc.getBuildings());
            }

            return this.$q.all(lookupList).then((result:Array<any>) => {
                this.filterOptions[0].Options = result[0].Options;

                if (this.siteId)
                    this.filterOptions[1].Options = result[1].Options;
            });
        }

        /**
         * @description Get complaints and push to array for infinite scroll.
         */
        getComplaints = (dateRange?: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Quality/Complaint/EntryView/';
           
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };

            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = this.getPagingParams(dateRange);

            this.servicecomplaints.getComplaints(params)
                .then((data) => {
                if (data.length == 0) { // Do not make any more requests if there's no more data to load.
                    this.isAllLoaded = true;
                }

                angular.forEach(data, (obj) => {
                    angular.forEach(obj.AccountableEmployeesInformation, (empObj) => {
                        empObj.ProfileImage = this.imageSvc.getUserImageFromAttachments(empObj.UserAttachments, true);
                    });
                    this.complaints.push(obj);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

         /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ComplaintEntryViewCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.complaints, { 'ComplaintId': id });
                    if (index != -1) {
                        this.complaints.splice(index, 1);
                    }
                }
            });
        }


        /**
        * @description to export complaint details to excel sheet 
        */
        export = (): void=> {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var complaintsToExport = [];
            for (var i = 0; i < this.complaints.length; i++) {
                complaintsToExport.push({
                    "Name": this.complaints[i].CustomerName, "Program": this.complaints[i].ProgramName, "ComplaintType": this.complaints[i].ComplaintTypeName,
                    "Description": this.complaints[i].Description, "Site": this.complaints[i].SiteName, "Building": this.complaints[i].BuildingName, "CreatedDate": this.complaints[i].FeedbackDate,
                    "CompletedDate": this.complaints[i].CompletedDate
                });
            }
            alasql('SELECT * INTO CSV("complaintdetails.csv",?) FROM ?', [ mystyle, complaintsToExport]);
        }

        /**
         * @description Return a paging param for the OData filter.
         */
        getPagingParams = (dateRange: commonInterfaces.IDateRange) => {
           
            var EndDate = moment(dateRange.endDate).endOf('day').format();
            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(dateRange.startDate);
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(EndDate);
            var params = <qualitySvc.IODataPagingParamModel>{
                $orderby: 'FeedbackDate desc',
                $top: this.top,
                $skip: this.recordsToSkip,
                $filter: `FeedbackDate gt DateTime'${startDate}' and FeedbackDate lt DateTime'${endDate}'`
            };

            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                var filter = '';

                switch (this.filter.filterType) {
                    case 'CustomerName':
                        filter = `startswith(${this.filter.filterType}, '${this.filter.filterCriteria}')`;
                        break;
                    case 'IsRepeatComplaint':
                        switch (this.filter.filterCriteria) {
                            case 'yes':
                                filter = `${this.filter.filterType} eq true`;
                                break;
                            case 'no':
                                filter = `${this.filter.filterType} eq false`;
                                break;
                        }
                        break;
                    default:
                        filter = `${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                }

                params['$filter'] += ` and ${filter}`;

                //saving filter in session storage
                this.localDataStoreSvc.setObject(this.localStorageComplaintFilterKey, this.filter);
            }
            else {
                //remove filter from session storage
                this.localDataStoreSvc.setObject(this.localStorageComplaintFilterKey, '');
                this.isFilterCollapsed = true;
            }

            //CompletedDate
            //Bool filter
            if (this.boolFilter != null) {
                var expression: string = '';

                //Create expression based on boolFilter
                if (!this.boolFilter)
                    expression = 'CompletedDate ne null';
                else
                    expression = 'CompletedDate eq null';
                   
                //Apply expression to new $filter or existing $filter
                if (params['$filter'])
                    params['$filter'] = params['$filter'] + " and " + expression;
                else
                    params['$filter'] = expression;
            }

            return params;
        }

        /**
         * @description Delete a complaint. 
         * @param {int} complaintId
         * */
        deleteComplaint = (complaintId: number): void => {
            bootbox.confirm('Are you sure you want to delete this complaint?',(result) => {
                if (result) {
                    this.servicecomplaints.deleteComplaint(complaintId).then((result) => {
                        for (var i = 0; i < this.complaints.length; i++) {
                            if (this.complaints[i].ComplaintId === complaintId) {
                                this.complaints.splice(i, 1);
                                break;
                            }
                        }
                    }, this.onFailure);
                }
            });
        }
    }

    angular.module('app').controller('ComplaintDetailCtrl', ComplaintDetailCtrl);
} 