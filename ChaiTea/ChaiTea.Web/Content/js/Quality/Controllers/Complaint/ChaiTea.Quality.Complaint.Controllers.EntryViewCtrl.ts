﻿/// <reference path="../../../_libs.ts" />
 
module ChaiTea.Quality.Complaint.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class ComplaintEntryViewCtrl {
        complaint = {};        
        complaintId: number = 0;
        floorName: string;
        employees = [];
        accountableEmployeeObjects = [];
        classificationName: string;
        basePath: string;
        options = {
            employeeIds: [],
            employeesWithImage: []
        };
        employeesWithImage = [];
        moduleIcon: string;
        moduleClass: string;
        customerRepresentativeTemplate: string;
        modalLeftSection: string;
        modalRightSection: string;
        clientName: string = '';
        selectedEmployeeText = "ASSIGNED_EMPLOYEES";
        translateOnPageLoad = {
            'COMPLAINT': 'COMPLAINT_STATIC',
            'CONFIRM': 'DELETE_CONFIRMATION',
            'SUCCESS': 'DELETE_SUCCESS',
            'WARNING': "CANNOT_DELETE_WARNING",
            "REASON": "CANNOT_DELETE_REASON"
        };
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            "chaitea.quality.services.employeesvc",
            'chaitea.quality.services.complaintssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.notificationsvc',
            '$translate',
            'id',
            '$uibModalInstance'
        ];
        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private complaintsSvc: qualitySvc.IComplaintsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $translate: angular.translate.ITranslateService,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance
            ) {

            this.clientName = userContext.Client.Name;
            this.complaintId = id;
            this.basePath = sitesettings.basePath;
            this.moduleClass = 'complaint';
            this.moduleIcon = 'complaint';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.complaints.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.complaints.rightsection.tmpl.html';
            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
            this.getComplaintById(this.complaintId);
        }

        initialize = (complaintId: number): void=> {
            this.getComplaintById(this.complaintId);
        }

        getComplaintById = (complaintId: number): void=> {
            if (complaintId > 0) {
                this.complaintsSvc.getComplaint(complaintId).then((result) => {
                    this.complaint = result;
                    this.accountableEmployeeObjects = result.AccountableEmployeesInformation;
                    this.getInitialEmployees();
                    this.getFloorByFloorId(result.FloorId);
                    this.getEmployeesWithImage();
                }, this.onFailure);
            }
        }

        getFloorByFloorId = (floorId: number): void => {
            if (floorId > 0) {
                this.lookupListSvc.getFloorByFloorId(floorId).then((result) => {
                    this.floorName = result.Name;
                }, this.onFailure);
            }
        }

        getInitialEmployees = (): void => {
            if (this.accountableEmployeeObjects != null) {
                var loopEnd = this.accountableEmployeeObjects.length <= 5 ? this.accountableEmployeeObjects.length : 5;
                for (var i = 0; i < loopEnd; i++) {
                    this.employees.push(this.accountableEmployeeObjects[i].Value);
                }
            }
        }

        addMoreEmployees = (): void=> {

            if (this.accountableEmployeeObjects.length == this.employees.length) {
                return;
            }

            if (this.accountableEmployeeObjects != null && this.accountableEmployeeObjects.length > 5) {
                var loopStart = this.employees.length > 0 ? this.employees.length : 0;
                var remainingEmployees = this.accountableEmployeeObjects.length - this.employees.length;
                var loopEnd = (remainingEmployees <= 5) ? (loopStart + remainingEmployees) : (loopStart + 5);

                for (var i = loopStart; i < loopEnd; i++) {
                    this.employees.push(this.accountableEmployeeObjects[i].Value);
                }
            }
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        redirectToDetails = () => {
            window.location.replace(this.basePath + 'Quality/Complaint/Details');
        }

        public redirectToEntry = () => {
            window.location.href = this.basePath + "Quality/Complaint/Entry/" + this.complaintId;
        }

        deleteComplaint = () => {
            this.alertSvc.confirmWithCallback(this.translateOnPageLoad.CONFIRM + ' ' + this.translateOnPageLoad.COMPLAINT + '?', (res) => {
                if (res) {
                    this.apiSvc.delete(this.complaintId, "ServiceExcellence/DeleteComplaint").then((response) => {
                        if (response.IsClosed) {
                            this.alertSvc.alert({ message: this.translateOnPageLoad.WARNING +  ' ' + this.translateOnPageLoad.COMPLAINT + ' ' + this.translateOnPageLoad.REASON });
                        }
                        else {
                            this.$uibModalInstance.close(true);
                            this.notificationSvc.successToastMessage(this.translateOnPageLoad.COMPLAINT + ' ' + this.translateOnPageLoad.SUCCESS);
                        }
                    });
                }
            });
        }

        private getEmployeesWithImage = (): any => {
            angular.forEach(this.accountableEmployeeObjects, (item) => {
                this.apiSvc.getById(item.EmployeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                    this.employeesWithImage.push({
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                    });
                }, this.onFailure);
            });
        }

        //TODO: J. Gray: Look into way we need the uniqueId

        getProfilePictureUniqueId = (employeeAttachments): string=> {

            var uniqueId: string = null;

            if (employeeAttachments.length > 0) {
                angular.forEach(employeeAttachments,(item) => {
                    if (item.UserAttachmentType === "ProfilePicture") {
                        uniqueId = item.UniqueId;
                    }
                });
            }
            return uniqueId;
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module("app").controller("ComplaintEntryViewCtrl", ComplaintEntryViewCtrl);
}