﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.WorkOrder.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    class WorkOrderIndexCtrl {
        moment = moment;
        siteId: number = 0;
        failColor: string = '#EF5C48';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').format(),
            endDate: moment()
        };
        basePath: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.reportssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.imagesvc',
            'chaitea.quality.services.workorderssvc',
            'chaitea.common.services.apibasesvc',
            '$uibModal'
        ];

        valueFormatter = function (val) {
            return val.toFixed(0) + '%';
        };
        reportsConfig = {
            goalLine: 90,
            counts: {
                workOrderTrends: {
                    count: 0,
                    outOf: 100
                },
                workOrderTrendsBySiteOrBuilding: { count: 0 },
                workOrderTrendsByProgramOrType: { count: 0 },
                workOrderPastDue: { count: 0 },
                workOrderOnTime: { count: 0}
            },

            workOrderTrends: {},
            workOrdersBySiteOrBuilding: {},
            workOrdersByProgramOrType: {},
            workOrderTrendsByProgramOrType: {},
            workOrderTrendsByEmployee: []
        };
        noOfMonths: number = 0;
        module: string = 'workOrders';
        viewLink: string = '';
        workOrders = [];
        siteName: string = '';
        submittedCount: number = 0;
        headerText: string = 'Start Date';
        clientName: string = '';
        programId: number = 0;
        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: qualitySvc.IReportsSvc,
            private lookuplistSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private workOrdersSvc: qualitySvc.IWorkOrdersSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.siteName = userContext.Site.Name;
            this.clientName = userContext.Client.Name;
            this.programId = userContext.Program.ID;

            this.reportsConfig.workOrderTrends = this.chartConfigBuilder('workOrderTrends', {});
            this.reportsConfig.workOrdersBySiteOrBuilding = this.chartConfigBuilder('workOrdersBySiteOrBuilding', {});
            this.reportsConfig.workOrdersByProgramOrType = this.chartConfigBuilder('workOrdersByProgramOrType', {});
            this.reportsConfig.workOrderTrendsByProgramOrType = this.chartConfigBuilder('workOrderTrendsByProgramOrType', {});

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });

            this.getCurrentMonthAverageScore();
        }

        /** @description Initializer for the controller. */
        public initialize = (): void => { }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private getSubmittedCount = (dateRange: commonInterfaces.IDateRange): void => {
            var params = {
                startDate: dateRange.startDate,
                endDate: this.dateFormatterSvc.formatDateShort(moment(this.dateRange.endDate).add(1, 'days'))
            };
            this.apiSvc.query(params, 'WorkOrders/SubmittedCount/:startDate/:endDate', false, false).then((result) => {
                this.submittedCount = result.SubmittedCount;
            }, this.onFailure);
        }

        private getRecentWorkOrders = (): void => {
            this.workOrders = [];
            this.viewLink = this.basePath + 'Quality/WorkOrder/EntryView/';

            var dateRange = {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };
            var params = {
                $top: 5,
                $skip: 0,
                $orderby: 'CreateDate desc',
                $filter: `CreateDate ge DateTime'${dateRange.startDate}' and CreateDate le DateTime'${dateRange.endDate}'`
            };

            if (this.programId) {
                params['$filter'] = params['$filter'] + ' and' + ' ProgramId eq ' + this.programId;
            }

            this.workOrdersSvc.getWorkOrders(params)
                .then((data) => {
                    
                    angular.forEach(data, (obj) => {
                        var dueDate: moment.Moment = obj.RequestDueDate ? moment(obj.RequestDueDate) : moment();
                        var diff: number;
                        if (obj.CloseDate) {
                            diff = moment(obj.CloseDate).diff(dueDate, 'days');
                        }
                        else {
                            diff = moment().diff(dueDate, 'days');
                        }
                        obj.TimeDiff = diff;
                        obj.AssignedToImage = this.imageSvc.getUserImageFromAttachments(obj.AssignedToEmployeeAttachments, true);
                        this.workOrders.push(obj);
                    });
                }, this.onFailure);
        }

        /**
         * @description Get current month average % completion.
         */
        private getCurrentMonthAverageScore = () => {
            this.chartsSvc.getWorkOrderTrends({
                startDate: this.dateFormatterSvc.formatDateShort(moment(new Date()).startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(new Date())
            }).then((result) => {
                if (!result) return;

                var onTimeData = result.OnTimeData,
                    lateData = result.LateData;

                // Calculate completion %
                var totalOnTimeData: number = <number>_.reduce(onTimeData, function (sum, n) {
                    return <number>sum + <number>n;
                });

                var totalWorkOrders: number = <number>_.reduce(onTimeData.concat(lateData), function (sum, n) {
                    return <number>sum + <number>n;
                });

                this.reportsConfig.counts.workOrderTrends.count = (totalOnTimeData / totalWorkOrders) * 100 || 0;
            });
        }

        /**
         * @description Work Order Trends.
         */
        private getWorkOrderTrends = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getWorkOrderTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {

                if (!result) return;

                var categories = result.Categories,
                    onTimeData = angular.copy(result.OnTimeData),
                    lateData = angular.copy(result.LateData);

                // Build the chart
                this.reportsConfig.workOrderTrends = this.chartConfigBuilder('workOrderTrends', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        tickInterval: result.OnTimeData.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
            },
                    series: [{
                        name: 'Completion',
                        lineWidth: 100,
                        data: result.OnTimeData
                    }]
                });

                // Set past due work orders count
                this.reportsConfig.counts.workOrderPastDue.count = <number>_.reduce(lateData, function (sum, n) {
                    return <number>sum + <number>n;
                }) || 0;

                //Set onTime count
                this.reportsConfig.counts.workOrderOnTime.count = <number>_.reduce(onTimeData, (sum, n) => (<number>sum + <number>n)) || 0;
            });
        }

        /**
         * @description By Site or Building.
         */
        private getWorkOrdersBySiteOrBuilding = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getWorkOrdersBySite({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.Categories) return;

                var categories = result.Categories,
                    onTimeData = result.OnTimeData,
                    lateData = result.LateData;

                // Build the chart
                this.reportsConfig.workOrdersBySiteOrBuilding = this.chartConfigBuilder('workOrdersBySiteOrBuilding', {
                    series: [
                        {
                            name: 'LATE',
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: lateData[i]

                                }
                            })
                        }, {
                            name: 'ON TIME',
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: onTimeData[i]

                                }
                            })
                        }],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    height: categories.length * 100

                });

                // Set counts for total work orders by site or building.
                this.reportsConfig.counts.workOrderTrendsBySiteOrBuilding.count = <number>_.reduce(onTimeData.concat(lateData), function (sum, n) {
                    return <number>sum + <number>n;
                });

            });
        }

        /**
         * @description By Program or Type.
         */
        private getWorkOrdersByProgramOrType = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getWorkOrdersByProgramOrType({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.Data.length) return;

                var data = result.Data.sort(function (a, b) {
                    var nameA = a.name.toLowerCase()
                    var nameB = b.name.toLowerCase()
                    if (nameA < nameB)
                        return -1;
                    if (nameA > nameB)
                        return 1;
                    return 0;
                });
                    
                // Build the chart
                this.reportsConfig.workOrdersByProgramOrType = this.chartConfigBuilder('workOrdersByProgramOrType', {
                    series: [
                        {
                            type: 'pie',
                            innerSize: '60%',
                            name: '',
                            data: _.map(data,(d, index) => {
                                var sliceId = d['id'],
                                    sliceName = d['name'],
                                    sliceY = d['y'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    id: sliceId,
                                    // Attach click event to each slice to re-render the line chart
                                    events: {
                                        click: ($event) => {
                                            this.getWorkOrderTrendsByProgramOrType(dateRange, {
                                                id: sliceId,
                                                name: sliceName.toUpperCase(),
                                                color: $event.point.color
                                            })
                                        }
                                    },
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                }); 

                    

                // Build the line chart based on the biggest slice of the pie on initial render
                this.getWorkOrderTrendsByProgramOrType(dateRange, {
                    id: data[0]['id'],
                    name: data[0]['name'],
                   
                });

            });
        }

        /**
         * @description Get trend chart based on legend click.
         */
        public loadTrend = (id: number, color: string, name: string): void => {
            var dateRange: commonInterfaces.IDateRange = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            }
            this.getWorkOrderTrendsByProgramOrType(dateRange, {
                id: id,
                name: name.toUpperCase(),
                color: color
            });
           
        }

        /**
         * @description Get trends by program or type.
         */
        private getWorkOrderTrendsByProgramOrType = (dateRange: commonInterfaces.IDateRange, selectedItem) => {
            this.chartsSvc.getWorkOrderTrendsByProgramOrType(<any>{
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                id: selectedItem.id
            }).then((result) => {
                var categories = result.Categories,
                    onTimeData = result.OnTimeData;

                this.reportsConfig.workOrderTrendsByProgramOrType = this.chartConfigBuilder('workOrderTrendsByProgramOrType', {
                    categories: categories,
                    series: [
                        {
                            color: selectedItem.color, //'#f26e54',
                            lineWidth: 3,
                            name: selectedItem.name,
                            data: onTimeData
                        }
                    ]
                });
            });
        }

        /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.WorkOrderEntryViewCtrl as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.workOrders, { 'WorkOrderId': id });
                    if (index != -1) {
                        this.workOrders.splice(index, 1);
                    }
                }
            });
        }

        /**
         * @description By Employees.
         */
        private getWorkOrdersByEmployees = (dateRange: commonInterfaces.IDateRange): void => {
            if (this.siteId == 0) return;

            this.chartsSvc.getWorkOrdersByEmployee({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                this.reportsConfig.workOrderTrendsByEmployee = result.Data;
            });
        }

        /**
         * @description Return the time remaining for a work order.
         */
        public getTimeRemaining = (workOrder) => {
            if (workOrder.TimeDiff === 0 || (workOrder.CloseDate && workOrder.TimeDiff <= 0)) return '';
            var remainder = workOrder.TimeDiff <= 0 ? { text: 'Left', label: 'success-color' } : { text: 'Late', label: 'warning-text' };
            return `<span class="${remainder.label}">
                        ${Math.abs(workOrder.TimeDiff)} days ${remainder.text}
                `;
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getWorkOrderTrends(dateRange);
            this.getWorkOrdersBySiteOrBuilding(dateRange);
            this.getWorkOrdersByProgramOrType(dateRange);
            this.getWorkOrdersByEmployees(dateRange);
            this.getRecentWorkOrders();
            this.getSubmittedCount(dateRange);
        }

        /**
         * @description Defines overridable chart options.
         */
        private chartConfigBuilder = (name, options) => {
            var chartOptions = {

                // Column Chart
                workOrderTrends: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        //min: 75,
                        //max: 100,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                value: 98,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                // Bar Chart
                workOrdersBySiteOrBuilding: <HighchartsOptions>{
                    title: {
                        text: ''
                    },
                    //height: 40,
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        lineHeight: '10px',
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    yAxis: [
                        {
                            tickInterval: 25,
                            min: 0,
                            max: 100,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                            title: {
                                text: ''
                            }
                        }, {
                            linkedTo: 0,
                            opposite: true,
                            allowDecimals: false,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            }
                        }
                    ],
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'Late',
                            data: []
                        }, {
                            name: 'On Time',
                            data: []
                        }
                    ]
                },

                // Pie Chart
                workOrdersByProgramOrType: {
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []
                        }
                    ]
                },

                // Line Chart
                workOrderTrendsByProgramOrType: {
                    legend: {
                        enabled: true,
                        labelFormatter: function () { return this.name.toUpperCase() + " TREND"; },
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10
                    },
                    title: {
                        text: ''
                    },

                    categories: [],
                    xAxis: {
                        allowDecimals: false,
                        tickWidth: 0,
                        plotBands: [
                            {

                            }
                        ],
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: '',
                        allowDecimals: false,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }

                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null // inherit from series
                            }
                        }
                    },
                    series: [
                        {
                            lineWidth: 3,
                            events: {
                                legendItemClick: () => (false)
                            }
                        }]

                }
            }
            return _.assign(chartOptions[name], options);
        }

    }

    angular.module('app').controller('Quality.WorkOrderIndexCtrl', WorkOrderIndexCtrl);
}  
