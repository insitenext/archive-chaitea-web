﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.WorkOrder.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class WorkOrderEntryViewCtrl {
        workOrder = {
            Id: 0,
            RequestAttachment: [],
            ResolveAttachment: [],
            WorkOrderAttachments: []
        };
        workOrderSeries = [];
        siteId: number = 0;
        modalInstance;
        basePath: string;
        employeesWithImage = [];
        moduleIcon: string;
        moduleClass: string;
        customerRepresentativeTemplate: string;
        modalLeftSection: string;
        modalRightSection: string;
        clientName: string = '';
        selectedEmployeeText = "ASSIGNED_EMPLOYEES";
        siteName: string = '';
        ObjectUrl: string = '';
        zoomScale: number;
        attachmentType = commonInterfaces.ATTACHMENT_TYPE;
        translateOnPageLoad = {
            'WORKORDER': 'WORKORDER_STATIC',
            'CONFIRM': 'DELETE_CONFIRMATION',
            'SUCCESS': 'DELETE_SUCCESS'
        };
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.quality.services.workorderssvc',
            'chaitea.quality.services.employeesvc',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.notificationsvc',
            '$translate',
            'id',
            '$uibModalInstance',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private workOrdersSvc: qualitySvc.IWorkOrdersSvc,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $translate: angular.translate.ITranslateService,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc
        ) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;
            this.siteName = userContext.Site.Name;
            this.workOrder.Id = id;
            this.zoomScale = sitesettings.windowSizes.isPhone ? 1 : 0.5;
            this.moduleClass = 'workorders';
            this.moduleIcon = 'work-order';
            this.modalLeftSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.workorders.leftsection.tmpl.html';
            this.modalRightSection = this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.workorders.rightsection.tmpl.html';
          
            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
            this.modalInstance = this.$uibModal;
            this.getWorkOrderById(this.workOrder.Id);
        }

        /**
         * @description Initialize the controller.
         */
        initialize = (id: number) => {
            this.workOrder.Id = id;
            this.getWorkOrderById(this.workOrder.Id);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
        * @description Success callback.
        */
        onSuccess = (response: any, messageFor: string): void=> {
            bootbox.alert( messageFor + ' deleted successfully');
        }

        /** 
         * EVENT HANDLERS
         * @description Redirect to details page. 
         */
        redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/WorkOrder/Details');
        }

        deleteWorkOrder = (): void => {
            this.alertSvc.confirmWithCallback(this.translateOnPageLoad.CONFIRM + ' ' + this.translateOnPageLoad.WORKORDER + '?', (res) => {
                if (res) {
                    this.workOrdersSvc.deleteWorkOrder(this.workOrder.Id).then((response) => {
                        this.$uibModalInstance.close(true);
                        this.notificationSvc.successToastMessage(this.translateOnPageLoad.WORKORDER + ' ' + this.translateOnPageLoad.SUCCESS);
                    });
                }
            });
        }
        /**
         * WORK ORDER IMAGE
         * @description Display a modal to view the image.
         */
        viewPhoto = (imageProperty, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            // Open the photo manage dialog
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'workOrderImagePreview.tmpl.html',
                controller: 'Quality.WorkOrderImageModalCtrl as vm',
                size: 'md',
                resolve: {
                    imagePublicId: () => { return this.workOrder[imageProperty]; }
                }
            });

            this.modalInstance.result.then(() => {

            }, () => { });
        }

        /** 
         * WORK ORDER
         * @description Get work order data.
         * @param {int} id
         */
        getWorkOrderById = (id: number): void => {
            if (id == 0) return;

            this.workOrdersSvc.getWorkOrder(id).then((result) => {
                this.workOrder = result;
                this.workOrder.Id = result.WorkOrderId;
                this.ObjectUrl = this.getAttachment(result.WorkOrderAttachments);
                this.getWorkOrderSeriesById(result.WorkOrderSeriesId);
                //create request and resolve attachment viewmodels
                this.workOrder.RequestAttachment = _.where(result.WorkOrderAttachments, { 'WorkOrderAttachmentType': 'Request' });
                this.workOrder.ResolveAttachment = _.where(result.WorkOrderAttachments, { 'WorkOrderAttachmentType': 'Resolve' });
                this.getEmployeesWithImage(this.workOrder);
            }, this.onFailure);
        }

        /** 
        * WORK ORDER
        * @description Get work order series data.
        * @param {int} id
        */
        getWorkOrderSeriesById = (id: number): void => {
            if (id == 0 || id == null) return;

            this.workOrdersSvc.getWorkOrderSeries(id).then((result) => {
                this.workOrderSeries = result;               
            }, this.onFailure);
        }

        private getAttachment = (attachment: any): string => {
            var img: string;
            if (attachment.length > 0) {
                img = this.awsSvc.getUrlByAttachment(attachment[0].UniqueId, attachment[0].AttachmentType);
            }
            return img;
        }
        private getEmployeesWithImage = (workOrder): any => {
          
            this.apiSvc.getById(workOrder.AssignedToId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee) => {

                    this.employeesWithImage.push({
                        Name: employee.Name,
                        EmployeeId: employee.EmployeeId,
                        UserAttachments: employee.UserAttachments,
                        HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        JobDescription: employee.JobDescription.Name,
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                });
            }, this.onFailure);
         
        }

        getProfilePictureUniqueId = (employeeAttachments): string=> {

            var uniqueId: string = null;

            if (employeeAttachments.length > 0) {
                angular.forEach(employeeAttachments,(item) => {
                    if (item.UserAttachmentType === "ProfilePicture") {
                        uniqueId = item.UniqueId;
                    }
                });
            }
            return uniqueId;
        }

        public redirectEntry = (): void => {
            window.location.href = this.basePath + "Quality/WorkOrder/Entry/" + this.workOrder.Id;
        }

        public redirectClose = (): void => {
            window.location.href = this.basePath + "Quality/WorkOrder/EntryFinalize/" + this.workOrder.Id;
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('Quality.WorkOrderEntryViewCtrl', WorkOrderEntryViewCtrl);
} 