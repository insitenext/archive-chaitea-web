﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.WorkOrder.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;

    class WorkOrderUpcomingCtrl {
        userId: number = 0;
        siteId: number = 0;

        isEntryDisabled: boolean = false;

        /** @description Paging variables. */
        recordsToSkip: number = 0;
        top: number = 20;
        scheduledWorkOrders = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        options = {
            recurrenceTypes: []
        }

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.quality.services.workorderssvc',
            'chaitea.quality.services.lookuplistsvc'];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private workOrdersSvc: qualitySvc.IWorkOrdersSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc) {

            this.basePath = sitesettings.basePath;
            this.userId = userInfo.userId;
            this.siteId = userContext.Site.ID;
            this.isEntryDisabled = this.checkIfEntryIsDisabled(this.siteId);

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element("#client-context").trigger("click");
                }, 500);
                return
            }
            this.getRecurrenceTypes();
            //this.getScheduledWorkOrders();

        }

        /** @description Initializer for the controller. */
        initialize = (): void => {

        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} siteId
         */
        checkIfEntryIsDisabled = (siteId: number): boolean => {
            return (siteId == 0);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging.
         */
        getScheduledWorkOrdersReset = () => {
            this.scheduledWorkOrders = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getScheduledWorkOrders();
        }

        /**
         * @description Get work orders schedules and push to array for infinite scroll.
         */
        getScheduledWorkOrders = (): void => {
            if (this.isBusy || this.isAllLoaded || this.isEntryDisabled) return;
            this.isBusy = true;

            var param = {
                top: this.top,
                skip: this.recordsToSkip
            }

            this.apiSvc.query(param, 'WorkOrderSeries/UpcomingWorkOrders/:top/:skip', false, true).then((data) => {
                    if (data.length == 0) {
                        this.isAllLoaded = true;
                    }

                    // Redefine a new recurrence object
                    angular.forEach(data, (obj) => {
                        var t = _.find(this.options.recurrenceTypes, { Key: obj.RecurrenceInfo.RecurrenceTypeId.toString() }) || null;
                        var recurrence = {
                            WorkOrderId: obj.WorkOrderId,
                            WorkOrderSeriesId: obj.WorkOrderSeriesId,
                            Summary: obj.Summary,
                            StartDate: obj.RecurrenceInfo.StartDate,
                            EndDate: obj.RecurrenceInfo.EndDate,
                            RecurrenceTypeId: obj.RecurrenceInfo.RecurrenceTypeId,
                            RecurrenceTypeName: t ? t.Value : '',
                            ScheduledDates: []
                        }

                        this.scheduledWorkOrders.push(recurrence);
                    });
                    this.isBusy = false;
                    this.recordsToSkip += this.top;

                }, this.onFailure);
        }

        /**
         * @description Gets all recurrence types for mapping purposes.
         */
        getRecurrenceTypes = () => {
            return this.lookupListSvc.getWorkOrderRecurrenceTypes().then((result) => {
                this.options.recurrenceTypes = result;
            })
        }

        /**
         * @description Handles collapsing and retrieving the schedule recurrence dates.
         */
        toggleRecurrence = ($event, recurrence) => {
            recurrence.IsToggled = !recurrence.IsToggled;
            if (recurrence.ScheduledDates && recurrence.ScheduledDates.length == 0) {
                recurrence.ScheduledDates = this.getProjectedDates(recurrence);
            }
        }

        /**
         * @description Returns an array of projected dates based on recurrence data.
         */
        getProjectedDates = (recurrence) => {
            // Set dates
            var startDate = recurrence.StartDate,
                endDate = recurrence.EndDate,
                recurrenceTypeId = recurrence.RecurrenceTypeId;

            startDate = moment(startDate).isBefore(new Date()) ? moment(new Date()) : moment(startDate);
            endDate = moment(endDate).isValid() ? moment(endDate) : moment().add(3, 'months');

            var dates = [];
            
            // Date projections
            var projections = [
                { increment: 1, type: 'days' },     // daily
                { increment: 1, type: 'weeks' },    // weekly
                { increment: 2, type: 'weeks' },    // 2x month
                { increment: 1, type: 'months' },   // monthly
                { increment: 3, type: 'months' },   // quarterly
                { increment: 6, type: 'months' },   // 6 months
                { increment: 1, type: 'years' }     // yearly
            ]

            // Create the date projection
            var date = function (d) {            
                if (recurrenceTypeId == 3) {
                    var day = d.get('date');
   
                    if (day <= 15) {                     
                        d = moment(d).date(15);                             
                    } else {
                        d= moment(d).add(1, 'month').date(1);
                    }
            
                    d=moment(d).add(1, 'day');
                } else {
                    var projection = projections[recurrenceTypeId - 1];
                    d.add(projection.increment, projection.type);
                }
        
                return d;
            }

            if (!recurrenceTypeId) return;

            while (startDate.isBefore(endDate)) {
                var tmp = startDate = date(startDate);
                dates.push(recurrenceTypeId == 3? moment(tmp).subtract(1, 'day').format(): startDate.format());
            }

            return dates;
        }

        deleteWOSeries = (id: number): void => {
            this.workOrdersSvc.deleteWorkOrderSeries(id).then((res) => {
                bootbox.alert('Deleted successfully');
                this.getScheduledWorkOrdersReset();
            });
        }

        private editWOSeries = (id: number): void => {
            window.location.href = `${this.basePath}Quality/WorkOrder/Entry/?workOrderSeriesId=${id}`;
        }
    }

    angular.module('app').controller('Quality.WorkOrderUpcomingCtrl', WorkOrderUpcomingCtrl);
}