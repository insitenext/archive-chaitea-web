﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.WorkOrder.Controllers {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = Quality.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceLookupListSvc = ChaiTea.Services;

    
    class WorkOrderDetailsCtrl {
        userId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        isEntryDisabled: boolean = false;
        isManager: boolean = false;
        siteName: string;
        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        workOrders = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };

        // Filter variables
        filterOptions = [
            { Key: 'AssignedToId', Value: 'Assigned To', Options: null, FieldType: 'select' },
            { Key: 'BuildingId', Value: 'Building', Options: null, FieldType: 'select' },
            {
                Key: 'CloseDate',
                Value: 'Status',
                Options: [
                    { Key: 'eq null', Value: 'Open' },
                    { Key: 'ne null', Value: 'Closed' }
                ],
                FieldType: 'select'
            },
            { Key: 'PriorityId', Value: 'Priority', Options: [], FieldType: 'select' },
        ];

        filter = {
            filterType: null,
            filterCriteria: null
        };

        boolFilter: boolean = true;

        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[] = [
            {
                title: 'Open',
                onClickFn: () => { this.boolFilter = true; this.getWorkOrdersReset() }
            },
            {
                title: 'Closed',
                onClickFn: () => { this.boolFilter = false; this.getWorkOrdersReset() }
            }
        ];

        isFilterCollapsed: boolean = true;
        localStorageWorkorderFilterKey: string;

        isCardView: boolean = true;
        module: string = 'workOrders';
        viewLink = [];
        viewCloseLink: string = '';
        headerText: string = "Start Date";
        clientName: string = '';

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.quality.services.workorderssvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$uibModal'
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private workOrdersSvc: qualitySvc.IWorkOrdersSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.basePath = sitesettings.basePath;
            this.userId = userInfo.userId;
            this.siteId = userContext.Site.ID;
            this.siteName = userContext.Site.Name;
            this.programId = userContext.Program.ID;
            this.clientName = userContext.Client.Name;

            this.isManager = this.canUserApproveWorkOrder();

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                // Reset the paging
                this.getWorkOrdersReset();
            });

            this.getAllLookuplist();
            
            this.localStorageWorkorderFilterKey = 'workorder-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;

            //setting filters from session storage.
            var filterObj = this.localDataStoreSvc.getObject(this.localStorageWorkorderFilterKey);
            if (filterObj) {
                this.filter = filterObj;
                this.isFilterCollapsed = false;
            }
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            //Remove building from filterOptions if a site has been chosen.
            if (!this.siteId) {
                _.remove(this.filterOptions,(data) => (data['Key'] == 'BuildingId'));
            }
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging.
         */
        getWorkOrdersReset = () => {
            this.isBusy = false;
            this.workOrders = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getWorkOrders();
        }

        /**
         * @description Get work orders and push to array for infinite scroll.
         */
        getWorkOrders = (): void => {
            this.viewLink = [];
            if (this.boolFilter && this.isManager) {
                this.viewLink.push(<commonInterfaces.actionLinks> {
                    Name: 'Edit',
                    Icon: 'fa-pencil',
                    Link: this.basePath + 'Quality/WorkOrder/Entry/'
                });
                this.viewLink.push(<commonInterfaces.actionLinks>{
                    Name: 'Close',
                    Icon: 'fa-close', 
                    Link: this.basePath + 'Quality/WorkOrder/EntryFinalize/'
                });
                this.viewLink.push(<commonInterfaces.actionLinks>{
                    Name: 'View',
                    Icon: 'fa-search',
                    Link: this.basePath + 'Quality/WorkOrder/EntryView/'
                });
            }
            else if (this.boolFilter && !this.isManager) {
                this.viewLink.push(<commonInterfaces.actionLinks>{
                    Name: 'View',
                    Icon: 'fa-search',
                    Link: 'Quality/WorkOrder/Entry/'
                });
            }
            this.viewCloseLink = this.basePath + 'Quality/WorkOrder/EntryView/';
           
            var dateRange = {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };

            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var paramsObj = {
                $top: this.top,
                $skip: this.recordsToSkip,
                $orderby: 'CreateDate desc',
                $filter: `CreateDate ge DateTime'${dateRange.startDate}' and CreateDate le DateTime'${dateRange.endDate}'`
            };

            var param = this.getPagingParams(paramsObj);
            
            this.workOrdersSvc.getWorkOrders(param)
                .then((data) => {
                    if (data.length === 0) {
                        this.isAllLoaded = true;
                    }

                    angular.forEach(data,(obj) => {
                        var dueDate: moment.Moment = obj.RequestDueDate ? moment(obj.RequestDueDate) : moment();
                        var diff: number;
                        if (obj.CloseDate) {
                            diff = moment(obj.CloseDate).diff(dueDate, 'days');
                        }
                        else {
                            diff = moment().diff(dueDate, 'days');
                        }
                        obj.TimeDiff = diff;
                        obj.AssignedToImage = this.imageSvc.getUserImageFromAttachments(obj.AssignedToEmployeeAttachments, true);
                        this.workOrders.push(obj);
                    });
                    this.isBusy = false;
                    this.recordsToSkip += this.top;

                }, this.onFailure);
        }
        
        /**
       * @description to export work order details to excel sheet 
       */
        export = (): void => {           
            var dateRange = {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };

            var paramsObj = {
                $orderby: 'CreateDate desc',
                $filter: `CreateDate ge DateTime'${dateRange.startDate}' and CreateDate le DateTime'${dateRange.endDate}'`
            };

            var params = this.getPagingParams(paramsObj);

            this.workOrdersSvc.getWorkOrders(params).then((result) => {                            
                if (result.length === 0) {                    
                    return;
                }

                var exports = [];
                angular.forEach(result, (obj) => {
                    var dueDate: moment.Moment = obj.RequestDueDate ? moment(obj.RequestDueDate) : moment();
                    var diff: number;
                    if (obj.CloseDate) {
                        diff = moment(obj.CloseDate).diff(dueDate, 'days');
                    }
                    else {
                        diff = moment().diff(dueDate, 'days');
                    }
                    obj.TimeDiff = diff;
                    exports.push(obj);
                });

                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Bold: "1" } } },
                };

                var workOrdersToExport = [];
                if (this.boolFilter) {
                    for (var i = 0; i < exports.length; i++) {
                        workOrdersToExport.push({
                            "AssignedTo": exports[i].AssignedToName, "Program": exports[i].ProgramName,
                            "Site": this.siteName, "Building": exports[i].BuildingName, "Floor": exports[i].FloorName, "Request Type": exports[i].ServiceTypeName,
                            "Summary": exports[i].Summary, "DueDate": exports[i].DueDate
                        });
                    }
                }
                else {
                    for (var i = 0; i < exports.length; i++) {
                        workOrdersToExport.push({
                            "AssignedTo": exports[i].AssignedToName, "Program": exports[i].ProgramName,
                            "Site": this.siteName, "Building": exports[i].BuildingName, "Floor": exports[i].FloorName, "Request Type": exports[i].ServiceTypeName,
                            "Summary": exports[i].Summary, "DueDate": exports[i].DueDate, "ClosedDate": exports[i].CloseDate
                        });
                    }
                }

                alasql('SELECT * INTO CSV("workordersdetails.csv",?) FROM ?', [mystyle, workOrdersToExport]);
            }, this.onFailure);            
        }        

        /**
         * @description Return a paging param for the OData filter.
         */
        getPagingParams = (params: any) => {

            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                var filter = '';

                switch (this.filter.filterType) {
                case 'CloseDate':
                    filter = `${this.filter.filterType} ${this.filter.filterCriteria}`;
                    break;
                default:
                    filter = `${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                }

                params['$filter'] = params['$filter'] + ` and ${filter}`;

                //saving filter in session storage
                this.localDataStoreSvc.setObject(this.localStorageWorkorderFilterKey, this.filter);
            }
            else {
                //remove filter from session storage
                this.localDataStoreSvc.setObject(this.localStorageWorkorderFilterKey, '');
                this.isFilterCollapsed = true;
            }

            //Bool filter
            if (this.boolFilter != null) {
                var expression: string = '';

                //Create expression based on boolFilter
                if (!this.boolFilter)
                    expression = 'CloseDate ne null';
                else
                    expression = 'CloseDate eq null';

                // Apply program based filter expression to expression
                if (this.programId)
                    expression = expression + ' and' + ' ProgramId eq ' + this.programId;  

                //Apply expression to new $filter or existing $filter
                if (params['$filter'])
                    params['$filter'] = params['$filter'] + " and " + expression;
                else
                    params['$filter'] = expression;
            }

            return params;
        }

        /**
        * LOOKUPS
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): angular.IPromise<any> => {
            var lookupList = [this.lookupListSvc.getEmployees()];
            var priorities = [];
            //Add building to filterOptions if a site has been chosen.
            if (this.siteId) {
                lookupList.push(this.lookupListSvc.getBuildings());
            }
            lookupList.push(this.apiSvc.get('WorkOrderPriority'));
            return this.$q.all(lookupList).then((result: Array<any>) => {
                this.filterOptions[0].Options = result[0].Options;
                var priorityResult = [];
                if (this.siteId) {
                    this.filterOptions[1].Options = result[1].Options;
                    priorityResult = result[2];
                }
                else {
                    priorityResult = result[1];
                }
                angular.forEach(priorityResult, (item) => {
                    priorities.push({
                        Key: item.PriorityId,
                        Value: item.Name
                    });
                });
                var index = _.findIndex(this.filterOptions, { 'Key': 'PriorityId'});
                this.filterOptions[index].Options = [];
                this.filterOptions[index].Options = priorities;
            });
        }

        /**
         * @description Determine whether a work order is late.
         */
        isLate = (workOrder) => {
            return !workOrder.CloseDate && new Date() > workOrder.DueDate;
        }

        /**
         * @description Return the time remaining for a work order.
         */
        getTimeRemaining = (workOrder) => {
            if (workOrder.TimeDiff === 0 || (workOrder.CloseDate && workOrder.TimeDiff <= 0)) return '';

            return Math.abs(workOrder.TimeDiff).toString();
        }

        public getDaysTag = (workOrder) => {
            if (workOrder.TimeDiff === 0 || (workOrder.CloseDate && workOrder.TimeDiff <= 0)) return '';
            var remainder = workOrder.TimeDiff <= 0 ? { text: 'DAYS_LEFT', label: 'success-color' } : { text: 'DAYS_LATE', label: 'warning-text' };
            return  remainder.text;
        }


        /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.WorkOrderEntryViewCtrl as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.workOrders, { 'WorkOrderId': id });
                    if (index != -1) {
                        this.workOrders.splice(index, 1);
                    }
                }
            });
        }

        /**
         * @description Returns whether the user is a manager.
         */
        canUserApproveWorkOrder = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }
    }

    angular.module('app').controller('Quality.WorkOrderDetailsCtrl', WorkOrderDetailsCtrl);
}