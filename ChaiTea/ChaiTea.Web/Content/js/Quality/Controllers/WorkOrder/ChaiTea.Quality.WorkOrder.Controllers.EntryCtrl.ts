﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.WorkOrder.Controllers {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    import qualitySvc = Quality.Services;
    import qualityInterfaces = Quality.Interfaces;
    import commonInterfaces = Common.Interfaces;


    interface IRecurrenceInfo {
        StartDate: Date;
        EndDate: Date;
        RecurrenceTypeId: number;
        DueIn: number;
    }

    interface IWOAttachment extends commonInterfaces.IAttachment {
        WorkOrderAttachmentId: number;
        WorkOrderAttachmentType:string;
    }

    interface IWorkOrderItem {
        WorkOrderId: number;
        ProgramId: number;

        // User section
        ReportedById: any;
        ContactNumber: any;
        Summary: any;
        //RequestImageUid: any;
        RequestAttachment: IWOAttachment[];
        BuildingId: any;
        FloorId: any;
        Location: any;
            
        // Manager section
        ServiceTypeId: any;
        Equipment: any;
        IsProductionAffected: boolean;
        PriorityId: any;
        IsRecurring: boolean;
        RequestDueDate: Date;
        DueDate: Date;
        IsInScope: boolean;
        IsBillable: boolean;
        Cost: any;
        PoNumber: any;
        BillingDescription: any;
        AssignedToId: any;
        WorkOrderNumber: any;
        RecurrenceInfo: IRecurrenceInfo;

        // Finalize section
        Resolution: any;
        CloseDate: Date;
        //ResolveImageUid: any;
        ResolveAttachment: IWOAttachment[];
        WorkOrderAttachments:any;
    }

    enum WO_ATTACHMENT_TYPE {
        Request,
        Resolve
    }

    class WorkOrderEntryCtrl {
        isManagerRole: boolean = false;
        isNavHidden: boolean = false;
        isStep1Valid: boolean = true;
        currentState: string = '';  // current state = form.step1, etc.
        currentMode: string = '';   // current mode  = Edit, Close
        RequestAttachmentName = 'Select Attachment';

        message = {
            header: '',
            text: ''
        };

        pageStates = {
            'form.step1': {
                title: 'Step 1',
                description: 'Add Work Order'
            },
            'form.step2': {
                title: 'Step 2',
                description: 'Additional Details'
            },
            'form.review': {
                title: 'Review',
                description: 'Work Order Summary'
            },
            'form.finalize': {
                title: 'Finalize',
                description: ''
            }
        }

        isEntryDisabled: boolean = false;

        // Set defaults
        workOrder = <IWorkOrderItem>{
            WorkOrderId: 0,
            ProgramId: 0,

            // User section
            ReportedById: null,
            ContactNumber: null,
            Summary: null,
            //RequestImageUid: null,
            BuildingId: null,
            FloorId: 0,
            Location: null,
            
            // Manager section
            ServiceTypeId: null,
            Equipment: null,
            IsProductionAffected: false,
            PriorityId: null,
            IsRecurring: false,
            DueDate: <Date>null,
            RequestDueDate: <Date>null,
            IsInScope: true,
            IsBillable: false,
            Cost: null,
            PoNumber: null,
            BillingDescription: null,
            AssignedToId: null,
            WorkOrderNumber: null,
            RecurrenceInfo: <IRecurrenceInfo> null,

            // Finalize section
            Resolution: null,
            CloseDate: <Date> null,
            //ResolveImageUid: null,
            RequestAttachment:[],
            ResolveAttachment:[],
            WorkOrderAttachments:[]
        };

        siteId: number = 0;
        programId: number = 0;
        fileUpload;
        modalInstance;

        options = {
            buildings: [],
            employees: [],
            customerRepresentatives: [],
            floors: [],
            workTypes: [],
            serviceTypes: [],
            priorities: [],
            equipments: [],
            recurrenceTypes: []
        };

        minDate: Date = new Date();
        maxDate: Date = moment(new Date()).add(2, 'years').toDate();
        basePath: string;
        multiselectTemplateUrl:string;

        validationRules = [
            { key: 'form.step1', required: ['Summary', 'BuildingId', 'ReportedById', 'FloorId', 'RequestDueDate'] },
            { key: 'form.step2', required: ['ServiceTypeId', 'PriorityId', 'AssignedToId'] },
            { key: 'form.finalize', required: ['CloseDate'] }
        ];
        workOrderSeriesId: number = 0;
        public static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            '$state',
            '$stateParams',
            'userInfo',
            '$log',
            '$timeout',
            '$filter',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.quality.services.workorderssvc',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'blockUI',
            '$window',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $state: angular.ui.IStateService,
            private $stateParams: angular.ui.IStateParamsService,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private workOrdersSvc: qualitySvc.IWorkOrdersSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private blockUI,
            private $window: angular.IWindowService,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc
        ) {

            // Define init properties to set proper context
            this.basePath = sitesettings.basePath;
            this.multiselectTemplateUrl = this.basePath + 'Content/js/Common/Templates/multiselect.tmpl.html';
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.isEntryDisabled = this.checkIfEntryIsDisabled(this.siteId, this.programId);

            $scope.$on('$stateChangeSuccess',(e, to) => {
                this.preSubmitCheck();
                this.currentState = this.$state.current.name || 'form.step1';
            });

            this.modalInstance = this.$uibModal;

        }

        /**
         * @description Initialize the controller.
         */
        initialize = (id: number, workOrderSeriesId: number, mode) => {
            this.isManagerRole = this.canUserApproveWorkOrder();
            this.workOrder.WorkOrderId = id;
            this.currentMode = mode;
            this.workOrderSeriesId = workOrderSeriesId;
            // remove the constraint of providing program to edit/close a work order
            if ((this.currentMode === 'Close' || (this.currentMode === 'Edit' && (this.workOrder.WorkOrderId || this.workOrderSeriesId))))
                this.isEntryDisabled = false;

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return;
            }
            //get all lookuplist for new work order
            if (this.currentMode === 'Edit') {
                this.getAllLookuplist();
            }
            
            if (this.workOrder.WorkOrderId) {//get workorder by id
                this.getWorkOrderById(this.workOrder.WorkOrderId);
            }
            else if (this.workOrderSeriesId) {
                this.getWorkOrderSeriesById();
            }

           
        }
        
        /**
         * @description Errors callback.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.alertSvc.alertWithCallback('Work Order was saved successfully!', this.redirectToDetails);
        }

        /**
         * @description Returns whether the user is a manager.
         */
        canUserApproveWorkOrder = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} siteId
         * @param {number} programId
         */
        checkIfEntryIsDisabled = (siteId: number, programId: number): boolean => {
            return (programId === 0 || siteId === 0);
        }

        /**
        * LOOKUPS
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): angular.IPromise<any>  => {
            return this.$q.all([
                this.lookupListSvc.getWorkOrderRecurrenceTypes(),
                this.apiSvc.get('WorkOrderPriority'),
                this.auditsScoreCardSvc.getCustomerRepresentatives(),
                this.lookupListSvc.getBuildings(),
                this.lookupListSvc.getEmployees(),
                this.lookupListSvc.getWorkOrderServiceTypes(this.workOrder.ProgramId ? this.workOrder.ProgramId : this.programId)
            ]).then((result: Array<any>) => {
                this.options.recurrenceTypes = result[0];
                this.options.priorities = result[1];
                this.options.customerRepresentatives = result[2];
                this.options.buildings = result[3].Options;
                this.options.employees = result[4].Options;
                this.options.serviceTypes = result[5].Options;
            });
        }

        /**
         * @description Handle when building changes.
         */
        onBuildingChange = ($event) => {
            this.workOrder.FloorId = null;
            this.getFloorByBuildingId();
        }

        /** 
         * LOOKUPS
         * @description Get floor based on buildingId. 
         * @param {number} buildingId
         */
        getFloorByBuildingId = (): void => {
            this.options.floors = [];

            if (this.workOrder.BuildingId > 0) {
                this.blockUI.start('Loading Floors...');
                this.lookupListSvc.getFloors({ id: this.workOrder.BuildingId }).then((data) => {
                    this.options.floors = data.Options;
                    if (this.blockUI) this.blockUI.stop();
                }, this.onFailure);
            }
        }

        /** 
         * WORK ORDER
         * @description Get work order data.
         * @param {int} id
         */
        getWorkOrderById = (id: number): void => {
            if (id === 0) return;

            this.workOrdersSvc.getWorkOrder(id).then((result) => {
                this.workOrder = result;
                this.workOrder.RequestDueDate = new Date(result.RequestDueDate);
                this.workOrder.IsRecurring = !!this.workOrder.IsRecurring;
               
                this.getFloorByBuildingId();

                //create request and resolve attachment viewmodels
                this.workOrder.RequestAttachment = <IWOAttachment[]>_.where(result.WorkOrderAttachments, { 'WorkOrderAttachmentType': 'Request' });
                this.workOrder.ResolveAttachment = <IWOAttachment[]>_.where(result.WorkOrderAttachments, { 'WorkOrderAttachmentType': 'Resolve' });

                // to retieve file name from unique id
                if (this.workOrder.RequestAttachment.length) {
                    var uId = this.workOrder.RequestAttachment[0].UniqueId.toString();
                    if (uId.indexOf('_') == uId.lastIndexOf('_'))// for older work orders created with out including filename during upload
                        this.RequestAttachmentName = 'Attachment_1'
                    else if (this.workOrder.RequestAttachment[0].AttachmentType == 'Image') // for images
                        this.RequestAttachmentName = uId.substring(uId.indexOf('_') + 1, uId.lastIndexOf('_'));
                    else // for other attachment types
                        this.RequestAttachmentName = uId.substring(uId.indexOf('/') + 1, uId.lastIndexOf('_'))
                }

                // Redirect to finalize state if mode=='Close'
                if (this.currentMode === 'Close') {
                    if (this.workOrder.ServiceTypeId) {
                        this.$state.go('form.finalize');
                    }
                    else {
                        this.$state.go('form.step2');
                        bootbox.alert("Provide all required fields to close work order");
                    }

                }
            }, this.onFailure);
        }

        private getWorkOrderSeriesById = (): void => {
            this.apiSvc.getById(this.workOrderSeriesId, "WorkOrderSeries").then((result) => {
                this.workOrder = result;
                this.workOrder.RequestDueDate = new Date(result.RequestDueDate);
                this.workOrder.IsRecurring = !!this.workOrder.IsRecurring;
                this.workOrder.RecurrenceInfo.StartDate = new Date(result.RecurrenceInfo.StartDate);
                if (result.RecurrenceInfo.EndDate) {
                    this.workOrder.RecurrenceInfo.EndDate = new Date(result.RecurrenceInfo.EndDate);
                }
             
                this.getFloorByBuildingId();

            }, this.onFailure);
        }

        /**
         * WORK ORDER
         * @description Get work order state
         */
        getPageState = (state: string) => {
            return this.pageStates[state];
        }

        /**
         * WORK ORDER
         * @description Validates the work order by current state.
         */
        isWorkOrderValid = (): boolean => {
            var isValid = true;
            this.isStep1Valid = true;

            var ruleIndex = _.findIndex(this.validationRules, { key: this.currentState });

            while (ruleIndex >= 0) {
                var rules = this.validationRules[ruleIndex];
                angular.forEach(rules['required'],(field) => {
                    if (!this.workOrder[field]) {
                        isValid = false;
                        if (ruleIndex == 0)
                            this.isStep1Valid = false;
                    }
                });

                --ruleIndex;
            }

            return isValid;
        };

        /**
         * WORK ORDER IMAGE
         * @description Handler when a photo is selected.
         */
        fileAdded = (imageProp, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();
            if (files && files.length) {
                var file = files[0];
                this.RequestAttachmentName = file.name;
                var fileExt = file.name.split('.').pop();
                var fileName = this.getRandToken() + '_' + this.RequestAttachmentName.substr(0, this.RequestAttachmentName.lastIndexOf('.')) + '.' + fileExt;
                var isImage = file.type.indexOf('image') >= 0;
                //create object to replace workOrder.RequestAttachment|ResolveAttachment
                var tempAttachment = <IWOAttachment>{
                    WorkOrderAttachmentType: imageProp == 'RequestAttachment' ? 'Request' : 'Resolve',
                    UniqueId: null,
                    AttachmentTypeId: Common.Interfaces.ATTACHMENT_TYPE.Image,
                    WorkOrderAttachmentId: 0,
                    AttachmentType: 'Image',
                    Name: file.name.toLowerCase()
                }

                //check if is NOT an image
                if (!isImage) {
                    tempAttachment.AttachmentType = 'File';
                    fileName = file.name.toLowerCase(); //only images get unique name
                }

                if (!this.workOrder[imageProp]) {
                    this.workOrder[imageProp] = [];
                }

                //merge existing attachment values into tempAttachment
                if (this.workOrder[imageProp] && this.workOrder[imageProp].length) {
                    _.merge(this.workOrder[imageProp][0], tempAttachment);

                }
                
                if (isImage) {
                    this.imageSvc.save(file, false)
                        .then((result) => {
                            this.handleResult(result, tempAttachment, imageProp);
                        });
                } else {
                    this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {
                        if (!creds.AccessKey || !creds.AccessSecret) throw "not authorized";
                        this.awsSvc.s3Upload(file, creds, commonInterfaces.S3Folders.file, commonInterfaces.S3ACL.private)
                            .then((result) => {
                                this.handleResult(result, tempAttachment, imageProp);
                            }, this.onFailure);
                    });
                }


                
            }

            return false;
        }

        private handleResult = (result, tempAttachment, imageProp) => {
            var publicId = result.Name;
            //[DW] is wired for ONE ATTACHMENT
            tempAttachment.UniqueId = publicId;

            if (this.workOrder[imageProp].length) {
                this.workOrder[imageProp][0] = tempAttachment;
            } else {
                this.workOrder[imageProp].push(tempAttachment);
            }

            // push or replace the single attachment
            if (this.workOrder.WorkOrderAttachments.length) {                                
                this.workOrder.WorkOrderAttachments[0] = this.workOrder[imageProp][0];                
            } else {
                this.workOrder.WorkOrderAttachments.push(this.workOrder[imageProp][0]);
            }
        }

        /**
         * WORK ORDER IMAGE
         * @description Display a modal to view the image.
         */
        viewPhoto = (imageProperty, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            // Open the photo manage dialog
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'workOrderImagePreview.tmpl.html',
                controller: 'Quality.WorkOrderImageModalCtrl as vm',
                size: 'md',
                resolve: {
                    imagePublicId: () => { return this.workOrder[imageProperty]; }
                }
            });

            this.modalInstance.result.then(() => {

            },() => { });
        }

        /** 
         * WORK ORDER
         * @description Submit the work order.
         */
        submitWorkOrder = ($event): void => {
            if ($event) $event.preventDefault();

            this.preSubmitCheck();

            var successRedirect = (res) => {
                if (!this.isManagerRole || (this.isManagerRole && this.currentMode === 'Edit')) {
                    mixpanel.track('Work order is created', { WorkOrderId: this.workOrder.WorkOrderId || res.WorkOrderId });
                    this.redirectToSubmitMessage('Thank You!', 'Your work order has been sent.');
                } else if (this.isManagerRole && this.currentMode === 'Close') {
                    mixpanel.track('Work order is closed', { WorkOrderId: this.workOrder.WorkOrderId || res.WorkOrderId });
                    this.redirectToSubmitMessage('Thank you!', 'Your work order has been closed.');
                } else {
                    mixpanel.track('Work order is managed', { WorkOrderId: this.workOrder.WorkOrderId || res.WorkOrderId });
                }
            };

            if (this.workOrder.WorkOrderId === 0 && !this.workOrderSeriesId) {
                this.workOrdersSvc.saveWorkOrder(this.workOrder).then(successRedirect);
            } else if (this.workOrder.WorkOrderId > 0 && !this.workOrderSeriesId) {
                this.workOrdersSvc.updateWorkOrder(this.workOrder.WorkOrderId, this.workOrder)
                    .then(successRedirect, this.onFailure);
            }
            else if (this.workOrder.WorkOrderId === 0 && this.workOrderSeriesId) {
                this.apiSvc.update(this.workOrderSeriesId, this.workOrder, "WorkOrderSeries").then(successRedirect, this.onFailure);
            }
        };

        /** 
         * PRESUBMIT CHECK
         * @description Model constraint checks prior to submit.
         */
        preSubmitCheck = () => {
            
            //Prefill Recurrenceinfo.StartDate with DueDate if empty
            //
            if (this.workOrder.RecurrenceInfo != null
                && (this.workOrder.RecurrenceInfo.StartDate == null || !this.workOrder.RecurrenceInfo.StartDate)) {
                console.log("no Recurrenceinfo.StartDate");
                this.workOrder.RecurrenceInfo.StartDate = this.workOrder.RequestDueDate || new Date();
            }

            if (this.workOrder.DueDate == null || !this.workOrder.DueDate || (this.workOrder.DueDate != this.workOrder.RequestDueDate && !this.workOrder.WorkOrderId)) {
                this.workOrder.DueDate = this.workOrder.RequestDueDate;
            }

            if (this.workOrder.FloorId == null) this.workOrder.FloorId = 0;

        }

        /**
         * CUSTOMER REPRESENTATIVES
         * @description Open a dialog to manage customer representatives and add to current list of customer representatives.
         */
        addNewCustomerRepresentative = ($event) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Common/Templates/modal.addEditCustomerRepresentative.tmpl.html',
                controller: 'Quality.CustomerRepresentativeModalCtrl as vm',
                size: 'md',
                resolve: {
                    customerRepresentatives: () => { return this.options.customerRepresentatives; },
                    customerRepresentative: () => {
                        return <qualityInterfaces.ICustomerRepresentative>{
                            CustomerRepresentativeId: 0,
                            Name: '',
                            Email: '',
                            JobDescription: ''
                        }
                    }
                }
            });

            this.modalInstance.result.then((customerRepresentative: qualityInterfaces.ICustomerRepresentative) => {
                if (customerRepresentative.CustomerRepresentativeId === 0) {
                    this.auditsScoreCardSvc.saveCustomerRepresentative(customerRepresentative)
                        .then((result) => {
                        customerRepresentative.CustomerRepresentativeId = result.CustomerRepresentativeId;
                        this.options.customerRepresentatives.push(customerRepresentative);
                    }, this.onFailure);
                }
            },() => { });
        }

        /**
         * @description Helper fn to find the lookup name by id.
         * @param {number} number
         * @param {string} lookup - from this.options
         */
        getNameByLookupId = (id: number, lookup: string) => {
            if (!id) return '';
            if (!this.options[lookup].length) {
                return '';
            }

            if (lookup === 'customerRepresentatives') {
                return _.find(this.options[lookup], { CustomerRepresentativeId: id })['Name'] || '';
            }
            else if (lookup === 'priorities') {
                return _.find(this.options[lookup], { PriorityId: id })['Name'] || '';
            }
            else {
                return _.find(this.options[lookup], { Key: id })['Value'] || '';
            }
        }

        /**
         * @description Redirect to a message page.
         */
        redirectToSubmitMessage = (header: string, text: string) => {
            this.isNavHidden = true;
            this.message = {
                header: header,
                text: text
            };

            this.$state.go('form.submitmessage');
        }

        /** 
         * EVENT HANDLERS
         * @description Cancel the data entry. 
         */
        cancel = ($event): void=> {
            $event.preventDefault();
            if (confirm('Are you sure? Your progress will not be saved.')) {
                if (this.workOrder.WorkOrderId !== 0) {
                    this.redirectToDetails();
                    return;
                }
                this.redirectToDetails();    
            }
        }

        /** 
         * EVENT HANDLER
         * @description Redirect to details page. 
         */
        redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/WorkOrder/Details');
        }

        /**
         * EVENT HANDLER
         * @description Performs action based on work order frequency.
         */
        onFrequencyChange = ($event, isRecurring: boolean): void => {
            if (!this.workOrderSeriesId) {
            this.workOrder.IsRecurring = isRecurring;
            var now = new Date();

            if (this.workOrder.IsRecurring) {
                //init recurrenceInfo
                if (this.workOrder.RecurrenceInfo == null)
                    this.workOrder.RecurrenceInfo = { StartDate: null, EndDate: null, RecurrenceTypeId: null, DueIn: null };

                this.workOrder.DueDate = null;
                this.workOrder.RecurrenceInfo.StartDate = this.minDate;
            } else {
                this.workOrder.DueDate = new Date();
                this.workOrder.RecurrenceInfo.StartDate = null;
                this.workOrder.RecurrenceInfo.EndDate = null;
                this.workOrder.RecurrenceInfo.RecurrenceTypeId = 0;
                this.workOrder.RecurrenceInfo.DueIn = null;
            }
        }
        }

        private checkIfNumber = (value: any) => {
            if (value && (!isNaN(value) || value.length > 1)) {
                return value.toString().replace(/[^0-9]/g, '');
            }
            return null;
        }

        /**
         * EVENT HANDLER
         * @description Performs action based on work order isInScope.
         */
        onIsInScopeChange = ($event, isInScope: boolean): void=> {
            this.workOrder.IsInScope = isInScope;

            if (this.workOrder.IsInScope) {
                this.workOrder.Cost = null;
                this.workOrder.PoNumber = null;
                this.workOrder.BillingDescription = null;
            }
        }

        /**
         * EVENT HANDLER
         * @description Performs action based on work order isBillable.
         */
        onIsBillableChange = ($event, isBillable: boolean): void=> {
            this.workOrder.IsBillable = isBillable;

            if (!this.workOrder.IsBillable) {
                this.workOrder.Cost = null;
                this.workOrder.PoNumber = null;
                this.workOrder.BillingDescription = null;
            }
        }

        /**
         * @description Helper to format filename before upload.
         * @param {string} fileName
         */
        getRandToken = () => {
            return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
        }
    }

    class WorkOrderImageModalCtrl {
        public static $inject: string[] = [
            '$scope',
            '$uibModalInstance',
            'imagePublicId'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private imagePublicId) {
        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel: () => void = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('Quality.WorkOrderEntryCtrl', WorkOrderEntryCtrl);
    angular.module('app').controller('Quality.WorkOrderImageModalCtrl', WorkOrderImageModalCtrl);
}