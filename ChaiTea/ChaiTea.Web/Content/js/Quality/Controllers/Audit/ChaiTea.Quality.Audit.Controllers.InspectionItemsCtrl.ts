﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    class NewInspectionItemsCtrl {
        private basePath: string = '';
        private auditProfileSection: qualityInterfaces.IAuditProfileSection = {
            AuditProfileSectionId: 0,
            AuditProfileId: 0,
            AreaId: 0,
            AreaName: null,
            AreaClassificationId: 0,
            AreaClassificationName: null,
            AssignedEmployees: [],
            InspectionItems: [],
            Profile: this.auditProfile
        };
        private areas: Array<qualityInterfaces.IKeyValuePair> = [];
        private areaClassifications: Array<qualityInterfaces.IKeyValuePair> = [];
        private inspectionItems: Array<qualityInterfaces.IKeyValuePair> = [];

        static $inject = [
            '$q',
            '$log',
            'sitesettings',
            '$uibModalInstance',
            'chaitea.quality.services.auditssvc',
            'chaitea.quality.services.lookuplistsvc',
            'auditProfile',
            'auditProfileSectionId'
        ];

        constructor(
            private $q: angular.IQService,
            private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private auditsSvc: qualitySvc.IAuditsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private auditProfile: qualityInterfaces.IAuditProfile,
            private auditProfileSectionId: number) {
            this.basePath = sitesettings.basePath;
            this.getAuditProfileSection(auditProfileSectionId);
        }

        /**
         * @description Get AuditProfileSection based on auditProfileSectionId.
         */
        getAuditProfileSection = (auditProfileSectionId: number) => {
            if (auditProfileSectionId <= 0) {
                this.auditProfileSection.AuditProfileId = this.auditProfile.AuditProfileId;
                this.getAreas(this.auditProfile.FloorId);
                return;
            }

            if (auditProfileSectionId > 0) {
                this.auditsSvc.getAuditProfileSection(auditProfileSectionId).then((result) => {
                    this.auditProfileSection = result;
                    this.auditProfile = result.Profile;

                    this.getAreas(this.auditProfile.FloorId);
                    this.getAreaClassificationsByArea(this.auditProfile.AuditProfileId, this.auditProfileSection.AuditProfileSectionId, this.auditProfileSection.AreaId);
                    this.getInspectionItemsByAreaClassification(this.auditProfileSection.AreaClassificationId);
                });
            }
        };

        /**
         * @description Get Areas based on floorId.
         */
        getAreas = (floorId: number) => {
            return this.lookupListSvc.getAreas({ id: floorId }).then((data) => {
                this.areas = data.Options;
            }, this.onFailure);
        };

        /**
         * @description On area change Get AreaClassification
         */
        onAreaChange = ($event): void=> {
            this.getAreaClassificationsByArea(this.auditProfile.AuditProfileId, this.auditProfileSection.AuditProfileSectionId, this.auditProfileSection.AreaId);
        };

        /**
         * @description Get AreaClassification based on auditProfileId, auditProfileSectionId and areaId.
         */
        getAreaClassificationsByArea = (auditProfileId: number, auditProfileSectionId: number, areadId: number) => {
            if (auditProfileId <= 0 || areadId <= 0) {
                return;
            }

            this.areaClassifications = [];
            this.inspectionItems = [];

            this.lookupListSvc.getAreaClassificationsForProfile(auditProfileId, auditProfileSectionId, areadId).then((data) => {
                this.areaClassifications = data.Options;
            }, this.onFailure);
        };

        /**
         * @description On AreaClassification change Get InspectionItems
         */
        onAreaClassificationChange = ($event): void=> {
            this.getInspectionItemsByAreaClassification(this.auditProfileSection.AreaClassificationId);
        };

        /**
         * @description Get InspectionItems based on AreaClassification
         */
        getInspectionItemsByAreaClassification = (areaClassificationId: number) => {
            if (areaClassificationId <= 0) {
                return;
            }

            this.inspectionItems = [];

            this.lookupListSvc.getAreaClassificationInspectionItems(areaClassificationId).then((result) => {
                this.inspectionItems = result.Options;
            }, this.onFailure);
        };

        /**
         * @description Create or Update AuditProfileSection
         */
        save = (): void => {
            var profileSection = _.omit(this.auditProfileSection, 'AreaClassificationName', 'AreaName', 'ProgramName');

            if (this.auditProfileSection.AuditProfileSectionId > 0) {
                // PUT
                this.auditsSvc.updateAuditProfileSection(this.auditProfileSection.AuditProfileSectionId, profileSection)
                    .then(this.onSuccess, this.onFailure);
            } else {
                // POST
                this.auditsSvc.saveAuditProfileSection(profileSection)
                    .then(this.onSuccess, this.onFailure);
            }
        };

        /**
         * @description Add or remove an inspection item from the list.
         */
        toggleInspectionItem = (id) => {
            if (this.auditProfileSection.InspectionItems.indexOf(id) > -1) {
                for (var i = 0; i < this.auditProfileSection.InspectionItems.length; i++) {
                    if (this.auditProfileSection.InspectionItems[i] === id) {
                        this.auditProfileSection.InspectionItems.splice(i, 1);
                        break;
                    }
                }
            }
            else {
                this.auditProfileSection.InspectionItems.push(id);
            }
        };

        /**
         * @description Toggle all selection for inspection items.
         */
        toggleAllInspectionItems = ($event) => {

            var checkbox = angular.element($event.currentTarget);

            this.auditProfileSection.InspectionItems = [];

            if (checkbox.prop("checked")) {
                angular.forEach(this.inspectionItems, (obj, index) => {
                    this.toggleInspectionItem(obj.Key * 1);
                });
            }
        };

        /**
         * @description Returns a boolean if the inspection item is found from the inspection items array.
         * @param {number} id
         */
        isInspectionItemSelected = (id: number) => {
            return this.auditProfileSection.InspectionItems.indexOf(id) > -1
        };

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            //this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.$uibModalInstance.close();
            bootbox.alert('Audit profile section was saved successfully!', this.redirectToProfileEntry);
        }

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        }

        /**
         * @description Return to the profile entry page.
         */
        redirectToProfileEntry = () => {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry/' +
                (this.auditProfileSection.AuditProfileId));
        }
    }

    angular.module('app').controller('Quality.InspectionItemsCtrl', NewInspectionItemsCtrl);
}