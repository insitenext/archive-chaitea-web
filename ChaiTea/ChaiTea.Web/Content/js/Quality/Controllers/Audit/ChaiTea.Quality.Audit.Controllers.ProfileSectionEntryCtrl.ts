﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    class AuditProfileSectionEntryCtrl {
        siteId: number = 0;
        isEntryDisabled: boolean = false;
        modalInstance;
        auditProfileId: number = 0;
        auditProfileSectionId: number = 0;
        basePath: string;

        auditProfile = {
            AuditProfileId: 0,
            ClientName: '',
            SiteName: '',
            BuildingName: '',
            FloorName: '',
            EmployeeName: '',
            FloorId: 0
        }

        auditProfileSection = {
            AuditProfileId: 0,
            AuditProfileSectionId: 0,
            AreaId: null,
            AreaClassificationId: null,
            InspectionItems: [],
            AssignedEmployees: []
        };

        options = {
            areas: [],
            areaClassifications: [],
            inspectionItems: [],
            employees: []
        };

        static $inject = [
            '$scope',
            '$uibModal',
            '$q',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.quality.services.auditssvc'];
        constructor(
            private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private auditsSvc: qualitySvc.IAuditsSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;

            this.modalInstance = this.$uibModal;

            //this.$scope.$watch('this.vm.auditProfileSection.InspectionItems', (item) => {
            //    this.$log.log(item);
            //}, true);
        }

        /** @description Initializer for the controller. */
        initialize = (auditProfileId, auditProfileSectionId): void => {
            this.auditProfileId = auditProfileId;
            this.auditProfileSectionId = auditProfileSectionId;

            if (this.auditProfileId == 0 && this.auditProfileSectionId == 0) return;

            this.isEntryDisabled = this.siteId == 0;

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return;
            }

            // Load section entry data
           
            this.auditProfileSection.AuditProfileId = auditProfileId;

            if (this.auditProfileSectionId > 0) {
                this.getAuditProfileSection(this.auditProfileSectionId);
            } else {
                this.getAuditProfile(this.auditProfileId);
            }

            //this.getPrograms();
            this.getEmployees();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            //this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            bootbox.alert('Audit profile section was saved successfully!', this.redirectToProfileEntry);
        }

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            this.redirectToProfileEntry();
        }

        /**
         * @description Return to the profile entry page.
         */
        redirectToProfileEntry = () => {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry/' +
                (this.auditProfileId || this.auditProfileSection.AuditProfileId));
        }

        /**
         * @description Get audit profile by auditProfileId.
         * @param {number} auditProfileId
         */
        getAuditProfile = (auditProfileId: number) => {
            this.auditsSvc.getAuditProfile(auditProfileId).then((result) => {
                this.auditProfile = result;
            }, this.onFailure);
        }

        /**
         * @description Get the audit profile section by auditProfileSectionId.
         * @param {number} auditProfileSectionId
         */
        getAuditProfileSection = (auditProfileSectionId: number): void => {
            this.auditsSvc.getAuditProfileSection(auditProfileSectionId).then((result) => {
                this.auditProfileSection = result;
                this.auditProfile = result.Profile;

                this.getAreas(this.auditProfile.FloorId);

                this.getAreaClassificationsByArea(
                    this.auditProfileSection.AuditProfileId,
                    this.auditProfileSection.AuditProfileSectionId,
                    this.auditProfileSection.AreaId);

                this.getInspectionItemsByAreaClassification(this.auditProfileSection.AreaClassificationId)
            }, this.onFailure);

        }

        /**
         * @description Save the profile section entry form.
         */
        saveProfileSectionEntryForm = (): void => {
            var profileSection = _.omit(this.auditProfileSection, 'AreaClassificationName', 'AreaName', 'ProgramName');

            if (this.auditProfileSectionId > 0) {
                // PUT
                this.auditsSvc.updateAuditProfileSection(this.auditProfileSectionId, profileSection)
                    .then(this.onSuccess, this.onFailure);
            } else {
                // POST
                this.auditsSvc.saveAuditProfileSection(profileSection)
                    .then(this.onSuccess, this.onFailure);
            }
        }

        getEmployees = () => {
            return this.lookupListSvc.getEmployees().then((data) => {
                this.options.employees = data.Options;
            }, this.onFailure);
        }

        getAreas = (floorId: number) => {
            return this.lookupListSvc.getAreas({ id: floorId }).then((data) => {
                this.options.areas = data.Options;
            }, this.onFailure);
        }


        onAreaChange = ($event) => {
            this.getAreaClassificationsByArea(this.auditProfile.AuditProfileId, this.auditProfileSectionId, this.auditProfileSection.AreaId);
        }

        onProgramChange = ($event) => {
            this.getAreaClassificationsByArea(this.auditProfile.AuditProfileId, this.auditProfileSectionId, this.auditProfileSection.AreaId);
        }

        /**
         * @description Get inspection items options by areaClassificationId.
         * @param {number}: areaClassificationId
         */
        onAreaClassificationChange = (areaClassificationId: number): void => {
            this.getInspectionItemsByAreaClassification(areaClassificationId);
        }

        /**
         * @description Get area classifications by auditProfileId and areaId.
         */
        getAreaClassificationsByArea = (auditProfileId: number, auditProfileSectionId: number, areaId: number): void => {
            this.options.areaClassifications = [];
            this.options.inspectionItems = [];
            if (auditProfileId > 0 && areaId > 0) {
                this.lookupListSvc.getAreaClassificationsForProfile(auditProfileId, auditProfileSectionId, areaId).then((data) => {
                    this.options.areaClassifications = data.Options;

                }, this.onFailure);
            } 
        }

        /**
         * @description Get inspection items by areaClassificationId.
         */
        getInspectionItemsByAreaClassification = (areaClassificationId: number) => {
            this.options.inspectionItems = [];
            if (!areaClassificationId) {
                return;
            };
            this.lookupListSvc.getAreaClassificationInspectionItems(areaClassificationId).then((data) => {
                this.options.inspectionItems = data.Options;
            }, this.onFailure);
        }


        /**
         * @description Add or remove an inspection item from the list.
         */
        toggleInspectionItem = (id) => {
            if (this.auditProfileSection.InspectionItems.indexOf(id) > -1) {
                for (var i = 0; i < this.auditProfileSection.InspectionItems.length; i++) {
                    if (this.auditProfileSection.InspectionItems[i] === id) {
                        this.auditProfileSection.InspectionItems.splice(i, 1);
                        break;
                    }
                }   
            }
            else {
                this.auditProfileSection.InspectionItems.push(id);
            }
        }

        /**
         * @description Toggle all selection for inspection items.
         */
        toggleAllInspectionItems = ($event) => {
            var $target = $event.currentTarget;

            this.auditProfileSection.InspectionItems = [];
            if ($target['checked'] == true) {
                angular.forEach(this.options.inspectionItems, (obj, index) => {
                    this.toggleInspectionItem(obj.Key*1);
                });
            }
        }

        /**
         * @description Open the modal instance and register submit callback.
         */
        assignEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'employeesModal.html',
                controller: 'AuditProfileSectionEmployeesModalCtrl as vm',
                size: 'md',
                resolve: {
                    employees: () => { return this.options.employees; },
                    selectedEmployees: () => { return this.auditProfileSection.AssignedEmployees; }
                }
            });

            this.modalInstance.result.then((selectedEmployees) => {
                this.auditProfileSection.AssignedEmployees = selectedEmployees;
            }, () => { });
        }

        /**
         * @description Return an employee object by employeeId.
         */
        getEmployeeById = (employeeId: number) => {
            return _.find(this.options.employees, (employee) => {
                return employee.Key == employeeId;
            });
        }

        /**
         * @description Returns a boolean if the inspection item is found from the inspection items array.
         * @param {number} id
         */
        isInspectionItemSelected = (id: number) => {
            return this.auditProfileSection.InspectionItems.indexOf(id) > -1
        }

        /**
         * @description Remove an employee object from the selected array.
         */
        removeEmployee = ($event, employeeId: number) => {
            $event.preventDefault();

            for (var i = 0; i < this.auditProfileSection.AssignedEmployees.length; i++) {
                if (this.auditProfileSection.AssignedEmployees[i] === employeeId) {
                    this.auditProfileSection.AssignedEmployees.splice(i, 1);
                    break;
                }
            }   
        }
    }

    class AuditProfileSectionEmployeesModalCtrl implements qualityInterfaces.IAuditProfileSectionEmployeesModalCtrl {

        static $inject = ['$scope', '$uibModalInstance', 'employees', 'selectedEmployees'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employees: any[],
            private selectedEmployees: any[]) {
    
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Close the modal instance and return the selected employees.
         */
        addEmployees = () => {
            this.$uibModalInstance.close(this.selectedEmployees);
        }
    }

    angular.module('app').controller('AuditProfileSectionEntryCtrl', AuditProfileSectionEntryCtrl);
    angular.module('app').controller('AuditProfileSectionEmployeesModalCtrl', AuditProfileSectionEmployeesModalCtrl);
}