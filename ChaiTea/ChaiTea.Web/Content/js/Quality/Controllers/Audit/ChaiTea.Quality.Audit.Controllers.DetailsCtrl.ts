﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    enum AuditFiler {
        Fails = 1,
        FailedAudits = 2,
        FailedInspectionItems = 3
    }

    enum AuditExport {
        Audit_Scores = 1,
        Inspection_Item_Scores = 2
    }


    class AuditDetailsCtrl {
        isManagerRole: boolean = false;
        isCrazyProgrammer: boolean = false;

        userId: number = 0;
        siteId: number = 0;
        isEntryDisabled: boolean = false;
        modalInstance;

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        audits = [];
        counts = {
            pending:0,
            completed:0
        };
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: new Date()
        };
        reportsConfig = {
            control: 3,     // goalLine
            minScale: 0,    // min scale
            scale: 5,       // max scale
            tickInterval: 1 // step
        }
        // Filter variables
        filterOptions = [
            { Key: 'AuditorId', Value: 'Auditor', Options: null, FieldType: 'select' },
            { Key: 'BuildingId', Value: 'Building', Options: null, FieldType: 'select' },
            {
                Key: 'IsJointAudit',
                Value: 'Audit Type',
                Options: [
                    {
                        Key: 'false',
                        Value: 'Internal'
                    },
                    {
                        Key: 'true',
                        Value: 'Joint'
                    }
                ],
                FieldType: 'select'
            },
            {
                Key: AuditFiler.Fails,
                Value: 'Fails',
                Options: [
                    {
                        Key: AuditFiler.FailedAudits,
                        Value: 'Failed Audits'
                    },
                    {
                        Key: AuditFiler.FailedInspectionItems,
                        Value: 'Failed Inspection Items'
                    }
                ],
                FieldType: 'select'
            }
        ];

        filter = {
            filterType: null,
            filterCriteria: null
        };

        //for sbmToggleSwitch
        boolFilter: boolean = false;

        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[] = [
            {
                title: 'Completed',
                onClickFn: () => { this.boolFilter = false; this.getAuditsReset() }
            },
            {
                title: 'Drafts',
                onClickFn: () => { this.boolFilter = true; this.getAuditsReset() }
            }
        ];

        isFilterCollapsed: boolean = true;
        localStorageAuditFilterKey: string;

        isCardView: boolean = true;
        module: string = 'audits';
        viewLink: string = '';
        viewCloseLink: string = '';
        headerText: string = 'AUDITED_ON';
        clientName: string = '';
        auditSettings: commonInterfaces.IAuditSettings = new Common.Interfaces.AuditSetting();
        isPageRefresh: boolean = true;
        exportOptions = [
            {
                Id: AuditExport.Audit_Scores,
                Name: 'Audit Scores'
            },
            {
                Id: AuditExport.Inspection_Item_Scores,
                Name: 'Inspection Item Scores'
            }
        ];
        exportId: number = null;
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.apibasesvc',
            '$filter',
            'chaitea.common.services.reportsvc',
            '$uibModal',
            'chaitea.common.services.localdatastoresvc',
            Common.Services.ImageSvc.id,
            Common.Services.NotificationSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $filter: angular.IFilterProvider,
            private reportHelperSvc: commonSvc.IReportSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private imageSvc: Common.Services.IImageSvc,
            private notificationSvc:Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.userId = userContext.UserId;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;

            this.getAllLookuplist();

            this.modalInstance = this.$uibModal;
            this.localStorageAuditFilterKey = 'audit-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;
            //setting filters from session storage.
            var filterObj = this.localDataStoreSvc.getObject(this.localStorageAuditFilterKey);
            if (filterObj) {
                this.filter = filterObj;
                this.isFilterCollapsed = false;
            }
            this.reportHelperSvc.getAuditSettings(userContext.Client.ID, userContext.Site.ID, true).then(res => {
                if (res) {
                    this.auditSettings = res;
                }
            });
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.isManagerRole = this.canUpsertAuditProfile();
            this.isCrazyProgrammer = this.userInfo.mainRoles.crazyprogrammers;
            //Remove building from filterOptions if a site has been chosen.
            if (!this.siteId) {
                _.remove(this.filterOptions,(data) => (data['Key'] == 'BuildingId'));
            }

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;
                if (this.isPageRefresh) {
                    this.setReportsConfig(obj);
                }
                else {
                    this.getAuditsReset({
                        startDate: this.dateFormatterSvc.formatDateFull(obj.startDate),
                        endDate: this.dateFormatterSvc.formatDateFull(obj.endDate)
                    });
                }
                this.isPageRefresh = false;
            });
        }

        /**
       * @description Set global reports configuration.
       */
        private setReportsConfig = (obj: any) => {
            return this.reportHelperSvc.getReportSettings().then((r) => {
                this.reportsConfig.control = r.ScoringProfileGoal;
                this.reportsConfig.minScale = r.ScoringProfileMinimumScore;
                this.reportsConfig.scale = r.ScoringProfileMaximumScore;
                this.getAuditsReset({
                    startDate: this.dateFormatterSvc.formatDateFull(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(obj.endDate)
                });
            });

        }

        /**
        * @description Returns whether the user is a manager.
        */
        private canUpsertAuditProfile = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} siteId
         */
        checkIfEntryIsDisabled = (siteId: number): boolean => {
            return (siteId === 0);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging and get complaints.
         */
        getAuditsReset = (dateRange?) => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.audits = [];
            this.recordsToSkip = 0;
            this.getAudits(dateRange);
        }

        /**
         * @description Open entryview page
         */
        public RedirectToEntryViewModal = (id): void => {
            window.location.href = this.basePath + 'Quality/Audit/EntryView/' + id;
        }

        /**
         * @description Open entry page
         */
        public RedirectToEntryModal = (id): void => {
            window.location.href = this.basePath + 'Quality/Audit/Entry/' + id;
        }

        /**
         * @description Get complaints and push to array for infinite scroll.
         */
        getAudits = (dateRange?: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Quality/Audit/Entry/';
            this.viewCloseLink = this.basePath + 'Quality/Audit/EntryView/';

            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(
                    moment(this.dateRange.endDate)
                        .add(1, 'day')
                        .startOf('day').toDate())
            };

            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var param = this.getPagingParams(dateRange);

            // SS: not sure why we are retrieving an unfiltered list of ALL audits - bad performance - looks like this call is a mistake since it is
            // followed by a similar call that is filtered.
            // this.auditsScoreCardSvc.getAuditScores();

            this.auditsScoreCardSvc.getAuditScores(param)
                .then((data) => {
                if (data.length == 0) {
                    this.isAllLoaded = true;
                }

                angular.forEach(data, (obj) => {
                    if (obj.AuditorAttachments) {
                        obj._auditorProfilePicture = this.imageSvc.getUserImageFromAttachments(obj.AuditorAttachments, true);
                    }
                    var todosCount = 0;
                    _.each(obj.ScorecardSections, function (item, index) {
                        var todos = _.pluck(_.where(item.ScorecardDetails, (val) => (val != null)), 'TodoItems.length');
                        todosCount += <number>_.reduce(todos, (ac: number, val: number) => (ac + val), 0);
                    });
                    if (obj.AuditorId == this.userId && obj.InProgress) {
                        obj.redirectionLink = this.basePath + 'Quality/Audit/Entry/';
                    }
                    if (obj.AuditorId != this.userId || !obj.InProgress) {
                        obj.redirectionLink = this.basePath + 'Quality/Audit/EntryView/';
                    }
                    obj.ToDos = todosCount;
                    this.audits.push(obj);
                });
                
                this.isBusy = false;
                this.recordsToSkip += this.top;

            }, this.onFailure);

        }

        public ShowEntryViewModal = (id: number): void => {
            this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.AuditEntryViewCtrl as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.audits, { 'AuditIt': id });
                    if (index != -1) {
                        this.audits.splice(index, 1);
                    }
                }
            });

        }
        /**
         * @description to export audit details to excel sheet 
         */
        private export = (dateRange: commonInterfaces.IDateRange): void => {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(
                    moment(this.dateRange.endDate)
                        .add(1, 'day')
                        .startOf('day').toDate())
            };
            var auditsToExport = [];
            var param = this.getPagingParams(dateRange);
            delete param['$top'];
            delete param['$skip'];
            this.auditsScoreCardSvc.getAuditScores(param)
                .then((scorecards) => {
                    angular.forEach(scorecards, (card) => {
                        var auditDate = moment(card.AuditDate);
                        angular.forEach(card.ScorecardSections, (section) => {
                            angular.forEach(section.ScorecardDetails, (detail) => {
                                if (this.exportId === AuditExport.Inspection_Item_Scores) {
                                    auditsToExport.push({
                                        "Site": card.SiteName,
                                        "Program": card.ProgramName,
                                        "Area": section.AreaName,
                                        "Building": card.BuildingName,
                                        "Floor": card.FloorName,
                                        "Area Classification Name": section.AreaClassificationName,
                                        "Area Classification Inspection Item Name": detail.AuditInspectionItemName,
                                        "Score": detail.Score || 'N/A',
                                        "Auditor Name": card.AuditorName,
                                        "Audit Type": card.IsJointAudit ? "Joint Audit" : "Internal Audit",
                                        "Audit Date": auditDate.format('MMM D') + "," + auditDate.format('YYYY HH:mm:ss')
                                    });
                                }
                            });
                        });
                        if (this.exportId === AuditExport.Audit_Scores) {
                            auditsToExport.push({
                                "Site": card.SiteName,
                                "Program": card.ProgramName,
                                "Building": card.BuildingName,
                                "Floor": card.FloorName,
                                "Score": card.Average || 'N/A',
                                "Auditor Name": card.AuditorName,
                                "Audit Type": card.IsJointAudit ? "Joint Audit" : "Internal Audit",
                                "Audit Date": auditDate.format('MMM D') + "," + auditDate.format('YYYY HH:mm:ss')
                            });
                        }
                    });
                    alasql('SELECT * INTO CSV("audit-details.csv",?) FROM ?', [mystyle, auditsToExport]);
                }, this.onFailure);
        }
        /**
         * @description Return a paging param for the OData filter.
         */
        getPagingParams = (dateRange: commonInterfaces.IDateRange) => {
            var params = <qualitySvc.IODataPagingParamModel>{
                $top: this.top,
                $skip: this.recordsToSkip,
                $orderby: 'AuditDate desc',
                $filter: `AuditDate gt DateTime'${dateRange.startDate}' and AuditDate lt DateTime'${dateRange.endDate}'`
            };
            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                if (this.filter.filterType == AuditFiler.Fails) {
                    if (this.filter.filterCriteria == AuditFiler.FailedAudits) {
                        params['$filter'] = params['$filter'] + " and Average lt " + this.reportsConfig.control;
                    }
                    else if (this.filter.filterCriteria == AuditFiler.FailedInspectionItems) {
                        params['$filter'] = params['$filter'] + ` and ScorecardSections/any(s:s/ScorecardDetails/any(d:d/Score lt ${this.reportsConfig.control}))`;
                    }
                }
                else {
                    var filter = `${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                    params['$filter'] += ` and ${filter}`;
                }

                //saving filter in session storage
                this.localDataStoreSvc.setObject(this.localStorageAuditFilterKey, this.filter);

            }
            else {
                //remove filter from session storage
                this.localDataStoreSvc.setObject(this.localStorageAuditFilterKey, '');
                this.isFilterCollapsed = true;
            }

            //Bool filter
            if (this.boolFilter !== null) {
                if (params['$filter'])
                    params['$filter'] = params['$filter'] + " and InProgress eq " + this.boolFilter;
                else {
                    params['$filter'] = "InProgress eq " + this.boolFilter;
                }

                //only show audits authored by the logged in user
                if (this.boolFilter) {
                    params['$filter'] += " and AuditorId eq " + this.userContext.UserId;            
                }
            }

            return params;
        }


        /**
        * LOOKUPS
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): angular.IPromise<any> => {
            var params = {
                $orderby: 'FirstName,LastName'
            };
            var lookupList = [
                this.tenantManagementSvc.getAuditorsByOData()
            ];

            //Add building to filterOptions if a site has been chosen.
            if (this.siteId) {
                lookupList.push(this.lookupListSvc.getBuildings());
            }

            return this.$q.all(lookupList).then((result:Array<any>) => {
                this.filterOptions[0].Options = _.map(result[0],(obj: any) => {
                    return { Key: obj.Id, Value: `${obj.FirstName} ${obj.LastName}` }
                });

                if (this.siteId) {
                    this.filterOptions[1].Options = result[1].Options;
                }
            }, this.onFailure);
        }


        /*
         * @description Delete audit
         */
        deleteAudit = ($event, $index:number, audit: qualityInterfaces.IAuditDetail): void => {
            $event.preventDefault();
            var that = this;

            bootbox.confirm('Are you sure you want to delete this audit?', (result) => {
                if (result) {
                    this.auditsScoreCardSvc.deleteAuditScore(audit.ScorecardId).then(function(data) {
                        that.audits.splice($index, 1);
                        that.notificationSvc.successToastMessage("Audit deletion successful.");
                    }, function (error) {
                        that.notificationSvc.errorToastMessage();
                        that.$log.error(error);
                    });
                }
            });
        }

        /*
         * @description Unfinalize completed Audit ( CrazyProgrammer role ONLY )
         */
        unfinalize = ($event, $index: number, audit: qualityInterfaces.IAuditDetail): void => {
            $event.preventDefault();
            var that = this;

            bootbox.confirm('Unfinalize this audit??',(result) => {
                if (result) {

                    if (!audit.InProgress)
                        audit.InProgress = true;

                    this.auditsScoreCardSvc.updateAuditScore(audit.ScorecardId, audit).then(function(data) {
                        that.notificationSvc.successToastMessage("Audit has been unfinalized. Reloading.");
                        setTimeout(function() { window.location.reload(); }, 2000);
                    }, function (error) {
                        var errorMessage = "Oops! There was a problem.";
                        audit.InProgress = false;
                        that.notificationSvc.errorToastMessage(errorMessage + "<br/><code>" + error + "</code>");
                        });
                }
            })
        }
    }

    angular.module('app').controller('Quality.AuditDetailsCtrl', AuditDetailsCtrl);
}