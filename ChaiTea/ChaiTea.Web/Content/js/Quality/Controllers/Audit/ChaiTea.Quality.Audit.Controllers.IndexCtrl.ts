﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import qualitySvc = Quality.Services;
    import coreSvc = Core.Services;
    import commonInterfaces = Common.Interfaces;
    import commonSvc = Common.Services;
    import serviceLookupListSvc = ChaiTea.Services;

    class AuditIndexCtrl {
        isManagerRole: boolean = false;
        currentDate = new Date();
        failColor: string = '#EF5C48';
        goalColor: string = '#48c101';

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').toDate(),
            endDate: new Date()
        };
        basePath: string;

        chartSettings: IChartSettings;
        goalLineWidth = 4;

        valueFormatter(val) {
            return val.toFixed(2);
        }

        valueFormatterInt(val) {
            return val.toFixed(0);
        }

        reportsConfig = {
            control: 3,         // goalLine
            minScale: 0,        // min scale
            scale: 5,           // max scale
            tickInterval: 1,    // step
            counts: {
                auditTrendsCurrentMonth: { control: 0, scale: 0, count: 0, jointCount: 0 },
                auditTrends: { totalcount: 0, matchGoalcount: 0, jointCount: 0, outOf: 0 },
                auditTrendsBySite: { matchGoalcount: 0, outOf: 0 },
                auditTrendsByArea: { matchGoalcount: 0, outOf: 0 },
                auditTrendsByProgram: { matchGoalcount: 0, outOf: 0 }
            },

            auditTrends: {},
            auditTrendsBySite: {},
            auditTrendsByArea: {},
            auditTrendsByProgram: {},
            programs: [],
            audits: []
        };
        auditSettings: commonInterfaces.IAuditSettings = new Common.Interfaces.AuditSetting();
        noOfMonths: number = 0;
        counts = {
            averageInternalBuilding: 0,
            averageJointBuilding: 0,
            buildingsFailing: 0
        };
        scoresTitle: string = 'Building';
        module: string = "audits";
        viewLink: string = '';
        viewCloseLink: string = '';
        failedAuditsCount: number = 0;
        headerText: string = 'AUDITED_ON';
        clientName: string = '';
        static $inject = [
            '$scope',
            '$q',
            '$filter',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.reportssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.quality.services.auditsscorecardsvc',
            '$uibModal',
            Common.Services.ImageSvc.id
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: qualitySvc.IReportsSvc,
            private lookuplistSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: serviceLookupListSvc.IBaseSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private imageSvc: Common.Services.IImageSvc) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.failColor = sitesettings.colors.coreColors.danger;
            this.goalColor = sitesettings.colors.secondaryColors.teal;
            this.scoresTitle = this.userContext.Site.ID == 0 ? 'Site' : this.userContext.Program.Name == 'Security' ? 'Post' : this.scoresTitle;
            this.clientName = userContext.Client.Name;
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                async.parallel([
                    (callback) => {
                        this.setReportsConfig()
                            .then(() => {
                                this.dateRange.startDate = obj.startDate;
                                this.dateRange.endDate = obj.endDate;
                                this.noOfMonths = Math.abs(
                                    moment(this.dateRange.startDate)
                                        .diff(
                                        moment(this.dateRange.endDate), 'months')) + 1;
                                callback(null, '');
                            });
                    },
                    (callback) => {
                        this.reportHelperSvc.getAuditSettings(this.userContext.Client.ID, this.userContext.Site.ID, true)
                            .then(res => {
                                if (res) {
                                    this.auditSettings = res;
                                }
                                callback(null, '');
                            });
                    }
                ], (err: Error, results: any) => {
                    this.refreshCharts({
                        startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                        endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                    });
                })
            });
            this.getPrograms();
        }

        /** @description Initializer for the controller. */
        public initialize = (): void => {
            this.isManagerRole = this.canUpsertAuditProfile();
        }

        /**
        * @description Returns whether the user is a manager.
        */
        private canUpsertAuditProfile = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Set global reports configuration.
         */
        private setReportsConfig = (): angular.IPromise<any> => {
            var def = this.$q.defer();
            async.waterfall([
                (callback) => {
                    if (this.userContext.Site.ID) {
                        // Get Sites Report Settings
                        this.reportHelperSvc.getReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    } else {
                        // Get All Sites Combined Report Settings
                        this.reportHelperSvc.getAllSitesCombinedSiteReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    }
                },
                (res, callback) => {
                    if (res) {
                        this.reportsConfig.control = res.ScoringProfileGoal;
                        this.reportsConfig.minScale = res.ScoringProfileMinimumScore;
                        this.reportsConfig.scale = res.ScoringProfileMaximumScore;

                        // Adjust steps based on scale
                        this.reportsConfig.tickInterval = this.reportHelperSvc.getTickIntervalByScale(this.reportsConfig.scale);
                        this.goalLineWidth = this.reportsConfig.control != 0 ? this.goalLineWidth : 0;
                    }
                    callback(null, res);
                }],
                (err, result) => {
                    if (err) {
                        this.onFailure(err);
                        def.reject(err);
                    }
                    def.resolve(result);
                });
            return def.promise;
        }

        /**
         * @description Open details page
         */
        public RedirectToEntryViewModal = (id): void => {
            window.location.href = this.basePath + 'Quality/Audit/EntryView/' + id;
        }

        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'Quality.AuditEntryViewCtrl as vm',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            }).result.then((res) => {
                if (res) {
                    var index = _.findIndex(this.reportsConfig.audits, { 'AuditIt': id });
                    if (index != -1) {
                        this.reportsConfig.audits.splice(index, 1);
                    }
                }
            });
        }

        private getRecentAudits = (dateRange: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Quality/Audit/Entry/';
            this.viewCloseLink = this.basePath + 'Quality/Audit/EntryView/';

            this.reportsConfig.audits = [];

            dateRange = {
                startDate: this.dateFormatterSvc.formatDateFull(moment(this.dateRange.startDate).toDate()),
                endDate: this.dateFormatterSvc.formatDateFull(
                    moment(this.dateRange.endDate)
                        .add(1, 'day')
                        .startOf('day').toDate())
            };

            var params = <qualitySvc.IODataPagingParamModel>{
                $top: 5,
                $skip: 0,
                $orderby: 'AuditDate desc',
                $filter: `AuditDate gt DateTime'${dateRange.startDate}' and AuditDate lt DateTime'${dateRange.endDate}' and InProgress eq false`
            };

            this.auditsScoreCardSvc.getAuditScores(params)
                .then((data) => {
                    angular.forEach(data, (obj) => {
                        if (obj.AuditorAttachments) {
                            obj._auditorProfilePicture = this.imageSvc.getUserImageFromAttachments(obj.AuditorAttachments, true);
                        }
                        var todosCount = 0;
                        _.each(obj.ScorecardSections, function (item, index) {
                            var todos = _.pluck(_.where(item.ScorecardDetails, (val) => (val != null)), 'TodoItems.length');
                            todosCount += <number>_.reduce(todos, (ac: number, val: number) => (ac + val), 0);
                        });
                        if (obj.AuditorId == this.userContext.UserId && obj.InProgress) {
                            obj.redirectionLink = this.basePath + 'Quality/Audit/Entry/';
                        }
                        if (obj.AuditorId != this.userContext.UserId || !obj.InProgress) {
                            obj.redirectionLink = this.basePath + 'Quality/Audit/EntryView/';
                        }
                        obj.ToDos = todosCount;
                        this.reportsConfig.audits.push(obj);
                    });
                }, this.onFailure);
        }

        private getFailedAuditsCount = (dateRange: commonInterfaces.IDateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            };
            this.apiSvc.query(params, 'Scorecard/FailedAuditsCount/:startDate/:endDate', false, false).then((result) => {
                this.failedAuditsCount = result.FailedAuditsCount;
            }, this.onFailure);
        }

        /**
         * @description Audit Trends
         */
        private getAuditTrends = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getAuditTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var categories = result.Categories,
                    internalData = _.map(result.InternalAuditData, (item: number) => (item == null) ? null: (_.round(item, this.auditSettings.ChartRoundingScale))),
                    jointData = _.map(result.JointAuditData, (item: number) => (item == null) ? null : (_.round(item, this.auditSettings.ChartRoundingScale))),
                    interalDataCopy = angular.copy(internalData),
                    jointDataCopy = angular.copy(jointData);

                this.counts.buildingsFailing = 0;

                this.counts.averageInternalBuilding = (internalData.length == 0) ? 0 : (_.reduce(interalDataCopy, function (a: number, b: number) {
                    return a + b;
                }, 0) / _.filter(interalDataCopy, val => val != null).length);

                this.counts.averageJointBuilding = (jointData.length == 0) ? 0 : (_.reduce(jointDataCopy, function (a: number, b: number) {
                    return a + b;
                }, 0) / _.filter(jointDataCopy, val => val != null).length);

                angular.forEach(jointData, (item) => {
                    this.counts.buildingsFailing += ((item != null) && (item < this.reportsConfig.control)) ? 1 : 0;
                });

                // Build the chart
                this.reportsConfig.auditTrends = this.chartConfigBuilder('auditTrends', {
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        max: this.reportsConfig.scale,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                    },
                    series: [
                        {
                            name: 'INTERNAL',
                            lineWidth: 3,
                            data: internalData
                        },
                        {
                            name: 'JOINT',
                            lineWidth: 3,
                            data: jointData
                        }]
                });
                var tempJointData = angular.copy(jointData);

                var totalData = angular.copy(jointData);
                totalData = totalData.concat(internalData);


                this.reportsConfig.counts.auditTrends = {
                    totalcount: _.remove(totalData, n => (n)).length,
                    matchGoalcount: _.remove(tempJointData, n => (n >= this.reportsConfig.control)).length,
                    outOf: categories.length,
                    jointCount: 0
                };


            });
        }

        /**
         * @description Site Audits
         */
        private getAuditTrendsBySite = (dateRange: commonInterfaces.IDateRange) => {
            var buildingParams = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            };
            this.chartsSvc.getAuditTrendsBySite({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var categories = result.Categories,
                    internalData = _.map(result.InternalData, (item: number) => (_.round(item, this.auditSettings.ChartRoundingScale))),
                    jointData = _.map(result.JointData, (item: number) => (_.round(item, this.auditSettings.ChartRoundingScale)));

                // Build the chart
                this.reportsConfig.auditTrendsBySite = this.chartConfigBuilder('auditTrendsBySite', {
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        max: this.reportsConfig.scale,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                    },
                    series: [
                        {
                            name: 'INTERNAL',
                            data: _.map(internalData, (data: number) => {
                                return { y: (angular.isNumber(data) ? data : 0) }
                            })
                        },
                        {
                            name: 'JOINT',
                            data: _.map(jointData, (data: number) => {
                                return { y: (angular.isNumber(data) ? data : 0) }
                            })
                        }],
                    //height: categories.length == 1 ? categories.length * 200 : categories.length * 115,
                    height: categories.length == 1 ? categories.length * 200 : categories.length * 150,
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                align: "right", color: "#fff", enabled: true, inside: true, x: -10
                            },
                            groupPadding: .15
                        }
                    }
                });
                var tempJointData = angular.copy(jointData);
                // Set counts
                this.reportsConfig.counts.auditTrendsBySite = {
                    matchGoalcount: _.remove(tempJointData, n => (n >= this.reportsConfig.control)).length,
                    outOf: categories.length
                };

            });
        }

        /**
         * @description Audits by Area
         */
        private getAuditTrendsByArea = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getAuditTrendsByAreaClassification({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {

                if (!result.AuditsAndCategories) return;

                var categories = [],
                    internalData = [],
                    jointData = [];

                angular.forEach(result.AuditsAndCategories, (data) => {
                    categories.push(data.Category);
                    internalData.push(
                        _.round(data.InternalAuditData, this.auditSettings.ChartRoundingScale));
                    jointData.push(
                        _.round(data.JointAuditData, this.auditSettings.ChartRoundingScale));
                });
                
                // Build the chart
                this.reportsConfig.auditTrendsByArea = this.chartConfigBuilder('auditTrendsByArea', {
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        max: this.reportsConfig.scale,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: { text: '' }
                        }],
                    },
                    series: [
                        {
                            name: 'INTERNAL',
                            data: _.map(internalData, (data: number) => {
                                return { y: data || 0 }
                            })
                        },
                        {
                            name: 'JOINT',
                            data: _.map(jointData, (data) => {
                                return { y: data || 0 }
                            })
                        }
                    ],
                    height: categories.length == 1 ? categories.length * 200 : categories.length * 115,
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                align: "right", color: "#fff", enabled: true, inside: true, x: -10
                            },
                            groupPadding: .15
                        }
                    }
                });
                var tempJointData = angular.copy(jointData);
                // Set counts
                this.reportsConfig.counts.auditTrendsByArea = {
                    matchGoalcount: _.remove(tempJointData, n => (n >= this.reportsConfig.control)).length,
                    outOf: categories.length
                };

            });
        }

        /**
         * @description Audit by Programs.
         */
        getAuditTrendsByProgram = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getAuditTrendsByProgram({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.Categories) return;

                var categories = result.Categories,
                    internalData = _.map(result.InternalData, (item: number) => (_.round(item, this.auditSettings.ChartRoundingScale))),
                    jointData = _.map(result.JointData, (item: number) => (_.round(item, this.auditSettings.ChartRoundingScale)));
                
                // Build the chart
                this.reportsConfig.auditTrendsByProgram = this.chartConfigBuilder('auditTrendsByProgram', {
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        max: this.reportsConfig.scale,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Internal',
                            data: _.map(internalData, (data) => {
                                return { y: data || 0 }
                            })
                        },
                        {
                            name: 'Joint',
                            data: _.map(jointData, (data) => {
                                return { y: data || 0 } //data || 0 < 3 ? '#ef5c48' : '#85c628'
                            })
                        }
                    ],
                    //height: categories.length * 180
                    height: categories.length == 1 ? categories.length * 200 : categories.length * 150,
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                align: "right", color: "#fff", enabled: true, inside: true, x: -10
                            },
                            groupPadding: .15
                        }
                    }
                });

                var tempJointData = angular.copy(jointData);

                // Set counts
                this.reportsConfig.counts.auditTrendsByProgram = {
                    matchGoalcount: _.remove(tempJointData, n => (n >= this.reportsConfig.control)).length,
                    outOf: categories.length
                };

            });
        }

        /**
         * @description Get programs.
         */
        getPrograms = (): void => {
            if (this.userContext.Program.ID > 0) {
                this.reportsConfig.programs.push({ Key: this.userContext.Program.ID, Value: this.userContext.Program.Name });
            } else {
                this.lookuplistSvc.getPrograms().then((result) => {
                    this.reportsConfig.programs = result.Options;
                });
            }

        }

        /**
         * @description Get locations data by program.
         * @param {number} programId
         */
        getLocationsResultsByProgram = (programId: number) => {
            var program = _.find(this.reportsConfig.programs, { Key: programId });

            if (program.Locations) return;

            var dateRange: commonInterfaces.IDateRange = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate)
            }

            this.chartsSvc.getAuditGridByProgram(programId, dateRange).then((result) => {
                // Round all scores
               result.Data = _.map(result.Data, (item:any) => {
                    item.TotalAverageScore = _.round(item.TotalAverageScore, this.auditSettings.ChartRoundingScale);
                    item.InternalAverageScore = _.round(item.InternalAverageScore, this.auditSettings.ChartRoundingScale);
                    item.JointAverageScore = _.round(item.JointAverageScore, this.auditSettings.ChartRoundingScale);
                    return item;
                })
                program.Locations = result;
            });
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            // Clear the location and program data
            angular.forEach(this.reportsConfig.programs, (program) => {
                program.IsToggled = false;
                if (program.Locations) program.Locations = null;
            });

            this.getAuditTrends(dateRange);
            this.getAuditTrendsBySite(dateRange);
            this.getRecentAudits(dateRange);
            if (this.userContext.Program.ID > 0) {
                this.getAuditTrendsByArea(dateRange);
            } else {
                this.getAuditTrendsByProgram(dateRange);
            }
            this.getFailedAuditsCount(dateRange);
        }

        /**
         * @description Defines overridable chart options.
         */
        chartConfigBuilder = (name, options) => {
            var chartOptions = {
                auditTrends: {
                    xAxis: {
                        tickColor: 'white',
                        categories: []
                    },
                    yAxis: {
                        title: '',
                        //tickInterval: this.reportsConfig.tickInterval, //if scale is 1-5
                        min: 0,
                        max: this.reportsConfig.scale,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',
                        plotLines: [
                            {
                                //value: 5,
                                value: this.reportsConfig.control,
                                color: '#00afd1',
                                width: this.goalLineWidth,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ]
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    series: [{}, {}],
                    plotOptions: {
                        dataLabels: {
                            enabled: true,
                            inside: true,
                            align: 'center',
                            color: '#fff',
                            y: -10
                        },
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }
                    }
                },
                auditTrendsBySite: {
                    xAxis: {
                        tickColor: 'white',
                        categories: []
                    },
                    yAxis: [{
                        tickInterval: this.reportsConfig.tickInterval,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',

                        title: '',
                        //min: 1,
                        max: this.reportsConfig.scale,
                        allowDecimals: false,

                        plotLines: [{
                            value: this.reportsConfig.control,
                            color: '#00afd1',
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: { text: '' }
                        }]
                    }, {

                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }],
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    series: [{}, {}],
                    plotOptions: {
                        min: 80,
                        max: 100,
                        bar: {
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }
                    }
                },
                auditTrendsByArea: {

                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: []
                    },
                    series: [{}, {}],
                    yAxis: [
                        {
                            tickInterval: this.reportsConfig.tickInterval,//1,
                            min: 0,
                            max: this.reportsConfig.scale,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            },
                            plotLines: [
                                {
                                    value: this.reportsConfig.control,
                                    color: '#00afd1',
                                    width: this.goalLineWidth,
                                    zIndex: 4,
                                    label: { text: '' }
                                }
                            ],
                        }, {
                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    plotOptions: {
                        min: 80,
                        max: 100,

                        bar: {
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }

                    }
                },
                auditTrendsByProgram: {
                    series: [{}, {}],
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        categories: []
                    },
                    yAxis: [
                        {
                            tickInterval: this.reportsConfig.tickInterval,//1,
                            min: 0,
                            max: this.reportsConfig.scale,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: '#e4e4e4',
                            title: {
                                text: ''
                            },
                            plotLines: [
                                {
                                    value: this.reportsConfig.control,//3,
                                    color: '#00afd1',
                                    width: this.goalLineWidth,
                                    zIndex: 4,
                                    label: { text: '' }
                                }
                            ]
                        }, {
                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    plotOptions: {
                        min: 80,
                        max: 100,
                        bar: {
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        itemStyle: {
                            lineHeight: '10px'
                        }

                    }
                }
            }
            return _.assign(chartOptions[name], options);
        }

        /**
        * DOM MANIPULATION
        */
        private toggleProgram = ($event, program) => {
            // Get the locations
            this.getLocationsResultsByProgram(program.Key);

            // Toggle the locations the Angular way.
            program.IsToggled = !program.IsToggled;
            angular.forEach(_.reject(this.reportsConfig.programs,
                p => (p.Key == program.Key)),
                p => { p.IsToggled = false; });
        }
    }

    angular.module('app').controller('Quality.AuditIndexCtrl', AuditIndexCtrl);
}  
