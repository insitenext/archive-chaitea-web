﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import commonSvc = Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IUserAttachment {
        UserAttachmentType: string;
        UniqueId: string;
    }
    enum FileType { Undefined, ProfilePicture };

    class AuditEntryViewCtrl {
        isManagerRole: boolean = false;

        scoreCardId: number = 0;
        clientId: number = 0;
        clientName: string = '';
        siteId: number = 0;
        programId: number = 0;
        modalInstance;
        auditProfileSections = [];
        failColor: string = '#EF5C48';

        options = {
            employees: [],
            employeeIds: [],
            employeesWithImage: []
        };

        basePath: string;
        audit = {
            ScorecardId: 0,
            InProgress: true,
            BuildingId: 0,
            FloorId: 0,
            ScorecardSections: [],
            CustomerRepresentatives: [],
            AverageScore: 0,
            ProfileId: 0
        };
        reportsConfig = {
            control: 3,     // goalLine
            minScale: 0,    // min scale
            scale: 5,       // max scale
            tickInterval: 1 // step
        }
        sectionFilters: any = [
            {
                'filterId': 0,
                'filterName': 'Scored',
                'filterValue': 'scored'
            },
            {
                'filterId': 1,
                'filterName': 'Unscored',
                'filterValue': 'unscored'
            },
            {
                'filterId': 2,
                'filterName': 'Failed',
                'filterValue': 'failed'
            },
        ]

        filterSelected:number=0;

        valueFormatter(val) {
            return val.toFixed(2);
        }

        rotateTracking: Array<{ id: string; css: any; }> = [];
        moduleIcon: string;
        moduleClass: string;
        customerRepresentativeTemplate: string;
        modalLeftSection: string;
        modalRightSection: string;
        employeesWithImage = [];
        allEmployees = [];
        selectedEmployeeText = "ASSIGNED_EMPLOYEES"
        isList: boolean = true;
        selectedSectionId: number = 0;
        selectedScorecardSection: any = [];
        zoomScale: number;
        auditSettings: commonInterfaces.IAuditSettings = new Common.Interfaces.AuditSetting();
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.quality.services.auditssvc',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.employeesvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.apibasesvc',
            '$uibModalInstance',
            'id'
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: angular.IFilterService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private auditsSvc: qualitySvc.IAuditsSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private id: number) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;
            this.clientName = userContext.Client.Name;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.moduleClass = 'audit-popup';
            this.moduleIcon = 'audit';
            this.zoomScale = sitesettings.windowSizes.isPhone ? 1 : 0.5;
            this.modalLeftSection = this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.audits.leftsection.tmpl.html';
            this.modalRightSection = this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.audits.rightsection.tmpl.html';
            this.modalInstance = this.$uibModal;
            this.isManagerRole = this.userInfo.mainRoles.managers;

            this.reportHelperSvc.getAuditSettings(userContext.Client.ID, userContext.Site.ID, true).then(res => {
                if (res) {
                    this.auditSettings = res;
                }
            });

            this.setReportsConfig().then(() => {
                this.getAuditById(this.id);
            });

            mixpanel.track('View Audit', { AuditId: id });
        }

        /**
         * @description Set global reports configuration.
         */
        private setReportsConfig = () => {
            return this.reportHelperSvc.getReportSettings().then((r) => {
                this.reportsConfig.control = r.ScoringProfileGoal;
                this.reportsConfig.minScale = r.ScoringProfileMinimumScore;
                this.reportsConfig.scale = r.ScoringProfileMaximumScore;

                // Adjust steps based on scale
                this.reportsConfig.tickInterval = this.reportHelperSvc.getTickIntervalByScale(this.reportsConfig.scale);
            });

        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * SCORE CARD
         * @description Get audit data.
         * @param {int} scoreCardId
         */
        getAuditById = (scoreCardId: number): void => {
            this.auditsScoreCardSvc.getAuditScore(scoreCardId).then((result) => {
                this.audit = result;
                _.each(this.audit.ScorecardSections, function (obj, index) {
                    obj.IsToggled = index == 0 ? true : false;
                    obj.sectionContainsImages = false;
                    obj.Todos = 0;
                    var scores = _.pluck(_.where(obj.ScorecardDetails, (val) => (val != null)), 'Score');
                    //remove null scores for average
                    scores = _.without(scores, null);
                    obj.ScoreAverage = <number>_.reduce(scores, (ac: number, val: number) => (ac + val), 0) / scores.length;
                    var todos = _.pluck(_.where(obj.ScorecardDetails, (val) => (val != null)), 'TodoItems.length');
                    obj.ToDos = <number>_.reduce(todos, (ac: number, val: number) => (ac + val), 0);
                    var images = _.pluck(_.where(obj.ScorecardDetails, (val) => (val != null)), 'ScorecardDetailImages.length');
                    obj.Images = <number>_.reduce(images, (ac: number, val: number) => (ac + val), 0);
                    obj.TodoList = [];
                    var todoList = _.reject(_.pluck(_.where(obj.ScorecardDetails, (val) => (val != null)), 'TodoItems'), (arr) => {
                        return _.isEmpty(arr);
                    });
                    if (todoList.length > 0) {
                        _.each(todoList, function (todo, index) {
                            obj.TodoList.push(todo[0]);
                        });
                    }
                    // check if any scorecard has images
                    var images = _.pluck(obj.ScorecardDetails, 'ScorecardDetailImages');
                    var imageExist = _.max(images, (chr) => (chr.length));
                    obj.sectionContainsImages = imageExist.length > 0;
                    obj.filterSelected = 0;
                });
                var averageScores = _.pluck(this.audit.ScorecardSections, 'ScoreAverage');
                averageScores = _.where(averageScores, (val)=>(val >= 0));
                this.audit.AverageScore = <number>_.reduce(averageScores,(ac: number, val: number) => (ac + val), null) / averageScores.length;

                var param = <qualitySvc.IODataPagingParamModel>{
                    $orderby: 'AuditProfileSectionId asc',
                    $filter: 'AuditProfileId eq ' + this.audit.ProfileId
                };
                this.auditsSvc.getAuditProfileSections(param).then((result) => {
                    this.auditProfileSections = result;

                    this.getAllEmployees();

                }, this.onFailure);
            }, this.onFailure);
        }



        getEmployeesWithImage = (areaId: number): void=> {
            if (this.auditProfileSections.length > 0) {
                angular.forEach(this.auditProfileSections,(item) => {
                    if (item.AssignedEmployees.length > 0 && item.AreaId == areaId) {
                        this.options.employeeIds = item.AssignedEmployees;
                    }
                });
                angular.forEach(this.options.employeeIds, (item) => {
                    var check = _.find(this.employeesWithImage, { 'EmployeeId': item });
                    if (!check) {
                        this.apiSvc.getById(item, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee],false).then((employee) => {

                            this.employeesWithImage.push({
                                Name: employee.Name,
                                EmployeeId: employee.EmployeeId,
                                UserAttachments: employee.UserAttachments,
                                HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                                JobDescription: employee.JobDescription.Name,
                                ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true),
                                AreaId: areaId
                            });
                        }, this.onFailure);

                    }

                });
            }
        }

        private getAllEmployees = (): void => {
            if (this.auditProfileSections.length > 0) {
                for (var i = 0; i < this.auditProfileSections.length; ++i) {
                    if (this.auditProfileSections[i].AssignedEmployees.length > 0) {
                        _.each(this.auditProfileSections[i].AssignedEmployees, (item) => {
                            var check = _.find(this.options.employeeIds, { 'EmployeeId': item });
                            this.options.employeeIds.push({
                                EmployeeId: item,
                                AreaId: this.auditProfileSections[i].AreaId
                            });
                        });
                    }
                }

                var distinctEmployees = _.uniq(this.options.employeeIds, 'EmployeeId');

                angular.forEach(distinctEmployees, (item) => {
                    this.apiSvc.getById(item.EmployeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee], false).then((employee) => {

                        this.allEmployees.push({
                            Name: employee.Name,
                            EmployeeId: employee.EmployeeId,
                            UserAttachments: employee.UserAttachments,
                            HireDate: (employee.HireDate ? moment(employee.HireDate).format('MMM DD, YYYY') : 'N/A'),
                            JobDescription: employee.JobDescription.Name,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true)
                        });
                        this.employeesWithImage = this.allEmployees;
                    }, this.onFailure);


                });

            }
        }

        private backToList = (): void => {
            this.isList = true;
            this.employeesWithImage = [];
            this.employeesWithImage = this.allEmployees;
        }
        /**
         * @description View inspection images.
         */
        viewInspectionItemImages = (inspection, $event): void => {
            $event.preventDefault();

            if (!inspection.ScorecardDetailImages.length) return;

            this.modalInstance = this.$uibModal.open({
                templateUrl: 'auditInspectionViewImagesModal.tmpl.html',
                controller: 'Quality.AuditInspectionViewImagesModalCtrl as vm',
                size: 'md',
                resolve: {
                    inspection: () => { return inspection; }
                }
            }).result.then((imageCss: any) => {

                var id = inspection.ScorecardDetailImages[0].ImageId;

                var settingIndex = _.findIndex(this.rotateTracking, { id: id });
                if (settingIndex == -1) {
                    this.rotateTracking[id] = {
                        id: id,
                        css: imageCss
                    };
                } else {
                    this.rotateTracking[id].css = imageCss;
                }

            });
        }

        /**
         * @description View inspection todo items.
         */
        viewInspectionItemTodos = (inspection, $event): void=> {
            $event.preventDefault();

            if (!inspection.TodoItems.length) return;

            this.modalInstance = this.$uibModal.open({
                templateUrl: 'auditInspectionViewTodosModal.html',
                controller: 'Quality.AuditInspectionViewTodosModalCtrl as vm',
                size: 'md',
                backdrop: true,
                resolve: {
                    todoItems: () => { return inspection.TodoItems; }
                }
            });
        }

        /**
         * EVENT HANDLERS
         * @description Cancel the data entry.
         */
        cancel = ($event): void=> {
            $event.preventDefault();
            this.redirectToDetails();
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * EVENT HANDLERS
         * @description Redirect to details page.
         */
        redirectToDetails = (): void=> {
            window.location.replace(this.basePath + 'Quality/Audit/');
        }

        toggleSection = ($event, section) => {
            $event.preventDefault();
            var employees = _.filter(this.options.employeeIds, {'AreaId': section.AreaId});
            this.employeesWithImage = [];
            _.each(employees, (emp) => {
                var obj = _.find(this.allEmployees, { 'EmployeeId': emp.EmployeeId });
                this.employeesWithImage.push(obj);
            });

            this.isList = false;
            this.selectedSectionId = section.ScorecardSectionId;
            var selectedSection = _.filter(this.audit.ScorecardSections, { 'ScorecardSectionId': section.ScorecardSectionId });
            this.selectedScorecardSection = selectedSection[0];
            this.selectedScorecardSection.filterSelected = 0;
            this.selectedScorecardSection.FilterBy = this.sectionFilters[0].filterValue;
            var todos = [];
            var that = this;
            _.each(this.selectedScorecardSection.ScorecardDetails, function (obj, index) {
                if (obj.ScorecardDetailImages.length > 0) {
                    obj.ScorecardDetailImages[0].mainImage = true;
                    obj.mainImagePublicId = obj.ScorecardDetailImages[0].PublicId;
                    obj.mainImageUrl = that.getAttachment(obj.mainImagePublicId);
                }
            });
        }
        private filterScorecardDetails = (element) => {
            this.selectedScorecardSection.FilteredScorecardDetails = [];
            var resultant = [];
            switch (this.selectedScorecardSection.FilterBy) {
                case this.sectionFilters[0].filterValue:
                    return element.Score != null ? true : false;
                case this.sectionFilters[1].filterValue:
                    return element.Score == null ? true : false;
                case this.sectionFilters[2].filterValue:
                    return (element.Score != null && element.Score < this.reportsConfig.control) ? true : false;
            }
        }

        private getAttachment = (uniqueId: any): string => {
            var img: string;
            img = this.awsSvc.getUrlByAttachment(uniqueId, commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image]);
            return img;
        }

        private makeMain = (inspection, image): void => {
            _.each(inspection.ScorecardDetailImages, function (obj, index) {
                if (obj.mainImage) {
                    obj.mainImage = false;
                }
            });
            var index = _.findIndex(inspection.ScorecardDetailImages, { 'ImageId': image.ImageId});
            inspection.ScorecardDetailImages[index].mainImage = true;
            inspection.mainImagePublicId = inspection.ScorecardDetailImages[index].PublicId;
            inspection.mainImageUrl = this.getAttachment(inspection.ScorecardDetailImages[index].PublicId);
        }

        toggleInspectionItem = ($event) => {
            $event.preventDefault();

            var element = angular.element($event.currentTarget);
            if (angular.element(window).width() < 768) {
                angular.element(".row-action-wrap").not(element.closest('.table-row').find(".row-action-wrap")).slideUp('fast');
                element.closest('.table-row').find(".row-action-wrap").slideToggle('fast');
            }
        }

        toggleInspectionComment = ($event) => {
            $event.preventDefault();

            var element = angular.element($event.currentTarget);
            element.next(".audit-comment-textarea").slideToggle('fast');
            angular.element(".audit-comment-textarea").width(angular.element(".table-title-well").width() + 30);
        }

    }

    class AuditInspectionViewImagesModalCtrl {
        currentImageView: qualityInterfaces.IScorecardDetailImage;

        storageId: string = 'quality:rotate:tracking';
        rotateTracking: Array<{ id: string; rotateDeg: number; width: number; height: number; }> = [];
        imageCss: any = {
            "-ms-transform": "rotate(0deg)",
            "-webkit-transform": "rotate(0deg)",
            "transform": "rotate(0deg)"
        };
        wrapperCss: any = { "display": "block" };

        static $inject = [
            '$scope',
            '$log',
            '$uibModalInstance',
            'inspection',
            'localStorageService',
            'chaitea.common.services.apibasesvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private inspection: qualityInterfaces.IScorecardDetail,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            if (inspection['ScorecardDetailImages'].length > 0) this.setCurrentImage(inspection['ScorecardDetailImages'][0]);
        }

        public rotateImage = (photo: qualityInterfaces.IScorecardDetailImage, reset?: boolean): void => {
            var storage: any = this.localStorageService.get(this.storageId);

            if (storage) this.rotateTracking = storage;

            var settingIndex = _.findIndex(this.rotateTracking, { id: photo.PublicId });
            var elem: any = angular.element('#current-image');
            var wrapper: any = angular.element('#audit-image-responsive');

            if (this.rotateTracking[settingIndex].rotateDeg == 0) {
                this.rotateTracking[settingIndex].height = (elem[0] ? elem[0].height : 0);
                this.rotateTracking[settingIndex].width = (elem[0] ? elem[0].width : 0);
            }

            if (!reset) {
                this.rotateTracking[settingIndex].rotateDeg = (this.rotateTracking[settingIndex].rotateDeg + 90 > 270 ? 0 : this.rotateTracking[settingIndex].rotateDeg + 90);

                //save settings
                this.localStorageService.set(this.storageId, <any>this.rotateTracking);

                //fire off reuqest to actually rotate the image on s3
                this.apiSvc.query({}, 'scorecarddetailimage/' + photo.ImageId + '/rotate/90', false, false).then((res: any) => {
                    //ignore res
                }, (error) => {
                    this.$log.error(error);
                });

            }

            //css rotate
            this.imageCss = {
                "-ms-transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)",
                "-webkit-transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)",
                "transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)"
            };

            //adjust wrapper
            this.wrapperCss = {
                "display": "block",
                "height": (this.rotateTracking[settingIndex].height ? this.rotateTracking[settingIndex].height + "px" : "auto")
            };
            if (this.rotateTracking[settingIndex].rotateDeg == 90 || this.rotateTracking[settingIndex].rotateDeg == 270) {
                this.wrapperCss = {
                    "display": "table-cell",
                    "height": (this.rotateTracking[settingIndex].width ? this.rotateTracking[settingIndex].width + "px" : "auto")
                };

            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel = (): void=> {
            this.$uibModalInstance.close(this.imageCss);

        };

        /**
         * @description Set active image for view.
         */
        public setCurrentImage = (photo: qualityInterfaces.IScorecardDetailImage) => {
            var storage: any = this.localStorageService.get(this.storageId);
            var settingIndex = _.findIndex(this.rotateTracking, { id: photo.PublicId });
            var elem: any = angular.element('#current-image');

            if (settingIndex == -1) {

                if (storage) this.rotateTracking = storage;

                this.rotateTracking.push({
                    id: photo.PublicId,
                    rotateDeg: 0,
                    height: (elem[0] ? elem[0].height : 0),
                    width: (elem[0] ? elem[0].width : 0)
                });
                settingIndex = this.rotateTracking.length - 1;

                this.localStorageService.set(this.storageId, <any>this.rotateTracking);
            }

            if (storage) {
                this.rotateImage(photo, true);
            }

            this.currentImageView = photo;
        }
    }

    class AuditInspectionViewTodosModalCtrl {
        currentImageView: qualityInterfaces.IScorecardDetailImage;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'todoItems'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private todoItems: qualityInterfaces.IAuditInspectionTodo[]) {
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('Quality.AuditEntryViewCtrl', AuditEntryViewCtrl);
    angular.module('app').controller('Quality.AuditInspectionViewImagesModalCtrl', AuditInspectionViewImagesModalCtrl);
    angular.module('app').controller('Quality.AuditInspectionViewTodosModalCtrl', AuditInspectionViewTodosModalCtrl);

}
