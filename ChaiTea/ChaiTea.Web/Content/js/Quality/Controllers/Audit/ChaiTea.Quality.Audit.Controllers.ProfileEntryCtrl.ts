﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    class AuditProfileEntryCtrl {
        auditProfileId: number = 0;
        siteId: number = 0;
        isEntryDisabled: boolean = false;
        modalInstance;
        assignedEmployees = [];
        basePath: string;
        isNew: boolean = false;
        allItemsSelected: boolean = false;
        selectedProfileSection: number = 0;
        profileSectionSelectedCount: number = 0;
        auditProfile = {
            AuditProfileId: 0,
            BuildingId: null,
            BuildingName: null,
            FloorId: null,
            FloorName: null
        };
        options = {
            buildings: [],
            floors: [],
            employees: [],
            scoringProfiles: [],
            employeeIds: [],
            employeesWithImage: [],
            areas: [],
            areaClassifications: [],
            inspectionItems: []
        };
        auditProfileSection = {
            AuditProfileSectionId: 0,
            AuditProfileId: 0,
            AreaId: 0,
            AreaName: null,
            AreaClassificationId: 0,
            AreaClassificationName: null,
            AssignedEmployees: [],
            InspectionItems: [],
            Profile: {}
        };
        auditProfileSections = [];

        static $inject = [
            '$q',
            '$scope',
            '$uibModal',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            'chaitea.quality.services.auditssvc',
            'chaitea.quality.services.employeesvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.imagesvc'];
        constructor(
            private $q: angular.IQService,
            private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private auditsSvc: qualitySvc.IAuditsSvc,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.modalInstance = this.$uibModal;
        }

        /** @description Initializer for the controller. */
        initialize = (auditProfileId: number): void => {
            this.auditProfileId = auditProfileId;
            this.isNew = this.auditProfileId == 0;
            this.isEntryDisabled = this.siteId == 0;

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return;
            }

            if (!this.isNew) {
                this.getAuditProfile(this.auditProfileId);
                this.getSectionsByAuditProfile(this.auditProfileId);
            }

            this.getAllLookuplist();
        }

        /** @description Select all checkboxes when table header checkbox is selected */
        selectAll = (): void => {
            for (var i = 0; i < this.auditProfileSections.length; i++) {
                this.auditProfileSections[i].isChecked = this.allItemsSelected;
            }

            this.allItemsSelected ? this.profileSectionSelectedCount = this.auditProfileSections.length : this.profileSectionSelectedCount = 0;
        }

        /** @description Select table header checkbox when all checkboxes are selected */
        selectEntity = (index: number): void => {

            this.auditProfileSections[index].isChecked ? this.profileSectionSelectedCount++ : this.profileSectionSelectedCount--;

            for (var i = 0; i < this.auditProfileSections.length; i++) {
                if (!this.auditProfileSections[i].isChecked) {
                    this.allItemsSelected = false;
                    return;
                }

                this.allItemsSelected = true;
            }
        };

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            bootbox.alert('Audit profile was saved successfully!',() => {
                // TODO: Response should provide the new auditProfileId
                if (this.isNew) {
                    window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry/' + response['AuditProfileId']);
                }
            });
        }

        onAuditProfileSectionSuccess = (response: any): void => {
            this.selectedProfileSection--;
            if (this.selectedProfileSection === 0) {
                bootbox.alert('Profile Section was saved successfully!',() => { });
                window.location.reload();
            }
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): void => {
            this.lookupListSvc.getBuildings().then((data) => {
                this.options.buildings = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getEmployees().then((data) => {
                this.options.employees = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getScoringProfiles().then((data) => {
                this.options.scoringProfiles = data.Options;
            }, this.onFailure);
        }

        /** 
         * @description Get floors based on buildingId. 
         * @param {number} buildingId
         */
        getFloorsByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                this.lookupListSvc.getFloors({ id: buildingId }).then((data) => {
                    this.options.floors = data.Options;
                }, this.onFailure);
            }
        }

        /** @description On building field change event. */
        onBuildingChange = this.getFloorsByBuildingId;

        /**
         * @description Gets the audit profile by auditProfileId.
         */
        getAuditProfile = (auditProfileId: number): void => {
            this.auditsSvc.getAuditProfile(auditProfileId).then((result) => {
                this.auditProfile = result;
                
                this.getFloorsByBuildingId(this.auditProfile.BuildingId);
            }, this.onFailure);
        }

        /**
         * @description Gets sections by auditProfileId.
         */
        getSectionsByAuditProfile = (auditProfileId: number) => {
            var param = <qualitySvc.IODataPagingParamModel>{
                $orderby: 'AuditProfileSectionId asc',
                $filter: 'AuditProfileId eq ' + auditProfileId
            };


            this.auditsSvc.getAuditProfileSections(param).then((result) => {
                this.auditProfileSections = result;

                if (result.length > 0) {
                    this.getEmployeesWithImage();
                }

            }, this.onFailure);
        }        

        /**
         * @description Gets Employees with Images by EmployeeIds.
         */
        getEmployeesWithImage = (): void=> {
            if (this.auditProfileSections.length > 0) {
                angular.forEach(this.auditProfileSections,(item) => {
                    if (item.AssignedEmployees.length > 0) {
                        this.options.employeeIds = _.union(this.options.employeeIds, item.AssignedEmployees);
                    }
                });

                this.employeeSvc.getEmployeeByIds(this.options.employeeIds).then((result) => {
                    if (result.length > 0) {
                        angular.forEach(result,(item) => {
                            this.options.employeesWithImage.push(<qualityInterfaces.IEmployeeWithImage>{
                                EmployeeId: item.EmployeeId,
                                EmployeeName: item.FirstName + ' ' + item.LastName,
                                UniqueId: this.getProfilePictureUniqueId(item.UserAttachments),
                                EmployeeImage: this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true)
                            });
                        });
                    }
                });
            }
        }

        /**
         * @description Look for EmployeeAttachment with ProfilePicture and returns Unique Id. 
         */
        getProfilePictureUniqueId = (employeeAttachments): string=> {

            var uniqueId: string = null;

            if (employeeAttachments.length > 0) {
                angular.forEach(employeeAttachments,(item) => {
                    if (item.UserAttachmentType === 'ProfilePicture') {
                        uniqueId = item.UniqueId;
                    }
                });
            }
            return uniqueId;
        }

        /**
         * @description Save an audit profile.
         */
        saveAuditProfile = (): void => {
            var profileEntry:any = _.omit(this.auditProfile, 'SiteName', 'BuildingName', 'FloorName', 'ResponsibleUsername', 'ScoringProfileDescription');
            
            //set to allow for no ResponsibleUser
            if (profileEntry.ResponsibleUserId == null) profileEntry.ResponsibleUserId = 0;

            if (this.auditProfileId > 0) {
                // PUT
                this.auditsSvc.updateAuditProfile(this.auditProfileId, profileEntry).then(this.onSuccess, this.onFailure);

            } else {
                // POST
                this.auditsSvc.saveAuditProfile(profileEntry).then(this.onSuccess, this.onFailure);
            }
        }

        /**
        * @description Open the modal instance for assign employees and register submit callback.
        */
        assignEmployees = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'employeesModal.html',
                controller: 'AuditProfileEntryEmployeesModalCtrl as vm',
                size: 'md',
                resolve: {
                    employees: () => { return this.options.employees; },
                    selectedEmployees: () => { return this.assignedEmployees; }
                }
            });

            this.modalInstance.result.then((selectedEmployees) => {
                this.buildAuditProfileSection(selectedEmployees);
                this.putAuditProfileSection();
            },() => { });
        }

        /**
        * @descrption Update assigned employees for each Audit Profile Section.
        */
        buildAuditProfileSection = (selectedEmployees: number[]): void => {
            for (var i = 0; i < this.auditProfileSections.length; i++) {
                if (this.auditProfileSections[i].isChecked) {
                    this.selectedProfileSection++;
                    this.auditProfileSections[i].AssignedEmployees = _.union(this.auditProfileSections[i].AssignedEmployees, selectedEmployees);
                }
            }
        }

        /**
        * @description Put updated assigned employees for each Audit Profile Section on server.
        */
        putAuditProfileSection = (): void => {
            for (var i = 0; i < this.auditProfileSections.length; i++) {
                if (this.auditProfileSections[i].isChecked) {
                    var profileSection = _.omit(this.auditProfileSections[i], 'AreaClassificationName', 'AreaName', 'ProgramName');
                    if (this.auditProfileSections[i].AuditProfileSectionId > 0) {
                        // PUT
                        this.auditsSvc.updateAuditProfileSection(this.auditProfileSections[i].AuditProfileSectionId, profileSection)
                            .then(this.onAuditProfileSectionSuccess, this.onFailure);
                    }
                    this.auditProfileSections[i].isChecked = false;
                }
            }

            this.allItemsSelected = false;
        }

        /**
        * @description Open the modal instance to show assigned employees
        */
        showAssignedEmployees = (index: number): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'showAssignedemployeesModal.html',
                controller: 'AuditProfileSectionAssignedEmployeesModalCtrl as vm',
                size: 'md',
                resolve: {
                    employees: () => { return this.options.employeesWithImage; },
                    selectedEmployees: () => { return this.auditProfileSections[index].AssignedEmployees; }
                }
            });

            this.modalInstance.result.then((selectedEmployees) => {
                this.removeAssignedEmployeeThroughModal(index, selectedEmployees);
            },() => { });
        }

        /**
         * @description Open modal instance to either create or update AuditProfileSection
         */
        addOrUpdateProfileSection = (auditProfile: qualityInterfaces.IAuditProfile, auditProfileSectionId: number): void=> {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Quality/Templates/Audit/quality.audit.profileEntry.inspectionItems.tmpl.html',
                controller: 'Quality.InspectionItemsCtrl as vm',
                resolve: {
                    auditProfile: () => { return auditProfile; },
                    auditProfileSectionId: () => { return auditProfileSectionId; }
                }
            });
        }
     
        /**
        * @description removes assigned employees through show assigned employee modal 
        */
        removeAssignedEmployeeThroughModal = (index: number, selectedEmployees: number[]): void => {

            this.auditProfileSections[index].AssignedEmployees = _.union(this.auditProfileSections[index].AssignedEmployees, selectedEmployees);

            var profileSection = _.omit(this.auditProfileSections[index], 'AreaClassificationName', 'AreaName', 'ProgramName');
            if (this.auditProfileSections[index].AuditProfileSectionId > 0) {
                // PUT
                this.auditsSvc.updateAuditProfileSection(this.auditProfileSections[index].AuditProfileSectionId, profileSection)
                    .then(this.onSuccess, this.onFailure);
            }
        }

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            this.redirectToProfileManager();
        }

        /** @description Redirect to details page. */
        redirectToProfileManager = (): void=> {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileManager');
        }

        deleteAuditProfileSection = (id: number): void => {
            bootbox.confirm('Are you sure you want to delete this profile section?',(result) => {
                if (result) {
                    this.auditsSvc.deleteAuditProfileSection(id).then((result) => {
                        for (var i = 0; i < this.auditProfileSections.length; i++) {
                            if (this.auditProfileSections[i].AuditProfileSectionId === id) {
                                this.auditProfileSections.splice(i, 1);
                                break;
                            }
                        }
                    }, this.onFailure);
                }
            });
        }
    }

    class AuditProfileEntryEmployeesModalCtrl implements qualityInterfaces.IAuditProfileSectionEmployeesModalCtrl {

        static $inject = ['$scope', '$uibModalInstance', 'employees', 'selectedEmployees'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employees: any[],
            private selectedEmployees: any[]) {

        }

        saveAssignedEmployees = (): void => {
            alert('Save has been clicked.');
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Close the modal instance and return the selected employees.
         */
        addEmployees = () => {
            this.$uibModalInstance.close(this.selectedEmployees);
        }
    }

    class AuditProfileSectionAssignedEmployeesModalCtrl implements qualityInterfaces.IAuditProfileSectionEmployeesModalCtrl {

        static $inject = ['$scope', '$uibModalInstance', 'employees', 'selectedEmployees'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employees: any[],
            private selectedEmployees: any[]) {

        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
        * @description Remove an employee object from the selected array.
        */
        removeEmployee = ($event, index: number) => {
            $event.preventDefault();
            this.selectedEmployees.splice(index, 1);
        }

        /**
        * @description save assigned employees after removing them through show assigned employee modal
        */
        save = () => {
            this.$uibModalInstance.close(this.selectedEmployees);
        }
    }

    angular.module('app').controller('AuditProfileEntryCtrl', AuditProfileEntryCtrl);
    angular.module('app').controller('AuditProfileEntryEmployeesModalCtrl', AuditProfileEntryEmployeesModalCtrl);
    angular.module('app').controller('AuditProfileSectionAssignedEmployeesModalCtrl', AuditProfileSectionAssignedEmployeesModalCtrl);
}