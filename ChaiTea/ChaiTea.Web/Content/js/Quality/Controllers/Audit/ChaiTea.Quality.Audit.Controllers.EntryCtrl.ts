﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonEnums = ChaiTea.Common.Enums;

    class AuditEntryCtrl {
        autoSaver;
        saveInSeconds: number = 10;
        goal: number = 0;
        siteSettings: ISiteSettings;

        scoreCardId: number = 0;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        modalInstance;

        options = {
            buildings: [],
            floors: [],
            employees: [],
            customerRepresentatives: [],
            quickComments: []
        };

        datepickers = {
            auditDate: false
        };
        maxDate: Date = new Date();
        basePath: string;
        isEntryDisabled: boolean = false;
        audit = {
            ScorecardId: 0,
            InProgress: true,
            BuildingId: 0,
            FloorId: 0,
            AuditDate: null,
            Comment: '',
            ScorecardSections: [],
            CustomerRepresentatives: [],
            ParentScorecardId: 0
        };

        validationRules =
        {
            'scoreCardScoreRequires': { required: ['AuditInspectionItemName', 'Comment', 'ScorecardDetailImages', 'TodoItems'] }
        }

        showNoInternetMsg: boolean = true;
        hasInternet: boolean = true;
        isSecurityProgram: boolean = false;
        selectedBuilding: string = '';
        selectedFloor: string = '';
        auditSetting: commonInterfaces.IAuditSettings = null;
        inspectionItemFailureValidations = {
            RequireToDosInternalAudit: false,
            ReRunAuditsInternalAudit: false
        }
        validationSummaryObj = [];
        translateOnPageLoad = {
            "AREA": "AREA_MESSAGE",
            "REQUIRED": "REQUIRED_MESSAGE",
            "FINALIZE": "CANNOT_FINALIZE_AUDIT",
            "FINALIZEQUESTION": "FINALIZE_AUDIT_QUESTION"
        };
        todo = <qualityInterfaces.IReRunAuditTodo>{};
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            'userContext',
            'userInfo',
            'blockUI',
            '$log',
            '$interval',
            '$timeout',
            '$filter',
            '$uibModal',
            '$http',
            '$translate',
            'chaitea.quality.services.auditssvc',
            'chaitea.quality.services.auditsscorecardsvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.alertsvc',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private blockUI,
            private $log: angular.ILogService,
            private $interval: angular.IIntervalService,
            private $timeout: angular.ITimeoutService,
            private $filter: any,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private $translate: angular.translate.ITranslateService,
            private auditsSvc: qualitySvc.IAuditsSvc,
            private auditsScoreCardSvc: qualitySvc.IAuditsScoreCardSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private alertSvc: commonSvc.IAlertSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            if (userContext.Program.Name == 'Security') {
                this.isSecurityProgram = true;
            }
            angular.forEach(this.translateOnPageLoad, (data, key) => {
                this.$translate(data).then((translated) => {
                    this.translateOnPageLoad[key] = translated;
                });
            });
            this.modalInstance = this.$uibModal;
            this.$scope.$watch('vm.audit.CustomerRepresentatives', (oldCustomerReps, newCustomerReps) => {
                if (oldCustomerReps.length != newCustomerReps.length) {
                    this.setInspectionItemFailureChecks();
                }
            });
           
        }

        private setReportsConfig = () => {
            return this.reportHelperSvc.getReportSettings().then((r) => {
                this.goal = r.ScoringProfileGoal;
            });

        }
        /**
         * @description Start or resume the auto save.
         */
        startAutoSave = () => {
            if (angular.isDefined(this.autoSaver)) {
                this.stopAutoSave();
            };

            var that = this;
            this.autoSaver = this.$interval(function () {
                that.saveAudit(null, true);
            }, this.saveInSeconds * 1000);

        }

        /**
         * @description Stop the autosave.
         */
        stopAutoSave = () => {
            if (angular.isDefined(this.autoSaver)) {
                this.$interval.cancel(this.autoSaver);
                this.autoSaver = undefined;
            }
        }

        /** @description Initializer for the controller. */
        initialize = (scoreCardId: number): void => {
            this.scoreCardId = scoreCardId;

            this.isEntryDisabled = this.checkIfEntryIsDisabled(this.siteId, this.programId);

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return;
            }

            this.getBuildings();

            this.setInspectionItemFailureChecks();
            if (this.scoreCardId === 0) return;
            this.getAllLookuplist(this.scoreCardId).then(() => {
                this.getAuditById(this.scoreCardId);
            });

            this.setReportsConfig();
            //temp for testing
            this.$scope.$watch(() => this.audit.ScorecardSections, (oldVal, newVal) => {
                console.info('ScoreCardSections', '', 'oldVal', oldVal, '', 'newVal', newVal);
            })
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            if (this.blockUI) this.blockUI.stop();
            if (response.status == commonEnums.HTTP_STATUS_CODES.NOT_FOUND) {
                this.scoreCardId = 0;
            }
            this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            if (this.blockUI) this.blockUI.stop();

            bootbox.alert('Audit was saved successfully!', this.redirectToDetails);
        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} siteId
         * @param {number} programId
         */
        checkIfEntryIsDisabled = (siteId: number, programId: number): boolean => {
            return (programId === 0 || siteId === 0);
        }

        /**
        * LOOKUPS
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (scoreCardId: number): angular.IPromise<any>  => {
            return this.$q.all([
                this.apiSvc.get('CustomerRepresentative', false),
                this.apiSvc.getLookupList('LookupList/Employees', true),
                this.apiSvc.get('QuickComment', true)
            ]).then((result:Array<any>) => {
                this.options.customerRepresentatives = result[0];
                this.options.employees = result[1].Options;
                this.options.quickComments = result[2];
            });

        }

        /**
         * LOOKUPS
         * @description Get buildings.
         */
        getBuildings = (): angular.IPromise<any> => {
            return this.apiSvc.getLookupList('LookupList/Buildings', true).then((res) => {
                this.options.buildings = res.Options;
            }, this.onFailure);
        }

        /** 
         * LOOKUPS
         * @description Get floor profiles based on buildingId. 
         * @param {number} buildingId
         */
        getFloorProfilesByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                var res = _.filter(this.options.buildings, { 'Key': buildingId });
                this.selectedBuilding = res[0].Value;
                console.log(this.selectedBuilding);
                var param = <qualitySvc.IODataPagingParamModel>{
                    $orderby: 'FloorName asc',
                    $filter: 'Profile ne null and Profile/ProfileSectionCount gt 0 and BuildingId eq ' + buildingId
                };

                this.blockUI.start('Loading Floors...');

                this.apiSvc.getByOdata(param, 'FloorProfile', false).then((res) => {
                    var floorProfiles = _.map(res,(profile) => {
                        return { Key: profile['FloorId'], Value: profile['FloorName'] }
                    });
                    this.options.floors = floorProfiles;
                    if (this.blockUI) this.blockUI.stop();
                });

            } else {
                this.options.floors = [];
            }
        }

        /**
         * LOOKUPS
         * @description Get customer representatives.
         */
        getCustomerRepresentatives = () => {
            return new this.$q((resolve, reject) => {
                if (this.options.customerRepresentatives.length == 0) {
                    this.auditsScoreCardSvc.getCustomerRepresentatives().then((data) => {
                        resolve(this.options.customerRepresentatives = data);
                    }, this.onFailure);
                } else {
                    resolve(this.options.customerRepresentatives);
                }
            });
        }

        /**
         * Check if ToDos need to be created for every Failing Inspection Item
        */
        private setInspectionItemFailureChecks = () => {
            async.waterfall([
                (callback) => {
                    if (!this.auditSetting) {
                        var params = {
                            ClientId: this.clientId,
                            SiteId: this.siteId,
                            CascadeClient: false
                        }
                        this.apiSvc.query(params, "AuditSetting", false, false).then((result: commonInterfaces.IAuditSettings) => {
                            this.auditSetting = result;
                            callback(null, result);
                        }).catch(err => (callback(err)));
                    }
                    else {
                        callback(null, this.auditSetting);
                    }
                },
                (auditSetting: commonInterfaces.IAuditSettings, callback) => {
                    if (!this.userInfo.mainRoles.customers && this.audit.CustomerRepresentatives.length <= 0) {
                        if (auditSetting.RequireToDosInternalAudit) {
                            this.inspectionItemFailureValidations.RequireToDosInternalAudit = true;
                        }
                        if (auditSetting.ReRunAuditsInternalAudit) {
                            this.inspectionItemFailureValidations.ReRunAuditsInternalAudit = true;
                        }
                    }
                    else {
                        this.inspectionItemFailureValidations.RequireToDosInternalAudit = false;
                        this.inspectionItemFailureValidations.ReRunAuditsInternalAudit = false;
                    }
                }
            ], (err, result) => {
                this.onFailure(err);
                return;
            });
        }

        /** 
         * LOOKUPS
         * @description On building field change event. 
         */
        onBuildingChange = this.getFloorProfilesByBuildingId;

        private onFloorChange = (floorId) => {
            var res = _.filter(this.options.floors, { 'Key': floorId });
            this.selectedFloor = res[0].Value; 
        }
        /** 
         * SCORE CARD
         * @description Get audit data.
         * @param {int} scoreCardId
         */
        getAuditById =  (scoreCardId: number): void => {
            if (scoreCardId == 0) return;

            this.blockUI.start('Loading...');
            //this.auditsScoreCardSvc.getAuditScore(scoreCardId).then((result) => {
            this.apiSvc.getById(scoreCardId, 'ScoreCard', false).then((result) => {

                this.blockUI.stop();
                this.audit = result;
                this.selectedBuilding = result.BuildingName;
                this.selectedFloor = result.FloorName;
                var isMobile = this.siteSettings.windowSizes.isTiny;
                var that = this;
                _.each(this.audit.ScorecardSections, function (obj, index) {
                    obj.IsToggled = index == 0 ? true : false;

                    for (var i = 0; i < obj.ScorecardDetails.length; i++) {
                        var item = obj.ScorecardDetails[i];
                        obj.ScorecardDetails[i].IsRequired = false;
                        //If on mobile close comment box by default;
                        if (isMobile) {
                            obj.ScorecardDetails[i].IsCommentCollapsed = true;
                        }
                        if (item.Score && item.Score < that.goal && that.inspectionItemFailureValidations.RequireToDosInternalAudit && item.TodoItems.length <= 0) {
                            obj.ScorecardDetails[i].RequireToDosInternalAudit = true;
                    }
                        else {
                            obj.ScorecardDetails[i].RequireToDosInternalAudit = false;
                        }
                    }

                });

                // Convert customer representatives to plain array<int>
                this.audit.CustomerRepresentatives = _.pluck(
                    result.CustomerRepresentatives, 'CustomerRepresentativeId');

                this.getFloorProfilesByBuildingId(this.audit.BuildingId);

                //Fixed ng-invalid-date when WAS string.
                this.audit.AuditDate = moment(this.audit.AuditDate).toDate();

                // Watch the model and trigger an auto save if changed.
                this.$scope.$watch(() => {
                    return this.audit;
                },(newVal, oldVal) => {
                        if (newVal != oldVal) {
                            this.startAutoSave();
                        }
                    }, true);

            }, this.onFailure);

        }

        /**
         * SCORE CARD
         * @description 
         */
        isScoreDetailRequired = (inspection: any): any => {
            if (inspection.length) {
                var rules = this.validationRules['scoreCardScoreRequires']['rules'];
                _.forEach(inspection,(newVal, oldVal) => {
                    if (newVal != oldVal) {
                        _.forEach(rules,(rule) => {
                            inspection[rules].length
                            inspection.isRequired = true;
                        })
                    }
                });
            }

        };
      
        /*
        * @description Delete audit
        */
        deleteAudit = ($event): void => {
            $event.preventDefault();
            var that = this;

            bootbox.confirm('Are you sure you want to delete this audit?',(result) => {
                if (result) {
                    that.apiSvc.delete(that.audit.ScorecardId, 'ScoreCard').then(function (data) {
                        mixpanel.track("Audit deleted.", { AuditId: that.audit.ScorecardId });
                        //cleanup
                        that.localStorageSvc.removeObject(that.localStorageSvc.getKey('ScoreCard', that.audit.ScorecardId));

                        that.redirectToDetails();

                    }, function (error) { that.$log.error(error); });
                }
            });
        }

        /**
         * SCORE CARD
         * @description Create an audit and redirect with the new auditId.
         */
        createAudit = ($event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (!this.audit.FloorId) return;

            this.blockUI.start('Creating Audit...');

            this.apiSvc.save({ FloorId: this.audit.FloorId, InProgress: true }, 'ScoreCard', false).then((result) => {
                this.blockUI.stop();

                mixpanel.track("Audit created.", { AuditId: this.audit.ScorecardId });

                window.location.replace(this.basePath + 'Quality/Audit/Entry/' + result.ScorecardId);
            });

        }

        /** 
         * SCORE CARD
         * @description Save the audit score card and the sections.
         */
        saveAudit = ($event, inProgress: boolean): void => {
            if ($event) $event.preventDefault();
            var that = this;

            var saveIt = () => {
                if ($event) this.blockUI.start('Saving...');

                this.stopAutoSave();
                if (inProgress || that.$scope.auditForm.$valid) {
                    that.$q.all([
                        that.saveAuditScoreCard(inProgress),
                        that.saveAuditScoreCardSections()
                    ]).then((result) => {
                        this.blockUI.stop();

                        this.hasInternet = true;
                        this.showNoInternetMsg = true;

                        //only save mixpanel if clicked NOT auto saved
                        if ($event && inProgress) mixpanel.track("Audit updated", { AuditId: this.audit.ScorecardId });

                        if (!inProgress) {
                            if ($event) mixpanel.track("Audit finalized", { AuditId: this.audit.ScorecardId });

                            //clear localstorage
                            that.localStorageSvc.removeAll();
                            //clear image rotation cache
                            this.localStorageSvc.removeObject('quality:rotate:tracking');
                            var forHistoricalPurposes = '';
                            if (moment(this.audit.AuditDate) < moment().date(1).hours(0).minutes(0).seconds(0)) {
                                forHistoricalPurposes = '  This audit is for previous month. It will only be stored for historical purposes and will not affect SEP Kpi score for any month.';
                            }
                            bootbox.alert('Audit was saved successfully!' + forHistoricalPurposes, this.redirectToDetails);
                            //this.redirectToDetails(that.audit.ScorecardId || 0);
                        }

                        //}, that.onFailure);
                    },(err) => {
                            this.blockUI.stop();
                            //disable photo fields and show msg
                            this.hasInternet = false;
                            if (this.showNoInternetMsg || !inProgress) {
                                bootbox.alert('Your internet connection appears to be weak. Your data has been saved, but your connection must improve before you finalize.');
                                this.showNoInternetMsg = false;
                            }
                        });
                } else {
                    this.blockUI.stop();
                }
            }

            if (!inProgress) {
                var result = true;
                if (this.isSecurityProgram) {
                    var scoreCardSections: any = this.audit.ScorecardSections;
                    angular.forEach(scoreCardSections, (item) => {
                        var score = _.pluck(item.ScorecardDetails, 'Score');
                        if (_.some(score, function (num) {
                            return num === null
                        })) {
                            result = false;
                        }
                    });
                }
                this.validationSummaryObj = [];
                if (this.inspectionItemFailureValidations.RequireToDosInternalAudit && !this.isSecurityProgram) {
                    this.createValidationMessageObject();
                }
                if (this.$scope.auditForm.$invalid || !result || this.validationSummaryObj.length > 0) {
                   
                    if (this.isSecurityProgram) {
                        this.alertSvc.alert({message: this.translateOnPageLoad.FINALIZE });
                    }
                    else {
                        this.modalInstance = this.$uibModal.open({
                            templateUrl: this.basePath + 'Content/js/Quality/Templates/Audit/quality.audit.validationSummaryModal.tmpl.html',
                            controller: 'Quality.ValidationSummaryModalCtrl as vm',
                            size: 'md',
                            resolve: {
                                validationSummaryObj: () => { return this.validationSummaryObj; }
                            }
                        });
                    }
                }
                else {
                    this.alertSvc.confirmWithCallback(this.translateOnPageLoad.FINALIZEQUESTION, (result) => {
                        if (result) {
                            if (this.isSecurityProgram) {
                                var scorecardSections: any = this.audit.ScorecardSections;
                                angular.forEach(scorecardSections, (item) => {
                                    item.ScorecardDetails = _.map(item.ScorecardDetails, (el: any) => {
                                        var newEl = angular.copy(el);
                                        newEl.Score = null
                                        return (el.Score === -1) ? newEl : el;
                                    });
                                });
                            }
                            if (this.inspectionItemFailureValidations.ReRunAuditsInternalAudit && !this.isSecurityProgram) {
                                let scores = [];
                                _.each(this.audit.ScorecardSections, (section: any) => {
                                    let scores_detail = _.map(section.ScorecardDetails.filter((item) => {
                                        if (item.Score) {
                                            return item.Score
                                        }
                                    }), 'Score');
                                    scores = scores.concat(scores_detail);
                                });
                                let average;
                                if (scores.length > 0) {
                                    average = scores.reduce((sum, a) => {
                                        return sum + a
                                    }, 0) / (scores.length || 1);
                                }
                                if (average < this.goal) {
                                    let dueDate = moment().add(30, 'd').toDate();
                                    this.todo = {
                                        OrgUserId: this.userInfo.userId,
                                        ScorecardId: this.audit.ScorecardId,
                                        FloorId: this.audit.FloorId,
                                        TodoItemId: 0,
                                        Comment: 'An audit at ' + this.selectedBuilding + ', ' + this.selectedFloor + ' has failed and requires a Re-Audit within 30 days.',
                                        DueDate: this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(dueDate))
                                    }
                                    this.apiSvc.save(this.todo, "TodoItem").then((res) => {
                                    }, this.onFailure);
                                }
                                this.apiSvc.getById(this.audit.ParentScorecardId, "TodoItem/ReRunAuditToDo").then((result) => {
                                    if (result) {
                                        var todo = result;
                                        if (!todo.IsComplete) {
                                            todo.IsComplete = true;
                                            this.apiSvc.update(todo.TodoItemId, todo, "TodoItem").then(this.onSuccess, this.onFailure);
                                        }
                                    }
                                });
                            }
                            saveIt();
                        }
                    });
                }
            } else { saveIt(); }
        };

        /**
         * SCORE CARD
         * @description Create validation check object
         */
        private createValidationMessageObject = (): void => {
            _.each(this.audit.ScorecardSections, (item) => {
                var check = item.ScorecardDetails.some(function (details) {
                    return details.RequireToDosInternalAudit && details.TodoItems.length == 0
                });
                if (check) {
                    this.validationSummaryObj.push(this.translateOnPageLoad.AREA + ' ' + item.AreaName);
                }
            });
            if (this.$scope.auditForm.$invalid) {
                this.validationSummaryObj.push(this.translateOnPageLoad.REQUIRED);
            }
        }

        /**
         * SCORE CARD
         * @description Save the audit score card.
         */
        saveAuditScoreCard = (inProgress: boolean): angular.IPromise<any> => {
            var localScoreCard = _.clone(this.audit, true);
            var scoreCard = _.pick(this.audit, 'ScorecardId', 'FloorId', 'InProgress', 'AuditDate', 'Comment', 'CustomerRepresentatives');

            scoreCard['InProgress'] = inProgress;

            // Convert customer representatives back to its complex form.
            var customerRepresentatives = [];
            angular.forEach(this.audit.CustomerRepresentatives,(customerRepresentativeId) => {
                var representative = _.find(this.options.customerRepresentatives, {
                    CustomerRepresentativeId: customerRepresentativeId
                });
                customerRepresentatives.push(representative);
            });

            scoreCard['CustomerRepresentatives'] = customerRepresentatives;

            //cached the full scorecard obj locally
            localScoreCard['CustomerRepresentatives'] = customerRepresentatives;
            var key: string = this.localStorageSvc.getKey('ScoreCard', this.audit.ScorecardId);
            this.localStorageSvc.setObject(key, localScoreCard);

            //try to update
            //do not cache this update because it is not the full obj
            return this.apiSvc.update(this.audit.ScorecardId, scoreCard, 'ScoreCard', false);
        }

        /**
         * SCORE CARD SECTIONS
         * @description Save all the score card sections details.
         */
        saveAuditScoreCardSections = (): angular.IPromise<any> => {
            var inspections = [];

            angular.forEach(this.audit.ScorecardSections,(section) => {
                angular.forEach(section.ScorecardDetails,(detail) => {
                    var newDetail = _.pick(detail, 'ScorecardDetailId', 'Score', 'Comment', 'ScorecardSectionId');
                    
                    inspections.push(newDetail);

                });
            });
            console.log('SCORE CARD SECTIONS ', inspections);
            return this.apiSvc.updateAll(inspections, 'ScoreCardDetail', true);

        }

        /**
        * SCORE CARD DETAIL TODO ITEMS
        * @description Build the inspection todo item modal.
        */
        editInspectionItemTodo = ($event, inspection, todo: qualityInterfaces.IAuditInspectionTodo) => {
            $event.preventDefault();
            var requireToDosInternalAudit = this.inspectionItemFailureValidations.RequireToDosInternalAudit;
            if (!inspection.Score || inspection.Score >= this.goal) {
                requireToDosInternalAudit = false;
            }
            var that = this;

            this.getCustomerRepresentatives().then(function (reps) {
                that.modalInstance = that.$uibModal.open({
                    templateUrl: 'auditInspectionItemModal.tmpl.html',
                    controller: 'Quality.AuditInspectionTodoModalCtrl as vm',
                    size: 'md',
                    resolve: {
                        employees: () => { return that.options.employees; },
                        customerRepresentatives: () => { return reps },
                        todo: () => {
                            if (todo) return angular.copy(todo);

                            return <qualityInterfaces.IAuditInspectionTodo>{
                                ScorecardDetailId: inspection.ScorecardDetailId,
                                TodoItemId: 0,
                                OrgUserId: null,
                                Comment: inspection.Comment,
                                DueDate: that.dateFormatterSvc.formatDateFull(new Date()),
                                IsActive: true
                            }
                        }
                    }
                });

                that.modalInstance.result.then((todo: qualityInterfaces.IAuditInspectionTodo) => {
                    // Delete the todo item
                    if (todo.IsActive == false) {
                        that.deleteInspectionItemTodo(todo.TodoItemId, inspection);
                        return;
                    }

                    // Save or update todo item
                    that.saveUpdateInspectionItemTodo(todo, inspection);

                },() => { });
            });
        }

        /**
        * SCORE CARD DETAIL TODO ITEMS
        * @description Save or update the todo item.
        */
        saveUpdateInspectionItemTodo = (todo: qualityInterfaces.IAuditInspectionTodo, inspection) => {
            todo.Comment = this.$filter('htmlToPlaintext')(todo.Comment);
            todo.FloorId = this.audit.FloorId;
            
            if (todo.TodoItemId == 0) {
                // Save the todo item
                this.auditsScoreCardSvc.saveAuditScoreDetailsTodo(todo).then((response) => {
                    inspection.RequireToDosInternalAudit = false;
                    todo.TodoItemId = response.TodoItemId;
                    if (inspection.TodoItems) {
                        inspection.TodoItems.push(todo);
                    } else {
                        inspection.TodoItems = [];
                        inspection.TodoItems.push(todo);
                    }
                    mixpanel.track("Created Audit Todo", { TodoItemId: todo.TodoItemId });
                }, this.onFailure);

            } else {
                // Update the todo item
                var todoItem = _.find(inspection.TodoItems, { TodoItemId: todo.TodoItemId });
                this.auditsScoreCardSvc.updateAuditScoreDetailsTodo(todo.TodoItemId, todo).then((response) => {
                    angular.copy(todo, todoItem);

                    if (todo.IsActive) mixpanel.track("Closed Audit Todo", { TodoItemId: todo.TodoItemId });
                }, this.onFailure);
            }
        }

        /**
        * SCORE CARD DETAIL TODO ITEMS
        * @description Delete the todo item.
        */
        deleteInspectionItemTodo = (todoItemId: number, inspection): void => {
            this.auditsScoreCardSvc.deleteAuditScoreDetailsTodo(todoItemId).then((response) => {
                for (var i = 0; i < inspection.TodoItems.length; i++) {
                    if (inspection.TodoItems[i].TodoItemId === todoItemId) {
                        inspection.TodoItems.splice(i, 1);
                        break;
                    }
                }
                if (inspection.TodoItems.length == 0) {
                    inspection.RequireToDosInternalAudit = true;
                }
            }, this.onFailure);
        }

        /**
         * SCORE CARD DETAIL IMAGE
         * @description Handler when a photo is selected.
         */
        fileAdded = (inspection, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            var that = this;

            if (files && files.length) {
                var file = files[0];

                if (file.type.indexOf('image') == -1) {
                    bootbox.dialog({
                        message: 'Invalid file format. Please select an image file.',
                        title: 'File Upload',
                        buttons: {
                            main: {
                                label: 'OK',
                                className: 'btn-danger'
                            }
                        }
                    });

                    return false;
                }
                this.uploadImage(file, $event, inspection);
            }


            return false;
        }

        /**
         * SCORE CARD DETAIL IMAGE
         * @description Upload the image using the file upload service.
         */
        uploadImage = (file, $event, inspection) => {
            var that = this;
            var scorecardDetailId = inspection.ScorecardDetailId,
                progressElement = angular.element('span#inspection-' + scorecardDetailId);

            try {

                this.imageSvc.save(file, false)
                    .then((result) => {
                        var publicId = result.Name;

                        if (!publicId) {
                            throw new Error("No public ID returned.");
                        }

                        that.blockUI.start('Loading...');

                        // Save the new image publicId
                        setTimeout(function () {
                            that.auditsScoreCardSvc.saveAuditScoreDetailsImage({
                                PublicId: publicId,
                                ScorecardDetailId: scorecardDetailId
                            }).then((result) => {

                                inspection.ScorecardDetailImages.push({
                                    ImageId: result.ImageId,
                                    PublicId: publicId,
                                    ScorecardDetailId: scorecardDetailId,
                                    Comment: null
                                });

                            }, (error) => {
                                throw new Error(error);
                            }).finally(() => { that.blockUI.stop(); });
                        }, 2000);

                    }, (error) => {
                        throw new Error(error);
                    });

            } catch (error) {
                this.$log.error(error);
            }
        }

        /**
         * SCORE CARD DETAIL IMAGE
         * @description Delete image from the inspection.
         */
        deleteImage = (photo, inspection): angular.IPromise<any>=> {
            var that = this;
            return new this.$q(function (resolve, reject) {
                that.auditsScoreCardSvc.deleteAuditScoreDetailsImage(photo.ImageId).then((result) => {
                    for (var i = 0; i < inspection.ScorecardDetailImages.length; i++) {
                        if (inspection.ScorecardDetailImages[i].ImageId === photo.ImageId) {
                            inspection.ScorecardDetailImages.splice(i, 1);
                            break;
                        }
                    }
                    resolve(result);
                },(result) => {
                        reject(result);
                    });
            });

        }

        /**
         * SCORE CARD DETAIL IMAGE
         * @description Display a modal to manage the uploaded images.
         */
        viewPhotos = (inspection, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            // Open the photo manage dialog
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'managePhotoUploads.tmpl.html',
                controller: 'Quality.ScorecardDetailImageModalCtrl as vm',
                size: 'md',
                resolve: {
                    inspection: () => { return inspection; },
                    scorecardDetailImages: () => { return inspection.ScorecardDetailImages; },
                    deleteImage: () => { return this.deleteImage; },
                    uploadImage: () => { return this.uploadImage; }
                }
            });

            this.modalInstance.result.then((scorecardDetailImage: qualityInterfaces.IScorecardDetailImage) => {

            },() => { });
        }

        /**
         * CUSTOMER REPRESENTATIVES
         * @description Open a dialog to manage customer representatives and add to current list of customer representatives.
         */
        addNewCustomerRepresentative = ($event) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Common/Templates/modal.addEditCustomerRepresentative.tmpl.html',
                controller: 'Quality.CustomerRepresentativeModalCtrl as vm',
                size: 'md',
                resolve: {
                    customerRepresentatives: () => { return this.options.customerRepresentatives; },
                    customerRepresentative: () => {
                        return <qualityInterfaces.ICustomerRepresentative>{
                            CustomerRepresentativeId: 0,
                            Name: '',
                            Email: '',
                            JobDescription: ''
                        }
                    }
                }
            });

            this.modalInstance.result.then((customerRepresentative: qualityInterfaces.ICustomerRepresentative) => {
                if (customerRepresentative.CustomerRepresentativeId == 0) {
                    this.auditsScoreCardSvc.saveCustomerRepresentative(customerRepresentative)
                        .then((result) => {
                        customerRepresentative.CustomerRepresentativeId = result.CustomerRepresentativeId;
                        this.options.customerRepresentatives.unshift(customerRepresentative);
                    }, this.onFailure);
                }
            },() => { });
        }

        //#region EVENT HANDLERS
        toggleSection = ($event, section) => {
            $event.preventDefault();

            section.IsToggled = !section.IsToggled;
            _.each(this.audit.ScorecardSections, function (obj) {
                if (section.ScorecardSectionId != obj.ScorecardSectionId) obj.IsToggled = false;
            });
        }

        toggleInspectionItem = ($event) => {
            $event.preventDefault();

            var element = $($event.currentTarget);

            if ($(window).width() < 768) {
                $(".ai-en-details").not(element.closest('.table-row').find('.ai-en-details')).slideUp('fast');
                element.closest('.table-row').find(".ai-en-details").slideToggle('fast');
            }
        }

        toggleInspectionComment = (inspection, $event) => {
            $event.preventDefault();

            inspection.IsCommentCollapsed = !inspection.IsCommentCollapsed;
        }

        onCommentBlur = ($event) => {
            var element = angular.element($event.currentTarget);
            element.parent('.audit-comment-textarea').removeClass('popText');
            angular.element('.overlay-comment').css('display', 'none');
        }

        onCommentFocus = ($event) => {
            var element = angular.element($event.currentTarget);

            if (angular.element(window).width() > 500 && $(window).width() < 1030) {
                var parent = element.parent('.audit-comment-textarea');
                parent.addClass('popText').css('top', '0');
                angular.element('.overlay-comment').css('display', 'block');
            }
        }

        /**
         * EVENT HANDLERS
         * @description Toggle the todo list dropdown list.
         */
        toggleTodoItems = ($event) => {
            $event.preventDefault();

            angular.element($event.currentTarget).parent().toggleClass('list-menu-active');
        }

        /** 
         * EVENT HANDLERS
         * @description Handle opening datepicker. 
         * @param {evt} $event
         * @param {string} which - The name of the datepicker.
         */
        openDatePicker = ($event, which): void => {
            $event.preventDefault();
            $event.stopPropagation();
            this.datepickers[which] = true;
        }

        /** 
         * EVENT HANDLERS
         * @description Cancel the data entry. 
         */
        cancel = ($event): void=> {
            $event.preventDefault();
            this.redirectToDetails();
        }

        /** 
         * EVENT HANDLERS
         * @description Redirect to details page. 
         */
        redirectToDetails = (): void => {
            window.location.href = this.basePath + 'Quality/Audit/Details';
        }
        //#endregion

        /**
        * @description Updates score in a ScorecardDetail. Used from QualitySecurityAudit component
        */
        changeScorecardDetail = (score, sectionIndex, detailIndex) => {
            this.audit.ScorecardSections[sectionIndex].ScorecardDetails[detailIndex].Score = score;
        }

        /**
        * @description Check if ToDos are mandatory
        */
        private highlightRequireToDos = (inspection: any): boolean => {
            inspection.RequireToDosInternalAudit = false;
            if (inspection.Score && inspection.Score < this.goal && this.inspectionItemFailureValidations.RequireToDosInternalAudit && inspection.TodoItems.length == 0) {
                inspection.RequireToDosInternalAudit = true;
                return true;
            }
            else {
                inspection.RequireToDosInternalAudit = false;
            }
            return false;
        }

        /**
        * @description Check if section needs to be highlighted
        */
        private highlightSection = (section: any): boolean => {
            if (_.some(section.ScorecardDetails, { 'RequireToDosInternalAudit': true })) {
                return true;
            }
            return false;
        }

    }

    class AuditInspectionTodoModalCtrl {
        isEmployee = true;
        datepickers = {
            createDate: false
        };
        isDueInTwentyFourHours: boolean = false;
        dueDate: Date;
        static $inject = [
            '$scope',
            '$uibModalInstance',
            'chaitea.core.services.dateformattersvc',
            'employees',
            'customerRepresentatives',
            'todo',
            Common.Services.DateSvc.id];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private employees: any[],
            private customerRepresentatives: any[],
            private todo: qualityInterfaces.IAuditInspectionTodo,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            if (todo.TodoItemId > 0 && todo.CustomerRepresentativeId) {
                this.isEmployee = false;
            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        setDueDate = () => {
            this.dueDate = this.isDueInTwentyFourHours ? moment().add(1, 'd').toDate() : null;
        }

        /**
         * @description Create a new profile and pass variable to parent controller.
         */
        saveTodo = (): void => {
            if (this.isEmployee) {
                this.todo.CustomerRepresentativeId = null;
            } else {
                this.todo.OrgUserId = null;
            }
            if (!this.isDueInTwentyFourHours) {
                this.dueDate = moment(this.dueDate).endOf('day').toDate();
            }
            this.todo.DueDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dueDate));
            this.$uibModalInstance.close(<qualityInterfaces.IAuditInspectionTodo> this.todo);
        }

        /**
         * @description Flag a todo item for delete.
         */
        deleteTodo = (): void => {
            this.todo.IsActive = false;
            this.$uibModalInstance.close(<qualityInterfaces.IAuditInspectionTodo> this.todo);
        }

        /** 
         * @description Handle opening datepicker. 
         * @param {evt} $event
         * @param {string} which - The name of the datepicker.
         */
        openDatePicker = ($event, which): void => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;
        }
    }

    class CustomerRepresentativeModalCtrl {
        title = 'Add';
        static $inject = [
            '$scope',
            '$uibModalInstance',
            'customerRepresentatives',
            'customerRepresentative'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private customerRepresentatives: any[],
            private customerRepresentative: qualityInterfaces.ICustomerRepresentative) {

        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Create a new customer representative.
         */
        saveCustomerRepresentative = (): void => {
            this.$uibModalInstance.close(<qualityInterfaces.ICustomerRepresentative> this.customerRepresentative);
        }
    }

    class ScorecardDetailImageModalCtrl {
        currentImage: qualityInterfaces.IScorecardDetailImage;
        scorecardDetailImage: qualityInterfaces.IScorecardDetailImage;

        storageId: string = 'quality:rotate:tracking';
        rotateTracking: Array<{ id: string; rotateDeg: number; width: number; height: number; }> = [];
        imageCss: any = {
            "-ms-transform": "rotate(0deg)",
            "-webkit-transform": "rotate(0deg)",
            "transform": "rotate(0deg)"
        };
        wrapperCss: any = { "display": "block" };

        //cdnUrl: string = '';

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'inspection',
            'scorecardDetailImages',
            'deleteImage',
            'uploadImage',
            'localStorageService',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private inspection: qualityInterfaces.IScorecardDetail,
            private scorecardDetailImages: qualityInterfaces.IScorecardDetailImage[],
            private deleteImage,
            private uploadImage,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            if (scorecardDetailImages.length > 0)  this.setCurrentImage(this.scorecardDetailImages[0]);

        }

        /**
         * @description Set active image for view.
         */
        public setCurrentImage = (photo: qualityInterfaces.IScorecardDetailImage) => {
            var storage: any = this.localStorageService.get(this.storageId);
            if (storage) {
                this.rotateImage(photo, true);
            }
            this.currentImage = photo;
        }

        /**
         * @description Set active image for view.
         */
        public rotateImage = (photo: qualityInterfaces.IScorecardDetailImage, reset?: boolean) => {

            var storage: any = this.localStorageService.get(this.storageId);

            if (storage) this.rotateTracking = storage; 

            var settingIndex = _.findIndex(this.rotateTracking, { id: photo.PublicId }); 
            var elem: any = angular.element('#current-image');
            var wrapper: any = angular.element('#audit-image-responsive');

            if (settingIndex == -1) {
                this.rotateTracking.push({
                    id: photo.PublicId,
                    rotateDeg: 0,
                    height: (elem[0] ? elem[0].height : 0),
                    width: (elem[0] ? elem[0].width : 0)
                });
                settingIndex = this.rotateTracking.length - 1;
            }

            if (this.rotateTracking[settingIndex].rotateDeg == 0) {
                this.rotateTracking[settingIndex].height = (elem[0] ? elem[0].height : 0);
                this.rotateTracking[settingIndex].width = (elem[0] ? elem[0].width : 0);
            }

            if (!reset) {
                this.rotateTracking[settingIndex].rotateDeg = (this.rotateTracking[settingIndex].rotateDeg + 90 > 270 ? 0 : this.rotateTracking[settingIndex].rotateDeg + 90);

                //save settings
                this.localStorageService.set(this.storageId, <any>this.rotateTracking);

                //fire off reuqest to actually rotate the image on s3
                this.apiSvc.query({}, 'scorecarddetailimage/' + photo.ImageId + '/rotate/90/', false, false).then((res: any) => {
                    //ignore res
                });

            }

            //css rotate
            this.imageCss = {
                "-ms-transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)",
                "-webkit-transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)",
                "transform": "rotate(" + this.rotateTracking[settingIndex].rotateDeg + "deg)"
            };

            //adjust wrapper
            this.wrapperCss = {
                "display": "block",
                "height": (this.rotateTracking[settingIndex].height ? this.rotateTracking[settingIndex].height + "px" : "auto")
            };
            if (this.rotateTracking[settingIndex].rotateDeg == 90 || this.rotateTracking[settingIndex].rotateDeg == 270) {
                this.wrapperCss = {
                    "display": "table-cell",
                    "height": (this.rotateTracking[settingIndex].width ? this.rotateTracking[settingIndex].width + "px" : "auto")
                };

            }

        }

        /**
         * @description Delete the image for the inspection.
         */
        public deletePhoto = (photo: qualityInterfaces.IScorecardDetailImage, inspection) => {
            this.deleteImage(photo, inspection).then(() => {
                if (this.scorecardDetailImages.length == 0) {
                    this.$uibModalInstance.dismiss('cancel');
                    return;
                } else {
                    this.setCurrentImage(this.scorecardDetailImages[0]);
                }
            });
        }

        /**
         * @description Handler when a photo is selected.
         */
        public fileAdded = (inspection, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length) {
                var file = files[0];
                this.uploadImage(file, $event, inspection);
            }

            return false;
        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save a score detail image.
         */
        public saveImage = (): void => {
            this.$uibModalInstance.close(<qualityInterfaces.IScorecardDetailImage> this.scorecardDetailImage);
        }
    }

    class AuditSecurityCriteriaModalModalCtrl {
        criterias = 
            {
                'Uniforms': [
                    'Uniform is worn correctly. There are no signs of wash needed.There are no tears. Appropriate patches are displayed.',
                    'Uniform is worn, but adjustments needed. There are no signs of wash needed. There are no tears. Appropriate patches are displayed.',
                    'Uniform is worn but includes additional personal clothing items. There are no signs of wash needed, but ironing is needed.There are no tears. Missing company patches.',
                    'Uniform is not worn correctly. Missing essential pieces of uniform. There are signs of wash needed.There are no tears but company patches are missing and additional personal clothing items are noticed.',
                    'Uniform is not worn correctly or not worn at all.Signs of wash needed. Visible tears in uniform. Missing company patches and using personal clothing items.'
                ],
                'Identification Credentials' : [
                    'Officer has visible state, client, and company security ID credentials on their uniform or attached to belt.',
                    'Officer has state, client, and company security ID credentials on their person but are not visible.',
                    'Officer has 2/3 of the following ID credentials: state security ID, client security ID, company security ID.',
                    'Officer has 1/3 of the following ID credentials: state security ID, client security ID, company security ID.',
                    'Officer does not have state, client, and company security ID on their person.'
                ],
                'Daily Activity Report Compliance': [
                    'Report is being utilized and updated every hour and on every event that occurs. Report is dated and timed correctly.',
                    'Report is being utilized and updated every hour and on every event that occurs. Report is not dated and timed correctly.',
                    'Report is being utilized but not updated every hour and on every event that occurs.Report is not dated and timed correctly.',
                    'Report is not being utilized completed. Missing hourly line items and is not dated and timed correctly.',
                    'Report is not being utilized at all.'
                ],
                'Post Orders Compliance': [
                    'Post Orders are readily accessible upon request.Post orders have been updated within the last 2 years.Post Orders have a manager approval signature, client approval signature, and Global Security approval signature.',
                    'Post Orders are readily accessible upon request.Post Orders are older than 2 years.Post Orders have a manager approval signature, client approval signatrure, and Global Security approval signature.',
                    'Post Orders are readily accessible upon request. Post Orders are older than 4 years.Post Orders are missing one of the following signatures: manager, client, Global Security.',
                    'Post Orders are not readily accessible upon request.Post Orders are older than 5 years.Post Orders are missing two of the following signatures: manager, client, Global Security',
                    'Post Orders do not exist.',
                ],
                'Shift Change Timing Compliance': [
                    'Officer has arrived to their post at the required time, completed pass down procedure, and signed into the post.',
                    'Officer has arrived to their post at the required time, but has not completed the pass down procedure or signed into the post.',
                    'Officer has arrived late to their post compared to their required time, and has not completed the pass down procedure or signed into the post.',
                    'Officer has arrived late to their post compared to their required time, and has not completed the pass down procedure and signed into the post.',
                    'Officer does not arrive to post.',
                ],
                'Professionalism': [
                    'Officer displays firm grasp of professionalism and treats employees with respect.',
                    'Officer displays signs of professionalism and treats employees with respect.',
                    'Officer displays average professionalism',
                    'Officer displays below average professionalism',
                    'Officer lacks professionalism',
                ]
        }

        static $inject = [
            '$scope',
            '$uibModalInstance'
            ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {}

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    class ValidationSummaryModalCtrl {
        
        static $inject = [
            '$scope',
            '$uibModalInstance',
            'validationSummaryObj'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private validationSummaryObj) {
        }

        /**
         * @description Dismiss the modal instance.
         */
        public cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

    }

    angular.module('app').controller('Quality.AuditEntryCtrl', AuditEntryCtrl);
    angular.module('app').controller('Quality.AuditInspectionTodoModalCtrl', AuditInspectionTodoModalCtrl);
    angular.module('app').controller('Quality.CustomerRepresentativeModalCtrl', CustomerRepresentativeModalCtrl);
    angular.module('app').controller('Quality.ScorecardDetailImageModalCtrl', ScorecardDetailImageModalCtrl);
    angular.module('app').controller('Quality.ValidationSummaryModalCtrl', ValidationSummaryModalCtrl);
}