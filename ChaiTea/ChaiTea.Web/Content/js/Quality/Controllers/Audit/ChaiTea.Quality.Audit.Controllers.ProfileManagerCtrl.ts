﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;
    import commonSvc = Common.Services;

    class AuditProfileManagerCtrl {
        isManagerRole: boolean = false;
        siteId: number = 0;
        programId: number = 0;
        programName: string = "";
        isEntryDisabled: boolean = false;
        modalInstance;
        activeAuditProfiles: any[] = [];
        options = {
            employees: []
        }

        /** @description Paging variables. */
        recordsToSkip: number = 0;
        top: number = 20;
        auditProfiles = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        isSecurityProgram: boolean = false;
        ScoringProfileId: number = 0;
        translateOnPageLoad = "CANNOT_CREATE_AUDITPROFILE";
        
        static $inject = [
            '$scope',
            '$q',
            '$uibModal',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            '$timeout',
            '$translate',
            'chaitea.quality.services.auditssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.reportsvc',
            Common.Services.AlertSvc.id,
            Common.Services.NotificationSvc.id,
            Common.Services.ApiBaseSvc.id];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $translate: angular.translate.ITranslateService,
            private auditsSvc: qualitySvc.IAuditsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private alertSvc: Common.Services.AlertSvc,
            private notificationSvc: Common.Services.NotificationSvc,
            private apiSvc: Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.programName = userContext.Program.Name;

            this.modalInstance = this.$uibModal;

            if (userContext.Program.Name == 'Security') {
                this.isSecurityProgram = true;
            }
            this.$translate(this.translateOnPageLoad).then((translated) => {
                this.translateOnPageLoad = translated;
            });
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            
            this.isEntryDisabled = this.siteId == 0 || this.programId==0;

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element("#client-context").trigger("click");
                }, 500);
                return;
            }
            this.getReportSettings();
            this.getAllAuditProfiles();

            this.isManagerRole = this.canUpsertAuditProfile();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        redirectToEntry = (): void => {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry');
        }

        /**
        * @description Returns whether the user is a manager.
        */
        canUpsertAuditProfile = (): boolean => {
            return this.userInfo.mainRoles.managers;
        }

        /**
         * @description Get complaints and push to array for infinite scroll.
         */
        getAllAuditProfiles = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var param = <qualitySvc.IODataPagingParamModel>{
                $orderby: 'FloorName asc'
            };

            this.auditsSvc.getAllAuditProfiles(param)
                .then((data) => {
                    this.auditProfiles = data;
                }, this.onFailure);
        }

        getReportSettings = (): void => {
            this.reportHelperSvc.getReportSettings().then((res) => {
                this.ScoringProfileId = res ? res.ScoringProfileId : null;
            }, this.onFailure);
        }

        /**
         * @description Get existing profiles for cloning a profile.
         */
        getActiveAuditProfiles = () => {
            return new this.$q((resolve, reject) => {
                if (this.activeAuditProfiles.length == 0) {
                    var param = <qualitySvc.IODataPagingParamModel>{
                        $orderby: 'FloorName asc',
                        $filter: 'Profile ne null'
                    };

                    this.auditsSvc.getAllAuditProfiles(param, true)
                        .then((data) => {
                            resolve(this.activeAuditProfiles = data);
                        }, this.onFailure);
                } else {
                    resolve(this.activeAuditProfiles);
                }
            });
        }

        /**
         * @description Get employees for creating new audit profile.
         */
        getEmployees = () => {
            return new this.$q((resolve, reject) => {
                if (this.options.employees.length ==0) {
                    this.lookupListSvc.getEmployees().then((data) => {
                        resolve(this.options.employees = data.Options);
                    }, this.onFailure);
                } else {
                    resolve(this.options.employees);
                }
            }); 
        }

        /**
         * @description Display modal dialog and show new audit profile options.
         * @param {number} buildingId
         * @param {number} floorId
         */
        createProfileModal = (buildingId: number, floorId: number): void => {
            var that = this;

            this.$q.all([
                this.getActiveAuditProfiles(),
                this.getEmployees()]).then(function (result) {

                that.modalInstance = that.$uibModal.open({
                    templateUrl: 'profileManagerModal.html',
                    controller: 'AuditProfileManagerModalCtrl as vm',
                    size: 'md',
                    resolve: {
                        existingProfiles: () => { return result[0]; },
                        employees: () => {  return result[1]; }
                    }
                });

                that.modalInstance.result.then((selectedItem: qualityInterfaces.ISelectedProfile) => {
                    if (selectedItem.newProfileType == 'clone') {
                        var auditProfileId = selectedItem.selectedProfile;
                        that.cloneProfileAndRedirect(auditProfileId, floorId, selectedItem.responsibleUserId);
                    } else {
                        if (that.ScoringProfileId) {
                            that.createProfileAndRedirect(buildingId, floorId, selectedItem.responsibleUserId);
                        }
                        else {
                            that.notificationSvc.errorToastMessage(that.translateOnPageLoad);
                        }
                    }

                }, () => { });
            });
        }

        createSecurityProfile = (buildingId: number, floorId: number): void => {
            this.alertSvc.confirmWithCallback("Are you sure you want to create a Security Audit profile?", (result) => {
                if (result) {
                    this.createSecurityProfileAndRedirect(buildingId, floorId);//, selectedItem.responsibleUserId);
                }
            })
        }

        /**
         * @description Create a new profile and redirect.
         * @param {number} buildingId
         * @param {number} floorId
         * @param {number} responsibleUserId
         */
        createProfileAndRedirect = (buildingId: number, floorId: number, responsibleUserId: number) => {
            this.auditsSvc.saveAuditProfile({
                BuildingId: buildingId,
                FloorId: floorId,
                ScoringProfileId: 1,
                ResponsibleUserId: responsibleUserId
            }).then((result) => {
                    this.redirectToAuditProfile(result.AuditProfileId);
                }, this.onFailure);
        }

        createSecurityProfileAndRedirect = (buildingId: number, floorId: number, responsibleUserId?: number) => {
            var promise: any;
            var promises = [];
            var auditProfileResult: any;
           
             async.waterfall([
                (callback) => {
                    this.apiSvc.query({}, 'SecurityAudit/SecurityAuditsAreaClassificationInfo')
                        .then(items => {
                            if (!items || items.length < 4) {
                                this.notificationSvc.errorToastMessage('The necessary Inspection Items are not available for a Security Audit. Please contact support.');
                                callback('The necessary Inspection Items are not available for a Security Audit. Please contact support.');
                                return;
                            }
                            callback(null, items);
                        })
                        .catch(err => (callback(err)));
                },
                (areas, callback) => {
                    this.auditsSvc.saveAuditProfile({
                        BuildingId: buildingId,
                        FloorId: floorId,
                        ScoringProfileId: 1,
                        ResponsibleUserId: responsibleUserId
                    })
                    .then(result => callback(null, result, areas))
                    .catch(err => callback(err));
                },
                (auditResult, areas, callback) => {
                    async.each(areas, (id: any, callback) => {
                        var inspectionItems = [];
                        inspectionItems.push(id.AreaClassificationInspectionItemId);
                        promise = this.auditsSvc.saveAuditProfileSection({
                                    AuditProfileSectionId: 0,
                                    AuditProfileId: auditResult.AuditProfileId,
                                    Profile: auditResult,
                                    AreaClassificationId: id.AreaClassificationId, 
                                    AreaId: id.AreaId,
                                    InspectionItems: inspectionItems,
                                    AuditProfileSection: 0,
                                    AssignedEmployees: []
                                })
                        promises.push(promise);
                    });

                    this.$q.all(promises)
                        .then(res => callback(null, auditResult))
                        .catch(err => callback(err));
                }
            ], (err, result) => {
                if (err) {
                    this.notificationSvc.errorToastMessage('Security Audit Profile could not be created. Please contact support.');
                    this.onFailure(err);
                    return;
                }

                this.notificationSvc.successToastMessage("Security Audit Profile created successfully.");
                var profile = _.find(this.auditProfiles, { FloorId: floorId });
                profile.Profile = result;
            });
        }

        /**
         * @description Create a new profile based on an existing profile selection and redirect.
         * @param {number} sourceFloorId
         * @param {number} targetFloorId
         * @param {number} responsibleUserId
         */
        cloneProfileAndRedirect = (auditProfileId: number, targetFloorId: number, responsibleUserId: number) => {
            this.auditsSvc.cloneAuditProfile(auditProfileId, targetFloorId, responsibleUserId).then((result) => {
                this.redirectToAuditProfile(result.Profile.AuditProfileId);
            }, this.onFailure);
        }

        /**
         * @description Redirect to audit profile.
         * @param {number} auditProfileId
         */
        redirectToAuditProfile = (auditProfileId: number): void => {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry/' + auditProfileId);
        }

        /**
         * @description Delete the audit profile associated with the floor.
         * @param {number} auditProfileId
         * @param {number} floorId
         */
        deleteAuditProfile = (auditProfileId: number, floorId: number) => {
            bootbox.confirm('Are you sure you want to delete this audit profile?', (result) => {
                if (result) {
                    this.auditsSvc.deleteAuditProfile(auditProfileId).then((result) => {
                        // Remove profile from a list to toggle the view
                        var profile = _.find(this.auditProfiles, { FloorId: floorId });
                        profile.Profile = null;


                        // Remove from active audit profiles list
                        _.remove(this.activeAuditProfiles, function(profile) {
                            return profile.Profile.AuditProfileId == auditProfileId;
                        });

                    }, this.onFailure);
                }
            });
        }
    }



    class AuditProfileManagerModalCtrl implements qualityInterfaces.IAuditProfileManagerModalCtrl {
        newProfile: qualityInterfaces.ISelectedProfile = {
            newProfileType: 'new',
            selectedProfile: null,
            responsibleUserId: 0
        };
        programName: any = "";
        hideModal:boolean = false;

        static $inject = ['$scope', '$uibModalInstance', 'existingProfiles', 'employees', 'userContext'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private existingProfiles: any[],
            private employees: any[],
            userContext: IUserContext) {
            this.programName = userContext.Program.Name;
        }
        
        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Create a new profile and pass variable to parent controller.
         */
        createNewProfile = (): void => {
            this.hideModal = true;
            bootbox.confirm(`Are you sure you want to create your Audit profile under the <strong>${this.programName}</strong> program?`, this.bootboxCallback);
        }

        bootboxCallback = (confirmed): void => {
            if (confirmed) {
                this.$uibModalInstance.close(<qualityInterfaces.ISelectedProfile> this.newProfile);
            } else {
                this.hideModal = false;
                this.$scope.$apply();
            }
        }
    }

    angular.module('app').controller('AuditProfileManagerCtrl', AuditProfileManagerCtrl);
    angular.module('app').controller('AuditProfileManagerModalCtrl', AuditProfileManagerModalCtrl);
} 