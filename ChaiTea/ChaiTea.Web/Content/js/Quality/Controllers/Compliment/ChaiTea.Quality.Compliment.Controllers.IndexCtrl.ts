﻿/// <reference path="../../../_libs.ts" />
/// <reference path="../../services/chaitea.quality.services.reportssvc.ts" />

module ChaiTea.Quality.Compliment.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IComplimentIndexCtrl {
        initialize(): void;
        onFailure(response: any): void;
        getComplimentTrends(dateRange: commonInterfaces.IDateRange): void;
        getComplimentsByType(dateRange: commonInterfaces.IDateRange): void;
        getComplimentTrendsBySite(dateRange: commonInterfaces.IDateRange): void;
        refreshCharts(dateRange: commonInterfaces.IDateRange): void;
    }

    class ComplimentIndexCtrl implements IComplimentIndexCtrl {
        currentDate = new Date();

        clientId: number = 0;
        programId: number = 0;
        siteId: number = 0;
        recentCompliments = [];
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };

        basePath: string;
        chartSettings: IChartSettings;

        chartsConfig = {
            goalLine: 90,
            counts: {
                complimentTrends: {
                    //count: function () {
                    //    return (80 / 100) * 100;
                    //} (),
                    count: 0,
                    outOf: 100,
                    jointCount: 0
                },
                complimentTrendsBySiteOrBuilding: { count: 0 },
                complimentTrendsByProgramOrType: { count: 0 },
                complimentPastDue: {
                    count: 0
                }
            },

            complimentTrends: {},
            //complimentBySiteOrBuilding: {},
            complimentByProgramOrType: {},
            complimentsByType: {},
            complimentTrendsByType: {},
            //complimentTrendsByProgramOrType: {},
            //complimentTrendsByEmployee: [],
            complimentTrendsBySite: {}
        };

        noOfMonths: number = 0;
        avgComplimentsPerMonth: number = 0;
        module: string = "compliments";
        viewLink: string = "";
        headerText: string = 'COMPLIMENT_DATE';
        clientName: string = '';

        static $inject = [
            '$scope',
            '$filter',
            'userInfo',
            'sitesettings',
            'userContext',
            'chaitea.core.services.messagebussvc',
            'chaitea.quality.services.complimentssvc',
            'chaitea.quality.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            '$uibModal',
            '$log'];
        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private userInfo:IUserInfo,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private complimentsSvc: qualitySvc.IComplimentsSvc,
            private chartsSvc: qualitySvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $log: angular.ILogService) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.clientId = userContext.Client.ID;
            this.programId = userContext.Program.ID;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {

            //TODO: change to use chartConfigBuilder
            this.chartsConfig.complimentTrends = this.chartConfigBuilder('complimentTrends', {});
            this.chartsConfig.complimentByProgramOrType = this.chartConfigBuilder('complimentByProgramOrType', {});
            this.chartsConfig.complimentTrendsByType = this.chartConfigBuilder('complimentTrendsByType', {});
            this.chartsConfig.complimentsByType = this.chartConfigBuilder('complimentsByType', {});
            this.chartsConfig.complimentTrendsBySite = this.chartConfigBuilder('complimentTrendsBySite', {});

        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }


        /**
         * @description Get compliment trends and update the compliment trends model.
         */
        getComplimentTrends = (dateRange: commonInterfaces.IDateRange): void=> {
            this.chartsSvc.getComplimentTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var totalCurrentMonth = result.InScopeComplaintCount;
                    this.chartsConfig.complimentTrends = this.chartConfigBuilder('complimentTrends', {
                        totalCompliments: totalCurrentMonth,
                        legend: { enabled: false },
                        categories: result.Categories,
                        series: [{
                            name: 'Compliments',
                            lineWidth: 3,
                            data: result.InScopeData
                        }],

                    });
                    this.chartsConfig.counts.complimentTrends = {
                        count: _.where(result.InScopeData, n => (n > 0)).length,
                        outOf: result.Categories.length,
                        jointCount: 0
                    };
                    this.avgComplimentsPerMonth = totalCurrentMonth == 0 ? 0 : Math.round(totalCurrentMonth/this.noOfMonths);
            }, this.onFailure);
        }

        /**
         * @description Get compliments by type and update the compliments by type model.
         */
        getComplimentsByType = (dateRange: commonInterfaces.IDateRange): void=> {
            var $trendsDiv = $("#complimentTrendsByType").closest("div.parent-container");
            $trendsDiv.hide();
            this.chartsSvc.getComplimentsByType({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {

                if (result.Data.length > 0) {
                    var maxElement = result.Data[0];

                    /* need to correct and refactor */
                    this.chartsSvc.getComplimentTrendsByTypeOrProgram({
                        trendId: maxElement.id,
                        startDate: dateRange.startDate,
                        endDate: dateRange.endDate
                    }).then((result) => {
                        if ($trendsDiv.is(":hidden")) {
                            $trendsDiv.show();
                        }

                        this.chartsConfig.complimentTrendsByType = this.chartConfigBuilder('complimentTrendsByType', {
                            totalCompliments: result.InScopeComplimentCount,
                            categories: result.Categories,
                            series: [{
                                //name: elt.name + ' Compliments Trend',
                                name: maxElement.name + ' Compliments Trend',
                                //lineWidth: 3,
                                data: result.InScopeData,
                                //events: {
                                //    legendItemClick: () => (false)
                                //}
                            }],
                            //legend: {
                            //    enabled: true,
                            //    align: 'right',
                            //    borderWidth: 0,
                            //    verticalAlign: 'top',
                            //    floating: true,
                            //    x: 0,
                            //    y: -10
                            //},
                        });
                    });

                    $.each(result.Data,(idx, elt) => {
                        if (idx == 0) {
                            elt.sliced = true;
                            elt.selected = true;
                        }
                        elt.marker = {
                            enabled: true,
                            symbol: 'circle'
                        };
                        elt.events = {
                            click: (arg) => {
                                this.chartsSvc.getComplimentTrendsByTypeOrProgram({
                                    trendId: elt.id,
                                    startDate: dateRange.startDate,
                                    endDate: dateRange.endDate
                                }).then((result) => {
                                    if ($trendsDiv.is(":hidden")) {
                                        $trendsDiv.show();
                                    }

                                    this.chartsConfig.complimentTrendsByType = this.chartConfigBuilder('complimentTrendsByType', {
                                        totalCompliments: result.InScopeComplimentCount,
                                        categories: result.Categories,
                                        series: [{
                                            color: $(arg.srcElement).attr("fill"),
                                            name: elt.name + ' Compliments',
                                            //lineWidth: 3,
                                            data: result.InScopeData,
                                            //events: {
                                            //    legendItemClick: () => (false)
                                            //}
                                        }],
                                        //legend: {
                                        //    enabled: true,
                                        //    align: 'right',
                                        //    borderWidth: 0,
                                        //    verticalAlign: 'top',
                                        //    floating: true,
                                        //    x: 0,
                                        //    y: -10
                                        //},
                                    });
                                });
                            }
                        };
                    });
                    this.chartsConfig.complimentsByType = this.chartConfigBuilder('complimentsByType', {
                        series: [{
                            type: 'pie',
                            innerSize: '60%',
                            name: 'Compliment type share',
                            data: result.Data,
                        }]
                    });
                }
            }, this.onFailure);
        }

        public loadTrend = (id: number, color: string, name: string): void => {
            var $trendsDiv = $("#complimentTrendsByType").closest("div.parent-container");
            $trendsDiv.hide();
            var startDate = this.dateFormatterSvc.formatDateShort(this.dateRange.startDate);
            var endDate = this.dateFormatterSvc.formatDateShort(this.dateRange.endDate);
            this.chartsSvc.getComplimentTrendsByTypeOrProgram({
                trendId: id,
                startDate: startDate,
                endDate: endDate
            }).then((result) => {
                if ($trendsDiv.is(":hidden")) {
                    $trendsDiv.show();
                }

                this.chartsConfig.complimentTrendsByType = this.chartConfigBuilder('complimentTrendsByType', {
                    totalCompliments: result.InScopeComplimentCount,
                    categories: result.Categories,
                    series: [{
                        color: color,
                        name: name + ' Compliments',
                        data: result.InScopeData,
                    }],
                });
            }, this.onFailure);
        }

        /**
         * @description Get compliment trends by site and update the compliment trends by site model.
         */
        getComplimentTrendsBySite = (dateRange: commonInterfaces.IDateRange): void=> {
            this.chartsSvc.getComplimentTrendsBySite({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                var totalZeroCompliments = 0;
                //result.Data.sort(function (a, b) { return b - a });
                if (result.Data.length) {

                    var inScope = { Keys: [], Categories: [], Data: [] };

                    inScope.Keys = this.getInScopeKeys(result.Data);
                    inScope.Categories = this.getInScopeCategories(result.Categories, inScope.Keys);
                    inScope.Data = this.getInScopeData(result.Data, inScope.Keys)


                    totalZeroCompliments = this.getAllInstanceInArray(result.Data, 0).length;
                }

                this.chartsConfig.complimentTrendsBySite = this.chartConfigBuilder('complimentTrendsBySite', {
                    totalObjects: <number>result.Data.length, // either number of sites or buildings depending on the view
                    totalZeroCompliments: <number>totalZeroCompliments,
                    displayChart: totalZeroCompliments != <number>result.Data.length,
                    xAxis: {
                        categories: <any[]>inScope.Categories,
                        labels: {
                            useHTML: true,
                            formatter: function () {
                                var labelArr = this.value.split(';');
                                return "<span style='font-size: 12px; font-weight: 600;'>" + labelArr[0] + "</span>";
                            },
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                            
                        },
                    },

                    series: [
                        {
                            name: 'IN SCOPE',
                            data: <any>inScope.Data,
                            color: this.sitesettings.colors.secondaryColors.teal,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            events: {
                                legendItemClick: function () {
                                    var seriesIndex = this.index;
                                    var series = this.chart.series;

                                    series[0].chart.axes[0].categories = <any[]>inScope.Categories;

                                    for (var i = 0; i < series.length; i++) {
                                        if (series[i].index === seriesIndex) {
                                            if (!series[i].visible) series[i].setVisible(true, false);
                                        } else {
                                            if (series[i].visible) series[i].setVisible(false, false);
                                        }
                                    }

                                    this.chart.chartHeight = inScope.Data.length <= 2 ? inScope.Data.length * 110 : inScope.Data.length * 75;
                                    //this.chart.chartHeight = 400;
                                    this.chart.setSize(this.chart.chartWidth, this.chart.chartHeight);
                                    this.chart.redraw();
                                    return false;
                                }
                            }
                        },
                        {
                            name: 'ALL',
                            data: <any>result.Data,
                            color: this.sitesettings.colors.secondaryColors.teal,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                align: 'right',
                                color: '#fff',
                                x: -10
                            },
                            visible: false,
                            events: {
                                legendItemClick: function () {
                                    var seriesIndex = this.index;
                                    var series = this.chart.series;

                                    series[1].chart.axes[0].categories = <any[]>result.Categories;

                                    for (var i = 0; i < series.length; i++) {
                                        if (series[i].index === seriesIndex) {
                                            if (!series[i].visible) series[i].setVisible(true, false);
                                        } else {
                                            if (series[i].visible) series[i].setVisible(false, false);
                                        }
                                    }

                                    this.chart.chartHeight = result.Data.length <= 2 ? result.Data.length * 110 : result.Data.length * 75;
                                    this.chart.redraw();
                                    this.chart.setSize(this.chart.chartWidth, this.chart.chartHeight);
                                    this.chart.redraw();
                                    return false;
                                }
                            }
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    height: inScope.Data.length <= 2 ? inScope.Data.length * 110 : inScope.Data.length * 75,
                    yAxis: [
                        {
                            allowDecimals: false,
                            min: 0,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                            title: {
                                text: ''
                            }
                        },
                        {
                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    tooltip: null,
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        },
                        bar: {
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth
                        }
                    }

                });


            }, this.onFailure)
        }

        /**
       * @description Get keys for the data point which has more than 0 complaints
       */
        getInScopeKeys = (data) => {

            var inScopeKeys = [];

            angular.forEach(data,(item, key) => {
                if (item > 0) {
                    inScopeKeys.push(key);
                }
            });

            return inScopeKeys;
        }

        /**
         * @description Get categories based on inScope.Keys
         */
        getInScopeCategories = (categories, keys) => {

            var inScopeCategories = [];

            angular.forEach(categories,(item, categoryKey) => {
                angular.forEach(keys,(key) => {
                    if (categoryKey == key) {
                        inScopeCategories.push(item);
                    }
                });
            });

            return inScopeCategories;
        };

        /**
         * @description Get data points based on inScope.Keys
         */
        getInScopeData = (data, keys) => {

            var inScopeData = [];

            angular.forEach(data,(item, dataKey) => {
                angular.forEach(keys,(key) => {
                    if (dataKey == key) {
                        inScopeData.push(item);
                    }
                });
            });

            return inScopeData;
        };

        /**
         * @description Get 4 latest compliments.
         */
        getRecentCompliments = (): void => {
            this.recentCompliments = [];
            this.viewLink = this.basePath + 'Quality/Compliment/EntryView/';
            var sDate = this.dateFormatterSvc.formatDateFull(this.dateRange.startDate);
            var eDate = this.dateFormatterSvc.formatDateFull(this.dateRange.endDate);

            var filterQuery;

            if (this.programId > 0) {
                filterQuery = `CreateDate ge DateTime'${sDate}' and CreateDate le DateTime'${eDate}' and ProgramId eq ${this.programId}`;
            }
            else {
                filterQuery = `CreateDate ge DateTime'${sDate}' and CreateDate le DateTime'${eDate}'`;
            }

            var param = <qualitySvc.IODataPagingParamModel>{
                $filter: filterQuery,
                $orderby: 'CreateDate desc',
                $top: 5,
                $skip: 0
            };

            this.complimentsSvc.getCompliments(param)
                .then((data) => {
                    angular.forEach(data, (obj) => {
                        angular.forEach(obj.AccountableEmployeesInformation, (empObj) => {
                            empObj.ProfileImage = this.imageSvc.getUserImageFromAttachments(empObj.UserAttachments, true);
                        });
                    this.recentCompliments.push(obj);
                });
            }, this.onFailure);
        }

        /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ComplimentEntryViewCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            });
        }

        getAllInstanceInArray = (arr, val) => {
            var instances = [], i = -1;
            while ((i = arr.indexOf(val, i + 1)) != -1) {
                instances.push(i);
            }
            return instances;
        }

        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getComplimentTrends(dateRange);
            this.getComplimentsByType(dateRange);
            this.getComplimentTrendsBySite(dateRange);
            this.getRecentCompliments();
        }


        /**
        * @description Defines overridable chart options.
        */
        chartConfigBuilder = (name, options) => {
            var chartOptions = {
                counts: {
                    complimentTrends: {
                        count: 0
                    }
                },
                complimentTrends: {
                    totalCompliments: 0,
                    categories: [],
                    series: [{}],
                    legend: {
                        enabled: false
                    }
                },
                complimentsByType: {
                    series: []
                },
                complimentTrendsBySite: {
                    backgroundColor:'#ffffff',
                    totalObjects: 0,
                    totalZeroCompliments: 0,
                    displayChart: false,
                    series: [{
                        name: 'Compliments',
                        data: [],
                        dataLabels: {
                            enabled: true,
                            color: '#fff',
                            inside: true
                        }
                    }],
                   // height: 400,
                    xAxis: {
                        categories: [],
                        labels: {
                            useHTML: true,
                            formatter: function () {
                                var labelArr = this.value.split(';');
                                return "<span style=' font-size: 12px; font-weight: 600;'>" + labelArr[0] + "</span>";
                                
                            },
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                            
                        }
                    },
                    yAxis: [{
                        allowDecimals: false,
                        min: 0,
                        title: {
                            text: ''
                        },
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                        {
                            linkedTo: 0,
                            opposite: true,
                            title: {
                                text: ''
                            }
                        }
                        //,
                        //labels: {
                        //    useHTML: true,
                        //    formatter: function () {
                        //        if (this.isFirst) {
                        //            return "<span style='position:absolute;left:-40px;font-weight:bold;text-align:center;'>Current<br />Month</span>";
                        //        }
                        //        else return "<span>" + this.value + "</span>";
                        //    }
                        //}
                    ],

                    tooltip: {
                        useHTML: true,
                        formatter: function () {
                            var labelArr = this.x.split(';');
                            var html = "<span>Compliments for period:</span><span style='display:inline-block;margin-left: 10px;font-weight:bold;'>" + this.y + "</span><br />";
                            html += "<span>Compliments for current month:</span><span style='display:inline-block;margin-left: 10px;font-weight:bold;'>" + labelArr[1] + "</span>";
                            return html;
                        }
                    },
                    //isLegendEnabled: true,
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        },
                    }
                },
                complimentTrendsByType: {
                    totalCompliments: null,//result.InScopeComplimentCount,
                    categories: null,//result.Categories,
                    series: [{
                        //color: $(arg.srcElement).attr("fill"),
                        //name: elt.name + ' Compliments Trend',
                        ////name: null,//maxElement.name + ' Compliments Trend',
                        //lineWidth: 3,
                        //data: null,
                        //events: {
                        //    legendItemClick: () => (false)
                        //}
                    }],
                    legend: {
                        //enabled: true,
                        //align: 'right',
                        //borderWidth: 0,
                        //verticalAlign: 'top',
                        //floating: true,
                        //x: 0,
                        //y: -10
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                },

            };

            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('ComplimentIndexCtrl', ComplimentIndexCtrl);
}  
