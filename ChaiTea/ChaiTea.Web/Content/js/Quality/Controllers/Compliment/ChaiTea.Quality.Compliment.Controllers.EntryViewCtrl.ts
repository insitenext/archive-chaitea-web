﻿/// <reference path="../../../_libs.ts" />





module ChaiTea.Quality.Compliment.Controllers {
    "use strict";

    import coreSvc = Core.Services;
    import qualitySvc = Quality.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class ComplimentEntryViewCtrl {
        compliment = {};
        floor = {};
        employees = [];
        accountableEmployeeObjects = [];
        accountableEmployeesInformation = [];
        itemWithImage = [];
        complimentId: number = 0;
        basePath: string;
        options = {
            employeeIds: [],
            employeesWithImage: []
        };
        clientName: string = '';
        employeesWithImage = [];
        moduleIcon: string;
        moduleClass: string;
        customerRepresentativeTemplate: string;
        modalLeftSection: string;
        modalRightSection: string;
        selectedEmployeeText = "ASSIGNED_EMPLOYEES";
        static $inject = [
            "$scope",
            "userInfo",
            "sitesettings",
            "userContext",
            "$log",
            "$uibModal",
            "chaitea.quality.services.employeesvc",
            "chaitea.quality.services.complimentssvc",
            "chaitea.quality.services.lookuplistsvc",
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            'id',
            '$uibModalInstance'
        ];

        constructor(
            private $scope,
            private userInfo: IUserInfo,
            sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private complimentsSvc: qualitySvc.IComplimentsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private id: number,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance
        ) {
            this.basePath = sitesettings.basePath;
            this.clientName = userContext.Client.Name;
            this.complimentId = id;
            this.moduleClass = 'compliment';
            this.moduleIcon = 'compliment';
            this.modalLeftSection = this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.compliments.leftsection.tmpl.html';
            this.modalRightSection = this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.compliments.rightsection.tmpl.html';
            this.getComplimentById(this.complimentId);

        }

        initialize = (complimentId: number): void => {
            this.complimentId = complimentId;
            this.getComplimentById(this.complimentId);
        }

        getComplimentById = (complimentId: number): void => {
            if (complimentId > 0) {
                this.complimentsSvc.getCompliment((complimentId)).then((result) => {
                    this.compliment = result;
                    this.accountableEmployeeObjects = result.AccountableEmployeeObjects;
                    this.accountableEmployeesInformation = result.AccountableEmployeesInformation;
                    this.getInitialEmployees();
                    this.getFloorByFloorId(result.FloorId);
                    this.getEmployeesWithImage();
                }, this.onFailure);
            }
        }

        getFloorByFloorId = (floorId: number): void => {
            if (floorId > 0) {
                this.lookupListSvc.getFloorByFloorId(floorId).then((result) => {
                    this.floor = result;
                }, this.onFailure);
            }
        }

        getInitialEmployees = (): void => {
            if (this.accountableEmployeeObjects != null) {
                var loopEnd = this.accountableEmployeeObjects.length <= 5 ? this.accountableEmployeeObjects.length : 5;
                for (var i = 0; i < loopEnd; i++) {
                    this.employees.push(this.accountableEmployeeObjects[i].Value);
                }
            }
        }

        addMoreEmployees = (): void => {

            if (this.accountableEmployeeObjects.length == this.employees.length) {
                return;
            }

            if (this.accountableEmployeeObjects != null && this.accountableEmployeeObjects.length > 5) {
                var loopStart = this.employees.length > 0 ? this.employees.length : 0;
                var remainingEmployees = this.accountableEmployeeObjects.length - this.employees.length;
                var loopEnd = (remainingEmployees <= 5) ? (loopStart + remainingEmployees) : (loopStart + 5);

                for (var i = loopStart; i < loopEnd; i++) {
                    this.employees.push(this.accountableEmployeeObjects[i].Value);
                }
            }
        }
        private getEmployeesWithImage = (): any => {
            angular.forEach(this.accountableEmployeesInformation, (item) => {
                if (item.IsEmployeeActive) {
                    this.apiSvc.getById(item.EmployeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((item) => {
                        this.employeesWithImage.push({
                            Name: item.Name,
                            EmployeeId: item.EmployeeId,
                            UserAttachments: item.UserAttachments,
                            HireDate: (item.HireDate ? moment(item.HireDate).format('MMM DD, YYYY') : 'N/A'),
                            JobDescription: item.JobDescription.Name,
                            ProfileImage: this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true)
                        });
                    });
                } else {
                    this.employeesWithImage.push({
                        Name: item.Name,
                        EmployeeId: item.EmployeeId,
                        UserAttachments: item.UserAttachments,
                        JobDescription: "Inactive Employee",
                        HireDate: (item.HireDate ? moment(item.HireDate).format('MMM DD, YYYY') : 'N/A'),
                        ProfileImage: this.imageSvc.getUserImageFromAttachments(item.UserAttachments, true)
                    });
                }
            }, this.onFailure);
        }

    
        getProfilePictureUniqueId = (employeeAttachments): string=> {

            var uniqueId: string = null;

            if (employeeAttachments.length > 0) {
                angular.forEach(employeeAttachments,(item) => {
                    if (item.UserAttachmentType === "ProfilePicture") {
                        uniqueId = item.UniqueId;
                    }
                });
            }
            return uniqueId;
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        edit = (): void => {
            this.redirectToEdit();
        }

        redirectToEdit = (): void => {
            window.location.replace(this.basePath + "Quality/Compliment/EntryView/" + this.complimentId);
        }

        public redirectToEntry = () => {
            window.location.href = this.basePath + "Quality/Compliment/Entry/" + this.complimentId;
        }

        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }

    angular.module("app").controller("ComplimentEntryViewCtrl", ComplimentEntryViewCtrl);
} 