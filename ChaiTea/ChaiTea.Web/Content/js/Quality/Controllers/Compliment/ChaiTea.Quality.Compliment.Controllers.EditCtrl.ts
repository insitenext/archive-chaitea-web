﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Compliment.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import qualitySvc = ChaiTea.Quality.Services;

    export interface IComplimentEditCtrl {
        complimentId: number;
        clientId: number;
        siteId: number;
        programId: number;
        initialize(complimentId: number): void;
        onFailure(response: any): void;
        onSuccess(response: any): void;
        getComplimentById(complaintId: number): void;
        checkIfEntryIsDisabled(complaintId: number, siteId: number, programId: number): boolean;
        getAllLookuplist(): void;
        getFloorsByBuildingId(buildingId: number): void;
        saveCompliment(): void;
        cancel(): void;
        redirectToDetails(): void;
        openDatePicker($event, which): void;
    }

    class ComplimentEditCtrl implements IComplimentEditCtrl {
        compliment = {
            AccountableEmployees: [],
            FeedbackDate: null,
            Description: ''
        }
        complimentId: number = 0;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;

        options = {
            complimentTypes: [],
            employees: [],
            buildings: [],
            floors: []
        };
        datepickers = {
            feedbackDate: false
        };
        maxDate: Date = new Date();
        basePath: string;
        isEntryDisabled: boolean = false;
        selectedEmployees: any[] = [];

        static $inject = [
            '$scope',
            'userInfo',
            'sitesettings',
            'userContext',
            '$log',
            '$timeout',
            '$filter',
            'chaitea.quality.services.complimentssvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope,
            private userInfo:IUserInfo,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $filter: any,
            private complimentsSvc: qualitySvc.IComplimentsSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private dateSvc: ChaiTea.Common.Services.IDateSvc) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
        }

        /** @description Initializer for the controller. */
        initialize = (complimentId: number): void => {
            this.complimentId = complimentId;

            this.isEntryDisabled = this.complimentId > 0 ? false :
                this.checkIfEntryIsDisabled(this.complimentId, this.siteId, this.programId);

            if (this.isEntryDisabled) {
                this.$timeout(() => {
                    angular.element('.site-picker a').trigger('click');
                }, 500);
                return
            }
            this.getAllLookuplist();
            this.getComplimentById(this.complimentId);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            if (response.ComplimentId > 0) {
                mixpanel.track('Compliment ' + (this.complimentId ? "Edited" : "Submitted"), { ComplimentId: response.ComplimentId });
                this.complimentId = response.ComplimentId;
            }
            bootbox.alert('Compliment was saved successfully!', this.redirectToDetails);
        }

        /** 
         * @description Get compliment object by complimentId. 
         * @param {int} complimentId
         */
        getComplimentById = (complimentId: number): void => {
            if (complimentId > 0) {
                this.complimentsSvc.getCompliment(complimentId).then((result) => {
                    result.FeedbackDate = moment(result.FeedbackDate).toDate();
                    this.compliment = result;
                    this.selectedEmployees = this.compliment.AccountableEmployees;
                    this.getFloorsByBuildingId(result.BuildingId);
                }, this.onFailure);
            } else {
                this.compliment = {
                    FeedbackDate: this.dateFormatterSvc.formatDateShort(new Date(), 'MM/DD/YYYY'),
                    AccountableEmployees: [],
                    Description: ''
                };
            }
        }

        /**
         * @description Check if data entry is disabled.
         * @param {number} complimentId
         * @param {number} siteId
         * @param {number} programId
         */
        checkIfEntryIsDisabled = (complimentId: number, siteId: number, programId: number): boolean => {
            return complimentId == 0 && (programId == 0 || siteId == 0);
        }

        /**
        * @description Populate bindings from lookup service.
        */
        getAllLookuplist = (): void => {
            this.lookupListSvc.getComplimentTypes({}).then((data) => {
                this.options.complimentTypes = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getEmployeesForCompliment({ id: this.complimentId }).then((data) => {
                this.options.employees = data.Options;
            }, this.onFailure);

            this.lookupListSvc.getBuildingsForCompliment({ id: this.complimentId }).then((data) => {
                this.options.buildings = data.Options;
            }, this.onFailure);
        }

        /** 
         * @description Get floors based on buildingId. 
         * @param {number} buildingId
         */
        getFloorsByBuildingId = (buildingId: number): void => {
            if (buildingId > 0) {
                this.lookupListSvc.getFloors({ id: buildingId }).then((data) => {
                    this.options.floors = data.Options;
                }, this.onFailure);
            }
        }

        /** @description On building field change event. */
        onBuildingChange = this.getFloorsByBuildingId;

        /** @description Create/update a new compliment. */
        saveCompliment = (): void => {
            if (this.$scope.complimentForm.$valid) {
                this.compliment.Description = this.$filter('htmlToPlaintext')(this.compliment.Description);
                // [NS 06/04/2015]: Selected employees aren't being pushed to the server
                this.compliment.AccountableEmployees = this.selectedEmployees;
                var tempDate = new Date();
                this.compliment.FeedbackDate = this.dateFormatterSvc.formatDateFull(new Date(this.compliment.FeedbackDate.getFullYear(), this.compliment.FeedbackDate.getMonth(), this.compliment.FeedbackDate.getDate(),
                    tempDate.getHours(), tempDate.getMinutes(), tempDate.getSeconds()));
                this.compliment.FeedbackDate = this.dateSvc.setCustomDatesToUtcDateTimeOffset(this.compliment.FeedbackDate);
                if (this.complimentId > 0) {
                    // PUT
                    this.complimentsSvc.updateCompliment(this.complimentId, this.compliment).then(this.onSuccess, this.onFailure);

                } else {
                    // POST
                    this.complimentsSvc.saveCompliment(this.compliment).then(this.onSuccess, this.onFailure);
                }
            }
        };

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            this.redirectToDetails('cancel');
        }

        /** @description Redirect to details page. */
        redirectToDetails = (name: string = ""): void=> {
            window.location.replace(this.basePath + "Quality/Compliment/Details");
        }

        /** 
         * @description Handle opening datepicker. 
         * @param {evt} $event
         * @param {string} which - The name of the datepicker.
         */
        openDatePicker = ($event, which): void => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;
        }
    }

    angular.module('app').controller('ComplimentEditCtrl', ComplimentEditCtrl);
} 