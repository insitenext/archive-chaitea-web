﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Compliment.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    class ComplaintDetailCtrl {
        siteId: number = 0;
        isEntryDisabled: boolean = false;

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 20;
        compliments= [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        basePath: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: new Date()
        };

        // Filter variables
        filterOptions = [
            { Key: 'ComplimentTypeId', Value: 'Type', Options: null, FieldType: 'select' },
            { Key: 'ProgramId', Value: 'Program', Options: null, FieldType: 'select' },
            { Key: 'BuildingId', Value: 'Building', Options: null, FieldType: 'select' },
            { Key: 'CustomerName', Value: 'From', Options: null, FieldType: 'text' }
            
        ];

        filter = {
            filterType: null,
            filterCriteria: null
        };
        boolFilter: boolean = true;
        //for Components.TabFilterHeader
        tabFilterItems: ChaiTea.Component.TabFilterItem[] = [
            {
                title: 'All',
                onClickFn: () => { this.boolFilter = false; this.getComplimentsReset() }
            }
        ];

        isFilterCollapsed: boolean = true;
        localStorageComplimentFilterKey: string;

        isCardView: boolean = true;
        module: string = 'compliments';
        viewLink: string = '';
        headerText: string = 'COMPLIMENT_DATE';
        clientName: string = '';
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$timeout',
            'userContext',
            '$log',
            'chaitea.quality.services.complimentssvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.quality.services.lookuplistsvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.localdatastoresvc',
            Common.Services.DateSvc.id,
            '$uibModal'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $timeout: angular.ITimeoutService,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private complimentsSvc: qualitySvc.IComplimentsSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private dateSvc: ChaiTea.Common.Services.DateSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.clientName = userContext.Client.Name;

            this.localStorageComplimentFilterKey = 'compliment-filter-' + this.userContext.Client.ID + '-' + this.userContext.Site.ID + '-' + this.userContext.Program;
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;

                // Reset the paging
                this.getComplimentsReset({
                    startDate: this.dateFormatterSvc.formatDateFull(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(obj.endDate)
                });
            });

            //setting filters from session storage.
            var filterObj = this.localDataStoreSvc.getObject(this.localStorageComplimentFilterKey);
            if (filterObj) {
                this.filter = filterObj;
                this.isFilterCollapsed = false;
            }
           
            this.getFilterOptions();
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {
            //Remove building from filterOptions if a site has been chosen.
            if (!this.siteId) {
                _.remove(this.filterOptions,(data) => (data['Key'] == 'BuildingId'));
            }
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging and get compliments.
         */
        getComplimentsReset = (dateRange?: commonInterfaces.IDateRange) => {
            this.isBusy = false;
            this.compliments = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getCompliments(dateRange);
        }

        /**
         * @description Get the filter options.
         */
        getFilterOptions = (): angular.IPromise<any> => {
            var lookupList = [
                this.lookupListSvc.getComplimentTypes(),
                this.lookupListSvc.getPrograms()
            ];

            //Add building to filterOptions if a site has been chosen.
            if (this.siteId) {
                lookupList.push(this.lookupListSvc.getBuildings());
            }

            return this.$q.all(lookupList)
                .then((result:Array<any>) => {
                this.filterOptions[0].Options = result[0].Options;
                this.filterOptions[1].Options = result[1].Options;
                
                //get building results from array
                if(this.siteId)
                    this.filterOptions[2].Options = result[2].Options;
            });
        }

        /**
         * @description Get complaints and push to array for infinite scroll.
         */
        getCompliments = (dateRange?: commonInterfaces.IDateRange): void => {
            this.viewLink = this.basePath + 'Quality/Compliment/EntryView/';
            dateRange = dateRange || {
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            };


            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = this.getPagingParams(dateRange);

            this.complimentsSvc.getCompliments(params)
                .then((data) => {
                    if (data.length == 0) {
                        this.isAllLoaded = true;
                    }

                    angular.forEach(data, (obj) => {
                        angular.forEach(obj.AccountableEmployeesInformation, (empObj) => {
                            empObj.ProfileImage = this.imageSvc.getUserImageFromAttachments(empObj.UserAttachments, true);
                        });
                        this.compliments.push(obj);
                    });
                    this.isBusy = false;
                    this.recordsToSkip += this.top;
                }, this.onFailure);
        }

        /**
         * @description Open details pop-up
         */
        public ShowEntryViewModal = (id): void => {

            this.$uibModal.open({
                templateUrl: this.basePath + 'Content/js/Quality/Templates/EntryViewModals/quality.entryview.common.tmpl.html',
                controller: 'ComplimentEntryViewCtrl as vm',
                size: 'lg',
                resolve: {
                    id: () => {
                        return id;
                    }
                }
            });
        }

        /**
       * @description to export compliments details to excel sheet 
       */
        export = (): void=> {
            var mystyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } },
            };
            var complimentsToExport = [];
            for (var i = 0; i < this.compliments.length; i++) {
                complimentsToExport.push({
                    "CustomerName": this.compliments[i].CustomerName, "Program": this.compliments[i].ProgramName, "ComplimentType": this.compliments[i].ComplimentTypeName,
                    "Description": this.compliments[i].Description, "Site": this.compliments[i].SiteName, "Building": this.compliments[i].BuildingName, "ComplimentDate": this.compliments[i].FeedbackDate
                });
            }
            alasql('SELECT * INTO CSV("complimentsdetails.csv",?) FROM ?', [mystyle, complimentsToExport]);
        }


        /**
         * @description Return a paging param for the OData filter.
         */
        getPagingParams = (dateRange: commonInterfaces.IDateRange) => {
            var EndDate = moment(dateRange.endDate).endOf('day').format();
            var startDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(dateRange.startDate);
            var endDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(EndDate);
            var params = <qualitySvc.IODataPagingParamModel>{
                $orderby: 'FeedbackDate desc',
                $top: this.top,
                $skip: this.recordsToSkip,
                $filter: `FeedbackDate gt DateTime'${startDate}' and FeedbackDate lt DateTime'${EndDate}'`
            };

            // Custom field filter
            if (this.filter.filterCriteria && this.filter.filterType) {
                var filter = '';

                switch (this.filter.filterType) {
                    case 'CustomerName':
                        filter = `startswith(${this.filter.filterType}, '${this.filter.filterCriteria}')`;
                        break;
                    default:
                        filter = `${this.filter.filterType} eq ${this.filter.filterCriteria}`;
                }

                params['$filter'] += ` and ${filter}`;

                //saving filter in session storage
                this.localDataStoreSvc.setObject(this.localStorageComplimentFilterKey, this.filter);
            }
            else {
                //remove filter from session storage
                this.localDataStoreSvc.setObject(this.localStorageComplimentFilterKey, '');
                this.isFilterCollapsed = true;
            }

            return params;
        }

        /**
         * @description Delete a compliment. 
         * @param {int} complimentId
         * */
        deleteCompliment = (complimentId: number): void => {
            bootbox.confirm('Are you sure you want to delete this compliment?', (result) => {
                if (result) {
                    this.complimentsSvc.deleteCompliment(complimentId).then((result) => {
                        for (var i = 0; i < this.compliments.length; i++) {
                            if (this.compliments[i].ComplimentId === complimentId) {
                                this.compliments.splice(i, 1);
                                break;
                            }
                        }
                    }, this.onFailure);
                }
            });
        }
    }

    angular.module('app').controller('ComplimentDetailCtrl', ComplaintDetailCtrl);
}  