﻿/// <reference path="../../../_libs.ts" />
 
module ChaiTea.Quality.Audit.Controllers {
    "use strict";

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import qualityInterfaces = Quality.Interfaces;

    interface IEmployeeWithImage {
        EmployeeId: number;
        EmployeeName: string;
        UniqueId: string;
        EmployeeImage: string;
    }

    class EmployeeAuditsAvgCtrl {
        modalInstance;
        employees = [];
        employeesByArea = [];
        employeeByArea = [];
        options = {
            employeeIds: [],
            employeesWithImage: []
        };
        dateRange: commonInterfaces.IDateRange = {
            startDate: this.dateFormatterSvc.formatDateShort(moment(new Date()).subtract(5, 'months').startOf('month')),
            endDate: this.dateFormatterSvc.formatDateShort(new Date())
        };

        static $inject = [
            '$scope',
            '$uibModal',
            "chaitea.quality.services.employeesvc",
            'chaitea.quality.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.imagesvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private reportSvc: qualitySvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
        ) {

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = this.dateFormatterSvc.formatDateShort(obj.startDate);
                this.dateRange.endDate = this.dateFormatterSvc.formatDateShort(obj.endDate);

                this.getAuditsByEmployee({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });


                this.getAuditsByEmployeeByArea({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });

            this.modalInstance = this.$uibModal;
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {}

        /*
         * Description To get average audits score of employees
         */
        private getAuditsByEmployee = (dateRange: commonInterfaces.IDateRange) => {
            this.reportSvc.getEmployeeAuditsAvg(this.dateRange).then((data) => {

                this.employees = [];

                angular.forEach(data,(obj) => {
                    this.employees.push(obj);
                });

                this.getEmployeesWithImage();
            });
        }
        /*
         * Description To get average audits score of employees By Area
         */
        private getAuditsByEmployeeByArea = (dateRange: commonInterfaces.IDateRange) => {
            this.reportSvc.getEmployeeAuditsAvgByArea(this.dateRange).then((data) => {

                this.employeesByArea = [];

                angular.forEach(data,(obj) => {
                    this.employeesByArea.push(obj);
                });

            });
        }

        getEmployeesWithImage = (): void=> {
            if (this.employees.length > 0) {
                angular.forEach(this.employees,(item) => {
                    this.options.employeeIds.push(item.EmployeeId);
                });

                this.employeeSvc.getEmployeeByIds(this.options.employeeIds).then((result) => {
                    if (result.length > 0) {
                        this.options.employeesWithImage = [];
                        angular.forEach(result, (item) => {
                            var uniqueId = this.getProfilePictureUniqueId(item.UserAttachments)
                            this.options.employeesWithImage.push(<IEmployeeWithImage>{
                                EmployeeId: item.EmployeeId,
                                EmployeeName: item.FirstName + " " + item.LastName,
                                UniqueId: uniqueId,
                                EmployeeImage: this.imageSvc.get(uniqueId)
                            });
                        });
                    }
                });
            }
        }

        getProfilePictureUniqueId = (employeeAttachments): string=> {

            var uniqueId: string = null;

            if (employeeAttachments.length > 0) {
                angular.forEach(employeeAttachments,(item) => {
                    if (item.UserAttachmentType === "ProfilePicture") {
                        uniqueId = item.UniqueId;
                    }
                });
            }
            return uniqueId;
        }

        /**
       * @description Open the modal instance to show employee avg score by area
       */
        showAvgByArea = (index: number): void => {
            this.employeeByArea= [];
            this.modalInstance = this.$uibModal.open({
                templateUrl: 'showAvgByArea.html',
                controller: 'EmployeeAvgAuditScoreByAreaCtrl as vm',
                size: 'md',
                resolve: {
                    employee: () => { return this.employees[index] },
                    areas: () => {
                        var empId = this.employees[index].EmployeeId;
                        var programName = this.employees[index].ProgramName;
                        for (var i = 0; i < this.employeesByArea.length; i++) {
                            if (this.employeesByArea[i].EmployeeId == empId && this.employeesByArea[i].ProgramName == programName) {
                                this.employeeByArea.push(this.employeesByArea[i]);
                            }
                        }
                        return this.employeeByArea;
                    }
                }
            });

        }

    }

    class EmployeeAvgAuditScoreByAreaCtrl implements qualityInterfaces.IAuditProfileSectionEmployeesModalCtrl{

        static $inject = ['$scope', '$uibModalInstance','employee', 'areas'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private employee: any[],
            private areas: any[]) {}

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

    }

    angular.module("app").controller("Quality.EmployeeAuditsAvgCtrl", EmployeeAuditsAvgCtrl);
    angular.module("app").controller("EmployeeAvgAuditScoreByAreaCtrl", EmployeeAvgAuditScoreByAreaCtrl);
} 