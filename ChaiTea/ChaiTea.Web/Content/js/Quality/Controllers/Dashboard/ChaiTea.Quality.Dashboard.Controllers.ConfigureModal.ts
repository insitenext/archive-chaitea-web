﻿module ChaiTea.Quality.Dashboard.Controllers {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    const currentEmployeeIdQueryParam = Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString();
    import enums = Common.Enums;

    interface ILinkItem {
        title: string;
        subTitle?: string;
        icon: string;
        href: string;
    }
    interface IUserDashBoardWidget {
        UserDashboardWidgetId: number;
        UserId: number;
        DashboardWidgetId: number;
        IsVisible: boolean;
    }


    enum FileType { Undefined, ProfilePicture };

    class ConfigureModalCtrl implements ChaiTea.Common.Interfaces.INgController {        
        staticDashboardTileInfoFlat = [
            { id: enums.DashboardWidgets.AuditTrend, name: 'AUDITS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.SurveysTrend, name: 'SURVEYS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.ComplaintsTrend, name: 'COMPLAINTS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.TodosCurrentMonth, name: 'TO-DOs', checked: false, dragging: true, editable: true }, 
            { id: enums.DashboardWidgets.AttendanceCurrentMonth, name: 'ATTENDANCE', checked: false, dragging: true, editable: true },           
            { id: enums.DashboardWidgets.WorkOrdersTrend, name: 'WORK ORDERS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.ConductCurrentMonth, name: 'CONDUCT', checked: false, dragging: true, editable: true },           
            { id: enums.DashboardWidgets.ReportItsTrend, name: 'REPORT ITS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'COMPLIMENTS', checked: false, dragging: true, editable: true },
            { id: enums.DashboardWidgets.ProfessionalismCurrentMonth, name: 'PROFESSIONALISM', checked: false, dragging: true, editable: true }        
        ];
        staticDashboardSectionNames = ['Half', 'Full', 'Thirds'];        
        staticDashboardTileInfo = [
            {
                name: 'Half',
                tiles: [
                    { id: enums.DashboardWidgets.AuditTrend, name: 'AUDITS', checked: false, dragging: true, editable: true },
                    { id: enums.DashboardWidgets.SurveysTrend, name: 'SURVEYS', checked: false, dragging: true, editable: true }
                ]
            }, {
                name: 'Full',
                tiles: [
                    { id: enums.DashboardWidgets.ComplaintsTrend, name: 'COMPLAINTS', checked: false, dragging: true, editable: true }
                ]
            }, {
                name: 'Thirds',
                tiles: [
                    { id: enums.DashboardWidgets.TodosCurrentMonth, name: 'TO-DOs', checked: false, dragging: true, editable: true }, 
                    { id: enums.DashboardWidgets.AttendanceCurrentMonth, name: 'ATTENDANCE', checked: false, dragging: true, editable: true },                   
                    { id: enums.DashboardWidgets.WorkOrdersTrend, name: 'WORK ORDERS', checked: false, dragging: true, editable: true },       
                    { id: enums.DashboardWidgets.ConductCurrentMonth, name: 'CONDUCT', checked: false, dragging: true, editable: true },             
                    { id: enums.DashboardWidgets.ReportItsTrend, name: 'REPORT ITS', checked: false, dragging: true, editable: true },
                    { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'COMPLIMENTS', checked: false, dragging: true, editable: true },
                    { id: enums.DashboardWidgets.ProfessionalismCurrentMonth, name: 'PROFESSIONALISM', checked: false, dragging: true, editable: true }            
                ]
            }
        ];    
        dashboardInfo = [];
        dashboardId = Common.Enums.DashboardIds.Quality;
        isClick: boolean = true;
        navigateToItem: number;
        widgets: enums.DashboardWidgets;
        todoId = enums.DashboardWidgets.TodosCurrentMonth;
        static $inject = [
            '$scope',
            '$log',
            'userInfo',
            'userContext',
            'sitesettings',
            '$uibModal',
            '$window',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.notificationsvc',
            '$uibModalInstance',
            'dashBoardTileInfo',          
        ];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $window: angular.IWindowService,
            private $timeout: angular.ITimeoutService,
            private apiSvc: Common.Services.IApiBaseSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private dashBoardTileInfo
        ) {
            this.getDashboardItems();            
            var sectionSettings = JSON.parse($window.localStorage.getItem('sbm-user-quality-dashboard-sections:' + this.userContext.UserId));            
            if (sectionSettings) {              
                this.staticDashboardTileInfo = sectionSettings;
            }
        }

        public initialize = () => {
        }

        /**
        * @description Make visible the items the user has already configured
        */
        private getDashboardItems = () => {
            var params = {
                userId: this.userContext.UserId,
                dashboardId: this.dashboardId
            }
            this.apiSvc.query(params, "UserDashboardWidget?userid=:userId&dashboardid=:dashboardId").then((result) => {
                this.dashboardInfo = result;
                this.checkUserTiles();
            }, this.onFailure);
        }

        /**
         * @description Find Tile object
         */
        private getTileObj = (id: number) => {
            var obj;
            _.some(this.staticDashboardTileInfo, function (section) {
                return _.some(section.tiles, function (tile) {
                    if (tile.id === id) {
                        obj = tile; return true;
                    }
                });
            });
            return obj;
        }

        /**
         * @description Make visible the items the user has already configured
         */
        private checkUserTiles = () => {
            angular.forEach(this.dashBoardTileInfo, (item, key) => {
                var obj = _.find(this.staticDashboardTileInfoFlat, { 'id': item.id });
                var objFlat = this.getTileObj(item.id);
                if (obj && item.visible) {
                    obj.checked = true;
                    objFlat.checked = true;
                }
                var oldIndex = _.findIndex(this.staticDashboardTileInfoFlat, { 'id': item.id });
                if (key != oldIndex) {
                    var obj = _.find(this.staticDashboardTileInfoFlat, { 'id': item.id });
                    var oldObj = this.staticDashboardTileInfoFlat[key];
                    this.staticDashboardTileInfoFlat[key] = obj;
                    this.staticDashboardTileInfoFlat[oldIndex] = oldObj;
                }
            });
        }

        /**
         * @description Make visible/invisible the items the user has selected
         */
        private changeItem = (id: number) => {
            var obj = _.find(this.staticDashboardTileInfoFlat, { 'id': id });
            var obj2 = this.getTileObj(id);
            if (obj.editable) {
                obj.checked = !obj.checked;
                obj2.checked = !obj2.checked;
                this.$timeout(() => {
                    obj2.dragging = false;
                }, 200);

            }
        }

        private sortDashboardTiles = () => {
            var tiles = [];            
            angular.forEach(this.staticDashboardTileInfo, (section) => {                
                angular.forEach(section.tiles, (tile) => {
                    tiles.push(tile);
                });
            });
            this.staticDashboardTileInfoFlat = tiles;            
        }

        /**
         * @description Save dashboard Items
         */
        private saveItems = () => {
            var toBeUpdated = [];
            var toBeCreated = [];
            var sections = [];
            angular.forEach(this.staticDashboardTileInfoFlat, (item) => {
                var obj = _.find(this.dashboardInfo, { 'DashboardWidgetId': item.id });
                if (!obj && item.checked) {
                    toBeCreated.push(item);
                }
            });
            angular.forEach(this.staticDashboardTileInfoFlat, (item) => {
                var obj = <IUserDashBoardWidget>_.find(this.dashboardInfo, { 'DashboardWidgetId': item.id });
                if (obj && obj.IsVisible != item.checked) {
                    obj.IsVisible = item.checked;
                    toBeUpdated.push(obj);
                }
            });
            angular.forEach(toBeUpdated, (item) => {
                var params = {
                    UserDashboardWidgetId: item.UserDashboardWidgetId,
                    UserId: item.UserId,
                    DashboardWidgetId: item.DashboardWidgetId,
                    DashboardId: this.dashboardId,
                    IsVisible: item.IsVisible
                }
                this.apiSvc.update(item.UserDashboardWidgetId, params, "UserDashboardWidget").then((result) => {
                }, this.onFailure);
            });

            angular.forEach(toBeCreated, (item) => {
                var params = {
                    UserId: this.userContext.UserId,
                    DashboardWidgetId: item.id,
                    DashboardId: this.dashboardId,
                    IsVisible: item.checked
                }
                this.apiSvc.save(params, "UserDashboardWidget").then((result) => {
                }, this.onFailure);
            });

            // Order flat collection and save to local the collection to preserve section order            
            this.sortDashboardTiles();
            this.$window.localStorage.setItem('sbm-user-quality-dashboard-sections:' + this.userContext.UserId, JSON.stringify(this.staticDashboardTileInfo));
            this.$uibModalInstance.close(this.staticDashboardTileInfoFlat);
        }

        /**
        * @description On mouse down
        */
        private checkDrag = (item) => {
            item.dragging = true;
            this.$timeout(() => {
                item.dragging = false;
            }, 1000);
        }        

        /**
         * @description On dragging the options
         */
        private ondragStartOption = (id: number, item) => {
            this.navigateToItem = id;
            item.dragging = true;
        }

        /**
         * @description On dropping the option
         */
        private onDropCompleteOption = (sectionIndex, index, item) => {            
            if (_.contains(this.staticDashboardSectionNames, item.name)) return false;
            var obj = _.find(this.staticDashboardTileInfoFlat, { 'id': item.id });                       
            obj.dragging = false;            
            if (item) {
                this.toggleOptionTiles(sectionIndex, index, item);
            }
        }

        /**
         * @description On dropping the section
         */
        private onDropCompleteOptionSection = (index, section) => {
            if (!_.contains(this.staticDashboardSectionNames, section.name)) return false;
            var obj = this.staticDashboardTileInfo[index];     
            if (section) {
                this.toggleOptionSection(index, section);
            }                             
        }

        /**
         * @description Set new index values for section
         */
        private toggleOptionSection = ($index, item) => {
            this.isClick = true;
            var otherIndex = this.staticDashboardTileInfo.indexOf(item);            
            if (otherIndex > $index) {
                this.staticDashboardTileInfo.splice($index, 0, item);
                this.staticDashboardTileInfo.splice(otherIndex + 1, 1);
            }
            else {
                this.staticDashboardTileInfo.splice($index + 1, 0, item);
                this.staticDashboardTileInfo.splice(otherIndex, 1);
            }
        }
       

        /**
         * @description Set new index values
         */
        private toggleOptionTiles = (sectionIndex, $index, item) => {
            this.isClick = true;
            var otherIndex = this.staticDashboardTileInfo[sectionIndex].tiles.indexOf(item);
            var obj = this.staticDashboardTileInfo[sectionIndex].tiles[$index];            
            // check to see if it's the same section, otherwise return false 
            if (otherIndex == -1) return false;                       
            if (otherIndex > $index) {
                this.staticDashboardTileInfo[sectionIndex].tiles.splice($index, 0, item);
                this.staticDashboardTileInfo[sectionIndex].tiles.splice(otherIndex + 1, 1);                
            }
            else {
                this.staticDashboardTileInfo[sectionIndex].tiles.splice($index + 1, 0, item);
                this.staticDashboardTileInfo[sectionIndex].tiles.splice(otherIndex, 1);                
            }
        }

        /**
         * @description Dismiss the modal instance.
         */
        private close = (): void => {
            this.$uibModalInstance.close(this.staticDashboardTileInfo);
        };

        /**
        * @description log error and notify error to user
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

    }

    angular.module('app').controller('Quality.Dashboard.ConfigureModalCtrl', ConfigureModalCtrl);
}