﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Dashboard.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import enums = Common.Enums;

    class DashboardIndexCtrl {
        basePath: string = '';
        clientId: number = 0;
        programId: number = 0;
        siteId: number = 0;
        isEmployee: boolean = false;
        coreColors: ICoreColors;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        goalLineWidth: number = 2;
        chartSettings: IChartSettings;
        reportsSettings = {
            audits: {
                goalLine: 0,
                maxScore: 0
            },
            surveys: {
                controlLimit: 0
            },
            complaints: {
                controlLimit: 0
            }
        };
        dashboardItem = {
            audits: {
                categories: [],
                internalAuditData: [],
                jointAuditData: [],
                lastUpdatedDate: ''
            },
            surveys: {
                lastUpdatedDate: ''
            },
            complaints: {
                lastUpdatedDate: ''
            },
            todos: {
                currentOpen: {
                    count: 0,
                    lastOpenDate: null
                },
                pastDue: {
                    count: 0,
                    oldestIncompleteDate: null
                },
                closedOnTime: {
                    percent: 0,
                    lastClosed: null
                },
                lastUpdatedDate: ''
            },
            workOrder: {
                lastUpdatedDate: ''
            },
            reportIts: {
                lastUpdatedDate: ''
            },
            compliments: {
                items: [],
                lastUpdatedDate: ''
            },
            attendance: {
                totalCount: 0,
                lastUpdatedDate: ''
            },
            conduct: {
                count: 0,
                lastUpdatedDate: ''
            },
            professionalism: {
                passingCount: 0,
                notPassingCount: 0,
                incompleteCount: 0,
                totalCount: 0,
                lastUpdatedDate: ''
            }
        }

        reportsConfig = {
            surveysTrend: {},
            auditsTrend: {},
            complaintsTrend: {},
            workOrdersTrend: {},
            reportItsTrend: {},
            attendanceTrend: {},
            professionalismTrend: {}
        };

        localStoreDashboardSettingsKey = "";
        dashboardId = Common.Enums.DashboardIds.Quality;
        dashBoardTileInfo = [
            { id: enums.DashboardWidgets.AuditTrend, name: 'Audits', url: 'Quality/Audit/', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12 clear', visible: true, section: 'half' },
            { id: enums.DashboardWidgets.SurveysTrend, name: 'Surveys', url: 'Quality/CustomerSurvey/', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12', visible: false, section: 'half' },
            { id: enums.DashboardWidgets.ComplaintsTrend, name: 'Complaints', url: 'Quality/Complaint/', class: 'col-md-12 col-lg-12 col-sm-12 col-xs-12', visible: false, section: 'full' },
            { id: enums.DashboardWidgets.TodosCurrentMonth, name: 'Todo', url: 'Quality/Todo/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12 clear', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.AttendanceCurrentMonth, name: 'Attendance', url: 'Personnel/Attendance/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.WorkOrdersTrend, name: 'WorkOrders', url: 'Quality/WorkOrder/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.ConductCurrentMonth, name: 'Conduct', url: 'Personnel/Conduct/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.ReportItsTrend, name: 'ReportIts', url: 'Personnel/Ownership/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.ComplimentsMostRecent, name: 'Compliments', url: 'Quality/Compliment/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' },
            { id: enums.DashboardWidgets.ProfessionalismCurrentMonth, name: 'Professionalism', url: 'Personnel/Audit/', class: 'col-md-4 col-lg-4 col-xs-12 col-sm-12', visible: false, section: 'thirds' }
        ];
        pieColors: string = '';
        isCustomer: boolean = false;
        static $inject = [
            '$scope',
            'userInfo',
            'sitesettings',
            'userContext',
            'chaitea.quality.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            '$q',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$window',
            '$uibModal'];
        constructor(
            private $scope: angular.IScope,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private chartsSvc: qualitySvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private $q: angular.IQService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $window: angular.IWindowService,
            private $uibModal: angular.ui.bootstrap.IModalService) {
            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.clientId = userContext.Client.ID;
            this.programId = userContext.Program.ID;
            this.siteId = userContext.Site.ID;
            this.isEmployee = this.userInfo.mainRoles.employees;
            this.isCustomer = this.userInfo.mainRoles.customers;
            this.coreColors = this.sitesettings.colors.coreColors;

            this.localStoreDashboardSettingsKey = 'sbm-user-quality-dashboard-settings:' + this.userContext.UserId;
            //To-do - remove this in the next iteration
            //this.$window.localStorage.removeItem('sbm-user-quality-dashboard-settings:' + this.userContext.UserId);

            var dashboardSettings = JSON.parse($window.localStorage.getItem(this.localStoreDashboardSettingsKey));
            // Repaint tiles base on localStorage
            if (dashboardSettings) {
                var localStorageTileOrder = [];
                dashboardSettings.forEach(((tile) => {
                    localStorageTileOrder.push(tile);
                }));
                this.dashBoardTileInfo = localStorageTileOrder;
            }
            var tileInfo = this.dashBoardTileInfo;

            if (dashboardSettings) {
                if (tileInfo.length > dashboardSettings.length) {
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
                else if (dashboardSettings.length > tileInfo.length) {
                    dashboardSettings.forEach(((tile, index, object) => {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index, 1);
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                } else {
                    dashboardSettings.forEach(((tile, index, object) => {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index, 1);
                        }
                    }));
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
            }
            if (dashboardSettings && !_.some(dashboardSettings, (o) => _.isNull(o))) {
                this.dashBoardTileInfo = dashboardSettings;
            }
            this.getDashboardItems();
        }

        private getDashboardItems = () => {
            var params = {
                userId: this.userContext.UserId,
                dashboardId: this.dashboardId
            }
            this.apiSvc.query(params, "UserDashboardWidget?userid=:userId&dashboardid=:dashboardId").then((result) => {
                if (result.length > 0) {
                    angular.forEach(result, (item) => {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': item.DashboardWidgetId });
                        if (obj && obj.visible != item.IsVisible) {
                            obj.visible = item.IsVisible;
                        }
                    });
                }
                else {
                    angular.forEach(this.dashBoardTileInfo, (item) => {
                        item.visible = true;
                        var params = {
                            UserId: this.userContext.UserId,
                            DashboardWidgetId: item.id,
                            DashboardId: this.dashboardId,
                            IsVisible: true
                        }
                        this.apiSvc.save(params, "UserDashboardWidget").then((result) => {
                        }, this.onFailure);
                    });
                }
                this.getDashBoardInfo();
            }, this.onFailure);
        }

        private showConfigOptions = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Quality/Templates/quality.dashboard.configure.options.tmpl.html',
                controller: 'Quality.Dashboard.ConfigureModalCtrl as vm',
                size: 'md',
                resolve: {
                    dashBoardTileInfo: () => {
                        return this.dashBoardTileInfo;
                    }
                }
            }).result.then(dashboardTileUpdated => {
                if (!dashboardTileUpdated) return false;
                angular.forEach(dashboardTileUpdated, (item, key) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': item.id });
                    var objIndex = _.findIndex(this.dashBoardTileInfo, { 'id': item.id });
                    if (obj && obj.visible != item.checked) {
                        obj.visible = item.checked;
                    }
                    if (key != objIndex) {
                        this.dashBoardTileInfo.splice(key, 0, obj);
                        this.dashBoardTileInfo.splice(objIndex + 1, 1);
                    }
                });
                // Set first half section item and first third section item with class clear
                var firstHalfTileClear = false;
                var firstThirdsTileClear = false;
                angular.forEach(this.dashBoardTileInfo, (item) => {
                    if (!firstHalfTileClear && item.section == 'half' && _.includes(item.class, 'clear')) {
                        firstHalfTileClear = true;
                    } else if (firstHalfTileClear && item.section == 'half' && _.includes(item.class, 'clear')) {
                        item.class = item.class.replace("clear", '');
                    } else if (!firstHalfTileClear && item.section == 'half' && !_.includes(item.class, 'clear')) {
                        item.class = item.class.concat(" clear");
                        firstHalfTileClear = true;
                    }
                    if (!firstThirdsTileClear && item.section == 'thirds' && _.includes(item.class, 'clear')) {
                        firstThirdsTileClear = true;
                    } else if (firstThirdsTileClear && item.section == 'thirds' && _.includes(item.class, 'clear')) {
                        item.class = item.class.replace("clear", '');
                    } else if (!firstThirdsTileClear && item.section == 'thirds' && !_.includes(item.class, 'clear')) {
                        item.class = item.class.concat(" clear");
                        firstThirdsTileClear = true;
                    }
                });
                this.dashBoardTileInfo = _.sortBy(this.dashBoardTileInfo, (item) => {
                    return item.visible ? 0 : 1;
                });
                this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(this.dashBoardTileInfo));
                // TODO: Find a way to repaint graphs withouth having to hit the APIs again
                this.reportsConfig.complaintsTrend = {};
                this.reportsConfig.auditsTrend = {};
                this.reportsConfig.reportItsTrend = {};
                this.reportsConfig.surveysTrend = {};
                this.reportsConfig.workOrdersTrend = {};
                this.reportsConfig.attendanceTrend = this.chartConfigBuilder('attendance', {});
                this.reportsConfig.professionalismTrend = this.chartConfigBuilder('professionalism', {});
                this.getDashBoardInfo();
            });
        }

        private getDashBoardInfo = () => {
            async.series({
                setReportsConfig: (callback: any) => {
                    this.setReportsConfig().then((results) => {
                        callback(null, results);
                    });
                },
                getSurveysTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.SurveysTrend });
                    if (obj && obj.visible) {
                        this.getSurveysTrend().then((results) => {
                        });
                    }
                    callback(null);
                },
                getAuditsTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.AuditTrend });
                    if (obj && obj.visible) {
                        this.getAuditsTrend().then((results) => {
                        });
                    }
                    callback(null);
                },
                getComplaintsTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ComplaintsTrend });
                    if (obj && obj.visible) {
                        this.getComplaintsTrend().then((results) => {
                        });
                    }
                    callback(null);
                },

            })
            async.auto({
                getTodosCounts: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.TodosCurrentMonth });
                    if (obj && obj.visible) {
                        this.getTodosCounts().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getWorkordersTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.WorkOrdersTrend });
                    if (obj && obj.visible) {
                        this.getWorkordersTrend().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getRecentCompliments: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ComplimentsMostRecent });
                    if (obj && obj.visible) {
                        this.getRecentCompliments().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getReportItsTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ReportItsTrend });
                    if (obj && obj.visible) {
                        this.getReportItsTrend().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getDashboardTrendsLastUpdatedDates: (callback: any) => {
                    this.getDashboardTrendsLastUpdatedDates().then((results) => {
                        callback(null, results);
                    });
                },
                getConductsCount: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ConductCurrentMonth });
                    if (obj && obj.visible) {
                        this.getConductsCount().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getAttendanceTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.AttendanceCurrentMonth });
                    if (obj && obj.visible) {
                        this.getAttendanceTrend().then((results) => {
                            callback(null, results);
                        });
                    }
                },
                getProfessionalismTrend: (callback: any) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.ProfessionalismCurrentMonth });
                    if (obj && obj.visible) {
                        this.getProfessionalismTrend().then((results) => {
                            callback(null, results);
                        });
                    }
                }
            })
        }

        private setReportsConfig = (): angular.IPromise<any> => {
            var def = this.$q.defer();
            async.waterfall([
                (callback) => {
                    if (this.userContext.Site.ID) {
                        // Get Sites Report Settings
                        this.reportHelperSvc.getReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    } else {
                        // Get All Sites Combined Report Settings
                        this.reportHelperSvc.getAllSitesCombinedSiteReportSettings()
                            .then((res) => (callback(null, res)))
                            .catch(err => callback(err));
                    }
                },
                (res, callback) => {
                    if (res) {
                        this.reportsSettings.surveys.controlLimit = res.SurveyControlLimit;
                        this.reportsSettings.complaints.controlLimit = res.ComplaintControlLimit;
                        this.reportsSettings.audits.goalLine = res.ScoringProfileGoal;
                        this.reportsSettings.audits.maxScore = res.ScoringProfileMaximumScore;
                    }
                    callback(null, res);
                }],
                (err, result) => {
                    if (err) {
                        this.onFailure(err);
                        def.reject(err);
                    }
                    def.resolve(result);
                });
            return def.promise;
        }

        private getDashboardTrendsLastUpdatedDates = (): angular.IPromise<any> => {
            return this.apiSvc.query(null, "Dashboard/QualityTrendsLastUpdatedDates", false, false).then((result) => {
                this.dashboardItem.audits.lastUpdatedDate = result.AuditsLastUpdated;
                this.dashboardItem.surveys.lastUpdatedDate = result.SurveysLastUpdated;
                this.dashboardItem.compliments.lastUpdatedDate = result.ComplimentsLastUpdated;
                this.dashboardItem.complaints.lastUpdatedDate = result.ComplaintsLastUpdated;
                this.dashboardItem.workOrder.lastUpdatedDate = result.WorkOrdersLastUpdated;
                this.dashboardItem.todos.lastUpdatedDate = result.TodosLastUpdated;
                this.dashboardItem.reportIts.lastUpdatedDate = result.ReportItsLastUpdated;
            }, this.onFailure);
        }

        private getSurveysTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().subtract(5, 'months').startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Surveys/SurveyTrends", false, false).then((result) => {
                    if (!result && result.Categories.length == 0) {
                        return;
                    }

                    var categories = result.Categories,
                        averages = result.Averages

                    var maxForFloor = <number>_.max(result.Categories) + 1;
                    if (maxForFloor < this.reportsSettings.surveys.controlLimit)
                        maxForFloor = this.reportsSettings.surveys.controlLimit + 1;

                    this.reportsConfig.surveysTrend = this.chartConfigBuilder('surveys', {
                        yAxis: {
                            minRange: maxForFloor,
                            min: 0,
                            max: 5,
                            title: '',
                            tickInterval: 1,
                            gridLineDashStyle: 'shortdash',
                            gridLineColor: this.sitesettings.colors.coreColors.border,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                            plotLines: [{
                                value: this.reportsSettings.surveys.controlLimit,
                                color: this.sitesettings.colors.coreColors.warning,
                                width: 2,
                                zIndex: 4,
                                label: { text: '' }
                            }],
                        },
                        xAxis: {
                            tickColor: this.sitesettings.colors.coreColors.default,
                            categories: categories,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        },
                        series: [{
                            name: 'Survey Score',
                            lineWidth: 100,
                            data: averages
                        }, {
                                name: 'Goal',
                                marker: {
                                    symbol: 'circle'
                                },

                                type: 'scatter',
                            }],
                        legend: {
                            enabled: false,
                            type: '',
                        },
                    });
            }, this.onFailure);

        }

        private getAuditsTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().subtract(5, 'months').startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Report/AuditTrends/:startDate/:endDate", false, false).then((result) => {
                    var categories = [];
                    _.forEach(result.Categories, (category) => {
                        var cat = category.slice(0, 3);
                        categories.push(cat);
                    });
                    this.dashboardItem.audits.categories = categories
                    this.dashboardItem.audits.internalAuditData = result.InternalAuditData;
                    this.dashboardItem.audits.jointAuditData = result.JointAuditData;

                    this.showAuditTrends();
                }, this.onFailure);
        }

        private showAuditTrends = () => {
            var goalLineWidth = this.reportsSettings.audits.goalLine != 0 ? this.goalLineWidth : 0;
            // Build the chart
            this.reportsConfig.auditsTrend = this.chartConfigBuilder('audits', {
                xAxis: {
                    tickColor: this.sitesettings.colors.coreColors.default,
                    categories: this.dashboardItem.audits.categories,
                    labels: {
                        enabled: true,
                        autoRotation: false
                    }
                },
                yAxis: {
                    minRange: this.reportsSettings.audits.goalLine,
                    max: this.reportsSettings.audits.maxScore,
                    min: 0,
                    labels: {
                        enabled: true
                    },
                    title: {
                        text: null
                    },
                    plotLines: [{
                        value: this.reportsSettings.audits.goalLine,
                        color: this.coreColors.warning,
                        width: goalLineWidth,
                        zIndex: 4,
                        label: { text: '' }
                    }],
                },
                plotOptions: {
                    column: {
                        showInLegend: true
                    }
                },
                series: [
                    {
                        name: 'Internal',
                        lineWidth: 3,
                        data: this.dashboardItem.audits.internalAuditData

                    },
                    {
                        name: 'Joint',
                        lineWidth: 3,
                        data: this.dashboardItem.audits.jointAuditData
                    },
                ],
                legend: {
                    enabled: true,
                    align: 'right',
                    verticalAlign: 'top',
                    layout: this.sitesettings.windowSizes.isTiny ? 'horizontal' : 'vertical',
                    symbolHeight: 12,
                    symbolWidth: 12,
                    symbolRadius: 3,
                    x: this.sitesettings.windowSizes.isTiny ? 0 : 10,
                    y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                    itemStyle: {
                        color: this.sitesettings.colors.coreColors.dark,
                        fontSize: '13px',
                        fontWeight: '600',
                    }
                },
            });
        }

        private getComplaintsTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().subtract(11, 'months').startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Report/ComplaintTrends/:startDate/:endDate", false, false).then((result) => {
                    var inScopeTotalCurrentMonth = (result.InScopeData.length) ? result.InScopeData[result.InScopeData.length - 1] : 0;
                    var outOfScopeTotalCurrentMonth = (result.OutOfScopeData.length) ? result.OutOfScopeData[result.OutOfScopeData.length - 1] : 0;

                    //get highest value and make sure enough room for Goal Line
                    var combinedData = _.union(result.InScopeData, result.OutOfScopeData);
                    var maxForFloor = <number>_.max(combinedData) + 2;
                    if (maxForFloor < this.reportsSettings.complaints.controlLimit)
                        maxForFloor = this.reportsSettings.complaints.controlLimit + 2;

                    this.reportsConfig.complaintsTrend = this.chartConfigBuilder('complaints', {
                        totalComplaints: <number>_.reduce(result.InScopeData.concat(result.OutOfScopeData), function (sum, n) {
                            return <number>sum + <number>n;
                        }),

                        categories: result.Categories,
                        yAxis: {
                            minRange: maxForFloor,
                            plotLines: [{
                                value: this.reportsSettings.complaints.controlLimit,
                                color: this.sitesettings.colors.coreColors.warning,
                                width: this.goalLineWidth,
                                zIndex: 4,
                                label: {
                                    text: '',
                                    style: {
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }
                            }],
                        },
                        xAxis: {
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        },
                        series: [
                            {
                                name: 'In Scope',
                                lineWidth: 3,
                                data: result.InScopeData
                            },
                            {
                                name: 'Out Of Scope',
                                lineWidth: 3,
                                data: result.OutOfScopeData,
                            }
                        ],

                        legend: {
                            enabled: true,
                            align: 'right',
                            layout: this.sitesettings.windowSizes.isTiny ? 'horizontal' : 'vertical',
                            verticalAlign: 'top',
                            x: this.sitesettings.windowSizes.isTiny ? 0 : 10,
                            y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                            itemStyle: {
                                color: this.sitesettings.colors.coreColors.dark,
                                fontSize: '13px',
                                fontWeight: '600',
                            }
                        },
                    });

                }, this.onFailure);
        }

        private getTodosCounts = (): angular.IPromise<any> => {
            if (this.isEmployee) {
                var employeeParams = {
                    startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                    endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day')),
                    employeeId: this.userInfo.userId
                };
                return this.apiSvc.query(employeeParams, 'TodoItem/EmployeeCounts', false, false).then((result) => {
                    this.dashboardItem.todos.closedOnTime.percent = (result.Closed + result.PastDue) == 0 ? 0 : parseFloat(((result.ClosedOnTime * 100) / (result.Closed + result.PastDue)).toFixed(2));
                    this.dashboardItem.todos.currentOpen.count = result.Open;
                    this.dashboardItem.todos.pastDue.count = result.PastDue;
                    this.dashboardItem.todos.closedOnTime.lastClosed = result.LastClosedDate;
                    this.dashboardItem.todos.currentOpen.lastOpenDate = result.LastOpenDate;
                    this.dashboardItem.todos.pastDue.oldestIncompleteDate = result.OldestIncompleteDate;

                }, this.onFailure);
            }
            else {
                var params = {
                    startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                    endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
                };
                return this.apiSvc.query(params, 'TodoItem/AllCounts', false, false).then((result) => {
                    this.dashboardItem.todos.closedOnTime.percent = (result.Closed + result.PastDue) == 0 ? 0 : parseFloat(((result.ClosedOnTime * 100) / (result.Closed + result.PastDue)).toFixed(2));
                    this.dashboardItem.todos.currentOpen.count = result.Open;
                    this.dashboardItem.todos.pastDue.count = result.PastDue;
                    this.dashboardItem.todos.closedOnTime.lastClosed = result.LastClosedDate;
                    this.dashboardItem.todos.currentOpen.lastOpenDate = result.LastOpenDate;
                    this.dashboardItem.todos.pastDue.oldestIncompleteDate = result.OldestIncompleteDate;
                }, this.onFailure);
            }
        }

        private getRecentCompliments = (): angular.IPromise<any> => {
            var params ={
                $top: 2,
                $orderby: 'CreateDate desc',
                $select: 'AccountableEmployees,Description,CreateDate,ComplimentId'
            };
            return this.apiSvc.getByOdata(params, "compliments").then((results) => {
                var employeeName = "";
                var employeeImg = "";
                angular.forEach(results, (obj) => {
                    if (obj.AccountableEmployees.length > 0) {
                        this.apiSvc.getById(obj.AccountableEmployees[0], "Employee").then((result) => {
                            employeeName = result.FirstName + " " + result.LastName;
                            employeeImg = this.imageSvc.getUserImageFromAttachments(result.UserAttachments, true);
                            this.dashboardItem.compliments.items.push({
                                employeeName: employeeName,
                                employeeImg: employeeImg,
                                createDate: obj.CreateDate,
                                description: obj.Description
                            });
                        }, this.onFailure);
                    }
                    else {
                        this.dashboardItem.compliments.items.push({
                            employeeName: employeeName,
                            employeeImg: employeeImg,
                            createDate: obj.CreateDate,
                            description: obj.Description
                        });
                    }
                });
            }, this.onFailure);
        }

        private getReportItsTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().subtract(5, 'months').startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "TrainingReport/Ownership/MonthlyTrends/:startDate/:endDate", false, false).then((result) => {
                if (!result) return;

                var categories = result.Month,
                    accepted = angular.copy(result.Accepted),
                    rejected = angular.copy(result.Rejected);

                // Build the chart
                this.reportsConfig.reportItsTrend = this.chartConfigBuilder('reportIts', {
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        categories: categories,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        tickInterval: result.Accepted.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [
                        {
                            name: 'Rejected',
                            lineWidth: 100,
                            data: rejected,
                            legendIndex: 1
                        },
                        {
                            name: 'Accepted',
                            lineWidth: 100,
                            data: accepted,
                            legendIndex: 0
                        }
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        symbolHeight: 12,
                        symbolWidth: 12,
                        symbolRadius: 3,
                        floating: true,
                        x: 0,
                        y: -10,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });
            }, this.onFailure);
        }

        private getWorkordersTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().subtract(5, 'months').startOf('month')),
                    endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Report/WorkOrderTrends/:startDate/:endDate", false, false).then((result) => {

                    if (!result) return;

                    var categories = result.Categories,
                        onTimeData = result.OnTimeData;

                    // Build the chart
                    this.reportsConfig.workOrdersTrend = this.chartConfigBuilder('workOrders', {
                        xAxis: {
                            tickColor: this.sitesettings.colors.coreColors.default,
                            categories: categories,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        },
                        yAxis: {
                            title: null,
                            tickInterval: onTimeData.length > 10 ? 10 : null,
                            labels: {
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        },
                        series: [{
                            name: 'Completion',
                            lineWidth: 100,
                            data: onTimeData
                        }]
                    });
                }, this.onFailure);
        }

        private getAttendanceTrend = (): angular.IPromise<any> => {
            this.getAttendanceLastModifiedDate();
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Issue/IssueStats", false, false).then((result) => {
                this.dashboardItem.attendance.totalCount = result.ArrivedLateCount + result.LeftEarlyCount + result.AbsentCount;

                var dataToPlot = [
                    { Name: 'Arrived Late', Count: result.ArrivedLateCount },
                    { Name: 'Left Early', Count: result.LeftEarlyCount },
                    { Name: 'Absent', Count: result.AbsentCount }];

                this.reportsConfig.attendanceTrend = this.chartConfigBuilder('attendance', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Attendance',
                            innerSize: '60%',
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Count'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });
            }, this.onFailure);
        }

        private getAttendanceLastModifiedDate = (): angular.IPromise<any> => {
            return this.apiSvc.query({}, "Issue/LastModifiedDate", false, false).then((result) => {
                this.dashboardItem.attendance.lastUpdatedDate = result.LastModifiedDate;
            }, this.onFailure);
        }

        private getConductsCount = (): angular.IPromise<any> => {
            this.getConductLastModifiedDate();
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "Conduct/ConductCount", false, false).then((result) => {
                this.dashboardItem.conduct.count = result.ConductCount;
            }, this.onFailure);
        }

        private getConductLastModifiedDate = (): angular.IPromise<any> => {
            return this.apiSvc.query({}, "Conduct/LastModifiedDate", false, false).then((result) => {
                this.dashboardItem.conduct.lastUpdatedDate = result.LastModifiedDate;
            }, this.onFailure);
        }

        private getProfessionalismTrend = (): angular.IPromise<any> => {
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(moment().startOf('month')),
                endDate: this.dateFormatterSvc.formatDateShort(moment().add(1, 'day'))
            }
            return this.apiSvc.query(params, "EmployeeAudit/EmployeeAuditStats", false, false).then((result) => {
                this.dashboardItem.professionalism.incompleteCount = result.IncompleteCount;
                this.dashboardItem.professionalism.totalCount = result.IncompleteCount + result.NotPassingCount + result.PassingCount;
                this.dashboardItem.professionalism.lastUpdatedDate = result.LastModifiedDate;

                var dataToPlot = [
                    { Name: 'Passing', Count: result.PassingCount },
                    { Name: 'Not Passing', Count: result.NotPassingCount },
                    { Name: 'Incomplete', Count: result.IncompleteCount }];


                var colors = [this.sitesettings.colors.coreColors.success, this.sitesettings.colors.coreColors.dangerDark, this.sitesettings.colors.coreColors.base];
                this.pieColors = colors.join();
                //Build the chart
                this.reportsConfig.professionalismTrend = this.chartConfigBuilder('professionalism', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Professionalism',
                            innerSize: '60%',
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Count'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });
            }, this.onFailure);
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private chartConfigBuilder = (name, options) => {
            var chartOptions = {

                // Column Chart
                audits: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: true,
                        align: 'right',
                        verticalAlign: 'top',
                        layout: 'vertical',
                        x: 10,
                        y: 20
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: true
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    }
                },

                // Column Chart
                surveys: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    }
                },

                // Column Chart
                workOrders: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    }
                },

                // Column Chart
                reportIts: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    }
                },

                // line chart
                complaints: {
                    chart: {
                        height: 140
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                    },
                    plotOptions: {
                        line: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            name: '',
                            data: []
                        }
                    ]
                },

                //pie chart
                attendance: {
                    chart: {
                        height: 200
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: false,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 100,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []
                        }
                    ]
                },

                ///pie chart
                professionalism: {
                    chart: {
                        height: 200
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: false,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 100,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []
                        }
                    ]
                }

            }
            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('Quality.DashboardIndexCtrl', DashboardIndexCtrl);
}
