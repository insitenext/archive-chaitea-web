﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.MyAudits.Controllers {

    "use strict";

    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import qualityInterfaces = Quality.Interfaces;

    interface IAuditScores {
        EmployeeId: number;
        AreaName: string;
        AverageScore: string;
        AverageScoringProfileScore: string;
        ProgramName: string;
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    class MyAuditsIndexCtrl {

        employees = [];
        employeesByArea: Array<IAuditScores> = [];
        employeeByArea = [];
        empId;

        dateRange: { date: Date; month: number; year: number } = {
            date: moment().toDate(),
            month: moment().month() + 1,
            year: moment().year()
        };

        static $inject = [
            '$scope',
            '$uibModal',
            "chaitea.quality.services.employeesvc",
            'chaitea.quality.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.core.services.messagebussvc',
            'userInfo',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private employeeSvc: qualitySvc.IEmployeeSvc,
            private reportSvc: qualitySvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private userInfo: IUserInfo,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc
        ) {

            this.empId = userInfo.userId;

            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.date = moment(obj.endDate).toDate();
                this.dateRange.month = moment(obj.endDate).month();
                this.dateRange.year = moment(obj.endDate).year();

                this.getAuditsByEmployeeByArea({
                    startDate : this.dateFormatterSvc.formatDateShort(new Date(this.dateRange.year, this.dateRange.month, 1)),
                    endDate: this.dateFormatterSvc.formatDateShort(new Date(this.dateRange.year, this.dateRange.month + 1, 0))
                });
            });
        }

        /*
         * Description To get average audits score of employees By Area
         */
        private getAuditsByEmployeeByArea = (params: qualityInterfaces.IMyAuditsParams) => {
           
            this.apiSvc.query(params, "Report/EmployeeAuditsAvgByAreaByEmployee/:startDate/:endDate").then((data) => {
                if (data.length == 1 && data[0].AreaName == null) {
                    this.employeesByArea = [];
                } else {
                    this.employeesByArea = data;
                }
            });
        }
    }

    angular.module("app").controller("Quality.MyAuditsIndexCtrl", MyAuditsIndexCtrl);

}