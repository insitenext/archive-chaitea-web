﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for audits.
 */
module ChaiTea.Quality.Services {
    'use strict';

    import commonInterfaces = Common.Interfaces;
 
    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Audit resource.
     */
    export interface IToDoResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IToDoSvc {
        getToDos(params?: commonInterfaces.IODataPagingParamModel): angular.IPromise<any>;
        getToDoById(id: number): angular.IPromise<any>;
        updateToDo(id: number, params?: Object): angular.IPromise<any>;
        saveToDo(params?: Object): angular.IPromise<any>;
        deleteToDo(id: number): angular.IPromise<any>;
    }

    class ToDoSvc implements angular.IServiceProvider {

        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];

        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IToDoSvc {
            return { 
                getToDos: (params) => { return this.getToDos(params); },
                getToDoById: (id) => { return this.getToDoById(id); },
                updateToDo: (id) => { return this.updateToDo(id); },
                saveToDo: (params) => { return this.saveToDo(params); },
                deleteToDo: (id) => { return this.deleteToDo(id); }
            }
        }  

        /**
         * @description Returns todo by item id
         */
        getToDoById = (id: number) => {
            return this.todoResource().get({ id: id }).$promise;
        }
        
        /**
         * @description Returns the todo list.
         */
        getToDos = (params?: IODataPagingParamModel) => {
            return this.todoResource().getByOData(params).$promise;
        }  
        
        /**
         * @description Creates/saves an audit profile.
         * @param {Object} params
         */
        saveToDo = (params?: Object): angular.IPromise<any> => {
            return this.todoResource().save(params || {}).$promise;
        }

        /**
         * @description Updates an audit profile.
         * @param {number} auditProfileId
         * @param {Object} params
         */
        updateToDo = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.todoResource().update(params).$promise;
        }  

        /**
        * @description Updates an audit profile.
        * @param {number} auditProfileId
        * @param {Object} params
        */
        deleteToDo = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.todoResource().delete(params).$promise;
        }  

        /**
         * @description Resource object for audit profile.
         */
        private todoResource = (isExtended?: boolean): IToDoResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var path: string = this.basePath + 'api/Services/TodoItem';

            //get resource
            return <IToDoResource>this.$resource(
                path + '/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: path,
                        params: { skip: 0 }
                    }
                });
        }
    }

    angular.module('app').service('chaitea.quality.services.todosvc', ToDoSvc);

}