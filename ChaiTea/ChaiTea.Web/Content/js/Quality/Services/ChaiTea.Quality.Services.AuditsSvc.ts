﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for audits.
 */
module ChaiTea.Quality.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Audit resource.
     */

    export interface IAuditResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IAuditFloorProfileResource extends IAuditResource {
        cloneProfile(Object): ng.resource.IResource<any>;
    }

    export interface IAuditsSvc {
        cloneAuditProfile(sourceId: number, targetId: number, responsibleUserId: number);
        getAuditProfile(id: number);
        getAllAuditProfiles(params?: IODataPagingParamModel, doIgnoreSite?: boolean): angular.IPromise<any>;
        saveAuditProfile(params?: Object): angular.IPromise<any>;
        updateAuditProfile(id: number, params?: Object): angular.IPromise<any>;
        deleteAuditProfile(id: number): angular.IPromise<any>;

        getAuditProfileSection(id: number): angular.IPromise<any>;
        getAuditProfileSections(params?: IODataPagingParamModel): angular.IPromise<any>;
        saveAuditProfileSection(params?: Object): angular.IPromise<any>;
        updateAuditProfileSection(id: number, params?: Object): angular.IPromise<any>;
        deleteAuditProfileSection(id: number): angular.IPromise<any>;
    }

    class AuditsSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IAuditsSvc {
            return {
                cloneAuditProfile: (sourceId, targetId, responsibleUserId) => { return this.cloneAuditProfile(sourceId, targetId, responsibleUserId); },
                getAuditProfile: (id: number) => { return this.getAuditProfile(id) },
                getAllAuditProfiles: (params, doIgnoreSite) => { return this.getAllAuditProfiles(params, doIgnoreSite); },
                saveAuditProfile: (params) => { return this.saveAuditProfile(params); },
                updateAuditProfile: (id, params) => { return this.updateAuditProfile(id, params); },
                deleteAuditProfile: (id) => { return this.deleteAuditProfile(id); },

                getAuditProfileSections: (params) => { return this.getAuditProfileSections(params); },
                getAuditProfileSection: (id) => { return this.getAuditProfileSection(id) },
                saveAuditProfileSection: (params) => { return this.saveAuditProfileSection(params); },
                updateAuditProfileSection: (id, params) => { return this.updateAuditProfileSection(id, params); },
                deleteAuditProfileSection: (id) => { return this.deleteAuditProfileSection(id); }
            }
        }

        /**
         * @description Returns an audit profile.
         * @param {number} auditProfileId
         */
        getAuditProfile = (id: number) => {
            return this.auditProfileResource().get({ id: id }).$promise;
        }

        /**
         * @description Returns an array of all audits profiles.
         * @param {IODataPagingParam} params
         */
        getAllAuditProfiles = (params?: IODataPagingParamModel, doIgnoreSite: boolean= false) => {
            return this.auditFloorProfileSectionResource().getByOData(angular.extend(params|| {}, { doIgnoreSite: doIgnoreSite })).$promise;
        }

        /**
         * @description Create a new profile or clone an existing profile.
         * @param {number} sourceId - auditProfileId where to copy from
         * @param {number} targetId - floorId destination to copy to
         */
        cloneAuditProfile = (sourceId: number, targetId: number, responsibleUserId: number) => {
            return this.auditFloorProfileSectionResource().cloneProfile({ sourceId: sourceId, targetId: targetId, responsibleUserId: responsibleUserId}).$promise;
        }

        /**
         * @description Creates/saves an audit profile.
         * @param {Object} params
         */
        saveAuditProfile = (params?: Object): angular.IPromise<any> => {
            return this.auditProfileResource().save(params || {}).$promise;
        }

        /**
         * @description Updates an audit profile.
         * @param {number} auditProfileId
         * @param {Object} params
         */
        updateAuditProfile = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.auditProfileResource().update(params).$promise;
        }

        /**
         * @description Delete an audit profile.
         * @param {number} auditProfileId
         */
        deleteAuditProfile = (id: number): angular.IPromise<any> => {
            return this.auditProfileResource().delete({ id: id }).$promise;
        }

        /**
         ** @description Returns a profile section object by auditProfileSectionId.
         * @param {number} auditProfileSectionId
         */
        getAuditProfileSection = (id: number): angular.IPromise<any> => {
            return this.auditProfileSectionResource().get({ id: id }).$promise;
        }

        /**
         * @description Returns an array of all audits profile sections.
         * @param {number} auditProfileId
         */
        getAuditProfileSections = (params?: IODataPagingParamModel) => {
            return this.auditProfileSectionResource().getByOData(params).$promise;
        }

        /**
         * @description Saves a profile section.
         * @param {Object} params
         */
        saveAuditProfileSection = (params?: Object): angular.IPromise<any> => {
            return this.auditProfileSectionResource().save(params || {}).$promise;
        }

        /**
         * @description Updates a profile section.
         * @param {number} auditProfileId
         * @param {Object} params
         */
        updateAuditProfileSection = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.auditProfileSectionResource().update(params).$promise;
        }

        /**
         * @description Delete an audit profile section.
         * @param {number} auditProfileSectionId
         */
        deleteAuditProfileSection = (id: number): angular.IPromise<any> => {
            return this.auditProfileSectionResource().delete({ id: id }).$promise;
        }

        /**
         * @description Resource object for audit profile.
         */
        private auditProfileResource = (): IAuditResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditResource>this.$resource(
                this.basePath + 'api/Services/AuditProfile/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/AuditProfile',
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for audit profile section.
         */
        private auditProfileSectionResource = (): IAuditResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditResource>this.$resource(
                this.basePath + 'api/Services/AuditProfileSection/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/AuditProfileSection',
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for audit floor profile section.
         */
        private auditFloorProfileSectionResource = (): IAuditFloorProfileResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditFloorProfileResource>this.$resource(
                this.basePath + 'api/Services/FloorProfile/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/FloorProfile/:doIgnoreSite',
                        params: { skip: 0 }
                    },
                    cloneProfile: {
                        method: 'GET',
                        url: this.basePath + 'api/Services/FloorProfile/Clone/:sourceId/:targetId/:responsibleUserId',
                    }
                });
        }
    }

    angular.module('app').service('chaitea.quality.services.auditssvc', AuditsSvc);
}


