﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for complaints.
 */
module ChaiTea.Quality.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Complaint resource.
     */
    // TODO: Define the complaints object properties.
    export interface IComplaintResource extends ng.resource.IResourceClass<any> {
        update(Object): ng.resource.IResource<any>;
        getByOData(Object): ng.resource.IResource<any>;
        ComplaintId: number;
        Description: string;
        ComplaintDate: Date;
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    export interface IComplaintsSvc {
        getComplaints(params?: IODataPagingParamModel);
        getComplaint(id: number): angular.IPromise<any>;
        updateComplaint(id: number, params?: Object): angular.IPromise<any>;
        saveComplaint(params?: Object): angular.IPromise<any>;
        deleteComplaint(id: number): angular.IPromise<any>;
    }

    class ComplaintsSvc implements angular.IServiceProvider {
        private basePath: string;
        private complaintsResource: IComplaintResource;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
            this.complaintsResource = this.resource();
        }

        public $get(): IComplaintsSvc {
            return {
                getComplaints: (params) => { return this.getComplaints(params); },
                getComplaint: (id) => { return this.getComplaint(id); },
                updateComplaint: (id, params) => { return this.updateComplaint(id, params); },
                saveComplaint: (params) => { return this.saveComplaint(params); },
                deleteComplaint: (id) => { return this.deleteComplaint(id); }
            }
        }

        /**
         * @description Returns a IComplaintResource type object.
         */
        private resource = (): IComplaintResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IComplaintResource>this.$resource(
                this.basePath + 'api/services/complaints/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: { // similar to query which the end point returns an array but for OData.
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/services/complaints',
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Returns an array of complaints.
         * @param {IODataPagingParam} params
         */
        getComplaints = (params?: IODataPagingParamModel) => {
            return this.complaintsResource.getByOData(params || {}).$promise;
        }

        /**
         * @description Returns a single complaint.
         * @param {Object} params
         */
        getComplaint = (id: number): angular.IPromise<any> => {
            return this.complaintsResource.get({ id: id }).$promise;
        }

        /**
         * @description Update a complaint.
         * @param {Object} params
         */
        updateComplaint = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id});
            return this.complaintsResource.update(params).$promise;
        }

        /**
         * @description Save a new complaint.
         * @param {Object} params
         */
        saveComplaint = (params?: Object): angular.IPromise<any> => {
            return this.complaintsResource.save(params || {}).$promise;
        }

        /**
         * @description Delete a complaint.
         * @param {number} id
         */
        deleteComplaint = (id: number): angular.IPromise<any> => {
            return this.complaintsResource.delete({ id: id }).$promise;
        }

    }

    angular.module('app').service('chaitea.quality.services.complaintssvc', ComplaintsSvc);
}


