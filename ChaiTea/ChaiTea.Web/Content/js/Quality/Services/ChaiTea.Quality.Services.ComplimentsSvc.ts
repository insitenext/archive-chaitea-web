﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for complaints.
 */
module ChaiTea.Quality.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Compliment resource.
     */
    export interface IComplimentResource extends ng.resource.IResourceClass<any> {
        update(Object): ng.resource.IResource<any>;
        getByOData(Object): ng.resource.IResource<any>;
    }

    export interface IComplimentsSvc {
        getCompliments(params?: IODataPagingParamModel);
        getCompliment(id: number): angular.IPromise<any>;
        updateCompliment(id: number, params?: Object): angular.IPromise<any>;
        saveCompliment(params?: Object): angular.IPromise<any>;
        deleteCompliment(id: number): angular.IPromise<any>;
    }

    class ComplimentsSvc implements angular.IServiceProvider {
        private basePath: string;
        private complimentsResource: IComplimentResource;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
            this.complimentsResource = this.resource();
        }

        public $get(): IComplimentsSvc {
            return {
                getCompliments: (params) => { return this.getCompliments(params); },
                getCompliment: (id) => { return this.getCompliment(id); },
                updateCompliment: (id, params) => { return this.updateCompliment(id, params); },
                saveCompliment: (params) => { return this.saveCompliment(params); },
                deleteCompliment: (id) => { return this.deleteCompliment(id); }
            }
        }

        /**
         * @description Returns a IComplaintResource type object.
         */
        private resource = (): IComplimentResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IComplimentResource>this.$resource(
                this.basePath + 'api/services/compliments/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: { // similar to query which the end point returns an array but for OData.
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/services/compliments',
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Returns an array of compliments.
         * @param {IODataPagingParam} params
         */
        getCompliments = (params?: IODataPagingParamModel) => {
            return this.complimentsResource.getByOData(params || {}).$promise;
        }

        /**
         * @description Returns a single compliment.
         * @param {Object} params
         */
        getCompliment = (id: number): angular.IPromise<any> => {
            return this.complimentsResource.get({ id: id }).$promise;
        }

        /**
         * @description Update a compliment.
         * @param {Object} params
         */
        updateCompliment = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.complimentsResource.update(params).$promise;
        }

        /**
         * @description Save a new compliment.
         * @param {Object} params
         */
        saveCompliment = (params?: Object): angular.IPromise<any> => {
            return this.complimentsResource.save(params || {}).$promise;
        }

        /**
         * @description Delete a compliment.
         * @param {number} id
         */
        deleteCompliment = (id: number): angular.IPromise<any> => {
            return this.complimentsResource.delete({ id: id }).$promise;
        }

    }

    angular.module('app').service('chaitea.quality.services.complimentssvc', ComplimentsSvc);
}