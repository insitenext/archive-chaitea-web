﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factory for work orders.
 */
module ChaiTea.Quality.Services {
    'use strict';

    export interface IWorkOrdersSvc {
        getWorkOrder(id: number);
        getWorkOrders(params?: any): angular.IPromise<any>;
        saveWorkOrder(params?: Object): angular.IPromise<any>;
        updateWorkOrder(id: number, params?: Object): angular.IPromise<any>;
        deleteWorkOrder(id: number, params?: Object): angular.IPromise<any>;

        getWorkOrderSeries(id: number);
        getWorkOrdersSeries(params?: IODataPagingParamModel): angular.IPromise<any>;
        saveWorkOrderSeries(params?: Object): angular.IPromise<any>;
        updateWorkOrderSeries(id: number, params?: Object): angular.IPromise<any>;
        deleteWorkOrderSeries(id: number): angular.IPromise<any>;
    }

    export interface IWorkOrdersResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    class WorkOrdersSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IWorkOrdersSvc {
            return {
                getWorkOrder: (id: number) => { return this.getWorkOrder(id) },
                getWorkOrders: (params) => { return this.getWorkOrders(params); },
                saveWorkOrder: (params) => { return this.saveWorkOrder(params); },
                updateWorkOrder: (id, params) => { return this.updateWorkOrder(id, params); },
                deleteWorkOrder: (id, params) => { return this.deleteWorkOrder(id); },

                getWorkOrderSeries: (id: number) => { return this.getWorkOrderSeries(id) },
                getWorkOrdersSeries: (params) => { return this.getWorkOrdersSeries(params); },
                saveWorkOrderSeries: (params) => { return this.saveWorkOrderSeries(params); },
                updateWorkOrderSeries: (id, params) => { return this.updateWorkOrderSeries(id, params); },
                deleteWorkOrderSeries: (id: number) => { return this.deleteWorkOrderSeries(id) }
            };
        }

        /**
         * @description Returns a work order.
         * @param {number} id
         */
        getWorkOrder = (id: number) => {
            return this.workOrdersResource().get({ id: id }).$promise;
        }

        /**
         * @description Returns an array of work orders.
         * @param {IODataPagingParam} params
         */
        getWorkOrders = (params?: any) => {
            return this.workOrdersResource().getByOData(params || {}).$promise;
        }

        /**
         * @description Creates/saves a work order.
         * @param {Object} params
         */
        saveWorkOrder = (params?: Object): angular.IPromise<any> => {
            return this.workOrdersResource().save(params || {}).$promise;
        }

        /**
         * @description Updates a work order.
         * @param {number} id
         * @param {Object} params
         */
        updateWorkOrder = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.workOrdersResource().update(params).$promise;
        }

        deleteWorkOrder = (id: number) => {
            return this.workOrdersResource().delete({ id: id }).$promise;
        }
        /**
         * @description Returns a work order series.
         * @param {number} id
         */
        getWorkOrderSeries = (id: number) => {
            return this.workOrdersSeriesResource().get({ id: id }).$promise;
        }

        /**
         * @description Returns an array of work orders series.
         * @param {IODataPagingParam} params
         */
        getWorkOrdersSeries = (params?: IODataPagingParamModel) => {
            return this.workOrdersSeriesResource().getByOData(params || {}).$promise;
        }

        /**
         * @description Creates/saves a work order series.
         * @param {Object} params
         */
        saveWorkOrderSeries = (params?: Object): angular.IPromise<any> => {
            return this.workOrdersSeriesResource().save(params || {}).$promise;
        }

        /**
         * @description Updates a work order series.
         * @param {number} id
         * @param {Object} params
         */
        updateWorkOrderSeries = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.workOrdersSeriesResource().update(params).$promise;
        }

        deleteWorkOrderSeries = (id: number) => {
            return this.workOrdersSeriesResource().delete({ id: id }).$promise;
        }
        /**
         * @description Resource object for work orders.
         */
        private workOrdersResource = (): IWorkOrdersResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IWorkOrdersResource>this.$resource(
                this.basePath + 'api/Services/WorkOrders/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        // url: this.basePath + 'api/Services/WorkOrders/bydate/:startDate/:endDate'
                        url: this.basePath + 'api/Services/WorkOrders'
                    }
                });
        }

        /**
         * @description Resource object for work orders series.
         */
        private workOrdersSeriesResource = (): IWorkOrdersResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IWorkOrdersResource>this.$resource(
                this.basePath + 'api/Services/WorkOrderSeries/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/WorkOrderSeries'
                    }
                });
        }
    }

    angular.module('app').service('chaitea.quality.services.workorderssvc', WorkOrdersSvc);
}