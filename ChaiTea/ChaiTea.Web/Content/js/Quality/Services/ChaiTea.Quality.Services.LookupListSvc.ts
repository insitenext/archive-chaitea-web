﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Quality.Services {
    'use strict'

    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    export interface ILookupListSvc {
        getComplaintTypesForProgram(params?: Object): angular.IPromise<any>;
        getComplaintTypesForComplaint(params?: Object): angular.IPromise<any>;
        getComplaintTypes(params?: Object): angular.IPromise<any>;
        getComplimentTypes(params?: Object): angular.IPromise<any>;
        getPreventableStatuses(params?: Object): angular.IPromise<any>;
        getClassifications(params?: Object): angular.IPromise<any>;
        getEmployeesForComplaint(params?: Object): angular.IPromise<any>;
        getEmployeesForCompliment(params?: Object): angular.IPromise<any>;
        getBuildingsForComplaint(params?: Object): angular.IPromise<any>;
        getBuildingsForCompliment(params?: Object): angular.IPromise<any>;
        getFloors(params?: Object): angular.IPromise<any>;
        getFloorByFloorId(floorId: number): angular.IPromise<any>;
        getAreas(params?: Object): angular.IPromise<any>;
        getPrograms(params?: Object): angular.IPromise<any>;
        getAreaClassificationsForProfile(auditProfileId: number, auditProfileSectionId: number, areaId: number): angular.IPromise<any>;
        getEmployees(): angular.IPromise<any>;
        getBuildings(): angular.IPromise<any>;
        getBuildingsForSite(id: number): angular.IPromise<any>;
        getAreaClassificationInspectionItems(id: number): angular.IPromise<any>;
        getScoringProfiles(): angular.IPromise<any>;
        getWorkOrderServiceTypes(ProgramId: number): angular.IPromise<any>;
        getWorkOrderRecurrenceTypes(): angular.IPromise<any>;
        getWorkOrderPriorities(): angular.IPromise<any>;
    }

    class LookupListSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$q', '$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {any} sitesettings
         */
        constructor(
            private $q: angular.IQService,
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ILookupListSvc {
            return {
                getComplaintTypesForProgram: (params) => { return this.getComplaintTypesForProgram(params); },
                getComplaintTypesForComplaint: (params) => { return this.getComplaintTypesForComplaint(params); },
                getComplaintTypes: (params) => { return this.getComplaintTypes(params); },
                getComplimentTypes: (params) => { return this.getComplimentTypes(params); },
                getPreventableStatuses: (params) => { return this.getPreventableStatuses(params); },
                getClassifications: (params) => { return this.getClassifications(params); },
                getEmployeesForComplaint: (params) => { return this.getEmployeesForComplaint(params); },
                getEmployeesForCompliment: (params) => { return this.getEmployeesForCompliment(params); },
                getBuildingsForComplaint: (params) => { return this.getBuildingsForComplaint(params); },
                getBuildingsForCompliment: (params) => { return this.getBuildingsForCompliment(params); },
                getFloors: (params) => { return this.getFloors(params); },
                getFloorByFloorId: (floorId) => { return this.getFloorByFloorId(floorId); },
                getAreas: (params) => { return this.getAreas(params); },
                getPrograms: (params) => { return this.getPrograms(params); },
                getAreaClassificationsForProfile: (auditProfileId, auditProfileSectionId, areaId) => { return this.getAreaClassificationsForProfile(auditProfileId, auditProfileSectionId, areaId); },
                getEmployees: () => { return this.getEmployees(); },
                getBuildings: () => { return this.getBuildings(); },
                getBuildingsForSite: (id) => { return this.getBuildingsForSite(id); },
                getAreaClassificationInspectionItems: (id) => { return this.getAreaClassificationInspectionItems(id); },
                getScoringProfiles: () => { return this.getScoringProfiles(); },
                getWorkOrderServiceTypes: (ProgramId) => { return this.getWorkOrderServiceTypes(ProgramId); },
                getWorkOrderRecurrenceTypes: () => { return this.getWorkOrderRecurrenceTypes(); },
                getWorkOrderPriorities: () => { return this.getWorkOrderPriorities(); }
            }
        }

        /**
         * Returns complaint types.
         * @param {Object} params
         */
        getComplaintTypes = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplaintTypes').get(params).$promise;
        }

        /**
         * Returns complaint types.
         * @param {Object} params
         */
        getComplaintTypesForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getComplaintTypes();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplaintTypes/Complaint/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
        * Returns complaint types.
        * @param {Object} params
        */
        getComplaintTypesForProgram = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplaintTypes/Program/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns compliment types.
         * @param {Object} params
         */
        getComplimentTypes = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplimentTypes').get(params).$promise;
        }

        /**
         * Returns preventable statuses.
         * @param {Object} params
         */
        getPreventableStatuses = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/PreventableStatuses').get(params).$promise;
        }

        /**
         * Returns classifications.
         * @param {Object} params
         */
        getClassifications = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Classifications').get(params).$promise;
        }

        /**
         * Returns employees.
         * @param {Object} params
         */
        getEmployeesForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getEmployees();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Employees/Complaint/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns employees.
         * @param {Object} params
         */
        getEmployeesForCompliment = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getEmployees();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Employees/Compliment/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        getBuildings = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/').get().$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        getBuildingsForSite = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Site/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        getBuildingsForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Complaint/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        getBuildingsForCompliment = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Compliment/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns floors.
         * @param {Object} params
         */
        getFloors = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Floors/Building/:id',
                { id: '@id' }).get(params).$promise;
        }

        getFloorByFloorId = (floorId: number): angular.IPromise<any>=> {
            return this.$resource(this.basePath + 'api/Services/Floor/:id', { id: '@id' }).get({ id: floorId }).$promise;
        }

        /**
         * Returns areas.
         * @param {Object} params
         */
        getAreas = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Areas/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns programs.
         * @param {Object} params
         */
        getPrograms = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Programs/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns area classifications for profile.
         */
        getAreaClassificationsForProfile = (auditProfileId: number, auditProfileSectionId: number, areaId: number): angular.IPromise<any> => {
            var params = { auditProfileId: auditProfileId, areaId: areaId };
            if (auditProfileSectionId > 0) {
                _.assign(params, { auditProfileSectionId: auditProfileSectionId });
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/AreaClassifications/:auditProfileId/:areaId/:auditProfileSectionId')
                .get(params).$promise;
        }

        /**
         * Returns employees for site.
         */
        getEmployees = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Employees').get().$promise;
        }

        /**
         * Returns buildings for site.
         * @param {number} id
         */
        getAreaClassificationInspectionItems = (id: number): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/AreaClassificationInspectionItems/:id').get({ id: id }).$promise;
        }

        /**
         * Returns scoring profiles.
         */
        getScoringProfiles = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ScoringProfiles').get().$promise;
        }

        /**
         * @description Returns a list of service types.
         */
        getWorkOrderServiceTypes = (ProgramId: number): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/WorkOrderServiceTypes/:ProgramId').get({ ProgramId: ProgramId }).$promise;
        }

        /**
         * @description Returns a list of recurrence pattern types.
         */
        getWorkOrderRecurrenceTypes = (): angular.IPromise<any> => {
            var defer = this.$q.defer();
            defer.resolve(<Array<qualityInterfaces.ILookupListOption>>[
                { Key: 1, Value: 'Daily' },
                { Key: 2, Value: 'Weekly' },
                { Key: 3, Value: '2x Month' },
                { Key: 4, Value: 'Monthly' },
                { Key: 5, Value: 'Quarterly' },
                { Key: 6, Value: '6 months' },
                { Key: 7, Value: 'Yearly' }]);

            return defer.promise;
        }

        /**
         * @description Returns a list of work order priorities.
         */
        getWorkOrderPriorities = (): angular.IPromise<any> => {
            var defer = this.$q.defer();
            defer.resolve(<Array<qualityInterfaces.ILookupListOption>>[
                { Key: 1, Value: 'Normal' },
                { Key: 2, Value: 'Critical' }]);

            return defer.promise;
        }
    }

    angular.module('app').service('chaitea.quality.services.lookuplistsvc', LookupListSvc);
}