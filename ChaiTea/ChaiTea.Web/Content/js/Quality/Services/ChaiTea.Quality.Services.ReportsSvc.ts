﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Quality.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    export interface IReportsSvc {
        getComplaintTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getComplaintTrendsByTypeOrProgram(params?: commonInterfaces.ITrendDateRange): angular.IPromise<any>;
        getComplaintsByType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getComplaintTrendsBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getComplimentTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getComplimentTrendsByTypeOrProgram(params?: commonInterfaces.ITrendDateRange): angular.IPromise<any>;
        getComplimentsByType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getComplimentTrendsBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;

        getAuditTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getAuditTrendsBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getAuditTrendsByProgram(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getAuditTrendsByAreaClassification(param?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getAuditGridByProgram(id, param?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getEmployeeAuditsAvg(param?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getEmployeeAuditsAvgByArea(param?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getEmployeeAuditsAvgByAreaByEmployee(param?: qualityInterfaces.IMyAuditsParams): angular.IPromise<any>;


        getWorkOrderTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getWorkOrdersBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getWorkOrdersByProgramOrType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getWorkOrderTrendsByProgramOrType(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getWorkOrdersByEmployee(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
    }

    class ReportsSvc implements angular.IServiceProvider  {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {obj} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings) {

            this.basePath = sitesettings.basePath;
        }

        public $get(): IReportsSvc {
            return {
                getComplaintTrends: (params) => {
                    return this.getComplaintTrends(params);
                },
                getComplaintTrendsByTypeOrProgram: (params) => {
                    return this.getComplaintTrendsByTypeOrProgram(params);
                },
                getComplaintsByType: (params) => {
                    return this.getComplaintsByType(params);
                },
                getComplaintTrendsBySite: (params) => {
                    return this.getComplaintTrendsBySite(params);
                },
                getComplimentTrends: (params) => {
                    return this.getComplimentTrends(params);
                },
                getComplimentTrendsByTypeOrProgram: (params) => {
                    return this.getComplimentTrendsByTypeOrProgram(params);
                },
                getComplimentsByType: (params) => {
                    return this.getComplimentsByType(params);
                },
                getComplimentTrendsBySite: (params) => {
                    return this.getComplimentTrendsBySite(params);
                },
                getAuditTrends: (params) => {
                    return this.getAuditTrends(params);
                },
                getAuditTrendsBySite: (params) => {
                    return this.getAuditTrendsBySite(params);
                },
                getAuditTrendsByProgram: (params) => {
                    return this.getAuditTrendsByProgram(params);
                },
                getAuditTrendsByAreaClassification: (params) => {
                    return this.getAuditTrendsByAreaClassification(params);
                },
                getAuditGridByProgram: (id, params) => {
                    return this.getAuditGridByProgram(id, params);
                },
                getEmployeeAuditsAvg: (params) => {
                    return this.getEmployeeAuditsAvg(params);
                },
                getEmployeeAuditsAvgByArea: (params) => {
                    return this.getEmployeeAuditsAvgByArea(params);
                },
                getEmployeeAuditsAvgByAreaByEmployee: (params) => {
                    return this.getEmployeeAuditsAvgByAreaByEmployee(params);
                },
                getWorkOrderTrends: (params) => {
                    return this.getWorkOrderTrends(params);
                },
                getWorkOrdersBySite: (params) => {
                    return this.getWorkOrdersBySite(params);
                },
                getWorkOrdersByProgramOrType: (params) => {
                    return this.getWorkOrdersByProgramOrType(params);
                },
                getWorkOrderTrendsByProgramOrType: (params) => {
                    return this.getWorkOrderTrendsByProgramOrType(params);
                },
                getWorkOrdersByEmployee: (params) => {
                    return this.getWorkOrdersByEmployee(params);
                }
            }
        }

        /**
         * @description Complaint Reports.
         */
        getComplaintTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplaintTrends/:startDate/:endDate').get(params).$promise;
        }

        getComplaintTrendsByTypeOrProgram = (params?: commonInterfaces.ITrendDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplaintTrendsByTypeOrProgram/:trendId/:startDate/:endDate').get(params).$promise;
        }

        getComplaintsByType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplaintsByType/:startDate/:endDate').get(params).$promise;
        }

        getComplaintTrendsBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplaintTrendsBySite/:startDate/:endDate').get(params).$promise;
        }

        /**
         * @description Compliment Reports.
         */
        getComplimentTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplimentTrends/:startDate/:endDate').get(params).$promise;
        }

        getComplimentTrendsByTypeOrProgram = (params?: commonInterfaces.ITrendDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplimentTrendsByTypeOrProgram/:trendId/:startDate/:endDate').get(params).$promise;
        }

        getComplimentsByType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplimentsByType/:startDate/:endDate').get(params).$promise;
        }

        getComplimentTrendsBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/ComplimentTrendsBySite/:startDate/:endDate').get(params).$promise;
        }
        
        /**
         * @description Audit Reports.
         */
        getAuditTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/AuditTrends/:startDate/:endDate').get(params).$promise;
        }

        getAuditTrendsBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/AuditTrendsBySite/:startDate/:endDate').get(params).$promise;
        }

        getAuditTrendsByProgram = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/AuditTrendsByProgram/:startDate/:endDate').get(params).$promise;
        }

        getAuditTrendsByAreaClassification = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/AuditTrendsByAreaClassification/:startDate/:endDate').get(params).$promise;
        }

        getAuditGridByProgram = (id: number, params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/AuditGridByProgram/:startDate/:endDate/:id').get({ id: id, startDate: params.startDate, endDate: params.endDate }).$promise;
        }

        getEmployeeAuditsAvg = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/EmployeeAuditsAvg/:startDate/:endDate').query(params).$promise;
        }

        getEmployeeAuditsAvgByArea = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/EmployeeAuditsAvgByArea/:startDate/:endDate').query(params).$promise;
        }

        getEmployeeAuditsAvgByAreaByEmployee = (params?: qualityInterfaces.IMyAuditsParams): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/EmployeeAuditsAvgByAreaByEmployee/:startDate/:endDate/:employeeId').query(params).$promise;
        }

        /**
         * @description Work Order Reports.
         */
        getWorkOrderTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/WorkOrderTrends/:startDate/:endDate').get(params).$promise;
        }

        getWorkOrdersBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/WorkOrdersBySite/:startDate/:endDate').get(params).$promise;
        }

        getWorkOrdersByProgramOrType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/WorkOrdersByProgramOrType/:startDate/:endDate').get(params).$promise;
        }

        getWorkOrderTrendsByProgramOrType = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/WorkOrderTrendsByProgramOrType/:startDate/:endDate/:id').get(params).$promise;
        }

        getWorkOrdersByEmployee = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/Report/WorkOrdersByEmployee/:startDate/:endDate').get(params).$promise;
        }
    }

    angular.module('app').service('chaitea.quality.services.reportssvc', ReportsSvc);
}

