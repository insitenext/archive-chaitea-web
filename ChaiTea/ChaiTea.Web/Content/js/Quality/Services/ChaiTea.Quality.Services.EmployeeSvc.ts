﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Quality.Services {
    "use strict";

    export interface IEmployeeSvc {
        getEmployeeByIds(employeeIds: number[]): angular.IPromise<any>;
    }

    export interface IEmployeeResource extends angular.resource.IResourceClass<any> {
        employees(employeeIds: number[]): angular.resource.IResource<any>;
    }

    class EmployeeSvc implements IEmployeeSvc {
        private basePath: string;

        static $inject = ["$resource", "sitesettings"];

        constructor(
            private $resource: angular.resource.IResourceService,
            sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        getEmployeeByIds = (employeeIds: number[]): angular.IPromise<any> => {
            return this.employeeResource().employees(employeeIds).$promise;
        }

        private employeeResource = (): IEmployeeResource => {
            return <IEmployeeResource>this.$resource(this.basePath + "api/Services/Employee/Employees", {}, { employees: { method: "POST", isArray: true } });
        }
    }

    angular.module("app").service("chaitea.quality.services.employeesvc", EmployeeSvc);
} 