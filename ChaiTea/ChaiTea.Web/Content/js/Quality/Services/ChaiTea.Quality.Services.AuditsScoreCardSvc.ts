﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for audits score card.
 */
module ChaiTea.Quality.Services {
    'use strict';

    export interface IAuditsScoreCardSvc {
        getAuditScores(params?: Object): angular.IPromise<any>;
        getAuditScore(id): angular.IPromise<any>;
        deleteAuditScore(id: number): angular.IPromise<any>;
        saveAuditScore(params?: Object): angular.IPromise<any>;
        updateAuditScore(id: number, params?: Object): angular.IPromise<any>;

        getAuditScoreDetails(params?: IODataPagingParamModel): angular.IPromise<any>;
        updateAuditScoreDetail(id: number, params?: Object): angular.IPromise<any>;
        updateAuditScoreDetails(params?: Object): angular.IPromise<any>;

        saveAuditScoreDetailsImage(params?: Object): angular.IPromise<any>;
        updateAuditScoreDetailsImage(id: number, params?: Object): angular.IPromise<any>;
        deleteAuditScoreDetailsImage(id: number): angular.IPromise<any>;

        getAuditScoreDetailsTodos(params?: IODataPagingParamModel);
        saveAuditScoreDetailsTodo(params?: Object): angular.IPromise<any>;
        updateAuditScoreDetailsTodo(id: number, params?: Object): angular.IPromise<any>;
        deleteAuditScoreDetailsTodo(id: number): angular.IPromise<any>;

        getCustomerRepresentatives(): angular.IPromise<any>;
        saveCustomerRepresentative(params?: Object): angular.IPromise<any>;
    }

    export interface IAuditsScoreCardRecource extends ng.resource.IResourceClass<any> {
        update(Object): ng.resource.IResource<any>;
        getByOData(Object): ng.resource.IResource<any>;
    }

    export interface IAuditsScoreCardDetailRecource extends ng.resource.IResourceClass<any> {
        update(Object): ng.resource.IResource<any>;
        updateBulkAction(Object): ng.resource.IResource<any>;
        getByOData(Object): ng.resource.IResource<any>;
    }

    class AuditsScoreCardSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IAuditsScoreCardSvc {
            return {
                getAuditScores: (params?: IODataPagingParamModel) => { return this.getAuditScores(params); },
                getAuditScore: (id) => { return this.getAuditScore(id); },
                saveAuditScore: (params?: Object) => { return this.saveAuditScore(params); },
                updateAuditScore: (id: number, params?: Object) => { return this.updateAuditScore(id, params); },
                deleteAuditScore: (id: number) => { return this.deleteAuditScore(id); },

                getAuditScoreDetails: (params?: IODataPagingParamModel) => { return this.getAuditScoreDetails(params); },
                updateAuditScoreDetail: (id: number, params?: Object) => { return this.updateAuditScoreDetail(id, params); },
                updateAuditScoreDetails: (params?: Object) => { return this.updateAuditScoreDetails(params); },

                saveAuditScoreDetailsImage: (params?: Object) => { return this.saveAuditScoreDetailsImage(params); },
                updateAuditScoreDetailsImage: (id: number, params?: Object) => { return this.updateAuditScoreDetailsImage(id, params); },
                deleteAuditScoreDetailsImage: (id: number) => { return this.deleteAuditScoreDetailsImage(id); },

                getAuditScoreDetailsTodos: (params?: IODataPagingParamModel) => { return this.getAuditScoreDetailsTodos(params); },
                saveAuditScoreDetailsTodo: (params?: Object) => { return this.saveAuditScoreDetailsTodo(params); },
                updateAuditScoreDetailsTodo: (id: number, params?: Object) => { return this.updateAuditScoreDetailsTodo(id, params); },
                deleteAuditScoreDetailsTodo: (id: number) => { return this.deleteAuditScoreDetailsTodo(id); },

                getCustomerRepresentatives: () => { return this.getCustomerRepresentatives(); },
                saveCustomerRepresentative: (params?: Object) => { return this.saveCustomerRepresentative(params); },
            }
        }

        /**
         * @description Audit score resources.
         */
        getAuditScores = (params?: IODataPagingParamModel) => {
            return this.auditsScoreCardResource().getByOData(params || {}).$promise;
        }

        getAuditScore = (id: number) => {
            return this.auditsScoreCardResource().get({ id: id }).$promise;
        }

        saveAuditScore = (params) => {
            return this.auditsScoreCardResource().save(angular.extend(params || {}, { InProgress: true })).$promise;
        }

        updateAuditScore = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.auditsScoreCardResource().update(params).$promise;
        }

        deleteAuditScore = (id: number): angular.IPromise<any> => {
            return this.auditsScoreCardResource().delete({ id: id }).$promise;
        }

        /**
         * @description Audit score details resources.
         */
        getAuditScoreDetails = (params?: IODataPagingParamModel) => {
            return this.auditsScoreCardDetailResource().getByOData(params || {}).$promise;
        }

        updateAuditScoreDetail = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.auditsScoreCardDetailResource().update(params).$promise;
        }

        updateAuditScoreDetails = (params?: Object): angular.IPromise<any>=> {
            return this.auditsScoreCardDetailResource().updateBulkAction(params).$promise;
        }

        /**
         * @description Audit score details image resources.
         */
        saveAuditScoreDetailsImage = (params?: Object): angular.IPromise<any> => {
            return this.auditsScoreCardDetailImageResource().save(params || {}).$promise;
        }

        updateAuditScoreDetailsImage = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.auditsScoreCardDetailImageResource().update(params).$promise;
        }

        deleteAuditScoreDetailsImage = (id: number): angular.IPromise<any> => {
            return this.auditsScoreCardDetailImageResource().delete({ id: id }).$promise;
        }

        /**
         * @description Audit score details todo resources.
         */
        getAuditScoreDetailsTodos = (params?: IODataPagingParamModel) => {
            return this.auditsScoreCardDetailTodoResource().getByOData(params || {}).$promise;
        }

        saveAuditScoreDetailsTodo = (params?: Object): angular.IPromise<any> => {
            return this.auditsScoreCardDetailTodoResource().save(params || {}).$promise;
        }

        updateAuditScoreDetailsTodo = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.auditsScoreCardDetailTodoResource().update(params).$promise;
        }

        deleteAuditScoreDetailsTodo = (id: number): angular.IPromise<any> => {
            return this.auditsScoreCardDetailTodoResource().delete({ id: id }).$promise;
        }

        /**
         * @description Customer representatives resources.
         */
        getCustomerRepresentatives = () => {
            return this.customerRepresentativeResource().query().$promise;
        }

        saveCustomerRepresentative = (params) => {
            return this.customerRepresentativeResource().save(angular.extend(params || {}, { InProgress: true })).$promise;
        }

        /**
         * @description Resource object for audits score card.
         */
        private auditsScoreCardResource = (): IAuditsScoreCardRecource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditsScoreCardRecource>this.$resource(
                this.basePath + 'api/Services/ScoreCard/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/ScoreCard'
                    }
                });
        }

        /**
         * @description Resource object for audits score card details.
         */
        private auditsScoreCardDetailResource = (): IAuditsScoreCardDetailRecource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };
            var updateBulkAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: true
            };

            return <IAuditsScoreCardDetailRecource>this.$resource(
                this.basePath + 'api/Services/ScoreCardDetail/:id', { id: '@id' },
                {
                    update: updateAction,
                    updateBulkAction: updateBulkAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/ScoreCardDetail',
                        params: { skip: 0 }
                    }
                });
        }

        /**
         * @description Resource object for audits score card details image.
         */
        private auditsScoreCardDetailImageResource = (): IAuditsScoreCardRecource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditsScoreCardRecource>this.$resource(
                this.basePath + 'api/Services/ScoreCardDetailImage/:id', { id: '@id' },
                {
                    update: updateAction
                });
        }

        /**
         * @description Resource object for audits score card details todo.
         */
        private auditsScoreCardDetailTodoResource = (): IAuditsScoreCardRecource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditsScoreCardRecource>this.$resource(
                this.basePath + 'api/Services/TodoItem/:id', { id: '@id' },
                {
                    update: updateAction
                });
        }

        /**
         * @description Resource object for customer representative.
         */
        private customerRepresentativeResource = (): IAuditsScoreCardRecource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditsScoreCardRecource>this.$resource(
                this.basePath + 'api/Services/CustomerRepresentative/:id', { id: '@id' },
                {
                    update: updateAction
                });
        }
    }

    angular.module('app').service('chaitea.quality.services.auditsscorecardsvc', AuditsScoreCardSvc);
}