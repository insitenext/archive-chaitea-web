﻿module ChaiTea.Quality.Audit.Filters {
    /**
     * @description Filter score cards.
     */
    export function AuditFilterScorecards() {
        return function (items, filterBy, goal) {
            var filtered = [];

            //Scored is default
            if (!filterBy)
                filterBy = 'scored';

            switch (filterBy) {
                case 'todoItems':              
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].TodoItems.length) {
                            filtered.push(items[i]);
                        }
                    }
                    break;
                case 'failed':
                    for (var i = 0; i < items.length; i++) {
                        if ((items[i].Score || 0) < goal) {
                            filtered.push(items[i]);
                        }
                    }
                    break;
                case 'inspected':
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].Score || items[i].Comment) {
                            filtered.push(items[i]);
                        }
                    }
                    break;
                case 'scored':
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].Score) {
                            filtered.push(items[i]);
                        }
                    }
                    break;
                case 'unscored':
                    for (var i = 0; i < items.length; i++) {
                        if (!items[i].Score) {
                            filtered.push(items[i]);
                        }
                    }
                    break;
                default:
                    filtered = items;
            }

            return filtered;
        };
    }
    angular.module('app').filter('auditFilterScorecards', AuditFilterScorecards);
} 