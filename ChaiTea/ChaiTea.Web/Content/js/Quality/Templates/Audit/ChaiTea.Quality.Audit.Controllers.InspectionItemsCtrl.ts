﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Quality.Audit.Controllers {
    'use strict';

    import qualitySvc = ChaiTea.Quality.Services;
    import qualityInterfaces = ChaiTea.Quality.Interfaces;

    class InspectionItemsCtrl {

        private basePath: string = "";
        private areas: Array<qualityInterfaces.IKeyValuePair> = [];
        private areaClassifications: Array<qualityInterfaces.IKeyValuePair> = [];
        private inspectionItems: Array<qualityInterfaces.IKeyValuePair> = [];

        static $inject = ["$q", "sitesettings", "$modalInstance", "chaitea.quality.services.lookuplistsvc", "chaitea.quality.services.auditssvc", "auditProfileSection"];

        constructor(
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private lookupListSvc: qualitySvc.ILookupListSvc,
            private auditsSvc : qualitySvc.IAuditsSvc,
            private auditProfileSection: qualityInterfaces.IAuditProfileSection) {
            this.basePath = sitesettings.basePath;
            this.getAreas(auditProfileSection.Profile.FloorId);
            this.getAreaClassificationsByArea(auditProfileSection.Profile.AuditProfileId, auditProfileSection.AuditProfileSectionId, auditProfileSection.AreaId);
            this.getInspectionItemsByAreaClassification(auditProfileSection.AreaClassificationId);
        }

        getAreas = (floorId: number) => {
            return new this.$q((resolve, reject) => {
                if (this.areas.length) {
                    resolve(this.areas);
                    return;
                }

                return this.lookupListSvc.getAreas({ id: floorId }).then((result) => {
                    this.areas = result.Options;
                    resolve(this.areas);
                });
            });
        };

        getAreaClassificationsByArea = (auditProfileId: number, auditProfileSectionId: number, areadId: number) => {
            this.areaClassifications = [];
            return new this.$q((resolve, reject) => {
                if (auditProfileId > 0 && areadId > 0) {
                    return this.lookupListSvc.getAreaClassificationsForProfile(auditProfileId, auditProfileSectionId, areadId).then((data) => {
                        this.areaClassifications = data.Options;
                        resolve(this.areaClassifications);
                    });
                }
            });
        };

        getInspectionItemsByAreaClassification = (areaClassificationId: number) => {
            this.inspectionItems = [];
            return new this.$q((resolve, reject) => {
                if (areaClassificationId > 0) {
                    return this.lookupListSvc.getAreaClassificationInspectionItems(areaClassificationId).then((result) => {
                        this.inspectionItems = result.Options;
                        resolve(this.inspectionItems);
                    });
                }
            });
        };

        onAreaChange = ($event): void => {
            this.auditProfileSection.AreaClassificationId = null;
            this.inspectionItems = [];
            this.getAreaClassificationsByArea;
        };

        onAreaClassificationChange = (areaClassificationId: number): void=> {
            this.getInspectionItemsByAreaClassification(areaClassificationId);
        };

        /**
         * @description Add or remove an inspection item from the list.
         */
        toggleInspectionItem = (id) => {
            if (this.auditProfileSection.InspectionItems.indexOf(id) > -1) {
                for (var i = 0; i < this.auditProfileSection.InspectionItems.length; i++) {
                    if (this.auditProfileSection.InspectionItems[i] === id) {
                        this.auditProfileSection.InspectionItems.splice(i, 1);
                        break;
                    }
                }
            }
            else {
                this.auditProfileSection.InspectionItems.push(id);
            }
        };

        /**
         * @description Toggle all selection for inspection items.
         */
        toggleAllInspectionItems = ($event) => {
            var $target = $event.currentTarget;

            this.auditProfileSection.InspectionItems = [];
            if ($target['checked'] == true) {
                angular.forEach(this.inspectionItems,(obj, index) => {
                    this.toggleInspectionItem(obj.Key * 1);
                });
            }
        };

        /**
         * @description Returns a boolean if the inspection item is found from the inspection items array.
         * @param {number} id
         */
        isInspectionItemSelected = (id: number) => {
            return this.auditProfileSection.InspectionItems.indexOf(id) > -1
        };

        save = (): void => {
            var profileSection = _.omit(this.auditProfileSection, 'AreaClassificationName', 'AreaName', 'ProgramName');

            if (this.auditProfileSection.AuditProfileSectionId > 0) {
                // PUT
                this.auditsSvc.updateAuditProfileSection(this.auditProfileSection.AuditProfileSectionId, profileSection)
                    .then(this.onSuccess, this.onFailure);
            } else {
                // POST
                this.auditsSvc.saveAuditProfileSection(profileSection)
                    .then(this.onSuccess, this.onFailure);
            }
        };

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            //this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.$modalInstance.close();
            bootbox.alert('Audit profile section was saved successfully!', this.redirectToProfileEntry);
        }

        /** @description Cancel the data entry. */
        cancel = (): void=> {
            this.$modalInstance.dismiss("cancel");
        }

        /**
         * @description Return to the profile entry page.
         */
        redirectToProfileEntry = () => {
            window.location.replace(this.basePath + 'Quality/Audit/ProfileEntry/' +
                (this.auditProfileSection.AuditProfileId));
        }
    }

    angular.module("app").controller("ChaiTea.Quality.Audit.ProfileEntry.InspectionItemsCtrl", InspectionItemsCtrl);
}