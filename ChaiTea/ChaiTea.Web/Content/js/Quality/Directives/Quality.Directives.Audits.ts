﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Quality.Directives {
    'use strict';

    /*
     * @description Requires Score value in Audit/Entry forms if any siblings are truthy
     */
    function ScoreValidatorDirective() {
        return <ng.IDirective>{
            restrict: 'A',
            require: 'ngModel',
            link: function(scope: any, elem, attrs, ctrl) {
                //items in Inspection to check
                scope.$watch('inspection', function(newVal, oldVal) {
                    if (newVal != oldVal) {
                        checkScore(scope, attrs);
                    }
                }, true);

                //init run
                checkScore(scope, attrs);
            }
        };

        function checkScore(scope, attrs) {
            var required = ['Comment', 'ScorecardDetailImages', 'TodoItems'];
                _.forEach(required,(req: string) => {
                    if (scope.inspection[req] && scope.inspection[req].length) {
                        scope.inspection.isRequired = true;
                        return false;
                    } else if (scope.inspection[req]) {
                        scope.inspection.isRequired = false;
                    }
            });
                if (scope.inspection.Score && scope.inspection.Score < attrs.goalCheck && attrs.requiredTodosInternal && scope.inspection.TodoItems.length <= 0) {
                scope.inspection.RequireToDosInternalAudit = true;
            }
            else {
                scope.inspection.RequireToDosInternalAudit = false;
            }
        }
    }

    angular.module('app').directive('scoreValidator', ScoreValidatorDirective);
} 