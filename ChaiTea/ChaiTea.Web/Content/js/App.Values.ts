﻿/**
 * @description Setup the value(s) and constant(s) for 'app' module.
 */

module ChaiTea {
    'use strict';

    export interface IClient extends ITimezone {
        ID: number;
        Name: string;
    }

    export interface ISite extends ITimezone {
        ID: number;
        Name: string;
    }

    export interface ITimezone {
        TimezoneName: string;
        OffsetInHours: string;
    }

    export interface IProgram {
        ID: number;
        Name: string;
    }
    export interface ILanguage {
        ID: number;
        Code: string;
        Name: string;
    }

    export interface IUserContext{
        UserId: number;
        Client: IClient;
        Site: ISite;
        Program: IProgram;
        Language: ILanguage;
        Timezone: ITimezone;
    }

    export interface IUserInfo {
        userId: number;
        employeeId?: number;
        orgUserId?: number;
        userEmail: string;
        userName: string;
        currentPicture?: string;
        name?: string;
        firstName?: string;
        lastName?:string;
        phoneNumber?:string;
        mainRoles: IMainRoles;
        activeRoles: Array<string>;
        userAttachments?: Array<any>;
        socialPictures?: Array<any>;
        backgroundPicture?: string;
    }

    export interface IMainRoles {
        crazyprogrammers: boolean;
        admins: boolean;           
        managers:boolean;
        users: boolean;
        employees: boolean;
        hradmins:boolean;
        billingviewers:boolean;
        timeclockviewers: boolean;
        trainingadmins: boolean;
        customers: boolean;
        profileviewers: boolean;
        sepviewers: boolean;
        messagebuilder:boolean;
        bonusviewers: boolean;
        managernotifications: boolean;
        customernotifications: boolean;
        suppliesviewers: boolean;
        todoviewers: boolean;
        turnoverviewers: boolean;
        ehsmanagers: boolean;
        surveyadmins: boolean;
        claimsmanagers: boolean;
        orgstructureuser: boolean;
    }

    export interface ISecondaryColors {
        teal: string;
        blue: string;
        lime: string;
        lavendar: string;
        orange: string;
        blueSteel: string;
        pink: string;
        tealDark: string;
        blueDark: string;
        limeDark: string;
        lavendarDark: string;
        orangeDark: string;
        blueSteelDark: string;
        pinkDark: string;
        muted: string;
    };

    export interface ICoreColors {
        default: string;
        primary: string;
        primaryDark: string;
        success: string;
        successDark: string;
        successLight: string;
        warning: string;
        warningDark: string;
        warningLight: string
        danger: string;
        dangerDark: string;
        dangerLight: string;
        border: string;
        gray: string;
        base: string;
        baseLight: string;
        baseDark: string;
        dark: string;
        darkDark: string;
    }

    export enum LANGUAGES{
        English = <any>"en",
        Spanish = <any>"es",
        French = <any>"fr",
        Gujarati = <any>"gu",
        Arabic = <any>"ar"
    }

    export interface ISiteSettings {
        fullDomain: string;
        protocol: string;
        host: string;
        basePath: string;
        defaultProfileImage: string;
        defaultBackgroundImage: string;
        chartSettings: IChartSettings;
        windowSizes: IWindowSizes;
        isLocalhost:boolean;
        isProd: boolean;
        isUat: boolean;
        isDev: boolean;
        isDebuggingEnabled: boolean;
        userLanguage: string; //"en","es","fr"
        tempLanguage: string;
        relativeModulePaths: {
            //Admin: string;
            Common: string;
            Components: string;
            //Contact: string;
            //Dashboard: string;
            //Financials: string;
            //ManagerConfiguration: string;
            //Personnel: string;
            //Quality: string;
            //Training: string;
        };
        colors: {
            secondaryColors: ISecondaryColors,
            coreColors: ICoreColors
        };
        wysiwygMenus: {
            default: Array<Array<string>>
        };
        isTriPartyHackUser: Function;
        mobileNavWidth: number;
        greentea: {
            dev: string;
            qa: string;
            uat: string;
            prod: string;
            local: string;
        };
        environment: string;
        access_token: string;
    }

    export interface IChartSettings {
        barWidth: number;
        groupPadding: number;
    }

    export interface IWindowSizes {
		isPhone:boolean;
        isTablet: boolean;
        isNotDesktop: boolean;
		isDesktop:boolean;
		isLargeDesktop:boolean;
        isExtraTiny: boolean; //maxWidth 500
        isTiny: boolean; //minWidth: 0
        isSmall: boolean; //minWidth: 768
        isMedium: boolean; //minWidth: 992
        isLarge: boolean; //minWidth: 1200
    }

    class ValuesSetup {
        constructor() {}

        /**
         * @description Set the user context values.
         */
        setupUserContext = (): IUserContext => {
            var defaultValue = { ID: 0, Name: '', TimezoneName: '', OffsetInHours: '' };
            var uContext = window['userContext'] || { UserId: 0, Client: defaultValue, Site: defaultValue, Program: defaultValue };
            uContext.Client = uContext.Client ? uContext.Client : defaultValue;
            uContext.Site = uContext.Site ? uContext.Site : defaultValue;
            uContext.Program = uContext.Program ? uContext.Program : defaultValue;
            var defaultTimezone = { TimezoneName: 'America/Los_Angeles', OffsetInHours: '-0700'};
            var tz = window['timezone'];
            uContext.Site.TimezoneName = tz.Site;
            uContext.Client.TimezoneName = tz.Client;
            uContext.Site.OffsetInHours = tz.Site ? moment.tz(uContext.Site.TimezoneName).format('ZZ') : null;
            uContext.Client.OffsetInHours = tz.Client ? moment.tz(uContext.Client.TimezoneName).format('ZZ') : null;
            uContext.Timezone = { TimezoneName: tz.Site || tz.Client || defaultTimezone.TimezoneName, OffsetInHours: uContext.Site.OffsetInHours || uContext.Client.OffsetInHours || defaultTimezone.OffsetInHours };
            return uContext;
        }
        
        /**
         * @description Set the user info values.
         */
        setupUserInfo = (): IUserInfo => {
            var uInfo = window['userInfo'] || {};

            var activeRoles: Array<string> = [];
            angular.forEach(uInfo.mainRoles, (val, key) => {
                if (val === true) {
                    activeRoles.push(key);
                }
            });


            return {
                userId: uInfo.userId,
                userEmail: uInfo.userEmail,
                userName: '',
                mainRoles: uInfo.mainRoles || {},
                activeRoles: activeRoles
            }
        }

        /**
         * @description Set the site settings values.
         */
        setupSiteSettings = (): ISiteSettings => {
            var basePath = window['basePath'] || {},
                windowWidth = angular.element(window).width(),
                _isDebuggingEnabled = (window && window["App"]) ? window["App"]["IsDebuggingEnabled"] : false,
                userLanguage = document.documentElement.lang || "en";
            

            function getCookieVal(cookie, key) {
                var cookieIndex = cookie.indexOf(key);
                if (cookieIndex === -1)
                    return undefined;
                var substr = cookie.substring(cookieIndex + key.length + 1);
                var end = substr.indexOf('; ');
                if (end === -1)
                    return substr;
                return substr.substring(0, end);
            }

            var environment;
            if (location.host.indexOf('uat') > -1) {
                environment = 'uat';
            } else if (location.host.indexOf('dev') > -1) {
                environment = 'dev';
            } else if (location.host.indexOf('qa') > -1) {
                environment = 'qa';
            } else if (location.host.indexOf('r2.') > -1) {
                environment = 'prod';
            } else {
                environment = 'local';
            }

            function makeGreenteaLinks(env) {
                return 'https://' + env + '-backoffice-cdn.sbminsite.com/';
            }

            return {
                fullDomain: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
                protocol: location.protocol,
                host: location.host,
                basePath: basePath,
                mobileNavWidth: windowWidth - 80,
                windowSizes: <IWindowSizes>{
                    isPhone: windowWidth < 768,
                    isTablet: windowWidth >= 768,
                    isNotDesktop: windowWidth < 992,
                    isDesktop: windowWidth > 992,
                    isLargeDesktop: windowWidth > 1167,
                    isExtraTiny: windowWidth < 500,
                    isTiny: windowWidth <= 768,
                    isSmall: windowWidth > 768 && windowWidth < 992,
                    isMedium: windowWidth > 992 && windowWidth < 1200,
                    isLarge: windowWidth > 992
                },
                chartSettings: <IChartSettings>{
                    barWidth: (windowWidth < 768) ? 25 : 30,
                    groupPadding: (windowWidth < 768) ? 0.15 : 0.25
                },
                isDebuggingEnabled: _isDebuggingEnabled,
                isLocalhost: location.host.indexOf('localhost') > -1,
                isUat: location.host.indexOf('uat') > -1,
                isDev: location.host.indexOf('dev') > -1,
                isProd: location.host.indexOf('r2.') > -1,
                userLanguage: userLanguage,
                tempLanguage: null,
                defaultProfileImage: basePath + 'Content/img/user.jpg',
                defaultBackgroundImage: basePath + 'Content/img/default-profile-background.png',
                relativeModulePaths: {
                    Common: basePath + 'Content/js/Common/',
                    Components: basePath + 'Content/js/Components/',
                },
                colors: {
                    secondaryColors: {
                        teal: '#1aaaa5', blue: '#06467c', lime: '#b5bd32', lavendar: '#994f9c', orange: '#f26130', blueSteel: '#1690c3', pink: '#ca3471',
                        tealDark: '#158c88', blueDark: '#053B69', limeDark: '#A6AD2E', lavendarDark: '#7F4181', orangeDark: '#D6562A', blueSteelDark: '#137CA8', pinkDark: '#90114e', muted: '#999c9e'
                    },
                    coreColors: {
                        default: '#ffffff', primary: '#00AFD1', primaryDark: '#009DBC', success: '#9FC62D', successDark: '#81A125', successLight: '#B9E047', warning: '#F99D00', warningDark: '#E79200',
                        warningLight: '#FFB71A', danger: '#E93D42', dangerDark: '#D0363C', dangerLight: '#FF575C', border: '#E8E8E8', gray: '#F5F5F5', base: '#CCCCCC', baseDark: '#9E9E9E', baseLight: '#E6E6E6',
                        dark: '#403841', darkDark: '#2F2930'
                    }

                },
                wysiwygMenus: {
                    default:[
                        ['bold'], ['italic'], ['underline'], ['ordered-list'], ['unordered-list'], ['link']
                    ]
                },
                isTriPartyHackUser : () => {
                    var users = [16742, 16743, 16744];
                    if(_.indexOf(users, window['userContext'].UserId) >= 0){
                        return true;
                    }
                    return false;
                }, //HACK For landscape and security.
                greentea: {
                    dev: makeGreenteaLinks('dev'),
                    qa: makeGreenteaLinks('qa'),
                    uat: makeGreenteaLinks('uat'),
                    prod: makeGreenteaLinks('prod'),
                    local: 'http://localhost:4300/'
                },
                environment: environment,
                access_token: getCookieVal(window.document.cookie, 'r2sbminsite'),
            }

        }
    }

    var valSetup = new ValuesSetup();

    angular.module('app').constant('userContext', valSetup.setupUserContext());
    angular.module('app').constant('userInfo', valSetup.setupUserInfo());
    angular.module('app').constant('sitesettings', valSetup.setupSiteSettings());

    export var SiteSettings = valSetup.setupSiteSettings();
}

