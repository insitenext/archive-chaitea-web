﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for audits.
 */
module ChaiTea.Services.Quality {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to Audit resource.
     */
    export interface IFloorProfileResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IAuditFloorProfileResource extends IFloorProfileResource {
        cloneProfile(Object): ng.resource.IResource<any>;
    }

    export interface IFloorProfileSvc {
        cloneAuditProfile(sourceId: number, targetId: number, responsibleUserId: number);
        getAllAuditProfiles(params?: commonInterfaces.IODataPagingParamModel, doIgnoreSite?: boolean): angular.IPromise<any>;
    }

    class FloorProfileSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ChaiTea.ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IFloorProfileSvc {
            return {
                cloneAuditProfile: (sourceId, targetId, responsibleUserId) => { return this.cloneAuditProfile(sourceId, targetId, responsibleUserId); },
                getAllAuditProfiles: (params, doIgnoreSite) => { return this.getAllAuditProfiles(params, doIgnoreSite); }
            }
        }

        /**
         * @description Returns an array of all audits profiles.
         * @param {IODataPagingParam} params
         */
        getAllAuditProfiles = (params?: commonInterfaces.IODataPagingParamModel, doIgnoreSite: boolean = false) => {
            return this.auditFloorProfileSectionResource().getByOData(angular.extend(params || {}, { doIgnoreSite: doIgnoreSite })).$promise;
        }

        /**
         * @description Create a new profile or clone an existing profile.
         * @param {number} sourceId - auditProfileId where to copy from
         * @param {number} targetId - floorId destination to copy to
         */
        cloneAuditProfile = (sourceId: number, targetId: number, responsibleUserId: number) => {
            return this.auditFloorProfileSectionResource().cloneProfile({ sourceId: sourceId, targetId: targetId, responsibleUserId: responsibleUserId }).$promise;
        }

        /**
         * @description Resource object for audit floor profile section.
         */
        private auditFloorProfileSectionResource = (): IAuditFloorProfileResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <IAuditFloorProfileResource>this.$resource(
                this.basePath + 'api/Services/FloorProfile/:id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/FloorProfile/:doIgnoreSite',
                        params: { skip: 0 }
                    },
                    cloneProfile: {
                        method: 'GET',
                        url: this.basePath + 'api/Services/FloorProfile/Clone/:sourceId/:targetId/:responsibleUserId',
                    }
                });
        }
    }

    angular.module('app').service('chaitea.services.quality.floorprofile', FloorProfileSvc);
}


