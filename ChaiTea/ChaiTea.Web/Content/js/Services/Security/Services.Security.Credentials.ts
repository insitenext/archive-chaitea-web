﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface ICredentialsSvc {
        getAwsCredentials(): ng.IPromise<any>;
        getAwsSignedUrl(folder: string, key: string): ng.IPromise<any>;
        getAwsCloudfrontSignedUrl(folder: string, key: string): ng.IPromise<any>;
    }

    class CredentialsSvc implements angular.IServiceProvider {

        private basePath: string;
        private errorMsg: string;
        public endPoint: string;

        static $inject = ['$q', '$resource', 'sitesettings'];
        constructor(
            private $q: angular.IQService,
            private $resource: ng.resource.IResourceService,
            private sitesettings: ChaiTea.ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ICredentialsSvc {
            return {
                getAwsCredentials: () => (this.getAwsCredentials()),
                getAwsSignedUrl: (folder: string, key: string) => (this.getAwsSignedUrl(folder, key)),
                getAwsCloudfrontSignedUrl: (folder: string, key: string) => (this.getAwsCloudfrontSignedUrl(folder, key))
            };
        }

        /**
         * @description Custom resource call.
         */
        private getAwsCredentials = () => {
            return this.securityResource('Aws/Credentials').get().$promise;
        }

        /**
         * @description Custom resource call.
         */
        private getAwsSignedUrl = (folder: string, key: string) => {

            var parts = key.split('.');
            var endpoint = 'Aws/SignedUrl/' + folder + '/' + parts[1] + '/' + parts[0];

            return this.securityResource(endpoint).get().$promise;
        }

        /**
         * @description Custom resource call.
         */
        private getAwsCloudfrontSignedUrl = (folder: string, key: string) => {

            var parts = key.split('.');
            var endpoint = 'Aws/SignedCloudfrontUrl/' + folder + '/' + parts[1] + '/' + parts[0];

            return this.securityResource(endpoint).get().$promise;
        }

        /**
         * @description Base resource method.
         */
        private securityResource = (endPoint: string, key?: string): ng.resource.IResourceClass<any> => {

            var url = this.basePath + 'api/Services/Security/' + endPoint + '/' + (key ? key : '');
            return this.$resource(url);
        }

    }


    angular.module('app').service('chaitea.services.credentials', CredentialsSvc);
}  