﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Services {
    'use strict'

    import serviceInterfaces = ChaiTea.Services.Interfaces;

    enum EndPoint {
        Undefined,
        ComplaintTypes,
        PreventableStatuses,
        Classifications,
        Employees,
        Buildings,
        Floors,
        Areas,
        Programs,
        AreaClassifications,
        AreaClassificationInspectionItems,
        ScoringProfiles,
        WorkOrderServiceTypes
    };

    export interface ILookupListSvc {
        getComplaintTypesForProgram(params?: Object): angular.IPromise<any>;
        getComplaintTypesForComplaint(params?: Object): angular.IPromise<any>;
        getComplaintTypes(params?: Object): angular.IPromise<any>;
        getComplimentTypes(params?: Object): angular.IPromise<any>;
        getPreventableStatuses(params?: Object): angular.IPromise<any>;
        getClassifications(params?: Object): angular.IPromise<any>;
        getEmployeesForComplaint(params?: Object): angular.IPromise<any>;
        getEmployeesForCompliment(params?: Object): angular.IPromise<any>;
        getBuildingsForComplaint(params?: Object): angular.IPromise<any>;
        getBuildingsForCompliment(params?: Object): angular.IPromise<any>;
        getFloors(params?: Object): angular.IPromise<any>;
        getFloorByFloorId(floorId: number): angular.IPromise<any>;
        getAreas(params?: Object): angular.IPromise<any>;
        getPrograms(params?: Object): angular.IPromise<any>;
        getAreaClassificationsForProfile(auditProfileId: number, auditProfileSectionId: number, areaId: number): angular.IPromise<any>;
        getEmployees(params?: Object): angular.IPromise<any>;
        getBuildings(): angular.IPromise<any>;
        getBuildingsForSite(id: number): angular.IPromise<any>;
        getAreaClassificationInspectionItems(id: number): angular.IPromise<any>;
        getScoringProfiles(): angular.IPromise<any>;
        getWorkOrderServiceTypes(ProgramId: number): angular.IPromise<any>;
        getWorkOrderRecurrenceTypes(): angular.IPromise<any>;
        getWorkOrderPriorities(): angular.IPromise<any>;
        getAttendanceIssueTypes(): angular.IPromise<any>;
        getConductTypes(): angular.IPromise<any>;
        getIncidentTypes(): angular.IPromise<any>;
        getOwnershipTypes(): angular.IPromise<any>;
        getReportTypes(): angular.IPromise<any>;
        getRejectTypes(): angular.IPromise<any>;
    }

    class LookupListSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = [
            '$q',
            '$resource',
            'sitesettings',
            'chaitea.common.services.apibasesvc'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {any} sitesettings
         */
        constructor(
            private $q: angular.IQService,
            private $resource: ng.resource.IResourceService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ILookupListSvc {
            return {
                getComplaintTypesForProgram: (params) => { return this.getComplaintTypesForProgram(params); },
                getComplaintTypesForComplaint: (params) => { return this.getComplaintTypesForComplaint(params); },
                getComplaintTypes: (params) => { return this.getComplaintTypes(params); },
                getComplimentTypes: (params) => { return this.getComplimentTypes(params); },
                getPreventableStatuses: (params) => { return this.getPreventableStatuses(params); },
                getClassifications: (params) => { return this.getClassifications(params); },
                getEmployeesForComplaint: (params) => { return this.getEmployeesForComplaint(params); },
                getEmployeesForCompliment: (params) => { return this.getEmployeesForCompliment(params); },
                getBuildingsForComplaint: (params) => { return this.getBuildingsForComplaint(params); },
                getBuildingsForCompliment: (params) => { return this.getBuildingsForCompliment(params); },
                getFloors: (params) => { return this.getFloors(params); },
                getFloorByFloorId: (floorId) => { return this.getFloorByFloorId(floorId); },
                getAreas: (params) => { return this.getAreas(params); },
                getPrograms: (params) => { return this.getPrograms(params); },
                getAreaClassificationsForProfile: (auditProfileId, auditProfileSectionId, areaId) => { return this.getAreaClassificationsForProfile(auditProfileId, auditProfileSectionId, areaId); },
                getEmployees: (params) => { return this.getEmployees(params); },
                getBuildings: () => { return this.getBuildings(); },
                getBuildingsForSite: (id) => { return this.getBuildingsForSite(id); },
                getAreaClassificationInspectionItems: (id) => { return this.getAreaClassificationInspectionItems(id); },
                getScoringProfiles: () => { return this.getScoringProfiles(); },
                getWorkOrderServiceTypes: (ProgramId) => { return this.getWorkOrderServiceTypes(ProgramId); },
                getWorkOrderRecurrenceTypes: () => { return this.getWorkOrderRecurrenceTypes(); },
                getWorkOrderPriorities: () => { return this.getWorkOrderPriorities(); },
                getAttendanceIssueTypes: () => { return this.getAttendanceIssueTypes(); },
                getConductTypes: () => { return this.getConductTypes(); },
                getIncidentTypes: () => { return this.getIncidentTypes(); },
                getOwnershipTypes: () => { return this.getOwnershipTypes(); },
                getReportTypes: () => { return this.getReportTypes(); },
                getRejectTypes: () => { return this.getRejectTypes(); }
            }
        }

        private getEndPoint = (endPoint: EndPoint, custom?: string): string => {
            return 'LookupList/' + EndPoint[endPoint].toString() + '/' + custom;
        }

        /**
         * Returns complaint types.
         * @param {Object} params
         */
        private getComplaintTypes = (params?: Object): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.ComplaintTypes));
        }

        /**
         * Returns complaint types.
         * @param {Object} params
         */
        private getComplaintTypesForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getComplaintTypes();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplaintTypes/Complaint/:id', { id: '@id' }).get(params).$promise;
        }

        /**
        * Returns complaint types.
        * @param {Object} params
        */
        private getComplaintTypesForProgram = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ComplaintTypes/Program/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns compliment types.
         * @param {Object} params
         */
        private getComplimentTypes = (params?: Object): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.ComplaintTypes));
        }

        /**
         * Returns preventable statuses.
         * @param {Object} params
         */
        private getPreventableStatuses = (params?: Object): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.PreventableStatuses));
        }

        /**
         * Returns classifications.
         * @param {Object} params
         */
        private getClassifications = (params?: Object): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.Classifications));
        }

        /**
         * Returns employees.
         * @param {Object} params
         */
        private getEmployeesForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getEmployees();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Employees/Complaint/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns employees.
         * @param {Object} params
         */
        private getEmployeesForCompliment = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getEmployees();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Employees/Compliment/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        private getBuildings = (): angular.IPromise<any> => {
            return this.apiSvc.get(this.getEndPoint(EndPoint.Buildings));
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        private getBuildingsForSite = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Site/:id',
                { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        private getBuildingsForComplaint = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Complaint/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns buildings.
         * @param {Object} params
         */
        private getBuildingsForCompliment = (params?: any): angular.IPromise<any> => {
            if (params.id == 0) {
                return this.getBuildings();
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/Buildings/Compliment/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns floors.
         * @param {Object} params
         */
        private getFloors = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Floors/Building/:id', { id: '@id' }).get(params).$promise;
        }

        private getFloorByFloorId = (floorId: number): angular.IPromise<any>=> {
            return this.$resource(this.basePath + 'api/Services/Floor/:id', { id: '@id' }).get({ id: floorId }).$promise;
        }

        /**
         * Returns areas.
         * @param {Object} params
         */
        private getAreas = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Areas/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns programs.
         * @param {Object} params
         */
        private getPrograms = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Programs/:id', { id: '@id' }).get(params).$promise;
        }

        /**
         * Returns area classifications for profile.
         */
        private getAreaClassificationsForProfile = (auditProfileId: number, auditProfileSectionId: number, areaId: number): angular.IPromise<any> => {
            var params = { auditProfileId: auditProfileId, areaId: areaId };
            if (auditProfileSectionId > 0) {
                _.assign(params, { auditProfileSectionId: auditProfileSectionId });
            }
            return this.$resource(this.basePath + 'api/Services/LookupList/AreaClassifications/:auditProfileId/:areaId/:auditProfileSectionId')
                .get(params).$promise;
        }

        /**
         * Returns employees for site.
         */
        private getEmployees = (params?: Object): angular.IPromise<any> => {
            console.log(params);

            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.Employees));
        }

        /**
         * Returns buildings for site.
         * @param {number} id
         */
        private getAreaClassificationInspectionItems = (id: number): angular.IPromise<any> => {
            return this.apiSvc.getById(id, this.getEndPoint(EndPoint.AreaClassificationInspectionItems));
        }

        /**
         * Returns scoring profiles.
         */
        private getScoringProfiles = (params?: Object): angular.IPromise<any> => {
            return this.apiSvc.getByOdata(params, this.getEndPoint(EndPoint.ScoringProfiles));
        }

        /**
         * @description Returns a list of service types.
         */
        private getWorkOrderServiceTypes = (id: number): angular.IPromise<any> => {
            return this.apiSvc.getById(id, this.getEndPoint(EndPoint.WorkOrderServiceTypes));
        }

        /**
         * @description Returns a list of recurrence pattern types.
         */
        private getWorkOrderRecurrenceTypes = (): angular.IPromise<any> => {
            var defer = this.$q.defer();
            defer.resolve(<Array<serviceInterfaces.ILookupListOption>>[
                { Key: 1, Value: 'Daily' },
                { Key: 2, Value: 'Weekly' },
                { Key: 3, Value: '2x Month' },
                { Key: 4, Value: 'Monthly' },
                { Key: 5, Value: 'Quarterly' },
                { Key: 6, Value: '6 months' },
                { Key: 7, Value: 'Yearly' }]);

            return defer.promise;
        }

        /**
         * @description Returns a list of work order priorities.
         */
        private getWorkOrderPriorities = (): angular.IPromise<any> => {
            var defer = this.$q.defer();
            defer.resolve(<Array<serviceInterfaces.ILookupListOption>>[
                { Key: 1, Value: 'Normal' },
                { Key: 2, Value: 'Critical' }]);

            return defer.promise;
        }

        /**
        * @description Returns a list of attendance issue types.
        */
        getAttendanceIssueTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/IssueTypes').query().$promise;;
        }
        
        /**
        * @description Returns a list of conduct issue types.
        */
        getConductTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ConductTypes').query().$promise;;
        }

        /**
           * @description Returns a list of safety Incident types.
           */
        getIncidentTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/IncidentTypes').query().$promise;;
        }
        /**
           * @description Returns a list of ownerships types.
           */
        getOwnershipTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Ownership/Classifications').query().$promise;;
        }

        getReportTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Ownership/ReportTypes').query().$promise;
        }

        getRejectTypes = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/Ownership/RejectionTypes').query().$promise;
        }
    }

    angular.module('app').service('chaitea.services.lookuplist', LookupListSvc);
}