﻿/**
 * @description Define and configure the 'app' module.
 */
module ChaiTea {
    'use strict';

    angular.module('app', [
        'ngResource',
        'ngSanitize',
        'ngAnimate',
        'ngTouch',
        'ui.event',
        'ui.bootstrap',
        'ui.router',
        'ui.utils',
        'infinite-scroll',
        'LocalStorageModule',
        'blockUI',
        'ui.multiselect',
        'ngFileUpload',
        'SignalR',
        'ngSignaturePad',
        'pascalprecht.translate',
        'ngTagsInput',
        'angular-bootstrap-select',
        'mwl.calendar',
        'toggle-switch',
        'ImageCropper',
        'wysiwyg.module',
        'mgo-angular-wizard',
        'ngFlowGrid',
        'ui.grid',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.infiniteScroll',
        'ui.select',
        'cgBusy',
        'nemLogging',
        'uiGmapgoogle-maps',
        'ngDraggable',
        'zendeskWidget',
        'pdf'
    ]).config(config);

    config.$inject =
    [
        '$compileProvider',
        '$provide',
        '$logProvider',
        'localStorageServiceProvider',
        '$httpProvider',
        '$locationProvider',
        'sitesettings',
        '$translateProvider',
        '$sceDelegateProvider',
        'userInfo',
        'userContext',
        'ZendeskWidgetProvider'
    ];

    function config(
        $compileProvider: angular.ICompileProvider,
        $provide: angular.auto.IProvideService,
        $logProvider: angular.ILogProvider,
        localStorageServiceProvider: ChaiTea.Common.Services.ILocalDataStoreSvc,
        $httpProvider: angular.IHttpProvider,
        $locationProvider: angular.ILocationProvider,
        sitesettings: ISiteSettings,
        $translateProvider: angular.translate.ITranslateProvider,
        $sceDelegateProvider: angular.ISCEDelegateProvider,
        userInfo: IUserInfo,
        userContext: IUserContext,
        ZendeskWidgetProvider: any): void {

        // Local storage settings
        localStorageServiceProvider['prefix'] = 'sbm';
        localStorageServiceProvider['storageType'] = 'sessionStorage';

        // Turn off debugging attributes and console logging
        $compileProvider.debugInfoEnabled(!sitesettings.isProd);
        $logProvider.debugEnabled(!sitesettings.isProd);

        $httpProvider.useApplyAsync(true);

        //$locationProvider.html5Mode(true);

        // Interceptors
        $httpProvider.interceptors.push('chaitea.core.services.httpinterceptorsvc');

        //toastr options config
        if (toastr) {
            toastr.options.positionClass = "toast-top-full-width",
            toastr.options.debug = true;
            toastr.options.closeButton = true;
            toastr.options.showMethod = 'slideDown';
            toastr.options.progressBar = true;
        }

        //#region Mixpanel configuration
        if (typeof mixpanel != 'undefined' && mixpanel.identify) {
            mixpanel.identify(userInfo.userId.toString());
            mixpanel.people.set({
                "$name": userInfo.lastName ? userInfo.firstName + ' ' + userInfo.lastName : userInfo.userEmail,
                "$created": new Date(),
                "$email": userInfo.userEmail,
                "$client": userContext.Client.Name,
                "$program": userContext.Program.Name,
                "$site": userContext.Site.Name,
                "$roles": userInfo.activeRoles.toString()
            });
        }
        //#endregion

        //#region Angular-Translate configuration
        $translateProvider.useUrlLoader(sitesettings.basePath+'api/Services/I18N');
        $translateProvider.preferredLanguage(sitesettings.userLanguage);
        $translateProvider.fallbackLanguage(LANGUAGES.English.toString());
        $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
        //#endregion

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|tel|sms):/);
        $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
            'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
            'https://*.sbminsite.com/**',
            'http://*.sbminsite.com/**',
            'https://api.forecast.io/**',
            'https://goo.gl/**',
            'http://goo.gl/**',
            'http://*.cloudfront.net/**',
            'https://*.cloudfront.net/**',
            'http://*.s3-us-west-1.amazonaws.com/**',
            'https://*.s3-us-west-1.amazonaws.com/**',
            'http://*.s3-us-west-2.amazonaws.com/**',
            'https://*.s3-us-west-2.amazonaws.com/**'
        ]);

        ZendeskWidgetProvider.init({
            accountUrl: 'help.sbminsite.com',
            beforePageLoad: (zE) => {
                zE.hide();
            }
        });
    }
}
