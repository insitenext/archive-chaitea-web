﻿module ChaiTea.Core.Services {
    'use strict';

    export interface IPrinter {
        print(templateUrl: string, data: Object): void;
        printFromScope(templateUrl: string, scope: angular.IScope): void;
    }

    class PrinterSvc implements angular.IServiceProvider {
        static $inject = ['$rootScope', '$compile', '$http', '$timeout', '$q', 'sitesettings'];
        basePath;
        constructor(private $rootScope: angular.IRootScopeService, private $compile, private $http, private $timeout, private $q, private sitesettings: ISiteSettings) {
            this.basePath = this.sitesettings.basePath;
        }

        public $get() {
            return {
                print: (templateUrl: string, data: Object) => (this.print(templateUrl, data)),
                printFromScope: (templateUrl: string, scope: angular.IScope) => (this.printFromScope)
            }
        }


        public print = function (templateUrl, data) {
            var that = this;
            this.$http.get(templateUrl).success(function (template) {
                var printScope = that.$rootScope.$new();
                angular.extend(printScope, data);
                var element = that.$compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function () {
                    if (printScope.$$phase || that.$http.pendingRequests.length) {
                        that.$timeout(waitForRenderAndPrint);
                    } else {
                        // Replace printHtml with openNewWindow for debugging
                        that.printHtml(element.html());
                        //that.openNewWindow(element.html());
                        printScope.$destroy();
                    }
                };
                waitForRenderAndPrint();
            });
        };

        public printFromScope = function (templateUrl, scope) {
            this.$rootScope.isBeingPrinted = true;
            var that = this;
            this.$http.get(templateUrl).success(function (template) {
                var printScope = scope;
                var element = that.$compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function () {
                    if (printScope.$$phase || that.$http.pendingRequests.length) {
                        that.$timeout(waitForRenderAndPrint);
                    } else {
                        //that.printHtml(element.html()).then(function () {
                        //    that.$rootScope.isBeingPrinted = false;
                        //});
                        that.openNewWindow(element.html());

                    }
                };
                waitForRenderAndPrint();
            });
        };


        private printHtml = function (html) {
            var deferred = this.$q.defer();
            var that = this;
            var ua = window.navigator.userAgent;
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var strFrameName = "printThis-" + (new Date()).getTime();
            var htmlContent = `<!doctype html>
                <html>
                <head>
                <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="all">
                <link href="${that.basePath}Content/css/print.css" rel="stylesheet" media="all">

                </head>
                <body onload="javascript:window.print();">
                <div class="container-fluid">
                    ${html}
                </div>
                `;

            if (isIE) {
                var IEWindow = window.open('','','width=200,height=100');
                IEWindow.document.write(htmlContent);
    
                IEWindow.document.close();
                IEWindow.focus();
                IEWindow.print();
                IEWindow.close();
            } else {
                var hiddenFrame = <any>angular.element('<iframe id="' + strFrameName + '" style="display: none"></iframe>').appendTo('body')[0],
                    hfContentWindow = (hiddenFrame.contentWindow || hiddenFrame.contentDocument);
                hfContentWindow.printAndRemove = function () {
                    hfContentWindow.print();
                    $(hiddenFrame).remove();
                };

                var doc = hfContentWindow.document.open("text/html", "replace");
                doc.write(htmlContent);
                deferred.resolve(strFrameName);
                doc.close();
                setTimeout(() => {
                    angular.element(hiddenFrame).remove();
                }, 1000);
            }

            return deferred.promise;
        };

        private openNewWindow = function (html) {
            var that = this;
            var htmlContent = `
                <div class="container-fluid">
                    ${html}
                </div>
                `;
            var newWindow = window.open("printTest.html");
            newWindow.addEventListener('load', function () {
                $(newWindow.document.head).html(`
                    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="all">
                    <link href="${that.basePath}Content/css/print.css" rel="stylesheet" media="all">`);
                $(newWindow.document.body).html(htmlContent);

            }, false);
        };


    }

    angular.module('app').service('chaitea.core.services.printer', PrinterSvc);
}