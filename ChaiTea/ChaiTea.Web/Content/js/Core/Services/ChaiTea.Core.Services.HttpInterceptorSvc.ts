﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Services {
    export interface IHttpInterceptorSvc {
        request(config);
        response(response);
        responseError(response);
    }

    HttpInterceptorSvc.$inject = [
        '$q',
        '$log',
        'sitesettings',
        'chaitea.common.services.alertsvc',
        'chaitea.common.services.notificationsvc',
        'jwtauthheader'];

    /**
    * @description A HTTP interceptor factory for handling global AJAX call events.
    */
    function HttpInterceptorSvc(
        $q: angular.IQService,
        $log: angular.ILogService,
        siteSettings: ISiteSettings,
        alertSvc: Common.Services.IAlertSvc,
        notificationSvc: Common.Services.INotificationSvc,
        jwtAuthHeader: string) {

        var basePath = siteSettings.basePath;

        return {
            request: request,
            response: response,
            responseError: responseError
        }

        /**
         * @description Gets called when a request is made.
         */
        function request(config) {

            config.headers = config.headers || {};

            if (siteSettings.tempLanguage) {
                config.headers["lang-override"] = siteSettings.tempLanguage;
            }

            if (jwtAuthHeader) {
                config.headers.Authorization = jwtAuthHeader;
            }

            return config || this.$q.when(config);
        }

        /**
         * @description Gets called when a response is returned.
         */
        function response(response) {
            return response || $q.when(response);
        }

        /**
         * @description Gets called when a response fails.
         */
        function responseError(response) {
            // Handle unauthorized status and redirect
            if (response.status === 401) {
                //Redirect to home page after alert. 
                //If response contains http it is from an external site.
                if (!_.contains(response.config.url, ['http'])) {
                    notificationSvc.expiredAlert();
                }
            }
            else if (response.status === 403) {
                notificationSvc.errorToastMessage('You are not authorized to perform this action. ' +
                    'For questions, please contact support@sbmcorp.com');
            }
             
            return $q.reject(response);
        }
    }

    angular.module('app').factory('chaitea.core.services.httpinterceptorsvc', HttpInterceptorSvc);
}