﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Services {
    import commonSvc = ChaiTea.Common.Services;

    ExceptionHandlerSvc.$inject = [
        '$log',
        'sitesettings',
        'chaitea.common.services.alertsvc'];
    /**
    * @description Exception handler service.
    */
    function ExceptionHandlerSvc(
        $log: angular.ILogService,
        sitesettings: ISiteSettings,
        alertSvc: commonSvc.IAlertSvc) {
        return function (exception, cause) {
            exception.message += ` (caused by "${cause}")`;

            
            if (_.contains(sitesettings.host, 'localhost')){
                $log.error(exception.message, exception);
                $log.error.apply($log, arguments);
            } else {
                $log.error(exception.message);
                $log.error.apply($log, arguments);
            }
        };

    }

    
    
    angular.module('app').factory('$exceptionHandler', ExceptionHandlerSvc);
    
}