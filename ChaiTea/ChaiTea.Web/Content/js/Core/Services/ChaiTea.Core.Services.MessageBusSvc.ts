﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Services {
    'use strict';

    export interface IMessageBusSvc {
        emitMessage(eventName: string | Common.Interfaces.MessageBusMessages, obj: any) : void;
        onMessage(eventName: string | Common.Interfaces.MessageBusMessages, scope: angular.IScope, func: (event: angular.IAngularEvent, ...args: any[]) => any): void
    }

    export class MessageBusSvc implements angular.IServiceProvider {
        static id = 'chaitea.core.services.messagebussvc';
        static $inject = ['$rootScope'];
        constructor(private $rootScope: angular.IRootScopeService) { }

        public $get(): IMessageBusSvc {
            return {
                emitMessage : (eventName, obj: any) => {
                    return this.emitMessage(<string>eventName, obj);
                },
                onMessage : (eventName, scope, func) => {
                    return this.onMessage(<string>eventName, scope, func);
                }
            }
        }

        /**
         * @description Publish a message to all subscribers.
         * @param {string} eventName
         * @param {any} obj - Data that needs to get transmitted to the receivers.
         */
        emitMessage = (eventName: string, obj: any): void => {
            this.$rootScope.$emit(eventName, obj || {});
        }

        /**
         * @description Event listener. Automatic disposal when scope gets removed.
         * @param {string} eventName
         * @param {ng.IScope} scope
         * @param {(event: angular.IAngularEvent, ...args: any[]) => any} func - Callback function.
         */
        onMessage = (eventName: string, scope: angular.IScope,  func:(event: angular.IAngularEvent, ...args: any[]) => any): void => {
           this.$rootScope.$on(eventName, func);
            //scope.$on('$destroy', unbind());
        }
    }

    angular.module('app').service(MessageBusSvc.id, MessageBusSvc);
}
