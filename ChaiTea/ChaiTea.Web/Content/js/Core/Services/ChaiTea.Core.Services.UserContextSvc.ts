﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Services {
    'use strict';

    export interface IBaseContext {
        ID: number;
        Name: string;
    }
    export interface ILanguageContext extends IBaseContext {
        Code: string;
    }

    export interface IProgramContext extends IBaseContext {
        Program: string;
    }

    export interface ISiteContext extends IBaseContext {
        Program: IProgramContext;
    }

    export interface IClientContext extends IBaseContext {
        Site: ISiteContext;
    }

    export interface ILookupListOption {
        Key: string;
        Value: string;
    }

    export interface IContextLookupList {
        Name: string;
        Options: ILookupListOption[];
        UserContext: IClientContext;
    }

    export interface IGetResource {
        (options?: any, clearLocalStorage?: boolean): angular.IPromise<IContextLookupList>;
    }

    export interface ILocalStoreUserCtx {
        UserContext: Object;
        Clients: ILocalStoreUserCtxItem;
        Sites: ILocalStoreUserCtxItem;
        Programs: ILocalStoreUserCtxItem;
        Language: ILocalStoreUserCtxItem;
    }

    export interface ILocalStoreUserCtxItem {
        Name: string;
        Options: Array<ILookupListOption>;
    }

    export interface IUserContextSvc {
        GetClients: IGetResource;
        GetSites: IGetResource;
        GetPrograms: IGetResource;
        GetLanguages: IGetResource;
        SetContext: IGetResource;
        GetSetLocalStore(): angular.IPromise<ILocalStoreUserCtx>;
        OpenContextPicker(useTimeout?: boolean, timeoutDuration?: number): angular.ui.bootstrap.IModalServiceInstance;
        SetUserContext(): angular.IPromise<ILocalStoreUserCtx>;
    }

    export class UserContextSvc implements angular.IServiceProvider {
        static id = 'chaitea.core.services.usercontextsvc';

        private basePath: string;
        private clientsResource: ng.resource.IResourceClass<ng.resource.IResource<any>>;
        private sitesResource: ng.resource.IResourceClass<ng.resource.IResource<any>>;
        private programsResource: ng.resource.IResourceClass<ng.resource.IResource<any>>;
        private languagesResource: ng.resource.IResourceClass<ng.resource.IResource<any>>;
        private userContextResource: ng.resource.IResourceClass<ng.resource.IResource<any>>;

        static $inject = [
            '$resource',
            '$q',
            '$timeout',
            'sitesettings',
            'chaitea.common.services.localdatastoresvc',
            '$uibModal'];
        constructor(
            $resource: ng.resource.IResourceService,
            private $q: ng.IQService,
            private $timeout: angular.ITimeoutService,
            private sitesettings: ChaiTea.ISiteSettings,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.basePath = sitesettings.basePath;
            this.clientsResource = $resource(this.basePath + 'api/Services/User/Clients', {}, { get: { method: 'GET', cache: false } });
            this.sitesResource = $resource(this.basePath + 'api/Services/User/Sites/:id/:save', { id: '@id', save: '@save' }, { get: { method: 'GET', cache: false } });
            this.programsResource = $resource(this.basePath + 'api/Services/User/Programs/:id/:isClientId', { id: '@id', isClientId: '@isClientId' }, { get: { method: 'GET', cache: false } });
            this.languagesResource = $resource(this.basePath + 'api/Services/User/Languages', {}, { get: { method: 'GET', cache: false } });
            this.userContextResource = $resource(this.basePath + 'api/Services/User/ActiveContext', {}, { get: { method: 'GET', cache: false } });
        }

        public $get(): IUserContextSvc {
            return {
                GetSetLocalStore: () => { return this.GetSetLocalStore(); },
                GetClients: (options) => { return this.GetClients(options); },
                GetSites: (options, clearLocalStorage) => { return this.GetSites(options, clearLocalStorage); },
                GetPrograms: (options, clearLocalStorage) => { return this.GetPrograms(options, clearLocalStorage); },
                GetLanguages: (options, clearLocalStorage) => { return this.GetLanguages(options, clearLocalStorage); },
                SetContext: (options: IUserContext) => { return this.SetContext(options); },
                OpenContextPicker: (useTimeout?: boolean, timeoutDuration?: number) => (this.OpenContextPicker(useTimeout, timeoutDuration)),
                SetUserContext: () => { return this.SetUserContext(); }
            }
        }

        public OpenContextPicker = (useTimeout?: boolean, timeoutDuration?:number): angular.ui.bootstrap.IModalServiceInstance => {
            var modalInstance;
            var options = {
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.contextPicker.tmpl.html',
                controller: 'UserContextCtrl as vm',
                size: 'md'
            };
            if (useTimeout == true) {
                this.$timeout(timeoutDuration || 1000).then(() => {
                    return this.$uibModal.open(options);
                });
            } else {
                return this.$uibModal.open(options);
            }
        }

        public GetSetLocalStore = (): angular.IPromise<ILocalStoreUserCtx> => {
            return this.clientsResource.get().$promise.then((res) => {
                if (res) {
                    this.setLocalUserContext(res);
                    return res;
                }
            });
        }

        public SetUserContext = (): angular.IPromise<ILocalStoreUserCtx> => {
            return this.userContextResource.get().$promise.then((res) => {
                if (res) {
                    this.setLocalUserContext(res);
                    return res;
                }
            });
        }

        public GetClients = (options?: any) => {
            return this.clientsResource.get(options).$promise
        }

        public GetSites = (options: any, clearLocalStorage: boolean = false) => {
            return this.sitesResource.get(options).$promise;
        }

        public GetPrograms = (options: any, clearLocalStorage: boolean = false) => {
            return this.programsResource.get(options).$promise;
        }

        public GetLanguages = (options: any, clearLocalStorage: boolean = false) => {
            return this.languagesResource.get(options).$promise;
        }

        public SetContext = (options: IUserContext) => {
            return this.userContextResource.save(options).$promise;
        }

        private getLocalUserContext = (): ILocalStoreUserCtx => {

            var ls = this.localStorageSvc.getObject('usercontext');

            return angular.fromJson(ls) || null;
        }

        private setLocalUserContext = (userContext: ILocalStoreUserCtx) => {
            this.localStorageSvc.setObject('usercontext', JSON.stringify(userContext));
            this.localStorageSvc.setObject('clients', angular.toJson(userContext.Clients));
        }

        private getFromLocalStorage = (keyName: string) => {
            var fromStorage = this.localStorageSvc.getObject(keyName);
            var def = this.$q.defer();
            if (fromStorage) {
                def.resolve(angular.fromJson(fromStorage));
            } else {
                def.reject(keyName + ' not in localStorage');
            }
            return def.promise;
        }

        private setLocalStorage = (keyName: string, data: any) => {
            this.localStorageSvc.setObject(keyName, angular.toJson(data), true);
        }
    }

    angular.module('app').service(UserContextSvc.id, UserContextSvc);
} 