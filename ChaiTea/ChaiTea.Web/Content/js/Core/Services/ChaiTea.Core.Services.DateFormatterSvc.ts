﻿module ChaiTea.Core.Services {
    'use strict';

    export interface IDateFormatterSvc {
        formatDateShort(date, format?: string): string;
        formatDateFull(date): string;
    }

    class DateFormatterSvc implements angular.IServiceProvider  {
        defaultFormat: string = 'MM-DD-YYYY';

        public $get(): IDateFormatterSvc {
            return {
                formatDateShort: (date, format?: string) => {
                    return this.formatDateShort(date, format);
                },
                formatDateFull: (date) => { return this.formatDateFull(date); }
            }
        }

        /**
         * @description Returns a date string with specified format.
         * @param {any} date
         * @param {string} format
         */
        formatDateShort = (date, format?: string): string=> {
            format = format || this.defaultFormat;
            return moment(date).format(format);
        }

        /**
         * @description Returns a date string with timestamp.
         * @param {any} date
         */
        formatDateFull = (date): string=> {
            return moment(date).format();
        }   
    }

    angular.module('app').service('chaitea.core.services.dateformattersvc', DateFormatterSvc);
} 