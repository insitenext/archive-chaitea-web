﻿/// <reference path="../../_libs.ts" />
module ChaiTea.Core.Controllers {
    'use strict';

    class SideBarCtrl implements ChaiTea.Common.Interfaces.INgController {
        basePath: string;
        mainNavItems = [];
        SubMenuItems = {
            settings : []
        };

        static $inject = ['$log', '$scope', 'userInfo', 'userContext', 'sitesettings', '$translate'];
        constructor(
            private $log: angular.ILogService,
            private $scope: angular.IScope,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private $translate: angular.translate.ITranslateService) {
            this.basePath = sitesettings.basePath;
        }

        public initialize = () => {
            var isManager = this.userInfo.mainRoles.managers;
            var isEmployee = this.userInfo.mainRoles.employees;
            var isCustomer = this.userInfo.mainRoles.customers;
            var isTrainingAdmin = this.userInfo.mainRoles.trainingadmins;
            var isHrAdmin = this.userInfo.mainRoles.hradmins;

            this.SubMenuItems.settings = [
                {
                    Name: 'SEP Bonus Config',
                    Url: this.basePath + 'Training/ServiceExcellence/BonusConfig',
                    Visible: isManager || isTrainingAdmin
                },
                {
                    Name: 'Course Admin',
                    Url: this.basePath + 'Training/Course/Index',
                    Visible: isTrainingAdmin
                },
                {
                    Name: 'Position Profile Builder',
                    Url: this.basePath + 'Settings/JobDescriptionBuilder/Index',
                    Visible: isTrainingAdmin
                },
                {
                    Name: 'Position Purpose Builder',
                    Url: this.basePath + 'Settings/PositionPurposeBuilder/Index',
                    Visible: isTrainingAdmin || isHrAdmin
                },
                {
                    Name: 'Message Builder',
                    Url: this.basePath + 'Settings/MessageBuilder/Index',
                    Visible: true
                },
                {
                    Name: 'Messages Sent',
                    Url: this.basePath + 'Settings/MessagesReceived/Index',
                    Visible: isManager || isCustomer
                },
                {
                    Name: 'KPI Builder',
                    Url: this.basePath + 'Settings/KPIBuilder/Index',
                    Visible: true
                },
            ]
            
            this.mainNavItems = [
                {
                    Name: 'Home',
                    IconName: 'fa-home2',
                    Url: this.basePath,
                    ChildrenItems: [],
                    Visible: !isEmployee
                },
                {
                    Name: 'My Profile',
                    IconName: 'fa-profile',
                    Url: 'Personnel/MyProfile/Index',
                    ChildrenItems: [],
                    Visible: isEmployee
                },
                {
                    Name: 'Quality',
                    IconName: 'fa-quality',
                    Url: this.basePath + 'Quality/Dashboard/Index',
                    ChildrenItems: [],
                    Visible: !isEmployee
                },
                {
                    Name: 'Personnel',
                    IconName: 'fa-personnel',
                    Url: this.basePath + 'Personnel/BoxScore/Index',
                    ChildrenItems: [],
                    Visible: !isEmployee
                },
                {
                    Name: 'Financials',
                    IconName: 'fa-financials',
                    Url: this.basePath + 'Financials/Dashboard/Index',
                    ChildrenItems: [],
                    Visible: !isEmployee
                },
                {
                    Name: 'Knowledge Center',
                    IconName: 'fa-training',
                    Url: this.basePath + 'Training/KnowledgeCenter/Index',
                    ChildrenItems: [],
                    Visible: true
                },
                {
                    Name: 'Create Report It',
                    IconName: 'fa-add-work-order',
                    Url: this.basePath + 'Personnel/Ownership/Entry',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && isEmployee
                },
                {
                    Name: 'Governance',
                    IconName: 'fa-governance',
                    Url: this.basePath + 'Governance/PositionFTE/Index',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isEmployee || isManager || isCustomer || isTrainingAdmin)
                },
                {
                    Name: 'Safety',
                    IconName: 'fa-safety',
                    Url: this.basePath + 'Personnel/Safety/Index',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isEmployee || isManager || isCustomer || isTrainingAdmin)
                },
                {
                    Name: 'Shop',
                    IconName: 'fa fa-shop',
                    Url: 'http://b2b.sbmstore.com/system/login.php',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isTrainingAdmin)
                },
                {
                    Name: 'Contact',
                    IconName: 'fa-phone',
                    Url: this.basePath + 'Contact/ContactPage/Index',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser //true
                },
                {
                    Name: 'Help Center',
                    IconName: 'fa-help-center',
                    Url: 'https://help.sbminsite.com',
                    Visible: !this.sitesettings.isTriPartyHackUser() //true
                },
                {
                    Name: 'Site Settings',
                    IconName: 'fa-cog2',
                    Url: '',
                    ChildrenItems: this.SubMenuItems.settings,
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isTrainingAdmin || isHrAdmin)
                }
            ]

            this.translateMenuItemNames();
        }

        translateMenuItemNames = () => {
            angular.forEach(this.mainNavItems, (menuItem) => {
                this.$translate(menuItem.Name).then((translation) => {
                    menuItem.Name = translation;
                });
                
                menuItem.isSubLinkActive = _.some(menuItem.ChildrenItems, { 'Url': window.location.pathname });
                menuItem.isActive = window.location.pathname == menuItem.Url || menuItem.isSubLinkActive;
                
                angular.forEach(menuItem.ChildrenItems, (childMenuItem) => {
                    childMenuItem.isActive = window.location.pathname == childMenuItem.Url;

                    this.$translate(childMenuItem.Name).then((translation) => {
                        childMenuItem.Name = translation;
                    });
                });
            });
        }
    }

    angular.module('app').controller("CoreSideBar", SideBarCtrl);
} 