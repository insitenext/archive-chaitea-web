﻿/// <reference path="../../../../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../services/chaitea.core.services.usercontextsvc.ts" />
/// <reference path="../../../../scripts/typings/angularjs/angular-resource.d.ts" />

module ChaiTea.Core.Controllers {

    declare var userInfo: any;

    import UserContextServiceNamespace = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;

    export interface IOption {
        ID?: number;
        Name: string;
    }

    export interface IPickerOptions {
        clients: Array<IOption>;
        sites: Array<IOption>;
        programs: Array<IOption>;
        languages: Array<IOption>;
    }

    export class UserContextCtrl {

        
        context: any;
        originalContext: any;
        options: IPickerOptions;
        originalOptions: any;
        
        clientName: string;
        siteName: string;
        programName: string;

        myPromise = null;
        disableProgramDropdown = false;

        caching: boolean = false;
        cacheTimeout = 900; // 15 minute timeout

        static $inject = [
            '$scope',
            'sitesettings',
            '$q',
            '$log',
            'userInfo',
            'userContext',
            'chaitea.core.services.usercontextsvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.messagebussvc',
            '$uibModalInstance',
            'chaitea.common.services.localdatastoresvc'];
        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private $q: angular.IQService,
            private $log: angular.ILogService,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private userContextService: ChaiTea.Core.Services.IUserContextSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private serviceMessageBus: ChaiTea.Core.Services.IMessageBusSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc
            ) {

            this.context = {};
            this.options = {
                clients: [],
                sites: [],
                programs: [],
                languages: []
            };
        }

        initialize = () => {
            
            this.context = this.localStorageSvc.getObject('usercontext').UserContext;
            if (!this.context) {
                this.userContextService.SetUserContext().then(() => {
                    this.context = this.localStorageSvc.getObject('usercontext');
                    this.initializeData();
                });                    
                
            } else {
                this.initializeData();
            }
            
            
        }

        public initializeData = () => {
            this.originalContext = this.context;

            if (!this.context.Site) {
                this.context.Site = { ID: 0 };
            }

            if (!this.context.Program) {
                this.context.Program = { ID: 0 };
            }

            if (!this.context.Language) {
                this.context.Language = { ID: 0 };
            }

            //get first list
            this.getClients();

            if (this.context.Client.ID) {
                //get other lists
                this.getSites(this.context.Client.ID);
                this.getPrograms(this.context.Client.ID, this.context.Site.ID);
            }

            this.getLanguages();
        }

        public onClientChange = () => {
            if (this.context.Client.ID == null) {
                this.options.sites = [];
                this.options.programs = [];
                return;
            }

            this.context.Site = null;
            this.context.Program = null;

            this.getSites(this.context.Client.ID, true);
            this.getPrograms(this.context.Client.ID, 0, true);
        }

        public onSiteChange = () => {
            this.getPrograms(this.context.Client.ID, this.context.Site.ID, true);
        }

        private getClients = () => {
            var key: string = 'clients-list:' + this.context.UserId;

            //check cache first
            var dataObj: any = this.localStorageSvc.getObject(key, this.cacheTimeout);

            if (!dataObj || !this.caching) {
                //pull new list
                this.myPromise = this.userContextService.GetClients().then((data: any) => {
                    if (data) {
                        //set for dropdown
                        this.options.clients = _.map(data.Clients.Options, this.optionMapper);
                        //cache
                        if (this.caching) {
                            this.localStorageSvc.setObject(key, data.Clients.Options, true);
                        }
                    }
                }, this.handleFailure);
            } else {
                this.options.clients = _.map(dataObj.data, this.optionMapper);;
            }
        }

        private getSites = (clientId: number, reset?:boolean) => {

            var key: string = 'sites-list:' + clientId;

            //check cache first
            var dataObj: any = this.localStorageSvc.getObject(key, this.cacheTimeout);

            if (!dataObj || !this.caching) {
                //pull new list
                this.myPromise = this.userContextService.GetSites({ id: clientId, save: false }).then((data: any) => {

                    if (data) {
                        //set for dropdown
                        this.options.sites = _.map(data.Sites.Options, this.optionMapper);
                        //cache
                        if (this.caching) {
                            this.localStorageSvc.setObject(key, data.Sites.Options, true);
                        }
                    }

                }, this.handleFailure);
            } else {
                this.options.sites = _.map(dataObj.data, this.optionMapper);
            }

            if (reset) {
                this.context.Site = this.options.sites[0];
                if (this.context.Site) {
                    this.context.SiteId = this.context.Site.ID;
                } else {
                    this.context.SiteId = 0;
                }
            }
        }

        private getPrograms = (clientId: number, siteId: number, reset?: boolean) => {

            var key: string = 'programs-list:' + clientId + ':' + siteId;

            //check cache first
            var dataObj: any = this.localStorageSvc.getObject(key, this.cacheTimeout);

            if (!dataObj || !this.caching) {
                //pull new list
                var params = (!siteId || siteId == 0 ? { id: clientId, isClientId: true } : { id: siteId, isClientId: false });
                this.myPromise = this.userContextService.GetPrograms(params).then((data: any) => {
                    if (data) {
                        //set for dropdown
                        this.options.programs = _.map(data.Programs.Options, this.optionMapper);
                        //cache
                        if (this.caching) {
                            this.localStorageSvc.setObject(key, data.Programs.Options, true);
                        }
                    }
                }, this.handleFailure);
            } else {
                this.options.programs = _.map(dataObj.data, this.optionMapper);
            }

            // HACK
            // HACK Hide all but Security and Landscape programs for specific users
            // HACK
            var usersToHideFrom = [16742, 16743, 16744];
            if (_.indexOf(usersToHideFrom, this.userContext.UserId) >= 0) {
                this.disableProgramDropdown = false;
                // Remove all but Security programs
                if (this.userContext.UserId === 16742) {
                    this.options.programs = _.filter(this.options.programs, item => (item.Name.toLowerCase().indexOf('security') >= 0));
                }
                // Remove all but Landscape programs
                if (_.indexOf([16743, 16744], this.userContext.UserId) >= 0) {
                    this.options.programs = _.filter(this.options.programs, item => (item.Name.toLowerCase().indexOf('landscaping') >= 0));
                }
            }
            // END HACK

            if (reset) {
                this.context.Program = this.options.programs[0];
                if (this.context.Program) {
                    this.context.ProgramId = this.context.Program.ID;
                } else {
                    this.context.ProgramId = 0;
                }
            }
        }

        private getLanguages = () => {
            var key: string = 'languages-list';

            //check cache first
            var dataObj: any = this.localStorageSvc.getObject(key, this.cacheTimeout);

            if (!dataObj) {
                //pull new list
                this.myPromise = this.userContextService.GetLanguages().then((data: any) => {
                    if (data) {
                        //set for dropdown
                        this.options.languages = _.map(data.Languages.Options, this.optionMapper);
                        //cache
                        this.localStorageSvc.setObject(key, data.Languages.Options, true);
                    }
                }, this.handleFailure);
            } else {
                this.options.languages = _.map(dataObj.data, this.optionMapper);
            }
        }

        private optionMapper = (o: any) => {
            if (!o.Key) {
                o.Key = 0;
            }
            return { ID: o.Key, Name: o.Value };            
        }
                        
        apply = () => {
            this.userContextService.SetContext(this.context).then(this.applyCallback, this.handleFailure);
        };

        applyCallback = () => {
            this.$uibModalInstance.close();
            window.setTimeout(() => {
                window.location.reload();
            }, 10);
        };

        cancel = () => {
            this.context = angular.copy(this.originalContext);
            this.options = angular.copy(this.originalOptions);

            this.$uibModalInstance.close();
        };

        handleFailure = (reason) => {
            this.$log.error(reason);
            return this.$q.reject(reason);
        };
    }
    angular.module('app').controller('UserContextCtrl', UserContextCtrl);
}