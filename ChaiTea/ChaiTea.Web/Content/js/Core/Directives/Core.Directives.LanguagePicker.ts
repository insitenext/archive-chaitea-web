﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Directives {
    'use strict';

    function languagePicker() {
        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            template:
            `<div class="pull-right line-height-50" data-ng-hide="vm.isLanguagePickerHidden">
                <select selectpicker=""
                        data-width="100px"
                        data-container="body"
                        data-ng-model="vm.selectedLanguage"
                        data-ng-change="vm.onSwitchLanguage($event)"
                        toggle-dropdown>
                    <option ng-repeat="r in vm.languages" value="{{r.Code}}" ng-selected="vm.selectedLanguage == r.Code">
                        {{r.Name}}
                    </option>
                </select>
            </div>`
        }
    }

    angular.module('app').directive('languagePicker', languagePicker);

}