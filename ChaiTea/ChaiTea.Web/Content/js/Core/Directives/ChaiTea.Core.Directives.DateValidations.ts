﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Core.Directives {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    /**
     * @description Validate that fromDate is before the endDate.
     */
    validateDateGreaterThan.$inject = ['$filter', 'chaitea.core.services.dateformattersvc'];
    function validateDateGreaterThan(
        $filter: angular.IFilterProvider,
        dateFormatterSvc: coreSvc.IDateFormatterSvc): angular.IDirective {

        var validationName = 'valDateGreaterThan';
        return <ng.IDirective>{
            restrict: 'A',
            require: 'ngModel',
            link: link
        };

        function link($scope, element, attrs, ctrl) {
            var validateDateRange = (inputValue)=> {
                var fromDate = dateFormatterSvc.formatDateShort(attrs['valDateGreaterThan'], 'MM/DD/YYYY');
                var toDate = dateFormatterSvc.formatDateShort(inputValue, 'MM/DD/YYYY');

                var isValid = isValidDateRange(fromDate, toDate);
                ctrl.$setValidity(validationName, isValid);

                return inputValue;
            };

            ctrl.$parsers.unshift(validateDateRange);
            ctrl.$formatters.push(validateDateRange);

            // Observe attribute change
            attrs.$observe(validationName, (comparisonModel)=> {
                return validateDateRange(ctrl.$viewValue);
            });
        }

        /**
         * @description Validates the from and to dates and make sure that the toDate 
         * is not less than fromDate.
         */
        function isValidDateRange(fromDate, toDate) {
            if (fromDate == '' || toDate == '')
                return true;

            if (isValidDate(fromDate) && isValidDate(toDate)) {
                var days = getDateDifference(fromDate, toDate);
                if (days < 0) {
                    return false;
                }
            }
            return true;
        };

        /**
         * @description Get date difference.
         */
        function getDateDifference (fromDate, toDate) {
            return Date.parse(toDate) - Date.parse(fromDate);
        }

        /**
         * @description Validates if an object is a valid date.
         */
        function isValidDate (date: any) {
            if (date == undefined)
                return false;
            var dateTime = Date.parse(date);

            if (isNaN(dateTime)) {
                return false;
            }
            return true;
        };

    }

    angular.module('app').directive('valDateGreaterThan', validateDateGreaterThan);
}