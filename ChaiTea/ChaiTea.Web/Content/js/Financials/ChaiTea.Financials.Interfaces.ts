﻿module ChaiTea.Financials.Interfaces {

    interface IUserAttachment extends Common.Interfaces.IAttachment {
        UserAttachmentId: number;
        UserAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    interface IEmployeeAttachment extends Common.Interfaces.IAttachment {
        EmployeeAttachmentId: number;
        EmployeeAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    enum USER_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    enum EMPLOYEE_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    export interface IEmployee {
        UserAttachments?: Array<IUserAttachment>;
        EmployeeId: number;
        HireDate?: string;
        JobDescription: string;
        Name: string;
        ProfileImage?: string;
    }

    export interface IEmployeesModalCtrl {
        cancel(): void;
        run(empId: number): void;
    }

    export interface IInvoiceItem {
        InvoiceItemId: number;
        Quantity: number;
        Rate: number;
        Amount: number;
        Description: string;
        LineNumber: number;
        CurrencyCodeId: number;
        CurrencyCodeName: string;
        IsTaxable: boolean;
        IsPaid: boolean;
        InvoiceId: number;
        ProgramName: string;
        ProgramId: number;
        TotalAmount: number;
    }

    export interface IInvoice {
        InvoiceId: number;
        InvoiceNumber: number;
        BilledAmount: number;
        CustomerId: number;
        Customer: ICustomerDetails;
        TaxAmount: number;
        InvoiceAmount: number;
        PaidAmount: number;
        OutstandingAmount: number;
        IsPaid: boolean;
        MonthOfService: string;
        MonthOfServiceDate: Date;
        InvoiceDate: Date;
        DueDate: Date;
        PO: string;
        Base: number;
        AboveBase: number;
        PaymentTermsTypeId: number;
        PaymentTermsTypeName: string;
        OrgId: number;
        InvoiceItems: Array<IInvoiceItem>;
    }

    export interface ICustomerDetails {
        CustomerId: number;
        Name: string;
        AddressOne: string;
        AddressTwo: string;
        City: string;
        Country: string;
        ZipCode: string;
    }

    export interface ITimeClock {
        TimeClockId: number;
        EmployeeId: number;
        Day: Date;
        StartTime: Date;
        EndTime: Date;
        RegularTime: number;
        OverTime: number;
        DoubleTime: number;
        ExtraTime: number;
        TotalTime: number;
        StraightTimeEquivalent?: number;
        TotalPay: number;
        WeekNo: number;
        EmployeeName: string;
        EmployeeJobDesc: string;
        EmployeeImage: string;
        TotalSTEHours: number;
        TotalHours: number;
    }

    export interface ITimeClockWithTotals {
        Data: Array<ITimeClock>;
        TotalOfTotalTime: number;
        TotalOfSTE: number;
    }


    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }
    export interface IWeek {
        weekNo: number;
        totalHours: number;
    }
    export interface IDayAggregate {
        day: Date;
        aggregateHours: number;
    }
    export interface ITimeClockEmployee {
        Day: Date;
        TotalHours: number;
        EmployeeId: number;
    }

    export interface IBillingMonthly {
        Month: string;
        Amount: number;
    }

    export interface IBillingSiteMonthly {
        SiteName: string;
        Total: number;
        Months: Array<IBillingMonthly>;
    }
    export interface ICalendarEvent {
        title: string; // The title of the event
        type: string; // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
        startsAt: Date; // A javascript date object for when the event starts
        ////endsAt: new Date(2015, 11, 5), // Optional - a javascript date object for when the event ends
        //editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
        //deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
        ////draggable: true, //Allow an event to be dragged and dropped
        //resizable: true, //Allow an event to be resizable
        incrementsBadgeTotal: boolean; //If set to false then will not count towards the badge total amount on the month and year view

        //#sreekanth added to customize for our needs with out changing actual directive functionality(modified tooltip template in actual angular-bootstrap-calendar-tpls.js file). do not set any value or ignore for default functionality.
        totalHours: number;
        aggregateHours: number;
        startTime: Date;
        endTime: Date;
        drillDown: boolean;
        refreshCount: number;
           
        //recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
        //cssClass: '' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
    }
}