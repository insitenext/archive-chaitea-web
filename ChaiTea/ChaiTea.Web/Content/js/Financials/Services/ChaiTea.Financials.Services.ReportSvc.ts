﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Financials.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IReportsSvc {
        getBillingTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBillingTrendsBarChart(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBillingByProgram(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBillingBySite(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBillingBySiteMonthly(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBillingDuesBarChart(): angular.IPromise<any>;
        getMonthlyTimeClockOfAllEmployees(params?: any): angular.IPromise<any>;
        getEmployeeTimeCLock(params?: IODataPagingParamModel): angular.IPromise<any>;
        getEmployeeTimeClockForTimeClockViewer(params?: IODataPagingParamModel): angular.IPromise<any>;

        //supplies related 
        getSuppliesMonthlyProductTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getSuppliesSpendByCategory(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getSuppliesSpendBillable(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getSuppliesSpendSubCommodityBillable(params?: any): angular.IPromise<any>;
        getSuppliesSpendGreen(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getSuppliesSpendSubCommodityGreen(params?: any): angular.IPromise<any>;
        getLatestUpdateDate(): angular.IPromise<any>;
        getSuppliesSpendByProgram(arams?: commonInterfaces.IDateRange): angular.IPromise<any>;
    }
    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }
    export interface IEmployeeTimeClockResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
    }

    class ReportsSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {obj} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings) {

            this.basePath = sitesettings.basePath;
        }

        public $get(): IReportsSvc {
            return {
                getBillingTrends: (params) => {
                    return this.getBillingTrends(params);
                },
                getBillingTrendsBarChart: (params) => {
                    return this.getBillingTrendsBarChart(params);
                },
                getBillingByProgram: (params) => {
                    return this.getBillingByProgram(params);
                },
                getBillingBySite: (params) => {
                    return this.getBillingBySite(params);
                },
                getBillingBySiteMonthly: (params) => {
                    return this.getBillingBySiteMonthly(params);
                },
                getBillingDuesBarChart: () => {
                    return this.getBillingDuesBarChart();
                },
                getMonthlyTimeClockOfAllEmployees: (params) => {
                    return this.getMonthlyTimeClockOfAllEmployees(params);
                },
                getEmployeeTimeCLock: (params?: IODataPagingParamModel) => {
                    return this.getEmployeeTimeCLock(params);
                },
                getEmployeeTimeClockForTimeClockViewer: (params?: IODataPagingParamModel) => {
                    return this.getEmployeeTimeClockForTimeClockViewer(params);
                },

                //supplies related
                getSuppliesMonthlyProductTrends: (params?: IODataPagingParamModel) => {
                    return this.getSuppliesMonthlyProductTrends(params);
                },
                getSuppliesSpendByCategory: (params?: IODataPagingParamModel) => {
                    return this.getSuppliesSpendByCategory(params);
                },

                getSuppliesSpendBillable: (params?: IODataPagingParamModel) => {
                    return this.getSuppliesSpendBillable(params);
                },

                getSuppliesSpendSubCommodityBillable: (params) => {
                    return this.getSuppliesSpendSubCommodityBillable(params);
                },
                getSuppliesSpendGreen: (params?: IODataPagingParamModel) => {
                    return this.getSuppliesSpendGreen(params);
                },
                getSuppliesSpendSubCommodityGreen: (params) => {
                    return this.getSuppliesSpendSubCommodityGreen(params);
                },
                getLatestUpdateDate: () => {
                    return this.getLatestUpdateDate();
                },
                getSuppliesSpendByProgram: (params?: IODataPagingParamModel) => {
                    return this.getSuppliesSpendByProgram(params);
                }
            }
        }

        /**
         * @description Complaint Reports.
         */
        getBillingTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/AggregateMonthlyTrends/:startDate/:endDate').get(params).$promise;
        }

        getBillingTrendsBarChart = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/MonthlyTrends/:startDate/:endDate').query(params).$promise;
        }

        getBillingByProgram = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/BillingByProgram/:startDate/:endDate').query(params).$promise;
        }

        getBillingBySite = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/BillingBySite/:startDate/:endDate').query(params).$promise;
        }


        getBillingBySiteMonthly = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/BillingBySiteMonthly/:startDate/:endDate').query(params).$promise;
        }

        getBillingDuesBarChart = (): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/AccountsReceivableAging/').query().$promise;
        }

        getMonthlyTimeClockOfAllEmployees = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/MonthlyTotalHours/:startDate/:endDate/:supervisorId').query(params).$promise;
        }
        getEmployeeTimeCLock = (params?: IODataPagingParamModel) => {
            return this.employeeTimeClockResource().getByOData(params || {}).$promise;
        }

        getEmployeeTimeClockForTimeClockViewer = (params?: IODataPagingParamModel) => {
            return this.employeeTimeClockGlobalResource().getByOData(params || {}).$promise;
        }

        //supplies related
        getSuppliesMonthlyProductTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/MonthlyProductTrends/:startDate/:endDate').get(params).$promise;
        }
        getSuppliesSpendByCategory = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendByCategory/:startDate/:endDate').query(params).$promise;
        }
        getSuppliesSpendBillable = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendByIsBillable/:startDate/:endDate').query(params).$promise;
        }
        getSuppliesSpendSubCommodityBillable = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendBySubCommodityByIsBillable/:startDate/:endDate/:commodityId/:isBillable').query(params).$promise;
        }
        getSuppliesSpendGreen = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendByIsGreen/:startDate/:endDate').query(params).$promise;
        }
        getSuppliesSpendSubCommodityGreen = (params?: any): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendBySubCommodityByIsGreen/:startDate/:endDate/:commodityId/:isGreen').query(params).$promise;
        }
        getLatestUpdateDate = (): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/POItem/LastUpdateDate').get().$promise;
        }
        getSuppliesSpendByProgram = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'api/Services/FinanceReport/SpendByProgram/:startDate/:endDate').query(params).$promise;
        }
        /**
        * @description Resource object for employee time clock
        */
        private employeeTimeClockResource = (): IEmployeeTimeClockResource => {
          
            return <IEmployeeTimeClockResource>this.$resource(
                this.basePath + 'api/Services/TimeClock/Supervisor/:id', { id: '@id' },
                {
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/TimeClock/Supervisor'
                    }
                });
        }

        /**
        * @description Resource object for global employee time clock
        */
        private employeeTimeClockGlobalResource = (): IEmployeeTimeClockResource => {

            return <IEmployeeTimeClockResource>this.$resource(
                this.basePath + 'api/Services/TimeClock/Global/:id', { id: '@id' },
                {
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/TimeClock/Global'
                    }
                });
        }
    }

    angular.module('app').service('chaitea.financials.services.reportssvc', ReportsSvc);
}

