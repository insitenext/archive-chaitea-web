﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.Invoices.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import invoicesInterfaces = ChaiTea.Financials.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    class InvoicesEntryViewCtrl {
        invoiceId: number = 0;
        basePath: string;

        invoiceNumber: number;
        invoiceDate: string;
        paymentTerms: string = "";
        PO: string = "";
        billedAmount: number = 0.00;
        isPaid: string ="UNPAID";
        taxAmount: number = 0;
        monthOfService: string = "";
        paidAmount: number = 0.00;
        invoiceAmount: number = 0.00;
        outstandingAmount: number = 0.00;
        invoiceItems: Array<Interfaces.IInvoiceItem>;
        customer: Interfaces.ICustomerDetails;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.services.lookuplist',
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
        }

        initialize = (invoiceId: number): void=> {
            this.invoiceId = invoiceId;


            this.getInvoiceDetails(invoiceId);
        }

        private getInvoiceDetails = (invoiceId) => {
            var base = this.basePath;

            this.apiSvc.getById(invoiceId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Invoice]).then((invoice: Interfaces.IInvoice) => {
                this.invoiceNumber = invoice.InvoiceNumber;
                this.customer = invoice.Customer;
                this.billedAmount = invoice.BilledAmount;
                this.invoiceDate =  (invoice.InvoiceDate ? moment(invoice.InvoiceDate).format('MM/DD/YYYY') : 'N/A');
                this.paymentTerms = invoice.PaymentTermsTypeName;
                this.PO = invoice.PO;
                this.taxAmount = invoice.TaxAmount;
                this.monthOfService = invoice.MonthOfService;
                this.paidAmount = invoice.PaidAmount;
                this.invoiceAmount = invoice.InvoiceAmount;
                this.outstandingAmount = invoice.OutstandingAmount;
                if (invoice.OutstandingAmount == 0)
                { this.isPaid = 'PAID'; }         
                this.invoiceItems = [];
                angular.forEach(invoice.InvoiceItems,(invoiceItem: Interfaces.IInvoiceItem) => {
                 
                    this.invoiceItems.push(<Interfaces.IInvoiceItem>{
                        Amount: invoiceItem.Amount,
                        CurrencyCodeId: invoiceItem.CurrencyCodeId,
                        CurrencyCodeName: invoiceItem.CurrencyCodeName,
                        Description: invoiceItem.Description,
                        InvoiceId: invoiceItem.InvoiceId,
                        InvoiceItemId: invoiceItem.InvoiceItemId,
                        IsPaid: invoiceItem.IsPaid,
                        IsTaxable: invoiceItem.IsTaxable,
                        LineNumber: invoiceItem.LineNumber,
                        Quantity: invoiceItem.Quantity,
                        Rate: invoiceItem.Rate,
                        ProgramId: invoiceItem.ProgramId,
                        ProgramName: invoiceItem.ProgramName,
                        TotalAmount: invoiceItem.TotalAmount
                    });
                });
            },
                //error : 404 
                function (response) {
                    window.location.replace(base + 'Financials/Invoices')
                })
            return this.invoiceId;
        }

    }

    angular.module("app").controller("Financials.InvoicesEntryViewCtrl", InvoicesEntryViewCtrl);
} 