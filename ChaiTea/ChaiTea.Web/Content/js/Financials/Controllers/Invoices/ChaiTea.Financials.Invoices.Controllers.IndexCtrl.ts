﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.Invoices.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import invoicesInterfaces = ChaiTea.Financials.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    class InvoicesIndexCtrl {
        invoiceId: number = 0;
        basePath: string;

        invoices = [];
        totalInvoiceAmount: number = 0.00;
        totalOutstandingAmount: number = 0.00;
        displayTotalInvoice: string;
        displayTotalOutstanding: string;
        top: number = 20;
        recordstoSkip: number = 0;

        isBusy: boolean = false;
        isAllLoaded: boolean = false;

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc) {

            this.basePath = sitesettings.basePath;

            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                //Reset the paging
                this.getInvoicesReset({
                    startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                    endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
                });
            });
        }

        initialize = (): void => {

        }

        showDetailsForInvoice = (invoiceId: number): void => {
            window.location.href = this.basePath + 'Financials/Invoices/EntryView/' + invoiceId;
        }

        getInvoicesReset = (dateRange: commonInterfaces.IDateRange): void => {
            this.totalInvoiceAmount = 0;
            this.totalOutstandingAmount = 0;
            this.invoices = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordstoSkip = 0;
            this.getInvoices(dateRange);
        }

        getInvoices = (dateRange: commonInterfaces.IDateRange): void => {
            
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = this.getPagingParams(dateRange);

            this.apiSvc.getByOdata(params, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.Invoice]).then((invoice): any => {
                if (invoice.length == 0) { // Do not make any more requests if there's no more data to load.
                    this.isAllLoaded = true;
                }
                angular.forEach(invoice,(obj) => {
                  
                    this.totalInvoiceAmount += obj.InvoiceAmount;
                    this.totalOutstandingAmount += obj.OutstandingAmount;
                    obj.InvoiceAmount = obj.InvoiceAmount.toFixed(2);
                    this.invoices.push(obj);
                });
                this.isBusy = false;
               
                this.recordstoSkip += this.top;
                this.displayTotalInvoice = this.totalInvoiceAmount.toFixed(2);
                this.displayTotalOutstanding = this.totalOutstandingAmount.toFixed(2);
            })
        }

        getPagingParams = (dateRange: commonInterfaces.IDateRange) => {
            var params = <IODataPagingParamModel>{
                $orderby: 'InvoiceDate desc',
                $top: this.top,
                $skip: this.recordstoSkip,
                $filter: `InvoiceDate gt DateTime'${dateRange.startDate}' and InvoiceDate le DateTime'${dateRange.endDate}'`
            };
            return params;
        }

        getInvoiceScroll = () => {
            this.getInvoices({
                startDate: this.dateFormatterSvc.formatDateFull(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateFull(this.dateRange.endDate)
            });
        }
    }

    angular.module("app").controller("Financials.InvoicesIndexCtrl", InvoicesIndexCtrl);
} 