﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.TimeClock.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import financialInterfaces = ChaiTea.Financials.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import financialsSvc = ChaiTea.Financials.Services;

    enum FileType { Undefined, ProfilePicture };

    class TimeClockOverviewCtrl {
        basePath: string;
        modalInstance;
        employeeId: number;
        selectedEmployeeId: number = 0;
        isTimeClockViewer: boolean;

        dateRange: { date: Date; month: number; year: number; startDate: Date; endDate: Date; } = {
            date: moment().toDate(),
            month: moment().month() + 1,
            year: moment().year(),
            startDate: moment(new Date()).startOf('month').toDate(),
            endDate: moment(new Date()).endOf('month').toDate()
        };

        view = 'month';
        currentDay = new Date();
        currentSelectionDate: string = '';
        firstSelectionDate: string = '';
        events: Array<financialInterfaces.ICalendarEvent> = [];

        showStraightTimeEquivalent: boolean = true;

        monthlyTimeClock: Array< financialInterfaces.ITimeClock > = [];
        timeClocks: Array<financialInterfaces.ITimeClock> = [];
        dayAggregates: Array<financialInterfaces.IDayAggregate> = [];
        totalHours = {
            RegularHours: 0,
            STEHours: 0
        }

        employeesWithAttachment: Array<financialInterfaces.IEmployee> = [];
        employeesWithTimeClock: Array<financialInterfaces.ITimeClock> =[];
        timeClock = <financialInterfaces.ITimeClock>{};

        daySelected: boolean = false;
        refreshCount: number = 0;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.localdatastoresvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private chartsSvc: financialsSvc.IReportsSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;
            this.employeeId = userInfo.userId;
        }

        initialize = (): void => {
            this.isTimeClockViewer = this.canViewAllEmployeesTimeClock();

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate);
            }
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.dateRange.date = moment(obj.endDate).toDate();
                this.dateRange.month = moment(obj.endDate).month() + 1;
                this.dateRange.year = moment(obj.endDate).year();
                this.dateRange.startDate = moment(obj.endDate).startOf('month').toDate(),
                this.dateRange.endDate = moment(obj.endDate).endOf('month').toDate();

                this.currentDay = this.dateRange.date;

                this.getEmployees();
                this.getMonthlyTimeClockOfAllEmployees();

                this.employeesWithTimeClock = [];
                
            });
        }

        /**
        * @description Returns whether the user is a timeclock viewer. 
        */
        private canViewAllEmployeesTimeClock = (): boolean => {
            return this.userInfo.mainRoles.timeclockviewers;
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        setCalendarEvents = () => {
            this.refreshCount += 1;
            if (!this.selectedEmployeeId) {
                this.events = [];
                angular.forEach(this.monthlyTimeClock, (monthlyTimeClockItem) => {
                    this.events.push(<financialInterfaces.ICalendarEvent>{
                        title: 'Working Hours',
                        type: 'success',
                        startsAt: moment(monthlyTimeClockItem.Day).toDate(),
                        incrementsBadgeTotal: false,
                        aggregateHours: this.showStraightTimeEquivalent ? Math.round(monthlyTimeClockItem.TotalSTEHours * 100) / 100 : Math.round(monthlyTimeClockItem.TotalHours * 100) / 100,
                        drillDown: false,
                        refreshCount: this.refreshCount
                    });
                });
            }
            else {
                this.events = [];
                angular.forEach(this.timeClocks, (timeclock) => {
                    this.events.push(<financialInterfaces.ICalendarEvent>{
                        title: 'Working Hours',
                        type: 'success',
                        startsAt: moment(timeclock.Day).toDate(),
                        incrementsBadgeTotal: false,
                        totalHours: this.showStraightTimeEquivalent ? Math.round(timeclock.StraightTimeEquivalent * 100) / 100 : Math.round(timeclock.TotalTime * 100) / 100,
                        aggregateHours: this.getAggregateDayHours(timeclock.Day),
                        startTime: timeclock.StartTime,
                        endTime: timeclock.EndTime,
                        drillDown: false,
                        refreshCount: this.refreshCount
                    });
                });
            }

        }

        
        //to get monthly time clock data for all employees
        getMonthlyTimeClockOfAllEmployees = (): void => {
            this.monthlyTimeClock = [];

            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                supervisorId: this.employeeId
            }

            if (this.isTimeClockViewer) {
                params.supervisorId = null
            }

            this.chartsSvc.getMonthlyTimeClockOfAllEmployees(params).then((data) => {
                this.monthlyTimeClock = data;
                this.setCalendarEvents();
            }, this.onFailure);
        }

        /**
        * @description get employees based on logged in user role
        */
        getEmployees = (): void => {
            var pagingParams = this.getPaginParamsToGetEmployees();
            this.apiSvc.getByOdata(pagingParams, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees): any => {

                angular.forEach(employees, (employee) => {
                    var timeClockObj = <financialInterfaces.ITimeClock>{};
                    timeClockObj.EmployeeName = employee.Name;
                    timeClockObj.EmployeeId = employee.EmployeeId;
                    timeClockObj.EmployeeJobDesc = employee.JobDescription ? employee.JobDescription.Name : '';
                    timeClockObj.EmployeeImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
                    this.employeesWithTimeClock.push(timeClockObj);
                });
            }, this.onFailure);
        }

        getAllEmployeesHoursForSelectedDates = (day: string): void => {
            this.daySelected = true;

            var clickedDay = this.formatDate(moment(day).format('YYYY/MM/DD'));
            var params = {
                startDate: clickedDay,
                endDate: clickedDay,
            }

            if (this.firstSelectionDate > clickedDay) {
                if (clickedDay >= this.currentSelectionDate) {
                    this.currentSelectionDate = this.firstSelectionDate;
                } 
                this.firstSelectionDate = clickedDay;
                params.endDate = this.currentSelectionDate;
            }
            else {
                if (this.firstSelectionDate == clickedDay || this.currentSelectionDate == clickedDay) {
                    this.resetDaySelectionFlags();
                } else {
                    if (!this.firstSelectionDate) {
                        this.firstSelectionDate = clickedDay;
                    }
                    this.currentSelectionDate = clickedDay;
                    params.startDate = this.firstSelectionDate;
                }
            }
            

            if (this.daySelected) {
                if (this.isTimeClockViewer) {
                    this.apiSvc.query(params, 'TimeClock/Global/:startDate/:endDate', false, false).then((data): any => {
                        this.assignEmployeesWithTimeClockData(data);
                    }, this.onFailure);
                }
                else {
                    this.apiSvc.query(params, 'TimeClock/Supervisor/:startDate/:endDate', false, false).then((data): any => {
                        this.assignEmployeesWithTimeClockData(data);
                    }, this.onFailure);
                }
            }
            
        }

        assignEmployeesWithTimeClockData = (data: financialInterfaces.ITimeClockWithTotals) => {
            angular.forEach(this.employeesWithTimeClock, (employeeWithTimeClock) => {

                var employeeTimeclocks = _.filter(data.Data, (e: financialInterfaces.ITimeClock) => { return e.EmployeeId == employeeWithTimeClock.EmployeeId; });

                var timeClock = <financialInterfaces.ITimeClock>{};
                timeClock.TotalTime = 0;
                timeClock.StraightTimeEquivalent = 0;

                angular.forEach(employeeTimeclocks, (data) => {
                    timeClock.TotalTime += data.TotalTime;
                    timeClock.StraightTimeEquivalent += data.StraightTimeEquivalent;
                });

                employeeWithTimeClock.EndTime = employeeTimeclocks.length == 1 ? employeeTimeclocks[0].EndTime : null;
                employeeWithTimeClock.StartTime = employeeTimeclocks.length == 1 ? employeeTimeclocks[0].StartTime : null;
                employeeWithTimeClock.TotalTime = timeClock ? Math.round(timeClock.TotalTime * 100) / 100 : 0;
                employeeWithTimeClock.StraightTimeEquivalent = timeClock ? Math.round(timeClock.StraightTimeEquivalent * 100) / 100 : 0;
            });

            if (this.selectedEmployeeId) {
                var clickedDayData = _.find(this.employeesWithTimeClock, (e) => { return e.EmployeeId == this.selectedEmployeeId; });
                this.totalHours.RegularHours = clickedDayData.TotalTime;
                this.totalHours.STEHours = clickedDayData.StraightTimeEquivalent;
            } else {
                this.totalHours.RegularHours = data.TotalOfTotalTime;
                this.totalHours.STEHours = data.TotalOfSTE;
            }
        }

        //to toggle between regular and STE hours
        toggleRegularAndSTEHours = () => {
            this.showStraightTimeEquivalent ? this.showStraightTimeEquivalent = false : this.showStraightTimeEquivalent = true;
            if (this.selectedEmployeeId) {
                this.calculateAggregateDayHours(this.timeClocks);
            }
            this.resetDaySelectionFlags();
            this.setCalendarEvents();
        }

        getPaginParamsToGetEmployees = () => {
            if (this.isTimeClockViewer) {
                var params = <financialInterfaces.IODataPagingParamModel>{
                    $orderby: 'Name asc',
                };
            }
            else {
                var params = <financialInterfaces.IODataPagingParamModel>{
                    $orderby: 'Name asc',
                    $filter: 'SupervisorId eq ' + this.employeeId
                };
            }
            return params;
        }

        //to get selected employee calendar 
        getEmployeeTimeClockData = (empId: number): void => {

            if (this.selectedEmployeeId == empId) {
                this.selectedEmployeeId = null;
            }
            else {
                this.selectedEmployeeId = empId;
                this.resetDaySelectionFlags();
            }

            //var clickedDayData = _.find(this.employeesWithTimeClock, (e) => { return e.EmployeeId == empId; });
            //this.totalHours.RegularHours = clickedDayData.RegularTime;
            //this.totalHours.STEHours = clickedDayData.StraightTimeEquivalent;

            var params = {
                startDate: this.formatDate(moment(this.dateRange.startDate).format('YYYY/MM/DD')),
                endDate: this.formatDate(moment(this.dateRange.endDate).format('YYYY/MM/DD')),
                employeeId: this.selectedEmployeeId
            }
            this.timeClocks = [];
            if (this.selectedEmployeeId) {

                if (this.isTimeClockViewer) {
                    this.apiSvc.query(params, 'TimeClock/Global/:startDate/:endDate/:employeeId', false, false).then((data): any => {
                        this.calculateAggregateDayHours(data.Data);
                        this.timeClocks = data.Data;

                        this.setCalendarEvents();
                    }, this.onFailure);
                }
                else {
                    this.apiSvc.query(params, 'TimeClock/Supervisor/:startDate/:endDate/:employeeId', false, false).then((data): any => {
                        this.calculateAggregateDayHours(data.Data);
                        this.timeClocks = data.Data;

                        this.setCalendarEvents();
                    }, this.onFailure);
                }
            }
            else {
                this.setCalendarEvents();
                this.resetDaySelectionFlags();
            }


        }

        resetDaySelectionFlags = () => {
            this.daySelected = false;
            this.firstSelectionDate = '';
            this.currentSelectionDate = '';
        }

        // to calculate aggregate total hours of each day
        calculateAggregateDayHours = (data: Array<financialInterfaces.ITimeClock>) => {
            if (!data.length)
                return;

            this.dayAggregates = [];
            var previousDate = data[0].Day;
            var aggregateHrs = 0;
            angular.forEach(data, (timeclock) => {
                if (timeclock.Day == previousDate) {
                    aggregateHrs = aggregateHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                }
                else {
                    this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                        day: previousDate,
                        aggregateHours: aggregateHrs
                    });
                    previousDate = timeclock.Day;
                    aggregateHrs = this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime;
                }
            });
            //to push last date aggregate
            this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                day: previousDate,
                aggregateHours: aggregateHrs
            });
        }

        //to get aggregate total hours of a particular day
        getAggregateDayHours = (date: Date): number => {

            for (var i = 0; i < this.dayAggregates.length; i++) {
                if (this.dayAggregates[i].day == date) {
                    return this.dayAggregates[i].aggregateHours;
                }
            }
        }


        formatDate = (sdate : string) => {
            var d = new Date(sdate);
            var date = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            var month_string = '';
            var date_string = '';
            if (month < 10)
                month_string = "0" + month;
            else
                month_string = month.toString();
            if (date < 10)
                date_string = "0" + date;
            else
                date_string = date.toString();

            var curr_date_format = year + "-" + month_string + "-" + date_string;

            return curr_date_format;
        }
    }

    angular.module("app").controller("Financials.TimeClockOverviewCtrl", TimeClockOverviewCtrl);
} 