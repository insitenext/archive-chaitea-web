﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.TimeClock.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import financialInterfaces = ChaiTea.Financials.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import financialSvc = ChaiTea.Financials.Services;

    interface IMonthTab {
        name: string,
        longName: string,
        index: number,
        value: number,
        year: number
    }

    class TimeClockIndexCtrl {
        basePath: string;
        modalInstance;
        userId: number;
        siteId: number;
        isTimeClockViewer: boolean = false; 

        dateRangeGraph = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').format(),
            endDate: new Date()
        };
        noOfMonthsSelected = 6;
        hours = {
            labels: [],
            regularHours: [],
            STEHours: [],
            overTimeHours: [],
            doubleTimeHours: [],
            totalRegularHours: 0,
            totalDoubleTimeHours: 0,
            totoalOverTimeHours: 0,
            totalHours: 0,
            totalSTEHours: 0
        }
        lastUpdatedDate: string;
        reportsConfig = {
            regularHours: {},
            STEHours: {},
        };       
        desktopMonthTabs: Array<IMonthTab> = [];
        mobileMonth: any;
        mobileMonthTabs: Array<IMonthTab> = [];
        currentMonthTab: IMonthTab;
        activeTabIndex: number = 0;
        currentMonth: string;
        totalMonthlyHours: number = 0;
        showSeletedTimeEquivalent: boolean = false;
        view = 'month';
        showStraightTimeEquivalent: boolean = true;
        selectedEmployeeId: number = 0;
        timeClocks: Array<financialInterfaces.ITimeClock> = [];
        dayAggregates: Array<financialInterfaces.IDayAggregate> = [];
        daySelected: boolean = false;
        firstSelectionDate: string = '';
        currentSelectionDate: string = '';
        refreshCount: number = 0;
        events: Array<financialInterfaces.ICalendarEvent> = [];
        monthlyTimeClock: Array<financialInterfaces.ITimeClock> = [];
        employeeId: number;
        dateRange: { date: Date; month: number; year: number; startDate: Date; endDate: Date; } = {
            date: moment().toDate(),
            month: moment().month() + 1,
            year: moment().year(),
            startDate: moment(new Date()).startOf('month').toDate(),
            endDate: moment(new Date()).endOf('month').toDate()
        };
        currentDay = new Date();
        employeesWithTimeClock: Array<financialInterfaces.ITimeClock> = [];
        totalHours = {
            RegularHours: 0,
            STEHours: 0
        }
        sitesettings: ISiteSettings;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.imagesvc',
            '$log',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.localdatastoresvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: financialSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $log: angular.ILogService,
            private notificationSvc: commonSvc.INotificationSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc) {

            this.sitesettings = sitesettings;
            this.basePath = sitesettings.basePath;
            this.userId = userInfo.userId;
            this.siteId = userContext.Site.ID;            
            this.modalInstance = this.$uibModal;
            this.employeeId = userInfo.userId;
            this.reportsConfig.regularHours = this.chartConfigBuilder('regularHours', {});
            this.reportsConfig.STEHours = this.chartConfigBuilder('STEHours', {});

            // This sets the straight/regular time hours section
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.HeaderDateChange, this.$scope, (event, obj) => {
                this.dateRangeGraph.startDate = obj.startDate;
                this.dateRangeGraph.endDate = obj.endDate;
                this.noOfMonthsSelected = Math.abs(moment(this.dateRangeGraph.startDate).diff(moment(this.dateRangeGraph.endDate), 'months')) + 1;
                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });

            });
        }

        /**
        * @description Called when page loads
        */
        initialize = (tabIndex: number): void => {
            this.showStraightTimeEquivalent = !tabIndex;
            this.isTimeClockViewer = this.CanViewAllEmployeesTimeClock();
            var currentDate = new Date();
            this.desktopMonthTabs = this.buildTabArray(moment().month(), moment().year());
            this.currentMonthTab = {
                name: moment().format("MMM"),
                longName: moment().format("MMMM"),
                index: 11,
                value: moment().month(),
                year: moment().year()
            };
            this.activeTabIndex = 11;
            this.mobileMonth = this.desktopMonthTabs[11];
            this.buildCalendar(currentDate);
        }

        buildTabArray = (month: number, year: number): any => {            
            var tabArray = new Array<IMonthTab>();
            var nextMonth = month;
            var i = 0;
            year = year - 1;

            while (i < 12) {
                nextMonth++;                
                var monthName = moment().month(nextMonth).format("MMM");
                var monthNameLong = moment().month(nextMonth).format("MMMM");

                if (nextMonth > 11) {
                    nextMonth = 0;
                    year++;
                }

                if (nextMonth === month) {                    
                    monthName = monthName + " '" + year.toString().substring(2, 4);                     
                }
                monthNameLong = monthNameLong + " '" + year.toString().substring(2, 4);                   
                
                var desktopTab = {
                    name: monthName,
                    longName: monthNameLong,
                    index: i,
                    value: nextMonth,
                    year: year
                };
                tabArray.push(desktopTab);                                
                i++;
            }
            return tabArray;
        }

        selectMonthChange = () => {
            this.tabMonthClick(null, this.mobileMonth.value, this.mobileMonth.year, this.mobileMonth.index);
        }

        /**
        * @description Builds calendar based on tab selection
        */
        tabMonthClick = ($event: JQueryEventObject, month: number, year: number, index: number) => {  
            if ($event) {
                $event.preventDefault();
            }
            var selectedDate = new Date(year, month, 1);                                     
            this.activeTabIndex = index;   
            this.mobileMonth = this.desktopMonthTabs[index];         
            this.buildCalendar(selectedDate);
        }
        
        /**
        * @description Sets daily hours and totals for the month
        */
        buildCalendar = (selectedDate: Date) => {
            this.dateRange.date = moment(selectedDate).toDate();
            this.dateRange.month = (moment(selectedDate).month()) + 1;
            this.dateRange.year = moment(selectedDate).year();
            this.dateRange.startDate = moment(selectedDate).startOf('month').toDate();
            this.dateRange.endDate = moment(selectedDate).endOf('month').toDate();                        
            this.currentDay = this.dateRange.date;
            this.currentMonth = moment(selectedDate).format("MMMM");
            this.getEmployees();
            this.getMonthlyTimeClockOfAllEmployees();
            this.employeesWithTimeClock = [];
            if (this.selectedEmployeeId) {
                this.getEmployeeTimeClockData();
            }
        }

        /**
       * @description get employees based on logged in user role
       */
        getEmployees = (): void => {
            var pagingParams = this.getPagingParamsToGetEmployees();
            this.apiSvc.getByOdata(pagingParams, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employees): any => {

                angular.forEach(employees, (employee) => {
                    var timeClockObj = <financialInterfaces.ITimeClock>{};
                    timeClockObj.EmployeeName = employee.Name;
                    timeClockObj.EmployeeId = employee.EmployeeId;
                    timeClockObj.EmployeeJobDesc = employee.JobDescription ? employee.JobDescription.Name : '';
                    timeClockObj.EmployeeImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);
                    this.employeesWithTimeClock.push(timeClockObj);
                });
            }, this.onFailure);
        }
        
        /**
        * @description Get monthly time clock data for all employees.
        */
        getMonthlyTimeClockOfAllEmployees = (): void => {
            this.monthlyTimeClock = [];
            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                supervisorId: this.employeeId
            }

            if (this.isTimeClockViewer) {
                params.supervisorId = null
            }

            this.chartsSvc.getMonthlyTimeClockOfAllEmployees(params).then((data) => {
                this.monthlyTimeClock = data;
                this.setCalendarEvents();
            }, this.onFailure);
        }

        /**
        * @description Toggle between regular and STE hours.
        */
        toggleRegularAndSTEHours = () => {
            this.showStraightTimeEquivalent = !this.showStraightTimeEquivalent;           
            this.refreshCharts({
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRangeGraph.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRangeGraph.endDate)
            });

            if (this.selectedEmployeeId) {
                this.calculateAggregateDayHours(this.timeClocks);
            }
            this.resetDaySelectionFlags();
            this.setCalendarEvents();
        }
        
        /**
        * @description Calculate aggregate total hours of each day.
        */
        calculateAggregateDayHours = (data: Array<financialInterfaces.ITimeClock>) => {
            if (!data.length)
            { return; }

            this.dayAggregates = [];
            var previousDate = data[0].Day;
            var aggregateHrs = 0;
            angular.forEach(data, (timeclock) => {
                if (timeclock.Day == previousDate) {
                    aggregateHrs = aggregateHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                }
                else {
                    this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                        day: previousDate,
                        aggregateHours: aggregateHrs
                    });
                    previousDate = timeclock.Day;
                    aggregateHrs = this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime;
                }
            });
            //to push last date aggregate
            this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                day: previousDate,
                aggregateHours: aggregateHrs
            });
        }

        /**
        * @description Clear shared logic flags.
        */
        resetDaySelectionFlags = () => {
            this.daySelected = false;
            this.firstSelectionDate = '';
            this.currentSelectionDate = '';
        }

        /**
        * @description Sets calendar totals for STE and regular time daily.
        */
        setCalendarEvents = () => {
            this.refreshCount += 1;
            this.totalMonthlyHours = 0;
            if (!this.selectedEmployeeId) {
                this.events = [];
                angular.forEach(this.monthlyTimeClock, (monthlyTimeClockItem) => {
                    var aggregateHours = this.showStraightTimeEquivalent ? Math.round(monthlyTimeClockItem.TotalSTEHours * 100) / 100 : Math.round(monthlyTimeClockItem.TotalHours * 100) / 100;
                    this.totalMonthlyHours += aggregateHours;
                    this.events.push(<financialInterfaces.ICalendarEvent>{
                        title: 'Working Hours',
                        type: 'success',
                        startsAt: moment(monthlyTimeClockItem.Day).toDate(),
                        incrementsBadgeTotal: false,
                        aggregateHours: aggregateHours,
                        drillDown: false,
                        refreshCount: this.refreshCount
                    });
                });
            }
            else {
                this.events = [];
                var previousTimeclockDay;
                angular.forEach(this.timeClocks, (timeclock) => {
                    if (previousTimeclockDay && previousTimeclockDay == timeclock.Day) {
                        return;
                    }
                    previousTimeclockDay = timeclock.Day;
                    this.totalMonthlyHours += this.getAggregateDayHours(timeclock.Day);
                    this.events.push(<financialInterfaces.ICalendarEvent>{
                        title: 'Working Hours',
                        type: 'success',
                        startsAt: moment(timeclock.Day).toDate(),
                        incrementsBadgeTotal: false,
                        totalHours: this.showStraightTimeEquivalent ? Math.round(timeclock.StraightTimeEquivalent * 100) / 100 : Math.round(timeclock.TotalTime * 100) / 100,
                        aggregateHours: this.getAggregateDayHours(timeclock.Day),
                        startTime: timeclock.StartTime,
                        endTime: timeclock.EndTime,
                        drillDown: false,
                        refreshCount: this.refreshCount
                    });
                });
            }

        }
        
        /**
        * @description Get aggregate total hours of a particular day.
        */
        getAggregateDayHours = (date: Date): number => {

            for (var i = 0; i < this.dayAggregates.length; i++) {
                if (this.dayAggregates[i].day == date) {
                    return this.dayAggregates[i].aggregateHours;
                }
            }
        }

        /**
        * @description Called on service failure.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
        * @description Gets params for oData call.
        */
        getPagingParamsToGetEmployees = () => {
            if (this.isTimeClockViewer) {
                var params = <financialInterfaces.IODataPagingParamModel>{
                    $orderby: 'Name asc',
                };
            }
            else {
                var params = <financialInterfaces.IODataPagingParamModel>{
                    $orderby: 'Name asc',
                    $filter: 'SupervisorId eq ' + this.employeeId
                };
            }
            return params;
        }

        /**
        * @description Gets employee hours for selected dates.
        */
        getAllEmployeesHoursForSelectedDates = (day: string): void => {
            this.daySelected = true;

            var clickedDay = this.formatDate(moment(day).format('YYYY/MM/DD'));
            var params = {
                startDate: clickedDay,
                endDate: clickedDay,
            }

            if (this.firstSelectionDate > clickedDay) {
                if (clickedDay >= this.currentSelectionDate) {
                    this.currentSelectionDate = this.firstSelectionDate;
                }
                this.firstSelectionDate = clickedDay;
                params.endDate = this.currentSelectionDate;
            }
            else {
                if (this.firstSelectionDate == clickedDay || this.currentSelectionDate == clickedDay) {
                    this.resetDaySelectionFlags();
                } else {
                    if (!this.firstSelectionDate) {
                        this.firstSelectionDate = clickedDay;
                    }
                    this.currentSelectionDate = clickedDay;
                    params.startDate = this.firstSelectionDate;
                }
            }

            if (this.daySelected) {
                if (this.isTimeClockViewer) {
                    this.apiSvc.query(params, 'TimeClock/Global/:startDate/:endDate', false, false).then((data): any => {
                        this.assignEmployeesWithTimeClockData(data);
                    }, this.onFailure);
                }
                else {
                    this.apiSvc.query(params, 'TimeClock/Supervisor/:startDate/:endDate', false, false).then((data): any => {
                        this.assignEmployeesWithTimeClockData(data);
                    }, this.onFailure);
                }
            }

        }

        formatDate = (sdate: string) => {
            var d = new Date(sdate);
            var date = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            var month_string = '';
            var date_string = '';
            if (month < 10) {
                month_string = "0" + month;
            } else {
                month_string = month.toString();
            }
            if (date < 10) {
                date_string = "0" + date;
            } else {
                date_string = date.toString();
            }
            var curr_date_format = year + "-" + month_string + "-" + date_string;

            return curr_date_format;
        }

        assignEmployeesWithTimeClockData = (data: financialInterfaces.ITimeClockWithTotals) => {
            angular.forEach(this.employeesWithTimeClock, (employeeWithTimeClock) => {

                var employeeTimeclocks = _.filter(data.Data, (e: financialInterfaces.ITimeClock) => { return e.EmployeeId == employeeWithTimeClock.EmployeeId; });

                var timeClock = <financialInterfaces.ITimeClock>{};
                timeClock.TotalTime = 0;
                timeClock.StraightTimeEquivalent = 0;

                angular.forEach(employeeTimeclocks, (data) => {
                    timeClock.TotalTime += data.TotalTime;
                    timeClock.StraightTimeEquivalent += data.StraightTimeEquivalent;
                });

                employeeWithTimeClock.EndTime = employeeTimeclocks.length == 1 ? employeeTimeclocks[0].EndTime : null;
                employeeWithTimeClock.StartTime = employeeTimeclocks.length == 1 ? employeeTimeclocks[0].StartTime : null;
                employeeWithTimeClock.TotalTime = timeClock ? Math.round(timeClock.TotalTime * 100) / 100 : 0;
                employeeWithTimeClock.StraightTimeEquivalent = timeClock ? Math.round(timeClock.StraightTimeEquivalent * 100) / 100 : 0;
            });

            if (this.selectedEmployeeId) {
                var clickedDayData = _.find(this.employeesWithTimeClock, (e) => { return e.EmployeeId == this.selectedEmployeeId; });
                this.totalHours.RegularHours = clickedDayData.TotalTime;
                this.totalHours.STEHours = clickedDayData.StraightTimeEquivalent;
            } else {
                this.totalHours.RegularHours = data.TotalOfTotalTime;
                this.totalHours.STEHours = data.TotalOfSTE;
            }
        }

        /**
        * @description Toggle the selectedEmployeeId
        */
        toggleSelectedEmployee = (empId: number): void => {
            if (this.selectedEmployeeId == empId) {
                this.selectedEmployeeId = null;
            } else {
                this.selectedEmployeeId = empId;
                this.resetDaySelectionFlags();
            }
            this.getEmployeeTimeClockData();
        }

        /**
        * @description Gets selected employee calendar.
        */
        getEmployeeTimeClockData = (): void => {

            var params = {
                startDate: this.formatDate(moment(this.dateRange.startDate).format('YYYY/MM/DD')),
                endDate: this.formatDate(moment(this.dateRange.endDate).format('YYYY/MM/DD')),
                employeeId: this.selectedEmployeeId
            }
            this.timeClocks = [];
            if (this.selectedEmployeeId) {

                if (this.isTimeClockViewer) {
                    this.apiSvc.query(params, 'TimeClock/Global/:startDate/:endDate/:employeeId', false, false).then((data): any => {
                        this.calculateAggregateDayHours(data.Data);
                        this.timeClocks = data.Data;

                        this.setCalendarEvents();
                    }, this.onFailure);
                }
                else {
                    this.apiSvc.query(params, 'TimeClock/Supervisor/:startDate/:endDate/:employeeId', false, false).then((data): any => {
                        this.calculateAggregateDayHours(data.Data);
                        this.timeClocks = data.Data;

                        this.setCalendarEvents();
                    }, this.onFailure);
                }
            }
            else {
                this.setCalendarEvents();
                this.resetDaySelectionFlags();
            }
        }
        
        /**
        * @description Returns whether the user is a timeclock viewer. 
        */
        private CanViewAllEmployeesTimeClock = (): boolean => {
            return this.userInfo.mainRoles.timeclockviewers;
        }

        getLastUpdateDate = (): void => {
            this.apiSvc.getByOdata(null, 'TimeClock/LastUpdateDate', false, false).then((result) => {
                this.lastUpdatedDate = result.LastUpdateDate;
            });
        }
        getHours = (dateRange: commonInterfaces.IDateRange) => {
            var params = this.getParams(dateRange);
            this.apiSvc.getByOdata(params, 'FinanceReport/HourMonthlyTrends/:startDate/:endDate/:supervisorId', false, false).then((result) => {
                if (!result) return;
                this.hours.labels = result.Labels;
                this.hours.regularHours = result.RegularHours;
                this.hours.STEHours = result.STEHours;
                this.hours.doubleTimeHours = result.DoubleTimeHours;
                this.hours.overTimeHours = result.OverTimeHours
                this.hours.totalSTEHours = result.TotalSTEHours;
                this.hours.totalRegularHours = result.TotalRegularHours;
                this.hours.totoalOverTimeHours = result.TotalOverTimeHours;
                this.hours.totalHours = result.TotalHours;
                this.hours.totalDoubleTimeHours = result.TotalDoubleTimeHours;

                this.showRegularHours();
                this.showSTEHours();
            });
        }
        showRegularHours = () => {
            var self = this;
                 // Build the chart
                this.reportsConfig.regularHours = this.chartConfigBuilder('regularHours', {
                    xAxis: {
                        tickColor: 'white',
                        categories: this.hours.labels,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.dark,
                                fontSize: '12px',
                                fontWeight: '400',
                                'font-family': 'Open Sans'
                            }
                        }
                    },
                    yAxis: {
                        stackLabels: {
                            enabled: false,
                            style: {
                                color: 'black',
                            }
                        },
                        title: null,
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.dark,
                                fontSize: '12px',
                                fontWeight: '600',
                                'font-family': 'Open Sans'
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            events: {
                                legendItemClick: (events) => {
                                    var selectedLegendObj = events.target;
                                    if (selectedLegendObj.visible) {
                                        switch (selectedLegendObj.index) {
                                            case 0:
                                                this.hours.totalHours -= this.hours.totalDoubleTimeHours;
                                                break;
                                            case 1:
                                                this.hours.totalHours -= this.hours.totoalOverTimeHours;
                                                break;
                                            case 2:
                                                this.hours.totalHours -= this.hours.totalRegularHours;
                                                break;
                                            default: return
                                        }
                                    }
                                    else {
                                        switch (selectedLegendObj.index) {
                                            case 0:
                                                this.hours.totalHours += this.hours.totalDoubleTimeHours;
                                                break;
                                            case 1:
                                                this.hours.totalHours += this.hours.totoalOverTimeHours;
                                                break;
                                            case 2:
                                                this.hours.totalHours += this.hours.totalRegularHours;
                                                break;
                                            default: return
                                        }
                                    }
                                    this.$scope.$apply();
                                }
                            }
                        }
                    },
                    series: [
                        {
                            name: 'REGULAR HOURS',
                            lineWidth: 3,
                            data: this.hours.regularHours,
                            index: 2,
                            legendIndex: 0
                        },
                        {
                            name: 'OVERTIME HOURS',
                            lineWidth: 3,
                            data: this.hours.overTimeHours,
                            index: 1,
                            legendIndex: 1
                        },
                        {
                            name: 'DOUBLE TIME HOURS',
                            lineWidth: 3,
                            data: this.hours.doubleTimeHours,
                            index: 0,
                            legendIndex: 2
                        }                                                                        
                    ],
                    legend: {
                        enabled: true,
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -15,
                        itemStyle: {
                            color: SiteSettings.colors.coreColors.dark,
                            fontSize: '10px',
                            fontWeight: '600',
                        }
                        
                    }
                });

        }

        showSTEHours = () => {
            // Build the chart
            this.reportsConfig.STEHours = this.chartConfigBuilder('STEHours', {
                yAxis: {
                    stackLabels: {
                        enabled: false,
                        style: {
                            color: 'black'
                        }
                    },
                    title: null,
                    labels: {
                        style: {
                            color: SiteSettings.colors.coreColors.dark,
                            fontSize: '12px',
                            fontWeight: '600',
                            'font-family': 'Open Sans'
                        }
                    }
                },
                xAxis: {
                    tickColor: 'white',
                    categories: this.hours.labels,
                    labels: {
                        style: {
                            color: SiteSettings.colors.coreColors.dark,
                            fontSize: '12px',
                            fontWeight: '400',
                            'font-family': 'Open Sans'
                        }
                    }
                },
                series: [
                    {
                        name: 'STE HOURS',
                        lineWidth: 3,
                        data: this.hours.STEHours
                    }
                ],
                categories: this.hours.labels,
                legend: {
                    enabled: false
                }
            });
        }

        getParams = (dateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                supervisorId: this.isTimeClockViewer ? '' : this.userId
            }
            return params;
        }
        /**
        * @description Refresh the graph data.
        * @description Call the data service and fetch the data based on params.
        */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getHours(dateRange);
            this.getLastUpdateDate();
        }

        /**
        * @description Defines overridable chart options.
        */
        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },
            });

            var chartOptions = {

                // Column Chart
                regularHours: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },

                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: 'white',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 5,
                        //min: 75,
                        //max: 100,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: SiteSettings.colors.coreColors.base,
                        plotLines: [
                            {
                                value: 98,
                                color: SiteSettings.colors.secondaryColors.blueSteel,
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.dark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    xAxis: {
                        tickColor: 'white',
                        labels: {
                            style: {
                                color: SiteSettings.colors.coreColors.dark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            color: SiteSettings.colors.secondaryColors.lime,
                            name: 'Completion',
                            lineWidth: 100
                        }
                    ]
                },

                // Area chart
                STEHours: {
                    categories: [],
                    series: [{}],
                    legend: {
                        enabled: false
                    }
                },

            }
            return _.assign(chartOptions[name], options);
        }
      
    }

    angular.module("app").controller("Financials.TimeClockIndexCtrl", TimeClockIndexCtrl);
} 