﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.TimeClock.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import financialInterfaces = ChaiTea.Financials.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import financialsSvc = ChaiTea.Financials.Services;

    enum FileType { Undefined, ProfilePicture };

    class TimeClockDetailsCtrl {
        basePath: string;
        modalInstance;
        employeeId: number;

        dateRange: { date: Date; month: number; year: number; startDate: Date; endDate: Date; } = {
            date: moment().toDate(),
            month: moment().month() + 1,
            year: moment().year(),
            startDate: moment(new Date()).startOf('month').toDate(),
            endDate: moment(new Date()).endOf('month').toDate()
        };

        view = 'month';
        currentDay = new Date();
        minDate = moment();
        events = [];

        employeeWithAttachment = <financialInterfaces.IEmployee>{
            EmployeeId: 0,
            HireDate: '',
            UserAttachments: null,
            JobDescription: '',
            Name: '',
            ProfileImage : ''
        };

        weeks = [];
        dayAggregates = []

        totalWeeksInMonth: number;
        active: number;
        showStraightTimeEquivalent: boolean = false;
        showSTEBtnText = 'SHOW STE HOURS'

        timeClock: financialInterfaces.ITimeClock;
        timeClocks = [];
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            '$modal',
            'chaitea.services.base',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.imagesvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private $log: angular.ILogService,
            private $modal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Services.IBaseSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private dateFormatterSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private chartsSvc: financialsSvc.IReportsSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc
        ) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$modal;
        }

        initialize = (id: number): void => {
            this.employeeId = id;
            this.active = 0;


            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.dateRange.endDate = new Date(dateSelection.endDate);
            }
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.monthpicker:datechange', this.$scope,(event, obj) => {
                this.dateRange.date = moment(obj.endDate).toDate();
                this.dateRange.month = moment(obj.endDate).month();
                this.dateRange.year = moment(obj.endDate).year();
                this.dateRange.startDate = moment(obj.endDate).startOf('month').toDate(),
                this.dateRange.endDate = moment(obj.endDate).endOf('month').toDate();

                this.currentDay = this.dateRange.date;
                this.totalWeeksInMonth = 0;
                this.getEmployee();
                this.getTimeClockData();
            });
        }

        /**
       * @description Returns whether the user is a timeclock viewer. 
       */
        private CanViewAllEmployeesTimeClock = (): boolean => {
            return this.userInfo.roles.indexOf('CanViewAllEmployeesTimeClock') >= 0;
        }

        /**
        * @description get  employee by id
        */
        getEmployee = (): void => {
            this.apiSvc.getById(this.employeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee): any => {

                this.employeeWithAttachment.JobDescription = employee.JobDescription.Name;
                this.employeeWithAttachment.Name = employee.Name;
                this.employeeWithAttachment.ProfileImage = this.imageSvc.getUserImageFromAttachments(employee.UserAttachments, true);

                console.log(this.employeeWithAttachment);
            });
        }

        setCalenderEvents = (): void => {
            this.events = [];
            angular.forEach(this.timeClocks,(timeclock) => {
                this.events.push(<financialInterfaces.ICalendarEvents>{
                    title: 'Working Hours',
                    type: 'success',
                    startsAt: moment(timeclock.Day).toDate(),
                    incrementsBadgeTotal: false,
                    totalHours: this.showStraightTimeEquivalent ? Math.round(timeclock.StraightTimeEquivalent * 100) / 100 : Math.round(timeclock.TotalTime * 100) / 100 ,
                    aggregateHours: this.getAggregateDayHours(timeclock.Day),
                    startTime: timeclock.StartTime,
                    endTime: timeclock.EndTime,
                    drillDown: false
                });
            });
        }
        getTimeClockData = (): void => {
            var params = this.getPagingParams();
            this.timeClocks = [];

            if (this.CanViewAllEmployeesTimeClock()) {
                this.chartsSvc.getEmployeeTimeClockForTimeClockViewer(params).then((data): any => {
                    this.calculateAggregateDayHours(data);

                    angular.forEach(data,(timeclock) => {
                        this.timeClock = timeclock;
                        this.timeClock.WeekNo = this.getMonthWeekOfDate(timeclock.Day);

                        this.timeClocks.push(this.timeClock);
                    });

                    this.setCalenderEvents();
                    this.getWeeklyHours();
                })
            }
            else {
                this.chartsSvc.getEmployeeTimeCLock(params).then((data): any => {
                    this.calculateAggregateDayHours(data);

                    angular.forEach(data,(timeclock) => {
                        this.timeClock = timeclock;
                        this.timeClock.WeekNo = this.getMonthWeekOfDate(timeclock.Day);

                        this.timeClocks.push(this.timeClock);
                    });

                    this.setCalenderEvents();
                    this.getWeeklyHours();
                })
            }
           

        }
        // gets the week of the month a given date falls in to
        getMonthWeekOfDate = (timeClockDate: string) => {
            var date = moment(timeClockDate).toDate();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
            this.totalWeeksInMonth = Math.ceil((date.getDate() + firstDay) / 7);
            return this.totalWeeksInMonth;
        }

        //to calculate weekly total hours 
        getWeeklyHours = () => {
            this.weeks = [];
            for (var i = 1; i <= this.totalWeeksInMonth; i++) {
                var totalHrs = 0;
                angular.forEach(this.timeClocks,(timeclock) => {
                    if (i == timeclock.WeekNo) {
                        totalHrs = totalHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                    }
                });

                this.weeks.push(<financialInterfaces.IWeek > {
                    totalHours: totalHrs,
                    weekNo: i
                });
            }
        }

        // to calculate aggregate total hours of each day
        calculateAggregateDayHours = (data: any) => {
            if (!data[0])
                return;

            this.dayAggregates = [];
            var previousDate = data[0].Day;
            var aggregateHrs = 0;
            angular.forEach(data,(timeclock) => {
                if (timeclock.Day == previousDate) {
                    aggregateHrs = aggregateHrs + (this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime);
                }
                else {
                    this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                        day: previousDate,
                        aggregateHours: aggregateHrs
                    });
                    previousDate = timeclock.Day;
                    aggregateHrs = this.showStraightTimeEquivalent ? timeclock.StraightTimeEquivalent : timeclock.TotalTime;
                }
            });
            //to push last date aggregate
            this.dayAggregates.push(<financialInterfaces.IDayAggregate>{
                day: previousDate,
                aggregateHours: aggregateHrs
            });
        }

        //to get aggregate total hours of a particular day
        getAggregateDayHours = (date: Date): number => {

            for (var i = 0; i < this.dayAggregates.length; i++) {
                if (this.dayAggregates[i].day == date) {
                    return this.dayAggregates[i].aggregateHours;
                }
            }
        }

        //reset to show STE hours on toggle
        getStraightTimeEquivalentHours = () => {
            this.showStraightTimeEquivalent ? this.showStraightTimeEquivalent = false : this.showStraightTimeEquivalent = true;
            this.showStraightTimeEquivalent ? this.showSTEBtnText = 'SHOW REG HOURS' : this.showSTEBtnText = 'SHOW STE HOURS';
            this.calculateAggregateDayHours(this.timeClocks);
            this.getWeeklyHours();
            this.setCalenderEvents();
        }

        getPagingParams = () => {
            var startDate = this.dateFormatterSvc.formatDateFull(new Date(this.dateRange.year, this.dateRange.month, 1));
            var endDate = this.dateFormatterSvc.formatDateFull(new Date(this.dateRange.year, this.dateRange.month + 1, 0));
            var params = <financialInterfaces.IODataPagingParamModel>{
                $orderby: 'StartTime asc',
                $filter: `EmployeeId eq ${this.employeeId} and Day ge DateTime'${startDate}' and Day le DateTime'${endDate}'`
            };
            return params;
        }

    }

    angular.module("app").controller("Financials.TimeClockDetailsCtrl", TimeClockDetailsCtrl);
} 