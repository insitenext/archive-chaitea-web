﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Financials.AccountsReceivableAging.Controllers {
    'use strict';

    import financialsSvc = ChaiTea.Financials.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;


    export interface IARAgingIndexCtrl {
        initialize(): void;
        onFailure(response: any): void;
        getBillingDuesBarChart(dateRange: commonInterfaces.IDateRange): void;
        refreshCharts(dateRange: commonInterfaces.IDateRange): void;
    }

    class AccountsReceivableAgingIndexCtrl implements IARAgingIndexCtrl {
     
        clientId: number = 0;
        programId: number = 0;
        siteId: number = 0;
        basePath: string;
        colors: ISecondaryColors;
        chartSettings: IChartSettings;

        valueFormatter = val => val.toFixed(0);
        /**
        * Define chart data object format for all the charts.
        */
        chartsConfig = {
            billingDues: {}

        };
        billingMonthlyBarChart: boolean = true;
        lastUpdatedDate: any;
        siteName: string = "All Sites";
        programName: string = "All Programs";
        static $inject = [
            '$scope',
            '$filter',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc'];

        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: financialsSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.colors = sitesettings.colors.secondaryColors;
            this.clientId = userContext.Client.ID;
            this.programId = userContext.Program.ID;
            this.siteId = userContext.Site.ID;
            this.siteName = userContext.Site.ID != 0 ? userContext.Site.Name : this.siteName;
            this.programName = userContext.Program.ID != 0 ? userContext.Program.Name : this.programName;
            this.getLastUpdatedate();
        }


        /** @description Initializer for the controller. */
        initialize = (): void => {
            this.refreshCharts();
        }

        private getLastUpdatedate = ():any =>{
            this.apiSvc.getByOdata('','Invoice/LastUpdateDate',false,false).then((result) => {
                this.lastUpdatedDate = result.LastUpdateDate;
            }, this.onFailure);
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
        * @description Refresh the graph data.
        * @description Call the data service and fetch the data based on params.
        */
        refreshCharts = (): void => {
            this.getBillingDuesBarChart();
        }

    
        /**
         * @description Past and Current Due Monthly.
         */
        getBillingDuesBarChart = () => {
            this.chartsSvc.getBillingDuesBarChart().then((result) => {
                if (!result) return;

                if (result.length == 1 && result[0].Base == 0 && result[0].AboveBase == 0)
                    this.billingMonthlyBarChart = false;
                else
                    this.billingMonthlyBarChart = true;

                var categories = [];
                var pastDue = [];
                var currentDue = [];

                if (result.length < 4) {
                    //to get xAxis values
                    var noOfMonths = 3;
                    var date = moment().add('month', -1);
                    for (var i = 0; i < noOfMonths; i++) {
                        categories.push(date.format('MMM \'YY'));
                        date.add('month', 1);
                    }
                    var index = 0;
                    angular.forEach(categories, (c) => {
                        var item = _.find(result, (o: any) => { return o.DueMonth == c });
                        if (item) {
                            pastDue.push(item.PastDue);
                            currentDue.push(item.CurrentDue);
                        } else {
                            pastDue.push(0);
                            currentDue.push(0);
                        }
                    });
                } else {
                    angular.forEach(result, (item) => {
                        categories.push(item.DueMonth);
                        pastDue.push(item.PastDue);
                        currentDue.push(item.CurrentDue);
                    });

                }
                categories[0] = 'PAST MONTHS';
                //// Build the chart
                this.chartsConfig.billingDues = this.chartConfigBuilder
                    ('billingDuesBarChart', {
                    series: [
                        {
                            name: 'PAST DUE',
                            color: this.colors.blue,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: pastDue[i]
                                }
                            }),
                        },
                        {
                            name: 'CURRENT',
                            color: this.colors.teal,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: currentDue[i]

                                }
                            })
                        }
                    ],
                });

            });
        }
                
        /**
         * @description Defines overridable chart options.
         */
        chartConfigBuilder = (name, options) => {
            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                }
            });

            var chartOptions = {

                // column Chart
                billingDuesBarChart: <HighchartsOptions>{
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    height: 350,
                    xAxis: {
                        tickColor: 'white',
                        type: 'category',
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600'
                            }
                        }
                    },
                    yAxis:
                    {
                        tickInterval: 5000,
                        min: 0,

                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            },
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: '#e4e4e4',

                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: '#fff',
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        lineHeight: '10px',
                        itemMarginTop: 20,
                        itemMarginBottom: 6,
                        reversed: true,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },

                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        }
                    },
                    series: [
                        {
                            name: 'Past Due',
                            data: []
                        },
                        {
                            name: 'Current',
                            data: []
                        }

                    ]
                },

            }
            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('AccountsReceivableAgingIndexCtrl', AccountsReceivableAgingIndexCtrl);
} 