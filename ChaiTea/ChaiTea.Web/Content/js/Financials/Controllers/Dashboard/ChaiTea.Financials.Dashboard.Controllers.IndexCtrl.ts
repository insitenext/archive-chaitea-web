﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.Dashboard.Controllers {
    'use strict';

    import financialsSvc = ChaiTea.Financials.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import enums = Common.Enums;

    class DashboardIndexCtrl {
        isEmployee: boolean = false;
        coreColors: ICoreColors;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        dateRangeParam = {
            startDate: '',
            endDate: ''
        }
        dashboardItem = {
            billingAndAging: {
                lastUpdatedDate: ''
            },
            supplies: {
                lastUpdatedDate: ''
            },
            hours: {
                labels: [],
                regularHours: [],
                STEHours: [],
                overTimeHours: [],
                doubleTimeHours: [],
                lastUpdatedDate: ''
            }
        }

        reportsConfig = {
            billingTrend: {},
            suppliesTrend: {},
            agingByDueDatesTrend: {},
            straightTimeHoursTrend: {},
            regularHoursTrend: {},
        };

        localStoreDashboardSettingsKey = "";
        dashboardId = Common.Enums.DashboardIds.Financials;
        dashBoardTileInfo = [
            { id: enums.DashboardWidgets.BillingTrend, name: 'Billing', url: 'Financials/BillingOverview/', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12', visible: true, section: 'half-1' },
            { id: enums.DashboardWidgets.SuppliesTrend, name: 'Supplies', url: 'Financials/Supplies/', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12', visible: true, section: 'half-1' },
            { id: enums.DashboardWidgets.AgingTrend, name: 'Aging By Due Dates', url: 'Financials/AccountReceivableAging/', class: 'col-md-12 col-lg-12 col-xs-12 col-sm-12', visible: true, section: 'full' },
            { id: enums.DashboardWidgets.StraightTimeHoursTrend, name: 'Straight Time Hours', url: 'Financials/TimeClock?tabIndex=1', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12', visible: true, section: 'half-2' },
            { id: enums.DashboardWidgets.RegularHoursTrend, name: 'Regular Hours', url: 'Financials/TimeClock?tabIndex=0', class: 'col-md-6 col-lg-6 col-sm-12 col-xs-12', visible: true, section: 'half-2' }
        ];

        static $inject = [
            '$scope',
            'userInfo',
            'sitesettings',
            'userContext',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.apibasesvc',
            '$window',
            '$uibModal'];
        constructor(
            private $scope: angular.IScope,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private chartsSvc: financialsSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $window: angular.IWindowService,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.dateRangeParam.startDate = this.dateFormatterSvc.formatDateShort(this.dateRange.startDate);
            this.dateRangeParam.endDate = this.dateFormatterSvc.formatDateShort(this.dateRange.endDate);

            this.reportsConfig = {
                billingTrend: this.chartConfigBuilder('billing', {}),
                suppliesTrend: this.chartConfigBuilder('supplies', {}),
                agingByDueDatesTrend: this.chartConfigBuilder('aging', {}),
                straightTimeHoursTrend: this.chartConfigBuilder('STEHours', {}),
                regularHoursTrend: this.chartConfigBuilder('regularHours', {})
            };

            this.localStoreDashboardSettingsKey = 'sbm-user-financials-dashboard-settings:' + this.userContext.UserId;

            var dashboardSettings = JSON.parse($window.localStorage.getItem(this.localStoreDashboardSettingsKey));
            // Repaint tiles base on localStorage
            if (dashboardSettings) {
                var localStorageTileOrder = [];
                dashboardSettings.forEach(((tile) => {
                    localStorageTileOrder.push(tile);
                }));
                this.dashBoardTileInfo = localStorageTileOrder;
            }
            var tileInfo = this.dashBoardTileInfo;

            if (dashboardSettings) {
                if (tileInfo.length > dashboardSettings.length) {
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
                else if (dashboardSettings.length > tileInfo.length) {
                    dashboardSettings.forEach(((tile, index, object) => {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index, 1);
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                } else {
                    dashboardSettings.forEach(((tile, index, object) => {
                        var match = _.find(tileInfo, { 'name': tile.name });
                        if (!match) {
                            object.splice(index, 1);
                        }
                    }));
                    _.forEach(tileInfo, ((tile) => {
                        var match = _.find(dashboardSettings, { 'name': tile.name });
                        if (!match) {
                            dashboardSettings.push({
                                id: tile.id,
                                name: tile.name,
                                url: tile.url,
                                class: tile.class,
                                visible: tile.visible
                            });
                        }
                    }));
                    this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(dashboardSettings));
                }
            }
            if (dashboardSettings && !_.some(dashboardSettings, (o) => _.isNull(o))) {
                this.dashBoardTileInfo = dashboardSettings;
            }
            
            this.getDashboardItems();
        }

        private getDashboardItems = () => {
            var params = {
                userId: this.userContext.UserId,
                dashboardId: this.dashboardId
            }
            this.apiSvc.query(params, "UserDashboardWidget?userid=:userId&dashboardid=:dashboardId").then((result) => {
                if (result.length > 0) {
                    angular.forEach(result, (item) => {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': item.DashboardWidgetId });
                        if (obj && obj.visible != item.IsVisible) {
                            obj.visible = item.IsVisible;
                        }
                    });
                }
                else {
                    angular.forEach(this.dashBoardTileInfo, (item) => {
                        item.visible = true;
                        var params = {
                            UserId: this.userContext.UserId,
                            DashboardWidgetId: item.id,
                            DashboardId: this.dashboardId,
                            IsVisible: true
                        }
                        this.apiSvc.save(params, "UserDashboardWidget").then((result) => {
                        }, this.onFailure);
                    });
                }
                this.getDashBoardInfo();
            }, this.onFailure);
        }

        private showConfigOptions = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Financials/Templates/financials.dashboard.configure.options.tmpl.html',
                controller: 'Financials.Dashboard.ConfigureModalCtrl as vm',
                size: 'md',
                resolve: {
                    dashBoardTileInfo: () => {
                        return this.dashBoardTileInfo;
                    }
                }
            }).result.then(dashboardTileUpdated => {
                if (!dashboardTileUpdated) return false;
                angular.forEach(dashboardTileUpdated, (item, key) => {
                    var obj = _.find(this.dashBoardTileInfo, { 'id': item.id });
                    var objIndex = _.findIndex(this.dashBoardTileInfo, { 'id': item.id });
                    if (obj && obj.visible != item.checked) {
                        obj.visible = item.checked;
                    }
                    if (key != objIndex) {
                        this.dashBoardTileInfo.splice(key, 0, obj);
                        this.dashBoardTileInfo.splice(objIndex + 1, 1);
                    }
                });
                this.$window.localStorage.setItem(this.localStoreDashboardSettingsKey, JSON.stringify(this.dashBoardTileInfo));
                // TODO: Find a way to repaint graphs withouth having to hit the APIs again
                this.reportsConfig.billingTrend = {};
                this.reportsConfig.suppliesTrend = {};
                this.reportsConfig.agingByDueDatesTrend = {};                
                this.reportsConfig.straightTimeHoursTrend = {};
                this.reportsConfig.regularHoursTrend = {};                
                this.getDashBoardInfo();
            });
        }        

        private getDashBoardInfo = () => {
             async.parallel({
                getBillingTrend: (callback: any) => {
                    if (this.userInfo.mainRoles.billingviewers) {
                        this.getBillingTrend().then((results) => {
                            callback(null, results);
                        });
                        this.getBillingAndAgingLastUpdatedate();
                    } else {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.BillingTrend });
                        if (obj) {
                            obj.visible = false;
                        }
                    }
                },
                getSuppliesTrend: (callback: any) => {
                    if (this.userInfo.mainRoles.suppliesviewers) {
                        this.getSuppliesTrend().then((results) => {
                            callback(null, results);
                        });
                        this.getSuppliesLatestUpdateDate();
                    } else {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.SuppliesTrend });
                        if (obj) {
                            obj.visible = false;
                        }
                    }
                },
                getAgingBuDueDateTrends: (callback: any) => {
                    if (this.userInfo.mainRoles.billingviewers) {
                        this.getAgingBuDueDateTrends().then((results) => {
                            callback(null, results);
                        });
                    } else {
                        var obj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.AgingTrend });
                        if (obj) {
                            obj.visible = false;
                        }
                    }
                },
                getStraightTimeHoursTrend: (callback: any) => {
                    if (this.userInfo.mainRoles.timeclockviewers || this.userInfo.mainRoles.managers) {
                        this.getHours().then((results) => {
                            callback(null, results);
                        });
                        this.getHoursLastUpdateDate();
                    } else {
                        var SteObj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.StraightTimeHoursTrend });
                        if (SteObj) {
                            SteObj.visible = false;
                        }
                        var RegObj = _.find(this.dashBoardTileInfo, { 'id': enums.DashboardWidgets.RegularHoursTrend });
                        if (RegObj) {
                            RegObj.visible = false;
                        }
                    }
                },
            })
        }

        private getBillingTrend = (): angular.IPromise<any> => {
            return this.chartsSvc.getBillingTrendsBarChart(this.dateRangeParam).then((result) => {
                if (!result) return;
                var categories = [];
                var baseData = [];
                var aboveBaseData = [];

                angular.forEach(result, (item) => {
                    categories.push(item.Month);
                    baseData.push(item.Base);
                    aboveBaseData.push(item.AboveBase);
                });

                this.reportsConfig.billingTrend = this.chartConfigBuilder('billing', {
                    series: [
                        {
                            name: 'Above Base',
                            color: this.sitesettings.colors.secondaryColors.blue,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: aboveBaseData[i]

                                }
                            })
                        },
                        {
                            name: 'Base',
                            color: this.sitesettings.colors.secondaryColors.teal,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: baseData[i]

                                }
                            })
                        }]
                });
            }, this.onFailure);

        }

        private getSuppliesTrend = (): angular.IPromise<any> => {
            if (this.userContext.Program.ID) {
            return this.chartsSvc.getSuppliesMonthlyProductTrends(this.dateRangeParam).then((result) => {
                if (!result) return;

                var index = result.Labels.length - 1;
                var categories = _.dropRight(result.Labels, 1);


                var dataSeries = [];
                var dataCategories = [];

                angular.forEach(result.Categories, function (value, key) {
                    var total: number = 0;
                    total = value[index];
                    value = _.dropRight(value, 1);
                    dataCategories.push({ data: value, name: key, total: total })
                });

                dataCategories.sort(function (a, b) {
                    return b.total - a.total
                });

                var categoryLength = dataCategories.length - 1;
                angular.forEach(dataCategories, (category) => {
                    dataSeries.push({ data: category.data, name: category.name.toLowerCase(), amount: category.total, index: categoryLength })
                    categoryLength -= 1;
                });

                this.reportsConfig.suppliesTrend = this.chartConfigBuilder('supplies', {
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        categories: categories,
                    },
                    yAxis: {
                        stackLabels: {
                            enabled: false,
                        },
                        title: null,
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: dataSeries,

                });
            }, this.onFailure);
            } else {
                return this.apiSvc.query(this.dateRangeParam, "FinanceReport/SpendByProgramMonthly/:startDate/:endDate")
                    .then((result) => {
                        if (!result || result.length == 0) return;

                        var categories = [];
                        angular.forEach(result[0].Months, (month) => {
                            categories.push(month.Month);
                        });


                        var dataSeries = [];
                        result.sort(function (a, b) {
                            return b.Total - a.Total
                        });

                        var index = result.length;
                        angular.forEach(result, (program) => {
                            var amount = [];

                            angular.forEach(program.Months, (month) => {
                                amount.push(month.Amount);
                            });
                            dataSeries.push({ data: amount, name: program.ProgramName, amount: program.Total, index: index });
                            index -= 1;
                        });

                        this.reportsConfig.suppliesTrend = this.chartConfigBuilder('supplies', {
                            xAxis: {
                                tickColor: this.sitesettings.colors.coreColors.default,
                                categories: categories,
                            },
                            yAxis: {
                                stackLabels: {
                                    enabled: false,
                                },
                                title: null,
                            },
                            plotOptions: {
                                series: {
                                    stacking: 'normal'
        }
                            },
                            series: dataSeries,

                        });
                    }, this.onFailure);
            }

        }

        private getAgingBuDueDateTrends = (): angular.IPromise<any> => {

            return this.chartsSvc.getBillingDuesBarChart().then((result) => {
                if (!result) return;

                var categories = [];
                var pastDue = [];
                var currentDue = [];

                if (result.length < 3) {
                    //to get xAxis values
                    var noOfMonths = 3;
                    var date = moment(_.clone(this.dateRangeParam.endDate)).add('month', -1);
                    for (var i = 0; i < noOfMonths; i++) {
                        categories.push(date.format('MMM \'YY'));
                        date.add('month', 1);
                    }
                    var index = 0;
                    angular.forEach(categories, (c) => {
                        var item = _.find(result, (o: any) => { return o.DueMonth == c });
                        if (item) {
                            pastDue.push(item.PastDue);
                            currentDue.push(item.CurrentDue);
                        } else {
                            pastDue.push(0);
                            currentDue.push(0);
                        }
                    });
                } else {
                angular.forEach(result, (item) => {
                            categories.push(item.DueMonth);
                            pastDue.push(item.PastDue);
                            currentDue.push(item.CurrentDue);
                    });
                    }
                categories[0] = 'PAST MONTHS';

                this.reportsConfig.agingByDueDatesTrend = this.chartConfigBuilder('aging', {
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        },

                        series: {
                            pointWidth: categories.length == 1 ? 200 : null
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    series: [
                        {
                            name: 'Past Due',
                            color: this.sitesettings.colors.secondaryColors.blue,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: pastDue[i]
                                }
                            }),
                        },
                        {
                            name: 'Current',
                            color: this.sitesettings.colors.secondaryColors.teal,
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: currentDue[i]

                                }
                            })
                        }
                    ]
                });
            }, this.onFailure);

        }

        private getHoursLastUpdateDate = (): void => {
            this.apiSvc.query(null, 'TimeClock/LastUpdateDate', false, false).then((result) => {
                this.dashboardItem.hours.lastUpdatedDate = result.LastUpdateDate;
            }, this.onFailure);
        }

        private getSuppliesLatestUpdateDate = (): void => {
            this.chartsSvc.getLatestUpdateDate().then((result) => {
                this.dashboardItem.supplies.lastUpdatedDate = result.LastUpdatedDate;
            }, this.onFailure);
        }

        private getBillingAndAgingLastUpdatedate = (): any => {
            this.apiSvc.getByOdata('', 'Invoice/LastUpdateDate', false, false).then((result) => {
                this.dashboardItem.billingAndAging.lastUpdatedDate = result.LastUpdateDate;
            }, this.onFailure);
        }

        private getHours = () => {
            return this.apiSvc.query(this.dateRangeParam, 'FinanceReport/HourMonthlyTrends/:startDate/:endDate/:supervisorId', false, false).then((result) => {
                if (!result) return;
                this.dashboardItem.hours.labels = result.Labels;
                this.dashboardItem.hours.regularHours = result.RegularHours;
                this.dashboardItem.hours.STEHours = result.STEHours;
                this.dashboardItem.hours.doubleTimeHours = result.DoubleTimeHours;
                this.dashboardItem.hours.overTimeHours = result.OverTimeHours;

                this.showRegularHours();
                this.showSTEHours();
            }, this.onFailure);
        }

        private showRegularHours = () => {
            this.reportsConfig.regularHoursTrend = this.chartConfigBuilder('regularHours', {
                xAxis: {
                    tickColor: this.sitesettings.colors.coreColors.default,
                    categories: this.dashboardItem.hours.labels,
                    labels: {}
                },
                yAxis: {
                    stackLabels: {
                        enabled: false,
                        style: {
                            color: 'black',
                        }
                    },
                    title: null,
                    labels: { }
                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                    }
                },
                series: [
                    {
                        name: 'Regular Hours',
                        lineWidth: 3,
                        data: this.dashboardItem.hours.regularHours,
                        index: 2,
                        legendIndex: 0
                    },
                    {
                        name: 'Overtime Hours',
                        lineWidth: 3,
                        data: this.dashboardItem.hours.overTimeHours,
                        index: 1,
                        legendIndex: 1
                    },
                    {
                        name: 'Double Time Hours',
                        lineWidth: 3,
                        data: this.dashboardItem.hours.doubleTimeHours,
                        index: 0,
                        legendIndex: 2
                    }
                ]
            });

        }

        private showSTEHours = () => {
            // Build the chart
            this.reportsConfig.straightTimeHoursTrend = this.chartConfigBuilder('STEHours', {
                yAxis: {
                    stackLabels: {
                        enabled: false,
                        style: {
                            color: 'black'
                        }
                    },
                    title: null,
                    tickInterval: 5000,
                    min: 0,

                    labels: {
                        formatter: function () {
                            return (this.value / 1000).toFixed(0) + ' k';
                        }
                    }
                },
                xAxis: {
                    tickColor: this.sitesettings.colors.coreColors.default,
                    categories: this.dashboardItem.hours.labels,
                    labels: {}
                },
                series: [
                    {
                        name: 'STE HOURS',
                        lineWidth: 3,
                        data: this.dashboardItem.hours.STEHours
                    }
                ],
                categories: this.dashboardItem.hours.labels,
            });
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        private chartConfigBuilder = (name, options) => {
            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                }
            });
            var chartOptions = {

                // Column Chart
                billing: {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category'
                    },
                    yAxis:
                    {
                        tickInterval: 200000,
                        min: 0,

                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,

                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        layout: this.sitesettings.windowSizes.isTiny ? 'horizontal' : 'vertical',
                        x: this.sitesettings.windowSizes.isTiny ? 0 : 10,
                        y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                        backgroundColor: this.sitesettings.colors.coreColors.default,
                        itemMarginBottom: 5,
                        symbolHeight: 12,
                        symbolWidth: 12,
                        symbolRadius: 3,
                        itemStyle: {
                            color: this.sitesettings.colors.coreColors.dark,
                            fontSize: '13px',
                            fontWeight: '600',
                        }
                    },

                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        }
                    },
                    series: []
                },

                // Column Chart
                supplies: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },
                    tooltip: {
                        borderRadius: 15,
                        pointFormat: '{series.name}: <b>${point.y}</b>',
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    legend: {
                        enabled: this.sitesettings.windowSizes.isDesktop ? true : false,
                        align: 'right',
                        verticalAlign: 'top',
                        layout: 'vertical',
                        x: this.sitesettings.windowSizes.isTiny ? 0 : 15,
                        y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                        backgroundColor: this.sitesettings.colors.coreColors.default,
                        itemMarginBottom: 5,
                        symbolHeight: 12,
                        symbolWidth: 12,
                        symbolRadius: 3,
                        reversed: true,
                        itemStyle: {
                            color: this.sitesettings.colors.coreColors.dark,
                            fontSize: '13px',
                            fontWeight: '600',
                        },
                        labelFormatter: function () {
                            var legendName = this.name;
                            var match = legendName.match(/.{1,18}/g);
                            return match.toString().replace(/\,/g, "<br/>");
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                radius: 5,
                                lineColor: null
                            },

                        }

                    },
                    yAxis: {
                        title: '',
                        tickInterval: 10000,
                        min: 0,
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,
                        plotLines: [
                            {
                                value: 98,
                                color: '#6C9DC7',
                                width: 4,
                                zIndex: 4,
                                label: { text: '' }
                            }
                        ],
                        labels: {}
                    }                        ,
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        labels: {}
                    },
                },

                // Column Chart
                aging: {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    height: 350,
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category',
                        labels: { }
                    },
                    yAxis:
                    {
                        tickInterval: 500000,
                        min: 0,

                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,

                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        layout: this.sitesettings.windowSizes.isTiny ? 'horizontal' : 'vertical',
                        x: this.sitesettings.windowSizes.isTiny ? 0 : 10,
                        y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                        backgroundColor: this.sitesettings.colors.coreColors.default,
                        itemMarginBottom: 5,
                        symbolHeight: 12,
                        symbolWidth: 12,
                        symbolRadius: 3,
                        itemStyle: {
                            color: this.sitesettings.colors.coreColors.dark,
                            fontSize: '13px',
                            fontWeight: '600',
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        }
                    },
                    series: [
                        {
                            name: 'Past Due',
                            data: []
                        },
                        {
                            name: 'Current',
                            data: []
                        }

                    ]
                },

                // Column Chart
                regularHours: {
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },
                    yAxis:
                    {
                        tickInterval: 5000,
                        min: 0,

                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,

                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: this.sitesettings.windowSizes.isDesktop ? true : false,
                        align: 'right',
                        verticalAlign: 'top',
                        layout: 'vertical',
                        x: this.sitesettings.windowSizes.isTiny ? 0 : 15,
                        y: this.sitesettings.windowSizes.isTiny ? 0 : 30,
                        backgroundColor: this.sitesettings.colors.coreColors.default,
                        itemMarginBottom: 5,
                        symbolHeight: 12,
                        symbolWidth: 12,
                        symbolRadius: 3,
                        itemStyle: {
                            color: this.sitesettings.colors.coreColors.dark,
                            fontSize: '13px',
                            fontWeight: '600',
                        }
                    },
                    tooltip: {
                        borderRadius: 15,
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        }
                    },
                    plotOptions: {
                        column: {
                            showInLegend: false
                        },
                        series: {
                            marker: {
                                symbol: 'circle',
                                lineWidth: 2,
                                radius: 5,
                            },

                        }

                    }
                },

                // Area chart
                STEHours: {
                    categories: [],
                    series: [{}],
                    yAxis:
                    {
                        tickInterval: 5000,
                        min: 0,

                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,

                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    }
                }

            }
            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('Financial.DashboardIndexCtrl', DashboardIndexCtrl);
}  