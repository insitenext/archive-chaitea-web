﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Financials.BillingOverview.Controllers {
    'use strict';

    import financialsSvc = ChaiTea.Financials.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;


    class BillingDetailsCtrl {
        
        programId: number = 0;
        programName: string = '';

        top: number = 20;
        recordsToSkip: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        dateRangeParam = {
            startDate: '',
            endDate: ''
        }
        invoiceItems = [];
        paidStatus: string = '';
        showByProgram: boolean = true;
        showSearch: boolean = false;
        searchQuery: string = '';
        static $inject = [
            '$scope',
            '$filter',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc',
            '$uibModal',
            'chaitea.common.services.notificationsvc'];

        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: financialsSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = moment(obj.startDate);
                this.dateRange.endDate = moment(obj.endDate);

                this.dateRangeParam = {
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                }

                this.getData();
            });
        }


        /** @description Initializer for the controller. */
        initialize = (id: number, paidStatus: string): void => {
            this.programId = this.userContext.Program.ID || id;
            if (!this.programId) {
                this.programName = "All Programs";
            }
            if (paidStatus == 'Paid' || paidStatus == 'Unpaid') {
                this.paidStatus = paidStatus;
                this.showByProgram = false;
            } 
        }

        private getData = () => {
            this.isAllLoaded = false;
            this.isBusy = false;
            this.recordsToSkip = 0;
            this.invoiceItems = [];
            if (this.showByProgram) {
                this.getInvoiceItems();
            } else {
                this.getInvoicesByType();
            }
        }

        private onBlur = () => {
            this.showSearch = false;
            this.searchQuery = '';
        }

        private search = (item) => {
            if (this.showByProgram) {
                return (item.InvoiceNumber.toString().indexOf(this.searchQuery || '') !== -1 ||
                    angular.lowercase(item.Description).indexOf(angular.lowercase(this.searchQuery) || '') !== -1 ||
                    angular.lowercase(moment(item.InvoiceDate).format('MM/DD/YY')).indexOf(angular.lowercase(this.searchQuery) || '') !== -1);
            } else {
                return (item.InvoiceNumber.toString().indexOf(this.searchQuery || '') !== -1 ||
                    angular.lowercase(moment(item.DueDate).format('MM/DD/YY')).indexOf(angular.lowercase(this.searchQuery) || '') !== -1 ||
                    angular.lowercase(moment(item.MonthOfServiceDate).format('MMM')).indexOf(angular.lowercase(this.searchQuery) || '') !== -1 ||
                    angular.lowercase(moment(item.InvoiceDate).format('MM/DD/YY')).indexOf(angular.lowercase(this.searchQuery) || '') !== -1);
            }
        }

        private getInvoicesByType = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                top: this.top,
                skip: this.recordsToSkip,
                isUnpaid: this.paidStatus == "Paid" ? false: true
            };

            this.apiSvc.query(params, 'Invoice/InvoiceByUnpaidFlag', false, true).then((result): any => {
                if (result.length == 0) {
                    this.isAllLoaded = true;
                    return;
                }
                if (!this.programName) {
                    this.programName = result[0].ProgramName;
                }
                angular.forEach(result, (obj) => {
                    this.invoiceItems.push(obj);
                });

                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        private getInvoiceItems = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = {
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                top: this.top,
                skip: this.recordsToSkip,
                programId: this.programId
            };

            this.apiSvc.query(params, 'Invoice/InvoiceItemsByProgram', false, true).then((result): any => {
                if (result.length == 0) {
                    this.isAllLoaded = true;
                    return;
                }
                if (!this.programName) {
                    this.programName = result[0].ProgramName;
                }
                angular.forEach(result, (obj) => {
                    this.invoiceItems.push(obj);
                });

                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        private export = (): void => {
            var exportStyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } }
            };
            var invoiceItemsToExport = [];
            if (this.showByProgram) {
                angular.forEach(this.invoiceItems, (item) => {
                    invoiceItemsToExport.push({
                        "Invoice #": item.InvoiceNumber,
                        "Invoice Date": moment(item.InvoiceDate).format('L'),
                        "Description": item.Description,
                        "Rate": item.Rate,
                        "Taxable": item.IsTaxable,
                        "Quantity": item.Quantity,
                        "Amount": "$" + item.Amount,
                        "Paid": item.IsPaid,
                        
                    });
                });
            } else {
                angular.forEach(this.invoiceItems, (item) => {
                    invoiceItemsToExport.push({
                        "Invoice #": item.InvoiceNumber,
                        "Invoice Date": moment(item.InvoiceDate).format('L'),
                        "Amount": "$" + item.Amount,
                        "Service Month": moment(item.MonthOfServiceDate).format('mmm'),
                        "Due Date": moment(item.DueDate).format('L'),
                        "Paid": item.OutstandingAmount == 0 ? 'Paid' : 'Unpaid',
                    });
                });
            }
            alasql('SELECT * INTO CSV("Invoice_Items.csv",?)FROM ?', [exportStyle, invoiceItemsToExport]);
        }

        private ShowInvoiceDetailsModal = (id): void => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Financials/Templates/Billing/billing.invoice.details.tmpl.html',
                controller: 'InvoiceDetailsModalCtrl as vm',
                size: 'lg',
                resolve: {
                    invoiceId: () => { return id; }
                }
            });
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }
    }

    class InvoiceDetailsModalCtrl {
        invoiceNumber: number;
        invoiceDate: string;
        paymentTerms: string = "";
        PO: string = "";
        billedAmount: number = 0.00;
        isPaid: string = "UNPAID";
        taxAmount: number = 0;
        monthOfService: string = "";
        paidAmount: number = 0.00;
        invoiceAmount: number = 0.00;
        outstandingAmount: number = 0.00;
        invoiceItems: Array<Interfaces.IInvoiceItem>;
        customer: Interfaces.ICustomerDetails;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            '$uibModalInstance',
            'invoiceId',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private invoiceId: number,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.getInvoiceDetails(invoiceId);
        }


        private getInvoiceDetails = (invoiceId) => {

            this.apiSvc.getById(invoiceId, 'Invoice').then((invoice: Interfaces.IInvoice) => {
                this.invoiceNumber = invoice.InvoiceNumber;
                this.customer = invoice.Customer;
                this.billedAmount = invoice.BilledAmount;
                this.invoiceDate = (invoice.InvoiceDate ? moment(invoice.InvoiceDate).format('MM/DD/YYYY') : 'N/A');
                this.paymentTerms = invoice.PaymentTermsTypeName;
                this.PO = invoice.PO;
                this.taxAmount = invoice.TaxAmount;
                this.monthOfService = invoice.MonthOfService;
                this.paidAmount = invoice.PaidAmount;
                this.invoiceAmount = invoice.InvoiceAmount;
                this.outstandingAmount = invoice.OutstandingAmount;
                if (invoice.OutstandingAmount == 0)
                { this.isPaid = 'PAID'; }
                this.invoiceItems = [];
                angular.forEach(invoice.InvoiceItems, (invoiceItem: Interfaces.IInvoiceItem) => {

                    this.invoiceItems.push(<Interfaces.IInvoiceItem>{
                        Amount: invoiceItem.Amount,
                        CurrencyCodeId: invoiceItem.CurrencyCodeId,
                        CurrencyCodeName: invoiceItem.CurrencyCodeName,
                        Description: invoiceItem.Description,
                        InvoiceId: invoiceItem.InvoiceId,
                        InvoiceItemId: invoiceItem.InvoiceItemId,
                        IsPaid: invoiceItem.IsPaid,
                        IsTaxable: invoiceItem.IsTaxable,
                        LineNumber: invoiceItem.LineNumber,
                        Quantity: invoiceItem.Quantity,
                        Rate: invoiceItem.Rate,
                        ProgramId: invoiceItem.ProgramId,
                        ProgramName: invoiceItem.ProgramName,
                        TotalAmount: invoiceItem.TotalAmount
                    });
                });
            }, this.onFailure)
        }

        private close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

    }

    angular.module("app").controller("InvoiceDetailsModalCtrl", InvoiceDetailsModalCtrl);
    angular.module('app').controller('BillingDetailsCtrl', BillingDetailsCtrl);
} 