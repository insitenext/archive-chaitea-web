﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Financials.BillingOverview.Controllers {
    'use strict';

    import financialsSvc = ChaiTea.Financials.Services;
    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;

    interface IBillingProgramData {
        ProgramId: number;
        ProgramName: string;
        Amount: number;
    }

    interface IBillingMonthly {
        Month: string;
        Amount: number;
    }

    interface IBillingSiteMonthly {
        SiteName: string;
        Total: number;
        Months: Array<IBillingMonthly>;
        IsSiteActive: boolean;
    }

    class BillingOverviewIndexCtrl {
        failColor: string = '#EF5C48';
        currentDate = new Date();

        clientId: number = 0;
        programId: number = 0;
        siteId: number = 0;
        colors: ISecondaryColors;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        dateRangeParam = {
            startDate: '',
            endDate: ''
        }
        basePath: string;

        chartSettings: IChartSettings;

        valueFormatter = val => val.toFixed(0);
        billingMonthlyBarChart: boolean = true;
        billingMonthlyByProgramBarChart: boolean = true;
        showBaseByProgramTrend: boolean = true;
        billingMonthlyTrendsByProgramData = [];
        billingSite: boolean = true;
        billingSiteMonthly: Array<IBillingSiteMonthly> = [];
        monthsToDisplay = [];
        /**
        * Define chart data object format for all the charts.
        */
        chartsConfig = {
            billingTrends: {},
            billingTrendsBarChart: {},
            billingTrendsbyProgramBarChart: {},
            billingProgram: {},
            billingSite: {},
            paidVsUnpaid: {},
            paidOrUnpaid: {}
        };

        totalAggregate: number;
        lastUpdatedDate: any;
        siteName: string = "All Sites";
        programName: string = "All Programs";
        monthlyTrendByProgramChartTitle: string = 'Select a Category to the Left';
        ghostColor: string = '';
        isMonthlyTrendGhostState: boolean = true;
        paidVsUnpaidBillingData = [];
        isPaidOrUnpaidGhostState: boolean = true;
        paidOrUnpaidChartTitle: string = 'Select a Category to the Left';
        paidOrUpaidType: string = 'Paid';
        showBillingByPrograms: boolean = false;
        showBillingBySite: boolean = false;
        showPaidVsUnpaid: boolean = false;

        static $inject = [
            '$scope',
            '$filter',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.reportsvc',
            '$window',
            'chaitea.common.services.notificationsvc'];

        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: financialsSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private $window: angular.IWindowService,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;
            this.clientId = userContext.Client.ID;
            this.programId = userContext.Program.ID;
            this.siteId = userContext.Site.ID;
            this.colors = sitesettings.colors.secondaryColors;
            this.siteName = userContext.Site.ID != 0 ? userContext.Site.Name : this.siteName;
            this.programName = userContext.Program.ID != 0 ? userContext.Program.Name : this.programName;
            this.ghostColor = this.sitesettings.colors.coreColors.gray;
            if (this.sitesettings.windowSizes.isTiny) {
                this.monthlyTrendByProgramChartTitle = 'Select a Category Above';
                this.paidOrUnpaidChartTitle = 'Select a Category Above'; 
            }

            this.showBillingByPrograms = !(!this.siteId && this.programId);
            this.showBillingBySite = !this.siteId;
            this.showPaidVsUnpaid = this.siteId > 0 || this.programId >0 ;
            this.chartsConfig = {
                billingTrends: this.chartConfigBuilder('billingTrendsBarChart', {}),
                billingTrendsBarChart: this.chartConfigBuilder('billingTrendsBarChart', {}),
                    billingTrendsbyProgramBarChart: this.chartConfigBuilder('billingTrendsbyProgramBarChart', {}),
                    billingProgram: this.chartConfigBuilder('billingprogram', {}),
                    billingSite: this.chartConfigBuilder('billingSite', {}),
                    paidVsUnpaid: this.chartConfigBuilder('paidVsUnpaid', {}),
                    paidOrUnpaid: this.chartConfigBuilder('paidOrUnpaid', {})
                };

            this.getLastUpdatedate();
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope,(event, obj) => {
                this.dateRange.startDate = moment(obj.startDate);
                this.dateRange.endDate = moment(obj.endDate);

                this.dateRangeParam = {
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                }
                this.isMonthlyTrendGhostState = true;
                this.isPaidOrUnpaidGhostState = true;
                this.refreshCharts(this.dateRangeParam);

            });
        }


        /** @description Initializer for the controller. */
        initialize = (): void => {
            
        }

        private getLastUpdatedate = ():any =>{
            this.apiSvc.getByOdata('','Invoice/LastUpdateDate',false,false).then((result) => {
                this.lastUpdatedDate = result.LastUpdateDate;
            }, this.onFailure);
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
        * @description Refresh the graph data.
        * @description Call the data service and fetch the data based on params.
        */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.getBillingTrendsBarChart(dateRange);
            this.getBillingTrendsByProgramBarChart(dateRange);
            this.getBillingByProgram(dateRange);
            this.getBillingBySiteMonthly(dateRange);
            this.getPaidVsUnpaidBillingTrend(dateRange);
        }

        /**
       * @description By Monthly Trends.
       */
        private getBillingTrendsBarChart = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getBillingTrendsBarChart({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result) return;

                var keepGoing: boolean = true;
                angular.forEach(result,(item) => {
                    if (keepGoing) {
                        if (item.Base == 0 && item.AboveBase == 0)
                            this.billingMonthlyBarChart = false;
                        else {
                            this.billingMonthlyBarChart = true;
                            keepGoing = false;
                        }
                    }
                });
                var categories = [];
                var baseData = [];
                var aboveBaseData = [];

                angular.forEach(result,(item) => {
                    categories.push(item.Month);
                    baseData.push(item.Base);
                    aboveBaseData.push(item.AboveBase);
                });

                //// Build the chart
                this.chartsConfig.billingTrendsBarChart = this.chartConfigBuilder('billingTrendsBarChart', {
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        },

                        series: {
                            cursor: 'pointer',
                            pointWidth: categories.length == 1 ? 200 : null
                        }
                    },
                    yAxis:
                    {
                        tickInterval: 10000,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            },
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,
                        title: {
                            text: ''
                        }
                    },
                    series: [
                        {
                            name: 'Above Base',
                            color: this.colors.blue,
                            events: {},
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: aboveBaseData[i]

                                }
                            }),
                            amount: _.sum(aboveBaseData)
                        },
                        {
                            name: 'Base',
                            color: this.colors.teal,
                            events: {},
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: baseData[i]

                                }
                            }),
                            amount: _.sum(baseData)
                        }],
                });
            });
        }

        private getBillingTrendsByProgramBarChart = (dateRange: commonInterfaces.IDateRange) => {
            var param = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
            this.apiSvc.query(param, 'FinanceReport/BillingMonthlyTrendsByProgram').then((result) => {
                if (!result) return;
                this.billingMonthlyTrendsByProgramData = result;
                this.showBillingMonthlyTrendByProgram(null);
            }, this.onFailure);
        }

        private showBillingMonthlyTrendByProgram = (categoryType: string) => {
            var categories = [];
            var dataSeries = [];
            var dataCategories = [];

            //to get xAxis values
            var noOfMonths = Math.abs(moment(this.dateRangeParam.startDate).diff(moment(this.dateRangeParam.endDate), 'months')) + 1;
            var date = moment(_.clone(this.dateRangeParam.startDate));
            for (var i = 0; i < noOfMonths; i++) {
                categories.push(date.format('MMM \'YY'));
                date.add('month', 1);
            }

            var keepGoing: boolean = true;
            angular.forEach(this.billingMonthlyTrendsByProgramData, (item) => {
                if (keepGoing) {
                    if (categoryType == 'Base') {
                        if (item.Base == 0)
                            this.billingMonthlyByProgramBarChart = false;
                        else {
                            this.billingMonthlyByProgramBarChart = true;
                            keepGoing = false;
                        }
                    } else if (categoryType == 'Above Base') {
                        if (item.AboveBase == 0)
                            this.billingMonthlyByProgramBarChart = false;
                        else {
                            this.billingMonthlyByProgramBarChart = true;
                            keepGoing = false;
                        }
                    }
                }
                item.XaxisLabel = moment(item.ServiceDate).utc().format('MMM \'YY');
            });

            _.chain(this.billingMonthlyTrendsByProgramData)
                .groupBy('ProgramName')
                .map((value, key) => {
                    var programData = [];
                    angular.forEach(categories, (c) => {
                        var val = _.find(value, (v: any) => {
                            return v.XaxisLabel == c;
                        });
                        if (val) {
                            if (!categoryType) {
                                programData.push(val.Base || val.AboveBase);
                            } else {
                                programData.push(categoryType == 'Base' ? val.Base : val.AboveBase)
                            }
                        } else {
                            programData.push(0);
                        }
                    });
                    var total = _.sum(programData);
                    if (total) {
                        dataCategories.push({ data: programData, id: value[0].ProgramId, name: key, total: total })
                    }
                }).value();

            //hack for ghost state 
            if (!categoryType) {
                this.isMonthlyTrendGhostState = true;
                var data = [];
                var value = _.max(_.pluck(dataCategories, "total"));
                for (var i = 0; i < categories.length; i++) {
                    data.push(value)
                };
                dataSeries.push({ data: data });
            }
            else {
                this.isMonthlyTrendGhostState = false;
                this.monthlyTrendByProgramChartTitle = "Program Trend";
                var categoryLength = dataCategories.length - 1;
                angular.forEach(dataCategories, (category) => {
                    dataSeries.push({
                        data: category.data,
                        id: category.id,
                        name: category.name,
                        amount: category.total,
                        index: categoryLength
                    })
                    categoryLength -= 1;
                });
            }
            //// Build the chart
            this.chartsConfig.billingTrendsbyProgramBarChart = this.chartConfigBuilder('billingTrendsbyProgramBarChart', {
                xAxis: {
                    categories: categories,
                    tickColor: this.sitesettings.colors.coreColors.default,
                    type: 'category',
                    labels: {
                        style: {
                            color: this.isMonthlyTrendGhostState ? this.sitesettings.colors.coreColors.default : this.sitesettings.colors.coreColors.baseDark ,
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    }
                },
                yAxis: {
                    labels: {
                        enabled: !this.isMonthlyTrendGhostState,
                        style: {
                            color: this.sitesettings.colors.coreColors.baseDark,
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    title: {
                        text: ''
                    },
                    gridLineDashStyle: 'shortdash',
                    gridLineColor: this.sitesettings.colors.coreColors.border,
                    tickInterval: 10000,
                    min: 0
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>${point.y}</b>',
                    enabled: !this.isMonthlyTrendGhostState
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false
                        },
                    },
                    series: {
                        cursor: !this.isMonthlyTrendGhostState ? 'pointer' :'',
                        states: {
                            hover: {
                                enabled: !this.isMonthlyTrendGhostState
                            }
                        },
                        pointWidth: categories.length == 1 ? 200 : null
                    }
                },
                series: dataSeries,
                legend: {
                    enabled: false
                }
            });
        }
        /**
       * @description By Program.
       */
        private getBillingByProgram = (dateRange: commonInterfaces.IDateRange): void=> {
            this.chartsSvc.getBillingByProgram({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {

                if (result.length > 0) {

                    var programName = [];
                    var amount = [];
                    var data: Array<IBillingProgramData> = [];
                    angular.forEach(result,(item: IBillingProgramData) => {

                        if (item.Amount > 0)
                            data.push(<IBillingProgramData>{ ProgramId: item.ProgramId, ProgramName: item.ProgramName, Amount: item.Amount });
                    });
                    this.chartsConfig.billingProgram = this.chartConfigBuilder('billingprogram', {
                        series: [
                            {
                                type: 'pie',
                                name: 'Amount',
                                innerSize: '60%',
                                data: _.map(data,(d, index) => {
                                    var sliceName = d['ProgramName'],
                                        slideId = d['ProgramId'],
                                        sliceY = d['Amount'];
                                    return {
                                        programId: slideId,
                                        name: sliceName,
                                        y: sliceY,
                                        sliced: index == 0,
                                        selected: index == 0
                                    }
                                }),
                                events: {
                                    click: (event) => {
                                        if (!this.sitesettings.windowSizes.isTiny) {
                                            this.goToDetailsView(event.point.options.programId);
                                            this.$scope.$apply();
                                        }
                                    }
                                }
                            }

                        ]
                    });
                } else {



                    //DW: fix to update visibility of 'Complaints by Program' charts and legend without refresh.
                    this.chartsConfig.billingProgram = [];
                }
            }, this.onFailure);
        }

        private getPaidVsUnpaidBillingTrend = (dateRange: commonInterfaces.IDateRange) => {

            var param = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
            this.apiSvc.query(param, 'FinanceReport/BillingMonthlyPaidVsUpaidAmount').then((result) => {
                if (!result) return;

                this.paidVsUnpaidBillingData = result;
                var paidAmount = _.sum(_.pluck(result, "PaidAmount"));
                var unpaidAmount = _.sum(_.pluck(result, "UnpaidAmount"));

                var dataToPlot = [
                    { Name: 'Paid', Amount: paidAmount },
                    { Name: 'Unpaid', Amount: unpaidAmount }];

                this.showPaidOrUnpaidTrends(null);

                //Build the chart
                this.chartsConfig.paidVsUnpaid = this.chartConfigBuilder('paidVsUnpaid', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Amount',
                            innerSize: '60%',
                            events: {
                                click: (event) => {
                                    this.showPaidOrUnpaidTrends(event.point.name);
                                    this.$scope.$apply();
                                }
                            },
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Amount'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });

            }, this.onFailure);
        }

        private showPaidOrUnpaidTrends = (type: string) => {
            var categories = [];
            var data = [];
            var color = '';

            this.paidOrUpaidType = type;
            this.isPaidOrUnpaidGhostState = false;

            //to get xAxis values
            var noOfMonths = Math.abs(moment(this.dateRangeParam.startDate).diff(moment(this.dateRangeParam.endDate), 'months')) + 1;
            var date = moment(_.clone(this.dateRangeParam.startDate));
            for (var i = 0; i < noOfMonths; i++) {
                categories.push(date.format('MMM \'YY'));
                date.add('month', 1);
            }

            if (type == 'Paid') {
                angular.forEach(categories, (c) => {
                    var item = _.find(this.paidVsUnpaidBillingData, (d) => {
                        return moment(d.ServiceDate).utc().format('MMM \'YY') == c;
                    });
                    if (item) {
                        data.push(item.PaidAmount);
                    } else {
                        data.push(0);
                    }
                });
                this.paidOrUnpaidChartTitle = "Paid Invoices";
                color = this.colors.teal;
            } else if (type == 'Unpaid') {
                angular.forEach(categories, (c) => {
                    var item = _.find(this.paidVsUnpaidBillingData, (d) => {
                        return moment(d.ServiceDate).utc().format('MMM \'YY') == c
                    });
                    if (item) {
                        data.push(item.UnpaidAmount);
                    } else {
                        data.push(0);
                    }
                });
                this.paidOrUnpaidChartTitle = "Unpaid Invoices";
                color = this.colors.blue;
            } else {
                this.isPaidOrUnpaidGhostState = true;
                categories = [1, 2, 3];
                data = [15000, 15000, 15000];
                color = this.sitesettings.colors.coreColors.gray;
            }

            //// Build the chart
            this.chartsConfig.paidOrUnpaid  = this.chartConfigBuilder('paidOrUnpaid', {
                xAxis: {
                    categories: categories,
                    tickColor: this.sitesettings.colors.coreColors.default,
                    type: 'category',
                    labels: {
                        enabled: !this.isPaidOrUnpaidGhostState,
                        style: {
                            color: this.sitesettings.colors.coreColors.baseDark,
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    }
                },
                yAxis: {
                    labels: {
                        enabled: !this.isPaidOrUnpaidGhostState,
                        style: {
                            color: this.sitesettings.colors.coreColors.baseDark,
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                    title: {
                        text: ''
                    },
                    gridLineDashStyle: 'shortdash',
                    gridLineColor: this.sitesettings.colors.coreColors.border,
                    tickInterval: 10000,
                    min: 0
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>${point.y}</b>',
                    enabled: !this.isPaidOrUnpaidGhostState,
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false
                        },
                    },
                    series: {
                        cursor: !this.isPaidOrUnpaidGhostState ? 'pointer' : '',
                        states: {
                            hover: {
                                enabled: !this.isPaidOrUnpaidGhostState
                            }
                        },
                        pointWidth: categories.length == 1 ? 200 : null
                    }
                },
                series: [
                    {
                        name: type + ' Invoices',
                        color: color,
                        events: {
                            click: (event) => {
                                this.goToPaidOrUnpaidView();
                                this.$scope.$apply();
                            }
                        },
                        data: _.map(categories, function (category, i) {
                            return {
                                name: category,
                                y: data[i]

                            }
                        }),
                        amount: _.sum(data)
                    }],
            });
        }

        private goToPaidOrUnpaidView = () => {
            var params = `?programId=${0}&paidStatus=${this.paidOrUpaidType}`;
            this.$window.open(this.basePath + 'Financials/BillingOverview/Details/' + params, '_blank');
        }

        /**
         * @description By Site or Building.
         */
        private getBillingBySite = () => {
            if (!this.billingSiteMonthly || _.all(this.billingSiteMonthly, { 'Total': null })) {
                this.billingSite = false;
                return;
            }
            else {
                this.billingSite = true;

                var categories = [];
                var amount = [];

                angular.forEach(this.billingSiteMonthly, (item) => {
                    let siteName = item.SiteName;
                    if (!item.IsSiteActive) {
                        siteName += ' (Inactive) ';
                    }
                    categories.push(siteName);
                    amount.push(item.Total);

                });

                // Build the chart
                this.chartsConfig.billingSite = this.chartConfigBuilder('billingSite', {

                    series: [
                        {
                            name: 'Amount',
                            data: _.map(categories, function (category, i) {
                                return {
                                    name: category,
                                    y: amount[i]

                                }
                            })
                        }],
                    height: categories.length * 75

                });
            }
        }

        private setMonthsToDisplay = () => {
            this.monthsToDisplay = [];
            do {
                this.monthsToDisplay.push((moment(this.dateRange.startDate).format('MMM') + ' ' + '\'' + moment(this.dateRange.startDate).format('YY')));
                this.dateRange.startDate = moment(this.dateRange.startDate).add(1, 'month');
            }while(moment(this.dateRange.startDate).isBefore(moment(this.dateRange.endDate)))
        }
        /**
         * @description By Site Monthly.
         */
        private getBillingBySiteMonthly = (dateRange: commonInterfaces.IDateRange) => {
            this.setMonthsToDisplay();

            this.chartsSvc.getBillingBySiteMonthly({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result) return;

                this.billingSiteMonthly = [];
                
                angular.forEach(result, (item: IBillingSiteMonthly) => {
                    let monthObj: any;
                    let tempMonths = [];
                    angular.forEach(this.monthsToDisplay, (month) => {
                        let obj = _.find(item.Months, { 'Month': month });
                        if (obj) {
                            monthObj = obj;
                        } else {
                            monthObj = {
                                Month: month,
                                Amount: null
                            };
                        }
                        tempMonths.push(monthObj);
                    });
                    this.billingSiteMonthly.push({
                        SiteName: item.SiteName,
                        Total: item.Months ? item.Total : null,
                        Months: tempMonths,
                        IsSiteActive: item.IsSiteActive
                    });
                });
                this.getBillingBySite();
            });
        }

        private goToDetailsView = (programId: number) => {
            var params = `?programId=${programId}&paidStatus=${null}`;
            this.$window.open(this.basePath + 'Financials/BillingOverview/Details/' + params, '_blank');
        }
        /**
         * @description Defines overridable chart options.
         */
        chartConfigBuilder = (name, options) => {
            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                }
            });

            var chartOptions = {

                // column Chart
                billingTrendsBarChart: {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    height: 350,
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category',
                        labels: {
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                format: '${point.y:,.2f}',
                            },
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    series: []
                },

                billingTrendsbyProgramBarChart: {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    height: 350,
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category',
                        labels: {
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis:
                    {
                        tickInterval: 10000,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            },
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        }
                    },
                    series: []
                },
                //Pie Chart
                billingprogram: {
                    chart: {
                        type: 'pie',
                        height: 300
                    },
                    title: {
                        text: ''
                    },

                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },
                        borderRadius: 15,
                        pointFormat: '<b>${point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true
                            },
                            showInLegend: true,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    legend: {
                        enabled: true,
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        floating: false,
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []

                        }
                    ]
                },

                // bar Chart
                billingSite: {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category',
                        labels: {
                            style: {
                                fontSize: '13px',
                                fontWeight: 'normal'
                            }
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: -20,
                        floating: true,
                        backgroundColor: this.sitesettings.colors.coreColors.default,
                        symbolPadding: 10,
                        symbolWidth: 10,
                        padding: 7,
                        itemMarginTop: 4,
                        itemMarginBottom: 6,
                        lineHeight: '10px'
                    },
                    yAxis: [
                        {
                            //tickInterval: 50000,
                            min: 0,

                            gridLineDashStyle: 'shortdash',
                            gridLineColor: this.sitesettings.colors.coreColors.border,
                            title: {
                                text: ''
                            }
                        }
                    ],
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true,
                                align: 'right',
                                color: 'black',
                                format: '${point.y:,.2f}',
                                y: -23,
                                x: 5,
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 700,
                                    color: 'black'
                                }
                            },
                            groupPadding: this.chartSettings.groupPadding,
                            pointWidth: this.chartSettings.barWidth,
                            color: this.colors.teal
                        }
                    },
                    series: [
                        {
                            name: 'Amount',
                            data: []
                        }
                    ]
                },

                //pie chart
                paidVsUnpaid: {
                    chart: {
                        height: 300
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: false,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'
                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: []
                        }
                    ]
                },

                paidOrUnpaid: {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'charts'
                    },
                    height: 350,
                    xAxis: {
                        tickColor: this.sitesettings.colors.coreColors.default,
                        type: 'category',
                        labels: {
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    yAxis:
                    {
                        tickInterval: 10000,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return (this.value / 1000).toFixed(0) + ' k';
                            },
                            style: {
                                color: this.sitesettings.colors.coreColors.baseDark,
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        },
                        gridLineDashStyle: 'shortdash',
                        gridLineColor: this.sitesettings.colors.coreColors.border,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>${point.y}</b>'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            },
                        }
                    },
                    series: []
                },
            }
            return _.assign(chartOptions[name], options);
        }
    }

    angular.module('app').controller('BillingOverviewIndexCtrl', BillingOverviewIndexCtrl);
} 