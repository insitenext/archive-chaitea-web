﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.MyTimeClock.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    class ChangeOfAddressCtrl {
        basePath: string;

        employeeId: number;
        employeeName: string;
        employeeEmailId: string;
        externalEmployeeId: number;
        supervisorMailId: string;

        address: {
            street: string;
            city: string;
            state: string;
            zipcode: number
        };

        success: number;
        message: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            'chaitea.common.services.mailsvc',
            'chaitea.common.services.apibasesvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            userInfo: IUserInfo,
            private mailSvc: commonSvc.IMailSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
            this.employeeId = userInfo.userId;
            this.employeeEmailId = userInfo.userEmail;
            this.employeeName = 'SBM Employee';
        }

        initialize = (): void => {
            this.getUserInfo();
            this.success = 0;
        }

        onSuccess = (): void => {
            this.success = 1;
            this.message = 'Your Request has been sent successfully';
        }

        onFailure = (): void => {
            this.success = 2;
            this.message = 'Some Problem occurred. Try Again. If problem repeats contact HR directly.';
        }

        getUserInfo = (): void => {
            this.apiSvc.getById(this.employeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee): any => {
                this.employeeName = employee.Name ? employee.Name : 'SBM Employee';
                this.supervisorMailId = employee.SupervisorEmail;
                this.externalEmployeeId = employee.ExternalEmployeeId;
            });
        }
        sendEmail = (): void => {

            //var Url = `${this.fullDomain}/Quality/CustomerSurvey/Survey/${token}`;

            var mailBody = `Dear Supervisor,<br /><br />
                            I am ${this.employeeName}.<br/><br/>
                            My Employee ID : </strong> ${this.externalEmployeeId} <br/><br/>
                            Kindly update my address with the below address.<br/><br/>
                            <strong>Street:</strong> ${this.address.street} <br/>
                            <strong>City:</strong> ${this.address.city} <br/>
                            <strong>State:</strong> ${this.address.state} <br/>
                            <strong>Zipcode:</strong> ${this.address.zipcode} <br/><br /><br />
                            Thank you,<br /><br />${this.employeeName}<br />`;

            this.mailSvc.sendMail({
                FromName: this.employeeName,
                FromEmail: this.employeeEmailId,
                ToName: 'HR Coordinator',
                ToEmail: this.supervisorMailId,
                Subject: 'Request for change of address',
                Message: mailBody,
            }).then(this.onSuccess, this.onFailure);

        }
    }

    angular.module("app").controller("ChangeOfAddressCtrl", ChangeOfAddressCtrl);
} 