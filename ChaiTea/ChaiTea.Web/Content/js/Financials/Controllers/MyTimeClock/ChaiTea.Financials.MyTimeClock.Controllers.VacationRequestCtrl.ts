﻿/// <reference path = "../../../_libs.ts" />

module ChaiTea.Financials.MyTimeClock.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commmonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import vacationRequestInterfaces = ChaiTea.Financials.Interfaces;

    export interface newRow {
        vtnType: string;
        vtnDate: string;
        hrsRequested: number;
    }

    class VacationRequestIndexCtrl {
        vacationType: number;
        vacationDate: Date;
        hoursRequested: number;
        daysAdded: Array<newRow>;
        basepath: string;
        employeeName: string;
        employeeId: number;
        externalEmployeeId: number;
        employeeEmailId: string;
        supervisorMailId: string;

        success: number;
        message: string;
        minDate: Date;

        vacationTypes = [
            { key: 1, value: 'Paid Time Off' },
            { key: 2, value: 'Un-Paid Time Off' }
        ]
        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.mailsvc'
        ];

        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private mailSvc: commmonSvc.IMailSvc) {
            this.basepath = sitesettings.basePath;
            this.daysAdded = [];
            this.employeeEmailId = userInfo.userEmail;
            this.employeeId = userInfo.userId;
            this.minDate = new Date();
        }

        initialize = (): void => {
            this.success = 0;
        }

        onSuccess = (): void => {
            this.success = 1;
            this.message = 'Your Request has been sent successfully';
        }

        onFailure = (): void => {
            this.success = 2;
            this.message = 'Some Problem occurred. Try Again. If problem repeats contact your supervisor directly.';
        }

        addRow = (): void=> {
            var vtype = this.vacationType;
            this.daysAdded.push(<newRow>{
                vtnDate: moment(this.vacationDate).format('dddd - MM/DD/YYYY'),
                vtnType: _(this.vacationTypes)
                    .filter(function (vacationtype) { return vacationtype.key == vtype ; })
                    .pluck('value')
                    .value()[0],
                hrsRequested: this.hoursRequested
            });
            this.vacationDate = null;
            this.vacationType = null;
            this.hoursRequested = null;
        }

        removeRow = ($index): void => {
            this.daysAdded.splice($index, 1);
        }

        submitRequest = (): void => {
            this.apiSvc.getById(this.employeeId, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.TrainingEmployee]).then((employee): any => {
                this.supervisorMailId = employee.SupervisorEmail;
                this.employeeName = employee.Name;
                this.externalEmployeeId = employee.ExternalEmployeeId;

                var Dates = '';
                angular.forEach(this.daysAdded,(data) => {
                    Dates = Dates + `<strong>Date:</strong> ${data.vtnDate} <br/>
                                 <strong>Type:</strong> ${data.vtnType} <br/>
                                 <strong>Hours:</strong> ${data.hrsRequested} <br/><br />`;
                });

                var mailBody = `Dear Supervisor,<br /><br />
                                I am ${this.employeeName}.<br/><br/>
                                My Employee ID : </strong> ${this.externalEmployeeId} <br/><br/>
                                Kindly consider my vacation request for following dates.<br/><br/>
                                ${Dates} Thank you,<br /><br />${this.employeeName}<br />`;

                this.mailSvc.sendMail({
                    FromName: this.employeeName,
                    FromEmail: this.employeeEmailId,
                    ToName: 'Supervisor',
                    ToEmail: this.supervisorMailId,
                    Subject: 'Request for vacation',
                    Message: mailBody,
                }).then(this.onSuccess, this.onFailure);
                this.daysAdded = [];
            });
        }
    }
    angular.module("app").controller("VacationRequestIndexCtrl", VacationRequestIndexCtrl);
}