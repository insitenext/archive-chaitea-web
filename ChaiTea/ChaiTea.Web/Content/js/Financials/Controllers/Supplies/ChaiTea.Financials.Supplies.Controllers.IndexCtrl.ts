﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.Supplies.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import financialSvc = ChaiTea.Financials.Services;
    import financialsInterfaces = ChaiTea.Financials.Interfaces;

    class SuppliesIndexCtrl {

        moment = moment;
        siteId: number = 0;
        failColor: string = '#EF5C48';
        subCommodityBillableTitle: string;
        subCommodityGreenTitle: string;
        lastUpdatedDate: string;

        urlParams = {
            isBillable: 'null',
            category: 'null',
            subCategory: 'null',
            isGreen:'null'
        }

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        basePath: string;
        billableCommodities = [];
        nonBillableCommodities = [];
        greenCommodities = [];
        notGreenCommodities = [];
        billable = {};
        nonBillable = {}
        green = {};
        notGreen = {};
        //For Billable vs Green

        makeBillableActive: boolean = true;
        makeGreenActive: boolean = false;

        // For Billable vs Non-billable
        isBillable: boolean = false;
        isNonBillable: boolean = false;

        // For Green vs Not-green

        isGreen: boolean = false;
        isNotGreen: boolean = false;

        isProgram: boolean = true;
        suppliesSite: boolean = false;
        singleSite: boolean = false;
        chartSettings: IChartSettings;
        monthsToDisplay = [];
        suppliesSiteMonthly: Array<financialsInterfaces.IBillingSiteMonthly> = [];
        siteName: string = "All Sites";
        programName: string = "All Programs";

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.financials.services.reportssvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            '$window'
        ];

        valueFormatter = function (val) {
            return val.toFixed(0) + '%';
        };
        reportsConfig = {
            goalLine: 90,
            suppliesMonthlyProductTrends: {},
            suppliesSpendByCategory: {},
            suppliesSpendBillable: {},
            suppliesSpendSubCommodityBillable: {},
            suppliesSpendGreen: {},
            suppliesSpendSubCommodityGreen: {},
            suppliesByProgram: {},
            suppliesMonthlyProgramTrend: {},
            suppliesBillingBySite: {},
            suppliesTrendByProgram: {}
        };

        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private chartsSvc: financialSvc.IReportsSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private $window: angular.IWindowService) {

            this.basePath = sitesettings.basePath;
            this.siteId = userContext.Site.ID;
            this.chartSettings = sitesettings.chartSettings;
            this.siteName = userContext.Site.ID != 0 ? userContext.Site.Name : this.siteName;
            this.programName = userContext.Program.ID != 0 ? userContext.Program.Name : this.programName;
            this.isProgram = userContext.Program.ID == 0 ? false : true;
            console.log(this.isProgram);
            this.reportsConfig.suppliesMonthlyProductTrends = this.chartConfigBuilder('suppliesMonthlyProductTrends', {});
            this.reportsConfig.suppliesSpendByCategory = this.chartConfigBuilder('suppliesSpendByCategory', {});
            this.reportsConfig.suppliesSpendBillable = this.chartConfigBuilder('suppliesSpendBillable', {});
            this.reportsConfig.suppliesSpendSubCommodityBillable = this.chartConfigBuilder('suppliesSpendSubCommodityBillable', {});
            this.reportsConfig.suppliesSpendGreen = this.chartConfigBuilder('suppliesSpendGreen', {});
            this.reportsConfig.suppliesSpendSubCommodityGreen = this.chartConfigBuilder('suppliesSpendSubCommodityGreen', {});
            this.reportsConfig.suppliesByProgram = this.chartConfigBuilder('suppliesSpendByProgram', {});
            this.reportsConfig.suppliesBillingBySite = this.chartConfigBuilder('suppliesBillingBySite', {});
            this.reportsConfig.suppliesMonthlyProgramTrend = this.chartConfigBuilder('suppliesMonthlyProgramTrend', {});
            this.reportsConfig.suppliesTrendByProgram = this.chartConfigBuilder('suppliesTrendByProgram', {})
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;

                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });
            this.getLatestUpdateDate();

        }

        /** @description Initializer for the controller. */
        private initialize = (): void => { }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }


        /**
         * @description supplies montly Trends.
         */
        private getSuppliesMonthlyProductTrends = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getSuppliesMonthlyProductTrends({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result) return;

                var index = result.Labels.length - 1;
                var categories = _.dropRight(result.Labels, 1);


                var dataSeries = [];
                var dataCategories = [];
                
                angular.forEach(result.Categories, function (value, key) {
                    var total: number = 0;
                    total = value[index];
                    value = _.dropRight(value, 1);
                    dataCategories.push({data: value, name: key, total: total})
                });

                dataCategories.sort(function (a, b) {
                    return b.total - a.total
                });
                
                var categoryLength = dataCategories.length - 1;
                angular.forEach(dataCategories, (category) =>{
                    dataSeries.push({ data: category.data, name: category.name, amount: category.total, index: categoryLength })
                    categoryLength -= 1;
                });
             
                // Build the chart
                this.reportsConfig.suppliesMonthlyProductTrends = this.chartConfigBuilder('suppliesMonthlyProductTrends', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    yAxis: {
                        stackLabels: {
                            enabled: true,
                            style: {
                                color: 'black'
                            }
                        },
                        title: null,
                        // tickInterval: result.Accepted.length > 10 ? 10 : null,
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: dataSeries,
                    legend: {
                        enabled: false
                    }
                });
            })
        }

        /**
         * @description By Program or Type.
         */
        private getSuppliesSpendByCategory = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getSuppliesSpendByCategory({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.length) return;

                var data = result;
               
                // Build the chart
                this.reportsConfig.suppliesSpendByCategory = this.chartConfigBuilder('suppliesSpendByCategory', {
                    series: [
                        {
                            type: 'pie',
                            name: 'suppliesSpendByCategory',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                }); 
                    
            });
        }


        /**
        * @description By Billable.
        */
        private getSuppliesSpendBillable = (dateRange: commonInterfaces.IDateRange) => {
            this.chartsSvc.getSuppliesSpendBillable({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }).then((result) => {
                if (!result.length) return;

                var data = result;
                var index = _.findIndex(data, { 'Percentage': 100 });
                if (index >= 0) {
                    if (data[index].Name == 'Billable') {
                        data.push({
                            Id: null,
                            IsBillable: false,
                            Name: 'Non-Billable',
                            Percentage: 0
                        });
                    }
                    else if (data[index].Name == 'Non-Billable') {
                        data.push({
                            Id: null,
                            IsBillable: true,
                            Name: 'Billable',
                            Percentage: 0
                        });
                    }
                }
                var dataToPlot = _.uniq(([]).concat(
                    _.where(data, { "Name": "Billable" }),
                    _.where(data, { "Name": "Non-Billable" })));

                this.billable = _.where(data, { "Name": "Billable" });
             
                this.nonBillable = _.where(data, { "Name": "Non-Billable" });
                this.billableCommodities = _.where(data, { "IsBillable": true });
                this.billableCommodities = _.drop(this.billableCommodities, 1);
                this.billableCommodities.sort(function (a, b) {
                    return b.Percentage - a.Percentage
                });
                this.nonBillableCommodities = _.where(data, { "IsBillable": false });
                this.nonBillableCommodities = _.drop(this.nonBillableCommodities, 1);
                this.nonBillableCommodities.sort(function (a, b) {
                    return b.Percentage - a.Percentage
                });
                // Build the chart
                this.reportsConfig.suppliesSpendBillable = this.chartConfigBuilder('suppliesSpendBillable', {
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });

            });
        }

        /*
        * By Sub-Commodity Billable.
        */
        private getSuppliesSpendSubCommodityBillable = (name: string, commodityId: number, isBillable: boolean) => {
            isBillable ? this.urlParams.isBillable = 'true' : this.urlParams.isBillable = 'false';
            this.urlParams.category = name;
            this.chartsSvc.getSuppliesSpendSubCommodityBillable({
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                commodityId: commodityId,
                isBillable: isBillable
            }).then((result) => {
                if (!result.length) return;
                var data = result;
                this.subCommodityBillableTitle = name[0].toUpperCase() + name.substr(1).toLowerCase();
                // Build the chart
                this.reportsConfig.suppliesSpendSubCommodityBillable = this.chartConfigBuilder('suppliesSpendSubCommodityBillable', {
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                });

                });
        }

        private getSuppliesSpendGreen = () => {
            var startDate = this.dateFormatterSvc.formatDateShort(this.dateRange.startDate);
            var endDate = this.dateFormatterSvc.formatDateShort(this.dateRange.endDate);
            this.chartsSvc.getSuppliesSpendGreen({
                startDate: startDate,
                endDate: endDate
            }).then((result) => {
                if (!result.length) return;

                var data = result;

                var index = _.findIndex(data, { 'Percentage': 100 });
                if (index >= 0) {
                    if (data[index].Name == 'Green') {
                        data.push({
                            Id: null,
                            IsGreen: false,
                            Name: 'Non-Green',
                            Percentage: 0
                        });
                    }
                    else if (data[index].Name == 'Non-Green') {
                        data.push({
                            Id: null,
                            IsGreen: true,
                            Name: 'Green',
                            Percentage: 0
                        });
                    }
                }

                var dataToPlot = _.uniq(([]).concat(
                    _.where(data, { "Name": "Green" }),
                    _.where(data, { "Name": "Non-Green" })));

               
              
                this.green = _.where(data, { "Name": "Green" });
                this.greenCommodities = _.where(data, { "IsGreen": true });
                this.greenCommodities = _.drop(this.greenCommodities, 1);
                this.greenCommodities.sort(function (a, b) {
                    return b.Percentage - a.Percentage
                });
                this.notGreen = _.where(data, { "Name": "Non-Green" });
                this.notGreenCommodities = _.where(data, { "IsGreen": false });
                this.notGreenCommodities = _.drop(this.notGreenCommodities, 1);
                this.notGreenCommodities.sort(function (a, b) {
                    return b.Percentage - a.Percentage
                });
                // Build the chart
                this.reportsConfig.suppliesSpendGreen = this.chartConfigBuilder('suppliesSpendGreen', {
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                });

            });
        }

        /*
      * By Sub-Commodity Green.
      */
        private getSuppliesSpendSubCommodityGreen = (name: string, commodityId: number, isGreen: boolean) => {
            isGreen ? this.urlParams.isGreen = 'true' : this.urlParams.isGreen = 'false';
            this.urlParams.category = name;
            this.chartsSvc.getSuppliesSpendSubCommodityGreen({
                startDate: this.dateFormatterSvc.formatDateShort(this.dateRange.startDate),
                endDate: this.dateFormatterSvc.formatDateShort(this.dateRange.endDate),
                commodityId: commodityId,
                isGreen: isGreen
            }).then((result) => {
                if (!result.length) return;
                var data = result;
                this.subCommodityGreenTitle = name[0].toUpperCase() + name.substr(1).toLowerCase();
                // Build the chart
                this.reportsConfig.suppliesSpendSubCommodityGreen = this.chartConfigBuilder('suppliesSpendSubCommodityGreen', {
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    sliced: index == 0,
                                    selected: index == 0,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }]
                });

            });
        }

        goToDetails = (param: any, isSubCategory: boolean): void => {
            isSubCategory ? this.urlParams.subCategory = param : this.urlParams.category = param;
            var params = `?isBillable=${this.urlParams.isBillable}&isGreen=${this.urlParams.isGreen}&category=${this.urlParams.category}&subCategory=${this.urlParams.subCategory}`;
            this.$window.location.href = this.basePath + 'Financials/Supplies/Details/' + params 
        }

        /**
        * @description Get complaints by type and update the complaints by type model.
        */
        private getSuppliesSpendByProgram = (dateRange: commonInterfaces.IDateRange): void=> {
            this.apiSvc.query({ startDate: dateRange.startDate, endDate: dateRange.endDate },
                "FinanceReport/SpendByProgram/:startDate/:endDate")
                .then((result) => {
                    if (!result || result.length == 0) {
                        return;
                    }
                    var data = result;
                    // Build the chart
                    this.reportsConfig.suppliesByProgram = this.chartConfigBuilder('suppliesSpendByProgram', {
                        series: [
                            {
                                type: 'pie',
                                name: '',
                                data: _.map(data, (d, index) => {
                                    var sliceId = d['ProgramId'],
                                        sliceName = d['ProgramName'],
                                        sliceY = d['Percentage'];
                                    return {
                                        name: sliceName,
                                        y: sliceY,
                                        sliced: index == 0,
                                        selected: index == 0,

                                        // Attach click event to each slice to re-render the line chart
                                        events: {
                                            click: ($event) => {
                                                this.getSuppliesTrendsByProgram(dateRange, {
                                                    id: sliceId,
                                                    name: sliceName.toUpperCase(),
                                                    color: $event.point.color
                                                })
                                            }
                                        },
                                        marker: {
                                            enabled: true,
                                            symbol: 'circle'
                                        }
                                    }
                                })
                            }]
                    }); 
                    
                    // Build the line chart based on the biggest slice of the pie on initial render
                    this.getSuppliesTrendsByProgram(dateRange, {
                        id: data[0]['ProgramId'],
                        name: data[0]['ProgramName'],
                    });

            }, this.onFailure);
        }

        /**
         * @description Get trends by program or type.
         */
        private getSuppliesTrendsByProgram = (dateRange: commonInterfaces.IDateRange, selectedItem) => {
            this.apiSvc.query({ startDate: dateRange.startDate, endDate: dateRange.endDate, programId: selectedItem.id },
                "FinanceReport/SpendByProgramTrend/:startDate/:endDate/:programId").then((result) => {
                    if (!result) {
                        return;
                    }
                    var categories = [],
                        amount = [];

                    angular.forEach(result[0].Months, (month) => {
                        categories.push(month.Month);
                        amount.push(month.Amount);
                    });
                this.reportsConfig.suppliesTrendByProgram = this.chartConfigBuilder('suppliesTrendByProgram', {
                    categories: categories,
                    series: [
                        {
                            color: selectedItem.color, //'#f26e54',
                            lineWidth: 3,
                            name: selectedItem.name,
                            data: amount
                        }
                    ]
                });
            });
        }

        /**
         * @description By Site or Building.
         */
        private getSuppliesBillingBySite = (dateRange: commonInterfaces.IDateRange) => {
            this.apiSvc.query({ startDate: dateRange.startDate, endDate: dateRange.endDate },
                "FinanceReport/SpendBySite/:startDate/:endDate").then((result) => {
                if (result.length == 1) {
                    this.singleSite = true;
                    return;
                }
                else if (result.length > 0) {
                    if (result.length == 1 && result[0].Amount == 0)
                        this.suppliesSite = false;
                    else
                        this.suppliesSite = true;

                    var categories = [];
                    var amount = [];

                    angular.forEach(result, (item) => {
                        categories.push(item.SiteName);
                        amount.push(item.Amount);

                    });
            
                    // Build the chart
                    this.reportsConfig.suppliesBillingBySite = this.chartConfigBuilder('suppliesBillingSite', {
                        series: [
                            {
                                name: 'Amount',
                                data: _.map(categories, function (category, i) {
                                    return {
                                        name: category,
                                        y: amount[i]

                                    }
                                })
                            }],
                        height: categories.length * 75

                    });
                }
                else
                    this.suppliesSite = false;
            });
        }

        /**
         * @description By Site Monthly.
         */
        private getSuppliesBySiteMonthly = (dateRange: commonInterfaces.IDateRange) => {
            this.setMonthsToDisplay();

            this.apiSvc.query({ startDate: dateRange.startDate, endDate: dateRange.endDate },
                "FinanceReport/SpendBySiteMonthly/:startDate/:endDate").then((result) => {
                if (!result) return;

                this.suppliesSiteMonthly = [];
                angular.forEach(result, (item: financialsInterfaces.IBillingSiteMonthly) => {
                    this.suppliesSiteMonthly.push({
                        SiteName: item.SiteName,
                        Total: item.Total,
                        Months: item.Months
                    });
                });

            });
        }

        /**
        * @description supplies montly Trends.
        */
        private getSuppliesMonthlyProgramTrend = (dateRange: commonInterfaces.IDateRange) => {
            this.apiSvc.query({ startDate: dateRange.startDate, endDate: dateRange.endDate },
                "FinanceReport/SpendByProgramMonthly/:startDate/:endDate")
                .then((result) => {
                if (!result || result.length == 0) return;

                //var index = result.Labels.length - 1;
                var categories = [];
                angular.forEach(result[0].Months, (month) => {
                    categories.push(month.Month);
                });
                
                
                var dataSeries = [];
                result.sort(function (a, b) {
                    return b.Total - a.Total
                });
       
                var index = result.length;
                angular.forEach(result, (program) => {
                    var amount = [];
                   
                    angular.forEach(program.Months, (month) => {
                        amount.push(month.Amount);
                    });
                    dataSeries.push({ data: amount, name: program.ProgramName, amount: program.Total, index: index });
                    index -= 1;
                });
              
              
                // Build the chart
                this.reportsConfig.suppliesMonthlyProgramTrend = this.chartConfigBuilder('suppliesMonthlyProductTrends', {
                    xAxis: {
                        tickColor: 'white',
                        categories: categories,
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '400',
                            }
                        }
                    },
                    yAxis: {
                        title: null,
                        labels: {
                            style: {
                                color: '#393f44',
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: dataSeries,
                    legend: {
                        enabled: false
                    }
                 });
                
            })
        }

        private setMonthsToDisplay = () => {
            this.monthsToDisplay = [];
            do {
                this.monthsToDisplay.push((moment(this.dateRange.startDate).format('MMM') + ' ' + '\'' + moment(this.dateRange.startDate).format('YY')));
                this.dateRange.startDate = moment(this.dateRange.startDate).add(1, 'month');
            } while (moment(this.dateRange.startDate).isBefore(moment(this.dateRange.endDate)))
        }

        
        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            if (this.isProgram) {
                this.reportsConfig.suppliesMonthlyProductTrends = {};
                this.reportsConfig.suppliesSpendByCategory = {};
                this.reportsConfig.suppliesSpendBillable = {};
                this.reportsConfig.suppliesSpendSubCommodityBillable = {};
                this.reportsConfig.suppliesSpendGreen = {};
                this.reportsConfig.suppliesSpendSubCommodityGreen = {};
                this.getSuppliesMonthlyProductTrends(dateRange);
                this.getSuppliesSpendByCategory(dateRange);
                this.getSuppliesSpendBillable(dateRange);
            }
            else {
                this.reportsConfig.suppliesByProgram = {};
                this.reportsConfig.suppliesMonthlyProgramTrend = {},
                this.reportsConfig.suppliesBillingBySite = { };
                this.getSuppliesMonthlyProgramTrend(dateRange);
                this.getSuppliesSpendByProgram(dateRange);
                this.getSuppliesBillingBySite(dateRange);
                this.getSuppliesBySiteMonthly(dateRange);
            }
        }

        /**
         * @description Defines overridable chart options.
         */
        private chartConfigBuilder = (name, options) => {
            
            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },
               
             
            });

                    var chartOptions = {

                        // Column Chart
                        suppliesMonthlyProductTrends: {
                            title: {
                                text: '',
                            },
                            credits: {
                                enabled: false,
                            },

                            legend: {
                                enabled: false
                            },
                            tooltip: {
                                borderRadius: 15,
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'
                                }
                            },
                            plotOptions: {
                                column: {
                                    showInLegend: false
                                },
                                series: {
                                    marker: {
                                        symbol: 'circle',
                                        fillColor: '#FFFFFF',
                                        lineWidth: 2,
                                        radius: 5,
                                        lineColor: null
                                    },
                                    
                                }
                                
                            },
                            yAxis: {
                                title: '',
                                tickInterval: 5,
                                gridLineDashStyle: 'shortdash',
                                gridLineColor: '#e4e4e4',
                                plotLines: [
                                    {
                                        value: 98,
                                        color: '#6C9DC7',
                                        width: 4,
                                        zIndex: 4,
                                        label: { text: '' }
                                    }
                                ],
                                labels: {
                                    style: {
                                        color: '#393f44',
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }
                            },
                            xAxis: {
                                tickColor: 'white',
                                labels: {
                                    style: {
                                        color: '#393f44',
                                        fontSize: '12px',
                                        fontWeight: '400',
                                    }
                                }
                            },
                            series: [
                                {
                                    color: '#edb326',
                                    name: 'Completion',
                                    lineWidth: 100
                                }
                            ]
                        },
                        // Pie Chart
                        suppliesSpendByCategory: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: true,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        suppliesSpendBillable: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    size: 100,
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        // Pie chart
                        suppliesSpendSubCommodityBillable: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        //Pie Chart
                        suppliesSpendGreen: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        // Pie chart
                        suppliesSpendSubCommodityGreen: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        suppliesSpendByProgram: {
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: true,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        // Bar Chart
                        suppliesBillingSite: <HighchartsOptions>{
                            title: {
                                text: ''
                            },
                            xAxis: {
                                tickColor: 'white',
                                type: 'category',
                                labels: {
                                    style: {
                                        fontSize: '13px',
                                        fontWeight: 'normal'
                                    }
                                }
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                align: 'right',
                                verticalAlign: 'top',
                                x: 0,
                                y: -20,
                                floating: true,
                                backgroundColor: '#fff',
                                symbolPadding: 10,
                                symbolWidth: 10,
                                padding: 7,
                                itemMarginTop: 4,
                                itemMarginBottom: 6,
                                lineHeight: '10px'
                            },
                            yAxis: [
                                {
                                    //tickInterval: 50000,
                                    min: 0,

                                    gridLineDashStyle: 'shortdash',
                                    gridLineColor: '#e4e4e4',
                                    title: {
                                        text: ''
                                    }
                                }
                            ],
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true,
                                        align: 'right',
                                        color: '#FFFFFF',
                                        x: -10,
                                    },
                                    groupPadding: this.chartSettings.groupPadding,
                                    pointWidth: this.chartSettings.barWidth
                                }
                            },
                            series: [
                                {
                                    name: 'Amount',
                                    data: []
                                }
                            ]
                        },

                        // Line Chart
                        suppliesTrendByProgram: {
                            legend: {
                                enabled: true,
                                labelFormatter: function () { return this.name.toUpperCase() + " TREND"; },
                                align: 'right',
                                borderWidth: 0,
                                verticalAlign: 'top',
                                floating: true,
                                x: 0,
                                y: -10
                            },
                            title: {
                                text: ''
                            },

                            categories: [],
                            xAxis: {
                                allowDecimals: false,
                                tickWidth: 0,
                                plotBands: [
                                    {

                                    }
                                ],
                                labels: {
                                    style: {
                                        color: '#393f44',
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }
                            },
                            yAxis: {
                                title: '',
                                allowDecimals: false,
                                gridLineDashStyle: 'shortdash',
                                gridLineColor: '#e4e4e4',
                                labels: {
                                    style: {
                                        color: '#393f44',
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }

                            },
                            tooltip: {
                                borderRadius: 15,
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            plotOptions: {
                                series: {
                                    marker: {
                                        symbol: 'circle',
                                        fillColor: '#FFFFFF',
                                        lineWidth: 2,
                                        radius: 5,
                                        lineColor: null // inherit from series
                                    }
                                }
                            },
                            series: [
                                {
                                    color: '#f26e54',
                                    lineWidth: 3,
                                    events: {
                                        legendItemClick: () => (false)
                                    }
                                }]

                        }

                    }
                    return _.assign(chartOptions[name], options);
        }

        // For Billable vs non-Billable

        private showBillable = (): void => {
            this.isBillable = !this.isBillable;
            this.isNonBillable = false;
        }

        private showNonBillable = (): void => {
            this.isNonBillable = !this.isNonBillable;
            this.isBillable = false;
        }

        //for Billable vs Green

        private changeToBillable = (): void => {
            this.makeBillableActive = true;
            this.makeGreenActive = false;
            this.isBillable = false;
            this.isNonBillable = false;
            this.subCommodityBillableTitle = "";
        }

        private changeToGreen= (): void => {
            this.makeGreenActive = true;
            this.makeBillableActive = false;
            this.isGreen = false;
            this.isNotGreen = false;
            this.subCommodityGreenTitle = "";
            this.getSuppliesSpendGreen();
        }

        // for green vs not green

        private showGreen = (): void => {
            this.isGreen = !this.isGreen;
            this.isNotGreen = false;
        }

        private showNotGreen = (): void => {
            this.isNotGreen = !this.isNotGreen;
            this.isGreen = false;
        }

        private getLatestUpdateDate = (): void => {
            this.chartsSvc.getLatestUpdateDate().then((result) => {
                this.lastUpdatedDate = result.LastUpdatedDate;
            });
        }


    }
    angular.module("app").controller("SuppliesIndexCtrl", SuppliesIndexCtrl);
}
