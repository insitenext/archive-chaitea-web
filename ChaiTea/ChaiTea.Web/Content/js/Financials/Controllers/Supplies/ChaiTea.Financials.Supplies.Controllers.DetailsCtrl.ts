﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Financials.Supplies.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    class SuppliesDetailsCtrl {
        
        basePath: string;
        purchaseOrders = [];
        showFilter: boolean = false;
        top: number = 20;
        recordsToSkip: number = 0;
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        fromIndexPage: boolean = false;

        filtersFromIndexPage = {
            isBillable: null,
            isGreen: null,
            CommodityName: '',
            SubCommodityName: ''
        };

        filterOptions = [
            { Key: 'ManufacturerName', Value: 'Manufacturer Name', Options: null, FieldType: 'text' },
            {
                Key: 'IsBillable', Value: 'Billable Flag', Options: [
                    { Key: 'yes', Value: 'Yes' },
                    { Key: 'no', Value: 'No' },
                ], FieldType: 'select'
            },
            {
                Key: 'IsGreen', Value: 'Green Flag', Options: [
                    { Key: 'yes', Value: 'Yes' },
                    { Key: 'no', Value: 'No' },
                ], FieldType: 'select' },
            { Key: 'CommodityName', Value: 'Commodity', Options: null, FieldType: 'text' },
            { Key: 'SubCommodityName', Value: 'Sub Commodity', Options: null, FieldType: 'text' }
        ];

        filter = {
            filterType: null,
            filterCriteria: null 
        };

        filterTypes = {
            IsBillable: null,
            IsGreen: null,
            Category: null
        }

        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            '$uibModal'
        ];

        constructor(
            private $scope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $uibModal:angular.ui.bootstrap.IModalService
        ) {
            this.basePath = sitesettings.basePath;
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = moment(obj.endDate).endOf('day');

                this.getPurchaseOrdersReset();
            });
        }

        private initialize = (filterParam: any): void => {
            if (!(filterParam.category == null) && !(filterParam.subCategory == null) && !(filterParam.isBillable == null) && !(filterParam.isGreen == null)) {
                this.fromIndexPage = true;

                if (filterParam.category != 'null') {
                    this.filtersFromIndexPage.CommodityName = filterParam.category;
                }

                if (filterParam.subCategory != 'null') {
                    this.filtersFromIndexPage.SubCommodityName = filterParam.subCategory;

                    if (filterParam.isBillable != 'null'){
                        String(filterParam.isBillable).toLowerCase() == 'true' ? this.filtersFromIndexPage.isBillable = true : this.filtersFromIndexPage.isBillable = false;
                    }

                    if (filterParam.isGreen != 'null') {
                        String(filterParam.isGreen).toLowerCase() == 'true' ? this.filtersFromIndexPage.isGreen = true : this.filtersFromIndexPage.isGreen = false;
                    }
                }
            }
        }

        private getPurchaseOrdersReset = (): void => {
            this.purchaseOrders = [];
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.getPurchaseOrders();
        }

        private getFilter = (): void => {
            this.showFilter = (this.showFilter ? false : true);
            this.filterTypes.IsBillable = null;
            this.filterTypes.IsGreen = null;
            this.filterTypes.Category = null;
        }

        private cancel = (): void => {
            this.filterTypes.IsBillable = null;
            this.filterTypes.IsGreen = null;
            this.filterTypes.Category = null;
            this.getPurchaseOrdersReset();
        }

        private getPurchaseOrders = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var params = this.getPagingParams();
            this.apiSvc.getByOdata(params, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.POItem]).then((result): any => {
                if (result.length == 0) {
                    this.isAllLoaded = true;
                }
                angular.forEach(result, (obj) => {
                    this.purchaseOrders.push(obj);    
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            });
        }

        private getPagingParams = () => {
            var startDate = this.dateFormatterSvc.formatDateFull(this.dateRange.startDate);
            var endDate = this.dateFormatterSvc.formatDateFull(this.dateRange.endDate);
            var params = <IODataPagingParamModel>{
                $orderby: 'CommitmentDate desc',
                $top: this.top,
                $skip: this.recordsToSkip,
                $filter: `CommitmentDate ge DateTime'${startDate}' and CommitmentDate le DateTime'${endDate}'`
            };

            if (this.filterTypes.IsBillable != null) {
                var filter = '';

                filter = `IsBillable eq ${this.filterTypes.IsBillable}`;

                params['$filter'] += `and ${filter}`;
            }
            if (this.filterTypes.IsGreen != null) {
                var filter = '';

                filter = `IsGreen eq ${this.filterTypes.IsGreen}`;

                params['$filter'] += ` and ${filter}`;
            }
            if (this.filterTypes.Category != null) {
                var filter = '';
                filter = `startswith(${'CommodityName'}, '${this.filterTypes.Category}')`
                params['$filter'] += ` and ${filter}`;
            }

            if (this.fromIndexPage) {

                var indexFilter = '';
                if (this.filtersFromIndexPage.CommodityName != '') {
                    indexFilter += ` and startswith(${'CommodityName'}, '${this.filtersFromIndexPage.CommodityName}')`;
                }

                if (this.filtersFromIndexPage.SubCommodityName != '') {
                    indexFilter += ` and startswith(${'SubCommodityName'}, '${this.filtersFromIndexPage.SubCommodityName}')`;
                }

                if (this.filtersFromIndexPage.isBillable != null) {
                    this.filtersFromIndexPage.isBillable ? indexFilter += ` and IsBillable eq true` : indexFilter += ` and IsBillable eq false`;
                }

                if (this.filtersFromIndexPage.isGreen != null) {
                    this.filtersFromIndexPage.isGreen ? indexFilter += ` and IsGreen eq true` : indexFilter += ` and IsGreen eq false`;
                }
               
                params['$filter'] += `${indexFilter}`;
            }

            if (this.userContext.Program.Name) {
                filter = `ProgramName eq '${this.userContext.Program.Name}'`;
                params['$filter'] += ` and ${filter}`;
            }

            if (this.userContext.Site.Name) {
                filter = `SiteId eq ${this.userContext.Site.ID}`;
                params['$filter'] += ` and ${filter}`;
            }

            return params;
        }

        private getPurchaseOrderScroll = () => {
            this.getPurchaseOrders();
        }

        private export = (): void => {
            var exportStyle = {
                headers: true,
                column: { style: { Font: { Bold: "1" } } }
            };
            var purchaseOrdersToExport = [];
            for (var i = 0; i < this.purchaseOrders.length; i++) {
                purchaseOrdersToExport.push({
                    "SBM #": this.purchaseOrders[i].SBMNumber,
                    "MFG #": this.purchaseOrders[i].ManufacturerNumber,
                    "MFG Code": this.purchaseOrders[i].ManufacturerName,
                    "Description 1": this.purchaseOrders[i].DescriptionOne,
                    "Description 2": this.purchaseOrders[i].DescriptionTwo,
                    "Date": moment(this.purchaseOrders[i].CommitmentDate).format('L'),
                    "Program": this.purchaseOrders[i].CommodityName,
                    "Quantity": this.purchaseOrders[i].Quantity,
                    "Amount": "$" + this.purchaseOrders[i].Amount
                });
            }
            alasql('SELECT * INTO CSV("Purchase-Orders-list.csv",?)FROM ?', [exportStyle, purchaseOrdersToExport]);
        }
    
        public popMobileModal = (templateUrl) => {
            console.log('uibmodal', templateUrl);
            this.$uibModal.open({
                windowClass: 'utility-filter-modal',
                templateUrl: templateUrl,
                scope: this.$scope,
                controllerAs: '$ctrl'
            });
        }
    }

    angular.module("app").controller("SuppliesDetailsCtrl", SuppliesDetailsCtrl);
}
