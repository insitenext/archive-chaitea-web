﻿/// <reference path="../../_libs.ts" />

module ChaiTea.User.Controllers {
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    interface IHobbyInfo {
        HobbyId: User.Enums.Hobby;
        Name: string;
        Category: User.Enums.HobbyCategory;
        Icon: string;
        IsChecked: boolean;
        IsActive: any;
        UserHobbyId: number;
        Color: string;
    }

    interface IFamilyInfo {
        StickerSet: User.Enums.StickerSet;
        SetMember: User.Enums.SetMember;
        MemberLabel: string;
        IsChecked: boolean;
        UserStickerId: number;
        Icon: string;
        FontSize: number;
    }

    interface ITravelInfo {
        CountryId: number;
        Name: string;
        IsChecked: boolean;
        IsActive: boolean;
        UserTravelId: number;
    }

    class ProfileIndexCtrl implements commonInterfaces.INgController {
        userContextId: number;
        user = {
            userId: 0,
            socialPictures: []
        };
        userId;
        orgUserId;
        employee: Training.Interfaces.IJobDescriptionExtended;
        socialPhotos: any[];
        Name: any;
        files = [];
        isCurrentUser: boolean = false;
        isManager: boolean = false;

        defaultProfileImage: string = '';
        saveUserId: number;
        staticAllHobbyData: Array<IHobbyInfo> = [
            {
                HobbyId: User.Enums.Hobby.Art,
                Name: 'Art',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-art',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.teal
            },
            {
                HobbyId: User.Enums.Hobby.Beach,
                Name: 'Beach',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-beach',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blue
            },
            {
                HobbyId: User.Enums.Hobby.Biking,
                Name: 'Biking',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-biking',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lime
            },
            {
                HobbyId: User.Enums.Hobby.Boating,
                Name: 'Boating',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-boating',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lavendar
            },
            {
                HobbyId: User.Enums.Hobby.Camping,
                Name: 'Camping',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-camping',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.orange
            },
            {
                HobbyId: User.Enums.Hobby.Cooking,
                Name: 'Cooking',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-cooking',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueSteel
            },
            {
                HobbyId: User.Enums.Hobby.Exercise,
                Name: 'Exercise',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-exercise',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.pink
            },
            {
                HobbyId: User.Enums.Hobby.Hiking,
                Name: 'Hiking',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-hiking',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.tealDark
            },
            {
                HobbyId: User.Enums.Hobby.MartialArts,
                Name: 'Martial Arts',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-martial-arts',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueDark
            },
            {
                HobbyId: User.Enums.Hobby.Music,
                Name: 'Music',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-music',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.limeDark
            },
            {
                HobbyId: User.Enums.Hobby.Photography,
                Name: 'Photography',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-photography',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lavendarDark
            },
            {
                HobbyId: User.Enums.Hobby.Skating,
                Name: 'Skating',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-skating',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.orangeDark
            },
            {
                HobbyId: User.Enums.Hobby.Swimming,
                Name: 'Swimming',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-swimming',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueSteelDark
            },
            {
                HobbyId: User.Enums.Hobby.Travel,
                Name: 'Travel',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-travel',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.pinkDark
            },
            {
                HobbyId: User.Enums.Hobby.Writing,
                Name: 'Writing',
                Category: User.Enums.HobbyCategory.General,
                Icon: 'fa-hobbies-writing',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.teal
            },
            {
                HobbyId: User.Enums.Hobby.Archery,
                Name: 'Archery',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-archery',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blue
            },
            {
                HobbyId: User.Enums.Hobby.Baseball,
                Name: 'Baseball',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-baseball',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lime
            },
            {
                HobbyId: User.Enums.Hobby.Basketball,
                Name: 'Basketball',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-basketball',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lavendar
            },
            {
                HobbyId: User.Enums.Hobby.Bowling,
                Name: 'Bowling',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-bowling',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.orange
            },
            {
                HobbyId: User.Enums.Hobby.Darts,
                Name: 'Darts',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-darts',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueSteel
            },
            {
                HobbyId: User.Enums.Hobby.Football,
                Name: 'Football',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-football',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.pink
            },
            {
                HobbyId: User.Enums.Hobby.Golf,
                Name: 'Golf',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-golf',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.tealDark
            },
            {
                HobbyId: User.Enums.Hobby.Hockey,
                Name: 'Hockey',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-hockey',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueDark
            },
            {
                HobbyId: User.Enums.Hobby.PingPong,
                Name: 'Ping Pong',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-pingpong',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.limeDark
            },
            {
                HobbyId: User.Enums.Hobby.Pool,
                Name: 'Pool',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-pool',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.lavendarDark
            },
            {
                HobbyId: User.Enums.Hobby.Racing,
                Name: 'Racing',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-racing',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.orangeDark
            },
            {
                HobbyId: User.Enums.Hobby.Running,
                Name: 'Running',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-running',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blueSteelDark
            },
            {
                HobbyId: User.Enums.Hobby.Soccer,
                Name: 'Soccer',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-soccer',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.pinkDark
            },
            {
                HobbyId: User.Enums.Hobby.Tennis,
                Name: 'Tennis',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-tennis',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.teal
            },
            {
                HobbyId: User.Enums.Hobby.Volleyball,
                Name: 'Volleyball',
                Category: User.Enums.HobbyCategory.Sports,
                Icon: 'fa-sports-volleyball',
                IsChecked: false,
                IsActive: null,
                UserHobbyId: 0,
                Color: SiteSettings.colors.secondaryColors.blue
            }
        ];
        staticStickerData: Array<IFamilyInfo> = [
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Person,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-person',
                MemberLabel: '',
                FontSize: 55
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Man,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-man',
                MemberLabel: '',
                FontSize: 55
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Woman,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-woman',
                MemberLabel: '',
                FontSize: 55
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Boy,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-boy',
                MemberLabel: '',
                FontSize: 50
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Girl,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-girl',
                MemberLabel: '',
                FontSize: 50
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Baby,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-baby',
                MemberLabel: '',
                FontSize: 40
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Pets,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-pets',
                MemberLabel: '',
                FontSize: 30
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Dog,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-dog',
                MemberLabel: '',
                FontSize: 30
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Cat,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-cat',
                MemberLabel: '',
                FontSize: 30
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Rabbit,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-rabbit',
                MemberLabel: '',
                FontSize: 30
            },
            {
                StickerSet: User.Enums.StickerSet.Default,
                SetMember: User.Enums.SetMember.Fish,
                IsChecked: false,
                UserStickerId: 0,
                Icon: 'fa-family-fish',
                MemberLabel: '',
                FontSize: 30
            }
        ];
        staticCountryGeoData: Array<commonInterfaces.IMapLocation> = [
            {
                siteId: User.Enums.Country.Afghanistan,
                name: 'Afghanistan',
                latitude: 33.93911,
                longitude: 67.70995300000004
            },
            {
                siteId: User.Enums.Country.Albania,
                name: 'Albania',
                latitude: 41.153332,
                longitude: 20.168330999999966
            },
            {
                siteId: User.Enums.Country.Algeria,
                name: 'Algeria',
                latitude: 28.033886,
                longitude: 1.659626000000003
            },
            {
                siteId: User.Enums.Country.Andorra,
                name: 'Andorra',
                latitude: 42.506285,
                longitude: 1.5218009999999822
            },
            {
                siteId: User.Enums.Country.Angola,
                name: 'Angola',
                latitude: -11.202692,
                longitude: 17.873886999999968
            },
            {
                siteId: User.Enums.Country.Antigua_and_Barbuda,
                name: 'Antigua and Barbuda',
                latitude: 17.060816,
                longitude: -61.79642799999999
            },
            {
                siteId: User.Enums.Country.Argentina,
                name: 'Argentina',
                latitude: -38.416097,
                longitude: -63.616671999999994
            },
            {
                siteId: User.Enums.Country.Armenia,
                name: 'Armenia',
                latitude: 40.069099,
                longitude: 45.03818899999999
            },
            {
                siteId: User.Enums.Country.Australia,
                name: 'Australia',
                latitude: -25.274398,
                longitude: 133.77513599999997
            },
            {
                siteId: User.Enums.Country.Austria,
                name: 'Austria',
                latitude: 47.516231,
                longitude: 14.550072
            },
            {
                siteId: User.Enums.Country.Azerbaijan,
                name: 'Azerbaijan',
                latitude: 40.143105,
                longitude: 47.57692700000007
            },
            {
                siteId: User.Enums.Country.Bahamas,
                name: 'Bahamas',
                latitude: 25.03428,
                longitude: -77.39627999999999
            },
            {
                siteId: User.Enums.Country.Bahrain,
                name: 'Bahrain',
                latitude: 26.0667,
                longitude: 50.55770000000007
            },
            {
                siteId: User.Enums.Country.Bangladesh,
                name: 'Bangladesh',
                latitude: 23.684994,
                longitude: 90.35633099999995
            },
            {
                siteId: User.Enums.Country.Barbados,
                name: 'Barbados',
                latitude: 13.193887,
                longitude: -59.54319799999996
            },
            {
                siteId: User.Enums.Country.Belarus,
                name: 'Belarus',
                latitude: 53.709807,
                longitude: 27.953389000000016
            },
            {
                siteId: User.Enums.Country.Belgium,
                name: 'Belgium',
                latitude: 50.503887,
                longitude: 4.4699359999999615
            },
            {
                siteId: User.Enums.Country.Belize,
                name: 'Belize',
                latitude: 17.189877,
                longitude: -88.49765000000002
            },
            {
                siteId: User.Enums.Country.Benin,
                name: 'Benin',
                latitude: 9.30769,
                longitude: 2.3158339999999953
            },
            {
                siteId: User.Enums.Country.Bhutan,
                name: 'Bhutan',
                latitude: 27.514162,
                longitude: 90.43360099999995
            },
            {
                siteId: User.Enums.Country.Bolivia,
                name: 'Bolivia',
                latitude: -16.290154,
                longitude: -63.58865300000002
            },
            {
                siteId: User.Enums.Country.Bosnia_and_Herzegovina,
                name: 'Bosnia and Herzegovina',
                latitude: 43.915886,
                longitude: 17.67907600000001
            },
            {
                siteId: User.Enums.Country.Botswana,
                name: 'Botswana',
                latitude: -22.328474,
                longitude: 24.684866000000056
            },
            {
                siteId: User.Enums.Country.Brazil,
                name: 'Brazil',
                latitude: -14.235004,
                longitude: -51.92527999999999
            },
            {
                siteId: User.Enums.Country.Brunei_Darussalam,
                name: 'Brunei Darussalam',
                latitude: 4.535277,
                longitude: 114.72766899999999
            },
            {
                siteId: User.Enums.Country.Bulgaria,
                name: 'Bulgaria',
                latitude: 42.733883,
                longitude: 25.485829999999964
            },
            {
                siteId: User.Enums.Country.Burkina_Faso,
                name: 'Burkina Faso',
                latitude: 12.238333,
                longitude: -1.5615930000000162
            },
            {
                siteId: User.Enums.Country.Burundi,
                name: 'Burundi',
                latitude: -3.373056,
                longitude: 29.91888599999993
            },
            {
                siteId: User.Enums.Country.Cabo_Verde,
                name: 'Cabo Verde',
                latitude: 15.120142,
                longitude: -23.605186799999956
            },
            {
                siteId: User.Enums.Country.Cambodia,
                name: 'Cambodia',
                latitude: 12.565679,
                longitude: 104.99096299999997
            },
            {
                siteId: User.Enums.Country.Cameroon,
                name: 'Cameroon',
                latitude: 7.369721999999999,
                longitude: 12.354722000000038
            },
            {
                siteId: User.Enums.Country.Canada,
                name: 'Canada',
                latitude: 56.130366,
                longitude: -106.34677099999999
            },
            {
                siteId: User.Enums.Country.Central_African_Republic,
                name: 'Central African Republic',
                latitude: 6.611110999999999,
                longitude: 20.93944399999998
            },
            {
                siteId: User.Enums.Country.Chad,
                name: 'Chad',
                latitude: 15.454166,
                longitude: 18.732207000000017
            },
            {
                siteId: User.Enums.Country.Chile,
                name: 'Chile',
                latitude: -35.675147,
                longitude: -71.54296899999997
            },
            {
                siteId: User.Enums.Country.China,
                name: 'China',
                latitude: 35.86166,
                longitude: 104.19539699999996
            },
            {
                siteId: User.Enums.Country.Colombia,
                name: 'Colombia',
                latitude: 4.570868,
                longitude: -74.29733299999998
            },
            {
                siteId: User.Enums.Country.Comoros,
                name: 'Comoros',
                latitude: -11.6455,
                longitude: 43.33330000000001
            },
            {
                siteId: User.Enums.Country.Congo,
                name: 'Congo',
                latitude: -4.038333,
                longitude: 21.758663999999953
            },
            {
                siteId: User.Enums.Country.Costa_Rica,
                name: 'Costa Rica',
                latitude: 9.748916999999999,
                longitude: -83.75342799999999
            },
            {
                siteId: User.Enums.Country.Côte_dIvoire,
                name: 'Côte dIvoire',
                latitude: 7.539988999999999,
                longitude: -5.547080000000051
            },
            {
                siteId: User.Enums.Country.Croatia,
                name: 'Croatia',
                latitude: 45.1,
                longitude: 15.200000000000045
            },
            {
                siteId: User.Enums.Country.Cuba,
                name: 'Cuba',
                latitude: 21.521757,
                longitude: -77.78116699999998
            },
            {
                siteId: User.Enums.Country.Cyprus,
                name: 'Cyprus',
                latitude: 35.126413,
                longitude: 33.429858999999965
            },
            {
                siteId: User.Enums.Country.Czech_Republic,
                name: 'Czech Republic',
                latitude: 49.81749199999999,
                longitude: 15.472962000000052
            },
            {
                siteId: User.Enums.Country.North_Korea,
                name: 'North Korea',
                latitude: 40.339852,
                longitude: 127.51009299999998
            },
            {
                siteId: User.Enums.Country.Democratic_Republic_of_the_Cong,
                name: 'Democratic Republic of the Cong',
                latitude: -4.038333,
                longitude: 21.758663999999953
            },
            {
                siteId: User.Enums.Country.Denmark,
                name: 'Denmark',
                latitude: 56.26392,
                longitude: 9.50178500000004
            },
            {
                siteId: User.Enums.Country.Djibouti,
                name: 'Djibouti',
                latitude: 11.825138,
                longitude: 42.59027500000002
            },
            {
                siteId: User.Enums.Country.Dominica,
                name: 'Dominica',
                latitude: 15.414999,
                longitude: -61.37097600000004
            },
            {
                siteId: User.Enums.Country.Dominican_Republic,
                name: 'Dominican Republic',
                latitude: 18.735693,
                longitude: -70.16265099999998
            },
            {
                siteId: User.Enums.Country.Ecuador,
                name: 'Ecuador',
                latitude: -1.831239,
                longitude: -78.18340599999999
            },
            {
                siteId: User.Enums.Country.Egypt,
                name: 'Egypt',
                latitude: 26.820553,
                longitude: 30.802498000000014
            },
            {
                siteId: User.Enums.Country.El_Salvador,
                name: 'El Salvador',
                latitude: 13.794185,
                longitude: -88.89652999999998
            },
            {
                siteId: User.Enums.Country.Equatorial_Guinea,
                name: 'Equatorial Guinea',
                latitude: 1.650801,
                longitude: 10.267894999999953
            },
            {
                siteId: User.Enums.Country.Eritrea,
                name: 'Eritrea',
                latitude: 15.179384,
                longitude: 39.78233399999999
            },
            {
                siteId: User.Enums.Country.Estonia,
                name: 'Estonia',
                latitude: 58.595272,
                longitude: 25.01360699999998
            },
            {
                siteId: User.Enums.Country.Ethiopia,
                name: 'Ethiopia',
                latitude: 9.145000000000001,
                longitude: 40.48967300000004
            },
            {
                siteId: User.Enums.Country.Fiji,
                name: 'Fiji',
                latitude: -17.713371,
                longitude: 178.06503199999997
            },
            {
                siteId: User.Enums.Country.Finland,
                name: 'Finland',
                latitude: 61.92410999999999,
                longitude: 25.748151000000007
            },
            {
                siteId: User.Enums.Country.France,
                name: 'France',
                latitude: 46.227638,
                longitude: 2.213749000000007
            },
            {
                siteId: User.Enums.Country.Gabon,
                name: 'Gabon',
                latitude: -0.803689,
                longitude: 11.60944399999994
            },
            {
                siteId: User.Enums.Country.Gambia,
                name: 'Gambia',
                latitude: 13.443182,
                longitude: -15.310138999999936
            },
            {
                siteId: User.Enums.Country.Georgia,
                name: 'Georgia',
                latitude: 32.1656221,
                longitude: -82.90007509999998
            },
            {
                siteId: User.Enums.Country.Germany,
                name: 'Germany',
                latitude: 51.165691,
                longitude: 10.451526000000058
            },
            {
                siteId: User.Enums.Country.Ghana,
                name: 'Ghana',
                latitude: 7.946527,
                longitude: -1.0231939999999895
            },
            {
                siteId: User.Enums.Country.Greece,
                name: 'Greece',
                latitude: 39.074208,
                longitude: 21.824311999999964
            },
            {
                siteId: User.Enums.Country.Grenada,
                name: 'Grenada',
                latitude: 12.1165,
                longitude: -61.678999999999974
            },
            {
                siteId: User.Enums.Country.Guatemala,
                name: 'Guatemala',
                latitude: 15.783471,
                longitude: -90.23075899999998
            },
            {
                siteId: User.Enums.Country.Guinea,
                name: 'Guinea',
                latitude: 9.945587,
                longitude: -9.69664499999999
            },
            {
                siteId: User.Enums.Country.Guinea_Bissau,
                name: 'Guinea Bissau',
                latitude: 11.803749,
                longitude: -15.180413000000044
            },
            {
                siteId: User.Enums.Country.Guyana,
                name: 'Guyana',
                latitude: 4.860416,
                longitude: -58.93018000000001
            },
            {
                siteId: User.Enums.Country.Haiti,
                name: 'Haiti',
                latitude: 18.971187,
                longitude: -72.285215
            },
            {
                siteId: User.Enums.Country.Honduras,
                name: 'Honduras',
                latitude: 15.199999,
                longitude: -86.24190499999997
            },
            {
                siteId: User.Enums.Country.Hungary,
                name: 'Hungary',
                latitude: 47.162494,
                longitude: 19.50330400000007
            },
            {
                siteId: User.Enums.Country.Iceland,
                name: 'Iceland',
                latitude: 64.963051,
                longitude: -19.020835000000034
            },
            {
                siteId: User.Enums.Country.India,
                name: 'India',
                latitude: 20.593684,
                longitude: 78.96288000000004
            },
            {
                siteId: User.Enums.Country.Indonesia,
                name: 'Indonesia',
                latitude: -0.789275,
                longitude: 113.92132700000002
            },
            {
                siteId: User.Enums.Country.Iran,
                name: 'Iran',
                latitude: 32.427908,
                longitude: 53.688045999999986
            },
            {
                siteId: User.Enums.Country.Iraq,
                name: 'Iraq',
                latitude: 33.223191,
                longitude: 43.679291000000035
            },
            {
                siteId: User.Enums.Country.Ireland,
                name: 'Ireland',
                latitude: 53.1423672,
                longitude: -7.692053600000008
            },
            {
                siteId: User.Enums.Country.Israel,
                name: 'Israel',
                latitude: 31.046051,
                longitude: 34.85161199999993
            },
            {
                siteId: User.Enums.Country.Italy,
                name: 'Italy',
                latitude: 41.87194,
                longitude: 12.567379999999957
            },
            {
                siteId: User.Enums.Country.Jamaica,
                name: 'Jamaica',
                latitude: 18.109581,
                longitude: -77.297508
            },
            {
                siteId: User.Enums.Country.Japan,
                name: 'Japan',
                latitude: 36.204824,
                longitude: 138.252924
            },
            {
                siteId: User.Enums.Country.Jordan,
                name: 'Jordan',
                latitude: 30.585164,
                longitude: 36.238414000000034
            },
            {
                siteId: User.Enums.Country.Kazakhstan,
                name: 'Kazakhstan',
                latitude: 48.019573,
                longitude: 66.92368399999998
            },
            {
                siteId: User.Enums.Country.Kenya,
                name: 'Kenya',
                latitude: -0.023559,
                longitude: 37.90619300000003
            },
            {
                siteId: User.Enums.Country.Kiribati,
                name: 'Kiribati',
                latitude: 1.8709422,
                longitude: -157.3628595
            },
            {
                siteId: User.Enums.Country.Kuwait,
                name: 'Kuwait',
                latitude: 29.31166,
                longitude: 47.48176599999999
            },
            {
                siteId: User.Enums.Country.Kyrgyzstan,
                name: 'Kyrgyzstan',
                latitude: 41.20438,
                longitude: 74.76609800000006
            },
            {
                siteId: User.Enums.Country.Laos,
                name: 'Laos',
                latitude: 19.85627,
                longitude: 102.495496
            },
            {
                siteId: User.Enums.Country.Latvia,
                name: 'Latvia',
                latitude: 56.879635,
                longitude: 24.60318899999993
            },
            {
                siteId: User.Enums.Country.Lebanon,
                name: 'Lebanon',
                latitude: 33.854721,
                longitude: 35.86228499999993
            },
            {
                siteId: User.Enums.Country.Lesotho,
                name: 'Lesotho',
                latitude: -29.609988,
                longitude: 28.233608000000004
            },
            {
                siteId: User.Enums.Country.Liberia,
                name: 'Liberia',
                latitude: 6.428055,
                longitude: -9.429498999999964
            },
            {
                siteId: User.Enums.Country.Libya,
                name: 'Libya',
                latitude: 26.3351,
                longitude: 17.228331000000026
            },
            {
                siteId: User.Enums.Country.Liechtenstein,
                name: 'Liechtenstein',
                latitude: 47.166,
                longitude: 9.555373000000031
            },
            {
                siteId: User.Enums.Country.Lithuania,
                name: 'Lithuania',
                latitude: 55.169438,
                longitude: 23.88127499999996
            },
            {
                siteId: User.Enums.Country.Luxembourg,
                name: 'Luxembourg',
                latitude: 49.815273,
                longitude: 6.129583000000025
            },
            {
                siteId: User.Enums.Country.Macedonia,
                name: 'Macedonia',
                latitude: 41.608635,
                longitude: 21.745274999999992
            },
            {
                siteId: User.Enums.Country.Madagascar,
                name: 'Madagascar',
                latitude: -18.766947,
                longitude: 46.869106999999985
            },
            {
                siteId: User.Enums.Country.Malawi,
                name: 'Malawi',
                latitude: -13.254308,
                longitude: 34.30152499999997
            },
            {
                siteId: User.Enums.Country.Malaysia,
                name: 'Malaysia',
                latitude: 4.210484,
                longitude: 101.97576600000002
            },
            {
                siteId: User.Enums.Country.Maldives,
                name: 'Maldives',
                latitude: 1.9772276,
                longitude: 73.53610100000003
            },
            {
                siteId: User.Enums.Country.Mali,
                name: 'Mali',
                latitude: 17.570692,
                longitude: -3.9961660000000165
            },
            {
                siteId: User.Enums.Country.Malta,
                name: 'Malta',
                latitude: 35.937496,
                longitude: 14.375415999999973
            },
            {
                siteId: User.Enums.Country.Marshall_Islands,
                name: 'Marshall Islands',
                latitude: 11.3246908,
                longitude: 166.84174239999993
            },
            {
                siteId: User.Enums.Country.Mauritania,
                name: 'Mauritania',
                latitude: 21.00789,
                longitude: -10.940834999999993
            },
            {
                siteId: User.Enums.Country.Mauritius,
                name: 'Mauritius',
                latitude: -20.348404,
                longitude: 57.55215199999998
            },
            {
                siteId: User.Enums.Country.Mexico,
                name: 'Mexico',
                latitude: 23.634501,
                longitude: -102.55278399999997
            },
            {
                siteId: User.Enums.Country.Micronesia,
                name: 'Micronesia',
                latitude: 6.8874581,
                longitude: 158.22252290000006
            },
            {
                siteId: User.Enums.Country.Monaco,
                name: 'Monaco',
                latitude: 43.73841760000001,
                longitude: 7.42461579999997
            },
            {
                siteId: User.Enums.Country.Mongolia,
                name: 'Mongolia',
                latitude: 46.862496,
                longitude: 103.84665599999994
            },
            {
                siteId: User.Enums.Country.Montenegro,
                name: 'Montenegro',
                latitude: 42.708678,
                longitude: 19.37438999999995
            },
            {
                siteId: User.Enums.Country.Morocco,
                name: 'Morocco',
                latitude: 31.791702,
                longitude: -7.092620000000011
            },
            {
                siteId: User.Enums.Country.Mozambique,
                name: 'Mozambique',
                latitude: -18.665695,
                longitude: 35.52956199999994
            },
            {
                siteId: User.Enums.Country.Myanmar,
                name: 'Myanmar',
                latitude: 21.916221,
                longitude: 95.95597399999997
            },
            {
                siteId: User.Enums.Country.Namibia,
                name: 'Namibia',
                latitude: -22.95764,
                longitude: 18.490409999999997
            },
            {
                siteId: User.Enums.Country.Nauru,
                name: 'Nauru',
                latitude: -0.522778,
                longitude: 166.93150300000002
            },
            {
                siteId: User.Enums.Country.Nepal,
                name: 'Nepal',
                latitude: 28.394857,
                longitude: 84.124008
            },
            {
                siteId: User.Enums.Country.Netherlands,
                name: 'Netherlands',
                latitude: 52.132633,
                longitude: 5.2912659999999505
            },
            {
                siteId: User.Enums.Country.NewZealand,
                name: 'NewZealand',
                latitude: -40.900557,
                longitude: 174.88597100000004
            },
            {
                siteId: User.Enums.Country.Nicaragua,
                name: 'Nicaragua',
                latitude: 12.865416,
                longitude: -85.20722899999998
            },
            {
                siteId: User.Enums.Country.Niger,
                name: 'Niger',
                latitude: 17.607789,
                longitude: 8.081666000000041
            },
            {
                siteId: User.Enums.Country.Nigeria,
                name: 'Nigeria',
                latitude: 9.081999,
                longitude: 8.675277000000051
            },
            {
                siteId: User.Enums.Country.Norway,
                name: 'Norway',
                latitude: 60.47202399999999,
                longitude: 8.46894599999996
            },
            {
                siteId: User.Enums.Country.Oman,
                name: 'Oman',
                latitude: 21.4735329,
                longitude: 55.975413
            },
            {
                siteId: User.Enums.Country.Pakistan,
                name: 'Pakistan',
                latitude: 30.375321,
                longitude: 69.34511599999996
            },
            {
                siteId: User.Enums.Country.Palau,
                name: 'Palau',
                latitude: 7.514979999999999,
                longitude: 134.58251999999993
            },
            {
                siteId: User.Enums.Country.Panama,
                name: 'Panama',
                latitude: 8.537981,
                longitude: -80.782127
            },
            {
                siteId: User.Enums.Country.Papua_New_Guinea,
                name: 'Papua New Guinea',
                latitude: -6.314992999999999,
                longitude: 143.95555000000002
            },
            {
                siteId: User.Enums.Country.Paraguay,
                name: 'Paraguay',
                latitude: -23.442503,
                longitude: -58.443831999999986
            },
            {
                siteId: User.Enums.Country.Peru,
                name: 'Peru',
                latitude: -9.189967,
                longitude: -75.015152
            },
            {
                siteId: User.Enums.Country.Philippines,
                name: 'Philippines',
                latitude: 12.879721,
                longitude: 121.77401699999996
            },
            {
                siteId: User.Enums.Country.Poland,
                name: 'Poland',
                latitude: 51.919438,
                longitude: 19.14513599999998
            },
            {
                siteId: User.Enums.Country.Portugal,
                name: 'Portugal',
                latitude: 39.39987199999999,
                longitude: -8.224454000000037
            },
            {
                siteId: User.Enums.Country.Qatar,
                name: 'Qatar',
                latitude: 25.354826,
                longitude: 51.183884000000035
            },
            {
                siteId: User.Enums.Country.South_Korea,
                name: 'South Korea',
                latitude: 35.907757,
                longitude: 127.76692200000002
            },
            {
                siteId: User.Enums.Country.Moldova,
                name: 'Moldova',
                latitude: 47.411631,
                longitude: 28.369885000000068
            },
            {
                siteId: User.Enums.Country.Romania,
                name: 'Romania',
                latitude: 45.943161,
                longitude: 24.966760000000022
            },
            {
                siteId: User.Enums.Country.Russia,
                name: 'Russia',
                latitude: 61.52401,
                longitude: 105.31875600000001
            },
            {
                siteId: User.Enums.Country.Rwanda,
                name: 'Rwanda',
                latitude: -1.940278,
                longitude: 29.873887999999965
            },
            {
                siteId: User.Enums.Country.Saint_Kitts_and_Nevis,
                name: 'Saint Kitts and Nevis',
                latitude: 17.357822,
                longitude: -62.78299800000002
            },
            {
                siteId: User.Enums.Country.Saint_Lucia,
                name: 'Saint Lucia',
                latitude: 13.909444,
                longitude: -60.97889299999997
            },
            {
                siteId: User.Enums.Country.Saint_Vincent_and_the_Grenadines,
                name: 'Saint Vincent and the Grenadines',
                latitude: 13.2528179,
                longitude: -61.1971628
            },
            {
                siteId: User.Enums.Country.Samoa,
                name: 'Samoa',
                latitude: -13.759029,
                longitude: -172.104629
            },
            {
                siteId: User.Enums.Country.San_Marino,
                name: 'San Marino',
                latitude: 43.94236,
                longitude: 12.457776999999965
            },
            {
                siteId: User.Enums.Country.Sao_Tome_and_Principe,
                name: 'Sao Tome and Principe',
                latitude: 0.18636,
                longitude: 6.613080999999966
            },
            {
                siteId: User.Enums.Country.Saudi_Arabia,
                name: 'Saudi Arabia',
                latitude: 23.885942,
                longitude: 45.079162
            },
            {
                siteId: User.Enums.Country.Senegal,
                name: 'Senegal',
                latitude: 14.497401,
                longitude: -14.452361999999994
            },
            {
                siteId: User.Enums.Country.Serbia,
                name: 'Serbia',
                latitude: 44.016521,
                longitude: 21.005858999999987
            },
            {
                siteId: User.Enums.Country.Seychelles,
                name: 'Seychelles',
                latitude: -4.679574,
                longitude: 55.49197700000002
            },
            {
                siteId: User.Enums.Country.Sierra_Leone,
                name: 'Sierra Leone',
                latitude: 8.460555,
                longitude: -11.779889000000026
            },
            {
                siteId: User.Enums.Country.Singapore,
                name: 'Singapore',
                latitude: 1.352083,
                longitude: 103.81983600000001
            },
            {
                siteId: User.Enums.Country.Slovakia,
                name: 'Slovakia',
                latitude: 48.669026,
                longitude: 19.69902400000001
            },
            {
                siteId: User.Enums.Country.Slovenia,
                name: 'Slovenia',
                latitude: 46.151241,
                longitude: 14.995462999999972
            },
            {
                siteId: User.Enums.Country.Solomon_Islands,
                name: 'Solomon Islands',
                latitude: -9.64571,
                longitude: 160.15619400000003
            },
            {
                siteId: User.Enums.Country.Somalia,
                name: 'Somalia',
                latitude: 5.152149,
                longitude: 46.19961599999999
            },
            {
                siteId: User.Enums.Country.South_Africa,
                name: 'South Africa',
                latitude: -30.559482,
                longitude: 22.937505999999985
            },
            {
                siteId: User.Enums.Country.South_Sudan,
                name: 'South Sudan',
                latitude: 6.876991899999999,
                longitude: 31.306978800000024
            },
            {
                siteId: User.Enums.Country.Spain,
                name: 'Spain',
                latitude: 40.46366700000001,
                longitude: -3.7492200000000366
            },
            {
                siteId: User.Enums.Country.SriLanka,
                name: 'SriLanka',
                latitude: 7.873053999999999,
                longitude: 80.77179699999999
            },
            {
                siteId: User.Enums.Country.Sudan,
                name: 'Sudan',
                latitude: 12.862807,
                longitude: 30.21763599999997
            },
            {
                siteId: User.Enums.Country.Suriname,
                name: 'Suriname',
                latitude: 3.919305,
                longitude: -56.027783
            },
            {
                siteId: User.Enums.Country.Swaziland,
                name: 'Swaziland',
                latitude: -26.522503,
                longitude: 31.465866000000005
            },
            {
                siteId: User.Enums.Country.Sweden,
                name: 'Sweden',
                latitude: 60.12816100000001,
                longitude: 18.643501000000015
            },
            {
                siteId: User.Enums.Country.Switzerland,
                name: 'Switzerland',
                latitude: 46.818188,
                longitude: 8.227511999999933
            },
            {
                siteId: User.Enums.Country.Syria,
                name: 'Syria',
                latitude: 34.80207499999999,
                longitude: 38.99681499999997
            },
            {
                siteId: User.Enums.Country.Tajikistan,
                name: 'Tajikistan',
                latitude: 38.861034,
                longitude: 71.27609299999995
            },
            {
                siteId: User.Enums.Country.Thailand,
                name: 'Thailand',
                latitude: 15.870032,
                longitude: 100.99254100000007
            },
            {
                siteId: User.Enums.Country.Timor_Leste,
                name: 'Timor Leste',
                latitude: -8.874217,
                longitude: 125.72753899999998
            },
            {
                siteId: User.Enums.Country.Togo,
                name: 'Togo',
                latitude: 8.619543,
                longitude: 0.8247820000000274
            },
            {
                siteId: User.Enums.Country.Tonga,
                name: 'Tonga',
                latitude: -21.178986,
                longitude: -175.198242
            },
            {
                siteId: User.Enums.Country.Trinidad_and_Tobago,
                name: 'Trinidad and Tobago',
                latitude: 10.691803,
                longitude: -61.22250300000002
            },
            {
                siteId: User.Enums.Country.Tunisia,
                name: 'Tunisia',
                latitude: 33.886917,
                longitude: 9.537499000000025
            },
            {
                siteId: User.Enums.Country.Turkey,
                name: 'Turkey',
                latitude: 38.963745,
                longitude: 35.243322000000035
            },
            {
                siteId: User.Enums.Country.Turkmenistan,
                name: 'Turkmenistan',
                latitude: 38.969719,
                longitude: 59.55627800000002
            },
            {
                siteId: User.Enums.Country.Tuvalu,
                name: 'Tuvalu',
                latitude: -7.4784205,
                longitude: 178.67992400000003
            },
            {
                siteId: User.Enums.Country.Uganda,
                name: 'Uganda',
                latitude: 1.373333,
                longitude: 32.290275000000065
            },
            {
                siteId: User.Enums.Country.Ukraine,
                name: 'Ukraine',
                latitude: 48.379433,
                longitude: 31.165579999999977
            },
            {
                siteId: User.Enums.Country.UAE,
                name: 'UAE',
                latitude: 23.424076,
                longitude: 53.84781799999996
            },
            {
                siteId: User.Enums.Country.UK,
                name: 'UK',
                latitude: 55.378051,
                longitude: -3.43597299999999
            },
            {
                siteId: User.Enums.Country.Tanzania,
                name: 'Tanzania',
                latitude: -6.369028,
                longitude: 34.888822000000005
            },
            {
                siteId: User.Enums.Country.USA,
                name: 'USA',
                latitude: 37.09024,
                longitude: -95.71289100000001
            },
            {
                siteId: User.Enums.Country.Uruguay,
                name: 'Uruguay',
                latitude: -32.522779,
                longitude: -55.76583500000004
            },
            {
                siteId: User.Enums.Country.Uzbekistan,
                name: 'Uzbekistan',
                latitude: 41.377491,
                longitude: 64.58526200000006
            },
            {
                siteId: User.Enums.Country.Vanuatu,
                name: 'Vanuatu',
                latitude: -15.376706,
                longitude: 166.959158
            },
            {
                siteId: User.Enums.Country.Venezuela,
                name: 'Venezuela',
                latitude: 6.42375,
                longitude: -66.58973000000003
            },
            {
                siteId: User.Enums.Country.Vietnam,
                name: 'Vietnam',
                latitude: 14.058324,
                longitude: 108.277199
            },
            {
                siteId: User.Enums.Country.Yemen,
                name: 'Yemen',
                latitude: 15.552727,
                longitude: 48.516388000000006
            },
            {
                siteId: User.Enums.Country.Zambia,
                name: 'Zambia',
                latitude: -13.133897,
                longitude: 27.849332000000004
            },
            {
                siteId: User.Enums.Country.Zimbabwe,
                name: 'Zimbabwe',
                latitude: -19.015438,
                longitude: 29.154856999999993
            }
        ];
        hobbyIndex: User.Enums.HobbyCategory = User.Enums.HobbyCategory.General;
        sportsIndex: User.Enums.HobbyCategory = User.Enums.HobbyCategory.Sports;
        userSports: Array<IHobbyInfo> = [];
        userHobbies: Array<IHobbyInfo> = [];
        userFamily: Array<IFamilyInfo> = [];
        userTravel: Array<ITravelInfo> = [];
        allTravel: Array<ITravelInfo> = [];
        showEditAboutMe: boolean = false;
        wysiwygMenu;
        modalInstance;
        map:any;
        returnedHobbyList: Array<IHobbyInfo> = [];
        returnedSportsList: Array<IHobbyInfo> = [];
        socialPictureFiles = [];
        canEditItems: boolean = false;
        static $inject = [
            '$scope',
            '$log',
            '$uibModal',
            'userContext',
            'userInfo',
            'sitesettings',
            Common.Services.UserInfoSvc.id,
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            Common.Services.UtilitySvc.id,
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.mapssvc'];
        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private userInfoSvc: ChaiTea.Common.Services.IUserInfoSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: Services.IBaseSvc,
            private utilSvc: Common.Services.IUtilitySvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private mapSvc: ChaiTea.Common.Services.IMapsSvc) {
            this.wysiwygMenu = sitesettings.wysiwygMenus.default;

            this.map = {
                center: new google.maps.LatLng(0, 0),
                zoom: 1,
                options: {
                    styles: this.mapSvc.getStyles().OldDryMud,
                },
                markers: []
            };
        }

        public initialize = (): void => {
            var paramId = this.utilSvc.getIdParamFromUrl(null, true);
            if (!paramId) {
                paramId = this.utilSvc.getQueryParamFromUrl(Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString());
            }
            this.user.userId = (paramId || this.userInfo.userId);

            this.isCurrentUser = (this.user.userId == this.userInfo.userId);
            this.isManager = this.userInfo.mainRoles.managers;
            this.canEditItems = this.isCurrentUser
                || this.userInfo.mainRoles.managers;
            this.getUserById(this.user.userId);
            this.setAllTravelInfo();
            this.getTravelLocations();
        }

        private getUserHobbies = (): void => {
            var params = {
                userId: this.saveUserId
            };
            this.apiSvc.query(params, 'UserHobby/ByUserId/:userId').then((result: Array<IHobbyInfo>) => {
                angular.forEach(result, (item) => {
                    angular.forEach(this.staticAllHobbyData, (data) => {
                        if (item.HobbyId == data.HobbyId) {
                            var newObj: IHobbyInfo = {
                                IsActive: item.IsActive,
                                UserHobbyId: item.UserHobbyId,
                                Category: data.Category,
                                Color: data.Color,
                                HobbyId: data.HobbyId,
                                Icon: data.Icon,
                                Name: data.Name,
                                IsChecked: item.IsActive ? true : data.IsChecked
                            };
                            data.IsActive = item.IsActive;
                            if (data.IsActive == true) {
                                data.IsChecked = true;
                            }
                            data.UserHobbyId = item.UserHobbyId;
                            if ((newObj.Category == User.Enums.HobbyCategory.General) && (newObj.IsActive)) {
                                this.userHobbies.push(newObj);
                            }
                            else if ((newObj.Category == User.Enums.HobbyCategory.Sports) && (newObj.IsActive)){
                                this.userSports.push(newObj);
                            }
                        }
                    });
                });
            }, this.handleFailure);
        }

        private editAboutMe = ():void => {
            this.showEditAboutMe = !this.showEditAboutMe;
        }

        private editMyHobbies = (category: User.Enums.HobbyCategory): void => {
            var availableHobbies: Array<IHobbyInfo> = [];
            if (this.returnedHobbyList.length > 0 && (category == User.Enums.HobbyCategory.General)) {
                availableHobbies = this.returnedHobbyList;
            }
            else if (this.returnedSportsList.length > 0 && (category == User.Enums.HobbyCategory.Sports)) {
                availableHobbies = this.returnedSportsList;
            }
            else {
                angular.forEach(this.staticAllHobbyData, (data) => {
                    if ((category == User.Enums.HobbyCategory.General) && (data.Category == User.Enums.HobbyCategory.General)) {
                        availableHobbies.push(data);
                    }
                    else if ((category == User.Enums.HobbyCategory.Sports) && (data.Category == User.Enums.HobbyCategory.Sports)) {
                        availableHobbies.push(data);
                    }
                });
            }
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/User/Templates/Profile/profile.myHobbies.html',
                controller: 'User.MyProfileMyHobbiesCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    userId: () => { return this.saveUserId; },
                    category: () => { return category; },
                    availableHobbies: () => { return availableHobbies; }
                }
            }).result.then(availableHobbies => {
                        if (category == User.Enums.HobbyCategory.General) {
                    this.userHobbies = [];
                    this.returnedHobbyList = availableHobbies;
                }
                else if (category == User.Enums.HobbyCategory.Sports) {
                    this.userSports = [];
                    this.returnedSportsList = availableHobbies;
                }
                angular.forEach(availableHobbies, (item) => {
                    if (item.IsChecked) {
                        if (category == User.Enums.HobbyCategory.General) {
                            this.userHobbies.push(item);
                        } else if (category == User.Enums.HobbyCategory.Sports) {
                            this.userSports.push(item);
                        }
                    }
                    });
            });            
                }

        private setAllTravelInfo = (): void => {
            var countryObj = User.Enums.Country;
                            var i: number = 0;
            angular.forEach(countryObj, (item) => {
                if (User.Enums.Country.hasOwnProperty(item) && !/^\d+$/.test(item)) {
                    this.allTravel.push({
                        CountryId: i,
                        Name: item.split('_').join(' '),
                        IsChecked: false,
                        IsActive: null,
                        UserTravelId: 0
                    });
                    i++;
                }
            });
        }

        private getUserTravelInfo = () => {
            var params = {
                userId: this.saveUserId
            };
            this.apiSvc.query(params, 'UserTravel/ByUserId/:userId').then((result) => {
                angular.forEach(result, (item) => {
                    angular.forEach(this.allTravel, (data) => {
                        if (item.CountryId == data.CountryId) {
                            var newObj:ITravelInfo = {
                                CountryId: item.CountryId,
                                Name: data.Name,
                                IsChecked: item.IsActive ? true : false,
                                IsActive: item.IsActive,
                                UserTravelId:item.UserTravelId
                            };
                            data.IsActive = item.IsActive;
                            data.UserTravelId = item.UserTravelId;
                            if (item.IsActive) {
                                data.IsChecked = true;
                            }
                            this.userTravel.push(newObj);
                        }
                    });
                });
                this.getTravelLocations();
            }, this.handleFailure);
        }

        private editMyTravel = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/User/Templates/Profile/profile.mytravel.html',
                controller: 'User.MyProfileMyTravelCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    userId: () => { return this.saveUserId; },
                    userTravel: () => { return this.userTravel; },
                    allTravel: () => { return this.allTravel; }
                }
            }).result.then(entireTravelInfo => {
                this.allTravel = entireTravelInfo.allTravel;
                this.userTravel = [];
                angular.forEach(entireTravelInfo.checkedCountries, (item) => {
                        var newObj: ITravelInfo = {
                            CountryId: item.CountryId,
                            Name: item.Name,
                            IsChecked: item.IsChecked,
                            IsActive: item.IsActive,
                            UserTravelId: item.UserTravelId
                        };
                        this.userTravel.push(newObj);
                });
                this.getTravelLocations();
            });
        }

        private saveAboutMe = () => {
            if (!this.user["AboutMe"]) return false;
            this.saveUser();
        }

        private saveUser = () => {
            this.userInfoSvc.updateUserInfo(this.userId, this.user).then((data) => {
                this.showEditAboutMe = !this.showEditAboutMe;
            }, this.handleFailure);
        }

        private getUserSticker = () => {
            var params = {
                userId: this.saveUserId
            };
            this.apiSvc.query(params, 'UserSticker/ByUserId/:userId').then((result: Array<IFamilyInfo>) => {
                angular.forEach(result, (item) => {
                    angular.forEach(this.staticStickerData, (data) => {
                        if (item.SetMember == data.SetMember) {
                            var newFamilyObj: IFamilyInfo = {
                                UserStickerId: item.UserStickerId,
                                SetMember: item.SetMember,
                                MemberLabel: User.Enums.MemberLabel[item.MemberLabel],
                                StickerSet: data.StickerSet,
                                IsChecked: false,
                                Icon: data.Icon,
                                FontSize: data.FontSize
                            };
                            this.userFamily.push(newFamilyObj);                       
                        }
                    });
                });
            }, this.handleFailure);
        }

        private addToMyFamily = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/User/Templates/Profile/profile.addtomyfamily.html',
                controller: 'User.MyProfileMyFamilyCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    userId: () => { return this.saveUserId; },
                    staticStickerData: () => { return this.staticStickerData; },
                    userFamily: () => { return this.userFamily; }
                }
            }).result.then(userFamily => {
                this.userFamily = userFamily;
            });
        }

        private removeFromMyFamily = (): void => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/User/Templates/Profile/profile.removefrommyfamily.html',
                controller: 'User.MyProfileMyFamilyCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    userId: () => { return this.saveUserId; },
                    staticStickerData: () => { return this.staticStickerData; },
                    userFamily: () => { return this.userFamily; }
                }
            })
        }

        /**
         * @description Returns the userInfo object by user id.
         */
        private getUserById = (userId: number): void => {

            async.auto({
                get_user_info: (callback) => {
                    if (!this.isCurrentUser) {
                        // EmployeeInfo
                        this.apiSvc.getById(userId, `Employee/User`).then((result) => {
                            if (result) {
                                this.saveUserId = result.UserId;
                                this.apiSvc.getById(result.UserId, 'UserInfo').then((user) => {
                                    this.user = angular.extend({}, user);
                                    this.userId = result.UserId;
                                    this.orgUserId = result.EmployeeId;
                                    callback(null, this.user);
                                }, this.handleFailure);
                            }
                        }, err => callback(err, null));
                    } else {
                        this.saveUserId = this.userContext.UserId;
                        this.apiSvc.getById(this.userContext.UserId, 'UserInfo').then((user) => {
                            this.user = angular.extend({}, user);
                            this.userId = user.UserId;
                            this.orgUserId = user.EmployeeId;
                            callback(null, this.user);
                        }, this.handleFailure);
                    }
                },
                social_images: ['get_user_info', (callback, results) => {
                    this.getUserHobbies();
                    this.getUserSticker();
                    this.getUserTravelInfo();
                    this.user.socialPictures = this.imageSvc.getSocialImagesFromAttachments(this.user["UserAttachments"]);
                }]
            }, (error, results) => {
                if (error) this.$log.error(error);


            });

        }

        public addSocialPicture = (user, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length && files[0]) {
                var file = files[0];

                this.imageSvc.readImage(file).then((data: any) => {
                    this.cropImage(user, data.dataUrl, Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]);
                }, this.handleFailure);
            }
        }

        private cropImage = (user: any, file: any, userAttachmentType?: any) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Common.ImageCropCtrl as vm',
                size: 'md',
                resolve: {
                    file: () => { return file; },
                    needDescription: () => { return userAttachmentType == Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]; }
                },
                backdrop: 'static'
            });

            this.modalInstance.result.then((modalRes: any) => {

                if (!modalRes) return;

                this.imageSvc.save(modalRes.originalImage, false, true).then((res: Common.Interfaces.IAwsObjectDetails) => {

                    var tempAttach = {
                        UniqueId: res.Name,
                        UserAttachmentType: userAttachmentType,
                        AttachmentType: 'Image',
                        Description: modalRes.imageDescription
                    }

                    user = this.utilSvc.mapKeysToCapital(user);

                    user.UserAttachments = _.isEmpty(user.UserAttachments) ? [] : user.UserAttachments;
                    user.UserAttachments.push(tempAttach);
                    this.imageSvc.save(modalRes.croppedImage, false, true, this.imageSvc.getThumbnailName(res.Guid)).then((res: Common.Interfaces.IAwsObjectDetails) => {

                        this.userInfoSvc.updateUserInfo(this.userId, user).then((res) => {
                            if (!res) throw Error('No result');
                            this.notificationSvc.successToastMessage('Image saved successfully.')
                            if (userAttachmentType == Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]) {
                                this.user.socialPictures = [] = angular.extend([], this.imageSvc.getSocialImagesFromAttachments(res.UserAttachments));
                            }
                        }, this.handleFailure);

                    }, (error) => {
                        this.notificationSvc.errorToastMessage(error);
                    });

                }, (error) => {
                    this.notificationSvc.errorToastMessage(error);
                });

            }, () => {

            });

        }

        public removeSocialPicture = ($index, uniqueId) => {
            if (!uniqueId) {
                this.$log.warn("Cannot delete image");
                return;
            }

            bootbox.confirm('Are you sure you want to remove this photo?', (result) => {
                if (result) {
                    _.remove(this.user.socialPictures, item => (item.uniqueId == uniqueId));
                    _.remove(this.user["UserAttachments"], (item: any) => (item.UniqueId == uniqueId));
                    this.userInfoSvc.updateUserInfo(this.userId, this.user).then((data) => {
                        this.notificationSvc.successToastMessage("Information has been updated.");
                    }, this.handleFailure);
                }
            });
        }

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }

        private getTravelLocations = () => {
            this.map.markers = [];
            angular.forEach(this.userTravel, (country) => {
                if (country.IsActive) {
                    this.addMarker(_.find(this.staticCountryGeoData, { 'siteId': country.CountryId }));
                }
            });
        }

        private addMarker = (location: commonInterfaces.IMapLocation): void => {

            var marker: ChaiTea.Common.Interfaces.IMapMarker = {
                id: location.siteId,
                coords: {
                    latitude: location.latitude,
                    longitude: location.longitude
                }
            };
            this.map.markers.push(marker);
        };
    }

    angular.module('app').controller('User.ProfileIndex', ProfileIndexCtrl);

    interface ISavehobby {
        UserId: number;
        HobbyId: number;
        IsActive: boolean;
        UserHobbyId: number;
    }

    class MyProfileMyHobbiesCtrl{
        showSportsTitle: boolean = true;
        saveHobbyitem: ISavehobby;
        tempHobbyList: Array<IHobbyInfo> = [];
        static $inject = [
            '$uibModalInstance',
            'chaitea.common.services.apibasesvc',
            'category',
            'availableHobbies',
            'userId',
            '$log'
        ];
        constructor(
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: Services.IBaseSvc,
            private category: User.Enums.HobbyCategory,
            private availableHobbies: Array<IHobbyInfo>,
            private userId: number,
            private $log: ng.ILogService
        ) {
            if (this.category == User.Enums.HobbyCategory.General) {
                this.showSportsTitle = false;
            }
            angular.forEach(this.availableHobbies, (item) => {
                var newObj: IHobbyInfo = {
                    UserHobbyId: item.UserHobbyId,
                    HobbyId: item.HobbyId,
                    Category: item.Category,
                    Name: item.Name,
                    Icon: item.Icon,
                    IsActive: item.IsActive,
                    IsChecked: item.IsChecked,
                    Color: item.Color
                };
                this.tempHobbyList.push(newObj);
            });
        }

        private checkHobby = (index: number): void => {
            this.tempHobbyList[index].IsChecked = !this.tempHobbyList[index].IsChecked;
        }

        private submit = (): void => {
            this.availableHobbies = [];
            this.availableHobbies = this.tempHobbyList;
            var updateOld: Array<ISavehobby> = [];
            angular.forEach(this.availableHobbies, (item) => {
                if ((item.IsChecked) && (item.IsActive == null)) {
                    this.saveHobbyitem = {
                        UserId: this.userId,
                        HobbyId: item.HobbyId,
                        IsActive: true,
                        UserHobbyId: item.UserHobbyId
                    };
                    this.apiSvc.save(this.saveHobbyitem, 'UserHobby').then((result) => {
                        item.UserHobbyId = result.UserHobbyId;
                    }, this.handleFailure);
                    item.IsActive = true;
                }
                if ((!item.IsChecked) && (item.IsActive == true)) {
                    this.saveHobbyitem = {
                        UserId: this.userId,
                        HobbyId: item.HobbyId,
                        IsActive: false,
                        UserHobbyId: item.UserHobbyId
                    };
                    updateOld.push(this.saveHobbyitem);
                    item.IsActive = false;
                }
                if ((item.IsActive == false) && (item.IsChecked)) {
                    this.saveHobbyitem = {
                        UserId: this.userId,
                        HobbyId: item.HobbyId,
                        IsActive: true,
                        UserHobbyId: item.UserHobbyId
                    };
                    updateOld.push(this.saveHobbyitem);
                    item.IsActive = true;
                }
            });
            if (updateOld.length > 0) {
                this.apiSvc.updateAll(updateOld, 'UserHobby/UpdateAll').then(() => { },this.handleFailure);
            }
            this.$uibModalInstance.close(this.availableHobbies);
        }

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }

        private close = ($e): void => {
            this.$uibModalInstance.close(this.availableHobbies);
        };
    }
    angular.module('app').controller('User.MyProfileMyHobbiesCtrl', MyProfileMyHobbiesCtrl);
            
    interface ISaveStickerInfo {
        UserId: number;
        StickerSet: number;
        SetMember: number;
        MemberLabel: number;
        DisplayPosition: number;
        IsActive: boolean;
        UserStickerId: number;
        }

    interface IMemberLabel {
        Id: number;
        Name: User.Enums.MemberLabel;
    }

    class MyProfileMyFamilyCtrl {
        prevIndex: number = null;
        selectedMember: IFamilyInfo;
        gotoNext: boolean = false;
        memberLabels: Array<IMemberLabel> = [];
        memberLabel: IMemberLabel;
        static $inject = [
            'userId',
            'staticStickerData',
            'userFamily',
            '$uibModalInstance',
            'chaitea.common.services.apibasesvc',
            '$log'
        ];

        constructor(
            private userId: number,
            private staticStickerData: Array<IFamilyInfo>,
            private userFamily: Array<IFamilyInfo>,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: Services.IBaseSvc,
            private $log: ng.ILogService
        ) {
        }

        private checkFamilyMember = (index: number) => {
            this.staticStickerData[index].IsChecked = !this.staticStickerData[index].IsChecked;
            this.selectedMember = this.staticStickerData[index];
            if (this.prevIndex != null) {
                this.staticStickerData[this.prevIndex].IsChecked = !this.staticStickerData[this.prevIndex].IsChecked;
            }
            this.prevIndex = index;
        }

        private removeCheckFamilyMember = (index: number) => {
            this.userFamily[index].IsChecked = !this.userFamily[index].IsChecked;
            this.selectedMember = this.userFamily[index];
            if (this.prevIndex != null) {
                this.userFamily[this.prevIndex].IsChecked = !this.userFamily[this.prevIndex].IsChecked;
            }
            this.prevIndex = index;
        }

        private next = () => {
            if (this.selectedMember) {
                this.gotoNext = true;
                if (this.prevIndex != null) {
                    this.staticStickerData[this.prevIndex].IsChecked = false;
                }
                this.getMemberLabels();
            }
        }

        private getMemberLabels = () => {
            var labelObj = User.Enums.MemberLabel;
            var i: number = 0;
            angular.forEach(labelObj, (item) => {
                if (User.Enums.MemberLabel.hasOwnProperty(item) && !/^\d+$/.test(item)) {
                    this.memberLabels.push({
                        Id: i,
                        Name: item
                    });
                    i++;
                }
            });
        }
        
        private addToFamily = () => {
            if (this.memberLabel && this.memberLabel.Id) {
                var saveMember: ISaveStickerInfo = {
                    UserId: this.userId,
                    StickerSet: this.selectedMember.StickerSet,
                    SetMember: this.selectedMember.SetMember,
                    MemberLabel: this.memberLabel.Id,
                    DisplayPosition: 0,
                    IsActive: true,
                    UserStickerId: this.selectedMember.UserStickerId
                };
                this.selectedMember.MemberLabel = User.Enums.MemberLabel[this.memberLabel.Id];
                this.apiSvc.save(saveMember, 'UserSticker').then((result) => {
                    this.selectedMember.UserStickerId = result.UserStickerId;
                    var newFamilyObj: IFamilyInfo = {
                        UserStickerId: this.selectedMember.UserStickerId,
                        SetMember: this.selectedMember.SetMember,
                        MemberLabel: this.selectedMember.MemberLabel,
                        StickerSet: this.selectedMember.StickerSet,
                        IsChecked: false,
                        Icon: this.selectedMember.Icon,
                        FontSize: this.selectedMember.FontSize
                    };
                    this.userFamily.push(newFamilyObj);
                    this.$uibModalInstance.close(this.userFamily);
                }, this.handleFailure);
            }
        }

        private removeFromFamily = () => {
            var removeMember: ISaveStickerInfo = {
                UserId: this.userId,
                StickerSet: this.selectedMember.StickerSet,
                SetMember: this.selectedMember.SetMember,
                MemberLabel: User.Enums.MemberLabel[this.selectedMember.MemberLabel],
                DisplayPosition: 0,
                IsActive: false,
                UserStickerId: this.selectedMember.UserStickerId
            };
            this.apiSvc.delete(removeMember.UserStickerId, 'UserSticker').then((reesult) => {
                _.remove(this.userFamily, { UserStickerId: removeMember.UserStickerId });
                this.$uibModalInstance.close(this.userFamily);
            }, this.handleFailure);
        }

        private addClose = ($e): void => {
            if (this.prevIndex != null) {
                this.staticStickerData[this.prevIndex].IsChecked = false;
            }
            this.$uibModalInstance.close(this.userFamily);
        };

        private removeClose = ($e): void => {
            if (this.prevIndex != null) {
                this.userFamily[this.prevIndex].IsChecked = false;
            }
            this.$uibModalInstance.close(this.userFamily);
        };

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }
    }

    angular.module('app').controller('User.MyProfileMyFamilyCtrl', MyProfileMyFamilyCtrl);

    interface ISaveTravel {
        UserId: number;
        CountryId: number;
        UserTravelId: number;
        IsActive: boolean;
    }

    class MyProfileMyTravelCtrl {
        checkedCountries: Array<ITravelInfo> = [];
        checkedCount: number = 0;
        userCount: number = 0;
        tempTravelList: Array<ITravelInfo> = [];
        static $inject = [
            '$uibModalInstance',
            'chaitea.common.services.apibasesvc',
            'allTravel',
            'userId',
            'userTravel',
            '$log'
        ];
        constructor(
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: Services.IBaseSvc,
            private allTravel: Array<ITravelInfo>,
            private userId: number,
            private userTravel: Array<ITravelInfo>,
            private $log: ng.ILogService
        ) {
            angular.forEach(this.allTravel, (item) => {
                var newObj: ITravelInfo = {
                    CountryId: item.CountryId,
                    Name: item.Name,
                    IsChecked: item.IsChecked,
                    IsActive: item.IsActive,
                    UserTravelId: item.UserTravelId
                };
                this.tempTravelList.push(newObj);
            });
        }

        private checkTravel = (index: number): void => {
            this.tempTravelList[index].IsChecked = !this.tempTravelList[index].IsChecked;
        }

        private submit = (): void => {
            this.allTravel = [];
            this.allTravel = this.tempTravelList;
            this.checkedCountries = [];
            this.checkedCountries = _.filter(this.allTravel, { 'IsChecked': true });
            this.checkedCount = this.checkedCountries.length;
            this.userCount = this.userTravel.length;
            if (this.checkedCount == 0 && this.userCount == 0) {
                this.close();
            }
            angular.forEach(this.checkedCountries, (item) => {
                if ((item.IsActive == null) && item.IsChecked) {
                    var saveObj: ISaveTravel = {
                        UserId: this.userId,
                        CountryId: item.CountryId,
                        UserTravelId: item.UserTravelId,
                        IsActive: true
                    };
                    this.apiSvc.save(saveObj, 'UserTravel').then((result) => {
                        this.checkedCount--;
                        item.UserTravelId = result.UserTravelId;
                        item.IsActive = true;
                        this.closeModal();
                    }, this.handleFailure);
                }
                else if ((item.IsActive == false) && item.IsChecked) {
                    var saveObj: ISaveTravel = {
                        UserId: this.userId,
                        CountryId: item.CountryId,
                        UserTravelId: item.UserTravelId,
                        IsActive: true
                    };
                    this.apiSvc.update(saveObj.UserTravelId, saveObj, 'UserTravel').then((result) => {
                        this.checkedCount--;
                        item.IsActive = true;
                        this.closeModal();
                    }, this.handleFailure);
                }
                else {
                    this.checkedCount--;
                    this.close();
                }
            });
            angular.forEach(this.userTravel, (item) => {
                if ((_.findIndex(this.checkedCountries, { 'CountryId': item.CountryId }) == -1) && item.IsChecked) {
                    var saveObj: ISaveTravel = {
                        UserId: this.userId,
                        CountryId: item.CountryId,
                        UserTravelId: item.UserTravelId,
                        IsActive: false
                    };
                    this.apiSvc.update(saveObj.UserTravelId, saveObj, 'UserTravel').then((result) => {
                        this.userCount--;
                        item.IsActive = false;
                        var index = _.findIndex(this.allTravel, { 'CountryId': item.CountryId });
                        this.allTravel[index].IsActive = false;
                        this.closeModal();
                    }, this.handleFailure);
                }
                else {
                    this.userCount--;
                    this.close();
                }
            });
        }

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }

        private closeModal = () => {
            if (this.checkedCount == 0 && this.userCount == 0) {
                var entireTravelInfo = {
                    checkedCountries: this.checkedCountries,
                    allTravel: this.allTravel
                };
                this.$uibModalInstance.close(entireTravelInfo);
            }
        }

        private close = (): void => {
            if (this.checkedCount == 0 && this.userCount == 0) {
                this.checkedCountries = [];
                this.checkedCountries = _.filter(this.allTravel, { 'IsChecked': true });
                var entireTravelInfo = {
                    checkedCountries: this.checkedCountries,
                    allTravel: this.allTravel
                };
                this.$uibModalInstance.close(entireTravelInfo);
            }
        };
    }
    angular.module('app').controller('User.MyProfileMyTravelCtrl', MyProfileMyTravelCtrl);
}