﻿/// <reference path="../../_libs.ts" />

module ChaiTea.User.Controllers {
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    interface IUserInfoVm extends IUserInfo {
        UserAttachments: any[];
    }

    interface ISectionChangedStates {
        photo: boolean;
        deletePhoto: boolean;
        phoneNumber: boolean;
        position: boolean;
        tasks: boolean;
        equipments: boolean;
    }

    enum FileType { Undefined, ProfilePicture };

    export class UserNotification {
        public NotificationTypeId = 0;
        public NotificationEventId = 0;
        public isActive = false;
        public isDefault = true;
        public _notificationText?= '';

        public constructor(init?: Partial<UserNotification>) {
            (<any>Object).assign(this, init);
        }
    }

    class IndexCtrl implements commonInterfaces.INgController {
        userContextId: number;
        user: IUserInfoVm = <IUserInfoVm>{};
        userId;
        orgUserId;
        employee;
        socialPhotos: any[] = [];
        Name: any;
        files = [];
        socialfiles = [];

        isCurrentUser: boolean = false;
        isManager: boolean = false;
        isEmployeeRole: boolean = false;

        isUserAnEmployee: boolean = false;

        defaultProfileImage: string = '';
        deletePhoto: boolean = false;
        phoneNumberInput: string = "";

        sectionChangedStates: ISectionChangedStates = {
            photo: false,
            deletePhoto: false,
            phoneNumber: false,
            position: false,
            tasks: false,
            equipments: false
        };

        wysiwygMenu;

        modalInstance;

        taskSelectLoading: boolean = false;
        taskInput: string = "";
        allTasks = [];
        equipmentSelectLoading: boolean = false;
        equipmentInput: string = "";
        allEquipments = [];
        allPositions = [];
        password;
        passwordMatch;
        notificationSubscriptions = [];
        userNotifications: UserNotification[] = [];

        groupedNotifications = {
            quality: [],
            personnel: [],
            financials: [],
            knowledgeCenter: [],
            governance: [],
            safety: []
        }

        showPersonnel = false;

        _saveButtonDisabled = true;

        static $inject = [
            '$scope',
            '$log',
            '$uibModal',
            'userContext',
            'userInfo',
            'sitesettings',
            Common.Services.UserInfoSvc.id,
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.apibasesvc',
            Common.Services.UtilitySvc.id,
            'chaitea.common.services.jobs',
            'chaitea.common.services.trainingemployee',
            Common.Services.NotificationSvc.id,
            Common.Services.AlertSvc.id
        ];
        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private userInfoSvc: ChaiTea.Common.Services.IUserInfoSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private apiSvc: Common.Services.IApiBaseSvc,
            private utilSvc: Common.Services.IUtilitySvc,
            private jobSvc: ChaiTea.Common.Services.IJobSvc,
            private trainingSvc: ChaiTea.Common.Services.ITrainingEmployeeSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private alertSvc: Common.Services.AlertSvc) {
            this.wysiwygMenu = sitesettings.wysiwygMenus.default;
        }

        public initialize = () => {
            var paramId = this.utilSvc.getIdParamFromUrl(null, true);
            this.user['userId'] = (paramId || this.userInfo.userId);

            this.isCurrentUser = (this.user.userId == this.userInfo.userId);
            this.isManager = this.userInfo.mainRoles.managers;
            this.isEmployeeRole = this.userInfo.mainRoles.employees;

            if (!this.isCurrentUser && !this.isManager) {
                bootbox.alert("You are not authorized to view this page.  We will redirect you back to your Dashboard.");
                setTimeout(() => {
                    window.location.href = this.sitesettings.basePath + '';
                }, 1000);
                return false;
            }

            this.getAllPositions();
            this.getAllTasks();
            this.getAllEquipments();
            this.getUserById(this.user.userId);

            this.mapNotificationsDescriptions();
        }

        /**
        * @description Delete photo
        */
        public removePhoto = () => {
            bootbox.confirm('Are you sure you want to delete the photo?', (result) => {
                if (result) {
                    this.remove();
                    this.saveUser();
                }
            });
        }

        /**
        * @description Save the changes made to the employee to the db
        */
        public saveUser = () => {
            this.userInfoSvc.updateUserInfo(this.userId, this.user).then((data) => {
                this.notificationSvc.successToastMessage("Information has been updated.");
                this.resetSectionStates();
            }, this.handleFailure);
        }

        /**
        * @description Remove the attachment from the collection
        */
        private remove = () => {
            var index: number;
            index = this.getAttachmentIndex(this.user.UserAttachments);
            if (index != -1) {
                this.user.UserAttachments.splice(index, 1)
                this.user.currentPicture = this.imageSvc.getUserImageFromAttachments(this.user.UserAttachments, true);
            }
            this.onSectionChange('deletePhoto');

        }

        public removeAttachment = ($index: number, id: number) => {
            if (!id) {
                this.$log.warn("Cannot delete image");
                return;
            }

            bootbox.confirm('Are you sure you want to remove this photo?', (result) => {
                if (result) {
                    this.socialPhotos.splice($index, 1);
                    _.remove(this.user.UserAttachments, (item) => (item.UserAttachmentId == id));
                    this.saveUser();
                }
            });
        }

        /**
        * @description Keeps track of changes to the different segments
        */
        public onSectionChange(sectionName: string) {
            this.sectionChangedStates[sectionName] = true;
        }

        private resetSectionStates = () => {
            this.sectionChangedStates = {
                photo: false,
                position: false,
                tasks: false,
                equipments: false,
                deletePhoto: false,
                phoneNumber: false
            };
        }

        /**
        * @description Returns the index of the profile picture attachment 
        */
        private getAttachmentIndex = (attachments: any[]): number => {

            var attachmentIndex: number = -1;
            var num: number = 0;
            angular.forEach(attachments, (attachment) => {
                if (attachment.UserAttachmentType === FileType[FileType.ProfilePicture]) {
                    attachmentIndex = num;
                    num++;
                }
            });

            return attachmentIndex;
        }

        /**
         * @description Returns the userInfo object by user id.
         */
        private getUserById = (userId: number): void => {
            async.auto({
                get_user_info: (callback) => {
                    if (!this.isCurrentUser) {
                        // EmployeeInfo
                        this.apiSvc.getById(userId, `Employee/User`).then((result) => {
                            if (result) {
                                this.apiSvc.getById(result.UserId, 'UserInfo').then((user) => {
                                    this.user = angular.extend({}, user);
                                    this.user.phoneNumber = user.PhoneNumber;
                                    this.user.userName = user.UserName;
                                    this.user.userEmail = user.Email;
                                    this.user.firstName = user.FirstName;
                                    this.user.lastName = user.LastName;
                                    this.userId = result.UserId;
                                    this.orgUserId = result.EmployeeId;
                                    callback(null, this.user);
                                }, this.handleFailure);
                            }
                        }, err => callback(err, null));
                    } else {
                        this.apiSvc.getById(this.userContext.UserId, 'UserInfo').then((user) => {
                            this.user = angular.extend({}, user);
                            this.user.phoneNumber = user.PhoneNumber;
                            this.user.userName = user.UserName;
                            this.user.userEmail = user.Email;
                            this.user.firstName = user.FirstName;
                            this.user.lastName = user.LastName;
                            this.userId = this.userContext.UserId;
                            callback(null, this.user);
                        }, this.handleFailure);
                    }
                },
                get_employee_info: ['get_user_info', (callback, results) => {
                    if (this.orgUserId || this.userInfo.userId) {
                        this.getEmployeeById(this.orgUserId || this.userInfo.userId)
                            .then(res => callback(null, res),
                            err => callback(err, null));
                    } else {
                        callback(null, results.get_user_info);
                    }
                }]
            }, (error, results) => {
                if (error) {
                    this.$log.error(error);
                }
                this.user.currentPicture = this.imageSvc.getUserImageFromAttachments(this.user.UserAttachments, true);
            });
        }

        /**
        * @description Returns the job description object by employee id.
        */
        private getEmployeeById = (employeeId: number): angular.IPromise<any> => {
            //get data from service
            return this.apiSvc.getById(employeeId, 'TrainingEmployee').then((employee: Training.Interfaces.IJobDescriptionExtended) => {
                if (!employee.EmployeeId) { return false; };

                this.isUserAnEmployee = true;
                this.user.phoneNumber = employee.PhoneNumber;
                this.orgUserId = employee.EmployeeId;

                return this.employee = employee;
            }, (err) => {
                //Do not want to notify user if an error;
                this.$log.error(err);
            });
        }

        /**
        * @description Saves the phone number to the db
        */
        public savePhoneNumber = ($event) => {
            if (!this.user.phoneNumber) return;

            // this.user.phoneNumber = phoneNumFromInput;
            this.onSectionChange('phoneNumber');
            this.saveUser();
        }

        public savePassword = ($event) => {
            if (this.password !== null && this.password !== undefined) {
                var appUser = {
                    UserId: this.userId,
                    Password: this.password
                };

                this.apiSvc.update(this.userId, appUser, 'ApplicationUser/UpdatePassword').then((res) => {
                    this.password = null;
                    this.passwordMatch = null;
                    this.notificationSvc.successToastMessage("Password has been updated.");
                }, this.handleFailure);
            }
        }

        /**
         * @description function to save image to AWS/S3 and use returned ID to insite db.
         * @param {string} fileName
         */
        public uploadImage = (user, files, $event, userAttachmentType?) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length && files[0]) {
                var file = files[0];

                this.imageSvc.readImage(file).then((data: any) => {
                    //error check the image here
                    this.cropImage(user, data.dataUrl, userAttachmentType);
                }, this.handleFailure);
            }
        }

        public cropImage = (user: any, file: any, userAttachmentType?: any) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Common.ImageCropCtrl as vm',
                size: 'md',
                resolve: {
                    file: () => { return file; },
                    needDescription: () => { return false; }
                },
                backdrop: 'static'
            });

            this.modalInstance.result.then((modalRes: any) => {

                if (!modalRes) return;

                this.imageSvc.save(modalRes.originalImage, false, true).then((res: commonInterfaces.IAwsObjectDetails) => {

                    var tempAttach = {
                        UniqueId: res.Name,
                        UserAttachmentType: userAttachmentType || 'ProfilePicture',
                        AttachmentType: 'Image'
                    }
                    user.UserAttachments = _.isEmpty(user.UserAttachments) ? [] : user.UserAttachments;
                    user.UserAttachments.push(tempAttach);
                    this.imageSvc.save(modalRes.croppedImage, false, true, this.imageSvc.getThumbnailName(res.Guid)).then((res: commonInterfaces.IAwsObjectDetails) => {

                        this.userInfoSvc.updateUserInfo(this.userId, user).then((res) => {
                            if (!res) throw Error('No result');
                            this.notificationSvc.successToastMessage('Image saved successfully.')
                            if (!userAttachmentType) {
                                this.user.currentPicture = this.imageSvc.getUserImageFromAttachments(user.UserAttachments, true);
                            } else {
                                if (res.UserAttachments) {
                                    user.UserAttachments = res.UserAttachments;
                                }
                            }
                        }, this.handleFailure);

                    }, (error) => {
                        this.notificationSvc.errorToastMessage(error);
                    });

                }, (error) => {
                    this.notificationSvc.errorToastMessage(error);
                });

            }, () => {

            });

        }

        /**
        * @description Helper to format filename before upload.
        * @param {string} fileName
        */
        getRandToken = () => {
            return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
        }

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
            this.notificationSvc.errorToastMessage();
        }

        public saveEmployee = () => {
            this.trainingSvc.updateTrainingEmployee(this.employee.EmployeeId, this.employee).then((data) => {
                this.notificationSvc.successToastMessage("Employee has been updated.");
                this.resetSectionStates();
            }, this.handleFailure);
        }

        private mapNotificationsDescriptions = () => {
            this.apiSvc.query({}, 'UserNotificationSubscription').then(userNotifications => {
                this.userNotifications = userNotifications;
                // Get notificationText from mappings; group by Type for display; map to global variable;
                this.userNotifications =
                    _.filter(this.userNotifications, (val) => {
                        let mapping = _.find(this.notificationMappings, current => {
                            return (current.eventId === val.NotificationEventId)
                                && (current.typeId === val.NotificationTypeId)
                                && (_.contains(this.userInfo.activeRoles, Roles[current.role].toLocaleLowerCase()));
                        });

                        if (!_.isEmpty(mapping)) {
                            val._notificationText = mapping.text || '';
                            return val;
                        }

                    });

                let grouped = _.groupBy(this.userNotifications, 'NotificationTypeId');
                _.map(grouped, (val, key) => {
                    let notificationType = NotificationType[key];
                    this.groupedNotifications[_.camelCase(notificationType)] = val;

                    if (_.contains(this.personnelNotificationTypes, notificationType)) {
                        this.showPersonnel = true;
                    }
                });
                this.startWatchOnNotificationConfigs();
                this._saveButtonDisabled = true;
            });
        }

        public saveNotificationConfigs = (): angular.IPromise<any> => {
            return this.apiSvc.updateAll(this.getFlattenedNotificationConfig(), 'UserNotificationSubscription')
                .then(() => {
                    this.notificationSvc.displaySimpleToast("Your notification configuration has been saved.");
                }, this.handleFailure);
        }

        public resetNotificationConfigs = () => {
            this.alertSvc.confirmWithCallback('Are you sure you want to reset your notifications to default?', (result) => {
                if (result) {
                    // TODO Save here??? Ask design
                    this.apiSvc.getResource('UserNotificationSubscription').delete().$promise.then(
                        () => {
                            _.forEach(this.groupedNotifications, (group, key) => {
                                _.forEach(this.groupedNotifications[key], val => {
                                    if (!val.isDefault) {
                                        val.isActive = false;
                                    }
                                })
                            })
                            this.notificationSvc.displaySimpleToast('Your notification configuration has been reset to default.');
                        },
                        (err) => {
                            this.notificationSvc.errorToastMessage("Unabled to update notification configuration. Please contact support.")
                        });
                }
            })
        }

        private getFlattenedNotificationConfig = () => {
            return _(_.cloneDeep(this.groupedNotifications))
                .map((val, key) => {
                    return _.flatten(val, true);
                })
                .flatten()
                .value();
        }

        private startWatchOnNotificationConfigs = () => {
            this.$scope.$watch(() => (this.groupedNotifications), (oldVal, newVal) => {
                if (_.isEqual(oldVal, newVal)) {
                    this._saveButtonDisabled = true;
                    return;
                }
                this._saveButtonDisabled = false;
            }, true);
        }

        //#region Tasks
        public addNewTaskItemKeyPress = ($event) => {
            if ($event.which === 13) {
                this.addNewTaskItem($event);
            }
        }

        public addNewTaskItem = ($event) => {
            var taskNameFromInput: any = this.taskInput;

            if (_.isObject(this.taskInput))
                taskNameFromInput = this.taskInput['Name'];

            if (!taskNameFromInput.length || taskNameFromInput === "") {
                return false;
            }

            this.taskSelectLoading = true;
            if (!_.any(this.allTasks, { 'Name': taskNameFromInput })) {
                var newJobTask = {
                    "Name": taskNameFromInput
                };
                this.jobSvc.saveNewJobTask(newJobTask).then((data) => {
                    this.allTasks.push(data);
                    this.employee.EmployeeTasks.push(data);
                    this.taskSelectLoading = false;
                    this.taskInput = "";
                    this.onSectionChange('tasks');
                })
            } else {
                if (!_.any(this.employee.EmployeeTasks, this.taskInput)) {
                    this.employee.EmployeeTasks.push(this.taskInput);
                    this.onSectionChange('tasks');
                }
                else {
                    var message: string = this.taskInput['Name'] + " task has already been assigned";

                    bootbox.alert(message);
                }
                this.taskInput = "";

            };
        }

        public removeNewTaskItem = (index) => {
            this.employee.EmployeeTasks.splice(index, 1);
            this.onSectionChange('tasks');
        }

        public getTasksForView = (current) => {
            var tasksCopy = angular.copy(this.allTasks);
            return tasksCopy;
        }

        //#endregion

        //#region Equipment and Supplies
        public addNewEquipmentItemKeyPress = ($event) => {
            if ($event.which === 13) {
                this.addNewEquipmentItem($event);
            }
        }

        public addNewEquipmentItem = ($event) => {

            var equipmentNameFromInput: any = this.equipmentInput;

            if (_.isObject(this.equipmentInput)) equipmentNameFromInput = this.equipmentInput['Name'];


            if (!equipmentNameFromInput.length || equipmentNameFromInput === "") {
                return false;
            }

            this.equipmentSelectLoading = true;
            if (!_.any(this.allEquipments, { 'Name': equipmentNameFromInput })) {

                var newJobEquipment = {
                    "Name": equipmentNameFromInput
                };
                this.jobSvc.saveNewJobEquipment(newJobEquipment).then((data) => {
                    this.allEquipments.push(data);
                    this.employee.EmployeeEquipment.push(data);
                    this.equipmentSelectLoading = false;
                    this.equipmentInput = "";
                    this.onSectionChange('equipments');
                })

            } else {

                //get obj from all list
                var exisitng: any = _.find(this.allEquipments, { 'Name': equipmentNameFromInput })

                if (!_.any(this.employee.EmployeeEquipment, exisitng)) {
                    this.employee.EmployeeEquipment.push(exisitng);
                    this.onSectionChange('equipments');
                } else {
                    var message: string = this.equipmentInput['Name'] + " equipment has already been assigned";
                    bootbox.alert(message);
                }
                this.equipmentInput = "";

            };
        }

        public removeNewEquipmentItem = (index) => {
            this.employee.EmployeeEquipment.splice(index, 1);
            this.onSectionChange('equipments');
        }

        public getEquipmentsForView = (current) => {
            var equipmentsCopy = angular.copy(this.allEquipments);
            return equipmentsCopy;
        }

        //#endregion

        private getAllPositions = () => {
            this.jobSvc.getAllPositions().then((data) => {
                angular.forEach(data, (item) => {
                    this.allPositions.push(item);
                })
            })
        }

        private getAllTasks = () => {
            this.jobSvc.getAllTasks().then((data) => {
                angular.forEach(data, (item) => {
                    this.allTasks.push(item);
                })
            })
        }

        private getAllEquipments = () => {
            this.jobSvc.getAllEquipments().then((data) => {
                angular.forEach(data, (item) => {
                    this.allEquipments.push(item);
                })
            })
        }

        private notificationMappings: NotificationMapping[] = [
            {
                typeId: NotificationType.Audit,
                eventId: NotificationEvent.Passed,
                text: 'When audit passes in area they are responsible for and their scorecard has been updated',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Audit,
                eventId: NotificationEvent.Failed,
                text: 'When audit failed in area they are responsible for and their scorecard has been updated',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Audit,
                eventId: NotificationEvent.Failed,
                text: 'When an audit fails',
                role: Roles.Managers
            },
            {
                typeId: NotificationType.Audit,
                eventId: NotificationEvent.Failed,
                text: 'When an audit fails',
                role: Roles.Customers
            },
            {
                typeId: NotificationType.Complaint,
                eventId: NotificationEvent.Created,
                text: 'When complaint is created about them and their scorecard has been updated',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Complaint,
                eventId: NotificationEvent.Created,
                text: 'When a complaint is created',
                role: Roles.Customers
            },
            {
                typeId: NotificationType.Compliment,
                eventId: NotificationEvent.Created,
                text: 'When compliment is created',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Compliment,
                eventId: NotificationEvent.Created,
                text: 'When compliment is created (Client)',
                role: Roles.Customers
            },
            {
                typeId: NotificationType.Ownership,
                eventId: NotificationEvent.Accepted,
                text: 'When a Report It is accepted',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Ownership,
                eventId: NotificationEvent.Rejected,
                text: 'When a Report Is rejected',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Ownership,
                eventId: NotificationEvent.Created,
                text: 'When an Associate\'s Report Is created',
                role: Roles.Managers
            },
            {
                typeId: NotificationType.ToDo,
                eventId: NotificationEvent.Created,
                text: 'When assigned to a ToDo',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.ToDo,
                eventId: NotificationEvent.Closed,
                text: 'When to a To-Do is completed',
                role: Roles.Managers
            },
            {
                typeId: NotificationType.Conduct,
                eventId: NotificationEvent.Created,
                text: 'When they receive a verbal warning, written warning, final warning, administrative leave or corrective action and their scorecard has been updated.',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Safety,
                eventId: NotificationEvent.Created,
                text: 'When an incident is reported about you and your scorecard has been updated.',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Professionalism,
                eventId: NotificationEvent.Passed,
                text: 'When professionalism audit fails and their scorecard has been updated.',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.Professionalism,
                eventId: NotificationEvent.Failed,
                text: 'When professionalism audit passes and their scorecard has been updated.',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.KnowledgeCenter,
                eventId: NotificationEvent.Created,
                text: 'When a new course is assigned to them',
                role: Roles.Employees
            },
            {
                typeId: NotificationType.WorkOrder,
                eventId: NotificationEvent.Created,
                text: 'When work order is created',
                role: Roles.Managers
            },
            {
                typeId: NotificationType.WorkOrder,
                eventId: NotificationEvent.Upcoming,
                text: '3 days prior to an upcoming Work Order',
                role: Roles.Managers
            },
        ]

        private personnelNotificationTypes = [NotificationType.Attendance, NotificationType.Conduct, NotificationType.Professionalism];
    }

    //TODO this might be declared somewhere else....
    export enum Roles {
        Managers = 2,
        Employees = 4,
        Customers = 9
    }
    export enum NotificationType {
        Other = 1,
        Audit = 2,
        Complaint = 3,
        Compliment = 4,
        Safety = 5,
        ToDo = 6,
        Message = 7,
        Conduct = 8,
        Attendance = 9,
        Professionalism = 10,
        KnowledgeCenter = 11,
        Ownership = 12,
        WorkOrder = 13
    }

    export enum NotificationEvent {
        Other = 1,
        Created = 2,
        Closed = 3,
        Failed = 4,
        Passed = 5,
        Accepted = 6,
        Rejected = 7,
        Deleted = 8,
        Exempted = 9,
        Unexempted = 10,
        AssociateFailed = 11,
        VendorCreated = 12,
        Upcoming = 13
    }
    export interface NotificationMapping {
        typeId: NotificationType;
        eventId: NotificationEvent;
        text: string;
        role: Roles
    }

    angular.module('app').controller('User.SettingsIndex', IndexCtrl);
}