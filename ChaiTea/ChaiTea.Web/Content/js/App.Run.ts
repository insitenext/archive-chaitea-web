﻿module ChaiTea {
    'use strict';

    import commonSvc = Common.Services;
    import coreSvc = Core.Services;

    /**
     * @description Invoke the run function for 'app' module.
     */
    run.$inject = [
        '$rootScope',
        'userInfo',
        'userContext',
        'sitesettings',
        'chaitea.common.services.reportsvc',
        'chaitea.core.services.messagebussvc',
        '$window',
        'chaitea.core.services.usercontextsvc',
        'chaitea.common.services.templatecachesvc',
        '$httpParamSerializer',
        'chaitea.common.services.utilssvc',
        '$state'
    ];
    function run(
        $rootScope: any,//angular.IRootScopeService,
        userInfo: IUserInfo,
        userContext: IUserContext,
        sitesettings: ISiteSettings,
        reportHelperSvc: commonSvc.IReportSvc,
        messageBusSvc: coreSvc.IMessageBusSvc,
        $window: angular.IWindowService,
        userContextSvc: ChaiTea.Core.Services.IUserContextSvc,
        templateCacheSvc: ChaiTea.Common.Services.ITemplateCacheSvc,
        $httpParamSerializerProvider: any,
        utilitySvc: Common.Services.IUtilitySvc,
        $state: any
    ): void {

        var windowElement = angular.element(window);
        $rootScope.isMultiSiteUser = false;

        //HACK to hide header and sidebar for viewing within MobileWebView in Titanium........-_-
        $rootScope.isMobileWebView = !_.isEmpty(location.search.match('isMobileWebView'));

        // Initialize Report Settings
        if (userInfo.userId) {
            reportHelperSvc.getReportSettings();
        }

         // LeftSideBar menu item click event
        $rootScope.menuItemClick = ($event) => {
            angular.element($event.currentTarget).parent('li').toggleClass('active');

            if (windowElement.width() < 1170) {
                updateSideBar(false);
            };
        }

        // Main hamburger button click event
        $rootScope.toggleSideBar = () => {
            if (windowElement.width() < 1170) { // sitesettings.windowSize.largeDesktop
                $rootScope.activeSide = !$rootScope.activeSide;
                updateSideBar($rootScope.activeSide);

                if (sitesettings.windowSizes.isTiny && $rootScope.activeSide) {
                    $rootScope.navWidth = sitesettings.mobileNavWidth;
                    $rootScope.navLeft = 0;
                } else if (sitesettings.windowSizes.isTiny && !$rootScope.activeSide) {
                    $rootScope.navWidth = 0;
                    $rootScope.navLeft = -(sitesettings.mobileNavWidth + 20);
                }
            }
        }

        updateSideBar();

        windowElement.resize(() => { updateSideBar(); });

        function updateSideBar(isActive?: boolean) {
            //Show Activity Center by default if is large screen
            if (!$rootScope.activeNotification) {
                $rootScope.activeNotification = windowElement.width() > 1600;
            }
        }

        // ZenDesk Feedback widget
        $rootScope.openFeedbackWidget = () => {
            messageBusSvc.emitMessage(Common.Interfaces.MessageBusMessages.OpenFeedbackWidget, {});
        };

        userContextSvc.GetSetLocalStore().then((res) => {
            $rootScope.isMultiSiteUser = (res.Sites && res.Sites.Options.length > 1) || (res.Clients && res.Clients.Options.length > 1);
        });

        //Add Notification templates to $templateCache
        var itemsToCache = [
            sitesettings.relativeModulePaths.Components + 'NotificationFeed/Templates/notificationfeed.innermessage.tmpl.html',
            sitesettings.relativeModulePaths.Components + 'NotificationFeed/Templates/notificationfeed.view.tmpl.html',
            sitesettings.relativeModulePaths.Common + 'Templates/modal.tmpl.html',
            sitesettings.relativeModulePaths.Components + 'Map/Templates/map.modal.tmpl.html',
            sitesettings.relativeModulePaths.Components + 'Headers/UtilityHeader/utility-nav-search.tmpl.html'
        ];

        templateCacheSvc.cacheMultipleTemplates(itemsToCache);

        /*
         * Override $state.href to allow for Urls without hashbang(#)
         * even when $locationProvider.html5Mode is set to false;
        */
        $state.baseHref = $state.href;
        $state.href = function href(stateOrName, params, options) {
            var href = $state.baseHref(stateOrName, params, options);
            if (_.has(params, 'external')) {
                href = href.replace('#/', '');
            }
            return href;
        };
        
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams: Object, fromState, fromParams) {
                //Allow for use of Ui.Router to link to pages outside of its "state"
                if (toState.external) {
                    event.preventDefault();

                    var currentParams = utilitySvc.convertQueryParamsIntoObject();
                    toParams = angular.merge(toParams, currentParams);

                    var newCombinedParams = $httpParamSerializerProvider(toParams);

                    var toUrl = <string>toState.url.split("?")[0];

                    if (toUrl[0] === "/") {
                        toUrl = toUrl.substring(1, toUrl.length);
                    }

                    $window.open(sitesettings.basePath + toUrl + ("?" + newCombinedParams || ""), '_self');
                }
            });
    }

    angular.module('app').run(run);
}