﻿/// <reference path = "../../../_libs.ts" />

module ChaiTea.Contact.ContactPage.Controllers {

    "use strict";
    import coreSvc = ChaiTea.Core.Services;
    import commmonSvc = ChaiTea.Common.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IUserAttachment {
        UserAttachmentType: string;
        UniqueId: string;
    }

    enum FileType { Undefined, ProfilePicture };

    class ContactPageIndexCtrl {

        basepath: string;
        employeeName: string;
        employeeId: number;
       
        supervisorName: string;
        supervisorPhone: string;
        supervisorEmail: string;
        supervisorImage: string;
        supervisorTitle: string;

        supervisorsBossName: string;
        supervisorsBossPhone: string;
        supervisorsBossEmail: string;
        supervisorsBossImage: string;
        supervisorsBossTitle: string;

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'userInfo',
            '$log',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            userContext: IUserContext,
            userInfo: IUserInfo,
            private $log: angular.ILogService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {
            this.basepath = sitesettings.basePath;
            this.employeeId = userInfo.userId;

            this.getSupervisor();

            mixpanel.track("Viewed Contact Information");
        }
        
        getSupervisor = (): void => {
            
            var defaultImg = "~/Content/img/user.jpg"
            this.apiSvc.getById(this.employeeId, "TrainingEmployee/WithJob").then((employee): any => {
                if (!employee.Supervisor) return;
                
                this.supervisorName = employee.Supervisor.Name;
                this.supervisorEmail = employee.Supervisor.Email;
                this.supervisorPhone = employee.Supervisor.PhoneNumber;
                this.supervisorTitle = employee.Supervisor.JobDescription;

                var attachment: Array<IUserAttachment> = [];
                if (employee.Supervisor.ProfilePictureUniqueId) {

                    attachment.push({ UserAttachmentType: "ProfilePicture", UniqueId: employee.Supervisor.ProfilePictureUniqueId });

                    this.supervisorImage = this.imageSvc.getUserImageFromAttachments(attachment, true);
                }
                else
                    this.supervisorImage = '';
                attachment.splice(0, 1);

                this.supervisorsBossName = employee.Supervisor.Manager.Name;
                this.supervisorsBossEmail = employee.Supervisor.Manager.Email;
                this.supervisorsBossPhone = employee.Supervisor.Manager.PhoneNumber;
                this.supervisorsBossTitle = employee.Supervisor.Manager.JobDescription;

                if (employee.Supervisor.Manager.ProfilePictureUniqueId) {
                    attachment.push({ UserAttachmentType: "ProfilePicture", UniqueId: employee.Supervisor.Manager.ProfilePictureUniqueId });
                    this.supervisorsBossImage = this.imageSvc.getUserImageFromAttachments(attachment, true);
                }
                else
                    this.supervisorsBossImage = null;
            });
        }
    }
    angular.module("app").controller("ContactPageIndexCtrl", ContactPageIndexCtrl);
} 