﻿
module ChaiTea.Admin.Interfaces {
    export interface INodeObject {
        Name: string;
        Id: string;
        SelectedValue: number;
        Collection: any;
    }

    export interface IAttribute {
        AttributeId: number;
        Code: string;
        Name?: string;
    }

    export interface IAttributeAttachment {
        AttributeAttachmentId: number;
        AttributeId: number;
        AttachmentId: number;
        AttributeValue: string;
        FileName: string;
        UniqueId: string;
        Attachment?: Common.Interfaces.IAttachment;
        DownloadUrl?: string;
        AttachmentType: string;
    }
}
 