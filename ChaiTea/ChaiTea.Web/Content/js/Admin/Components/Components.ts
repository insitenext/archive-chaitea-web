﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Components {

    class ClientSelecterCtrl {

        clients: Array<ChaiTea.Services.Interfaces.ILookupListOption>;
        client: ChaiTea.Services.Interfaces.ILookupListOption;

        sites: Array<ChaiTea.Services.Interfaces.ILookupListOption>;
        site: ChaiTea.Services.Interfaces.ILookupListOption;

        programs: Array<ChaiTea.Services.Interfaces.ILookupListOption>;
        program: ChaiTea.Services.Interfaces.ILookupListOption;

        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            'localStorageService',
            'sitesettings',
            'chaitea.common.services.apibasesvc'];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

        }

        private $onInit = () => {
            this.loadClientList();
        }

        private loadClientList = () => {

            var clients: any = this.localStorageService.get('clients');
            if (clients) {

                this.clients = clients.Options;

            } else {

                this.apiSvc.getLookupList('User/Clients').then((res) => {
                    this.clients = res.Clients;
                    this.localStorageService.set('client', res.Clients.Options);
                });

            }            

        }

        private loadSiteList = (clientId: number) => {

            this.apiSvc.getLookupList('User/Sites/' + clientId + '/false').then((res) => {

                this.sites = res.Sites.Options;

            });
        }

        private loadProgramList = (siteId: number) => {

            this.apiSvc.getLookupList('User/Programs/' + siteId + '/false').then((res) => {

                this.programs = res.Programs.Options;

            }); 

        }

        public onListChange = (list: string) => {

            if (list === 'client' && this.client) {
                this.loadSiteList(this.client.Key);
            }

            if (list === 'site' && this.site && this.site.Key) {
                this.loadProgramList(this.site.Key);
            }

            if (list === 'program' && this.site) {

            }

        }

        private saveClientSelection = () => {
                
        }

    }

    angular.module('app').component('clientSelecter', {
        template: `
                    <div class="row padding-top padding-bottom">
                        <div class="col-md-4">
                            <select class="form-control ng-pristine ng-valid ng-touched" id="clients"
                                                ng-options="c.Value for c in $ctrl.clients track by c.Key"
                                                ng-change="$ctrl.onListChange('client')"
                                                ng-model="$ctrl.client"></select>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control ng-pristine ng-valid ng-touched" id="clients"
                                                ng-options="c.Value for c in $ctrl.sites track by c.Key"
                                                ng-change="$ctrl.onListChange('site')"
                                                ng-model="$ctrl.site"></select>
                        </div>
                        <div class="col-md-4">
                            <select ng-if="!$ctrl.hideProgram" ng-disabled="true" class="form-control ng-pristine ng-valid ng-touched" id="clients"
                                                ng-options="c.Value for c in $ctrl.programs track by c.Key"
                                                ng-model="$ctrl.program"></select>
                        </div>
                    </div>
                `,
        controller: ClientSelecterCtrl,
        bindings: {
            client: '=',
            site: '=',
            program: '=',
            hideProgram: '='
        }
    });

}