﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for resolving admin tenant management services.
 */
module ChaiTea.Admin.Services {
    'use strict';

    import commonSvc = ChaiTea.Common.Services;

    export interface ITenantManagementResolverSvc {
        getOrgs(): angular.IPromise<any>;
        getClients(): angular.IPromise<any>;
        getPrograms(): angular.IPromise<any>;
        getAreas(): angular.IPromise<any>;
        getUsers(): angular.IPromise<any>;
    }

    class TenantManagementResolverSvc implements angular.IServiceProvider {
        carchedOrgs: Array<any> = [];
        cachedClients: Array<any> = [];
        cachedPrograms: Array<any> = [];
        cachedAreas: Array<any> = [];

        static $inject = [
            '$q',
            '$stateParams',
            'chaitea.common.services.tenantmanagementsvc'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(
            private $q: angular.IQService,
            private $stateParams: angular.ui.IStateParamsService,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        }

        public $get(): ITenantManagementResolverSvc {

            return {
                getOrgs: () => { return this.getOrgs(); },
                getClients: () => { return this.getClients(); },
                getPrograms: () => { return this.getPrograms(); },
                getAreas: () => { return this.getAreas(); },
                getUsers: () => { return this.getUsers(); }
            };
        }

        getOrgs = () => {
            return new this.$q(function (resolve, reject) {
                if (this.cachedOrgs.length) {
                    resolve(this.cachedOrgs);
                } else {
                    this.tenantManagementSvc.getOrgsByOData().then(function (data) {
                        this.cachedOrgs = data;
                        resolve(data);
                    });
                }
            });
        };

        getClients = () => {
            return new this.$q(function (resolve, reject) {
                if (this.cachedClients.length) {
                    resolve(this.cachedClients);
                } else {
                    this.tenantManagementSvc.getClientsByOData().then(function (data) {
                        this.cachedClients = data;
                        resolve(data);
                    });
                }
            });
        };

        getPrograms = () => {
            return new this.$q(function (resolve, reject) {
                if (this.cachedPrograms.length) {
                    resolve(this.cachedPrograms);
                } else {
                    this.tenantManagementSvc.getProgramsByOData({ $orderby: 'Name' }).then(function (data) {
                        this.cachedPrograms = data;
                        resolve(data);
                    });
                }
            });
        };

        getAreas = () => {
            return new this.$q(function (resolve, reject) {
                if (this.cachedAreas.length) {
                    resolve(this.cachedAreas);
                } else {
                    this.tenantManagementSvc.getAreasByOData().then(function (data) {
                        this.cachedAreas = data;
                        resolve(data);
                    });
                }
            });
        };

        getUsers = () => {
            return new this.$q(function (resolve, reject) {
                this.tenantManagementSvc.getUsers().then(function (data) {
                    resolve(data);
                });
            });
        };
    }

    angular.module('app').service('chaitea.admin.services.tenantmanagementresolversvc', TenantManagementResolverSvc);
} 