﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    export interface IOption {
        ID?: number;
        Name: string;
    }

    class TenantManagementUsersCtrl {
        basePath: string;
        modalInstance;
        private cacheTimeout = 900; // 15 minute timeout
        roles: Array<commonInterfaces.IUserRole> = [];
        myPromise = null;
        languages: Array<IOption> = [];
        clients: Array<commonInterfaces.IClient> = [];

        // Paging
        recordsToSkip: number = 0;
        top: number = 20;
        users = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        searchCriteria: string;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc',
            'orgs',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.core.services.usercontextsvc',
            Common.Services.NotificationSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private orgs,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            private userContextService: ChaiTea.Core.Services.IUserContextSvc,
        private notificationSvc: Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;
            this.initialize();
        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            this.setInitialLookups();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {

        }

        /**
        * LOOKUPS
        * @description Set initial lookup collection.
        */
        setInitialLookups = () => {
            this.getLanguages();
        }

        /**
         * @description Resets the paging and re-get data.
         */
        getUsersReset = (): void => {
            this.users = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getUsers();
        }

        /**
         * @desription Get users.
         */
        getUsers = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var param = <commonInterfaces.IPagingParamModel>{
                top: this.top,
                skip: this.recordsToSkip
            };
            if (this.searchCriteria) {
                param.phrase = this.searchCriteria;
            }

            this.tenantManagementSvc.getUsers(param).then((data) => {
                if (data.length == 0) {
                    this.isAllLoaded = true;
                }

                angular.forEach(data, (obj) => {
                    this.users.push(obj);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
                window.scrollTo(0, document.body.scrollHeight);
            }, this.onFailure);
        }

        /**
         * @description Get roles.
         */
        getRoles = (): angular.IPromise<any> => {
            return new this.$q((resolve, reject) => {
                if (this.roles.length) {
                    resolve(this.roles);
                    return;
                }

                return this.tenantManagementSvc.getRolesByOData({}).then((result) => {
                    this.roles = result;
                    resolve(this.roles);
                });
            });
        }

        getClients = (userid: number): angular.IPromise<any> => {
            this.clients = [];
            var param = <commonInterfaces.IUser>{
                Id: userid
            };
            return new this.$q((resolve, reject) => {
                return this.tenantManagementSvc.getClientsById(param).then((result) => {
                    this.clients = result;
                    resolve(this.clients);
                });
            });
        }

        private optionMapper = (o: any) => {
            if (!o.Key) {
                o.Key = 0;
            }
            return { ID: o.Key, Name: o.Value };
        }

        private getLanguages = () => {
            var key: string = 'languages-list';

            //check cache first
            var dataObj: any = this.localStorageSvc.getObject(key, this.cacheTimeout);

            if (!dataObj) {
                //pull new list
                this.myPromise = this.userContextService.GetLanguages().then((data: any) => {
                    if (data) {
                        //set for dropdown
                        this.languages = _.map(data.Languages.Options, this.optionMapper);
                        //cache
                        this.localStorageSvc.setObject(key, data.Languages.Options, true);
                    }
                }, this.onFailure);
            } else {
                this.languages = _.map(dataObj.data, this.optionMapper);
            }
        }

        /**
         * @description Add/edit a user in a modal.
         */
        addEditUser = ($event: JQueryEventObject, user) => {
            if ($event) $event.preventDefault();

            // Get roles and open the edit modal
            this.getRoles().then((roles) => {
                if (user) {
                    this.getClients(user.Id).then((clients) => {
                        user.Clients = clients;
                        this.modalInstance = this.$uibModal.open({
                            templateUrl: 'manageUserModal.tmpl.html',
                            controller: 'Admin.EditUserModalCtrl as vm',
                            size: 'md',
                            resolve: {
                                clients: () => {
                                    return _.map(this.orgs, (obj) => {
                                        return { Id: obj['Id'], Name: obj['Name'] };
                                    });
                                },
                                user: () => {
                                    if (user) return angular.copy(user);

                                    return {
                                        Clients: [],
                                        Roles: []
                                    };
                                },
                                roles: () => { return roles; },
                                languages: () => { return angular.copy(this.languages); }
                            }
                        });

                        // Update the UI
                        this.modalInstance.result.then((userObj: commonInterfaces.IUser) => {
                            if (user) {
                                angular.copy(userObj, user);
                            } else {
                                userObj.Password = null;
                                this.users.push(userObj);
                            }

                        }, null);
                    });
                } else {
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: 'manageUserModal.tmpl.html',
                        controller: 'Admin.EditUserModalCtrl as vm',
                        size: 'md',
                        resolve: {
                            clients: () => {
                                return _.map(this.orgs, (obj) => {
                                    return { Id: obj['Id'], Name: obj['Name'] };
                                });
                            },
                            user: () => {
                                if (user) return angular.copy(user);

                                return {
                                    Clients: [],
                                    Roles: []
                                };
                            },
                            roles: () => { return roles; },
                            languages: () => { return angular.copy(this.languages); }
                        }
                    });

                    // Update the UI
                    this.modalInstance.result.then((userObj: commonInterfaces.IUser) => {
                        if (user) {
                            angular.copy(userObj, user);
                        } else {
                            userObj.Password = null;
                            this.users.push(userObj);
                        }

                    }, null);
                }
            });

        }

        /**
        * @description Search users.
        */
        search = ($event: JQueryEventObject): void => {
            if ($event) $event.preventDefault();

            if (this.searchCriteria != null && this.searchCriteria.length < 3) {
                this.notificationSvc.errorToastMessage('Search criteria needs to have three or more characters');
                return null;
            } 
            this.getUsersReset();
        }

        /**
         * @description Reset the search filter.
         */
        reset = ($event: JQueryEventObject): void => {
            if ($event) $event.preventDefault();

            this.searchCriteria = null;
            this.search($event);
        }
    }

    angular.module('app').controller('Admin.TenantManagementUsersCtrl', TenantManagementUsersCtrl);
}  