﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import adminInterfaces = ChaiTea.Admin.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminSvc = ChaiTea.Admin.Services;
    import commonSvc = ChaiTea.Common.Services;

    interface CourseTypesScope extends angular.IScope {
        gridOptions: GridOptions;
        gridApi: uiGrid.IGridApi;
        iconClasses: Array<any>;
        showIconPicker;
    }

    interface GridOptions extends uiGrid.IGridOptions {
        data: any[];
    }

    class CourseTypesCtrl {

        basePath: string = '';
        sort: string = 'asc';
        params: any = {
            $orderby: 'EmployeeId asc',
            $skip: 0,
            $top: 25
        };

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            '$http',
            'chaitea.common.services.apibasesvc',
            Common.Services.NotificationSvc.id
        ];
        
        constructor(
            private $scope: CourseTypesScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc){

            this.basePath = sitesettings.basePath;
            $scope.iconClasses = [];
            
            //config grid
            var self = this;

            $scope.showIconPicker = (attribute?: any) => {
                this.$uibModal.open({
                    templateUrl: this.sitesettings.basePath + 'Content/js/Admin/Templates/admin.CourseTypes.icons.tmpl.html',
                    controller: 'Admin.CourseTypesIconModalCtrl as vm',
                    size: 'lg',
                    resolve: {
                        attribute: () => (attribute)
                    }
                }).result.then(returned => {
                    if (returned) {
                        //this.finishAttributeEdit(returned);
                    }
                });
            }

            $scope.gridOptions = {
                data: [],
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                rowHeight: 38,
                enableSorting: true,
                columnDefs: [
                    { name: 'course type id', field: 'CourseTypeId', enableCellEdit: false, visible: false },
                    { name: 'name', field: 'Name', type: 'string' },
                    {
                        name: 'icon', field: 'Icon',
                        enableCellEdit: false,
                        cellTemplate: `<div ng-click=\"grid.appScope.showIconPicker(row)\" class="ui-grid-cell-contents"><span class="fa fa-lg fa-{{ COL_FIELD }}"></span> {{ COL_FIELD }}</div>`,
                        width: 300
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;

                    gridApi.edit.on.afterCellEdit($scope, function (rowEntity:ChaiTea.Services.Interfaces.ICourseType, colDef, newValue, oldValue) {
                        if (newValue == oldValue) return;

                        if (!rowEntity.Name || !rowEntity.Icon) return;

                        //Update
                        if (rowEntity.CourseTypeId) {
                            apiSvc.update(rowEntity.CourseTypeId, rowEntity, 'CourseType').then(() => {
                                self.notificationSvc.successToastMessage(`Course Type "<strong>${rowEntity.Name}</strong>" has been updated successfully.`);
                            });
                        //Create
                        } else {
                            rowEntity.CourseTypeId = 0;
                            apiSvc.save(rowEntity, 'CourseType').then(() => {
                                self.notificationSvc.successToastMessage(`Course Type "<strong>${rowEntity.Name}</strong>" has been created successfully.`);
                            });
                        }
                    });
                }
            };
            $scope.gridOptions.enableCellEditOnFocus = true;
            $scope.gridOptions.enableHorizontalScrollbar = false;
            this.loadGrid($scope, false);

        }

        public createNew = () => {
            var self = this;

            this.$scope.gridOptions.data.push({ CourseTypeId: null, Name: '', Icon: '' });

            //timeout used to fix issue with grid not moving
            this.$timeout(function () {
                self.$scope.gridApi.cellNav.scrollToFocus(self.$scope.gridOptions.data[self.$scope.gridOptions.data.length - 1], self.$scope.gridOptions.columnDefs[1]);
            }, 100);
        }

        private loadGrid = ($scope: any, loadMore: boolean): void => {

            if (loadMore) {
                this.params.$skip += this.params.$top;
            }

            this.apiSvc.get('CourseType').then((res) => {
                if (loadMore) {
                    $scope.gridOptions.data = $scope.gridOptions.data.concat(res);
                } else {
                    $scope.gridOptions.data = res;
                }
            });


            this.$http.get(this.sitesettings.basePath + 'Content/data/icon-classes.json').success((res: string[]) => {
                angular.forEach(res, (val, key) => {
                    $scope.iconClasses.push(val.split('fa-')[1]);
                })
                
            });

        }

    }

    class CourseTypesIconModalCtrl {

        resultObj: adminInterfaces.INodeObject[];
        iconClasses = [];

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'sitesettings',
            'attribute',
            '$http',
            '$log',            
            'chaitea.common.services.apibasesvc',
            Common.Services.NotificationSvc.id
        ];

        constructor(
            private $scope: CourseTypesScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private attribute: any,
            private $http: angular.IHttpService,
            private $log: angular.ILogService,            
            private apiSvc: commonSvc.IApiBaseSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {

            this.resultObj = [];           

        }

        initialize = () => {
            this.$http.get(this.sitesettings.basePath + 'Content/data/icon-classes.json').success((res: string[]) => {
                angular.forEach(res, (val, key) => {
                    this.iconClasses.push(val.split('fa-')[1]);
                })

            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        closeModal = (): void => {
            this.$uibModalInstance.dismiss('close');
        };

        selectIcon = (iconItem: any) => {
            if (this.attribute.entity) {

                this.attribute.entity.Icon = iconItem;

                //Update
                if (this.attribute.entity.CourseTypeId) {
                    this.apiSvc.update(this.attribute.entity.CourseTypeId, this.attribute.entity, 'CourseType').then(() => {                        
                        this.notificationSvc.successToastMessage(`Course Type "<strong>${this.attribute.entity.Name}</strong>" has been updated successfully.`);
                        this.closeModal();
                    });
                //Create
                } else {
                    this.attribute.entity.CourseTypeId = 0;
                    if (this.attribute.entity.Name == '') {
                        this.notificationSvc.warningToastMessage('Please create a course name in order to save this course type.');
                        this.closeModal();
                    } else {

                        this.apiSvc.save(this.attribute.entity, 'CourseType').then(() => {
                            this.notificationSvc.successToastMessage(`Course Type "<strong>${this.attribute.entity.Name}</strong>" has been created successfully.`);
                            this.closeModal();
                        });
                    }
                }
            }            
        }
    }

    angular.module('app').controller('Admin.CourseTypesCtrl', CourseTypesCtrl);
    angular.module('app').controller('Admin.CourseTypesIconModalCtrl', CourseTypesIconModalCtrl);

}