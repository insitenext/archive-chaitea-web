﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    class TenantManagementMainCtrl {
        modalInstance;

        static $inject = ['$scope', '$log', '$state', 'tabs'];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService,
            private $state: angular.ui.IStateService,
            private tabs: Array<any>) {
            $scope.$on("$stateChangeSuccess",() => {
                this.tabs.forEach((tab) => {
                    tab.active = this.active(tab.route);
                });
            });
        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {

        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * ROUTING
         * @description Routing routines.
         */
        navigateTo = (route) => {
            this.$state.go(route);
        }

        active = (route) => {
            return this.$state.is(route);
        };
    }

    angular.module('app').controller('Admin.TenantManagementMainCtrl', TenantManagementMainCtrl);
}