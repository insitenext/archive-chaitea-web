﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    "use strict";

    enum Settings { Client, Site, Program };
    enum State { Create, Update };

    interface IAdminOrgSettings {
        Id?: number;
        Name: string;
        Longitude?: number;
        Latitude?: number;
    }

    export interface ITimeZones {
        Key: number;
        Value: string;
    }
    class ClientCtrl {

        basePath: string = '';

        client: ChaiTea.Common.Interfaces.ILookupListOption;
        site: ChaiTea.Common.Interfaces.ILookupListOption;
        program: ChaiTea.Common.Interfaces.ILookupListOption;

        currentState: State = State.Update;
        buttonText: string = State[State.Update];
        currentSetting: Settings = Settings.Client;
        selectedSetting: IAdminOrgSettings = {
            Id: 0,
            Name: '',
            Longitude: 0,
            Latitude: 0
        };
        latitude: number = 0;
        longitude: number = 0;
        timeZone: ChaiTea.Common.Interfaces.ILookupListOption;
        timezones: Array<ChaiTea.Common.Interfaces.ILookupListOption> = [];
        IsDstEnabled: boolean = true;
        settingsText: string = '';
        hideProgram: boolean = true;
        modalInstance;
        fileForProfile;
        fileForBackground;
        orgSetting: Common.Interfaces.IOrgSetting ;
        profileDone: boolean = false;
        backGroundDone: boolean = false;
        existingBackground: string = '';
        existingProfile: string = '';
     
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.apibasesvc',
            Common.Services.NotificationSvc.id,
            'chaitea.common.services.localdatastoresvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: Common.Services.IAwsSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc) {

            this.basePath = sitesettings.basePath;

            //add watchers
            this.$scope.$watch('$ctrl.client', (value) => {                
                if (!value) return;

                this.site = null;
                this.program = null;
                this.currentSetting = Settings.Client;
                this.getClient(parseInt(this.client.Key));
                this.timeZone = null;
                if (this.client)
                    this.getOrgSetting(parseInt(this.client.Key));
            });

            this.$scope.$watch('$ctrl.site', (value) => {

                if (!value) return;

                if (!this.site.Key) {
                    this.currentSetting = Settings.Client;
                    return;
                }

                this.program = null;
                this.currentSetting = Settings.Site;
                this.getSite(parseInt(this.site.Key));
                if (this.site)
                    this.getOrgSetting(parseInt(this.site.Key));             
            });

            this.$scope.$watch('$ctrl.latitude', (value: number) => {
                this.selectedSetting.Latitude = value;
            });

            this.$scope.$watch('$ctrl.longitude', (value: number) => {
                this.selectedSetting.Longitude = value;
            });

        }

        initialize = () => {
            this.getTimeZones();

        };

        public clearSelections = () => {
            this.client = null;
            this.site = null;
            this.program = null;

            this.selectedSetting.Id = 0;
            this.selectedSetting.Name = '';
            this.selectedSetting.Latitude = 0;
            this.selectedSetting.Longitude = 0;
            this.timeZone = null;
        };

        public save = () => {

            var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Client];

            if (this.currentSetting == Settings.Site) {
                endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Site];
            }

            if (this.currentState == State.Create) {

                this.apiSvc.save(this.selectedSetting, endpoint).then((res) => {

                    toastr.success(Settings[this.currentSetting] + ' settings saved');
                    this.loadSettings(res);

                }, this.handleError);

            } else {

                this.apiSvc.update(this.selectedSetting.Id, this.selectedSetting, endpoint).then((res) => {

                    this.notificationSvc.successToastMessage(Settings[this.currentSetting] + ' settings saved');
                    this.loadSettings(res);

                }, this.handleError);
            }
        };


        private getTimeZones = () => {
            angular.forEach(moment.tz.names(), (data, index) => {
                this.timezones.push({
                         Key: data,
                         Value: data + ' - (UTC ' + (moment.tz(data).utcOffset() / 60).toFixed(2) + ')'
                });
            });
        };

        public saveImages = () => {
            this.profileDone = false;
            this.backGroundDone = false;
            if ((this.fileForProfile && this.fileForProfile[0]) || (this.fileForBackground && this.fileForBackground[0])) {
                var profileFile = this.fileForProfile ? this.fileForProfile[0] : null;
                var backgroundFile = this.fileForBackground ? this.fileForBackground[0] : null;

                var profileFileType: string = profileFile ? profileFile.type : null;
                var backgroundFileType: string = backgroundFile ? backgroundFile.type : null;
                if ((profileFileType && !(_.startsWith(profileFileType, 'image'))) ||
                    (backgroundFileType && !(_.startsWith(backgroundFileType, 'image')))) {
                    bootbox.alert("Select an image file!");
                }
                else {
                    this.credentialSvc.getAwsCredentials().then((creds: Common.Interfaces.ISecurity) => {
                        var profileAttachmentId: number = 0;
                        var backgroundAttachmentId: number = 0;

                        if (!creds.AccessKey || !creds.AccessSecret) return this.handleFailure('not authorized');
                        //add profile pic attachment
                        if (profileFile) {
                            this.awsSvc.s3Upload(profileFile, creds, Common.Interfaces.S3Folders.image, Common.Interfaces.S3ACL.publicRead, false)
                                .then((dataProfile: any) => {
                                    var attachment = {
                                        AttachmentId: 0,
                                        AttachmentType: Common.Interfaces.ATTACHMENT_TYPE[Common.Interfaces.ATTACHMENT_TYPE.Image],
                                        AttachmentTypeId: Common.Interfaces.ATTACHMENT_TYPE.Image,
                                        UniqueId: dataProfile.Name,
                                        Name: profileFile.name,
                                        Description: 'Profile Image'
                                    };

                                    this.apiSvc.save(attachment, "Attachment").then((result) => {
                                        this.orgSetting.ProfileImgAttachmentId = result.AttachmentId;
                                        this.profileDone = true;
                                        this.UpdateAll();
                                    }, this.handleFailure);

                                }, this.handleFailure);
                        }
                        else {
                            this.profileDone = true;
                        }
                        if (backgroundFile) {
                            this.awsSvc.s3Upload(backgroundFile, creds, Common.Interfaces.S3Folders.image, Common.Interfaces.S3ACL.publicRead, false)
                                .then((dataBackground: any) => {
                                    var attachment = {
                                        AttachmentId: 0,
                                        AttachmentType: Common.Interfaces.ATTACHMENT_TYPE[Common.Interfaces.ATTACHMENT_TYPE.Image],
                                        AttachmentTypeId: Common.Interfaces.ATTACHMENT_TYPE.Image,
                                        UniqueId: dataBackground.Name,
                                        Name: backgroundFile.name,
                                        Description: 'Background Image'
                                    };

                                    this.apiSvc.save(attachment, "Attachment").then((result) => {
                                        this.orgSetting.BackgroundImgAttachmentId = result.AttachmentId;
                                        this.backGroundDone = true;
                                        this.UpdateAll();
                                    }, this.handleFailure);
                                }, this.handleFailure);
                        }
                        else {
                            this.backGroundDone = true;
                        }
                    }, this.handleError);
                }
            }
        };

        public saveTimeZone = () => {

            if (this.orgSetting)
            {
                this.orgSetting.TimeZone = this.timeZone.Key;
                this.orgSetting.UtcOffsetInMinutes = moment.tz(this.timeZone.Key).utcOffset();
                this.orgSetting.IsDstEnabled = this.IsDstEnabled;
                this.orgSetting.Id = this.site ? parseInt(this.site.Key) : parseInt(this.client.Key) ;
             
                this.apiSvc.update(this.orgSetting.Id, this.orgSetting, "Org").then(this.successMessage, this.handleFailure);
            }
            else {
             
                var timezoneSetting = {
                    Id: this.site ? parseInt(this.site.Key) : parseInt(this.client.Key),
                    UtcOffsetInMinutes: moment.tz(this.timeZone.Key).utcOffset(),
                    TimeZone: this.timeZone.Key,
                    IsDstEnabled: this.IsDstEnabled,
                };
                this.apiSvc.save(timezoneSetting, "Org").then((result) => {
                    this.orgSetting = {
                        BackgroundImgAttachmentId: result.BackgroundImgAttachmentId,
                        Id: result.Id,
                        IsDstEnabled: result.IsDstEnabled,
                        ProfileImgAttachmentId: result.ProfileImgAttachmentId,
                        TimeZone: result.Timezone,
                        Name: result.Name,
                        UtcOffsetInMinutes: result.UtcOffsetInMinutes
                    };
                    this.successMessage();          
                }, this.handleFailure);

            }
        };

        private UpdateAll = () => {
            if (this.profileDone && this.backGroundDone) {
                this.apiSvc.update(this.orgSetting.Id, this.orgSetting, "Org").then(this.successMessage, this.handleFailure);
            }
        };

        private getClient = (clientId: number) => {

            var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Client];
            this.apiSvc.getById(clientId, endpoint).then((org: ChaiTea.Common.Interfaces.IOrgSetting) => {
                this.loadSettings(org);

            });

        };
        private successMessage = () => {
            this.notificationSvc.successToastMessage(`Your item has been saved.`);
        };
        private getOrgSetting = (clientId: number) => {
            this.existingProfile = '';
            this.existingBackground = '';
            this.orgSetting = null;
            var isSite = false;
            var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Org];
            if (this.site)
                isSite = true;
            var params = {
                clientId: clientId,
                isSite: isSite
            };
            this.apiSvc.query(params, "Org/Clients/:clientId/:isSite", false, true).then((resultantOrgSetting: any) => {
                if (resultantOrgSetting.length != 0) {
                    //   this.orgSetting = resultantOrgSetting[0];
                    this.orgSetting = {
                        BackgroundImgAttachmentId: resultantOrgSetting[0].BackgroundImgAttachmentId,
                        Id: resultantOrgSetting[0].Id,
                        IsDstEnabled: resultantOrgSetting[0].IsDstEnabled,
                        ProfileImgAttachmentId: resultantOrgSetting[0].ProfileImgAttachmentId,
                        TimeZone: resultantOrgSetting[0].Timezone,
                        Name: resultantOrgSetting[0].Name,
                        UtcOffsetInMinutes: resultantOrgSetting[0].UtcOffsetInMinutes
                    };
                    // this.orgSetting.TimeZone = resultantOrgSetting[0].Timezone;
                    if (this.orgSetting.ProfileImgAttachmentId) {
                        this.apiSvc.getById(this.orgSetting.ProfileImgAttachmentId, 'Attachment').then((result) => {
                            this.existingProfile = this.awsSvc.getUrlByAttachment(result.UniqueId, Common.Interfaces.ATTACHMENT_TYPE[Common.Interfaces.ATTACHMENT_TYPE.Image]);
                        }, this.handleFailure);
                    }
                    if (this.orgSetting.BackgroundImgAttachmentId) {
                        this.apiSvc.getById(this.orgSetting.BackgroundImgAttachmentId, 'Attachment').then((result) => {
                            this.existingBackground = this.awsSvc.getUrlByAttachment(result.UniqueId, Common.Interfaces.ATTACHMENT_TYPE[Common.Interfaces.ATTACHMENT_TYPE.Image]);
                        }, this.handleFailure);
                    }
                    if (this.orgSetting.TimeZone) {
                        //  this.timeZone.Value = this.orgSetting.TimeZone;
                        this.timeZone = _.find(this.timezones, { Value: this.orgSetting.TimeZone + ' - (UTC ' + (moment.tz(this.orgSetting.TimeZone).utcOffset() / 60).toFixed(2) + ')' });
                        this.IsDstEnabled = this.orgSetting.IsDstEnabled;
                    }
                }
                else
                    this.timeZone = null;
            }, this.handleFailure);
        };

        private getSite = (siteId: number) => {

            var endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Site];
            this.apiSvc.getById(siteId, endpoint).then((org: ChaiTea.Common.Interfaces.IOrg) => {

                this.loadSettings(org);

            });

        };

        private loadSettings = (org: ChaiTea.Common.Interfaces.IOrg) => {

            if (org) {
                this.selectedSetting.Id = org.Id;
                this.selectedSetting.Name = org.Name;
                this.selectedSetting.Longitude = org.Longitude;
                this.selectedSetting.Latitude = org.Latitude;
                if (org.Latitude && org.Longitude) {
                    this.latitude = org.Latitude;
                    this.longitude = org.Longitude;
                }
            }

            this.settingsText = "";

            if (this.client && !this.site && !this.program) {
                this.settingsText = "Applying settings to " + this.client.Value;
            }

            if (this.client && this.site && !this.program) {
                this.settingsText = "Applying settings to " + this.site.Value;
            }

            if (this.client && this.site && this.program) {
                this.settingsText = "Applying settings to " + this.program.Value;
            }

            this.buttonText = State[State.Update];
            this.currentState = State.Update;

        };

        private handleError = (error: any) => {
            this.notificationSvc.errorToastMessage('Please contact support@sbmcorp.com');
        };
        public handleFailure = (error) => {
            this.$log.error(error);
            this.notificationSvc.errorToastMessage('Please contact support@sbmcorp.com');
        };

    }

    angular.module('app').controller('Admin.ClientCtrl', ClientCtrl);
}