﻿module ChaiTea.Admin.Controllers {
    'use strict';
    
    import commonSvc = ChaiTea.Common.Services;
    import adminInterfaces = ChaiTea.Admin.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;

    interface IAreaClassification {
        AreaClassificationId: number;
        Name: string;
        IsActive: boolean;
    }

    class TenantManagementAreaClassificationCtrl {
        
        private selectedObj: IAreaClassification = {
            AreaClassificationId: 0,
            Name: '',
            IsActive: true
        };        
        private basePath: string = ''; 
        private modalInstance: any;
        private areaTitle: string = 'Area Classification';
        private itemTitle: string = 'Inspection Item';

        private areaClassificationObjects = [
            {
                Name: this.areaTitle,
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: this.itemTitle,
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            }
        ];        

        static $inject = [
            '$scope',
            '$q',            
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'sitesettings',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private sitesettings: ISiteSettings,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private lookupListSvc: commonSvc.ILookupListSvc,
            private apiSvc: commonSvc.IApiBaseSvc,
            private notificationSvc: commonSvc.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;

            window.location.href = sitesettings.greentea[sitesettings.environment] + 'TenantManagement/AreaTypes?token=' + sitesettings.access_token;

            // Use the following if debugging locally...
            //
            // window.location.href = 'http://localhost:4300/TenantManagement/AreaTypes?token=' + this.getCookieVal(window.document.cookie, 'r2sbminsite');
        }


        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            this.setAreaClassifications();            
        }

        /**
        * @description Set the areaClassifications collection.
        */
        setAreaClassifications = () => {
            var areaClassificationNode = this.getNodeByName(this.areaTitle);
            areaClassificationNode.SelectedValue = 0;                                    
             
            this.lookupListSvc.getAreaClassifications().then((data) => {
                areaClassificationNode.Collection = data.Options;                                               
            }, this.onFailure);
        }

        /**
         * @description Handle when a selection changes.
         */
        onOptionChange = ($event: JQueryEventObject, $index: number, obj: adminInterfaces.INodeObject) => {
            // Clear descendant options
            this.clearAllDescendantsByIndex($index);

            // Get direct descendant options
            var indexer = $index + 1;
            if (indexer >= this.areaClassificationObjects.length) return;
            var childName = this.areaClassificationObjects[indexer].Name;            

            if (obj.SelectedValue) {
                this.loadInspectionList(obj.SelectedValue);                                
            }
        }

        /**
         * @description Loads a collection of Inspection Items based on the areaClassificationId
         */
        private loadInspectionList = (areaClassificationId: number) => {

            this.apiSvc.getLookupList('LookupList/AreaClassificationInspectionItems/' + areaClassificationId ).then((res) => {                
                var inspectionNode = this.getNodeByName('Inspection Item');
                inspectionNode.Collection = res.Options;
            }, this.onFailure);
        }

        /**
         * @description Clears all descendants collection and selected value.
         */
        clearAllDescendantsByIndex = (index: number) => {
            _.each(this.areaClassificationObjects, function (obj, i) {
                if (i > index) {
                    obj.Collection = [];
                    obj.SelectedValue = null;
                }
            });
        }
               
        /**
         * @description Gets a node object by name.
         */
        getNodeByName = (name: string) => {
            return _.find(this.areaClassificationObjects, { Name: name });
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage('An error has occurred');
        }

        /**
         * @description Display a modal to edit a node item.
         */
        editEntry = ($event: JQueryEventObject, obj: any) => {
            if ($event) $event.preventDefault();

            if (obj && !obj.SelectedValue) {
                this.notificationSvc.errorToastMessage(`Please select a ${obj.Name}`);
                return;
            }

            var nodeName = obj.Name;
            var nodeCollection = _.find(this.areaClassificationObjects, { Name: obj.Name }).Collection;
            var collectionObject = _.find(nodeCollection, { Key: obj.SelectedValue });
            var areaEndpoint = '/AreaClassification';
            var itemEndpoint = '/AreaClassificationInspectionItem';

            if (!collectionObject) {
                this.notificationSvc.errorToastMessage(`Invalid ${obj.Name} selection`);
                return;
            }
            
            this.addEditEntry(
                {
                    templateUrl: 'addToKVPEntityModal.tmpl.html',
                    controller: 'Admin.EditEntryForAreaClassificationModalCtrl as vm',
                    resolve: {
                        obj: () => { return obj },
                        title: () => { return obj ? `Editing ${obj.Name}` : 'New'; },
                        entity: () => { return angular.copy(collectionObject); }
                    }
                }, (d: any) => {                    
                    this.selectedObj.AreaClassificationId = collectionObject.Key;
                    this.selectedObj.Name = d;
                    collectionObject.Value = d;

                    if (nodeName === this.areaTitle) {
                        // update AreaClassification                       
                        this.apiSvc.update(collectionObject.Key, this.selectedObj, areaEndpoint).then((result) => {
                        }, this.onFailure); 
                    }
                    else {
                        // update InspectionItem  
                        this.apiSvc.update(collectionObject.Key, this.selectedObj, itemEndpoint).then((result) => {
                        }, this.onFailure); 
                    }                    

                }, null);
        }

        /**
         * @description Display a modal to add to current node based on parent selection.
         */
        addEntry = ($event: JQueryEventObject, $index: number, obj) => {
            if ($event) $event.preventDefault();

            // Get the parent node
            var indexer = $index - 1;
            var parent = null;
            if (indexer != -1) {
                parent = this.areaClassificationObjects[indexer];
            }                        
            var nodeName = obj.Name;
            var nodeCollection = _.find(this.areaClassificationObjects, { Name: obj.Name }).Collection;
            var collectionObject = _.find(nodeCollection, { Key: obj.SelectedValue });
            var areaEndpoint = '/AreaClassification';
            var itemEndpoint = '/AreaClassificationInspectionItem';
            
            // Do not allow if parent is present and no value is selected
            if (parent && !parent.SelectedValue) {
                this.notificationSvc.errorToastMessage(`Please select a ${parent.Name}`);
                return;
            }

            this.addEditEntry(
                {
                    templateUrl: 'addToKVPEntityModal.tmpl.html',
                    controller: 'Admin.EditEntryForAreaClassificationModalCtrl as vm',
                    resolve: {
                        obj: () => { return obj },
                        title: () => { return obj ? `Adding ${obj.Name}` : 'New'; },
                        entity: () => { return obj }
                    }
                }, (d: any) => {
                    this.selectedObj.AreaClassificationId = 0;
                    this.selectedObj.Name = d;

                    if (nodeName === this.areaTitle) {
                        // insert AreaClassification                                               
                        this.apiSvc.save(this.selectedObj, areaEndpoint).then((res) => {                       
                            var areaClassificationNode = this.getNodeByName(this.areaTitle);
                            areaClassificationNode.Collection.push({ Key: res.AreaClassificationId, Value: res.Name });
                            areaClassificationNode.SelectedValue = res.AreaClassificationId;                             
                            this.loadInspectionList(res.AreaClassificationId);      
                        }, this.onFailure);
                    }
                    else {
                        this.selectedObj.AreaClassificationId = parent.SelectedValue;                        
                        // insert AreaClassificationInspectionItem 
                        this.apiSvc.save(this.selectedObj, itemEndpoint).then((res) => {
                            var inspectionNode = this.getNodeByName(this.itemTitle);
                            inspectionNode.Collection.push({ Key: res.AreaClassificationInspectionItemId, Value: res.Name });
                            inspectionNode.SelectedValue = res.AreaClassificationInspectionItemId;
                        }, this.onFailure);
                    }                    
                }, null);
        }

        /**
         * @description Add/edit a node.
         * @param {Object}      modalSettings
         * @param {Function}    callback
         * @param {Function}    rejectCallback 
         */
        addEditEntry = (modalSettings: angular.ui.bootstrap.IModalSettings, callback, rejectCallback?) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: modalSettings.templateUrl,
                controller: modalSettings.controller,
                size: 'md',
                resolve: modalSettings.resolve
            });

            this.modalInstance.result.then(callback, rejectCallback);
        }

    }

    angular.module('app')
        .controller('Admin.TenantManagementAreaClassificationCtrl', TenantManagementAreaClassificationCtrl);
}