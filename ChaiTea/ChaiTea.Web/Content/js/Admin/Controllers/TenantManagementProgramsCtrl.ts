﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    class TenantManagementProgramsCtrl {
        tenantNodeObjects = [
            {
                Name: 'Client',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Site',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Building',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            }
        ];

        tenantNodeObjectLookups = [
            {
                Name: 'Program',
                Id: 'ProgramId',
                Collection: []
            }
        ];

        activeBuildingProgramAssignment = {}

        basePath: string;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc',
            'clients',
            'programs',
            'chaitea.common.services.notificationsvc',
            '$translate'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private clients,
            private programs,
            private notificationSvc: commonSvc.INotificationSvc,
            private $translate: angular.translate.ITranslateService) {

            this.basePath = sitesettings.basePath;
        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            this.setInitialLookups();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            if (response.status === 400) {
                this.$translate(response.data.Message).then((translation) => {
                    this.notificationSvc.errorToastMessage(translation);
                });
            }
            else {
                this.notificationSvc.errorToastMessage();
            }
            this.$log.error(response);
        }


        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void => {            
            this.$translate('PROGRAM_ASSIGNMENT_SUCCESS').then((translation) => {
                this.notificationSvc.successToastMessage(translation);
            });
        }

        /**
        * LOOKUPS
        * @description Set initial lookup collection.
        */
        setInitialLookups = () => {
            var clientNode = this.getNodeByName('Client', this.tenantNodeObjects);
            clientNode.SelectedValue = 0;
            clientNode.Collection = this.clients;

            var programNode = this.getNodeByName('Program', this.tenantNodeObjectLookups);
            programNode.Collection = this.programs;
        }

        /**
         * @description Handle when a selection changes.
         */
        onOptionChange = ($event: JQueryEventObject, $index: number, obj: adminInterfaces.INodeObject) => {
            // Clear descendant options
            this.clearAllDescendantsByIndex($index);


            // Get direct descendant options
            var indexer = $index + 1;

            // Load up the assignments for building
            if (indexer >= this.tenantNodeObjects.length && obj.Name === 'Building') {
                this.setAssignmentForBuilding(obj.SelectedValue);
            } else {
                var childName = this.tenantNodeObjects[indexer].Name;

                if (obj.SelectedValue) this.populateCollectionByName(childName, obj);
            }
        }

        /**
         * @description Clears all descendants collection and selected value.
         */
        clearAllDescendantsByIndex = (index: number) => {
            _.each(this.tenantNodeObjects, function (obj, i) {
                if (i > index) {
                    obj.Collection = [];
                    obj.SelectedValue = null;
                }
            });
        }

        /**
         * @description Gets a node object by name.
         */
        getNodeByName = (name: string, node: Array<any>) => {
            return _.find(node, { Name: name });
        }

        /**
         * @description Gets the service function definition for GET by name.
         */
        getObjectsFuncByName = (name: string) => {
            var queries = {
                Client: this.tenantManagementSvc.getClientsByOData,
                Site: this.tenantManagementSvc.getSitesByOData,
                Building: this.tenantManagementSvc.getBuildingsByOData
            }

            return queries[name];
        }

        /**
         * @description Gets a collection by name filtered by parentId.
         */
        populateCollectionByName = (name: string, parent: adminInterfaces.INodeObject) => {
            this.getObjectsFuncByName(name)(
                {
                    $filter: `${parent.Id} eq ${parent.SelectedValue}`
                })
                .then((response) => {

                    this.getNodeByName(name, this.tenantNodeObjects).Collection = response;
                }, this.onFailure);
        }

        /**
         * @description Set active site-program assignment.
         */
        setAssignmentForBuilding = (buildingId: number) => {
            this.activeBuildingProgramAssignment = {};
            this.tenantManagementSvc.getBuildingProgramAssignments(buildingId).then((response) => {
                this.activeBuildingProgramAssignment = _.pluck(response, 'Id');
            });
        }

        /**
         * @description Save programs-to-site assignments.
         */
        saveAssignments = ($event: JQueryEventObject, buildingId: number): void => {
            if ($event) $event.preventDefault();

            var programIds = <Array<number>>this.activeBuildingProgramAssignment;
            this.tenantManagementSvc.updateBuildingProgramAssignment(buildingId, programIds).then(this.onSuccess, this.onFailure);
        }
    }

    angular.module('app').controller('Admin.TenantManagementProgramsCtrl', TenantManagementProgramsCtrl);
}