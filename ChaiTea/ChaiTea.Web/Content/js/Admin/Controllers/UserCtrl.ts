﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    class UserCtrl {

        basePath: string = '';
        sort: string = 'asc';
        params: any = {
            $orderby: 'EmployeeId asc',
            $skip: 0,
            $top: 25
        };

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.apibasesvc'
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;

            //config grid
            var self = this;
            $scope['gridOptions'] = <uiGrid.IGridOptions>{
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                enableSorting: true,
                useExternalSorting: false,
                infiniteScrollDown: true,
                infiniteScrollRowsFromEnd: 10,
                columnDefs: [
                    { name: 'employee id', field: 'EmployeeId', enableCellEdit: false },
                    { name: 'user id', field: 'UserId', enableCellEdit: false },
                    { name: 'first', field: 'FirstName', enableCellEdit: false},
                    { name: 'last', field: 'LastName', enableCellEdit: false },
                    { name: 'site id', field: 'SiteId', enableCellEdit: false },
                    { name: 'site name', field: 'SiteName'},
                    { name: 'test templ', field: 'yourMom', cellTemplate: `<a href="javascript:void(0);" class="btn btn-xs btn-primary margin-horizontal-20"><span class="fa fa-pencil"/> edit</a>` }
                ],
                onRegisterApi: function (gridApi) {
                    gridApi.infiniteScroll.on.needLoadMoreData($scope, function () {
                        self.loadGrid($scope, true);
                    });
                    gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                        console.log('edited row id:' + JSON.stringify(rowEntity) + ' Column:' + colDef + ' newValue:' + newValue + ' oldValue:' + oldValue);
                        $scope.$apply();
                    });
                    $scope['gridApi'] = gridApi;
                }
            };

            this.loadGrid($scope, false);

            

        }


        initialize = () => {

            console.log('load user admin');

        }

        private loadGrid = ($scope: any, loadMore: boolean): void => {

            if (loadMore) {
                this.params.$skip += this.params.$top;
            }

            this.apiSvc.getByOdata(this.params, 'Employee').then((res) => {
                if (loadMore) {
                    $scope['gridOptions'].data = $scope['gridOptions'].data.concat(res);
                } else {
                    $scope['gridOptions'].data = res;
                }
            });
            
            //$scope['gridOptions'].data =  [{
            //    "first-name": "Cox",
            //    "friends": ["friend0"],
            //    "address": { street: "301 Dove Ave", city: "Laurel", zip: "39565" },
            //    "getZip": function () {
            //        return this.address.zip;
            //    }
            //}];

        }

    }

    angular.module('app').controller('Admin.UserCtrl', UserCtrl);
}