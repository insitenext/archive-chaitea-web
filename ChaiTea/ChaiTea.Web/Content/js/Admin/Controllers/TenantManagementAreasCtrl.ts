﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    class TenantManagementAreasCtrl {
        basePath: string;

        tenantNodeObjects = [
            {
                Name: 'Client',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Site',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },

            {
                Name: 'Building',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Floor',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            }
        ];

        tenantNodeObjectLookups = [
            {
                Name: 'Area',
                Id: 'AreaId',
                Collection: []
            }
        ];

        activeFloorAreaAssignment = {}

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc',
            'clients',
            'areas'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private clients,
            private areas) {

            this.basePath = sitesettings.basePath;
        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            this.setInitialLookups();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.alertSvc.alertWithCallback('Assignment was saved successfully!', null);
        }

        /**
        * LOOKUPS
        * @description Set initial lookup collection.
        */
        setInitialLookups = () => {
            var clientNode = this.getNodeByName('Client', this.tenantNodeObjects);
            clientNode.SelectedValue = 0;
            clientNode.Collection = this.clients;

            var areaNode = this.getNodeByName('Area', this.tenantNodeObjectLookups);
            areaNode.Collection = this.areas;
        }

        /**
         * @description Handle when a selection changes.
         */
        onOptionChange = ($event: JQueryEventObject, $index: number, obj: adminInterfaces.INodeObject) => {          
            // Clear descendant options
            this.clearAllDescendantsByIndex($index);
        
            // Get direct descendant options
            var indexer = $index + 1;

            // Load up the assignments for floor
            if (indexer >= this.tenantNodeObjects.length && obj.Name === 'Floor') {
                this.setAssignmentForFloor(obj.SelectedValue);
            } else {
                var childName = this.tenantNodeObjects[indexer].Name;

                if (obj.SelectedValue) this.populateCollectionByName(childName, obj);
            }
        }

        /**
         * @description Clears all descendants collection and selected value.
         */
        clearAllDescendantsByIndex = (index: number) => {
            _.each(this.tenantNodeObjects, function (obj, i) {
                if (i > index) {
                    obj.Collection = [];
                    obj.SelectedValue = null;
                }
            });
        }

        /**
         * @description Gets a node object by name.
         */
        getNodeByName = (name: string, node: Array<any>) => {
            return _.find(node, { Name: name });
        }

        /**
         * @description Gets the service function definition for GET by name.
         */
        getObjectsFuncByName = (name: string) => {
            var queries = {
                Client: this.tenantManagementSvc.getClientsByOData,
                Site: this.tenantManagementSvc.getSitesByOData,
                Building: this.tenantManagementSvc.getBuildingsByOData,
                Floor: this.tenantManagementSvc.getFloorsByOData
            }

            return queries[name];
        }

        /**
         * @description Gets a collection by name filtered by parentId.
         */
        populateCollectionByName = (name: string, parent: adminInterfaces.INodeObject) => {
            this.getObjectsFuncByName(name)(
                {
                    $filter: `${parent.Id} eq ${parent.SelectedValue}`
                })
                .then((response) => {

                this.getNodeByName(name, this.tenantNodeObjects).Collection = response;
            }, this.onFailure);
        }

        /**
         * @description Set active floor-area assignment.
         */
        setAssignmentForFloor = (floorId: number) => {
            this.activeFloorAreaAssignment = {};
            this.tenantManagementSvc.getFloorAreaAssignments(floorId).then((response) => {
                this.activeFloorAreaAssignment = _.pluck(response, 'Id');
            });
        }

        /**
         * @description Save areas-to-floor assignment.
         */
        saveAssignments = ($event: JQueryEventObject, floorId: number): void=> {
            if ($event) $event.preventDefault();

            var areaIds = <Array<number>>this.activeFloorAreaAssignment;
            this.tenantManagementSvc.updateFloorAreaAssignment(floorId, areaIds).then(this.onSuccess, this.onFailure);
        }
    }

    angular.module('app').controller('Admin.TenantManagementAreasCtrl', TenantManagementAreasCtrl);
} 