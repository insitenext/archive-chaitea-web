﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    class TenantManagementNodeCtrl {
        modalInstance;
        testModal: string = 'addToEntitySiteModal.tmpl.html';
        modalEditController = {
            'Client': 'Admin.EditEntryForEntityModalCtrl as vm',
            'Site': 'Admin.EditEntryForSiteModalCtrl as vm',
            'Building': 'Admin.EditEntryForEntityModalCtrl as vm',
            'Floor': 'Admin.EditEntryForEntityModalCtrl as vm'
        }
        modalEditTemplate = {
            'Client': 'addToEntityModal.tmpl.html',
            'Site': 'addToEntitySiteModal.tmpl.html',
            'Building': 'addToEntityModal.tmpl.html',
            'Floor': 'addToEntityModal.tmpl.html'
        }
        modalAddController = {
            'Client': 'Admin.AddToEntityModalCtrl as vm',
            'Site': 'Admin.AddToEntitySiteModalCtrl as vm',
            'Building': 'Admin.AddToEntityModalCtrl as vm',
            'Floor': 'Admin.AddToEntityModalCtrl as vm'
        }
        
        tenantNodeObjects = [
            {
                Name: 'Client',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Site',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },

            {
                Name: 'Building',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            },
            {
                Name: 'Floor',
                Id: 'ParentId',
                SelectedValue: null,
                Collection: []
            }
        ];

        scoringProfiles = [];

        basePath: string;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.notificationsvc',
            'clients',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private clients,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;
        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            this.setClients();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
        * @description Set the clients collection.
        */
        setClients = () => {
            var clientNode = this.getNodeByName('Client');
            clientNode.SelectedValue = 0;
            clientNode.Collection = this.clients;
        }

        /**
         * @description Handle when a selection changes.
         */
        onOptionChange = ($event: JQueryEventObject, $index: number, obj: adminInterfaces.INodeObject) => {
            // Clear descendant options
            this.clearAllDescendantsByIndex($index);

            // Get direct descendant options
            var indexer = $index + 1;
            if (indexer >= this.tenantNodeObjects.length) return;
            var childName = this.tenantNodeObjects[indexer].Name;

            if (obj.SelectedValue) this.populateCollectionByName(childName, obj);

        }

        /**
         * @description Clears all descendants collection and selected value.
         */
        clearAllDescendantsByIndex = (index: number) => {
            _.each(this.tenantNodeObjects, function (obj, i) {
                if (i > index) {
                    obj.Collection = [];
                    obj.SelectedValue = null;
                }
            });
        }

        /**
         * @description Gets a node object by name.
         */
        getNodeByName = (name: string) => {
            return _.find(this.tenantNodeObjects, { Name: name });
        }

        /**
         * @description Gets the service function definition for GET by name.
         */
        getObjectsFuncByName = (name: string) => {
            var queries = {
                Client: this.tenantManagementSvc.getClientsByOData,
                Site: this.tenantManagementSvc.getSitesByOData,
                Building: this.tenantManagementSvc.getBuildingsByOData,
                Floor: this.tenantManagementSvc.getFloorsByOData
            }

            return queries[name];
        }

        /**
         * @description Gets the service function definition for SAVE by name.
         */
        saveObjectFuncByName = (name: string) => {
            var queries = {
                Client: this.tenantManagementSvc.saveClient,
                Site: this.tenantManagementSvc.saveSite,
                Building: this.tenantManagementSvc.saveBuilding,
                Floor: this.tenantManagementSvc.saveFloor
            }

            return queries[name];
        }     

        /**
         * @description Gets the service function definition for PUT by name.
         */
        updateObjectFuncByName = (name: string) => {

            var queries = {
                Client: this.tenantManagementSvc.updateClient,
                Site: this.tenantManagementSvc.updateSite,
                Building: this.tenantManagementSvc.updateBuilding,
                Floor: this.tenantManagementSvc.updateFloor
            }

            return queries[name];
        }
        
        /**
         * @description Gets a collection by name filtered by parentId.
         */
        populateCollectionByName = (name: string, parent: adminInterfaces.INodeObject) => {
            this.getObjectsFuncByName(name)(
                {
                    $filter: `${parent.Id} eq ${parent.SelectedValue}`
                })
                .then((response) => {

                this.getNodeByName(name).Collection = response;

            }, this.onFailure);
        }

        /**
         * @description Collection mapper to include count in the name.
         */
        nodeMapper = (obj) => {
            return { Id: obj['Id'], Name: obj['ChildCount'] ? `${obj['Name']} (${obj['ChildCount']})` : obj['Name'] };
        }

        /**
         * @description Display a modal to edit a node name.
         */
        editEntry = ($event: JQueryEventObject, obj: any) => {
            if ($event) $event.preventDefault();

            if (obj && !obj.SelectedValue) {
                this.notificationSvc.errorToastMessage(`Please select a ${obj.Name}`);                
                return;
            }

            var nodeCollection = _.find(this.tenantNodeObjects, { Name: obj.Name }).Collection;
            var collectionObject = _.find(nodeCollection, { Id: obj.SelectedValue });

            if (!collectionObject) {
                this.notificationSvc.errorToastMessage(`Invalid ${obj.Name} selection`);                
                return;
            }

            this.addEditEntry(
                {
                    templateUrl: <string>this.modalEditTemplate[obj.Name],
                    controller: <string>this.modalEditController[obj.Name],
                    resolve: {
                        obj: ()=>{return obj},
                        title: () => { return obj ? `Editing ${obj.Name}` : 'New'; },
                        entity: () => { return angular.copy(collectionObject); },
                        entityUpdateFunc: () => { return this.updateObjectFuncByName(obj.Name) },
                        entityAddFunc: () => { return this.saveObjectFuncByName(obj.Name); }
                    }
                },(d: adminInterfaces.INodeObject) => {
                    collectionObject.Name = d.Name;
                    
                    
                }, null);
        }

        /**
         * @description Display a modal to add to current node based on parent selection.
         */
        addEntry = ($event: JQueryEventObject, $index: number, obj) => {
            if ($event) $event.preventDefault();

            // Get the parent node
            var indexer = $index - 1;
            var parent = null;
            if (indexer != -1) {
                parent = this.tenantNodeObjects[indexer];
            }

            // Do not allow if parent is present and no value is selected
            if (parent && !parent.SelectedValue) {
                this.notificationSvc.errorToastMessage(`Please select a ${parent.Name}`);                
                return;
            }

            
            this.addEditEntry(
                {
                    templateUrl: <string>this.modalEditTemplate[obj.Name],
                    controller: <string>this.modalAddController[obj.Name],
                    resolve: {
                        obj: () => { return obj },
                        entity: () => { return { Name: ''}; },
                        parent: () => { return parent; },
                        entityAddFunc: () => { return this.saveObjectFuncByName(obj.Name) }
                    }
                },(d: adminInterfaces.INodeObject[]) => {
                    if (parent) {
                        // Increment the child collection count for parent node
                        var parentCollection = _.find(parent.Collection, { Id: parent.SelectedValue });
                        parentCollection['ChildCount'] = parseInt(parentCollection['ChildCount']) + d.length;
                    }

                    // Add new value to the entity collection
                    for (var i = 0; i < d.length; i++) {
                        obj.Collection.push(d[i]);
                    }
                }, null);
        }

        /**
         * @description Add/edit a node.
         * @param {Object}      modalSettings
         * @param {Function}    callback
         * @param {Function}    rejectCallback 
         */
        addEditEntry = (modalSettings: angular.ui.bootstrap.IModalSettings, callback, rejectCallback?) => {
            this.modalInstance = this.$uibModal.open({
                templateUrl: modalSettings.templateUrl,
                controller: modalSettings.controller,
                size: 'md',
                resolve: modalSettings.resolve
            });

            this.modalInstance.result.then(callback, rejectCallback);
        }

        deleteEntry = ($event: JQueryEventObject, obj: any) => {
            if (obj && !obj.SelectedValue) {
                this.notificationSvc.errorToastMessage(`Please select a ${obj.Name}`);                
                return;
            }

            var nodeCollection = _.find(this.tenantNodeObjects, { Name: obj.Name }).Collection;
            var collectionObject = _.find(nodeCollection, { Id: obj.SelectedValue });

            if (!collectionObject) {
                this.notificationSvc.errorToastMessage(`Invalid ${obj.Name} selection`);                
                return;
            }
          
            var objEntry = _.find(obj.Collection, { Id: obj.SelectedValue });
            
            this.alertSvc.confirmWithCallback('Are you sure you want to delete ' + obj.Name + ' ' + objEntry['Name'] + '?', (result) => {
                if (result) { 
                    this.apiSvc.delete(obj.SelectedValue, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[obj.Name]]).catch(this.onFailure);                                                
                    // remove it from the collection
                    for (var i = 0; i < obj.Collection.length; i++) {
                        if (obj.Collection[i].Id === obj.SelectedValue) {
                            obj.Collection.splice(i, 1);
                            break;
                        }
                    }
                }
            });
        }
    }


    angular.module('app').controller('Admin.TenantManagementNodeCtrl', TenantManagementNodeCtrl);
}