﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import adminInterfaces = ChaiTea.Admin.Interfaces;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminSvc = ChaiTea.Admin.Services;
    import commonSvc = ChaiTea.Common.Services;

    interface IClientReportSettings extends commonInterfaces.IReportSettings {
        ClientId: string;
    }

    var _client = {
        reportSettings: [
            {
                name: "ComplaintControlLimit",
                value: null,
                isRequired: true,
                inputType: 'number'
            }, {
                name: "ScoringProfileGoal",
                value: null,
                isRequired: true,
                inputType: 'number'
            },
            {
                name: "SurveyControlLimit",
                value: null,
                isRequired: true,
                inputType: 'number'
            },
            {
                name: "ScoringProfileId",
                value: null,
                isRequired: false,
                inputType: 'hidden'
            }
        ]
    }

    class AddToEntitySiteModalCtrl {
        title = 'Add New';
        clientReportSettings = [];
        scoringProfiles: any = [];
        message: string = '';
        resultObj: adminInterfaces.INodeObject[];
        selectedScoringProfile = null;
        externalSiteId: any;
        hasExternalSiteId: boolean;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'entity',
            'parent',
            'entityAddFunc',
            'obj',
            '$log',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.apibasesvc'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private entity: adminInterfaces.INodeObject,
            private parent: adminInterfaces.INodeObject,
            private entityAddFunc: any,
            private obj: any,
            private $log: angular.ILogService,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private apiSvc: commonSvc.IApiBaseSvc) {
            this.resultObj = [];
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            this.message = response.data.Message || 'An error has occurred while saving';
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            var params = {
                Name: this.entity.Name
            }
            // Pass parentId as param if available
            if (this.parent) {
                var parentIdName = this.parent.Id;
                params[parentIdName] = this.parent.SelectedValue;
            }
            // Save and close on success
            this.entityAddFunc(params).then((result) => {
                this.resultObj.push({ Id: result.Id, Name: result.Name, SelectedValue: 0, Collection: [] });
                var sitelookup = {
                    orgId: 10041, // Hardcoded SBM ID until new client ID structure is determined
                    siteId: result.Id,
                    externalSiteId: this.externalSiteId
                };

                // insert externalSiteId
                this.apiSvc.save(sitelookup, 'Site/SiteLookup/').then((result) => {
                    this.$uibModalInstance.close(this.resultObj);
                }, this.onFailure);
            });
        }
    }

    class AddToEntityModalCtrl {
        title = 'Add New';
        clientReportSettings = [];
        scoringProfiles: any = [];

        entityList: string[];
        resultObj: adminInterfaces.INodeObject[];

        selectedScoringProfile = null;

        static $inject = ['$scope', '$uibModalInstance', 'entity', 'parent', 'entityAddFunc', 'obj', 'chaitea.common.services.tenantmanagementsvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private entity: adminInterfaces.INodeObject,
            private parent: adminInterfaces.INodeObject,
            private entityAddFunc: any,
            private obj: any,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc) {

            this.resultObj = [];
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            for (var i = 0; i < this.entityList.length; i++) {
                var params = {
                    Name: this.entityList[i]
                }

                // Pass parentId as param if available
                if (this.parent) {
                    var parentIdName = this.parent.Id;
                    params[parentIdName] = this.parent.SelectedValue;
                }

                // Save and close on success
                this.entityAddFunc(params).then((result) => {
                    this.resultObj.push({ Id: result.Id, Name: result.Name, SelectedValue: 0, Collection: [] });

                    if (this.resultObj.length === this.entityList.length)
                        this.$uibModalInstance.close(this.resultObj);
                });
            }

        }
    }

    class EditEntryForEntityModalCtrl {
        clientReportSettings: any = [];
        scoringProfiles: any = [];

        selectedScoringProfile = null;

        static $inject = ['$scope', '$uibModalInstance', 'title', 'entity', 'entityUpdateFunc', 'entityAddFunc', 'obj', '$q', 'chaitea.common.services.tenantmanagementsvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private title: string,
            private entity: adminInterfaces.INodeObject,
            private entityUpdateFunc: any,
            private entityAddFunc: any,
            private obj: any,
            private $q: angular.IQService,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc) {
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            var callback = (result) => {
                this.$uibModalInstance.close({ Id: result.Id, Name: result.Name });
            }

            // Save and close on success
            if (this.entity.Id) {
                this.entityUpdateFunc(this.entity.Id, this.entity).then(callback);
            } else {
                this.entityAddFunc(this.entity).then(callback);
            }


        }
    }

    class EditEntryForSiteModalCtrl {
        clientReportSettings: any = [];
        scoringProfiles: any = [];
        selectedScoringProfile = null;
        externalSiteId: any;
        hasExternalSiteId: boolean = false;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'title',
            'entity',
            'entityUpdateFunc',
            'entityAddFunc',
            'obj',
            '$q',
            '$log',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private title: string,
            private entity: any,
            private entityUpdateFunc: any,
            private entityAddFunc: any,
            private obj: any,
            private $q: angular.IQService,
            private $log: angular.ILogService,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private apiSvc: commonSvc.IApiBaseSvc,
            private notificationSvc: commonSvc.INotificationSvc) {

            if (entity.Id != null && entity.Id != undefined)
                this.loadSiteLookup();
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
         * @description Get the external site id, 404 is thrown if null is returned from service
         */
        private loadSiteLookup = (): void => {
            this.apiSvc.getById(this.entity.Id, 'Site/SiteLookup').then((response) => {
                if (response.ExternalSiteId !== null && response.ExternalSiteId !== undefined) {
                    this.externalSiteId = Number(response.ExternalSiteId);
                    this.hasExternalSiteId = true;
                }
            },
                (error) => {
                    if (error.status === 404) {
                        this.hasExternalSiteId = false;
                    }
                    else {
                        this.notificationSvc.errorToastMessage();
                    }
                });
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            // update externalSiteId
            if (!this.hasExternalSiteId && this.externalSiteId !== undefined) {
                var sitelookup = {
                    OrgID: 10041, // Hardcoded SBM ID until new client ID structure is determined
                    SiteID: this.entity.Id,
                    ExternalSiteId: this.externalSiteId
                };

                this.apiSvc.save(sitelookup, 'Site/SiteLookup/').then((result) => {
                    this.updateAndClose();
                }, this.onFailure);
            }
            else {
                this.updateAndClose();
            }
        }

        updateAndClose = (): void => {
            var callback = (result) => {
                this.$uibModalInstance.close({ Id: result.Id, Name: result.Name });
            }

            // Save and close on success
            if (this.entity.Id) {
                this.entityUpdateFunc(this.entity.Id, this.entity).then(callback);
            } else {
                this.entityAddFunc(this.entity).then(callback);
            }
        }
    }

    class EditEntryForProgramModalCtrl {
        clientReportSettings: any = [];
        scoringProfiles: any = [];
        selectedScoringProfile = null;
        programLookup: any;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'title',
            'entity',
            'entityUpdateFunc',
            'entityAddFunc',
            'obj',
            '$q',
            '$log',
            'chaitea.common.services.tenantmanagementsvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private title: string,
            private entity: any,
            private entityUpdateFunc: any,
            private entityAddFunc: any,
            private obj: any,
            private $q: angular.IQService,
            private $log: angular.ILogService,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private apiSvc: commonSvc.IApiBaseSvc,
            private notificationSvc: commonSvc.INotificationSvc) {

            if (entity.Id != null && entity.Id != undefined)
                this.loadProgramLookup();
        }

        onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }

        /**
         * @description Get the program lookup record, 404 is thrown if null is returned from service
         */
        loadProgramLookup = (): void => {
            this.apiSvc.getById(this.entity.Id, 'Programs/ProgramLookup/').then((response) => {
                if (response != null && response != undefined) {
                    this.programLookup = response;
                }
            }, (error) => {
                if (error.status !== 404) {
                    this.notificationSvc.errorToastMessage();
                }
            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            var callback = (result) => {
                this.programLookup.Id = result.Id;
                this.programLookup.Name = result.Name;

                this.apiSvc.save(this.programLookup, 'Programs/ProgramLookup/').then((result) => {
                    this.$uibModalInstance.close({ Id: result.Id, Name: result.Name });
                }, this.onFailure);
            }

            // Save and close on success
            if (this.entity.Id) {
                this.entityUpdateFunc(this.entity.Id, this.entity).then(callback);
            } else {
                this.entityAddFunc(this.entity).then(callback);
            }
        }
    }

    class EditUserModalCtrl {
        Clients: Array<commonInterfaces.IClient>;
        User: commonInterfaces.IUser;
        Roles: Array<commonInterfaces.IUserRole>;
        UserClients: Array<any>;
        title: string = 'Edit User';
        message: string = '';
        PluckedSelectedClientIds: any = [];
        SelectedUserClients: any = [];
        Languages: Array<IOption> = [];

        local: any = {
            SelectedClient: null
        }

        static $inject = ['$scope', '$uibModalInstance', 'clients', 'user', 'roles', 'languages', 'chaitea.common.services.tenantmanagementsvc', 'chaitea.common.services.notificationsvc'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private clients: Array<commonInterfaces.IClient>,
            private user: commonInterfaces.IUser,
            private roles: Array<commonInterfaces.IUserRole>,
            private languages: Array<IOption>,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc,
            private notificationSvc: commonSvc.INotificationSvc) {
            this.Clients = clients;
            this.User = user;
            this.Roles = roles;
            this.Languages = languages;
            this.PluckedSelectedClientIds = _.pluck(this.User['Clients'], 'Id');
            this.UserClients = angular.copy(user["Clients"]);
            this.title = !this.User.Id ? 'New User' : this.title;
            var self = this;
            this.$scope.$watch(() => (this.PluckedSelectedClientIds), (newVal, oldVal) => {
                var allClients = this.Clients;

                //Create new list of models from selection
                this.UserClients = _.filter(allClients, (client: any) => {
                    return _.contains(newVal, client.Id);
                });


                //Add roles to existing and empty array to Roles if not available
                _.forEach(this.UserClients, (client) => {
                    _.forEach(this.User.Clients, (uClient) => {
                        if (client.Id == uClient.Id && uClient.Roles.length) {
                            client.Roles = uClient.Roles;
                        }
                    })

                    if (!client.Roles)
                        client.Roles = [];
                });
            });
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            if (response.data != null && response.data.length > 0) {
                this.notificationSvc.errorToastMessage(response.data);
            } else {
                this.notificationSvc.errorToastMessage();
            }
        }

        /**
         * @description Toggle role for a user.
         */
        toggleRole = (role: commonInterfaces.IUserRole): void => {

            if (_.find(_.find(this.UserClients, { Id: this.currentSelectedClientId() }).Roles, { Id: role.Id })) {
                _.remove(_.find(this.UserClients, { Id: this.currentSelectedClientId() }).Roles, { Id: role.Id });
            }
            else {
                var roleModel = _.pick(role, 'Id', 'Name');
                _.find(this.UserClients, { Id: this.currentSelectedClientId() }).Roles.push(roleModel);
            }
        }

        /**
         * @description Pulls the last selected item (ID) from the SelectedClient array.
         */
        currentSelectedClientId = (): number => {
            if (this.local.SelectedClient !== undefined && this.local.SelectedClient.length > 0) {
                return this.local.SelectedClient.slice(-1)[0];
            }
            return null;
        }

        /**
         * @description Checks to see if more than one client item has been checked.
         */
        isMultipleClientSelect = (): boolean => {
            return (this.local.SelectedClient !== undefined && this.local.SelectedClient !== null && this.local.SelectedClient.length > 1);
        }

        /**
         * @description Check if a roleId is in the list of user's roles.
         */
        isRoleSelected = (id: number): boolean => {
            return _.find(_.find(this.UserClients, { Id: this.currentSelectedClientId() }).Roles, { Id: id }) != null;
        }

        getRolesForSelectedClient = () => {

        }

        /**
         * @description Save or update the user.
         */
        save = (): void => {
            this.message = '';
            var userObj = angular.copy(this.User);

            if (this.isMultipleClientSelect()) {
                var currentClientRoles = _.find(this.UserClients, { Id: this.currentSelectedClientId() }).Roles;
                for (var i = 0; i < this.local.SelectedClient.length; i++) {
                    if (this.local.SelectedClient[i] !== this.currentSelectedClientId()) {
                        _.find(this.UserClients, { Id: this.local.SelectedClient[i] }).Roles = currentClientRoles;
                    }
                }
            }

            // update with the most recent changes
            userObj.Clients = this.UserClients;

            // Save or update the user
            if (userObj.Id) {
                this.tenantManagementSvc.updateUser(userObj.Id, userObj).then((result) => {
                    this.notificationSvc.successToastMessage("User has been updated.");
                    this.$uibModalInstance.close(result);
                }, this.onFailure);
            } else {
                this.tenantManagementSvc.saveUser(userObj).then((result) => {
                    this.notificationSvc.successToastMessage("User has been created.");
                    this.$uibModalInstance.close(result);
                }, this.onFailure);
            }
        }
    }

    class EditEntryForAreaClassificationModalCtrl {
        static $inject = ['$scope', '$uibModalInstance', 'title', 'entity', 'obj', '$q'];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private title: string,
            private entity: any,
            private obj: any,
            private $q: angular.IQService) {
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Save the new value.
         */
        save = (): void => {
            this.$uibModalInstance.close(this.entity.Value);
        }
    }

    angular.module('app').controller('Admin.AddToEntityModalCtrl', AddToEntityModalCtrl);
    angular.module('app').controller('Admin.AddToEntitySiteModalCtrl', AddToEntitySiteModalCtrl);
    angular.module('app').controller('Admin.EditEntryForEntityModalCtrl', EditEntryForEntityModalCtrl);
    angular.module('app').controller('Admin.EditEntryForProgramModalCtrl', EditEntryForProgramModalCtrl);
    angular.module('app').controller('Admin.EditEntryForSiteModalCtrl', EditEntryForSiteModalCtrl);
    angular.module('app').controller('Admin.EditUserModalCtrl', EditUserModalCtrl);
    angular.module('app').controller('Admin.EditEntryForAreaClassificationModalCtrl', EditEntryForAreaClassificationModalCtrl);
} 