﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {

    import ApiEndpoints = Common.ApiServices.Interfaces.ApiEndpoints;

    class CoreIndexCtrl implements Common.Interfaces.INgController{

        attributes: Array<Interfaces.IAttribute> = [];
        attributeAttachments: Array<Interfaces.IAttributeAttachment> = [];

        createEditForm;
        showCreateEditBox = false;
        isEdit = false;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.alertsvc',
            Common.Services.NotificationSvc.id
        ];

        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: Common.Services.INotificationSvc) {


        }
        
        public initialize = () => {

        }

        //#region Attribute
        public loadAttributes = () => {
            if (this.attributes.length) return;

            this.apiSvc.get(ApiEndpoints[ApiEndpoints.Attribute]).then((res: Array<Interfaces.IAttribute>) => {
                if (!res.length) return this.handleFailure('No attributes');
                this.attributes = res;
            },this.handleFailure);
        }

        public showAttributeCreate = (attribute?:any) => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Admin.CoreEditAttributeCtrl as vm',
                size: 'md',
                resolve: {
                    attribute: () => (attribute)
                }
            }).result.then(returned => {
                if (returned) {
                    this.finishAttributeEdit(returned);
                }
            }, this.handleFailure);
        }

        private finishAttributeEdit = (attribute: Interfaces.IAttribute) => {
            this.notificationSvc.successToastMessage(`Your item has been saved.`);
            if (attribute) {
                this.attributes.push(attribute);
            }
        }
        //#endregion Attribute 


        //#region AttributreAttachment
        public loadAttributeAttachments = () => {
            if (this.attributes.length == 0) return;

            this.apiSvc.get(ApiEndpoints[ApiEndpoints.AttributeAttachment]).then((res: Array<Interfaces.IAttributeAttachment>) => {
                if (!res.length) return this.handleFailure('No attributeAttachments');
                this.attributeAttachments = res;
                angular.forEach(this.attributeAttachments, (attachment) => {
                    attachment.DownloadUrl = this.awsSvc.getUrlByAttachment(attachment.UniqueId, attachment.AttachmentType);
                });
            }, this.handleFailure);
        }

        public showAttributeAttachmentCreate = (attributeAttachment?: Interfaces.IAttributeAttachment) => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Admin.CoreEditAttributeAttachmentCtrl as vm',
                size: 'md',
                resolve: {
                    attributeAttachment: () => (attributeAttachment),
                    attributes:()=>(this.attributes)
                }
            }).result.then(returned => {
                if (returned) {
                    this.finishAttributeAttachmentEdit(returned);
                    }
                }, this.handleFailure);
        }

        public deleteAttacment = (attributeAttachment: Interfaces.IAttributeAttachment) => {
            this.alertSvc.confirmWithCallback('Are you sure you want to delete \'' + attributeAttachment.FileName + '\' ?', (result: boolean) => {
                if (result) {
                    this.apiSvc.delete(attributeAttachment.AttributeAttachmentId, 'AttributeAttachment').then((result) => {
                        _.remove(this.attributeAttachments, (temp) => {
                            return temp.AttributeAttachmentId == attributeAttachment.AttributeAttachmentId;
                        })
                        this.notificationSvc.successToastMessage('Attribute Attachment has been deleted successfully!');
                    }, this.handleFailure);
                }
            });
        }

        private finishAttributeAttachmentEdit = (attributeAttachment: Interfaces.IAttributeAttachment) => {
            this.notificationSvc.successToastMessage(`Your item has been saved.`);
            
            if (attributeAttachment) {
                attributeAttachment.DownloadUrl = this.awsSvc.getUrlByAttachment(attributeAttachment.UniqueId, attributeAttachment.AttachmentType);
                this.attributeAttachments.push(attributeAttachment);
            }
        }
        //#endregion AttributeAttachment

        private handleFailure = (error) => {
            this.$log.error(error);
            return false;
        }

    }

    class CoreEditAttributeCtrl {
        createEditForm;
        isEdit: boolean = false;

        basePath: string;
        modalTitle: string = 'Create Attribute';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        hideFooter: boolean = true;
        static $inject = ['$scope', '$log', 'sitesettings', '$uibModalInstance', Common.Services.ApiBaseSvc.id, 'attribute'];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: Common.Services.IApiBaseSvc,
            private attribute: Interfaces.IAttribute) {

            if (attribute) {
                this.isEdit = true;
                this.modalTitle = 'Edit Attribute';
            }
            
            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Admin/Templates/Core/modal.admin.core.attribute.create.tmpl.html';

        }

        public saveAttribute = () => {
            if (!this.attribute) return;

            var attribute = {
                AttributeId: this.attribute.AttributeId,
                Code: this.attribute.Code,
                Name: this.attribute.Name
            };

            if (attribute.AttributeId) {
                this.apiSvc.update(attribute.AttributeId, attribute, ApiEndpoints[ApiEndpoints.Attribute])
                    .then((res: Interfaces.IAttribute) => {
                        this.done();
                    }, this.handleFailure);
            } else {
                this.apiSvc.save(attribute, ApiEndpoints[ApiEndpoints.Attribute])
                    .then((res: Interfaces.IAttribute) => {
                        this.done(res);
                    }, this.handleFailure);
            }
        }

        /**
        * @description Dismiss the modal instance.
        */
        cancel = (): void => {
            this.$uibModalInstance.dismiss();
        };

        done = (attribute?:Interfaces.IAttribute) => {
            this.$uibModalInstance.close(attribute || undefined);
        }

        public handleFailure = (error) => {
            this.$log.error(error);
        }
    }

    class CoreEditAttributeAttachmentCtrl {
        createEditForm;
        isEdit: boolean = false;
        files;
        isDisabled: boolean = false;
        basePath: string;
        modalTitle: string = 'Create Attribute Attachment';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        hideFooter: boolean = true;
       
        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            '$uibModalInstance',
            Common.Services.ApiBaseSvc.id,
            Common.Services.ImageSvc.id,
            'chaitea.services.credentials',
            'chaitea.common.services.awssvc',
            Common.Services.NotificationSvc.id,
            'attributeAttachment',
            'attributes'];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private apiSvc: Common.Services.IApiBaseSvc,
            private imageSvc: Common.Services.IImageSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private awsSvc: Common.Services.IAwsSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private attributeAttachment: Interfaces.IAttributeAttachment,
            private attributes: Array<Interfaces.IAttribute>) {

            if (attributeAttachment) {
                this.isEdit = true;
                this.modalTitle = 'Edit Attribute Attachment';
                this.isDisabled = true
            }
            else {
                this.isDisabled = false;
            }
          

            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Admin/Templates/Core/modal.admin.core.attributeAttachment.create.tmpl.html';

        }

        public saveAttributeAttachment = () => {
            if (!this.attributeAttachment) return;

            if (this.files && this.files[0]) {
                var file = this.files[0];

                var params = {
                    attributeId: this.attributeAttachment.AttributeId,
                    attributeValue: this.attributeAttachment.AttributeValue
                }

                this.apiSvc.query(params, 'AttributeAttachment/AttachmentCount/:attributeId/:attributeValue', false, false).then((result) => {
                    if (result.AttributeAttachmentCount > 0) {
                        this.notificationSvc.warningToastMessage(`There is already an existing attachment for the chosen Attribute and Value. Please edit the existing values.`);
                    }
                    else {
                        //auth
                        this.credentialSvc.getAwsCredentials().then((creds: Common.Interfaces.ISecurity) => {

                            if (!creds.AccessKey || !creds.AccessSecret) return this.handleFailure('not authorized');
                            this.awsSvc.s3Upload(file, creds, Common.Interfaces.S3Folders.file, Common.Interfaces.S3ACL.publicRead, false).then((data: any) => {

                                var attributeAttachment: Interfaces.IAttributeAttachment = this.attributeAttachment;
                                attributeAttachment["AttachmentType"] = Common.Interfaces.ATTACHMENT_TYPE[Common.Interfaces.ATTACHMENT_TYPE.File], //data.Type,
                                    attributeAttachment.UniqueId = data.Name,
                                    attributeAttachment.Attachment = {
                                    AttachmentId: attributeAttachment.AttachmentId,
                                        AttachmentTypeId: Common.Interfaces.ATTACHMENT_TYPE.File,
                                        UniqueId: data.Key
                                    };
                                if (attributeAttachment.AttributeAttachmentId) {
                                    this.apiSvc.update(attributeAttachment.AttributeAttachmentId, attributeAttachment, ApiEndpoints[ApiEndpoints.AttributeAttachment])
                                        .then((res: Interfaces.IAttributeAttachment) => {
                                            this.done(res);
                                        }, this.handleFailure);
                                } else {
                                    this.apiSvc.save(attributeAttachment, ApiEndpoints[ApiEndpoints.AttributeAttachment])
                                        .then((res: Interfaces.IAttributeAttachment) => {
                                            this.done(res);
                                        }, this.handleFailure);
                                }
                            });
                        });
                    }
                });
                
            }
            else if (this.attributeAttachment.UniqueId) {
                this.apiSvc.update(this.attributeAttachment.AttributeAttachmentId, this.attributeAttachment, ApiEndpoints[ApiEndpoints.AttributeAttachment])
                    .then((res: Interfaces.IAttributeAttachment) => {
                        this.done(res);
                    }, this.handleFailure);
            }
        }
        

        /**
        * @description Dismiss the modal instance.
        */
        cancel = (): void => {
            this.$uibModalInstance.dismiss();
        };

        done = (attributeAttachment?: Interfaces.IAttributeAttachment) => {
            this.$uibModalInstance.close(attributeAttachment || undefined);
        }

        public handleFailure = (error) => {
            this.$log.error(error);
        }
    }

    angular.module('app')
        .controller('Admin.CoreIndexCtrl', CoreIndexCtrl)
        .controller('Admin.CoreEditAttributeCtrl', CoreEditAttributeCtrl)
        .controller('Admin.CoreEditAttributeAttachmentCtrl', CoreEditAttributeAttachmentCtrl);
}