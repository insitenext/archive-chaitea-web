﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    enum Settings { Client, Site, Program };
    enum State { Create, Update };

    interface IAdminReportSettings extends ChaiTea.Common.Interfaces.IReportSettings {
        id?: number;
        ClientId?: number;
        SiteId?: number;
        ProgramId?: number;
        OrgId?: number;
    }

    class ReportCtrl {

        basePath: string = '';
        sort: string = 'asc';
        params: any = {
            $orderby: 'EmployeeId asc',
            $skip: 0,
            $top: 25
        };

        auditChartOptions = [
            { name: '', id: 0 },
            { name: '0 digits', id: 0 },
            { name: '1 digit (##.#)', id: 1 },
            { name: '2 digits (##.##)', id: 2 },
            { name: '3 digits (##.###)', id: 3 }
        ]

        client: ChaiTea.Common.Interfaces.ILookupListOption;
        site: ChaiTea.Common.Interfaces.ILookupListOption;
        program: ChaiTea.Common.Interfaces.ILookupListOption;
        scoringProfiles: Array<ChaiTea.Common.Interfaces.ILookupListOption> = [];
        scoringProfile: ChaiTea.Common.Interfaces.ILookupListOption;

        currentState: State = State.Create;
        buttonText: string = State[State.Create];
        currentSetting: Settings = Settings.Client;

        reportSetting: IAdminReportSettings;
        auditSettings: ChaiTea.Common.Interfaces.IAuditSettings;

        settingsText: string = '';

        auditList: Array<ChaiTea.Common.Interfaces.ILookupListOption> = [];

        myPromise = null;

        auditMsg: string = '';

        isDisabled: boolean = true;
        auditConfigSettings: ChaiTea.Common.Interfaces.IAuditSettings;
        clientScoringProfile = null;
        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.alertsvc',
            Common.Services.NotificationSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            private alertSvc: ChaiTea.Common.Services.IAlertSvc,
            private notificationSvc: Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;

            //add watchers
            this.$scope.$watch('$ctrl.client', (value) => {

                if (!value) return;

                this.site = null;
                this.program = null;

                this.currentSetting = Settings.Client;
                this.clientScoringProfile = null;
                this.scoringProfile = null;
                this.retirieveSetting();

            });

            this.$scope.$watch('$ctrl.site', (value) => {

                if (!value) return;

                if (!value['Key']) {

                    this.site = null;
                    this.program = null;

                    this.currentSetting = Settings.Client;
                } else {
                    this.program = null;

                    this.currentSetting = Settings.Site;
                }
                this.retirieveSetting();
            });

            this.$scope.$watch('$ctrl.program', (value) => {

                if (!value) return;

                this.currentSetting = Settings.Program;

                this.retirieveSetting();

            });

        }

        initialize = () => {
            this.getScoringProfiles();
        }

        public clearSelections = () => {
            this.client = null;
            this.site = null;
            this.program = null;
            this.loadSettings(null);
        }

        public save = () => {

            this.reportSetting.ScoringProfileId = parseInt(this.scoringProfile.Key);

            var parts = this.scoringProfile.Value.split(' - ');
            var max = parseInt(parts[1]);

            if (this.reportSetting.ScoringProfileGoal > max) {
                this.notificationSvc.errorToastMessage('Limits and control must be <= ' + max);
                return;
            }
            //set ids
            if (this.currentSetting == Settings.Client) {
                this.reportSetting.id = parseInt(this.client.Key);
                this.reportSetting.ClientId = this.reportSetting.id;
            } else if (this.currentSetting == Settings.Site) {
                this.reportSetting.id = parseInt(this.site.Key);
                this.reportSetting.SiteId = this.reportSetting.id;
            } else {
                this.reportSetting.ProgramId = parseInt(this.program.Key);
                this.reportSetting.OrgId = parseInt(this.site.Key);
            }

            if (this.currentState == State.Create) {
                this.reportSetting.id = null;
                this.apiSvc.save(this.reportSetting, Settings[this.currentSetting] + 'ReportSetting').then((res) => {
                    this.apiSvc.update(parseInt(this.client.Key), {}, 'ClientReportSetting/UpdateCreateAllSitesClientReportSetting').then((result) => {
                    }, this.handleError);
                    this.updateAllScoringProfiles();
                    this.notificationSvc.successToastMessage(Settings[this.currentSetting] + ' settings saved');
                    this.loadSettings(res);

                }, this.handleError);

            } else {

                this.apiSvc.update(this.reportSetting.id, this.reportSetting, Settings[this.currentSetting] + 'ReportSetting').then((res) => {
                    this.apiSvc.update(parseInt(this.client.Key), {}, 'ClientReportSetting/UpdateCreateAllSitesClientReportSetting').then((result) => {
                    }, this.handleError);
                    this.updateAllScoringProfiles();
                    this.notificationSvc.successToastMessage(Settings[this.currentSetting] + ' settings saved');
                    this.loadSettings(res);

                }, this.handleError);

            }

        }

        public updateAllScoringProfiles = () => {
            const params = {
                ClientId: this.client.Key,
                ScoringProfileId: this.scoringProfile.Key
            };
            this.apiSvc.update(null, params, 'ClientReportSetting/UpdateAllScoringProfiles').then((result) => {
            }, () => {
                this.handleError
            });
        }

        public runAudit = () => {

            this.auditList = [];
            this.auditMsg = '';

            //get all client sittings
            this.apiSvc.get('ClientReportSetting').then((settings) => {

                //get all clients
                this.apiSvc.getLookupList('User/Clients').then((lists) => {

                    angular.forEach(lists.Clients.Options, (client: ChaiTea.Common.Interfaces.ILookupListOption) => {

                        var exists = _.find(settings, { ClientId: parseInt(client.Key) });
                        if (!exists) {
                            this.auditList.push(client);
                        }

                    });

                    if (!this.auditList.length) {
                        this.auditMsg = 'All clients have report settings';
                    }

                });

            }, this.handleError);

        }

        private getScoringProfiles = () => {

            this.apiSvc.getLookupList('LookupList/ScoringProfiles').then((res: any) => {

                this.scoringProfiles = res.Options;

            }, (error) => {
                this.notificationSvc.errorToastMessage('Failed loading scoring profiles');
            });

        }

        private getAuditSettings = (clientId: any, siteId?: any, cascasdeClient?: boolean) => {
            var params:any = {
                ClientId: clientId
            }

            //Add other query params if SiteId is present
            if (siteId) {
                params = {
                    ClientId: clientId,
                    SiteId: siteId,
                    CascadeClient: cascasdeClient
                };
            }
            // Create new AuditSetting object
            this.auditSettings = new Common.Interfaces.AuditSetting();
            angular.extend(this.auditSettings, params);

            this.auditConfigSettings = new Common.Interfaces.AuditSetting();
            angular.extend(this.auditConfigSettings, params);
            async.waterfall([
                (callback) => {
                    this.apiSvc.query(params, 'AuditSetting', false, false)
                        .then((res: Common.Interfaces.IAuditSettings) => {
                            if (res) {
                                angular.extend(this.auditSettings, res);
                            }
                            callback(null, res);
                        })
                        .catch(err => callback(err));
                },
                (result, callback) => {
                    if (result && this.site && this.site.Key) {
                        if (result.SiteId == this.site.Key) {
                            angular.extend(this.auditConfigSettings, this.auditSettings);
                        }
                        else {
                            params = {
                                ClientId: clientId,
                                SiteId: siteId,
                                CascadeClient: false
                            };
                            this.apiSvc.query(params, 'AuditSetting', false, false)
                                .then((res: Common.Interfaces.IAuditSettings) => {
                                    if (res.SiteId) {
                                        angular.extend(this.auditConfigSettings, res);
                                    }
                                })
                                .catch(err => callback(err));
                        }
                    }
                }
            ], (err, result) => {
                this.handleError(err);
            });
        }

        public saveDisplaySettings = () => {
            var params: Common.Interfaces.IAuditSettings = this.auditSettings;
            if (this.auditSettings.ChartRoundingScale >= 0 && this.auditSettings.ClientId) {

                params.ChartRoundingScale = _.parseInt(<any>params.ChartRoundingScale);
                this.apiSvc.save(params, 'AuditSetting')
                    .then(res => {
                        this.auditConfigSettings.ChartRoundingScale = params.ChartRoundingScale;
                        this.notificationSvc.displaySimpleToast('Audit Chart Settings saved');
                    }, this.handleError);
            }
        }

        private auditConfigure = (isRequireToDoItem: boolean) => {
            if (this.client && this.site && this.site.Key) {
                if (isRequireToDoItem) {
                    this.auditConfigSettings.RequireToDosInternalAudit = !this.auditConfigSettings.RequireToDosInternalAudit;
                    if (this.auditConfigSettings.RequireToDosInternalAudit) {
                        this.alertSvc.alert({ message: 'This will also turn on Re-Run Audit option' });
                        this.auditConfigSettings.ReRunAuditsInternalAudit = true;
                    }
                    else {
                        this.alertSvc.confirmWithCallback('Would you like to turn off the Re-Run Audit option as well?', (result: boolean): void => {
                            if (result) {
                                this.auditConfigSettings.ReRunAuditsInternalAudit = false;
                            }
                            else {
                                this.auditConfigSettings.ReRunAuditsInternalAudit = true;
                            }
                            this.$scope.$apply();
                        });
                    }
                }
                else {
                    if (!this.auditConfigSettings.RequireToDosInternalAudit) {
                        this.auditConfigSettings.ReRunAuditsInternalAudit = !this.auditConfigSettings.ReRunAuditsInternalAudit;
                    }
                }
            }
        }

        private saveAuditConfig = () => {
            if (this.client && this.site && this.site.Key) {
                if (this.auditConfigSettings.AuditSettingId == 0) {
                    this.apiSvc.save(this.auditConfigSettings, "AuditSetting").then((res) => {
                        this.auditSettings.AuditSettingId = this.auditConfigSettings.AuditSettingId;
                        this.auditSettings.SiteId = this.auditConfigSettings.SiteId;
                        this.auditSettings.RequireToDosInternalAudit = this.auditConfigSettings.RequireToDosInternalAudit;
                        this.auditSettings.ReRunAuditsInternalAudit = this.auditConfigSettings.ReRunAuditsInternalAudit;
                        this.notificationSvc.successToastMessage('Your item has been saved.');
                    }, this.handleError);
                }
                else {
                    this.apiSvc.update(this.auditConfigSettings.AuditSettingId, this.auditConfigSettings, "AuditSetting").then(this.successMessage, this.handleError);
                }
            }
        }

        private successMessage = () => {
            this.notificationSvc.successToastMessage('Your item has been saved.');
        }
        private retirieveSetting = () => {

            var lookupId: number = null;
            var url = Settings[this.currentSetting] + 'ReportSetting';
            if (this.currentSetting == Settings.Client) {
                lookupId = parseInt(this.client.Key);
                this.getAuditSettings(this.client.Key);
            } else if (this.currentSetting == Settings.Site) {
                lookupId = parseInt(this.site.Key);
                this.getAuditSettings(this.client.Key, this.site.Key, true);
            } else {
                lookupId = parseInt(this.site.Key);
                url += '/' + this.program.Key
            }

            //load current settings
            this.apiSvc.getById(lookupId, url).then((res) => {
                this.loadSettings(res);
                
            }, (error: any) => {
                if (error.status == 404) {
                    this.loadSettings(null);
                }
            });

        }

        private loadSettings = (res: any) => {

            if (res) {
                this.reportSetting = res;
            }

            this.settingsText = "";
            this.isDisabled = true;

            if (this.client && !this.site && !this.program) {
                this.isDisabled = false;
                if (!res || !res.ClientReportSettingId) {
                    this.reportSetting.ScoringProfileGoal = 1;
                    this.reportSetting.ComplaintControlLimit = 1;
                    this.reportSetting.SurveyControlLimit = 1;
                }
                this.settingsText = " - Applying settings to " + this.client.Value;
                this.reportSetting.id = (res ? res.ClientReportSettingId : null);
            }

            if (this.client && this.site && !this.program) {
                this.settingsText = " - Applying settings to " + this.site.Value;
                this.reportSetting.id = (res ? res.SiteReportSettingId : null);
            }

            if (this.client && this.site && this.program) {
                this.settingsText = " - Applying settings to " + this.program.Value;
                this.reportSetting.id = (res ? res.ProgramReportSettingId : null);
            }

            if (!this.reportSetting.id) {
                this.currentState = State.Create;
                this.buttonText = State[State.Create];
                this.scoringProfile = null;
                if (this.site || this.program) {
                    this.scoringProfile = this.clientScoringProfile;
                    this.clearForm();
                }
            } else {
                this.buttonText = State[State.Update];
                this.currentState = State.Update;
                this.scoringProfile = _.find(this.scoringProfiles, { Key: res.ScoringProfileId });
            }
            if (this.client && !this.site && !this.program) {
                this.clientScoringProfile = this.scoringProfile;
            }
        }

        private clearForm = () => {
            this.reportSetting.id = null;
            this.reportSetting.ClientId = null;
            this.reportSetting.SiteId = null;
            this.reportSetting.ProgramId = null;
            this.reportSetting.ScoringProfileId = null;
            this.reportSetting.ComplaintControlLimit = null;
            this.reportSetting.ScoringProfileGoal = null;
            this.reportSetting.SurveyControlLimit = null;
        }

        private handleError = (error: any) => {
            this.$log.error(error);
            this.notificationSvc.errorToastMessage('Please contact support@sbmcorp.com');
        }

    }

    angular.module('app').controller('Admin.ReportCtrl', ReportCtrl);
}