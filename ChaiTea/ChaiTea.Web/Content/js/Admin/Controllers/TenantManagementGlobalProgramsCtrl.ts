﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    class TenantManagementGlobalProgramsCtrl {
        // Sorting
        order: string = 'Name';
        reverse: boolean = false;

        // Paging
        recordsToSkip: number = 0;
        top: number = 20;
        programs = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        searchCriteria: string;

        modalInstance;
        basePath: string;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;

        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
          
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging and re-get data.
         */
        getProgramsReset = (): void => {
            this.programs = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getPrograms();
        }

        /**
         * @desription Get the programs.
         */
        getPrograms = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var param = <commonInterfaces.IODataPagingParamModel>{
                $orderby: `${this.order} ${(this.reverse ? 'desc' : 'asc') }`,
                $top: this.top,
                $skip: this.recordsToSkip
            };
            if (this.searchCriteria) {
                param.$filter = `startswith(Name,'${this.searchCriteria}')`;
            }

            this.tenantManagementSvc.getProgramsByOData(param).then((data) => {
                if (data.length == 0) {
                    this.isAllLoaded = true;
                }

                angular.forEach(data,(obj) => {
                    this.programs.push(obj);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        addEditProgram = ($event: JQueryEventObject, program): void => {
            if ($event) $event.preventDefault();

            this.modalInstance = this.$uibModal.open({
                templateUrl: 'addToGlobalProgramModal.tmpl.html',
                controller: 'Admin.EditEntryForProgramModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return program ? `Editing ${program.Name}` : 'New Program'; },
                    entity: () => { return angular.copy(program) || { Name: '' }; },
                    entityUpdateFunc: () => { return this.tenantManagementSvc.updateProgram; },
                    entityAddFunc: () => { return this.tenantManagementSvc.saveProgram; },
                    reportSettingFunc: () => { },
                    obj: () => { }
                }
            });

            this.modalInstance.result.then((d: adminInterfaces.INodeObject) => {
                if (!program) {
                    this.programs.push(d);
                } else {
                    program.Name = d.Name;
                }

            }, null);
        }

        deleteProgram = ($event: JQueryEventObject, program): void => {
            if ($event) $event.preventDefault();
        }

        /**
         * @description Search the programs.
         */
        search = ($event: JQueryEventObject): void=> {
            if ($event) $event.preventDefault();

            this.getProgramsReset();
        }

        reset = ($event: JQueryEventObject): void=> {
            this.searchCriteria = null;
            this.search($event);
        }

        searchCriteriaChange = (): void=> {
            if (!this.searchCriteria) this.getProgramsReset();
        }
    }


    angular.module('app').controller('Admin.TenantManagementGlobalProgramsCtrl', TenantManagementGlobalProgramsCtrl);
} 