﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import adminSvc = ChaiTea.Admin.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import adminInterfaces = ChaiTea.Admin.Interfaces;

    class TenantManagementGlobalAreasCtrl {
        // Sorting
        order: string = 'Name';
        reverse: boolean = false;

        // Paging
        recordsToSkip: number = 0;
        top: number = 20;
        areas = [];
        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        searchCriteria: string;

        modalInstance;
        basePath: string;

        static $inject = [
            '$scope',
            '$q',
            'sitesettings',
            '$log',
            '$timeout',
            '$uibModal',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.tenantmanagementsvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $q: angular.IQService,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $timeout: angular.ITimeoutService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private alertSvc: commonSvc.IAlertSvc,
            private tenantManagementSvc: commonSvc.ITenantManagementSvc) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;

        }

        /**
         * @description Initialize the controller.
         */
        initialize = () => {
            //this.getAllLookuplist();
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Resets the paging and re-get data.
         */
        getAreasReset = (): void => {
            this.areas = [];
            this.recordsToSkip = 0;
            this.isAllLoaded = false;
            this.getAreas();
        }

        /**
         * @desription Get the areas.
         */
        getAreas = (): void => {
            if (this.isBusy || this.isAllLoaded) return;
            this.isBusy = true;

            var param = <commonInterfaces.IODataPagingParamModel>{
                $orderby: `${this.order} ${(this.reverse? 'desc' : 'asc')}`,
                $top: this.top,
                $skip: this.recordsToSkip
            };
            if (this.searchCriteria){
                param.$filter = `startswith(Name,'${this.searchCriteria}')`;
            }

            this.tenantManagementSvc.getAreasByOData(param).then((data) => {
                if (data.length == 0) {
                    this.isAllLoaded = true;
                }

                angular.forEach(data,(obj) => {
                    this.areas.push(obj);
                });
                this.isBusy = false;
                this.recordsToSkip += this.top;
            }, this.onFailure);
        }

        /**
         * @description Edit an area name.
         */
        addEditArea = ($event: JQueryEventObject, area): void => {
            if ($event) $event.preventDefault();

            this.modalInstance = this.$uibModal.open({
                templateUrl: 'addToEntityModal.tmpl.html',
                controller: 'Admin.EditEntryForEntityModalCtrl as vm',
                size: 'md',
                resolve: {
                    title: ()=>{ return area ? `Editing ${area.Name}`: 'New Area'; },
                    entity: () => { return angular.copy(area) || { Name: ''}; },
                    entityUpdateFunc: () => { return this.tenantManagementSvc.updateArea; },
                    entityAddFunc: () => { return this.tenantManagementSvc.saveArea; },
                    reportSettingFunc: () => { },
                    obj: () => { }
                }
            });

            this.modalInstance.result.then((d: adminInterfaces.INodeObject) => {
                if (!area) {
                    this.areas.push(d);
                } else {
                    area.Name = d.Name;
                }
               
            }, null);
        }

        deleteArea = ($event: JQueryEventObject, area): void => {
            if ($event) $event.preventDefault();
        }

        /**
         * @description Search the areas.
         */
        search = ($event: JQueryEventObject): void=> {
            if ($event) $event.preventDefault();

            this.getAreasReset();
        }

        /**
         * @description Reset the search filter.
         */
        reset = ($event: JQueryEventObject): void=> {
            if ($event) $event.preventDefault();

            this.searchCriteria = null;
            this.search($event);
        }

        searchCriteriaChange = (): void=> {
            if (!this.searchCriteria) this.getAreasReset();
        }
    }


    angular.module('app').controller('Admin.TenantManagementGlobalAreasCtrl', TenantManagementGlobalAreasCtrl);
} 