﻿module ChaiTea.Common.Filters {

    /**
      * @description Remove html from text
      */
    var HtmlToPlainText = function () {
        return function (text) {
            return text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    }
    angular.module('app').
        filter('htmlToPlaintext', HtmlToPlainText);

    displayTimezone.$inject = ['userContext'];
    function displayTimezone(userContext: IUserContext) {
        return function (text) {
            return text + ' ' + moment.tz(userContext.Timezone.TimezoneName).format('z');
        }
    }

    angular.module('app').filter('displayTimezone', displayTimezone);

}