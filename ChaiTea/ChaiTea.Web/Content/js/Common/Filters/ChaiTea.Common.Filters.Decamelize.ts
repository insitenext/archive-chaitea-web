﻿ module ChaiTea.Common.Filters {
     export function DecamelizeFilter() {
         return function (input) {

             //Handle non-strings
             if (typeof input !== "string")
                 return input;
             
             //Find and add space after captial letters
             var result = camelToTitle(input);

             return result;
         }


         function camelToTitle(str) {
             return str.replace(/([A-Z][a-z]+)/g, " $1") // Words beginning with UC
                 .replace(/([A-Z][A-Z]+)/g, " $1") // "Words" of only UC
                 .replace(/([^A-Za-z ]+)/g, " $1"); // "Words" of non-letters
         }

         function titleToCamel(str) {
             return str.replace(/\W+(.)/g, function (x, chr) {
                 return chr.toUpperCase();
             })
         }
     }

     

     angular.module('app').filter('decamelize', DecamelizeFilter);
}
