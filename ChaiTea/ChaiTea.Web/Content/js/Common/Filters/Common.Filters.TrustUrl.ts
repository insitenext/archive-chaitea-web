﻿var trustUrl = function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}
trustUrl.$inject = ['$sce'];

var trustAsHtml = function ($sce) {
    return function (html) {
        return $sce.trustAsHtml(html);
    };
}
trustAsHtml.$inject = ['$sce'];


angular.module('app')
    .filter('trustUrl', trustUrl)
    .filter('trustAsHtml',trustAsHtml);
