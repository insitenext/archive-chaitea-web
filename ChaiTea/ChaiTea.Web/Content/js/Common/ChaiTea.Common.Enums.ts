﻿module ChaiTea.Common.Enums {
    export enum Modules {
        Quality = <any>"quality",
        Personnel = <any>"personnel",
        Financials = <any>"financials",
        KnowledgeCenter = <any>"knowledge center",
        Governance = <any>"governance",
        Safety = <any>"safety"
    }

    export enum HTTP_STATUS_CODES {
        CONTINUE = 100,
        SWITCHING_PROTOCOLS = 101,
        OK = 200,
        CREATED = 201,
        ACCEPTED = 202,
        NON_AUTHORITATIVE_INFORMATION = 203,
        NO_CONTENT = 204,
        RESET_CONTENT = 205,
        PARTIAL_CONTENT = 206,
        MULTIPLE_CHOICES = 300,
        MOVED_PERMANENTLY = 301,
        FOUND = 302,
        SEE_OTHER = 303,
        NOT_MODIFIED = 304,
        USE_PROXY = 305,
        TEMPORARY_REDIRECT = 307,
        BAD_REQUEST = 400,
        UNAUTHORIZED = 401,
        PAYMENT_REQUIRED = 402,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        METHOD_NOT_ALLOWED = 405,
        NOT_ACCEPTABLE = 406,
        PROXY_AUTHENTICATION_REQUIRED = 407,
        REQUEST_TIMEOUT = 408,
        CONFLICT = 409,
        GONE = 410,
        LENGTH_REQUIRED = 411,
        PRECONDITION_FAILED = 412,
        REQUEST_ENTITY_TOO_LARGE = 413,
        REQUEST_URI_TOO_LONG = 414,
        UNSUPPORTED_MEDIA_TYPE = 415,
        REQUESTED_RANGE_NOT_SATISFIABLE = 416,
        EXPECTATION_FAILED = 417,
        UNPROCESSABLE_ENTITY = 422,
        TOO_MANY_REQUESTS = 429,
        INTERNAL_SERVER_ERROR = 500,
        NOT_IMPLEMENTED = 501,
        BAD_GATEWAY = 502,
        SERVICE_UNAVAILABLE = 503,
        GATEWAY_TIMEOUT = 504,
        HTTP_VERSION_NOT_SUPPORTED = 505,
    }
	export enum QUERY_PARAMETERS {
        CurrentEmployeeId = <any>"CE_ID"

    }
    export enum ATTRIBUTE_CODE {
        TIME_CLOCK = 1,
        UNION_CODE = 2
    }
    export enum LOCALSTORAGE_ITEMS {
        sideBarOpenState = <any>"sbm.sidebar:openstate"
	}
    export enum KPITYPE {
        Safety = 1,
        Complaints = 2,
        Conduct = 3,
        Attendance = 4,
        Professionalism = 5,
        Quality = 6,
        Training = 7,
        Ownership = 8
    }

    export enum LOCAL_STORAGE_KEYS {
        GOVERNANCE_MAP_CURRENT_TAB = <any>"governance.map-currentTabIndex"
    }

    export enum DashboardWidgets {
        TeamSafetyIncidents = 1,
        ComplaintsTrend = 2,
        TodosStats = 3,
        AuditTrend = 4,
        TeamHoursCurrentMonth = 5,
        PositionProfilesCompletion = 6,
        ComplimentsMostRecent = 7,
        SurveysMostRecent = 8,
        ToDosClosedOnTime = 9,
        MessagesDelivered = 10,
        Weather = 11,
        SurveysTrend = 12,
        TodosCurrentMonth = 13,
        WorkOrdersTrend = 14,
        ReportItsTrend = 15,
        BillingTrend = 16,
        SuppliesTrend = 17,
        AgingTrend = 18,
        StraightTimeHoursTrend = 19,
        RegularHoursTrend = 20,
        AttendanceCurrentMonth = 21,
        ConductCurrentMonth = 22,
        ProfessionalismCurrentMonth = 23,
        FailedAuditsCurrentMonth = 24,
        ReAudits = 25,
        FailedInspectionItemTodosCurrentMonth  = 26
    }

    export enum DashboardIds {
        Home = 1,
        Quality = 2,
        Financials = 3
    }
}
