﻿module ChaiTea.Common.Interfaces {
    export interface INgController {
        initialize: Function;
    }

    export interface IKeyValue {
        Key: number;
        Value: string;
    }

    export enum MessageBusMessages {
        //Notifications
        NotificationsActivityFeed = <any>'notification:activity-feed',
        NotificationsTodo = <any>'notification:todo',
        //Counts
        NotificationsTodoCount = <any>'notification-todocount',
        NotificationsActivityCount = <any>'notification-activitycount',
        NotificationsMessagesCount = <any>'notification-messagescount',
        MessageBuilderCreated = <any>'messagebuilder-created',
        //Context Picker
        ContextPickerOpen = <any>'contextpicker-open',
        ContextPickerClose = <any>'contextpicker-close',

        UserInfoUpdated = <any>'user-info:updated',
        //header
        HeaderDateChange = <any>'components.headerpanel:datechange',
        OpenFeedbackWidget = <any>'components.feedbackwidget:open',

        DetailsFilterChange = <any>'components.detailsfilter:change',
        DetailsFilterClear = <any>'components.detailsfilter:clear',
    }

    export enum S3Folders { image, file, video, audio };
    export enum S3ACL { private, publicRead, publicReadWrite, authenticatedRead, bucketOwnerRead, bucketOwnerFullControl };
    export enum OrgTypes { Organization = 1, Region = 2, Division = 3, Client = 4, Site = 5 };

    export interface ISecurity {
        AccessKey: string;
        AccessSecret: string;
        AccessToken: string;
        Signature: string;
        Endpoint: IEndpoint;
        TimeStamp: number;
    }

    export interface ISecuritySignedUrl {
        Url: string;
        EndPoint: string;
        Key: string;
    }

    export interface IEndpoint {
        Bucket: string;
        Cdn: string;
        Region: string;
    }

    export interface IAwsObjectDetails {
        Url: string;
        Key: string;
        Name: string;
        Guid: string;
        Type: string;
        Size: number;
        Folder: string;
        AttachmentType: string;
        AttachmentTypeId: number;
        Thumbnail?: string;
    }

    export interface IAwsVideoObject {
        Url: string;
        Key: string;
        Name: string;
        Thumbnails: Array<string>;
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    export interface IPagingParamModel {
        top: number;
        skip: number;
        phrase: string;
    }

    export interface ILookupListOption {
        Key: string;
        Value: string;
    }

    export interface IDateRange {
        startDate?: string;
        endDate?: string;
    }

    export interface ITrendDateRange extends IDateRange {
        trendId: number;
    }

    export interface IMailMessage {
        FromName: string;
        FromEmail: string;
        ToName: string;
        ToEmail: string;
        Subject: string;
        Message: string;
        Tag?: string;
    }

    export interface IEmployee {
        Id: number;
        Name: string;
        PositionName: string;
        PositionId: number;
    }
    export interface IEmployeeWithAttachment {
        EmployeeId: number;
        HireDate?: string;
        JobDescription: string;
        Name: string;
        ProfileImage?: string;
    }
    export interface IEmployeePosition {
        Id: number;
        PositionName: string;
    }

    export interface IUser {
        Id: number;
        FirstName: string;
        LastName: string;
        UserName: string;
        Password: string;
        ActiveLanguageId: number;
        Clients: Array<any>
    }

    export interface IUserRole {
        Id: number;
        Name: string;
    }

    export interface IClient {
        Id: number;
        Name: string;
    }

    export interface IOrg {
        Id: number;
        Name: string;
        Longitude?: number;
        Latitude?: number;
    }

    export interface IOrgSetting {
        Id: number;
        Name: string;
        UtcOffsetInMinutes: number;
        TimeZone: string;
        IsDstEnabled: boolean;
        ProfileImgAttachmentId: number;
        BackgroundImgAttachmentId: number;
    }

    export interface IReportSettingsAll {
        ClientReportSettings: IReportSettings;
        SiteReportSettings: IReportSettings;
        ProgramReportSettings: IReportSettings;
    }

    export interface IReportSettings {
        ScoringProfileId?: number;
        ScoringProfileMinimumScore: number;
        ScoringProfileMaximumScore: number;
        ScoringProfileGoal: number;
        ComplaintControlLimit: number;
        SurveyControlLimit: number;
        ClientReportSettingId?: number;
        SiteReportSettingId?: number;
        ProgramReportSettingId?: number;
    }

    export interface IAllSiteReportSettings {
        ClientReportSettings: IReportSettings;
        AllSitesReportSettings: IReportSettings[];
    }

    export interface IAuditSettings {
        AuditSettingId?: number;
        ClientId: number;
        SiteId?: number;
        ChartRoundingScale: number;
        RequireToDosInternalAudit: boolean;
        ReRunAuditsInternalAudit: boolean;
    }

    export class AuditSetting implements IAuditSettings{
        public AuditSettingId: number = 0;
        public ClientId: number;
        public SiteId: number = 0;
        public ChartRoundingScale: number = 2; // Defaults to 2 decimal points.
        public RequireToDosInternalAudit: boolean = false;
        public ReRunAuditsInternalAudit: boolean = false;
        constructor() { }
    }

    export interface IAttachment {
        AttachmentId?: number;
        AttachmentTypeId: number;
        AttachmentType?: string;
        UniqueId: string;
        Name?: string;
        Description?: string;
    }

    export enum KPIType {
        Safety,
        Complaints,
        Conduct,
        Attendance,
        Professionalism,
        Quality,
        Training,
        Ownership
    }

    export enum ATTACHMENT_TYPE {
        Image = 1,
        Video = 2,
        Audio = 3,
        Pdf = 4,
        File = 5
    }

    export enum ALL_ATTACHMENT_TYPES {
        ProfilePicture,
        SocialPicture,
        BackgroundPicture,
        Misc
    }

    export enum CONDUCT_ATTACHMENT_TYPE {
        Misc
    }

    export enum OWNERSHIP_ATTACHMENT_TYPE {
        Misc
    }

    export enum SAFETY_ATTACHMENT_TYPE {
        Misc
    }

    export enum ATTENDANCE_ATTACHMENT_TYPE {
        Misc
    }

    export interface ILocalStorageImage {
        Id: number;
        Key: string;
        Image: string;
    }

    export interface IMultipartUploadMap {
        Parts: Array<IMultipartUploadMapPart>;
    }

    export interface IMultipartUploadMapPart {
        ETag: string;
        PartNumber: number;
    }

    export interface actionLinks {
        Name: string;
        Link: string;
        Icon: string;
    }

    
} 