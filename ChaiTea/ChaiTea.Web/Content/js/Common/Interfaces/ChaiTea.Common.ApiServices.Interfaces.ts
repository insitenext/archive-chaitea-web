﻿module ChaiTea.Common.ApiServices.Interfaces {

    export enum ApiEndpoints {
        Attribute,
        AttributeAttachment,
        Client,
        Site,
        Building,
        Floor,
        Program,
        Area,
        ApplicationUser,
        ApplicationRole,
        ScoreCardDetail,
        ScoreCardDetailImage,
        CustomerRepresentative,
        AuditProfileSection,
        FloorProfile,
        Complaints,
        Compliments,
        SurveyResponses,
        Employee,
        LookupList,
        Report,
        WorkOrderSeries,
        TrainingEmployee,
        Job,
        JobTask,
        ServiceExcellence,
        Course,
        EmployeeCourse,
        QuickComment,
        Contract,
        EmployeeAudit,
        Invoice,
        TimeClock,
        ToDoItem,
        KpiComment,
        Ownership,
        Department,
        UserInfo,
        Conduct,
        Issue,
        Safety,
        POItem,
        ReportSetting,
        ClientReportSetting,
        SiteReportSetting,
        ProgramReportSetting,
        Org
    }

    /*Start Misc*/
    export interface ILookupListOption {
        Key: number;
        Value: string;
    }

    /*Start Employee*/
    export interface ITrainingEmployeeWithCourse {
        Email?: string;
        UserAttachments?: Array<IUserAttachment>;
        OrgUserId: number;
        HireDate?: string;
        JobDescription: IJobDescription;
        Name: string;
        PhoneNumber?: string;
        ProfileImage?: string;
        EmployeeCourses?: Array<IEmployeeCourse>;
        CoursesPercentCompleted?: number;
        CoursesCompleted?: number;
        CoursesExpected?: number;
    }

    export interface IJobEquipment {
        JobEquipmentId?: number;
        Name: string;
    }

    export interface ISupervisor {
        SupervisorId: number;
        Name: string;
        JobDescription: string;
        PhoneNumber: string;
        Email?: string;
        ProfilePictureUniqueId?: string;
        ProfileImage?: string;
        Manager?: IManager;
    }

    export interface IUserAttachment extends Common.Interfaces.IAttachment {
        UserAttachmentId: number;
        UserAttachmentType: EMPLOYEE_ATTACHMENT_TYPE;
    }

    enum EMPLOYEE_ATTACHMENT_TYPE {
        ProfilePicture = 1
    }

    export interface IManager {
        ManagerId: number;
        Name: string;
        JobDescription: string;
        PhoneNumber: string;
        Email?: string;
        ProfileImage?: string;
        ProfilePictureUniqueId?: string;
    }

    /* Course*/
    export interface ICourse {
        CourseId?: number;
        Name: string;
        Overview?: string;
        DeadlineHire: number;
        DeadlineHireType: string;
        RetakeAfter: number;
        RetakeAfterType: string;
        OrgType: string;
        OrgClient?: IOrg;
        OrgSite?: IOrg;
        EmployeeFirstName?: string;
        EmployeeId: number;
        EmployeeLastName?: string;
        CourseAttachments: Array<ICourseAttachment>;
        Tags: Array<ITag>;
        CourseType?: ICourseType;
        CourseTypeId: number;
        CreateDate?: string;
        IsFeatured: boolean;
        coverImage: string;
        AverageRating?: number;
        TotalCourseTrainings?: number;
    }

    export interface ICourseType {
        CourseTypeId?: number;
        Name: string;
        Icon?: string;
        Color?: string;
    }

    export interface ITag {
        TagId?: number;
        Name: string;
    }

    export interface ICourseAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        CourseAttachmentId?: number;
        CourseAttachmentType: string;
        CourseId?: number;
        UniqueId: string;
        Name: string;
        Description: string;
        CourseAttachmentQuestions?: Array<ICourseAttachmentQuestion>;
        CourseSubAttachments?: Array<ICourseAttachment>;
        OrderPriority: number;
        CourseAttachmentParentUniqueId?: string;
        LanguageId?: number;
        Language?: ILanguage;
        IsPassed?: boolean;
        Views?: number;
    }

    export interface ICourseAttachmentQuestion {
        CourseAttachmentQuestionId?: number;
        CourseAttachmentQuestionOptions: Array<ICourseAttachmentQuestionOption>;
        OrderPriority: number;
        Question: string;
    }

    export interface ICourseAttachmentQuestionOption {
        CourseQuizQuestionOptionId?: number;
        IsAnswer: boolean;
        Option: string;

    }

    export interface ICourseAssign {
        ClientIds?: Array<number>;
        SiteIds?: Array<number>;
        CourseIds?: Array<number>;
        IsUnassign?: boolean;
    }

    export interface ICourseEmployeeAssign extends ICourseAssign {

        EmployeeIds?: Array<number>;

    }

    export interface IEmployeeCourse {
        EmployeeCourseId?: number;
        OrgUserId: number;
        CourseId: number;
        Course?: ICourse;
        IsSelfInitiated: boolean;
        IsCompleted?: boolean;
        CompletedDate?: string;
        TotalCompleted?: number;
        IsPassedDue?: boolean;
        DueDate?: string;
        SnapshotEmployeeCourses?: Array<ISnapshotEmployeeCourse>;
        CreateDate?: Date;
        Employee?: IEmployee;
        CreateById?: number;
        CanDelete?: boolean;
        CanAcknowledge?: boolean;
        IsActive?: boolean;
        FeaturedImage?: string;
        Views?: number;
        StatusIcon?: string;
        StatusClass?: string;
        ShowAcknowledgeButton?: boolean;
    }

    export interface ISnapshotEmployeeCourse {
        OrgUserId: number;
        SnapshotCourseId: number;
        SnapshotCourse?: ISnapshotCourse;
        EmployeeCourseId: number;
        CourseAttachmentId: number;
        Passed: boolean;
        Name: string;
        UniqueId: string;
        Description: string;
        CreateDate?: Date;
        ManagerOrgUserId?: number;
    }

    export interface ISnapshotCourse {
        SnapshotCourseId?: number;
        Name: string;
        Overview: string;
        DeadlineHire: number;
        DeadlineHireType: string;
        RetakeAfter: number;
        RetakeAfterType: string;
        OrgId: number;
        OrgUserId: number;
        CourseTypeId: number;
        SnapshotCourseQuestions: Array<ISnapshotCourseQuestions>;
    }

    export interface ISnapshotCourseQuestions {
        SnapshotCourseQuestionId?: number;
        OrderPriority: number;
        Question: string;
        Answer: string;
        SnapshotCourseQuestionOptions: Array<ISnapshotCourseQuestionOption>;
    }

    export interface ISnapshotCourseQuestionOption {
        SnapshotCourseQuestionOptionId?: number;
        Option: string;
        IsAnswer: boolean;
    }

    /*Employee*/
    export interface IEmployee {
        EmployeeId: number;
        FirstName: string;
        LastName: string;
        SiteId: number
        SiteName: string;
        UserAttachments: Array<any>;
        Id?: number;
        HireDate?: Date;
    }

    export interface IOrg {
        Id?: number;
        IsUserMapped?: boolean;
        Name: string;
    }

    /*Job*/
    export interface IJobDescription {
        DutySumarry: string;
        JobDuties: Array<IDuty>;
        JobId: number;
        LegalDescriptionSummary: string;
        LegalDescriptions: Array<ILegalDescription>;
        Name: string;
        Purpose: string;
        SampleTaskSummary: string;
        SampleTasks: Array<ISampeTask>;
        Summary: string;
        WorkEnvironment: string;
    }

    export interface IJobTask {
        JobTaskId?: number;
        Name: string;
    }

    export interface ISampeTask {
        SampleTaskId?: number;
        Name: string;
        JobId: number;
    }

    export interface ILegalDescription {
        LegalDescriptionId?: number;
        Name: string;
        Description: string;
    }

    export interface IDuty {
        DutyId?: number;
        Description: string;
    }

    /*Complaint*/
    export interface IComplaint {
        ComplaintId: number;
        IsRepeatComplaint: boolean;
        Note: string;
        Action: string;
        ProgramName: string;
        ComplaintTypeId: number;
        ComplaintTypeName: string;
        ComplaintStatusId: number;
        ClassificationId: number;
        ClassificationName: string;
        PreventableStatusId: number;
        PreventableStatusName: string;
        BuildingName: string;
        SiteName: string;
        SiteId: number;
        ClientId: number;
        CompletedDate: Date;
        FeedbackId: number;
        FeedbackDate: Date;
        CreateDate: Date;
        UpdateDate: Date;
        CustomerName: string;
        CustomerPhoneNumber: string;
        CreateUserId: number;
        CreateUserName: string;
        AccountableEmployees: Array<number>;
        AccountableEmployeeObjects: Array<ILookupListOption>;
        UpdateUserId: number;
        Description: string;
        BuildingId: number;
        FloorId: number;
        ProgramId: number;
        FeedbackEmployeeObjects: Array<IFeedbackEmployee>;
    }

    export interface IFeedbackEmployee {
        FeedbackEmployeeId: number;
        FeedbackId: number;
        EmployeeId: number;
        IsExempt: boolean;
    }
    export interface ICompliment {
        AccountableEmployeeObjects: Array<ILookupListOption>;
        AccountableEmployees: Array<number>;
        BuildingId: number;
        BuildingName: string;
        ClientId: number;
        ComplimentId: number;
        ComplimentTypeId: number;
        ComplimentTypeName: string;
        CreateDate: Date;
        CreateUserId: number;
        CreateUserName: string;
        CustomerName: string;
        CustomerPhoneNumber: string;
        Description: string;
        FeedbackDate: Date;
        FeedbackId: number;
        FloorId: number;
        ProgramId: number;
        ProgramName: string;
        SiteId: number;
        SiteName: string;
        UpdateDate: Date;
        UpdateUserId: number;
    }

    export interface IProfessionalismAudit {
        CheckBoxOptions: Array<any>;
        CheckBoxSelections: Array<any>;
        Comment: string;
        CreateBy: string;
        CreateDate: Date;
        EmployeeAuditId: number;
        EmployeeId: number;
        IsEventBasedAudit: boolean;
        IsPass: boolean;
        KpiOptions: Array<any>;
        UpdateBy: string;
        UpdateDate: Date;
    }

    /* Safety */
    export interface ISafetyItem {
        SafetyId: number;
        DateOfIncident: Date;
        Detail: string;
        IncidentTypes: Array<IIncidentType>;
        OrgUserId: number;
        SafetyAttachments: Array<ISafetyAttachment>;
        CreateDate: Date;
        IsExempt: boolean;
    }

    export interface IIncidentType {
        IncidentTypeId: number;
        Name: string;
        isSelected: boolean;
    }

    export interface ISafetyIncidentItem {
        SafetyIncidentId: number;
        DateOfOccurrence: Date;
        Details: string;
        SafetyIncidentTypes: Array<ISafetyIncidentType>;
        OrgUserId: number;
    }

    export interface ISafetyIncidentType {
        SafetyIncidentTypeId: number;
        Name: string;
        isSelected: boolean;
    }

    export interface ISafetyAttachmentType {
        SafetyAttachmentTypeId: number;
        Name: string;
    }

    export interface ISafetyAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        SafetyAttachmentTypeId?: number;
        SafetyAttachmentType: string;
        UniqueId: string;
    }

    /* ownership*/
    export interface IOwnership {
        OwnershipId: number;
        BuildingId: number;
        Classifications: Array<any>;
        CustomerRepresentatives: Array<any>;
        FloorId: number;
        Description: string;
        EmployeeId: number;
        IsCritical: number;
        CreateDate: Date;
        ReportTypeId: number;
        RejectionTypeId: number;
        Status: any;
        OwnershipAttachments: Array<IOwnershipAttachment>;
        IsExempt: boolean;
    }

    export interface IOwnershipAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        OwnershipAttachmentTypeId?: number;
        OwnershipAttachmentType: string;
        UniqueId: string;
    }

    export interface IOwnershipType {
        OwnershipTypeId: number;
        Name: string;
        IsSelected: boolean;
    }

    /* Conduct */
    export interface IConduct {
        ConductId: number;
        DateOfOccurrence: Date;
        Reason: string;
        ConductTypes: Array<IConductType>;
        OrgUserId: number;
        ConductAttachments: Array<IConductAttachment>;
    }

    export interface IConductType {
        ConductTypeId: number;
        Name: string;
        IsSelected: boolean;
    }

    export interface IConductAttachmentType {
        ConductAttachmentTypeId: number;
        Name: string;
    }

    export interface IConductAttachment {
        AttachmentId?: number;
        AttachmentType: string;
        ConductAttachmentTypeId?: number;
        ConductAttachmentType: string;
        UniqueId: string;
    }

    /* Attendance */
    export interface IAttendanceItem {
        IssueId: number;
        DateOfOccurrence: Date;
        Comment: string;
        IssueTypeId: number;
        IssueType: string;
        ReasonId: number;
        ReasonName: string;
        OrgUserId: number;
    }

    export interface IAttendanceType {
        IssueTypeId: number;
        Name: string;
        Reasons: Array<IAttendanceReason>;
    }

    export interface IAttendanceReason {
        ReasonId: number;
        Name: string;
    }

    /* Language */
    export interface ILanguage {
        LanguageId?: number;
        Name: string;
        Code: string;
        Selected?: boolean;
    }
    export interface IInvoiceItem {
        InvoiceItemId: number;
        Quantity: number;
        Rate: number;
        Amount: number;
        Description: string;
        LineNumber: number;
        CurrencyCodeId: number;
        CurrencyCodeName: string;
        IsTaxable: boolean;
        IsPaid: boolean;
        InvoiceId: number;
    }

    export interface IInvoice {
        InvoiceId: number;
        InvoiceNumber: number;
        BilledAmount: number;
        TaxAmount: number;
        InvoiceAmount: number;
        PaidAmount: number;
        OutstandingAmount: number;
        IsPaid: boolean;
        MonthOfService: string;
        MonthOfServiceDate: Date;
        InvoiceDate: Date;
        DueDate: Date;
        PO: string;
        Base: number;
        AboveBase: number;
        PaymentTermsTypeId: number;
        PaymentTermsTypeName: string;
        OrgId: number;
        InvoiceItems: Array<IInvoiceItem>;
    }

}  