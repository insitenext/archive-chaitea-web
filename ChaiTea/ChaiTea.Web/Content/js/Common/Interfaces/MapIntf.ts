﻿module ChaiTea.Common.Interfaces {

    export interface IMapStyles {
        Apple: any;
        BlueWater: any;
        SubtleGrayscale: any;
        UltraLight: any;
        ShadesGrey: any;
        MidnightCommander: any;
        OrangeDark: any;
        Navigation: any;
        Zombie: any;
        Frozen: any;
        OldDryMud: any;
        Insite: any;
    }

    export interface IMapOptions {
        styles: any;        
    }

    export interface IMapLocation {
        latitude: number;
        longitude: number;
        name?: string;
        control?: number;
        siteId?: number;
        complaintControlLimit?: number;
    }

    export interface IMap {
        center: IMapLocation;
        zoom: number;
        options: IMapOptions;
    }

    export interface IGeoLocation {
        accuracy: number;
        altitude: number;
        altitudeAccuracy: number;
        heading: number
        latitude: number;
        longitude: number;
        speed: number;
    }

    export interface IMapMarker {
        id: number;
        coords: IMapLocation;
        options?: IMapMarkerOptions;
        window?: IMapMarkerWindow;
        events?: any;
    }

    export interface IMapMarkerWindow {
        title: string;
        metric: number;
        metricLabel: string;
        data: any;
        link: string;
        template: string;
        class: string;
        passing: boolean;
        options: IMapMarkerWindowOptions;
        customColor: string;
        show: boolean;
    }

    export interface IMapMarkerWindowOptions {
        maxWidth?: number;
        disableAutoPan?: boolean;
        pixelOffset?: any;
    }

    export interface IMapMarkerOptions {
        draggable: boolean;
        icon: IMapSVGIcon;
        labelContent: string;
        labelAnchor: string;
        labelClass: string;
        labelInBackground: boolean;
    }

    export interface IMapSVGIcon {
        path: string;
        fillColor: string;
        fillOpacity: number;
        strokeColor: string;
        strokeWeight: number;
        scale: number;
    }

    export interface IMapCircle {
        id: number;
        center: IMapLocation;
        radius: number;
        stroke: IMapCircleStroke;
        fill: IMapCircleFill;
        geodesic: IMapCircleGeo;
    }

    export interface IMapCircleStroke {
        color: string;
        weight: number;
        opacity: number;
    }

    export interface IMapCircleFill {
        color: string;
        opacity: number;
    }

    export interface IMapCircleGeo {
        draggable: boolean;
        clickable: boolean;
        editable: boolean;
        visible: boolean;
        control: any;
    }

}