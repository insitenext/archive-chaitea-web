﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;

    interface ILanguageResource extends ng.resource.IResourceClass<any> {
        get(): ng.resource.IResource<any>;
    }

    export interface ILanguageSvc {
        getLanguages();
    }

    class LanguageSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ILanguageSvc {
            return {
                getLanguages: () => { return this.getLanguages(); }
            };
        }

        /**
         * @description gets list of cultures.
         */
        getLanguages = () => {
            return this.languageResource().get().$promise;
        }

        /**
         * @description Resource object for userinfo.
         */
        private languageResource = (): ILanguageResource => {
            var url = this.basePath + 'api/Services/Language/';

            return <ILanguageResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    get: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }
    }
    angular.module('app').service('chaitea.common.services.languagesvc', LanguageSvc);
}