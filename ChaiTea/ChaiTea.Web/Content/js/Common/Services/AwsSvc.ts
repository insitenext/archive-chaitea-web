﻿/// <reference path="../../_libs.ts" />

/**
 * @description Amazon AWS service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IAwsSvc {
        getCredentials(creds: commonInterfaces.ISecurity): AWS.Credentials;
        s3MultipartUpload(file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, fileNameOverride?: string): angular.IPromise<any>;
        s3Upload(file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, isBase64?: boolean, fileNameOverride?: string): angular.IPromise<any>;
        s3Download(key: string, credentials: commonInterfaces.ISecurity, bucket?: string): angular.IPromise<any>;
        encodeVideo(fileNameFull: string, creds: commonInterfaces.ISecurity, presets?: Array<string>): angular.IPromise<any>;
        getUrl(key: string): string;
        getSecureUrlByName(key: string, folder: commonInterfaces.S3Folders):angular.IPromise<any>;
        getUrlByName(fileName: string, folder: commonInterfaces.S3Folders): string;
        getSignedVideoUrl(videoName: string, videoSize?: number): angular.IPromise<commonInterfaces.IAwsVideoObject>;
        getUrlByAttachment(uniqueId: string, attachmentType: string): string;
        getFolderByType(fileType: string): commonInterfaces.S3Folders;
        getFolderByAttachmentType(attachmentType: string): commonInterfaces.S3Folders;
        getFileIcon(attachmentType: string): string;
    }

    export class AwsSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.awssvc';
        roles: Array<string> = ["private", "public-read", "public-read-write", "authenticated-read", "bucket-owner-read", "bucket-owner-full-control"];

        static $inject = [
            '$log',
            '$q',
            '$timeout',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.utilssvc',
            'awsconfig'
        ];
        constructor(
            private $log: ng.ILogService,
            private $q: ng.IQService,
            private $timeout: ng.ITimeoutService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private lookupListSvc: ChaiTea.Services.ILookupListSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private awsConfig: commonInterfaces.IEndpoint) {
        }

        public $get(): IAwsSvc {
            return {
                getCredentials: (creds: commonInterfaces.ISecurity) => { return this.getCredentials(creds); },
                s3MultipartUpload: (file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, fileNameOverride?: string) => { return this.s3MultipartUpload(file, credentials, folder, acl, fileNameOverride); },
                s3Upload: (file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, isBase64?: boolean, fileNameOverride?: string) => { return this.s3Upload(file, credentials, folder, acl, isBase64, fileNameOverride); },
                s3Download: (key: string, credentials: commonInterfaces.ISecurity, bucket?: string) => { return this.s3Download(key, credentials, bucket); },
                encodeVideo: (fileNameFull: string, creds: commonInterfaces.ISecurity, presets?: Array<string>) => { return this.encodeVideo(fileNameFull, creds, presets); },
                getUrl: (key: string) => { return this.getUrl(key); },
                getSecureUrlByName: (key: string, folder: commonInterfaces.S3Folders) => { return this.getSecureUrlByName(key, folder); },
                getUrlByName: (fileName: string, folder: commonInterfaces.S3Folders) => { return this.getUrlByName(fileName, folder); },
                getSignedVideoUrl: (videoName: string, videoSize?: number) => { return this.getSignedVideoUrl(videoName, videoSize); },
                getUrlByAttachment: (uniqueId: string, attachmentType: string) => { return this.getUrlByAttachment(uniqueId, attachmentType); },
                getFolderByType: (fileType: string) => { return this.getFolderByType(fileType); },
                getFolderByAttachmentType: (attachmentType: string) => (this.getFolderByAttachmentType(attachmentType)),
                getFileIcon: (attachmentType: string) => (this.getFileIcon(attachmentType))
        }
        }

        private getCredentials = (creds: commonInterfaces.ISecurity): AWS.Credentials => {

            var credentials = new AWS.Credentials(creds.AccessKey, creds.AccessSecret, creds.AccessToken);
            AWS.config.update({ credentials: credentials });
            AWS.config.region = creds.Endpoint.Region;

            return credentials;

        }

        /**
         * @description Starts a multipart upload.
         */
        private s3MultipartUpload = (file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, fileNameOverride?: string): angular.IPromise<any> => {

            var def = this.$q.defer();
           
            var parts: number = 0;
            var partNum: number = 1;
            var partSize: number = 1024 * 1024 * 10;
            var uploadMap = {
                Parts: []
            };
            var uploadId: string = '';
            var partCounter: number = 0;
            var maxTries: number = 3;

            try {

                this.getCredentials(credentials);

                //set required
                var bucket: string = credentials.Endpoint.Bucket;
                var fileName = this.utilSvc.generateUniqueToken();
                var fileExt = file.name.split('.').pop();
                var fileNameFull = (fileNameOverride ? fileNameOverride : fileName + '.' + fileExt);

                var folder: commonInterfaces.S3Folders = folder || this.getFolderByType(file.type);
                var key = commonInterfaces.S3Folders[folder].toString() + "/" + fileNameFull;
                var attachmentTypeId = this.getAttachmentTypeByType(file.type);
                var attachmentType = commonInterfaces.ATTACHMENT_TYPE[this.getAttachmentTypeByType(file.type)];

                if (file.size > partSize) {

                    var multiPartParams: AWS.S3CreateMultipartUploadRequest = {
                        Bucket: bucket,
                        Key: key,
                        ContentType: file.type,
                        ACL: this.getAcl(acl),
                    };

                    var s3 = new AWS.S3({ params: { Bucket: bucket } });

                    s3.createMultipartUpload(multiPartParams,(err, data) => {

                        if (err) return def.reject(err);

                        uploadId = data.UploadId;
                        parts = Math.ceil(file.size / partSize);
                        partCounter = parts;
                        def.notify(5);

                        for (var i = 0; i < file.size; i += partSize) {

                            var end = Math.min(i + partSize, file.size);

                            //build part params
                            var params: AWS.S3UploadPartRequest = {
                                Key: key,
                                Body: this.sliceBlob(file, i, end, file.type),
                                Bucket: bucket,
                                PartNumber: partNum,
                                UploadId: uploadId

                            };

                            var tries: number = 1;
                            this.uploadPart(s3, uploadId, params, maxTries, tries).then((partData) => {

                                var mapPart: commonInterfaces.IMultipartUploadMapPart = {
                                    ETag: partData.ETag,
                                    PartNumber: partData.PartNumber
                                };
                                
                                //manually set the index, requests will not come back in order
                                uploadMap.Parts[partData.PartNumber - 1] = mapPart;
                                //update percent bar
                                def.notify(Math.round((uploadMap.Parts.length / parts) * 100));

                                if (--partCounter == 0) {

                                    var doneParams: AWS.S3CompleteMultipartUploadRequest = {
                                        Bucket: bucket,
                                        Key: key,
                                        MultipartUpload: uploadMap,
                                        UploadId: uploadId
                                    };

                                    this.completeMultipartUpload(s3, doneParams).then((completeData) => {

                                        var details: commonInterfaces.IAwsObjectDetails = {
                                            Url: this.getUrl(key),
                                            Key: key,
                                            Name: fileNameFull,
                                            Guid: fileName,
                                            Type: file.type,
                                            Size: file.size,
                                            Folder: commonInterfaces.S3Folders[folder].toString().toLowerCase(),
                                            AttachmentType: attachmentType,
                                            AttachmentTypeId: attachmentTypeId
                                        };

                                        def.resolve(details);

                                    },(err: any) => {

                                        def.reject(err);

                                    });

                                }

                            },(err: any) => {
                                
                                def.reject(err);

                            });

                            partNum++;

                        }

                    });

                } else {
                    //call s3Upload
                    this.s3Upload(file, credentials, folder, acl, false, fileNameOverride).then((data) => {

                        def.resolve(data);

                    },(error: any) => {

                        def.reject(error);

                    }, (progress: any) => {

                        def.notify(progress);

                    });
                }

            } catch (error) {
                console.log(error);
                def.reject(error);
            }

            return def.promise;

        }

        /**
         * @description Uploads part of file.
         */
        private uploadPart = (s3: AWS.S3, uploadId: string, params: AWS.S3UploadPartRequest, maxTries: number, tries: number, def?: any): angular.IPromise<any> => {

            var $self = this;

            if (!def) {
                def = this.$q.defer();
            }

            try {

                var request: any = s3.uploadPart(params, null);

                request.on('error', function (res) {

                    //check for network failure
                    if (tries <= maxTries) {

                        $self.$timeout(function () {
                            console.log('Network Error Retrying: ', params.PartNumber);
                            tries++;
                            $self.uploadPart(s3, uploadId, params, maxTries, tries, def);
                        }, 1000);

                    } else {
                        def.reject(res.code);
                    }

                }).on('success', function (response) {
                    var data: any = response.data;
                    data.PartNumber = params.PartNumber;
                    def.resolve(data);
                }).send();

            } catch (error) {
                def.reject(error);
            }

            return def.promise;

        }

        /**
         * @description Finishes multipart upload and combines the parts.
         */
        private completeMultipartUpload = (s3: AWS.S3, params: AWS.S3CompleteMultipartUploadRequest) => {

            var def = this.$q.defer();

            try {

                s3.completeMultipartUpload(params, (err, data) => {

                    if (err) return def.reject(err);

                    def.resolve(data);

                });

            } catch (error) {
                def.reject(error);
            }

            return def.promise;

        }

        private sliceBlob(blob, start, end, fileType) {

            if (blob.mozSlice) {
                return blob.mozSlice(start, end, fileType);
            } else if (blob.webkitSlice) {
                return blob.webkitSlice(start, end, fileType);
            } else if (blob.slice) {
                return blob.slice(start, end, fileType);
            } else {
                throw new Error("File slice is not supported");
            }

        }


        /**
         * @description Upload file to S3.
         */
        private s3Upload = (file: any, credentials: commonInterfaces.ISecurity, folder?: commonInterfaces.S3Folders, acl?: commonInterfaces.S3ACL, isBase64?: boolean, fileNameOverride?: string): angular.IPromise<any> => {

            var def = this.$q.defer();

            try {
                //set creds
                this.getCredentials(credentials);

                //set required
                var bucket: string = credentials.Endpoint.Bucket;
                var fileName = this.utilSvc.generateUniqueToken();
                var fileNameFull = (fileNameOverride ? fileNameOverride + '.png' : fileName + '.png');

                if (!isBase64) {
                    var fileExt = file.name.split('.').pop();
                    var fileNameFull = (fileNameOverride ? fileNameOverride : fileName + '.' + fileExt);
                } else {
                    var binary = atob(file.split(',')[1]);
                    var array = [];
                    for (var i = 0; i < binary.length; i++) {
                        array.push(binary.charCodeAt(i));
                    }
                    file = new Blob([new Uint8Array(array)], { type: 'image/png' });
                }
                
                var folder: commonInterfaces.S3Folders = folder || this.getFolderByType(file.type);
                var key = commonInterfaces.S3Folders[folder].toString() + "/" + fileNameFull;
                var attachmentTypeId = this.getAttachmentTypeByType(file.type);
                var attachmentType = commonInterfaces.ATTACHMENT_TYPE[this.getAttachmentTypeByType(file.type)];

                var s3 = new AWS.S3({ params: { Bucket: bucket } });
                var params: AWS.S3PutObjectRequest = {
                    ACL: this.getAcl(acl),
                    Key: key,
                    ContentType: file.type,
                    Body: file,
                    Bucket: bucket
                };

                var uploader: any = s3.putObject(params,(err: any, data: AWS.S3PutObjectOutput) => {

                    if (err) def.reject(err);

                    var details: commonInterfaces.IAwsObjectDetails = {
                        Url: this.getUrl(key),
                        Key: key,
                        Name: fileNameFull,
                        Guid: fileName,
                        Type: file.type,
                        Size: file.size,
                        Folder: commonInterfaces.S3Folders[folder].toString().toLowerCase(),
                        AttachmentType: attachmentType,
                        AttachmentTypeId: attachmentTypeId
                    };

                    def.resolve(details);

                });

                uploader.on('httpUploadProgress', function (progress) {
                    var percent: number = Math.round(progress.loaded / progress.total * 100);
                    def.notify(percent);
                });


            } catch (error) {
                console.log(error);
                def.reject(error);
            }

            return def.promise;

        }

        /**
         * @description download object from s3.
         */
        private s3Download = (key: string, credentials: commonInterfaces.ISecurity, bucket?: string) => {

            var def = this.$q.defer();

            try {

                var bucket: string = credentials.Endpoint.Bucket;
                key = commonInterfaces.S3Folders[commonInterfaces.S3Folders.image].toString() + "/" + key;
                //set creds
                this.getCredentials(credentials);

                var s3 = new AWS.S3({ params: { Bucket: bucket } });

                s3.getObject({ Bucket: bucket, Key: key}, function (err, data) {
                        
                    if (err) def.reject(err);

                    def.resolve(data);

                });

            } catch (error) {
                console.log(error);
                def.reject(error);
            }

            return def.promise;

        }

        /**
         * @description Get full URL of file.
         */
        private encodeVideo = (fileNameFull: string, creds: commonInterfaces.ISecurity, presets?: Array<string>): angular.IPromise<any> => {

            var def = this.$q.defer();
            var elastictranscoder = new AWS.ElasticTranscoder();
            var fileNameParts = fileNameFull.split('.');
            var fileExt = fileNameParts[1];
            var fileName = fileNameParts[0];

            var pipelineId = '1441407359960-m8cz8b'; //prod

            if (creds.Endpoint.Bucket.indexOf('dev') > 0) {
                pipelineId = '1442009080126-z5kg3g';
            }

            //TODO: allow user to pass custom presets

            //http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/ElasticTranscoder.html
            var eParams: AWS.ElasticTranscoderCreateJobRequest = {
                Input: {
                    AspectRatio: 'auto',
                    Container: 'auto',
                    FrameRate: 'auto',
                    Interlaced: 'auto',
                    Key: 'video/' + fileNameFull,
                    Resolution: 'auto'
                },
                PipelineId: pipelineId,
                OutputKeyPrefix: 'video/',
                Outputs: this.getTranscoderOutputList(fileName)
            };

            elastictranscoder.createJob(eParams,(err, data) => {

                console.log('encode data', data);

                if (err) def.reject(err);

                def.resolve(data);

            });

            return def.promise;

        }

        /**
         * @description Returns list of transcoded files.
         */
        private getTranscoderOutputList = (fileName: string): Array<any> => {
            
            return [
                {
                    Key: fileName + '-720p.mp4',
                    PresetId: '1449001013255-ergpge',
                    Rotate: 'auto',
                    ThumbnailPattern: fileName + '-tb-720-{count}'
                },
                {
                    Key: fileName + '-480p.mp4',
                    PresetId: '1449001064210-5mciyo',
                    Rotate: 'auto',
                    ThumbnailPattern: fileName + '-tb-480-{count}'
                },
                {
                    Key: fileName + '-720p.webm',
                    PresetId: '1448921162741-jbgm3l',
                    Rotate: 'auto',
                    ThumbnailPattern: fileName + '-tb-720-{count}'
                },
                {
                    Key: fileName + '-480p.webm',
                    PresetId: '1449001109885-92354k',
                    Rotate: 'auto',
                    ThumbnailPattern: fileName + '-tb-480-{count}'
                },
            ];
            
        }

        /**
         * @description Get CDN URL of file.
         */
        private getUrl = (key: string): string => {
            return 'https://' + this.awsConfig.Cdn + '/' + key;
        }

        /**
         * @description Get CDN URL of file by name and folder.
         */
        private getUrlByName = (uniqueId: string, folder: commonInterfaces.S3Folders): string => {
            return this.getUrl(commonInterfaces.S3Folders[folder] + '/' + uniqueId);
        }

        /**
         * @description Get Secure CDN URL of file.
         * @todo Change to strongly types interfaces
         */
        private getSecureUrlByName = (uniqueId: string, folder: commonInterfaces.S3Folders): angular.IPromise<any> => {
            var def = this.$q.defer();

            this.credentialSvc.getAwsCloudfrontSignedUrl(commonInterfaces.S3Folders[folder], uniqueId).then((data: commonInterfaces.ISecuritySignedUrl) => {
                var details: commonInterfaces.IAwsObjectDetails = <any>{
                    Key: data.Key,
                    Name: uniqueId,
                    Url: data.Url
                };

                def.resolve(details);
            },def.reject);
            return def.promise;
        }

        /**
         * @description Get CDN URL with token of file.
         */
        private getSignedVideoUrl = (videoName: string, videoSize?: number, encoding?: string): angular.IPromise<commonInterfaces.IAwsVideoObject> => {

            var def = this.$q.defer();
            var supportedVideoSizes: Array<number> = [480, 720];

            try {

                var size = (videoSize ? videoSize : null);
                var encoding = (encoding ? encoding : 'mp4');
                var parts = videoName.split('.');
                var fileName = parts[0];
                var fileExt = parts[1];

                if (size && supportedVideoSizes.indexOf(size) < 0) {
                    throw 'Video size not supported';
                }

                var key = (!size ? videoName : fileName + '-' + size.toString() + 'p' + '.' + encoding);

                //create tokens for video and thumbs
                async.parallel([
                    //video
                    (callback) => {
                        this.credentialSvc.getAwsCloudfrontSignedUrl('video', key).then((data: commonInterfaces.ISecuritySignedUrl) => {
                            callback(null, data)
                        });
                    },
                    //thumbnail
                    (callback) => {
                        this.credentialSvc.getAwsCloudfrontSignedUrl('video', fileName + '-tb-' + size + '-00001.png').then((data: commonInterfaces.ISecuritySignedUrl) => {
                            callback(null, data)
                        });
                    }
                ],(err: any, res: Array<commonInterfaces.ISecuritySignedUrl>) => {
    
                    if (err) throw err; 

                    var details: commonInterfaces.IAwsVideoObject = {
                        Key: res[0].Key,
                        Name: key,
                        Thumbnails: [
                            res[1].Url
                        ],
                        Url: res[0].Url
                    };

                    def.resolve(details);

                });


            } catch (error) {
                def.reject(error);
            }

            return def.promise;

        }

        /**
         * @description Maps attachment types to s3 folders
         */
        private getUrlByAttachment = (uniqueId: string, attachmentType: string): string => {

            //these are the types that map one-to-one

            var mappings = ['image', 'file', 'video', 'audio'];
            var folder = commonInterfaces.S3Folders[commonInterfaces.S3Folders.file];
            var search = mappings.indexOf(attachmentType.toLowerCase());

            if (search > -1)
            {
                folder = uniqueId.toLowerCase().indexOf('image') !== -1
                    ? ''
                    : commonInterfaces.S3Folders[search];
            }

            return this.getUrl(folder + '/' + uniqueId);
        }

        /**
         * @description Converts enum to acl string.
         */
        private getAcl = (acl: commonInterfaces.S3ACL): string => {
            var role = acl || commonInterfaces.S3ACL.authenticatedRead;
            return this.roles[acl];
        }

        /**
         * @description Get signed URL.
         */
        private getFolderByType = (fileType: string): commonInterfaces.S3Folders => {

            var typeParts = fileType.split('/');

            if (typeParts[0] === 'video') {
                return commonInterfaces.S3Folders.video;
            }

            if (typeParts[0] === 'image') {
                return commonInterfaces.S3Folders.image;
            }

            if (typeParts[0] === 'audio') {
                return commonInterfaces.S3Folders.audio;
            }

            return commonInterfaces.S3Folders.file;
        }

        /**
         * @description Get file icon.
         */
        private getFileIcon = (attachmentType: string) => {
            if (attachmentType == commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Audio]) {
                return 'fa-file-sound-o';
            }
            if (attachmentType == commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Video]) {
                return 'fa-play2';
            }
            if (attachmentType == commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Pdf]) {
                return 'fa-file-pdf-o';
            }
            if (attachmentType == commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image]) {
                return 'fa-file-image-o';
            }
            return 'fa-file-o';
        }

        /**
         * @description Get S3 Folder by the attachment type.
         */
        private getFolderByAttachmentType = (attachmentType: string): commonInterfaces.S3Folders => {
            var folder: commonInterfaces.S3Folders;
            switch (attachmentType) {
                case commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Pdf].toString():
                    folder = commonInterfaces.S3Folders.file;
                    break;
                case commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Audio].toString():
                    folder = commonInterfaces.S3Folders.audio;
                    break;
                case commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Image].toString():
                    folder = commonInterfaces.S3Folders.image;
                    break;
                case commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Video].toString():
                    folder = commonInterfaces.S3Folders.video;
                    break;
                case commonInterfaces.ATTACHMENT_TYPE[commonInterfaces.ATTACHMENT_TYPE.Audio].toString():
                    folder = commonInterfaces.S3Folders.audio;
                    break;
                default:    
                    folder = commonInterfaces.S3Folders.file;
                    break;
            }

            return folder;
        }

        /**
         * @description Returns the attachment type.
         */
        private getAttachmentTypeByType = (fileType: string): commonInterfaces.ATTACHMENT_TYPE => {

            var typeParts = fileType.split('/');

            if (typeParts[0] === 'video') {
                return commonInterfaces.ATTACHMENT_TYPE.Video;
            }

            if (typeParts[0] === 'image') {
                return commonInterfaces.ATTACHMENT_TYPE.Image;
            }

            if (typeParts[0] === 'audio') {
                return commonInterfaces.ATTACHMENT_TYPE.Audio;
            }

            var pdfMimeTypes: Array<string> = ['pdf', 'x-pdf', 'acrobat', 'vnd.pdf', 'pdf', 'x-pdf'];
            if (pdfMimeTypes.indexOf(typeParts[1]) > -1) {
                return commonInterfaces.ATTACHMENT_TYPE.Pdf;
            }

            return commonInterfaces.ATTACHMENT_TYPE.File;

        }

        



    }

    angular.module('app').service(AwsSvc.id, AwsSvc);
}
