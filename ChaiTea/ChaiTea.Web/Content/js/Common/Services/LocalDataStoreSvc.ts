﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface ILocalDataStore {
        dt: Date;
        endPoint: string;
        data: any;
    }

    export interface ILocalDataStoreSvc {
        isSupported();
        getKey(endPoint: string, id?: any);
        getObject(key: string, time?: number);
        setObject(key: string, obj: any, canExpire?: boolean);
        removeObject(key: string);
        removeAll();
        save(endpoint: string, data: Object, images: any);
        sync();
        saveImage(file: any, id?: number);
        findImagesById(id: number);
    }

    class LocalDataStoreSvc implements angular.IServiceProvider {

        static $inject = [
            'localStorageService',
            '$q',
            'chaitea.common.services.utilssvc',
            'userContext'

        ];
        constructor(
            private localStorageService: ng.local.storage.ILocalStorageService,
            private $q: angular.IQService,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private userContext: IUserContext
            ) {

        }

        public $get(): ILocalDataStoreSvc {
            return {
                isSupported: () => { return this.isSupported(); },
                getKey: (endPoint: string, unique?: boolean, id?: any) => { return this.getKey(endPoint, id); },
                getObject: (key: string, time?: number) => { return this.getObject(key, time); },
                setObject: (key: string, obj: any, canExpire?: boolean) => { return this.setObject(key, obj, canExpire); },
                removeObject: (key: string) => { return this.removeObject(key); },
                removeAll: () => { return this.removeAll(); },
                save: (endpoint: string, data: Object, images: any) => { return this.save(endpoint, data, images); },
                sync: () => { return this.sync(); },
                saveImage: (file: any, id?: number) => { return this.saveImage(file, id); },
                findImagesById: (id: number) => { return this.findImagesById(id); }
            }
        }

        /**
         * @description Get an object by key.
         */
        private isSupported = (): boolean => {
            return this.localStorageService.isSupported;
        }

        /**
         * @description Get an object by key.
         */
        private getKey = (endPoint: string, id?: any): string => {
            var key: string = this.userContext.Client.ID.toString() + ':' + this.userContext.Site.ID.toString() + ':' + this.userContext.Program.ID.toString();
            return key + ':' + endPoint + (id ? ':' + id.toString() : '');

        }

        private getUniqueKey = (endPoint: string, id?: any): string => {
            var key = this.getKey(endPoint, id);
            return key + ':' + this.utilSvc.generateUniqueToken();
        }

        /**
         * @description Get an object by key.
         */
        private getObject = (key: string, time = 28800) => {

            var current: number = moment().unix();
            var expireSecs = time; //8 hrs 
            var data: any = this.localStorageService.get(key);

            if (_.has(data, 'dt') && _.has(data, 'data')) {

                //expire date is attached
                //chekci f still valid
                if ((current - data.dt) > expireSecs) {
                    this.localStorageService.remove(key);
                    data = null;
                }
            }

            return data;
        }

        /**
         * @description Set object in session storage.
         */
        private setObject = (key: string, obj: any, canExpire?: boolean) => {

            //attach expire date to cached obj
            if (canExpire) {
                obj = {
                    dt: moment().unix(),
                    data: obj
                };
            }

            //this.localStorageService.set(key, saveObj);
            this.localStorageService.set(key, obj);
        }

        /**
         * @description Remove object in session storage.
         */
        private removeObject = (key: string) => {
            this.localStorageService.remove(key);
        }

        /**
         * @description Remove object in session storage.
         */
        private removeAll = () => {
            this.localStorageService.clearAll();
        }


        private save = (endpoint: string, data: Object, images: any) => {

            var imageStrs: Array<string> = [];
            var processList = [];
            var id: string = this.utilSvc.generateUniqueToken();

            //process images
            angular.forEach(images,(i) => {
                processList.push(this.processImage(i));
            });

            return this.$q.all(processList).then((imgList) => {
                
                //store images
                angular.forEach(images,(i) => {

                });

                var saveObj: Object = {
                    dt: moment(),
                    endpoint: endpoint,
                    data: data,
                    images: imageStrs
                };

                this.setObject('sync-' + id, saveObj);

            });

        }

        private sync = (): angular.IPromise<any> => {

            var def = this.$q.defer();

            var keys: Array<string> = this.localStorageService.keys();


            //loop through keys and find any objs that need to be synced

            //if objects to sync

            //get aws creds

            //convert base64 to blob

            console.log(keys);

            return def.promise;

        }

        private saveImage = (file: any, id?: number): angular.IPromise<any> => {

            var def = this.$q.defer();
            var key: string = this.getUniqueKey('image', id);

            if (!file) return;

            this.processImage(file).then((imageUrl) => {

                this.setObject(key, imageUrl);
                var image: commonInterfaces.ILocalStorageImage = { Id: id, Key: key, Image: imageUrl };

                def.resolve(image);

            });

            return def.promise;

        }

        private findImagesById = (id: number): any => {

            var keys: any = this.localStorageService.keys();
            var images: Array<{ id: number; key: string; image: string; }> = [];
            angular.forEach(keys, (key) => {

                var obj: any = this.getObject(key);
                var lookUpId = this.getIdFromKey(key);

                if (key.indexOf('image') > -1 && (lookUpId == id)) {
                    images.push({ id: lookUpId, key: key, image: obj});
                }

            });

            console.log(images);

            return images;

        }

        private getIdFromKey = (key: string): any => {
            var parts: any = key.split(':');
            return parts[4];
        }

        private processImage = (file: any): angular.IPromise<any> => {

            var def = this.$q.defer();

            // generate a new FileReader object
            var reader = new FileReader();

            // inject an image with the src url
            reader.onload = function (event: any) {
                var url: string = event.target.result;
                def.resolve(url);
            }
 
            // when the file is read it triggers the onload event above.
            reader.readAsDataURL(file);

            return def.promise;

        }

    }

    angular.module('app').service('chaitea.common.services.localdatastoresvc', LocalDataStoreSvc);
}
