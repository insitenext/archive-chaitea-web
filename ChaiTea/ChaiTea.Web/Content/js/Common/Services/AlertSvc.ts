﻿/// <reference path="../../_libs.ts" />

/**
 * @description Wrapper service for alerts.
 */
module ChaiTea.Common.Services {
    'use strict';

    export interface IAlertSvc {
        alertWithCallback(message: string, callback?: () => void): void;
        alert(options: BootboxAlertOptions): void;
        confirmWithCallback(message: string, callback?: (result: boolean) => void): void;
        confirm(options: BootboxConfirmOptions): void;
        expiredAlert(): void;
    }

    export class AlertSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.alertsvc';
        static $inject = [];
        constructor() { }

        public $get(): IAlertSvc {
            return {
                alertWithCallback: (message, callback) => { return this.alertWithCallback(message, callback); },
                alert: (options) => { return this.alert(options); },
                confirmWithCallback: (message, callback) => { return this.confirmWithCallback(message, callback); },
                confirm: (options) => { return this.confirm(options); },
                expiredAlert: () => { return this.expiredAlert(); }
            }
        }

        alertWithCallback = (message: string, callback?: () => void) => {
            bootbox.alert(message, callback);
        }

        alert = (options: BootboxAlertOptions) => {
            bootbox.alert(options);
        }

        confirmWithCallback = (message: string, callback?: (result: boolean) => void) => {
            bootbox.confirm(message, callback);
        }

        confirm = (options: BootboxConfirmOptions) => {
            bootbox.confirm(options);
        }

        expiredAlert = () => {
            var self = this;
            var message = `
                <h4 class="padding-vertical-20 text-center">You have been signed out due to inactivity. </h4>
                <p class="text-center">For questions, please contact support@sbmcorp.com</p>
                <br/>
                <p class="text-center">
                    <small>You will be taken there automatically in less that 5 seconds</small>
                </p>
            `;
            var countdownTime = 5000;

            setTimeout(() => {
                self.redirectToLogin();
                return;
            }, countdownTime);

            bootbox.dialog(<BootboxDialogOptions>{
                message: message,
                callback: (res) => {
                    return self.redirectToLogin;
                },
                closeButton: false,
                buttons:
                {
                    done: {
                        label: 'Go to login',
                        callback: () => {
                            self.redirectToLogin();
                        }
                    }
                }
            });
        }

        redirectToLogin = () => {
            var href = SiteSettings.basePath + "Account/Login"
            return window.location.href = href;
        }
    }

    angular.module('app').service(AlertSvc.id, AlertSvc);
}
