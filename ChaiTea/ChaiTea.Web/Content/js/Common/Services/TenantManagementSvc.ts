﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for tenant management.
 */
module ChaiTea.Common.Services {
    'use strict';

    /**
     * @description Extend IResourceClass to include new implementation specific
     * to tenant management resource.
     */
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import commonSvc = ChaiTea.Common.Services;

    export interface ITenantManagementResource extends ng.resource.IResourceClass<any> {
        getOrgsByOData(params?: Object): ng.resource.IResource<any>;
        getByOData(params?: Object): ng.resource.IResource<any>;
        getAuditorsOData(params?: Object): ng.resource.IResource<any>;
        getAssignments(params?: Object): ng.resource.IResource<any>;
        updateAssignments(buildingId, programIds): ng.resource.IResource<any>;
        update(params?: Object): ng.resource.IResource<any>;
        getClientsById(params?: Object): ng.resource.IResource<any>;
    }

    export interface ISiteTenantManagementResource extends ng.resource.IResourceClass<any> {
        getByOData(params?: Object): ng.resource.IResource<any>;
        getAssignments(params?: Object): ng.resource.IResource<any>;
        updateAssignments(siteId, programIds): ng.resource.IResource<any>;
        update(params?: Object): ng.resource.IResource<any>;
    }

    export interface IFloorTenantManagementResource extends ng.resource.IResourceClass<any> {
        getByOData(params?: Object): ng.resource.IResource<any>;
        getAssignments(params?: Object): ng.resource.IResource<any>;
        updateAssignments(siteId, programIds): ng.resource.IResource<any>;
        update(params?: Object): ng.resource.IResource<any>;
    }

    export interface ITenantManagementSvc {
        getOrgsByOData(params?: Object): angular.IPromise<any>;

        getClientsByOData(params?: Object): angular.IPromise<any>;
        saveClient(params?: Object): angular.IPromise<any>;
        updateClient(id: number, params?: Object): angular.IPromise<any>;
        getClientsById(params?: Object): angular.IPromise<any>;

        getClientReportSettingsByOData(params?: Object): angular.IPromise<any>;
        saveClientReportSettings(params?: Object): angular.IPromise<any>;
        updateClientReportSettings(id: number, params?: Object): angular.IPromise<any>;

        getScoringProfiles(): angular.IPromise<any>;

        getSitesByOData(params?: Object): angular.IPromise<any>;
        getSiteProgramAssignments(siteId: number): angular.IPromise<any>;
        saveSite(params?: Object): angular.IPromise<any>;
        updateSite(id: number, params?: Object): angular.IPromise<any>;
        updateSiteProgramAssignment(id: number, programIds: Array<number>): angular.IPromise<any>;    

        getBuildingProgramAssignments(siteId: number): angular.IPromise<any>;
        getBuildingsByOData(params?: Object): angular.IPromise<any>;
        saveBuilding(params?: Object): angular.IPromise<any>;
        updateBuilding(id: number, params?: Object): angular.IPromise<any>;
        updateBuildingProgramAssignment(id: number, programIds: Array<number>): angular.IPromise<any>;        

        getFloorsByOData(params?: Object): angular.IPromise<any>;
        getFloorAreaAssignments(floorId: number): angular.IPromise<any>;
        saveFloor(params?: Object): angular.IPromise<any>;
        updateFloor(id: number, params?: Object): angular.IPromise<any>;
        updateFloorAreaAssignment(id: number, areaIds: Array<number>): angular.IPromise<any>;        

        getProgramsByOData(params?: Object): angular.IPromise<any>;
        saveProgram(params?: Object): angular.IPromise<any>;
        updateProgram(id: number, params?: Object): angular.IPromise<any>;

        getAreasByOData(params?: Object): angular.IPromise<any>;
        saveArea(params?: Object): angular.IPromise<any>;
        updateArea(id: number, params?: Object): angular.IPromise<any>;

        getUsers(params?: Object): angular.IPromise<any>;
        getAuditorsByOData(params?: Object): angular.IPromise<any>;
        saveUser(params?: Object): angular.IPromise<any>;
        updateUser(id: number, params?: Object): angular.IPromise<any>;

        getRolesByOData(params?: Object): angular.IPromise<any>;
        saveRole(params?: Object): angular.IPromise<any>;
        updateRole(id: number, params?: Object): angular.IPromise<any>;

        getAreaClassificationInspectionItems(areaClassificationId: number): angular.IPromise<any>;
    }

    class TenantManagementSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = [
            '$resource',
            'sitesettings',
            'chaitea.common.services.apibasesvc'
        ];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings,
            private apiSvc: commonSvc.IApiBaseSvc) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ITenantManagementSvc {
            return {
                getOrgsByOData: (params) => { return this.getOrgsByOData(params); },

                getClientsByOData: (params) => { return this.getClientsByOData(params); },
                saveClient: (params) => { return this.saveClient(params); },
                updateClient: (id, params) => { return this.updateClient(id, params); },
                getClientsById: (params) => { return this.getClientsById(params); },

                getClientReportSettingsByOData: (params) => { return this.getClientReportSettingsByOData(params); },
                saveClientReportSettings: (params) => { return this.saveClientReportSettings(params); },
                updateClientReportSettings: (id, params) => { return this.updateClientReportSettings(id, params); },

                getScoringProfiles: () => { return this.getScoringProfiles(); },

                getSitesByOData: (params) => { return this.getSitesByOData(params); },
                getSiteProgramAssignments: (siteId) => { return this.getSiteProgramAssignments(siteId); },
                updateSiteProgramAssignment: (siteId, programIds) => { return this.updateSiteProgramAssignment(siteId, programIds); },
                saveSite: (params) => { return this.saveSite(params); },
                updateSite: (id, params) => { return this.updateSite(id, params); },                        

                getBuildingProgramAssignments: (buildingId) => { return this.getBuildingProgramAssignments(buildingId); },
                getBuildingsByOData: (params) => { return this.getBuildingsByOData(params); },
                updateBuildingProgramAssignment: (buildingId, programIds) => { return this.updateBuildingProgramAssignment(buildingId, programIds); },
                saveBuilding: (params) => { return this.saveBuilding(params); },
                updateBuilding: (id, params) => { return this.updateBuilding(id, params); },                

                getFloorsByOData: (params) => { return this.getFloorsByOData(params); },
                getFloorAreaAssignments: (floorId) => { return this.getFloorAreaAssignments(floorId); },
                updateFloorAreaAssignment: (floorId, areaIds) => { return this.updateFloorAreaAssignment(floorId, areaIds); },
                saveFloor: (params) => { return this.saveFloor(params); },
                updateFloor: (id, params) => { return this.updateFloor(id, params); },                

                getProgramsByOData: (params) => { return this.getProgramsByOData(params); },
                saveProgram: (params) => { return this.saveProgram(params); },
                updateProgram: (id, params) => { return this.updateProgram(id, params); },

                getAreasByOData: (params) => { return this.getAreasByOData(params); },
                saveArea: (params) => { return this.saveArea(params); },
                updateArea: (id, params) => { return this.updateArea(id, params); },

                getUsers: (params) => { return this.getUsers(params); },
                getAuditorsByOData: (params) => { return this.getAuditorsByOData(params); },
                saveUser: (params) => { return this.saveUser(params); },
                updateUser: (id, params) => { return this.updateUser(id, params); },

                getRolesByOData: (params) => { return this.getRolesByOData(params); },
                saveRole: (params) => { return this.saveRole(params); },
                updateRole: (id, params) => { return this.updateRole(id, params); },

                getAreaClassificationInspectionItems: (areaClassificationId) => { return this.getAreaClassificationInspectionItems(areaClassificationId); },                
            }
        }

        //#region Orgs
        getOrgsByOData = (params?: Object): angular.IPromise<any> => {
            return this.clientResource().getOrgsByOData(params || {}).$promise;
        }        
        
        //#region Clients
        getClientsByOData = (params?: Object): angular.IPromise<any> => {
            return this.clientResource().getByOData(params || {}).$promise;
        }

        getClientsById = (params?: Object): angular.IPromise<any> => {
            return this.clientResource().getClientsById(params || {}).$promise;
        }

        saveClient = (params?: Object): angular.IPromise<any>=> {
            return this.clientResource().save(params || {}).$promise;
        }

        updateClient = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.clientResource().update(params).$promise;
        }
        //#endregion

        //#region ClientReportSettings
        getClientReportSettingsByOData = (params?: Object): angular.IPromise<any> => {
            return this.clientReportSettingsResource().getByOData(params || {}).$promise;
        }

        saveClientReportSettings = (params?: Object): angular.IPromise<any>=> {
            return this.clientReportSettingsResource().save(params || {}).$promise;
        }

        updateClientReportSettings = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.clientReportSettingsResource().update(params).$promise;
        }
        //#endregion

        //#region Lookups
        /**
         * Returns scoring profiles.
         */
        getScoringProfiles = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/ScoringProfiles').get().$promise;
        }

        //#endregion

        //#region Sites        
        
        getSitesByOData = (params?: Object): angular.IPromise<any> => {
            return this.siteResource().getByOData(params || {}).$promise;
        }

        getSiteProgramAssignments = (siteId: number): angular.IPromise<any> => {
            return this.siteResource().getAssignments({ siteId: siteId }).$promise;
        }

        updateSiteProgramAssignment = (siteId: number, programIds: Array<number>): angular.IPromise<any> => {
            return this.siteResource().updateAssignments({ siteId: siteId }, programIds).$promise;
        }

        saveSite = (params?: Object): angular.IPromise<any>=> {
            return this.siteResource().save(params || {}).$promise;
        }

        updateSite = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.siteResource().update(params).$promise;
        }
        //#endregion

        //#region Buildings
        getBuildingProgramAssignments = (buildingId: number): angular.IPromise<any> => {
            return this.buildingResource().getAssignments({ buildingId: buildingId }).$promise;
        }

        updateBuildingProgramAssignment = (buildingId: number, programIds: Array<number>): angular.IPromise<any> => {
            return this.buildingResource().updateAssignments({ buildingId: buildingId }, programIds).$promise;
        }

        getBuildingsByOData = (params?: Object): angular.IPromise<any> => {
            return this.buildingResource().getByOData(params || {}).$promise;
        }

        saveBuilding = (params?: Object): angular.IPromise<any>=> {
            return this.buildingResource().save(params || {}).$promise;
        }

        updateBuilding = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.buildingResource().update(params).$promise;
        }
        //#endregion

        //#region Floors
        getFloorsByOData = (params?: Object): angular.IPromise<any> => {
            return this.floorResource().getByOData(params || {}).$promise;
        }

        getFloorAreaAssignments = (floorId: number): angular.IPromise<any> => {
            return this.floorResource().getAssignments({ floorId: floorId }).$promise;
        }

        updateFloorAreaAssignment = (floorId: number, areaIds: Array<number>): angular.IPromise<any> => {
            return this.floorResource().updateAssignments({ floorId: floorId }, areaIds).$promise;
        }

        updateFloor = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.floorResource().update(params).$promise;
        }

        saveFloor = (params?: Object): angular.IPromise<any>=> {
            return this.floorResource().save(params || {}).$promise;
        }
        //#endregion

        //#region Programs
        getProgramsByOData = (params?: Object): angular.IPromise<any> => {
            return this.programResource().getByOData(params || {}).$promise;
        }

        updateProgram = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.programResource().update(params).$promise;
        }

        saveProgram = (params?: Object): angular.IPromise<any>=> {
            return this.programResource().save(params || {}).$promise;
        }           
        //#endregion

        //#region Areas
        getAreasByOData = (params?: Object): angular.IPromise<any> => {
            return this.areaResource().getByOData(params || {}).$promise;
        }

        updateArea = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.areaResource().update(params).$promise;
        }

        saveArea = (params?: Object): angular.IPromise<any>=> {
            return this.areaResource().save(params || {}).$promise;
        }
        //#endregion

        //#region Users
        getUsers = (params?: Object): angular.IPromise<any> => {
            return this.userResource().get(params || {}).$promise;
        }

        getAuditorsByOData = (params?: Object): angular.IPromise<any> => {
            return this.userResource().getAuditorsOData(params || {}).$promise;
        }

        updateUser = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.userResource().update(params).$promise;
        }

        saveUser = (params?: Object): angular.IPromise<any>=> {
            return this.userResource().save(params || {}).$promise;
        }
        //#endregion

        //#region Roles
        getRolesByOData = (params?: Object): angular.IPromise<any> => {
            return this.userRoleResource().getByOData(params || {}).$promise;
        }

        updateRole = (id: number, params?: Object): angular.IPromise<any>=> {
            params = angular.extend(params || {}, { id: id });
            return this.userRoleResource().update(params).$promise;
        }

        saveRole = (params?: Object): angular.IPromise<any>=> {
            return this.userRoleResource().save(params || {}).$promise;
        }    
        //#endregion

        /**
         * @description Resource object managing clients.
         */
        private clientResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/Client';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    },
                    getOrgsByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url + '/Orgs'
                    },
                    getClientsById: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: this.basePath + 'api/Services/ApplicationUser/Client'
                    }
                });
        }

        /**
        * @description Resource object managing client's report settings.
        */
        private clientReportSettingsResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/ReportSetting';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: false,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing sites.
          */
        private siteResource = (): ISiteTenantManagementResource => {
            var url = this.basePath + 'api/Services/Site';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var getProgramsBySiteAction: ng.resource.IActionDescriptor = {
                method: 'GET',
                //cache: false,
                isArray: true,
                params: { siteId: '@siteId' },
                url: `${url}/Programs/:siteId`
            };

            var updateProgramsBySiteAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false,
                params: { siteId: '@siteId' },
                url: `${url}/Programs/:siteId`
            };

            return <ISiteTenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getAssignments: getProgramsBySiteAction,
                    updateAssignments: updateProgramsBySiteAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing buildings.
          */
        private buildingResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/Building';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var getProgramsByBuildingAction: ng.resource.IActionDescriptor = {
                method: 'GET',
                isArray: true,
                params: { siteId: '@buildingId' },
                url: `${url}/Programs/:buildingId`
            };

            var updateProgramsByBuildingAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false,
                params: { buildingId: '@buildingId' },
                url: `${url}/Programs/:buildingId`
            };

            return <ITenantManagementResource>this.$resource(
                ` ${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getAssignments: getProgramsByBuildingAction,
                    updateAssignments: updateProgramsByBuildingAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing floors.
          */
        private floorResource = (): IFloorTenantManagementResource => {
            var url = this.basePath + 'api/Services/Floor';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var getAreasByFloorAction: ng.resource.IActionDescriptor = {
                method: 'GET',
                isArray: true,
                params: { floorId: '@floorId' },
                url: `${url}/Areas/:floorId`
            };

            var updateAreasByFloorAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false,
                params: { siteId: '@floorId' },
                url: `${url}/Areas/:floorId`
            };

            return <IFloorTenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    updateAssignments: updateAreasByFloorAction,
                    getAssignments: getAreasByFloorAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing programs.
          */
        private programResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/Program';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing areas.
          */
        private areaResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/Area';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        /**
          * @description Resource object managing app users.
          */
        private userResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/ApplicationUser';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    get: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    },
                    getAuditorsOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: ` ${url}/Auditors`
                    }
                });
        }

        /**
          * @description Resource object managing roles users.
          */
        private userRoleResource = (): ITenantManagementResource => {
            var url = this.basePath + 'api/Services/ApplicationRole';
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            return <ITenantManagementResource>this.$resource(
                `${url}/:id`, { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }

        getAreaClassificationInspectionItems = (areaClassificationId: number): angular.IPromise<any> => {
            var params = { areaClassificationId: areaClassificationId };
            return this.$resource(this.basePath + 'api/Services/LookupList/AreaClassificationInspectionItems/:areaClassificationId')
                .get(params).$promise;
        }       
    }

    angular.module('app').service('chaitea.common.services.tenantmanagementsvc', TenantManagementSvc);
} 