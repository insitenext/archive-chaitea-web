﻿/// <reference path="../../_libs.ts" />

/**
 * @description Wrapper service for alerts.
 */

module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;

    interface IUserResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    export interface IUserInfoSvc {
        getUserInfo(id: number, cache?: boolean);
        getUserInfoByOrgUserId(orgUserId: number):angular.IPromise<IUserInfo>;
        updateUserInfo(id: number, params?: any): angular.IPromise<any>;
    }

    enum EndPoint { Undefined};

    export class UserInfoSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.userinfosvc';
        private basePath: string;

        static $inject = [
            '$log',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.core.services.messagebussvc'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(
            private $log: angular.ILogService,
            sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private utilitySvc: Common.Services.IUtilitySvc,
            private messageBusSvc: Core.Services.IMessageBusSvc) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IUserInfoSvc {
            return {
                getUserInfo: (id, cache) => this.getUserInfo(id, cache),
                getUserInfoByOrgUserId: (orgUserId) => this.getUserInfoByOrgUserId(orgUserId),
                updateUserInfo: (id: number, params?: any) => this.updateUserInfo(id, params)
            };
        }

        /**
         * @description get user information and camelcases keys.
         */
        getUserInfo = (id: number, cache?: boolean): angular.IPromise<IUserInfo> => {
            return this.apiSvc.getById(id, 'UserInfo', cache == false ? false : true).then((res) => {
                return this.utilitySvc.mapKeysToCamelCase(res);
            }, this.handleFailure);
        }

        /**
         * @description get user information by OrgUserId;
         */
        getUserInfoByOrgUserId = (orgUserId: number): angular.IPromise<IUserInfo> => {
            return this.apiSvc.getById(orgUserId, "Employee/User").then((userInfo:IUserInfo) => {
                return this.utilitySvc.mapKeysToCamelCase(userInfo);
            },this.handleFailure);
        }

        /**
        * @description Updates a user Info.
        * @param {number} id
        * @param {Object} params
        */
        updateUserInfo = (id: number, params?: Object, lowerCaseResultKeys?:boolean): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });

            //Capitalize keys for saving
            params = _.mapKeys(params as any, (val, key:any) => {
                return _.capitalize(key);
            });
            
            return this.apiSvc.update(id, params, 'UserInfo', true).then((res) => {
                //Notify app to update any userInfo uses with latest data
                this.messageBusSvc.emitMessage(Common.Interfaces.MessageBusMessages.UserInfoUpdated, null);
                return lowerCaseResultKeys ? this.utilitySvc.mapKeysToCamelCase(res) : res;
            }, this.handleFailure);
        }

        handleFailure = (errMsg) => {
            this.$log.error(errMsg);
        }

    }
    angular.module('app').service(UserInfoSvc.id, UserInfoSvc);
}