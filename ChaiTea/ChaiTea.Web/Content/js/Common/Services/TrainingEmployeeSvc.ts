﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factory for training builder service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = Core.Services;
    import commonInterfaces = Common.Interfaces;
    import commonSvc = Common.Services;

    interface ITrainingEmployeeResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
        getList(Object): ng.resource.IResource<any>;
    }

    export interface ITrainingEmployeeSvc {
        getEmployeeList(params?: commonInterfaces.IODataPagingParamModel): angular.IPromise<any>;
        getPositionProfileById(id: number);
        getPositionProfileLocationAndRoute(id: number);
        updateTrainingEmployee(id: number, params?: any): angular.IPromise<any>;
    }

    enum EndPoint { Undefined, GetWithJob, GetEmployeeAreas };

    class TrainingEmployeeSvc implements angular.IServiceProvider {
        private basePath: string;


        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ITrainingEmployeeSvc {
            return {
                getEmployeeList: (params) => { return this.getEmployeeList(params); },
                getPositionProfileById: (id) => { return this.getPositionProfileById(id); },
                getPositionProfileLocationAndRoute: (id) => { return this.getPositionProfileLocationAndRoute(id); },
                updateTrainingEmployee: (id: number, params?: any) => (this.updateTrainingEmployee(id, params))
            };
        }

        /**
         * @description Get employees.
         */
        getEmployeeList = (params?: commonInterfaces.IODataPagingParamModel) => {
            return this.trainingEmployeeResource().getByOData(params).$promise;
        }

        /**
         * @description By Job Description by ID.
         */
        getPositionProfileById = (id: number) => {
            return this.trainingEmployeeResource(EndPoint.GetWithJob).get({ id: id }).$promise;
        }

        /**
         * @description By Job Description by ID.
         */
        getPositionProfileLocationAndRoute = (id: number) => {
            return this.trainingEmployeeResource(EndPoint.GetEmployeeAreas).getList({ id: id }).$promise;
        }

        /**
        * @description Updates a trainingEmployee.
        * @param {number} id
        * @param {Object} params
        */
        updateTrainingEmployee = (id: number, params?: Object): angular.IPromise<any> => {
            params = angular.extend(params || {}, { id: id });
            return this.trainingEmployeeResource().update(params).$promise;
        }

        /**
         * @description Resource object for training.
         */
        private trainingEmployeeResource = (endpoint?: EndPoint, isArray?: boolean): ITrainingEmployeeResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/TrainingEmployee/' + (endpoint ? EndPoint[endpoint].toString() + '/' : '');

            return <ITrainingEmployeeResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getList: { method: 'GET', isArray: true},
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }


    }

    angular.module('app').service('chaitea.common.services.trainingemployee', TrainingEmployeeSvc);
}