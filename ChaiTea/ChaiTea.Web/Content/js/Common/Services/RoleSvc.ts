﻿/// <reference path="../../_libs.ts" />

/**
 * @description A role provider to expose roles specific checks.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    class RoleService implements angular.IServiceProvider {
        private roles: string[];

        static $inject = [];
        constructor() {
            
        }

        public setRoles(roles) {
            this.roles = roles;
        }
       
        public $get() {
            return {
                getRoles: () => { return this.roles; }
            };
        }
    }

    angular.module('app').provider('roleSvc', RoleService);
}
 