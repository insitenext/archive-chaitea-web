﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Common.Services {
    'use strict'

    export interface ILookupListSvc {
        getQuickComments(params?: Object): angular.IPromise<any>;        
        getAreaClassifications(): angular.IPromise<any>;        
    }

    class LookupListSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {any} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ILookupListSvc {
            return {
                getQuickComments: (params) => { return this.getQuickComments(params); },                
                getAreaClassifications: () => {
                    return this.getAreaClassifications();
                }
            }
        }

        /**
         * Returns quick comments.
         * @param {Object} params
         */
        getQuickComments = (params?: Object): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/QuickComment').query(params).$promise;
        }

        getAreaClassifications = (): angular.IPromise<any> => {
            return this.$resource(this.basePath + 'api/Services/LookupList/AreaClassifications/')
                .get().$promise;
        }
    }

    angular.module('app').service('chaitea.common.services.lookuplistsvc', LookupListSvc);
}
