﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IApiBaseSvc {
        setEndPoint(endPoint: string): void;
        getResource(endPoint: string): ng.resource.IResourceClass<any>;
        get(endPoint?: string, cache?: boolean): ng.IPromise<any>;
        getById(id: number, endPoint?: string, cache?: boolean): ng.IPromise<any>;
        getByOdata(params?: Object, endPoint?: string, cache?: boolean, isArray?: boolean): ng.IPromise<any>;
        getLookupList(endPoint?: string, cache?: boolean): ng.IPromise<any>;
        query(params: Object, endPoint?: string, cache?: boolean, isArray?: boolean): ng.IPromise<any>;
        save(params: Object, endPoint?: string, cache?: boolean): ng.IPromise<any>;
        update(id: number, params?: Object, endPoint?: string, cache?: boolean): ng.IPromise<any>;
        updateAll(params: Object, endPoint?: string, cache?: boolean): ng.IPromise<any>;
        delete(id: number, endPoint?: string): ng.IPromise<any>;
    }

    interface IBaseResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
        updateAll(Object): ng.resource.IResource<any>;
        getLookupList(): ng.resource.IResource<any>;
    }

    export class ApiBaseSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.apibasesvc';
        private basePath: string;
        private errorMsg: string;
        public endPoint: string;

        static $inject = ['$q', '$resource', 'sitesettings', 'chaitea.common.services.localdatastoresvc'];
        constructor(
            private $q: angular.IQService,
            private $resource: ng.resource.IResourceService,
            private sitesettings: ChaiTea.ISiteSettings,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IApiBaseSvc {
            return {
                setEndPoint: (endPoint: string) => (this.setEndPoint(endPoint)),
                getResource: (endPoint: string) => (this.getResource(endPoint)),
                get: (endPoint?: string, cache?: boolean) => (this.get(endPoint, cache)),
                getById: (id: number, endPoint?: string, cache?: boolean) => (this.getById(id, endPoint, cache)),
                getByOdata: (params?: Object, endPoint?: string, cache?: boolean, isArray?: boolean) => (this.getByOdata(params, endPoint, cache, isArray)),
                getLookupList: (endPoint?: string, cache?: boolean) => (this.getLookupList(endPoint, cache)),
                query: (params: Object, endPoint?: string, cache?: boolean, isArray?: boolean) => (this.query(params, endPoint, cache, isArray)),
                save: (params: Object, endPoint?: string, cache?: boolean) => (this.save(params, endPoint, cache)),
                update: (id: number, params?: Object, endPoint?: string, cache?: boolean) => (this.update(id, params, endPoint, cache)),
                updateAll: (params: Object, endPoint?: string, cache?: boolean) => (this.updateAll(params, endPoint, cache)),
                delete: (id: number, endPoint?: string) => (this.delete(id, endPoint)),
            };
        }

        /**
         * @description Sets the endpoint for the resource.
         */
        private setEndPoint = (endPoint: string) => {
            this.endPoint = endPoint;
        }

        /**
         * @description Custom resource call.
         */
        private getResource = (endPoint: string) => {
            return this.baseResource(endPoint);
        }

        /**
         * @description Base getById crud service.
         */
        private getByOdata = (params?: Object, endPoint?: string, cache?: boolean, isArray?: boolean): any => {
            params = (params ? params : {});
            this.endPoint = (endPoint ? endPoint : this.endPoint);

            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);
                        var chachedObj: any = this.localStorageSvc.getObject(key);

                        if (chachedObj) {
                            resolve(chachedObj);
                        } else {
                            this.baseResource(endPoint, isArray).getByOData(params).$promise.then((res) => {

                                this.localStorageSvc.setObject(key, res);
                                resolve(res);

                            }, (err) => {
                                reject(err);
                            });
                        }

                    } else {
                        resolve(this.baseResource(endPoint, isArray).getByOData(params).$promise);
                    }

                }
            });
        }

        /**
         * @description Base getById crud service.
         */
        private getById = (id: number, endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint, id);
                        var cachedObj: any = this.localStorageSvc.getObject(key);

                        if (cachedObj) {
                            resolve(cachedObj);
                        } else {
                            this.baseResource(endPoint).get({ id: id }).$promise.then((res) => {

                                this.localStorageSvc.setObject(key, res);
                                resolve(res);

                            }, (err) => {
                                reject(err);
                            });
                        }

                    } else {
                        resolve(this.baseResource(endPoint).get({ id: id }).$promise);
                    }
                }
            });
        }

        /**
         * @description Base get crud service.
         */
        private get = (endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);
                        var cachedObj: any = this.localStorageSvc.getObject(key);

                        if (cachedObj) {
                            resolve(cachedObj);
                        } else {
                            this.baseResource(endPoint).query().$promise.then((res) => {

                                this.localStorageSvc.setObject(key, res);
                                resolve(res);

                            }, (err) => {
                                reject(err);
                            });
                        }

                    } else {
                        resolve(this.baseResource(endPoint).query().$promise);
                    }
                }
            });
        }

        /**
         * @description Base get crud service.
         */
        private getLookupList = (endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);
                        var chachedObj: any = this.localStorageSvc.getObject(key);

                        if (chachedObj) {
                            resolve(chachedObj);
                        } else {
                            this.baseResource(endPoint).getLookupList().$promise.then((res) => {

                                this.localStorageSvc.setObject(key, res);
                                resolve(res);

                            }, (err) => {
                                reject(err);
                            });
                        }
                    } else {
                        resolve(this.baseResource(endPoint).getLookupList().$promise);
                    }
                }
            });
        }

        /**
         * @description Base query crud service.
         */
        private query = (params, endPoint?: string, cache?: boolean, isArray?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);
                        var cachedObj: any = this.localStorageSvc.getObject(key);

                        if (cachedObj) {
                            resolve(cachedObj);
                        } else {
                            this.baseResource(endPoint, isArray).query(params).$promise.then((res) => {

                                this.localStorageSvc.setObject(key, res);
                                resolve(res);

                            }, (err) => {
                                reject(err);
                            });
                        }

                    } else {
                        resolve(this.baseResource(endPoint, isArray).query(params).$promise);
                    }
                }
            })
        }

        /**
         * @description Base update crud service.
         */
        private update = (id, params?, endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            params = angular.extend(params || {}, { id: id });
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint, id);

                        //store in localstorage incase of failure
                        this.localStorageSvc.setObject(key, params);

                        //try to update
                        this.baseResource(endPoint).update(params).$promise.then((res) => {
                            resolve(res);
                        }, (err) => {
                            reject(err);
                        });

                    } else {
                        resolve(this.baseResource(endPoint).update(params).$promise);
                    }
                }
            });
        }

        /**
         * @description Base update crud service.
         */
        private updateAll = (params, endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            params = params || {};
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);

                        //store in localstorage incase of failure
                        this.localStorageSvc.setObject(key, params);

                        //try to update
                        this.baseResource(endPoint).updateAll(params).$promise.then((res) => {
                            resolve(res);
                        },(err) => {
                            reject(err);
                        });

                    } else {
                        resolve(this.baseResource(endPoint).updateAll(params).$promise);
                    }
                }
            });
        }

        /**
         * @description Base save crud service.
         */
        private save = (params: Object, endPoint?: string, cache?: boolean): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    if (cache && this.localStorageSvc.isSupported()) {

                        var key: string = this.localStorageSvc.getKey(endPoint);

                        //store in localstorage incase of failure
                        this.localStorageSvc.setObject(key, params);

                        //try to save
                        this.baseResource(endPoint).save(params).$promise.then((res) => {
                            resolve(res);
                        },(err) => {
                            //store on localstorage on failure
                            resolve(err);
                        });
                    } else {
                        resolve(this.baseResource(endPoint).save(params).$promise);
                    }
                }
            });
        }

        /**
         * @description Base delete crud service.
         */
        private delete = (id: number, endPoint?: string): any => {
            this.endPoint = (endPoint ? endPoint : this.endPoint);
            return new this.$q((resolve, reject) => {
                if (!this.validate()) {
                    reject(this.errorMsg);
                } else {
                    resolve(this.baseResource(endPoint).delete({ id: id }).$promise);
                }
            });
        }

        /**
         * @description Checks if the resource has been setup.
         */
        private validate = (): boolean => {
            try {

                if (!this.endPoint) {
                    throw 'Endpoint required';
                }

                return true;

            } catch (error) {
                this.errorMsg = error;
                return false;
            }
        }

        /**
         * @description Base resource method.
         */
        private baseResource = (endPoint?: string, isArray: boolean = true): IBaseResource => {

            var url = this.basePath + 'api/Services/' + (endPoint ? endPoint : this.endPoint) + '/';

            return <IBaseResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: {
                        method: 'PUT',
                        isArray: false
                    },
                    updateAll: {
                        method: 'PUT',
                        isArray: true
                    },
                    getLookupList: {
                        method: 'GET',
                        isArray: false
                    },
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: isArray,
                        url: url,
                        params: { skip: 0 }
                    },
                    query: {
                        isArray: isArray
                    },
                });
        }

    }


    angular.module('app').service(ApiBaseSvc.id, ApiBaseSvc);
}  