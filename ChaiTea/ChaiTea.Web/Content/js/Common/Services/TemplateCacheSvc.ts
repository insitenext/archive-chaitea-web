﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {

    export interface ITemplateCacheSvc {
        cacheMultipleTemplates(templatePaths:Array<string>): void;
    }

    class TemplateCacheSvc implements angular.IServiceProvider {

        static $inject = ['$log','$templateCache','$templateRequest'];

        constructor(
            private $log: angular.ILogService,
            private $templateCache: angular.ITemplateCacheService,
            private $templateRequest: angular.ITemplateRequestService) { }

        public $get(): ITemplateCacheSvc {
            return {
                cacheMultipleTemplates: (templatePaths: Array<string>) => (this.cacheMultipleTemplates(templatePaths))
            }
        }
        
        private cacheMultipleTemplates = (templatePathsname: Array<string>) => {
            angular.forEach(templatePathsname, (path) => {
                this.$templateRequest(path);  
            });
        }
    }


    angular.module('app').service('chaitea.common.services.templatecachesvc', TemplateCacheSvc);
}