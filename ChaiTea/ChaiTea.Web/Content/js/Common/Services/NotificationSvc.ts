﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;

    export interface INotificationSvc {
        initHub(hubName?: string, clientId?: number, userId?: number, orgUserId?: number);
        notifyAll(message: INotificationMessage);
        notifyAllOnClient(clientId: number, message: INotificationMessage);

        displayToast(notification: INotificationMessage, options?: Object);
        displaySimpleToast(message: string);

        successToastMessage(message: string);
        infoToastMessage(message: string);
        errorToastMessage(message?: string);
        warningToastMessage(message: string);
        expiredAlert();
    }

    export interface INotificationMessage {
        type: NOTIFICATION_TYPE;     //MESSAGE or ACTION
        display?: string; //info,warning,success,error
        message: string;
        title?: string;
        link?: string;
    }

    export enum NOTIFICATION_TYPE {
        MESSAGE,
        ACTION
    }

    export class NotificationSvc implements ng.IServiceProvider {
        static id = 'chaitea.common.services.notificationsvc';
        signalRPort = 444;

        notificationHub;
        
        isConnected: boolean = false;
        notificationEventName = "notification:";


        static $inject = [
            '$log',
            'Hub',
            'chaitea.common.services.utilssvc',
            'chaitea.core.services.messagebussvc',
            'sitesettings',
            'ZendeskWidget'];

        constructor(
            private $log: angular.ILogService,
            private Hub: any,
            private UtilitySvc: commonSvc.IUtilitySvc,
            private MessageBus: coreSvc.IMessageBusSvc,
            private sitesettings: ISiteSettings,
            private ZendeskWidget: any) {
        }

        public $get(): INotificationSvc {
            return {
                initHub: (hubName?: string, clientId?: number) => {
                    return this.initHub(hubName, clientId)
                },
                notifyAll: (message: INotificationMessage) => (this.notifyAll(message)),
                notifyAllOnClient: (clientId: number, message: INotificationMessage) => (this.notifyAllOnClient(clientId, message)),
                displayToast: (notification: INotificationMessage, options: Object) => (this.displayToast(notification, options)),
                displaySimpleToast: (message: string) => (this.displaySimpleToast(message)),
                successToastMessage: (message: string) => (this.successToastMessage(message)),
                infoToastMessage: (message: string) => (this.infoToastMessage(message)),
                errorToastMessage: (message?: string) => (this.errorToastMessage(message)),
                warningToastMessage: (message: string) => (this.warningToastMessage(message)),
                expiredAlert: () => (this.expiredAlert())
            }
        }

        initHub = (hubName: string = "notification", clientId: number = 0, userId: number = 0, orgUserId: number = 0) => {
            /*
            var self = this;
            var listeners: any = {};
            listeners.HandleIncomingBroadcastNotification = function (message) {
                self.handleBroadcast(message);
            }

            if (clientId > 0) {
                listeners.HandleIncomingClientNotification = function (message) {
                    self.handleBroadcast(message, clientId);
                }
            }

            if (userId > 0) {
                listeners.HandleIncomingUserNotification = function (message) {
                    self.handleUserMessage(message);
                }
            }

            if (orgUserId > 0) {
                listeners.HandleIncomingOrgUserNotification = function (message) {
                    self.handleOrgUserMessage(message);
                }
            }

            this.notificationHub = new this.Hub(hubName, {
                pingInterval: null,
                rootPath: this.sitesettings.fullDomain +
                    ((!this.sitesettings.isLocalhost) ? `:${this.signalRPort}`: "") +
                    this.sitesettings.basePath + "signalr",
                listeners: listeners,
                errorHandler: (error) => {
                    this.$log.error('Notification Hub Error', error);
                },
                methods: ['NotifyAll', 'NotifyClientUsers', 'RegisterForNotifications', 'UnregisterForNotifications'],
                stateChanged: (state) => {
                    switch (state.newState) {
                        case $.signalR.connectionState.connecting:
                            this.isConnected = false;
                            break;
                        case $.signalR.connectionState.connected:
                            this.$log.info('connected');
                            this.notificationHub.RegisterForNotifications();
                            this.isConnected = true;
                            break;
                        case $.signalR.connectionState.reconnecting:
                            this.$log.info('reconnecting')
                            this.isConnected = false;
                            break;
                        case $.signalR.connectionState.disconnected:
                            this.$log.info('disconnected');
                            this.notificationHub.UnregisterForNotifications();
                            this.isConnected = false;
                            break;
                        default:
                            break;
                    }
                }
            });
            */
        }

        notifyAll = (notification: INotificationMessage) => {
            if (!this.isConnected || !notification) return false;

            this.notificationHub.NotifyAll(JSON.stringify(notification));
        }

        notifyAllOnClient = (clientId: number, notification: INotificationMessage) => {
            if (!this.isConnected || clientId <= 0 || !notification) return false;

            this.notificationHub.NotifyClientUsers(clientId, JSON.stringify(notification));
        }

        //message json handle
        private handleBroadcast = (message, clientId = null) => {
            if (this.UtilitySvc.isJSONString(message)) {
                message = angular.fromJson(message);
                var obj = <INotificationMessage>message;
                switch (obj.type) {
                    case NOTIFICATION_TYPE.ACTION:
                        this.emitNotificationMessage(obj.title, obj);
                        break;
                    case NOTIFICATION_TYPE.MESSAGE:
                        this.displayToast(obj);
                        break;
                    default:
                        this.displayToast(obj);
                        break;
                }
            }
            else if (angular.isString(message)) {
                this.displaySimpleToast(<string>message);
                return;
            }

        }

        private handleUserMessage = (message) => {
            this.handleOrgUserMessage(message);
        }

        private handleOrgUserMessage = (message) => {
            if (message == 'ActivityFeed') {
                this.emitNotificationMessage('activity-feed', message);
            } else if (message == 'Todo') {
                this.emitNotificationMessage('todo', message);
            } else {
                this.displaySimpleToast(message);
            }
        }

        emitNotificationMessage = (eventName, obj) => {
            this.MessageBus.emitMessage(this.notificationEventName+eventName, obj);
        }

        private handleClientBroadcast = (clientId, message) => {
            if (this.UtilitySvc.isJSONString(message)) {
                message = angular.fromJson(message);
            }
        }

        public displayToast = (notification: INotificationMessage, options: Object = {}) => {
            toastr[
                notification.display || "info"
            ](
                notification.message || "",
                notification.title || "",
                options || {}
                );
        }

        public displaySimpleToast = (message: string) => {
            toastr.info(message);
        }

        public successToastMessage = (message: string) => {
            toastr.success(message);
        }

        public infoToastMessage = (message: string) => {
            toastr.info(message);
        }

        public errorToastMessage = (message?: string) => {
            if (!message) {
                message = "We're sorry there seems to have been an issue.";
            }
            // Add Zendesk widget if it is loaded.
            // using onclick() due to toastr message not being in angular scope
            if (this.sitesettings.isProd && _.isObject(window["zE"])) {
                message = message + `<div id="alertReportButton" class="btn btn-xs btn-danger btn-clear pull-right margin-right-20">Report this</div>`;
            }

            toastr.error(message);

            //bind click even on ReportButton
            angular.element('#alertReportButton').on('click', () => {
                this.MessageBus.emitMessage(Common.Interfaces.MessageBusMessages.OpenFeedbackWidget, null);
            });
        }

        public warningToastMessage = (message: string) => {
            toastr.warning(message);
        }

        public expiredAlert = () => {
            var self = this;
            var message = `
                <h4 class="padding-vertical-20 text-center">You have been signed out due to inactivity. </h4>
                <p class="text-center">For questions, please contact support@sbmcorp.com</p>
                <br/>
                <p class="text-center">
                    <small>You will be taken there automatically in less that 5 seconds</small>
                </p>
            `;
            var countdownTime = 5000;

            setTimeout(() => {
                self.logOutUser();
                return;
            }, countdownTime);

            bootbox.dialog(<BootboxDialogOptions>{
                message: message,
                callback: (res) => {
                    return self.logOutUser();
                },
                closeButton: false,
                buttons:
                {
                    done: {
                        label: 'Go to login',
                        callback: () => {
                            self.logOutUser();
                        }
                    }
                }
            });
        }

        private logOutUser = () => {
            var logoutForm = angular.element('#logoutForm');

            if (logoutForm) {
                this.MessageBus.emitMessage('sessionStorage:clear-date', true);
                logoutForm.submit();
            }
        }

    }

    angular.module('app').service(NotificationSvc.id, NotificationSvc);
} 