﻿/// <reference path="../../_libs.ts" />

/**
 * @description Amazon AWS service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    enum ImageExts { jpg, png, gif };

    export interface IImageSvc {
        
        get(key: string): any;
        getByKey(key: string): any;
        getFromAttachments(attachments: any[], attachmentType?: ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean): any;
        getUserImageFromAttachments(attachments: any[], getThumbnail?: boolean): any;
        getAllImagesFromAttachmentsByType(attachments: any[], attachmentType: Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean): { uniqueId   : number, src: string }[];
        getSocialImagesFromAttachments(attachments: any[]): any[];
        save(file: any, createProfileThumbnail?: boolean, isBinary?: boolean, fileNameOverride?: string): any;
        readImage(file: any): any;
        getThumbnailName(guid: string): any;
        download(key: string): angular.IPromise<any>;
        rotate(key: string, degree: number): angular.IPromise<any>;

    }

    export class ImageSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.imagesvc';     

        defaultImage: string = '';
        //thumbnailSizes: Array<number> = [75, 150, 300];
        thumbnailSize: number = 300;
        maxWidth: number = 800;
        maxHeight: number = 800;

        static $inject = [
            '$log',
            '$q',
            '$timeout',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.lookuplist',
            'chaitea.services.credentials',
            'chaitea.common.services.utilssvc',
            'awsconfig',
            'chaitea.common.services.awssvc'

        ];
        constructor(
            private $log: ng.ILogService,
            private $q: ng.IQService,
            private $timeout: ng.ITimeoutService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private lookupListSvc: ChaiTea.Services.ILookupListSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private awsConfig: commonInterfaces.IEndpoint,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc) {

            this.defaultImage = sitesettings.basePath + 'Content/img/user.jpg';
        }

        public $get(): IImageSvc {
            return {
                get: (key: string) => { return this.get(key); },
                getByKey: (key: string) => { return this.getByKey(key); },
                getFromAttachments: (attachments: any[], attachmentType?: ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean) => { return this.getFromAttachments(attachments, attachmentType, getThumbnail); },
                getUserImageFromAttachments: (attachments: any[], getThumbnail?: boolean) => { return this.getUserImageFromAttachments(attachments, getThumbnail); },
                getAllImagesFromAttachmentsByType: (attachments: any[], attachmentType?: Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean) => (this.getAllImagesFromAttachmentsByType(attachments, attachmentType, getThumbnail)),
                getSocialImagesFromAttachments: (attachments: any[]) => this.getSocialImagesFromAttachments(attachments),
                save: (file: any, createProfileThumbnail?: boolean, isBinary?: boolean, fileNameOverride?: string) => { return this.save(file, createProfileThumbnail, isBinary, fileNameOverride); },
                readImage: (file: any) => { return this.readImage(file); },
                getThumbnailName: (guid: string) => { return this.getThumbnailName(guid); },
                download: (key: string) => { return this.download(key); },
                rotate: (key: string, degree: number) => { return this.rotate(key, degree); }
            }
        }

        /**
         * @description Retrieve image from server
         */
        private get = (key: string): any => {

            var partsName = key.split('/');
            if (partsName.length > 1) {
                //all older files are jpgs migrated from cloudinary
                key = key + '_t.' + ImageExts[ImageExts.jpg];
            }
            
            return this.awsSvc.getUrlByName(key, commonInterfaces.S3Folders.image);

        } 

        /**
         * @description Retrieve image from server by key
         */
        private getByKey = (key: string): any => {
            return this.awsSvc.getUrl(key);
        }

        /**
         * @description Retrieve the profile image from the server.
         */
        private getFromAttachments = (attachments: any[], attachmentType?: ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean): any => {
            
            var userImg: string = this.defaultImage;

            angular.forEach(attachments, (attachment) => {
                if (attachment.UserAttachmentType === ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES[attachmentType].toString()) {

                    userImg = this.getImageByUniqueId(attachment.UniqueId, getThumbnail);

                }
            });

            return userImg;
            
        }

        /**
         * @description Retrieve the profile image from the server.
         */
        private getUserImageFromAttachments = (attachments: any[], getThumbnail?: boolean): any => {
            
            var userImg: string = this.defaultImage;

            angular.forEach(attachments, (attachment) => {

                if (attachment.UserAttachmentType === ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES[ChaiTea.Common.Interfaces.ALL_ATTACHMENT_TYPES.ProfilePicture].toString()) {
                    
                    userImg = this.getImageByUniqueId(attachment.UniqueId, getThumbnail);

                }
            });

            return userImg;

        }

        /**
         * @description Retrieves social images from the attachments array.
         */
        private getSocialImagesFromAttachments = (attachments: any[]) => {
            let socialImages = [];
            var images = this.getAllImagesFromAttachmentsByType(
                attachments,
                Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture,
                false);

            return images;
        }

        /**
         * @description Retrieve the Attachment image by type from the server.
         */
        private getAllImagesFromAttachmentsByType = (attachments: any[], attachmentType: Common.Interfaces.ALL_ATTACHMENT_TYPES, getThumbnail?: boolean): { uniqueId: number, src: string }[] => {
            var images: { uniqueId: number, src: string, description: string }[] = [];
            angular.forEach(attachments, (attachment) => {
                if (attachment.UserAttachmentType === Common.Interfaces.ALL_ATTACHMENT_TYPES[attachmentType].toString()) {
                    images.push({ src: this.getImageByUniqueId(attachment.UniqueId, getThumbnail), uniqueId: attachment.UniqueId, description: attachment.Description });
                }
            });
            return images;
        }

        /**
         * @description Retrieves image from the server.
         */
        private getImageByUniqueId = (uniqueId: string, getThumbnail: boolean): string => {

            var key: string = uniqueId;
            var parts = uniqueId.split('.');

            var partsName = key.split('/');
            if (partsName.length > 1) {
                //all older files are jpgs migrated from cloudinary
                key = (getThumbnail ? parts[0] + '_t.' + ImageExts[ImageExts.jpg] : key);
            } else {
                key = (getThumbnail ? parts[0] + '_t.' + parts[1] : key);
            }

            return this.awsSvc.getUrlByName(key, commonInterfaces.S3Folders.image);

        }

        /**
         * @description Save image to server.
         */
        private save = (file: any, createThumbnail?: boolean, isBinary?: any, fileNameOverride?: string): angular.IPromise<any> => {
            
            var def = this.$q.defer();
            var useFileReader: boolean = true;

            try {

                if (!file) throw "file required";
                
                //auth
                this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {
                    
                    if (!creds.AccessKey || !creds.AccessSecret) def.reject('not authorized');

                    async.waterfall([
                        (callback) => {

                            //read image to create base64
                            //only needed for thumbnails

                            if (isBinary) {
                                callback(null, file);
                            } else {

                                if (createThumbnail) {

                                    this.readImage(file).then((imageData) => {
                                        callback(null, imageData.dataUrl);
                                    }, (error) => {
                                        callback(error, null);
                                    });

                                } else {
                                    useFileReader = false;
                                    callback(null, file);

                                }
                            }

                        },
                        (imageData: any, callback) => {

                             //create thumbnail of image

                            if (!createThumbnail) {
                                callback(null, imageData, null, fileNameOverride);
                            } else {
                                this.cropImage(imageData, this.thumbnailSize, this.thumbnailSize).then((croppedDataUrl) => {

                                    fileNameOverride = (fileNameOverride ? fileNameOverride : this.utilSvc.generateUniqueToken());

                                    this.awsSvc.s3Upload(croppedDataUrl, creds, commonInterfaces.S3Folders.image, commonInterfaces.S3ACL.publicRead, true, this.getThumbnailName(fileNameOverride)).then((data: commonInterfaces.IAwsObjectDetails) => {

                                        callback(null, imageData, data, fileNameOverride);

                                    }, (error: any) => {
                                        //throw error
                                        callback(error, null);
                                    }, (progress: any) => {
                                        //update progress bar
                                    });

                                }, (error) => {
                                    callback(error, null);
                                });
                            }

                        },
                        (imageData: any, thumbnail: commonInterfaces.IAwsObjectDetails, fileNameOverride: string, callback) => {

                            //upload image to s3

                            this.awsSvc.s3Upload(imageData, creds, commonInterfaces.S3Folders.image, commonInterfaces.S3ACL.publicRead, useFileReader, fileNameOverride).then((data: commonInterfaces.IAwsObjectDetails) => {

                                if (createThumbnail) {
                                    data.Thumbnail = thumbnail.Url;
                                }

                                callback(null, data);

                            }, (error: any) => {
                                //throw error
                                callback(error, null);
                            }, (progress: any) => {
                                //update progress bar
                            });
                        }
                    ], (err, res) => {
                        
                        if (err) {
                            this.$log.error(err);
                            def.reject(err);
                        }

                        def.resolve(res);

                    });


                });

            } catch (error) {
                this.$log.error(error)
                def.reject(error);
            }

            return def.promise;

        } 

        /**
         * @description Returns standard name for thumbnail.
         */
        private getThumbnailName = (guid: string): string => {
            var ext: string = '_t';
            return guid + ext;
        }

        /**
         * @description Convert image to dataUrl.
         */
        private readImage = (file: any): angular.IPromise<any> => {
            var def = this.$q.defer();
            var imageData: any = {
                width: 0,
                height: 0,
                dataUrl: ''
            };

            if (FileReader) {
                var fr = new FileReader();
                fr.onload = function () {
                    
                    var img = new Image;

                    img.onload = function () {

                        imageData.width = img.width;
                        imageData.height = img.height;
                        imageData.dataUrl = fr.result;

                        def.resolve(imageData);

                    };

                    img.src = fr.result;    

                }
                fr.readAsDataURL(file);
            }

            return def.promise;


        }

        /**
         * @description Get the coordinates for the face.
         */
        private getFaceCordinates = (dataUrl: any): angular.IPromise<any> => {
            var def = this.$q.defer();

            angular.element(document.body).append('<img id="image-render-img" style="display: none" />');

            var img: any = angular.element('#image-render-img')[0];
            var imageObj = new Image();
            var tracker: any = new tracking.ObjectTracker(['face']);

            tracker.setStepSize(1.7);
            img.src = dataUrl;

            tracking.track('#image-render-img', tracker);
            tracker.on('track', function (event) {

                if (!event.data[0]) {
                    def.reject('Face not detected');
                } else {
                    def.resolve(event.data[0]);
                }

            });

            return def.promise;

        }

        /**
         * @description Draw image on canvas and crop.
         */
        private cropImage = (imageData: any, width: number, height: number): angular.IPromise<any> => {
            var def = this.$q.defer();

            angular.element(document.body).append('<canvas id="image-render-canvas" />');

            var canvas: any = angular.element('#image-render-canvas')[0];
            var context = canvas.getContext('2d');
            var imageObj = new Image();
            var imgSrc: any = '';

            //draw to canvas on load
            imageObj.onload = function () {

                canvas.width = width;
                canvas.height = height;
                context.drawImage(canvas, 0, 0, width, height);

                imgSrc = canvas.toDataURL("image/png");

                def.resolve(imgSrc);

            };

            //load image data
            imageObj.src = imageData.dataUrl;

            return def.promise;
        }

        /**
         * @description Download image from server.
         */
        private download = (key: string): angular.IPromise<any> => {

            var def = this.$q.defer();

            //auth
            this.credentialSvc.getAwsCredentials().then((creds: commonInterfaces.ISecurity) => {

                this.awsSvc.s3Download(key, creds).then((data: any) => {

                    console.log('image', data);

                }, (error) => {
                    console.log('imagearrow', error);
                });

            });


            return def.promise;
            

        }

        private rotate = (key: string, degree: number): angular.IPromise<any> => {

            var def = this.$q.defer();

            //auth
            var url = 'attachment/' + key + '/rotate/' + degree;
            this.apiSvc.query({}, url, false).then((res: any) => {

                def.resolve(res);

            });

            return def.promise;


        }

    }

    angular.module('app').service(ImageSvc.id, ImageSvc);
}