﻿/// <reference path="../../_libs.ts" />

/**
 * @description Send email service and mail delivery factory.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IMailSvc {
        sendMail(mailMessage: commonInterfaces.IMailMessage): angular.IPromise<any>;
        openForm(mailType: string, resolveObj?: Object);
    }

    class MailSvc implements angular.IServiceProvider {
        modalInstance;
        basePath: string;
        tag: string = 'sbminsite';

        static $inject = ['$q', '$http', '$uibModal', 'sitesettings'];
        constructor(
            private $q: angular.IQService,
            private $http: angular.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal;
        }

        public $get(): IMailSvc {
            return {
                sendMail: (mailMessage) => { return this.sendMail(mailMessage); },
                openForm: (mailType, resolveObj?) => { return this.openForm(mailType, resolveObj); }
            }
        }

        /**
         * @description Sends the email and return a promise object.
         */
        sendMail = (mailMessage: commonInterfaces.IMailMessage): angular.IPromise<any> => {         
            var url = `${this.basePath}api/Services/SendMail`;
            var params = {
                FromName: mailMessage.FromName,
                FromEmail: mailMessage.FromEmail,
                ToName: mailMessage.ToName,
                ToEmail: mailMessage.ToEmail,
                Subject: mailMessage.Subject,
                Message: mailMessage.Message,
                Tag: mailMessage.Tag? mailMessage.Tag: this.tag
            }

            return this.$http.post(url, params);
        }

        /**
         * @description Instantiate a modal instance and provide an interface to send an email based on a mail type.
         * @param {string} mailType - pre-defined mail types
         * @param {object} resolveObj - any object dependency to be resolved
         */
        openForm = (mailType: string, resolveObj?: Object) => {
            var mailForms = {
                'survey': { templateUrl: this.basePath + 'Content/js/Common/Templates/modal.mailSurvey.tmpl.html', controller: 'Common.MailModalSurveyCtrl as vm' }
            };

            this.modalInstance = this.$uibModal.open({
                templateUrl: mailForms[mailType].templateUrl,
                controller: mailForms[mailType].controller,
                size: 'md',
                resolve: {
                    paramsforCloud: () => { return resolveObj; }
                }
            });

            return this.modalInstance.result;
        }
    }

    angular.module('app').service('chaitea.common.services.mailsvc', MailSvc);
}
