﻿/// <reference path="../../_libs.ts" />

/**
 * @description Wrapper service for alerts.
 */

module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;

    interface ICultureResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
    }

    export interface ICultureSvc {
        getCultures();
    }

    class CultureSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ICultureSvc {
            return {
                getCultures: () => { return this.getCultures(); }
            };
        }

        /**
         * @description gets list of cultures.
         */
        getCultures = () => {
            return this.cultureResource().getByOData({}).$promise;
        }

        /**
         * @description Resource object for userinfo.
         */
        private cultureResource = (): ICultureResource => {
            var url = this.basePath + 'api/Services/Culture/';

            return <ICultureResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }
    }
    angular.module('app').service('chaitea.common.services.culturesvc', CultureSvc);
}