﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Services {
    'use strict';

    export interface IFileUploadSvc {
        upload(file, url: string, params, $event, $progressElem, fileName);
    }

    class FileUploadSvc implements angular.IServiceProvider {
        fileUpload;

        static $inject = ['$q', 'Upload'];
        constructor(
            private $q: angular.IQService,
            private Upload) { }

        public $get(): IFileUploadSvc {
            return {
                upload: (file, url, params, $event, $progressElem, fileName) => { return this.upload(file, url, params, $event, $progressElem, fileName); }
            }
        }

        /**
         * @description Upload file and return a promise object.
         */
        upload = (file, url, params, $event, $progressElem, fileName): angular.IPromise<any> => {
            var that = this;
            var progressElement = angular.element($progressElem);
            return new this.$q(function (resolve, reject) {
                that.fileUpload = that.Upload.upload({
                    url: url,
                    fields: params,
                    file: file,
                    transformRequest: angular.identity,
                    fileName: fileName || file.name
                }).progress(function (e) {
                    if (!progressElement) return;
                    progressElement
                            .css('width', 100.0 * e.loaded / e.total + '%');

                    }).success(function (data, status, headers, config) {
                        var publicId = data.public_id;

                        resolve(data);
                    });
            });
        }
    }

    angular.module('app').service('chaitea.common.services.fileuploadsvc', FileUploadSvc);
}
