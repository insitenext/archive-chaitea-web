﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factory for report helper service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonSvc = Services;
    import commonInterfaces = Common.Interfaces;

    export interface IReportSvc {
        getReportSettings(): angular.IPromise<commonInterfaces.IReportSettings>;
        getAuditSettings(clientId: any, siteId?: any, cascasdeClient?: boolean): angular.IPromise<commonInterfaces.IAuditSettings>;
        getAllSitesCombinedSiteReportSettings(): angular.IPromise<commonInterfaces.IReportSettings>;
        getTickIntervalByScale(scaleMax): number;
        clearLocalSettings();
    }

    class ReportSvc implements angular.IServiceProvider {
        private basePath: string;
        private clientId: number = 0;
        private storeKey: string = '';

        static $inject = [
            '$q',
            '$resource',
            '$log',
            'sitesettings',
            'userContext',
            'chaitea.common.services.alertsvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.apibasesvc'
        ];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(
            private $q: angular.IQService,
            private $resource: ng.resource.IResourceService,
            private $log: ng.ILogService,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private alertSvc: commonSvc.IAlertSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

            this.basePath = sitesettings.basePath;
            this.clientId = userContext.Client.ID;

            this.storeKey = `report.setting-${this.clientId.toString()}`;
        }

        public $get(): IReportSvc {
            return {
                getReportSettings: () => { return this.getReportSettings(); },
                getAuditSettings: (clientId: any, siteId?: any, cascasdeClient?: boolean) => (this.getAuditSettings(clientId, siteId, cascasdeClient)),
                getAllSitesCombinedSiteReportSettings: () => (this.getAllSitesCombinedSiteReportSettings()),
                getTickIntervalByScale: (scaleMax) => { return this.getTickIntervalByScale(scaleMax); },
                clearLocalSettings: () => { }
            };
        }

        /**
         * @description Pre-set the report settings during the run phase.
         */
        private setReportSettings = (obj: commonInterfaces.IReportSettings) => {
            this.localDataStoreSvc.setObject(this.storeKey, obj);
        }

        /**
         * @description Return the report settings from the localStorage or resolve via the resource object if not available.
         */
        private getReportSettings = (): angular.IPromise<commonInterfaces.IReportSettings> => {

            var def = this.$q.defer();

            var url = ChaiTea.Services.Interfaces.ApiEndpoints[ChaiTea.Services.Interfaces.ApiEndpoints.ReportSetting];
            url += '/' + this.userContext.Client.ID + '/' + this.userContext.Site.ID + '/' + this.userContext.Program.ID;

            this.apiSvc.getByOdata({}, url, false, false).then((res: commonInterfaces.IReportSettingsAll) => {

                if (res.ProgramReportSettings) {

                    def.resolve(res.ProgramReportSettings);

                } else if (res.SiteReportSettings) {

                    def.resolve(res.SiteReportSettings);

                } else {

                    if (res.ClientReportSettings == null) {
                        this.apiSvc.getByOdata({}, url, false, false).then((res: commonInterfaces.IReportSettingsAll) => {
                            if (res.ProgramReportSettings) {
                                def.resolve(res.ProgramReportSettings);
                            } else if (res.SiteReportSettings) {
                                def.resolve(res.SiteReportSettings);
                            } else {
                                def.resolve(res.ClientReportSettings);
                            }
                        });
                    }
                    else {
                        def.resolve(res.ClientReportSettings);
                    }
                }
            }, (err) => {
                def.reject(err);
            });


            return def.promise;

        }

        private getAuditSettings = (clientId: any, siteId?: any, cascasdeClient?: boolean): angular.IPromise<Common.Interfaces.IAuditSettings> => {
            var def = this.$q.defer();

            var params: any = {
                ClientId: clientId
            }

            // Create new AuditSetting object
            var auditSettings = new Common.Interfaces.AuditSetting();

            //Add other query params if SiteId is present
            if (siteId) {
                params = {
                    ClientId: clientId,
                    SiteId: siteId,
                    CascadeClient: cascasdeClient
                };
            }

            angular.extend(auditSettings, params);

            this.apiSvc.getByOdata(params, 'AuditSetting', false, false)
                .then((res: Common.Interfaces.IAuditSettings) => {
                    if (res) {
                        def.resolve(angular.extend(auditSettings, res));
                    }
                })
                .catch(err => {
                    def.reject(err);
                });

            return def.promise;
        }

        private getAllSitesCombinedSiteReportSettings = (): angular.IPromise<commonInterfaces.IReportSettings> => {
            var def = this.$q.defer();

            if (!this.clientId) {
                this.$log.log('There is no clientId');
                def.reject('No client Id');
                return;
            }

            var siteSettings: commonInterfaces.IReportSettings = {
                ComplaintControlLimit: 0,
                ScoringProfileGoal: 0,
                ScoringProfileId: 0,
                ScoringProfileMaximumScore: 0,
                ScoringProfileMinimumScore: 0,
                SurveyControlLimit: 0
            };

            var url = ChaiTea.Services.Interfaces.ApiEndpoints[ChaiTea.Services.Interfaces.ApiEndpoints.SiteReportSetting];

            this.apiSvc.getByOdata({}, `${url}/AllByClient/${this.clientId}/`, false, false).then((res: commonInterfaces.IAllSiteReportSettings) => {
                if (res) {
                    angular.forEach(res.AllSitesReportSettings, (item) => {
                        //Complaint Control Limit
                        siteSettings.ComplaintControlLimit = siteSettings.ComplaintControlLimit
                            + item.ComplaintControlLimit;
                    });
                    //Scoring Profile Goal
                    var checkEqual = res.AllSitesReportSettings.every((item, index, arr) => {
                        if (index === 0) {
                            return true;
                        }
                        return item.ScoringProfileGoal === arr[index - 1].ScoringProfileGoal;
                    });
                    siteSettings.ScoringProfileGoal = checkEqual ? res.AllSitesReportSettings[0].ScoringProfileGoal || res.ClientReportSettings.ScoringProfileGoal : 0;

                    //Survey Control Limit
                    siteSettings.SurveyControlLimit = res.AllSitesReportSettings.reduce(((sum, item) => {
                        return sum + item.SurveyControlLimit;
                    }), 0) / (res.AllSitesReportSettings.length != 0 ? res.AllSitesReportSettings.length : 1);

                    siteSettings.ComplaintControlLimit = siteSettings.ComplaintControlLimit || res.ClientReportSettings.ComplaintControlLimit;
                    siteSettings.ScoringProfileMaximumScore = siteSettings.ScoringProfileMaximumScore || res.ClientReportSettings.ScoringProfileMaximumScore;
                    siteSettings.ScoringProfileMinimumScore = siteSettings.ScoringProfileMinimumScore || res.ClientReportSettings.ScoringProfileMinimumScore;
                    siteSettings.SurveyControlLimit = (res.ClientReportSettings.SurveyControlLimit > siteSettings.SurveyControlLimit ? res.ClientReportSettings.SurveyControlLimit : siteSettings.SurveyControlLimit);

                    def.resolve(siteSettings);
                }
            }, this.$log.log)

            return def.promise;
        }

        /**
         * @description Pre-defined scale based on a given max number.
         */
        private getTickIntervalByScale = (scaleMax: number) => {
            var scales = {
                5: 1,
                10: 1,
                100: 10
            };
            return scales[scaleMax] || 1;
        }

        /**
         * @description Removes local/sessionStorage sbm.report.setting-{this.storeKey}.
         */
        private clearLocalSettings = () => {
            this.localDataStoreSvc.setObject(this.storeKey, "");

            this.notificationSvc.displayToast(<commonSvc.INotificationMessage>{
                title: "Settings Changed.",
                message: "Your settings have been changed by your manager.  <br/><a href=\"#\" onclick=\"window.location.reload()\" >Please click here to refresh this page.</a>",
                display: "error",
                type: NOTIFICATION_TYPE.MESSAGE

            }, {})
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

    }

    angular.module('app').service('chaitea.common.services.reportsvc', ReportSvc);
}