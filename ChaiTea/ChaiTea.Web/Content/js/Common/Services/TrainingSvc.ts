﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factory for training builder service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import serviceInterfaces = ChaiTea.Services.Interfaces;

    export enum CourseAttachmentTypes { Content, Cover };

    export interface ITraining {
        isPassed: boolean;
        passedDate: Date;
    }

    export interface ICourseCompliance {
        TotalTrainings: number;
        IsSelfInitiated: boolean;
        IsCompleted: boolean;
        IsCompliant: boolean;
        TotalCompleted: number;
        IsPassedRetake: boolean;
        CompletedDate: string;
        DueDate: string;
        RetakeDate: string;
    }

    export interface ICourseComplianceMetrics {
        Control: number;
        TotalCourses: number;
        TotalTrainings: number;
        TotalCompliant: number;
        CompliancePercent: number;
    }

    export interface ITrainingSvc {
        calculateCourseComplianceMetrics(employeeCourses: Array<serviceInterfaces.IEmployeeCourse>): ICourseComplianceMetrics;
        calculateCourseCompliance(employeeCourse: serviceInterfaces.IEmployeeCourse): ICourseCompliance;
        saveSnapshot(employeeCourse: serviceInterfaces.IEmployeeCourse, courseAttachment: serviceInterfaces.ICourseAttachment, isPassed: boolean, answers?: Array<string>, managerOrgUserId?: number);
        getCourseTrainingsByLanguage(course: serviceInterfaces.ICourse): serviceInterfaces.ICourse;
        getComplianceCourse(course: serviceInterfaces.ICourse): serviceInterfaces.ICourse;
        getEmployeeCourseList(employeeId: number): angular.IPromise<any>;
        isPositionProfileComplete(employee: Training.Interfaces.ITrainingEmployee): boolean;
        filterCourseTypes(client: IClient, data: Array<serviceInterfaces.ICourseType>): Array<serviceInterfaces.ICourseType>;
        filterCourses(client: IClient, data: Array<serviceInterfaces.ICourse>): Array<serviceInterfaces.ICourse>;
    }


    export class TrainingSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.trainingsvc';
        private basePath: string;

        static $inject = [
            '$resource',
            'sitesettings',
            '$q',
            'chaitea.common.services.apibasesvc',
            'userContext',
        ];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            private sitesettings: ISiteSettings,
            private $q: angular.IQService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private userContext: IUserContext
        ) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): ITrainingSvc {
            return {
                calculateCourseComplianceMetrics: (employeeCourses: Array<serviceInterfaces.IEmployeeCourse>) => { return this.calculateCourseComplianceMetrics(employeeCourses) },
                calculateCourseCompliance: (employeeCourse: serviceInterfaces.IEmployeeCourse) => { return this.calculateCourseCompliance(employeeCourse) },
                saveSnapshot: (employeeCourse: serviceInterfaces.IEmployeeCourse, courseAttachment: serviceInterfaces.ICourseAttachment, isPassed: boolean, answers?: Array<string>, managerOrgUserId?: number) => {
                    return this.saveSnapshot(employeeCourse, courseAttachment, isPassed, answers, managerOrgUserId)
                },
                getCourseTrainingsByLanguage: (course: serviceInterfaces.ICourse) => { return this.getCourseTrainingsByLanguage(course) },
                getComplianceCourse: (course: serviceInterfaces.ICourse) => { return this.getComplianceCourse(course) },
                getEmployeeCourseList: (employeeId: number) => { return this.getEmployeeCourseList(employeeId) },
                isPositionProfileComplete: (employee: Training.Interfaces.ITrainingEmployee): boolean => (this.isPositionProfileComplete(employee)),
                filterCourseTypes: (client: IClient, data: Array<serviceInterfaces.ICourseType>): Array<serviceInterfaces.ICourseType> => (this.filterCourseTypes(client, data)),
                filterCourses: (client: IClient, data: Array<serviceInterfaces.ICourse>): Array<serviceInterfaces.ICourse> => (this.filterCourses(client, data))
            };
        }

        /**
        * @description Calculates compliance using the course snapshots.
        */
        private calculateCourseComplianceMetrics = (employeeCourses: Array<serviceInterfaces.IEmployeeCourse>): ICourseComplianceMetrics => {

            var metrics: ICourseComplianceMetrics = {
                Control: 80,
                TotalCourses: 0,
                TotalTrainings: 0,
                TotalCompliant: 0,
                CompliancePercent: 100
            };

            var trainings: Array<ICourseCompliance> = [];

            //sort
            angular.forEach(employeeCourses, (training: serviceInterfaces.IEmployeeCourse) => {

                var comp = this.calculateCourseCompliance(training);

                if (!comp.IsSelfInitiated) {
                    trainings.push(comp);
                }

            });

            metrics.TotalCompliant = _.filter(trainings, { IsCompliant: true }).length;
            metrics.TotalCourses = trainings.length;

            var totalArray = _.pluck(trainings, 'TotalTrainings');
            metrics.TotalTrainings = _.sum(totalArray);

            if (metrics.TotalCourses > 0) {
                metrics.CompliancePercent = _.round((metrics.TotalCompliant / metrics.TotalCourses) * 100);
            }

            return metrics;

        }

        /**
        * @description Calculates compliance using the course snapshots.
        */
        private calculateCourseCompliance = (employeeCourse: serviceInterfaces.IEmployeeCourse): ICourseCompliance => {

            var compliance: ICourseCompliance = {
                TotalTrainings: 0,
                IsSelfInitiated: false,
                IsCompleted: false,
                IsCompliant: true,
                IsPassedRetake: false,
                TotalCompleted: 0,
                CompletedDate: '',
                DueDate: '',
                RetakeDate: ''
            };

            //filter out the courses that are not your selected language
            var complianceCourse = this.getComplianceCourse(employeeCourse.Course);
            var trainings: Array<ITraining> = [];
            var deadlineDate: moment.Moment;
            var retakeDate: moment.Moment;

            compliance.TotalTrainings = complianceCourse.CourseAttachments.length;

            //get course due date
            if (employeeCourse.CreateDate || employeeCourse.DueDate) {

                if (employeeCourse.DueDate) {
                    deadlineDate = moment(employeeCourse.DueDate);
                    compliance.DueDate = deadlineDate.format('MM/DD/YYYY');
                } else {
                    //check date added to employeecourse
                    deadlineDate = moment(employeeCourse.CreateDate).add(complianceCourse.DeadlineHireType, complianceCourse.DeadlineHire);
                    compliance.DueDate = moment(deadlineDate).format('MM/DD/YYYY');
                }

            }

            if (employeeCourse.SnapshotEmployeeCourses.length) {

                //check each attachment for a snapshot 
                angular.forEach(complianceCourse.CourseAttachments, (attachment: serviceInterfaces.ICourseAttachment) => {

                    var snapshots = _.sortBy(_.where(employeeCourse.SnapshotEmployeeCourses, { UniqueId: attachment.Attachment.UniqueId }), 'CreateDate').reverse();
                                    
                    //always check the latest snapshot
                    if (snapshots.length) {
                        trainings.push({ isPassed: snapshots[0].Passed, passedDate: snapshots[0].CreateDate });
                    }

                });

                trainings = _.sortBy(trainings, 'passedDate').reverse();
                compliance.TotalCompleted = _.where(trainings, { isPassed: true }).length;

                if ((compliance.TotalCompleted == complianceCourse.CourseAttachments.length) && trainings.length) {
                    compliance.IsCompleted = true;
                    compliance.CompletedDate = moment(trainings[0].passedDate).format('MM/DD/YYYY');
                }

            }

            //check if past due
            if (compliance.IsCompleted && compliance.CompletedDate) {
                retakeDate = moment(compliance.CompletedDate).add(complianceCourse.RetakeAfterType, complianceCourse.RetakeAfter);

                if (retakeDate.isBefore(compliance.CompletedDate)) {
                    compliance.IsPassedRetake = true;
                    compliance.IsCompleted = false;
                }

                if (compliance.IsPassedRetake) {
                    //get new deadline date
                    deadlineDate = retakeDate.add(complianceCourse.DeadlineHireType, complianceCourse.DeadlineHire);
                    compliance.DueDate = moment(deadlineDate).format('MM/DD/YYYY');
                }

            }

            //check due date
            if (deadlineDate.isBefore(moment()) && !compliance.IsCompleted) {
                compliance.IsCompliant = false;
            }

            //check if the user added the course through the knowledge center
            if (employeeCourse.Employee.Id == employeeCourse.CreateById) {
                compliance.IsSelfInitiated = true;
            }

            return compliance;

        };

        /**
        * @description Saves snapshot to DB.
        */
        private saveSnapshot = (employeeCourse: serviceInterfaces.IEmployeeCourse, courseAttachment: serviceInterfaces.ICourseAttachment, isPassed: boolean, answers?: Array<string>, managerOrgUserId?: number) => {

            var courseSnapshotQuestions: Array<serviceInterfaces.ISnapshotCourseQuestions> = [];
            
            //map questions
            angular.forEach(courseAttachment.CourseAttachmentQuestions, (question, index) => {

                var snapshotQuestionOptions: Array<serviceInterfaces.ISnapshotCourseQuestionOption> = [];
                angular.forEach(question.CourseAttachmentQuestionOptions, (option) => {
                    var snapshotQuestionOption: serviceInterfaces.ISnapshotCourseQuestionOption = {
                        Option: option.Option,
                        IsAnswer: option.IsAnswer,
                    };
                    snapshotQuestionOptions.push(snapshotQuestionOption);
                });

                var courseSnapshotQuestion: serviceInterfaces.ISnapshotCourseQuestions = {
                    OrderPriority: question.OrderPriority,
                    Question: question.Question,
                    Answer: (answers[index] ? answers[index] : ''),
                    SnapshotCourseQuestionOptions: snapshotQuestionOptions
                };
                courseSnapshotQuestions.push(courseSnapshotQuestion);

            });

            //map course
            var courseSnapshot: serviceInterfaces.ISnapshotCourse = {
                Name: employeeCourse.Course.Name,
                Overview: employeeCourse.Course.Overview,
                DeadlineHire: employeeCourse.Course.DeadlineHire,
                DeadlineHireType: employeeCourse.Course.DeadlineHireType,
                RetakeAfter: employeeCourse.Course.RetakeAfter,
                RetakeAfterType: employeeCourse.Course.RetakeAfterType,
                OrgId: (employeeCourse.Course.OrgType == ChaiTea.Common.Interfaces.OrgTypes[ChaiTea.Common.Interfaces.OrgTypes.Site] ? employeeCourse.Course.OrgSite.Id : employeeCourse.Course.OrgClient.Id),
                OrgUserId: employeeCourse.Course.EmployeeId,
                CourseTypeId: employeeCourse.Course.CourseTypeId,
                SnapshotCourseQuestions: courseSnapshotQuestions
            };

            return new this.$q((resolve, reject) => {

                //save course
                this.apiSvc.save(courseSnapshot, "EmployeeCourse/Course/Snapshot").then((res: serviceInterfaces.ISnapshotCourse) => {

                    var employeeCourseSnapShot: serviceInterfaces.ISnapshotEmployeeCourse = {
                        OrgUserId: employeeCourse.OrgUserId,
                        SnapshotCourseId: res.SnapshotCourseId,
                        EmployeeCourseId: employeeCourse.EmployeeCourseId,
                        CourseAttachmentId: courseAttachment.CourseAttachmentId,
                        Passed: isPassed,
                        UniqueId: courseAttachment.Attachment.UniqueId,
                        Name: courseAttachment.Attachment.Name,
                        Description: courseAttachment.Attachment.Description,
                        ManagerOrgUserId: (managerOrgUserId ? managerOrgUserId : null)
                    };

                    //create employee course snapshot
                    this.apiSvc.save(employeeCourseSnapShot, "EmployeeCourse/Snapshot").then((res) => {

                        resolve(res);

                    });

                });
            });

        }

        /**
        * @description Gets all courses the user is required to take
        */
        private getComplianceCourse = (course: serviceInterfaces.ICourse): serviceInterfaces.ICourse => {

            var originalCourse = _.clone(course, true);

            ////get total english course
            ////get total in the users language
            ////if different, add the english version of the course
            var courseEnglish = this.getCourseTrainingsByLanguage(originalCourse, 'en');
            var complianceCourse = this.getCourseTrainingsByLanguage(course);

            if (complianceCourse.CourseAttachments.length != courseEnglish.CourseAttachments.length) {

                angular.forEach(courseEnglish.CourseAttachments, (courseAttachment: serviceInterfaces.ICourseAttachment) => {

                    //look for user language version
                    var attachment = _.find(complianceCourse.CourseAttachments, { CourseAttachmentParentUniqueId: courseAttachment.Attachment.UniqueId });
                    if (!attachment) {
                        complianceCourse.CourseAttachments.push(courseAttachment);
                    }

                });

            }

            return complianceCourse;

        }

        /**
        * @description Filters course attachments by language.
        */
        private getCourseTrainingsByLanguage = (course: serviceInterfaces.ICourse, languageCode?: string): serviceInterfaces.ICourse => {

            if (languageCode) {
                course.CourseAttachments = _.where(course.CourseAttachments, { Language: { Code: languageCode.toLowerCase() }, CourseAttachmentType: CourseAttachmentTypes[CourseAttachmentTypes.Content] });
            } else {
                course.CourseAttachments = _.where(course.CourseAttachments, { Language: { Code: this.sitesettings.userLanguage }, CourseAttachmentType: CourseAttachmentTypes[CourseAttachmentTypes.Content] });
            }

            return course;
        }

        /**
        * @description Retrievers list of the all training courses
        */
        private getEmployeeCourseList = (employeeId: number): angular.IPromise<any> => {

            var def = this.$q.defer();

            this.apiSvc.get('EmployeeCourse/AllByEmployeeId/' + employeeId).then((employeeCourses: serviceInterfaces.IEmployeeCourse) => {
                def.resolve(employeeCourses);
            });

            return def.promise;

        }

        /**
        * @description Checks if the course attachments have been passed
        */
        private getTrainingStatus = (employeeCourse: serviceInterfaces.IEmployeeCourse) => {

            angular.forEach(employeeCourse.Course.CourseAttachments, (item: serviceInterfaces.ICourseAttachment, key) => {
                
                //check is the training has been passed
                item.IsPassed = false;
                if (employeeCourse.SnapshotEmployeeCourses.length) {

                    var snapshots = _.where(employeeCourse.SnapshotEmployeeCourses, { CourseAttachmentId: item.CourseAttachmentId });

                    if (snapshots.length && snapshots[0].Passed) {
                        item.IsPassed = true;
                    }

                }

            });

        }

        /**
        * @description Checks if the position profile is complete
        */
        private isPositionProfileComplete = (employee: Training.Interfaces.ITrainingEmployee): boolean => {
            employee.IsComplete = false;
            if (!employee.JobDescription) {
                employee.Status =  'Job description missing';
            }
            // else if (!employee.EmployeeTasks.length) {
            //    employee.Status = 'Job tasks missing';
            //}
            else if (!employee.SupervisorId) {
                employee.Status =  'Supervisor missing';
            }
            else if (!employee.AreaCount) {
                employee.Status =  'Area assignment missing';
            }
            else if (!employee.HireDate) {
                employee.Status = 'Hire date missing';
            }
            else if (!employee.EmployeeEquipment.length) {
                employee.Status = 'Job equipment missing';
            }
            else if (!employee.SupervisorPhoneNumber) {
                employee.Status = 'Supervisor phone number is missing';
            }
            else if (!employee.SupervisorEmail) {
                employee.Status = 'Supervisor email is missing';
            }
            else {
                employee.Status = 'Complete';
                employee.IsComplete = true;
            }

            return employee.IsComplete;
        }

        private gmpExceptions: Array<any> = [
            { Name: "Google", Id: 10061 },
            { Name: "Amazon", Id: 10078 },
            { Name: "Apple Inc.", Id: 10001 },
            { Name: "Intel", Id: 10054 },
            { Name: "Spirit Aerosystems", Id: 10003 },
            { Name: "Textron - Cessna", Id: 10010 },
            { Name: "Wells Fargo", Id: 10011 },
            { Name: "Monsanto", Id: 10012 },
            { Name: "CBRE/Amazon", Id: 10205 }
        ];

        /**
        * @description Filters course types by client
        */
        private filterCourseTypes = (client: IClient, data: Array<serviceInterfaces.ICourseType>): Array<serviceInterfaces.ICourseType> => {

            //TODO: move to DB or better location
            //Should map overrides in DB
            if (_.some(this.gmpExceptions, (item) => { return client.ID === item.Id || client.Name === item.Name })) {
                var removed = _.remove(data, function (t) {
                    return t.Name == 'GMP';
                });
            }

            return data;

        }

        /**
        * @description Filters courses by client
        */
        private filterCourses = (client: IClient, data: Array<serviceInterfaces.ICourse>): Array<serviceInterfaces.ICourse> => {

            //TODO: move to DB or better location
            //Should map overrides in DB
            if (_.some(this.gmpExceptions, (item) => { return client.ID === item.Id || client.Name === item.Name })) {
                var removed = _.remove(data, function (c) {
                    return c.CourseType.Name == 'GMP';
                });
            }

            return data;

        }

    }

    angular.module('app').service(TrainingSvc.id, TrainingSvc);
}