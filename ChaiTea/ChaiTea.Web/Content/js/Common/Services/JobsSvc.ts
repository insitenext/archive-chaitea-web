﻿module ChaiTea.Common.Services {
    'use strict';
    
    export interface IJobSvc {
        saveNewJobTask(params?: Object): ng.IPromise<any>;
        saveNewJobEquipment(params?: Object): ng.IPromise<any>;
        getAllTasks(): ng.IPromise<any>;
        getAllPositions(): ng.IPromise<any>;
        getAllEquipments(): ng.IPromise<any>;
    }

    interface IJobResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
        update(Object): ng.resource.IResource<any>;
    }

    class JobSvc implements angular.IServiceProvider{
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IJobSvc {
            return {
                getAllTasks: () => (this.getAllTasks()),
                getAllPositions: () => (this.getAllPositions()),
                saveNewJobTask: (params?: any) => (this.saveNewJobTask(params)),
                getAllEquipments: () => (this.getAllEquipments()),
                saveNewJobEquipment: (params?: any) => (this.saveNewJobEquipment(params))
            };
        }

        getAllTasks = () => {
            return this.jobTaskResource().query().$promise;
        }

        getAllEquipments = () => {
            return this.jobEquipmentResource().query().$promise;
        }

        getAllPositions = () => {
                return this.jobResource().query().$promise;
        }

        saveNewJobTask = (params) => {
            return this.jobTaskResource().save(params).$promise;
        }

        saveNewJobEquipment = (params) => {
            return this.jobEquipmentResource().save(params).$promise;
        }



        /**
         * @description Resource object for training.
         */
        private jobResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };

            var url = this.basePath + 'api/Services/Job/';

          

            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }


        private jobTaskResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };
          
            var url = this.basePath + 'api/Services/JobTask/';
       
            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }

        private jobEquipmentResource = (): IJobResource => {
            var updateAction: ng.resource.IActionDescriptor = {
                method: 'PUT',
                isArray: false
            };
          
            var url = this.basePath + 'api/Services/Equipment/';



            return <IJobResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    update: updateAction,
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url,
                        params: { skip: 0 }
                    }
                });
        }
    }


    angular.module('app').service('chaitea.common.services.jobs', JobSvc);
}