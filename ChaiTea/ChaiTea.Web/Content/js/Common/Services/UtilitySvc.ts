﻿/// <reference path="../../_libs.ts" />

/**
 * @description Common utilities service.
 */
module ChaiTea.Common.Services {
    'use strict';

    import commonInterfaces = Common.Interfaces;

    enum FileType { Undefined, ProfilePicture };

    export interface IUtilitySvc {
        generateUniqueToken(): string;
        isJSONString(string): boolean;
        getIdParamFromUrl(url?: string, reqNumber?: boolean);
        getQueryParamFromUrl(name: string, url?: string);
        convertQueryParamsIntoObject(queryParams?: string): Object;
        mapKeysToCamelCase(objectToMap: Object);
        mapKeysToCapital(objectToMap: Object);
        removeHtmlFromString(content: string, addSpaces?: boolean): string;
        validateEmail(email: string): boolean;
        getWebLink(stateName: string): string;
    }

    export interface IThumbnailSize {
        width: number;
        height: number;
    }

    export class UtilitySvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.utilssvc';
        static $inject = ['$location', '$window','navigationconfig'];
        constructor(
            private $location: angular.ILocationService,
            private $window: angular.IWindowService,
            private navigationConfig: any
        ) { }

        public $get(): IUtilitySvc {
            return {
                generateUniqueToken: () => { return this.generateUniqueToken(); },
                getIdParamFromUrl: (url?: string, reqNumber?: boolean) => (this.getIdParamFromUrl(url, reqNumber)),
                convertQueryParamsIntoObject: (queryParams?: string) => this.convertQueryParamsIntoObject(queryParams),
                isJSONString: (stringToTest: string) => (this.isJSONString(stringToTest)),
                getQueryParamFromUrl: (name: string, url?: string) => this.getQueryParamFromUrl(name, url),
                mapKeysToCamelCase: (objectToMap: Object) => (this.mapKeysToCamelCase(objectToMap)),
                mapKeysToCapital: (objectToMap: Object) => (this.mapKeysToCapital(objectToMap)),
                removeHtmlFromString: (content: string, addSpaces?: boolean) => this.removeHtmlFromString(content, addSpaces),
                validateEmail: (email: string) => this.validateEmail(email),
                getWebLink: (stateName: string) => this.getWebLink(stateName)
            }
        }

        /**
         * @description Return a unique GUID token string.
         */
        generateUniqueToken = (): string => {
            function _p8(s?) {
                var p = (Math.random().toString(16) + '000000000').substr(2, 8);
                return s ? '-' + p.substr(0, 4) + '-' + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        }

        isJSONString = (stringToTest: string) => {
            try {
                JSON.parse(stringToTest);
            } catch (e) {
                return false;
            }
            return true;
        }

        getQueryParamFromUrl = (name: string, url?: string) => {
            if (!url) url = window.location.href;
            
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        getIdParamFromUrl = (url?: string, reqNumber?: boolean) => {
            var url = url || window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
            if (id == "") {
                var str = url.slice(0, url.lastIndexOf('/'));
                id = str.substring(str.lastIndexOf('/') + 1);
            }
            var parsedId = parseInt(id);
            if (reqNumber != null && (!_.isNumber((parsedId)) || isNaN(parsedId))) {
                console.log('Must be a number');
                return;
            }
            return id;
        }

        /*
        * @description Return object from passed in query string parameters or current URL's query string parameters
        */
        convertQueryParamsIntoObject = (queryParams?: string): Object => {
            return (queryParams || this.$window.location.search).replace(/(^\?)/, '').split("&").map(function (n) { return n = n.split("="), this[n[0]] = n[1], this }.bind({}))[0];
        }

        /*
        * @description Return object with keys mapped to CamelCase
        */
        mapKeysToCamelCase = (object: Object): Object => {
            return _.mapKeys(object, (val: any, key: any) => {
                return _.camelCase(key);
            });
        }
        /*
        * @description Return object with keys mapped to first letter being capitalized 
        */
        mapKeysToCapital = (object: Object): Object => {
            return _.mapKeys(object, (val: any, key: any) => {
                return _.capitalize(key);
            });
        }

        /*
        * @description Return object with HTML Regex removed.
        */
        removeHtmlFromString = (content: string, addSpaces?: boolean) => {
            return content.replace(/<\/?[^>]+>/gi, (addSpaces ? ' ' : ''));
        }

        validateEmail = (email: string):boolean => {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        getCookie = (name) => {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) {
                return parts.pop().split(";").shift();
            }
            return null;
        }

        getWebLink = (stateName: string) => {
            var url: string = '';
            // get base domain
            url += 'https://' + this.navigationConfig.WebDomain;
            // add page that will handle and auth and redirect
            url += '/' + this.navigationConfig.WebDomainRedirector;
            // add jwt token
            url += '?token=' + this.getCookie('r2sbminsite');
            // add state to redirect too
            url += '&redirect=' + encodeURIComponent(stateName);
            return url;
        }
    }

    angular.module('app').service(UtilitySvc.id, UtilitySvc);
}
