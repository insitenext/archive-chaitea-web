﻿/// <reference path="../../_libs.ts" />

/**
 * @description Wrapper service for alerts.
 */

module ChaiTea.Common.Services {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;

    interface IActivityFeedResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
    }

    export interface IActivityFeedSvc {
        getActivityFeedItems();
    }

    class ActivityFeedSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor
         * @param {ng.resource.IResourceService} $resource
         * @param {Object} sitesettings
         */
        constructor(private $resource: ng.resource.IResourceService, sitesettings: ISiteSettings) {
            this.basePath = sitesettings.basePath;
        }

        public $get(): IActivityFeedSvc {
            return {
                getActivityFeedItems: () => { return this.getActivityFeedItems(); }
            };
        }

        /**
         * @description gets list of cultures.
         */
        getActivityFeedItems = () => {
            return this.activityFeedResource().getByOData({}).$promise;
        }

        /**
         * @description Resource object for userinfo.
         */
        private activityFeedResource = (): IActivityFeedResource => {
            var url = this.basePath + 'api/Services/Notification/';

            return <IActivityFeedResource>this.$resource(
                url + ':id', { id: '@id' },
                {
                    getByOData: {
                        method: 'GET',
                        cache: false,
                        isArray: true,
                        url: url
                    }
                });
        }
    }
    angular.module('app').service('chaitea.common.services.activityfeedsvc', ActivityFeedSvc);
}