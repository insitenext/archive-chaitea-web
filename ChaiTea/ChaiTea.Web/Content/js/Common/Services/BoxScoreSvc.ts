﻿/// <reference path="../../_libs.ts" />

/**
 * Register the factories for SafetyIncident.
 */
module ChaiTea.Common.Services {
    'use strict';

    export interface IBoxScoreSvc {
        getPersonnelAuditOptions(): angular.IPromise<any>;
        getProfessionalismModalContent(employeeId: number, startDate: string, endDate: string): angular.IPromise<any>;
        buildProfessionalismContent(data, options): string;
    }

    class BoxScoreSvc implements angular.IServiceProvider {
        private basePath: string;

        kpiOptions = {
            appearance: [],
            responsiveness: [],
            attitude: [],
            equipment: []
        };

        static $inject = [
            '$log',
            '$q',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
        ];
        constructor(
            private $log: ng.ILogService,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc) {

        }

        public $get(): IBoxScoreSvc {
            return {
                getPersonnelAuditOptions: () => { return this.getPersonnelAuditOptions(); },
                getProfessionalismModalContent: (employeeId: number, startDate: string, endDate: string) => { return this.getProfessionalismModalContent(employeeId, startDate, endDate); },
                buildProfessionalismContent: (data, options) => { return this.buildProfessionalismContent(data, options); }
            }
        }

        private getPersonnelAuditOptions = (): angular.IPromise<any> => {

            var def = this.$q.defer();

            this.apiSvc.get("LookupList/EmployeeAuditOptions").then((res) => {

                this.kpiOptions.appearance = _.where(res, { KpiOption: { Name: 'Appearance' } });
                this.kpiOptions.appearance["Description"] = "Employee wears an SBM tidy uniform and has good hygiene.";
                this.kpiOptions.appearance["Name"] = "Appearance";

                this.kpiOptions.attitude = _.where(res, { KpiOption: { Name: 'Attitude' } });
                this.kpiOptions.attitude["Description"] = "Employee displays a positive approachable demeanor, shows respect and is attentive.";
                this.kpiOptions.attitude["Name"] = "Attitude";

                this.kpiOptions.responsiveness = _.where(res, { KpiOption: { Name: 'Responsiveness' } });
                this.kpiOptions.responsiveness["Description"] = "Employee displays a quick response time and shows initiative.";
                this.kpiOptions.responsiveness["Name"] = "Responsiveness";

                this.kpiOptions.equipment = _.where(res, { KpiOption: { Name: 'Closet & Equipment' } });
                this.kpiOptions.equipment["Description"] = "Employee maintains proper care of equipment and a tidy closet without any hazardous conditions.";
                this.kpiOptions.equipment["Name"] = "Equipment & Closets";

                def.resolve(this.kpiOptions);

            }, (err) => {
                def.reject(err);
            });


            return def.promise;

        }


        private getProfessionalismModalContent = (employeeId: number, startDate: string, endDate: string): angular.IPromise<any> => {

            var def = this.$q.defer();
            var params: any = {
                $orderby: 'CreateDate desc',
                $filter: `EmployeeId eq ${employeeId} and CreateDate gt DateTime'${startDate}' and CreateDate le DateTime'${endDate}'`
            };

            async.parallel([
                (callback) => {

                    this.apiSvc.getByOdata(params, ChaiTea.Services.Interfaces.ApiEndpoints[ChaiTea.Services.Interfaces.ApiEndpoints.EmployeeAudit])
                        .then((res: Array<ChaiTea.Services.Interfaces.IProfessionalismAudit>) => {
                            callback(null, res);
                        }, (error) => {
                            callback(error, null);
                        });

                },
                (callback) => {

                    this.getPersonnelAuditOptions().then((res) => {
                        callback(null, res);
                    }, (error) => {
                        callback(error, null);
                    });

                }
            ], (err, res) => {

                if (err) {
                    def.reject(err);
                } else {
                    def.resolve(res);
                }

            });
            return def.promise;

        }


        private buildProfessionalismContent = (data: ChaiTea.Services.Interfaces.IProfessionalismAudit, options: any): string => {

            var body = '';

            //appearance
            var appearanceExists = _.any(data.CheckBoxOptions, { KpiOption: { KpiOptionId: 1 } });
            body += this.buildAuditOption(options.appearance, appearanceExists);

            //attitude
            var attitudeExists = _.any(data.CheckBoxOptions, { KpiOption: { KpiOptionId: 2 } });
            body += this.buildAuditOption(options.attitude, attitudeExists);

            //responsiveness
            var responsivenessExists = _.any(data.CheckBoxOptions, { KpiOption: { KpiOptionId: 4 } });
            body += this.buildAuditOption(options.responsiveness, responsivenessExists);

            //equipment
            var equipmentExists = _.any(data.CheckBoxOptions, { KpiOption: { KpiOptionId: 3 } });
            body += this.buildAuditOption(options.equipment, equipmentExists);

            body += '<hr/>';
            body += '<div><strong>Overall Comment</strong></div>';
            body += '<div>' + data.Comment + '</div>';

            return body;
        
        }

        private buildAuditOption = (option: any, passed: boolean): string => {
            var content = '';

            content += '<div class="row padding-top padding-bottom">';
            content += '<div class="col-md-12 padding-top"><strong>' + option.Name + '</strong></div>';
            content += '<div class="col-md-8">' + option.Description + '</div>';
            content += '<div class="pull-right padding-right text-' + (passed ? 'danger' : 'success') + '"><strong>' + (passed ? 'FAILED' : 'PASSED') + '</strong></div>';
            content += '</div>';

            return content;

        }

    }

    angular.module('app').service('chaitea.common.services.boxscoresvc', BoxScoreSvc);
}


