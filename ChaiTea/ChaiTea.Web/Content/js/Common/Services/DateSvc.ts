﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Common.Services {
    'user strict';

    export interface IDateSvc {
        setCustomDatesToUtcDateTimeOffset(date: string);
        getFromUtcDatetimeOffset(date: string, format?: string);
        setFilterDatesToSitesDatetimeOffset(date: string);
    }

    export class DateSvc implements angular.IServiceProvider {
        static id = 'chaitea.common.services.datesvc';
        offset: string = null;
        temp: ng.IPromise<any>;
        static $inject = [
            'userContext',
            'chaitea.common.services.apibasesvc',
            '$q'
        ];
        constructor(
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private $q: ng.IQService
        ) { }
        public $get(): IDateSvc {
            return {
                setCustomDatesToUtcDateTimeOffset: (date: string) => this.setCustomDatesToUtcDateTimeOffset,
                getFromUtcDatetimeOffset: (date: string) => this.getFromUtcDatetimeOffset,
                setFilterDatesToSitesDatetimeOffset: (date: string, format?: string) => this.setFilterDatesToSitesDatetimeOffset(date)
            }
        }

        public setFilterDatesToSitesDatetimeOffset = (date: string) => {
            var localOffset = moment(date).utcOffset();
            var siteOffset = moment.tz(date, this.userContext.Timezone.TimezoneName).utcOffset();
            return moment(date).utc().add('hours', (localOffset / 60) - (siteOffset / 60)).format('YYYY-MM-DDTHH:mm:ssZ');
        }

        public setCustomDatesToUtcDateTimeOffset = (date: string) => {
            return moment(new Date(date)).utc().format('YYYY-MM-DDTHH:mm:ssZ');
        }

        public getFromUtcDatetimeOffset = (date: string, format?: string) => {
            if (!format) {
                format = 'YYYY-MM-DD';
            }
            return moment.tz(date, this.userContext.Timezone.TimezoneName).format(format);
        }
    }

    angular.module('app').service(DateSvc.id, DateSvc);
}