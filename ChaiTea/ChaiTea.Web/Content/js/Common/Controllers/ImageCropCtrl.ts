﻿


module ChaiTea.Common.Controllers {

    class ImageCropCtrl {

        basePath: string;
        modalTitle: string = 'Crop Image';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        imgUrl: string = "";
        imageCropStep: number = 1;
        imageCropResult: string = null;
        imageCropResultBlob: any = null;
        initCrop: boolean = false;
        croppedImage: string = null;
        imageDescription: string = null;

        static $inject = [
            '$scope',
            '$uibModalInstance',
            '$timeout',
            'sitesettings',
            'chaitea.common.services.imagesvc',
            'file',
            'needDescription',
            'chaitea.common.services.notificationsvc'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $timeout: angular.ITimeoutService,
            private sitesettings: ISiteSettings,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private file: any,
            private needDescription: boolean,
            private notificationSvc: Common.Services.INotificationSvc) {

            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Common/Templates/Image/modal.cropImage.tmpl.html';
            this.modalFooterTemplate = this.basePath + 'Content/js/Common/Templates/Image/modal.cropImage.footer.tmpl.html';

            this.imgUrl = file;

            this.$scope.$watch('vm.imageCropResult', function (newVal) {
                if (newVal) {
                    this.croppedImage = newVal;
                }
            });

        }

        /**
        * @description Dismiss the modal instance.
        */
        public crop = (): void => {
            this.initCrop = true;
        }

        /**
        * @description Dismiss the modal instance.
        */
        public reset = (): void => {
            this.imageCropResult = null;
            this.imageCropStep = 2
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {

            if (this.imageCropStep > 2) {
                var res: any = {
                    originalImage: this.imgUrl,
                    croppedImage: this.imageCropResult,
                    imageDescription: this.imageDescription
                };
                this.$uibModalInstance.close(res);
            } else {
                this.notificationSvc.errorToastMessage('Please crop image before closing');
            }
        };

    }

    angular.module('app').controller('Common.ImageCropCtrl', ImageCropCtrl);

}

