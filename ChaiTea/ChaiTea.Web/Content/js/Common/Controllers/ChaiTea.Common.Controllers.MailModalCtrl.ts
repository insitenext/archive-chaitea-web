﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = Common.Interfaces;

    interface ICustomerSurveyMailer extends ng.IScope {
        SiteName: string;
        programName: string;
        surveyURL: string;
        siteManagerName: string;
        siteManagerImage: string;
    }
 
    class MailModalSurveyCtrl {
        basePath: string = '';
        fullDomain: string = '';

        clientName: string = '';
        siteId: number = 0;
        programId: number = 0;
        programName: string = '';
        programs = [];

        supervisorName: string = '';
        supervisorImage: string = '';

        isMailSent = false;
        surveyMail = {
            RecipientName: null,
            RecipientEmail: null,
            ProgramId: null,
            BulkNameEmails: null
        };
        bulkEmailFormErrors = [];

        disableEntry: boolean = false;
        isAuthorized: boolean = false;
        alert = {
            type: '',
            message: '',
            show: false
        };
        btnText: string = 'Send Survey';
        isEmailAddressInvalid: boolean = false;
        static $inject = [
            '$scope',
            '$rootScope',
            '$log',
            '$uibModalInstance',
            'chaitea.common.services.mailsvc',
            'userContext',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'userInfo',
            '$http',
            '$compile',
            '$timeout',
            'chaitea.quality.services.employeesvc',
            'paramsforCloud',
            'chaitea.common.services.imagesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.utilssvc'
        ];
        constructor(
            private $scope: ICustomerSurveyMailer,
            private $rootScope: angular.IRootScopeService,
            private $log: angular.ILogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private mailSvc: commonSvc.IMailSvc,
            private userContext: IUserContext,
            private siteSettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private userInfo: IUserInfo,
            private $http: angular.IHttpService,
            private $compile: angular.ICompileService,
            private $timeout: angular.ITimeoutService,
            private employeesvc: qualitySvc.IEmployeeSvc,
            private paramsforCloud: any,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private utilitySvc: Common.Services.UtilitySvc) {
            this.basePath = siteSettings.basePath;
            this.fullDomain = siteSettings.fullDomain;

            this.clientName = userContext.Client.Name;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.programName = userContext.Program.Name;

            this.isAuthorized = (this.userInfo.mainRoles.managers);
            if (this.isAuthorized == false) {
                this.showMailerAlert('na');
            }

            if (!this.siteId) {
                this.showMailerAlert('danger');
            } else {
                this.apiSvc.query({}, 'Surveys/Programs', false, false).then((result) => {
                    this.programs = result.Options;

                    // Pre-select program if a program is already set in context
                    if (this.programId) {
                        if (_.find(this.programs, { Key: this.programId })) {
                            this.surveyMail.ProgramId = (this.programId || null);
                        }
                    }
                });
            }

            if (this.paramsforCloud) {
                this.disableEntry = true;
                this.surveyMail.ProgramId = this.paramsforCloud.ProgramId;
                this.surveyMail.RecipientName = this.paramsforCloud.Recipient.Name;
                this.surveyMail.RecipientEmail = this.paramsforCloud.Recipient.EmailAddress;
                this.btnText = 'Resend Survey';
            }
        }

        /** @description Initializer for the controller. */
        initialize = (): void => {

        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }

        /**
         * @description Alerts for MailSvc OpenForm
         */
        showMailerAlert = (alertType: string): void => {
            this.alert.show = true;
            switch (alertType) {
                case 'na':
                    this.alert.message = `Only a manager can send out Surveys.`;
                    this.alert.type = 'danger';
                    break;
                case 'danger':
                    this.alert.message = "A site needs to be selected before you can send a survey.";
                    this.alert.type = 'danger';
                    break;
                case 'warning':
                    this.alert.message = `The selected program ${this.programName} doesn't have survey questions created.`;
                    this.alert.type = 'warning';
                    break;
                case 'success':
                    this.alert.message = 'The survey has been sent successfully!';
                    this.alert.type = 'success';
                    break;
                default:
                    break;
            }
        }

        /**
         * @description Initiate the survey request and handle chain of events.
         */
        sendSurvey = (): void => {
            if (!this.utilitySvc.validateEmail(this.surveyMail.RecipientEmail)) {
                this.isEmailAddressInvalid = true;
                return;
            }
            this.alert.show = false;
            if (this.surveyMail.RecipientName && this.surveyMail.RecipientEmail) {
                var programId = this.surveyMail.ProgramId;
                var surveyUrl = `${this.fullDomain}/Quality/CustomerSurvey/Survey/`;
                var nameEmailList = [];

                nameEmailList.push({ name: this.surveyMail.RecipientName, emailAddress: this.surveyMail.RecipientEmail });

                this.apiSvc.save({
                    recipients: nameEmailList,
                    surveyUrl: surveyUrl
                }, 'Surveys?ProgramId=' + programId, false).then((result) => {
                    mixpanel.track('Survey sent', { SurveyProgramId: programId });
                    this.showMailerAlert('success');
                    this.$uibModalInstance.dismiss('sent');
                }, this.onFailure);
            } else {
                this.alert.message = "Missing required field";
                this.alert.show = true;
            }
        }

        /**
        * @description Process and send bulk emails
        */
        sendBulkSurvey = () => {
            var programId = this.surveyMail.ProgramId,
                nameEmailList = [],
                duplicateEmails = [];
            this.bulkEmailFormErrors = [];
            this.alert.show = false;

            if (!this.surveyMail.BulkNameEmails) {
                return;
            }

            //Parse name and email from string
            this.surveyMail.BulkNameEmails = this.surveyMail.BulkNameEmails.replace(/\r?\n|\r/g, "");
            angular.forEach(this.surveyMail.BulkNameEmails.split(';'), (item: string, index) => {
                if (!item) {
                    return;
                }
                var name, emailAddress;
                // Handle items that are "{name} <{email}>" format
                if (item.indexOf('<') >= 0) {
                    name = item.split('<')[0];
                    emailAddress = item.substring(item.lastIndexOf('<') + 1, item.lastIndexOf('>'));

                    //validate email address
                    if (!emailAddress || !this.utilitySvc.validateEmail(emailAddress)) {
                        let invalidEmailText = 'Invalid or missing email address: ';
                        if (_.contains(item, invalidEmailText)) {
                            this.bulkEmailFormErrors.push(item);
                        } else {
                            this.bulkEmailFormErrors.push(invalidEmailText + item);
                        }
                        return;
                    };
                } else {
                    this.$log.error('Incorrect format.  Format should be as follows: "John Doe <johndoe@email.com>;"');
                    if (!_.contains(this.bulkEmailFormErrors, item)) {
                        this.bulkEmailFormErrors.push(item);
                    }
                    return;
                }

                //Check to make sure email is not duplicate
                if (_.any(nameEmailList, { emailAddress: emailAddress })) {
                    duplicateEmails.push(emailAddress);
                    return;
                }

                if (name && emailAddress) {
                    nameEmailList.push({ name: name, emailAddress: emailAddress });
                } else {
                    this.bulkEmailFormErrors.push('Invalid Recipient: ' + item);
                }
            });

            if (this.bulkEmailFormErrors.length === 0) {
                var surveyUrl = `${this.fullDomain}/Quality/CustomerSurvey/Survey/`;

                this.apiSvc.save({
                    recipients: nameEmailList,
                    surveyUrl: surveyUrl
                }, 'Surveys?ProgramId=' + programId, false).then((result) => {
                    mixpanel.track('Survey sent', { SurveyProgramId: programId });
                    this.showMailerAlert('success');
                    this.$uibModalInstance.dismiss('sent');
                }, this.onFailure);

                //clear textarea
                this.surveyMail.BulkNameEmails = [];
            } else {
                //add email errors back to textarea               
                this.surveyMail.BulkNameEmails = this.bulkEmailFormErrors.join(';');                
            }                        
        }
    }

    angular.module('app').controller('Common.MailModalSurveyCtrl', MailModalSurveyCtrl);
}