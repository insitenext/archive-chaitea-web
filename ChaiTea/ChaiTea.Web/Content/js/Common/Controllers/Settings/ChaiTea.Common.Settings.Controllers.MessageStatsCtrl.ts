﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Common.Controllers {
    'use strict';

    enum filters {
        All,
        Opened,
        NotOpened
    }

    interface IEmployee {
        EmployeeId: number,
        Name: string,
        JobDescription: string,
        ProfileImage: string,
        IsActive: boolean
    }

    interface IMessage {
        Id: number;
        ProfileImage: string;
        DepartmentName: string;
        To: string;
        Details: string;
        Title: string;
        Site: string;
        When: string;
        LabelColor: string;
        SentDate: Date
    }

    interface IResult {
        TotalRecipients: number,
        Opened: number,
        NotOpened: number
    }

    class MessageStatsCtrl {

        modalInstance;
        messageStatsChart: any = {};
        totalRecipients: number = 0;
        opened: number = 0;
        notOpened: number = 0;
        filter: number = filters.All;
        employees: Array<IEmployee> = [];

        // Paging variables
        recordsToSkip: number = 0;
        top: number = 10;

        isBusy: boolean = false;
        isAllLoaded: boolean = false;
        viewFullMessage: boolean = false;
        siteText: string = '';
        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'message',
            '$uibModal',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.imagesvc',
            '$q',
            'result',
            'fromMessagesReceived',
            'userContext',
            'chaitea.common.services.notificationsvc'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private message: IMessage,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private $q: ng.IQService,
            private result: IResult,
            private fromMessagesReceived: boolean,
            private userContext: IUserContext,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc) {
            this.siteText = this.userContext.Client.Name + ' - ';
            if (this.userContext.Site.ID > 0) {
                this.siteText += this.userContext.Site.Name;
            }
            else {
                this.siteText += 'All Sites';
            }
            this.messageStatsChart = this.chartConfigBuilder('messageStats', {});

            this.getMessageStats();
            this.getMessages();
        }

        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.notificationSvc.errorToastMessage();
            this.$log.error(response);
        }

        public getMessageStats = (): void => {
            this.totalRecipients = this.result.TotalRecipients;
            this.opened = this.result.Opened;
            this.notOpened = this.result.NotOpened;

            var data = [];
            data.push({
                name: 'OPENED MESSAGES', y: this.result.Opened
            });
            data.push({
                name: 'NOT OPENED MESSAGES', y: this.result.NotOpened
            });
            // Build the chart
            this.messageStatsChart = this.chartConfigBuilder('messageStats', {
                series: [
                    {
                        type: 'pie',
                        name: 'messageStats',
                        innerSize: '60%',
                        data: _.map(data, (d, index) => {
                            var sliceName = d['name'],
                                sliceY = d['y'];
                            return {
                                name: sliceName,
                                y: sliceY,
                            }
                        })
                    }],

            });
        }

        public changeFilter = (filterType): void => {
            this.employees = [];
            switch (filterType) {
                case filters.All: this.filter = filters.All;
                    break;

                case filters.Opened: this.filter = filters.Opened;
                    break;

                case filters.NotOpened: this.filter = filters.NotOpened;
                    break;

            }
            this.getMessageReset();
        }

        public getMessageReset = (): void => {
            this.isBusy = false;
            this.isAllLoaded = false;
            this.recordsToSkip = 0;
            this.getMessages();
        }

        public toggleFullMessage = () => {
            this.viewFullMessage = !this.viewFullMessage;
        }

        public getMessages = (): void => {
            var def = this.$q.defer();
            if (this.isBusy || this.isAllLoaded) {
                return;
            }
            this.isBusy = true;

            var endPoint: any;
            var params: any;
            if (this.fromMessagesReceived) {
                endPoint = "MessagesReceived/OrgUserById/:messageId/:top/:skip/:isNew";
                params = {
                    messageId: this.message.Id,
                    top: this.top,
                    skip: this.recordsToSkip,
                    isNew: null
                };
                if (this.filter == filters.Opened) {
                    params['isNew'] = false
                }
                else if (this.filter == filters.NotOpened) {
                    params['isNew'] = true
                }
            }
            else {
                endPoint = "MessageStats/:id"
                params = {
                    id: this.message.Id,
                    $top: this.top,
                    $skip: this.recordsToSkip
                };
                if (this.filter == filters.Opened) {
                    params['$filter'] = `IsNew eq false`
                }
                else if (this.filter == filters.NotOpened) {
                    params['$filter'] = `IsNew eq true`
                }
            }

            var promises = [];
            promises.push(
                this.apiSvc.getByOdata(params, endPoint).then((result) => {
                    if (result.length === 0) {
                        this.isAllLoaded = true;
                    }
                    angular.forEach(result, (item) => {
                        var employeeParams: any;
                        if (this.fromMessagesReceived) {
                            employeeParams = {
                                id: item
                            };
                        }
                        else {
                            employeeParams = {
                                id: item.OrgUserId
                            };
                        }
                        this.apiSvc.getByOdata(employeeParams, "MessageStats/Employee/:id", false, false).then((data) => {
                            this.employees.push({
                                EmployeeId: data.EmployeeId,
                                Name: data.Name ? data.Name : "Unknown",
                                JobDescription: data.JobDescription ? data.JobDescription : "",
                                ProfileImage: (data.UserAttachments ? this.imageSvc.getUserImageFromAttachments(data.UserAttachments, true) : SiteSettings.defaultProfileImage),
                                IsActive: data.IsActive
                            });
                        }, this.onFailure);
                        this.isBusy = true;

                    });

                }, this.onFailure));

            this.$q.all(promises).then((cb) => {
                this.isBusy = false;
                this.recordsToSkip += this.top;
            });
        }
        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        private chartConfigBuilder = (name, options) => {

            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },


            });

            var chartOptions = {

                // Pie Chart
                messageStats: {
                    chart: {
                        height: 220
                    },
                    title: {
                        text: ''
                    },
                    height: 220,
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                        type: '',
                    },
                    tooltip: {
                        style: {
                            padding: 15,
                            fontWeight: 'bold'

                        },

                        legend: {
                            enabled: false,
                        },
                        borderRadius: 15,
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,

                        },
                        series: {
                            allowPointSelect: true,
                            slicedOffset: 0
                        }
                    },
                    series: [
                        {
                            type: 'pie',
                            name: '',
                            innerSize: '60%',
                            size: '90%'

                        }
                    ]
                },

            }
            return _.assign(chartOptions[name], options);
        }

    }

    angular.module("app").controller('Common.MessageStatsCtrl', MessageStatsCtrl);
}