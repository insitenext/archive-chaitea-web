﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Common.Controllers {
    'use strict';

    interface IMessage {
        Id: number;
        ProfileImage: string;
        DepartmentName: string;
        To: string;
        Details: string;
        Title: string;
        Site: string;
        When: string;
        LabelColor: string;
        SentDate: Date
    }

    class SendMessageCtrl {
        modalInstance;
        static $inject = [
            '$scope',
            'sitesettings',
            'blockUI',
            '$log',
            '$window',
            '$uibModalInstance',
            '$http',
            'message',
            '$uibModal',
            'fromMessagesReceived'
        ];
        constructor(
            private $scope,
            sitesettings: ISiteSettings,
            private blockUI,
            private $log: angular.ILogService,
            private $window: ng.IWindowService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private $http: angular.IHttpService,
            private message: IMessage,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private fromMessagesReceived: boolean) {
        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

    }
    angular.module("app").controller('Common.SendMessageCtrl', SendMessageCtrl);
}