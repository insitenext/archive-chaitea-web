﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;

    export interface IInitializeOptions {
        isDatePickerOff: boolean;
    }

    export interface IDateRange {
        startDate?: Date;
        endDate?: Date;
    }

    enum FileType { Undefined, ProfilePicture };

    class HeaderPanelCtrl{
        userId: number = 0;
        user = {
            name: "",
            currentPicture: "",
            phoneNumber: ""
        }
        hasMultipleClientSites: boolean = false;
       
        nowDate: string;

        allUnread = 0;
        counts = {
            unreadActivity:0,
            unreadMessages: 0,
            unreadTodos:0
        }

        notifications: Array<ChaiTea.Components.Interfaces.INotification>;
        messageNotifications: Array<ChaiTea.Components.Interfaces.IMessageNotification>;
        //todos:Array<ITodoNotification>;
        modalInstance;

        isContentModalOpen: boolean = false;

        markingIsBusy:boolean = false;

        currentWeather;

        static $inject = [
            '$scope',
            '$filter',
            '$sce',
            '$window',
            '$compile',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.localdatastoresvc',
            '$translate',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.messagebussvc',
            '$uibModal',
            'WeatherService',
            'chaitea.common.services.imagesvc',
            'chaitea.core.services.usercontextsvc',
            'chaitea.common.services.userinfosvc',
            'ZendeskWidget',
            'chaitea.common.services.utilssvc'
        ];
        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private $sce: angular.ISCEService,
            private $window: angular.IWindowService,
            private $compile: angular.ICompileService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private $translate: angular.translate.ITranslateService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private serviceMessageBus: ChaiTea.Core.Services.IMessageBusSvc,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private WeatherService: ChaiTea.Components.Services.IWeatherService,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private userContextSvc: ChaiTea.Core.Services.IUserContextSvc,
            private userInfoSvc: ChaiTea.Common.Services.IUserInfoSvc,
            private ZendeskWidget: any,
            private utilsSvc: ChaiTea.Common.Services.IUtilitySvc
        ) {

            // check for logout from CDN
            var logoutUser = this.utilsSvc.getQueryParamFromUrl('logout');
            if (logoutUser) {
                this.logOutUser();
                return;
            }

            this.userId = userInfo.userId;
            var unbind = $scope.$watch('dateRange',() => {
                this.onLoadSwitchDateRange();
                unbind();
            });
            var monthUnbind = $scope.$watch('monthDateRange',() => {
                this.onLoadSwitchMonthDateRange();
                monthUnbind();
            });

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('date.selection-' + this.userId.toString());
            if (dateSelection) {
                this.dateRange = <IDateRange> {
                    startDate: moment(dateSelection.startDate).toDate(),
                    endDate: moment(dateSelection.endDate).toDate()
                }
            }
            // Try setting month picker date from session storage.
            var monthDateSelection = this.localDataStoreSvc.getObject('month.date.selection-' + this.userId.toString());
            if (monthDateSelection) {
                this.monthDateRange = {
                    endDate: moment(monthDateSelection.endDate).toDate()
                }
            }
          
            this.getUserClientsAndSites().then(() => {
                var ele = $('.site-picker');
                if (!this.hasMultipleClientSites) {
                    ele.addClass('hidden-employees');
                    $compile(ele)($scope);
                }
            });
            
        }

        /** @description Initializer for the controller. */
        initialize = (options: IInitializeOptions): void => {
           
            this.nowDate = moment().format();

            this.getUserById(this.userContext.UserId);
            //this.getMessageNotifications();
            //this.getTodos();

            //Subscribe to global notification SignalR Hub/channel in ~/Helpers/NotificationHub.cs
            this.notificationSvc.initHub("notification", this.userContext.Client.ID, this.userContext.UserId, this.userInfo.userId);
            
            //watch for changes to localStorage report settings
            this.serviceMessageBus.onMessage('sessionStorage:clear-report-setting', this.$scope,() => {
                this.reportHelperSvc.clearLocalSettings();
            });
            
            //this.serviceMessageBus.onMessage('notification:activity-feed', this.$scope, (message) => {
            //    this.getMessageNotifications();
            //});

            this.WeatherService.autoSetLatLong().then(() => {
                this.WeatherService.currentForecast((err, data) => {
                    if (!data) {
                        this.onFailure('No weather data');
                        return;
                    }
                    this.currentWeather = data.currently;
                    this.currentWeather._icon = this.currentWeather.icon.toLowerCase().indexOf('cloud') > 0 ? 'fa-cloud2' : 'fa-sun3';
                });    
            },this.onFailure);

            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsTodoCount, null, (event, data) => {
                if (data) {
                    this.counts.unreadTodos = data;
                }
            });

            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsActivityCount, null, (event, data) => {
                if (data) {
                    this.counts.unreadActivity = data;
                }
            });

            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsMessagesCount, null, (event, data) => {
                if (data) {
                    this.counts.unreadMessages = data;
                }
            });

            this.$scope.$watch(() => (this.counts), (newVal, oldVal) => {
                if (newVal == oldVal) return;
                this.calcNotificationsCount();
            }, true);


            //Context Picker
            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.ContextPickerOpen, null, (event, data) => {
                if (data) {
                    this.openContextPicker();
                }
            });

            //Current user's User Info changed
            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.UserInfoUpdated, null, () => {
                this.getUserById(this.userContext.UserId, false);
            });

            //Open Feedback Widget
            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.OpenFeedbackWidget, null, () => {
                this.openFeedbackWidget();
            });
        }

        /**
        * @description Set a default date range on initial load.
        * @description Provide it the full date including time and timezone.
        */
        dateRange = <IDateRange> {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').toDate(),
            endDate: new Date()
        }

        monthDateRange = {
            endDate: new Date()
        }

        private goToMyProfile = () => {
            return this.utilsSvc.getWebLink('/personnel/user/' + this.userContext.UserId);
        }

        private goToUserSettings  = () => {
            return this.utilsSvc.getWebLink('/personnel/user-settings');
        }

        private getUserClientsAndSites = () => {
            return this.apiSvc.getLookupList('User/Clients').then((res) => {
                this.hasMultipleClientSites = (res.Clients.Options.length > 1 || res.Sites.Options.length > 1) ? true : false;
            });

        }

        /**
         * @description Returns the userInfo object by user id NOT EMPLOYEE.
         */
        private getUserById = (userId: number, cache?: boolean): void => {

            //get data from service
            this.userInfoSvc.getUserInfo(userId, cache).then((result) => {
                if (!result.userId) return false;
                
                this.user = result;
                
                this.user.name = (result.firstName + " " + result.lastName);

                //TODO [DW] IMAGESVC: might need larger size thumbnail
                this.user.currentPicture = this.imageSvc.getUserImageFromAttachments(result.userAttachments, true);
                //Set userInfo.userId to OrgUserId
                let orgUserId = angular.copy(this.userInfo.userId);
                angular.extend(this.userInfo, this.user);
                this.userInfo.userId = orgUserId;
            });


        }

        public completeTodoItem = (todo) => {
            
        }

        private calcNotificationsCount = () => {
            this.allUnread = _.sum([this.counts.unreadActivity,this.counts.unreadMessages,this.counts.unreadTodos]);
        }
        
        //#region Context Functions
        public openContextPicker = () => {

            if (!this.isContentModalOpen) {
                this.isContentModalOpen = true;
                this.modalInstance = this.userContextSvc.OpenContextPicker();

                this.modalInstance.result.then(() => {
                    this.isContentModalOpen = false;
                });
            }

        }
        //#endregion

        //Used by "logout" button in SideBar.
        //Added to solve the stale date left in the sessionStorage
        public logOutUser = () => {
            var logoutForm = angular.element('#logoutForm');

            if (logoutForm) {
                mixpanel.track('Logged Out', { Email: this.userInfo.userEmail });
                this.messageBusSvc.emitMessage('sessionStorage:clear-date', true);
                logoutForm.submit();
            }
        }

        /**
        * @description Broadcast wrapper.
        * @param {string} eventName
        * @param {obj} obj - data to pass to subscribers.
        */
        broadCastChange = (eventName, obj: IDateRange): void=> {

            // Set active date range for dateRangePicker and store in local storage
            this.localDataStoreSvc.setObject('date.selection-' + this.userId.toString(), obj);

            this.messageBusSvc.emitMessage(eventName, obj);
        }

        onLoadSwitchDateRange = (): void=> {
            if (this.dateRange.endDate) {
                this.broadCastChange('components.headerpanel:datechange',
                    {
                        startDate: moment(this.dateRange.startDate).startOf('month').toDate(),
                        endDate: this.dateRange.endDate
                    });
                //this.$uibModalInstance.close();
            }
        }

        broadCastMonthChange = (eventName, obj: IDateRange): void=> {

            // Set active month for monthPicker and store in local storage
            this.localDataStoreSvc.setObject('month.date.selection-' + this.userId.toString(), obj);

            this.messageBusSvc.emitMessage(eventName, obj);
        }

        onLoadSwitchMonthDateRange = (): void=> {
            if (this.monthDateRange.endDate) {
                this.broadCastMonthChange('components.monthpicker:datechange',
                    {
                        endDate: this.monthDateRange.endDate
                    });
            }
        }

        public openFeedbackWidget = (): void => {
            window.open('https://help.sbminsite.com/hc/en-us/requests/new', '_blank');
        }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }
    }

    angular.module('app').controller('HeaderPanelCtrl', HeaderPanelCtrl);
}