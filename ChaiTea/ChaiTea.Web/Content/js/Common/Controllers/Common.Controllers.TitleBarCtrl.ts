﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    interface IDateModel {
        minDate: string;
        maxDate: Date;
        monthActive: number;
        dateRange?:IDateRange
    }

    class TitlePanelCtrl {
        userId;
        isDatePickerOff: boolean = false;
        datepickers = {
            startDate: false,
            endDate: false
        };
        dateModel: IDateModel = {
            minDate: moment(new Date()).subtract(3, 'years').format(),
            maxDate: new Date(),
            monthActive: 6
        };
        nowDate: string;
        modalInstance;
        static $inject = [
            '$scope',
            '$filter',
            '$sce',
            'userContext',
            'userInfo',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.utilssvc',
            '$log',
            'chaitea.common.services.localdatastoresvc',
            '$translate',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.reportsvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.core.services.messagebussvc',
            '$uibModal'
        ];

        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            private $sce: angular.ISCEService,
            userContext: IUserContext,
            private userInfo: IUserInfo,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private utilitySvc: commonSvc.IUtilitySvc,
            private $log: angular.ILogService,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private $translate: angular.translate.ITranslateService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private reportHelperSvc: commonSvc.IReportSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            this.userId = userInfo.userId;

            this.onClearSessionDate();

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('date.selection-' + this.userId.toString());
            if (dateSelection) {
                this.dateRange = <IDateRange> {
                    startDate: angular.isDate(new Date(dateSelection.startDate)) ? dateSelection.startDate : '',
                    endDate: angular.isDate(new Date(dateSelection.endDate)) ? dateSelection.endDate : ''
                }
            }
        }

        /**
        * @description Set a default date range on initial load.
        * @description Provide it the full date including time and timezone.
        */
        dateRange = <IDateRange> {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').toDate(),
            endDate: new Date()
        }

        /** @description Initializer for the controller. */
        public initialize = (options: IInitializeOptions): void => {
            options = options || { isDatePickerOff: false };
            this.isDatePickerOff = options.isDatePickerOff;
        }

        //#region Old DatePicker Function
        onClearSessionDate = (): void => {
            this.messageBusSvc.onMessage('sessionStorage:clear-date', null,() => {
                this.localDataStoreSvc.setObject('date.selection-' + this.userId.toString(), "");
            })
        }      

        /** 
          * @description Handle opening datepicker. 
          * @param {obj} $event
          * @param {string} which - The name of the datepicker.
          */
        openDatePicker = ($event, which) => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.datePicker.tmpl.html',
                controller: 'DatePickerModalController as vm',
                resolve: {
                    dateModel: () => {
                        return this.dateModel;
                    }
                },
                size: 'sm'
            });

            this.modalInstance.result.then((dateModel: IDateModel) => {
                this.dateModel = dateModel;
                this.dateRange = this.dateModel.dateRange;
            });
        }
        //#endregion
    }

    angular.module("app").controller('TitleBarController', TitlePanelCtrl)

    class DatePickerModalController {
        datepickers ={
            startEnd: false,
            endDate:false
        }

    static $inject = [
            '$scope',
            '$uibModalInstance',
            'userInfo',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.localdatastoresvc',
            'chaitea.core.services.messagebussvc',
            'dateModel'
        ];

        constructor(
            private $scope: angular.IScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private userInfo: IUserInfo,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateModel:IDateModel) {
        }

        public close = () => {
            this.dateModel.dateRange = {
                startDate: moment(new Date()).subtract(this.dateModel.monthActive - 1, 'months').startOf('month').toDate(),
                endDate: new Date()
            };
            this.$uibModalInstance.close(this.dateModel);
        }

        /**
        * @description Set active button and broadcast current month.
        * @description Convert the month into a fromDate and toDate date range.
        * @param {int} month
        * @param {obj} $event
        */
        onSwitchMonthsRange = (month: number, $event): void=> {
            if (month == this.dateModel.monthActive) return;

            this.dateModel.monthActive = month;

            this.broadCastChange('components.headerpanel:datechange', this.dateRange = {
                startDate: moment(new Date()).subtract(month - 1, 'months').startOf('month').toDate(),
                endDate: new Date()
            });

            this.close();
        }

        /**
        * @description Handle date range submit button.
        */
        onSwitchDateRange = (): void=> {
            if (this.dateRange.endDate) {
                this.broadCastChange('components.headerpanel:datechange',
                    {
                        startDate: moment(this.dateRange.startDate).startOf('month').toDate(),
                        endDate: this.dateRange.endDate
                    });
            }
        }

        /**
       * @description Triggered when the fromDate datepicker changes (onBlur).
       */
        startDateOnChange = (): void => {
            this.dateRange.endDate = null;
            this.dateModel.monthActive = 6; // reset

            if (this.dateRange.startDate) {
                this.dateModel.monthActive = 0;

                if (this.dateRange.endDate) return;
                this.dateRange.endDate = new Date();
            }
        }

        /**
        * @description Set a default date range on initial load.
        * @description Provide it the full date including time and timezone.
        */
        dateRange = <IDateRange> {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').toDate(),
            endDate: new Date()
        }

        /**
        * @description Returns the number of months between a date range.
        */
        getMonthsByDateRange = (dateRange: IDateRange): number=> {
            return moment(dateRange.endDate).diff(moment(dateRange.startDate), 'month') + 1;
        }
        
        /** 
          * @description Handle opening datepicker. 
          * @param {obj} $event
          * @param {string} which - The name of the datepicker.
          */
        openDatePicker = ($event, which) => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;
        }

        /**
       * @description Broadcast wrapper.
       * @param {string} eventName
       * @param {obj} obj - data to pass to subscribers.
       */
        private broadCastChange = (eventName, obj: IDateRange): void=> {
            // Collapse the entire date picker
            //$('#date-picker-panel').slideUp(300);

            

            // Set active month and store in local storage
            this.dateModel.monthActive = this.getMonthsByDateRange(obj);
            this.localDataStoreSvc.setObject('date.selection-' + this.userInfo.userId.toString(), obj);

            this.messageBusSvc.emitMessage(eventName, obj);

            this.close();
        }

    }

    angular.module('app').controller('DatePickerModalController', DatePickerModalController);
}