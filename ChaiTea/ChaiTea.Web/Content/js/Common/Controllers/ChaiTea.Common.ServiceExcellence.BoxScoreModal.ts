﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Controllers {

    import trainingSvc = ChaiTea.Training.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import trainingInterfaces = ChaiTea.Training.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    import trainingServices = ChaiTea.Training.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import coreSvc = ChaiTea.Core.Services;
    import personnelInterfaces = ChaiTea.Personnel.Interfaces;
    import qualityInterfces = ChaiTea.Quality.Interfaces;
    import safetyEnums = ChaiTea.Common.Safety.Enums;

    interface IItem {
        topLeftText: string;
        topRightText: string;
        topRightClass: string;
        bodyHeader: string;
        bodyText: string;
        subBodytext: Array<string>;
        leftFooter: string;
        editButtonLink: string;
        viewButtonLink: string;
        isDelete: boolean;
        id: number;
        isHistorical: boolean;
        isExempt: boolean;
        prevIsExempt: boolean;
    }

    interface IKPIComment {
        Comment: string;
        EmployeeId: number;
        EmployeeKpiMasterId: number;
        IsPass: boolean;
        Month: number;
        Year: number;
    }

    interface IKPIQualityAuditsEmployeeScores {
        AreaName: string;
        AverageScoringProfileScore: number;
    }

    interface IKPIQuality {
        Goal: number;
        AverageScore: number;
    }

    interface IEmployeeAdd {
        EmployeeId: number;
        Name: string;
        JobDescription: string;
        ProfileImage: string;
        IsKpiExempted: boolean;
    }

    class SEPBoxScoreModalCtrl {
        basePath: string;
        startDate: any;
        endDate: any;
        isManager: boolean = false;

        modalTitle: string;
        isClosed: boolean;
        kpiComment: IKPIComment;
        tempComment: string;
        employeeAuditScores: Array<IKPIQualityAuditsEmployeeScores> = [];
        qualityScores = <IKPIQuality>{
            Goal: 0,
            AverageScore: 0
        };

        itemList: Array<IItem> = [];
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';

        toUpdate: boolean = false;

        modalInstance;
        myPromise = null;
        loaded: boolean = false;

        exemptKpiFlag: boolean = false;
        exemptItemsArray: Array<any> = [];
        attendenceItems: Array<personnelInterfaces.IAttendanceItem> = [];
        conductItems: Array<personnelInterfaces.IConduct> = [];
        safetyItems: Array<ChaiTea.Common.ApiServices.Interfaces.ISafetyItem> = [];
        compliantItems: Array<ChaiTea.Common.ApiServices.Interfaces.IComplaint> = [];
        reportItItems: Array<ChaiTea.Common.ApiServices.Interfaces.IOwnership> = [];

        feedbackEmployeeItems: Array<qualityInterfces.IFeedbackEmployee> = [];
        isKpiExempted: boolean = false;
        isFinalized: boolean = false;
        currentKpiId: number;
        prevIsKpiExempted: boolean = false;
        promises = [];
        promise: any;
        static $inject = [
            '$scope',
            '$log',
            'userInfo',
            '$uibModalInstance',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            'chaitea.services.credentials',
            'chaitea.services.lookuplist',
            'chaitea.common.services.awssvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.boxscoresvc',
            'chaitea.core.services.dateformattersvc',
            'blockUI',
            'dateRange',
            'employee',
            'module',
            'isScorecardFinalized',
            '$uibModal',
            'chaitea.common.services.reportsvc',
            'isReportCard',
            'year',
            'month',
            'userContext',
            '$filter',
            '$translate',
            '$q'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $log: angular.ILogService,
            private userInfo: IUserInfo,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private credentialSvc: ChaiTea.Services.ICredentialsSvc,
            private serviceLookupListSvc: ChaiTea.Services.ILookupListSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc,
            private utilSvc: ChaiTea.Common.Services.IUtilitySvc,
            private boxScoreSvc: ChaiTea.Common.Services.IBoxScoreSvc,
            private dateFormatterSvc: Core.Services.IDateFormatterSvc,
            private blockUI,
            private dateRange,
            private employee: trainingInterfaces.IEmployeeInfo,
            private module: string,
            private isScorecardFinalized: boolean,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private reportHelperSvc: commonSvc.IReportSvc,
            private isReportCard: boolean,
            private year: number,
            private month: number,
            private userContext: IUserContext,
            private $filter: any,
            private $translate,
            private $q: angular.IQService
            ) {

            this.modalTitle = this.module.toLocaleUpperCase();
            this.basePath = sitesettings.basePath;
            this.isClosed = this.employee.IsClosed;
            this.isFinalized = isScorecardFinalized;
            this.isManager = this.userInfo.mainRoles.managers;

            if (this.isReportCard) {
                this.startDate = dateRange.startDate;
                this.endDate = dateRange.endDate;
            }
            else{
                this.startDate = this.dateFormatterSvc.formatDateFull(moment([this.dateRange.year, this.dateRange.month - 1]));
                this.endDate = this.dateFormatterSvc.formatDateFull(moment(this.startDate).endOf('month'));
            }
            this.getInfoByEmployeeId();
        }

        public getInfoByEmployeeId = (): any => {
            this.itemList = [];
            this.exemptItemsArray = [];

            this.loaded = false;
            switch (this.module) {
                case 'attendance':
                    var params: any = {
                        $orderby: 'DateOfOccurrence desc',
                        $filter: `OrgUserId eq ${this.employee.EmployeeId} and DateOfOccurrence gt DateTime'${this.startDate}' and DateOfOccurrence le DateTime'${this.endDate}'`
                    };
                    this.myPromise = this.apiSvc.getByOdata(params, 'Issue').then((result) => {
                        this.attendenceItems = result;
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            var date = moment(item.DateOfOccurrence);
                            listItem.topLeftText = date.format('MM.DD.YYYY');
                            listItem.topRightText = item.IssueTypeName;
                            listItem.bodyHeader = item.IssueTypeName == "Absent" ? item.IssueTypeName : item.IssueTypeName + ': ' + this.$filter('displayTimezone')(this.$filter('date')(date.toDate(), 'h:mm a', this.userContext.Timezone.OffsetInHours));
                            listItem.bodyText = item.Comment || '-There are no comments-';
                            listItem.subBodytext = [];
                            listItem.subBodytext.push(item.ReasonName ? item.ReasonName : "No Reason");
                            listItem.leftFooter = "";
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Attendance/Entry/' + item.IssueId;
                            listItem.isDelete = true;
                            listItem.isExempt = item.IsExempt;
                            listItem.prevIsExempt = item.IsExempt;
                            listItem.id = item.IssueId;
                            listItem.isHistorical = (moment(item.DateOfOccurrence) < moment(item.CreateDate).date(1).hours(0).minutes(0).seconds(0)) ? true : false;
                            this.itemList.push(listItem);
                        });

                        this.loaded = true;
                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Attendance])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Attendance;
                    break;

                case 'conduct':
                    var params: any = {
                        $orderby: 'DateOfOccurrence desc',
                        $filter: `OrgUserId eq ${this.employee.EmployeeId} and DateOfOccurrence gt DateTime'${this.startDate}' and DateOfOccurrence le DateTime'${this.endDate}'`
                    };
                    this.myPromise = this.apiSvc.getByOdata(params, 'Conduct').then((result) => {
                        this.conductItems = result;
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = this.$filter('date')(item.DateOfOccurrence, 'MM.dd.yyyy', this.userContext.Timezone.OffsetInHours);
                            listItem.topRightText = 'CONDUCT OCCURRENCE';
                            listItem.bodyText = item.Reason || '-There are no comments-';
                            listItem.subBodytext = [];
                            angular.forEach(item.ConductTypes, (conductType) => {
                                listItem.subBodytext.push(conductType.Name);
                            });
                            listItem.leftFooter = "";
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Conduct/Entry/' + item.ConductId;
                            listItem.isDelete = true;
                            listItem.isExempt = item.IsExempt;
                            listItem.prevIsExempt = item.IsExempt;
                            listItem.id = item.ConductId;
                            listItem.isHistorical = (moment(item.DateOfOccurrence) < moment(item.CreateDate).date(1).hours(0).minutes(0).seconds(0)) ? true : false;
                            this.itemList.push(listItem);

                        });

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Conduct])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Conduct;
                    break;

                case 'complaints':
                    var params: any = {
                        $orderby: 'CreateDate desc',
                        $filter: `FeedbackDate ge DateTime'${this.startDate}' and FeedbackDate lt DateTime'${this.endDate}' and AccountableEmployees/any(d:d/EmployeeId eq ${this.employee.EmployeeId})`
                    };

                    this.myPromise = this.apiSvc.getByOdata(params, 'Complaints').then((result: Array<ChaiTea.Common.ApiServices.Interfaces.IComplaint>) => {
                        this.compliantItems = result;
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = moment(item.FeedbackDate).format('MM.DD.YYYY');
                            listItem.topRightText = 'COMPLAINT FILED';
                            listItem.bodyHeader = "Actions taken: " + item.Action;
                            listItem.bodyText = item.Description;
                            listItem.leftFooter = item.ClassificationName;
                            listItem.editButtonLink = this.sitesettings.basePath + 'Quality/Complaint/Entry/' + item.ComplaintId;
                            listItem.isDelete = true;
                            listItem.isExempt = _.find(item.FeedbackEmployeeObjects, { 'EmployeeId': this.employee.EmployeeId }).IsExempt;
                            listItem.prevIsExempt = _.find(item.FeedbackEmployeeObjects, { 'EmployeeId': this.employee.EmployeeId }).IsExempt;
                            listItem.id = item.ComplaintId;
                            listItem.isHistorical = (moment(item.FeedbackDate) < moment(item.CreateDate).date(1).hours(0).minutes(0).seconds(0)) ? true : false;
                            this.itemList.push(listItem);
                        });

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Complaints])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Complaints;
                    break;

                case 'safety':
                    
                    var params: any = {
                        $orderby: 'DateOfIncident desc',
                        $filter: `EmployeeId eq ${this.employee.EmployeeId} and DateOfIncident gt DateTime'${this.startDate}' and DateOfIncident le DateTime'${this.endDate}'`
                    };
                    this.myPromise = this.apiSvc.getByOdata(params, '/Personnel/Safety').then((result: Array<ChaiTea.Common.ApiServices.Interfaces.ISafetyItem>) => {
                        this.safetyItems = result;
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            var date = moment(item.DateOfIncident);
                            var incidentTypes = safetyEnums.StaticIncidentTypes.AsList;
                            listItem.topLeftText = date.format('MM.DD.YYYY');
                            listItem.topRightText = 'INCIDENT_OCCURRENCE';
                            listItem.bodyText = item.Detail || '-There are no details-';
                            listItem.subBodytext = [];
                            angular.forEach(item.IncidentTypes, (incident) => {
                                var findIncident = _.find(incidentTypes, { 'IncidentTypeId': incident });
                                listItem.subBodytext.push(findIncident.Code);
                            });
                            listItem.leftFooter = item.SafetyAttachments.length ? item.SafetyAttachments.length + ' attachments' : '';
                            listItem.viewButtonLink = this.sitesettings.basePath + 'Personnel/Safety/EntryView/' + item.SafetyId;
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Safety/Entry/' + item.SafetyId;
                            listItem.isDelete = true;
                            listItem.isExempt = item.IsExempt;
                            listItem.prevIsExempt = item.IsExempt;
                            listItem.id = item.SafetyId;
                            listItem.isHistorical = (moment(item.DateOfIncident) < moment(item.CreateDate).date(1).hours(0).minutes(0).seconds(0)) ? true : false;
                            this.itemList.push(listItem);
                        });

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Safety])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Safety;
                    break;

                case 'report it':
                    var params: any = {
                        $orderby: 'StatusUpdateDate desc',
                        $filter: `EmployeeId eq ${this.employee.EmployeeId} and StatusUpdateDate gt DateTime'${this.startDate}' and StatusUpdateDate le DateTime'${this.endDate}' and Status eq '1'`
                    };
                    this.myPromise = this.apiSvc.getByOdata(params, 'Ownership').then((result) => {
                        this.reportItItems = result;
                        angular.forEach(result, (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = moment(item.StatusUpdateDate).format('MM.DD.YYYY');
                            listItem.topRightText = item.Status == 1 ? 'Status : Accepted' : 'Status : Pending/Rejected';
                            listItem.bodyText = item.Description;
                            listItem.leftFooter = item.OwnershipAttachments.length ? item.OwnershipAttachments.length + ' attachments' : '';
                            listItem.editButtonLink = this.sitesettings.basePath + 'Personnel/Ownership/Entry/' + item.OwnershipId;
                            listItem.isDelete = false;
                            listItem.isExempt = item.IsExempt;
                            listItem.prevIsExempt = item.IsExempt;
                            listItem.id = item.OwnershipId;
                            listItem.isHistorical = false;
                            this.itemList.push(listItem);
                        });

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Ownership])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Ownership;
                    break;

                case 'quality':
                    var params: any = {
                        startDate: this.dateFormatterSvc.formatDateShort(this.startDate),
                        endDate: this.dateFormatterSvc.formatDateShort(this.endDate),
                        employeeId: this.employee.EmployeeId
                    };
                    this.myPromise = this.apiSvc.query(params, "Report/EmployeeAuditsAvgByAreaByEmployee/:startDate/:endDate/:employeeId").then((result) => {

                        this.reportHelperSvc.getReportSettings().then((d) => {
                            this.qualityScores.Goal = d.ScoringProfileGoal;
                            this.qualityScores.AverageScore = this.employee.EmployeeKpis.filter(t => t.Kpi.Name.indexOf(Common.Interfaces.KPIType[Common.Interfaces.KPIType.Quality]) != -1)[0].Metric;
                        });
                        var listItem: IItem = <IItem>{};
                        if (result.length > 0 && result[0].AreaName != null) {
                            listItem.topLeftText = 'Area';
                            listItem.topRightText = 'Score';
                            listItem.isDelete = false;
                            this.itemList.push(listItem);
                            this.employeeAuditScores = result;
                        }

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name.indexOf(Common.Interfaces.KPIType[Common.Interfaces.KPIType.Quality]) != -1)[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Quality;
                    break;

                case 'professionalism':

                    this.myPromise = this.boxScoreSvc.getProfessionalismModalContent(this.employee.EmployeeId, this.startDate, this.endDate).then((res) => {

                        angular.forEach(res[0], (item) => {
                            var listItem: IItem = <IItem>{};
                            listItem.topLeftText = moment(item.CreateDate).format('MM.DD.YYYY');
                            listItem.topRightText = item.IsPass ? 'Passed' : 'FAILED';
                            listItem.topRightClass = item.IsPass ? 'text-success' : 'text-danger';
                            listItem.bodyText = this.boxScoreSvc.buildProfessionalismContent(item, res[1]);
                            listItem.leftFooter = item.IsEventBasedAudit ? 'Event based' : '';
                            listItem.isDelete = false;
                            listItem.id = item.EmployeeAuditId;
                            listItem.isHistorical = false;
                            this.itemList.push(listItem);
                        });

                        this.loaded = true;

                    }, this.onFailure);

                    this.isKpiExempted = this.employee.EmployeeKpis.filter(t => t.Kpi.Name == Common.Interfaces.KPIType[Common.Interfaces.KPIType.Professionalism])[0].IsExempt;
                    this.currentKpiId = Common.Enums.KPITYPE.Professionalism;
                    break;

                case 'comments':
                    var params: any = {
                        year: this.isReportCard ? this.year : this.dateRange.year,
                        month: this.isReportCard ? this.month : this.dateRange.month,
                        employeeId: this.employee.EmployeeId
                    };
                    this.apiSvc.getByOdata(params, 'KpiComment/:year/:month/:employeeId', false, false).then((result) => {
                        this.tempComment = result.Comment;
                        this.kpiComment = result;

                        this.loaded = true;

                    }, this.onFailure);
                    break;


                default:

                    this.loaded = true;

                    break;

            };
            this.prevIsKpiExempted = this.isKpiExempted;
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.loaded = true;
        }

        /**
        * @description Dismiss the modal instance and cancel all changes
        */
        public cancel = (): void => {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
         * @description Dismiss the modal instance.
         */
        private close = () => {
            this.$uibModalInstance.close(this.toUpdate);
        }

        private save = (): void => {
            var promise: any;
            this.promises = [];
            if (this.isKpiExempted != this.prevIsKpiExempted) {
                var employeeMonthlyKpi = _.find(this.employee.EmployeeKpis, (item) => {
                    return this.currentKpiId == item.Kpi.KpiId;
                });
                promise = this.apiSvc.update(employeeMonthlyKpi.EmployeeMonthlyKpiId, null, 'ServiceExcellence/ExemptKpi/' + this.isKpiExempted).then((result) => {
                    this.toUpdate = true;
                    angular.forEach(this.itemList, (item) => {
                        if (item.prevIsExempt != item.isExempt) {
                            this.exemptIndividualOccurrences(item);
                        }
                    });
                }, this.onFailure);
                this.promises.push(promise);
            }
            else {
                angular.forEach(this.itemList, (item) => {
                    if (item.prevIsExempt != item.isExempt) {
                        this.exemptIndividualOccurrences(item);
                    }
                });
            }

            this.$q.all(this.promises).then(() => {
                this.close();
            });
        };

        private exemptIndividualOccurrences = (item: any) => {
            var kpiItem: any;
            var promise: any;
            switch (this.module) {
                case 'attendance':
                    kpiItem = _.find(this.attendenceItems, (data) => {
                        return data.IssueId == item.id;
                    });
                    kpiItem.IsExempt = item.isExempt;
                    promise = this.apiSvc.update(item.id, kpiItem, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Issue]).then((result) => {
                        this.toUpdate = true;
                    }, this.onFailure);
                    this.promises.push(promise);
                    break;
                case 'conduct':
                    kpiItem = _.find(this.conductItems, (data) => {
                        return data.ConductId == item.id;
                    });
                    kpiItem.IsExempt = item.isExempt;
                    promise = this.apiSvc.update(item.id, kpiItem, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Conduct]).then((result) => {
                        this.toUpdate = true;
                    }, this.onFailure);
                    this.promises.push(promise);
                    break;
                case 'complaints':
                    var complaint = _.find(this.compliantItems, (data) => {
                        return data.ComplaintId == item.id;
                    });
                    kpiItem = _.find(complaint.FeedbackEmployeeObjects, (data) => {
                        return data.EmployeeId == this.employee.EmployeeId;
                    });
                    kpiItem.IsExempt = item.isExempt;
                    promise = this.apiSvc.updateAll(kpiItem, 'Complaints/ExemptComplaint').then((result) => {
                        this.toUpdate = true;
                    }, this.onFailure);
                    this.promises.push(promise);
                    break;
                case 'safety':
                    kpiItem = _.find(this.safetyItems, (data) => {
                        return data.SafetyId == item.id;
                    });
                    kpiItem.IsExempt = item.isExempt;
                    promise = this.apiSvc.update(item.id, kpiItem, 'Personnel/Safety').then((result) => {
                        this.toUpdate = true;
                    }, this.onFailure);
                    this.promises.push(promise);
                    break;
                case 'report it':
                    kpiItem = _.find(this.reportItItems, (data) => {
                        return data.OwnershipId == item.id;
                    });
                    kpiItem.IsExempt = item.isExempt;
                    promise = this.apiSvc.update(item.id, kpiItem, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Ownership]).then((result) => {
                        this.toUpdate = true;
                    }, this.onFailure);
                    this.promises.push(promise);
                    break;
                default:
                    break;
            };
        }


        public saveComment = (): any => {
            this.apiSvc.update(this.kpiComment.EmployeeKpiMasterId, this.kpiComment, 'KpiComment', false).then((result) => {
                this.toUpdate = true;
                bootbox.alert("Comment has been saved");
                this.close();
            }, this.onFailure);
        }

        public delete = (id: number): any => {
            bootbox.confirm("Are you sure you want to delete this entry?", (res) => {
                if (res) {
                    switch (this.module) {
                        case 'attendance':
                            this.apiSvc.delete(id, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Issue]).then((result) => {
                                if (result.IsClosed) {
                                    bootbox.alert("You can not delete attendance issue of the month for which scorecard entry is disabled");
                                }
                                else {
                                    this.getInfoByEmployeeId();
                                }
                            }, this.onFailure);
                            break;
                        case 'conduct':
                            this.apiSvc.delete(id, ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Conduct]).then((result) => {
                                if (result.IsClosed) {
                                    bootbox.alert("You can not delete conduct of the month for which scorecard entry is disabled");
                                }
                                else {
                                    this.getInfoByEmployeeId();
                                }
                            }, this.onFailure);
                            break;
                        case 'complaints':
                            this.apiSvc.delete(id, "ServiceExcellence/DeleteComplaint").then((result) => {
                                if (result.IsClosed) {
                                    bootbox.alert("You can not delete complaint of the month for which scorecard entry is disabled");
                                }
                                else {
                                    this.getInfoByEmployeeId();
                                }
                            }, this.onFailure);
                            break;
                        case 'safety':
                            this.apiSvc.delete(id, 'Personnel/Safety').then((result) => {
                                if (result.IsClosed) {
                                    bootbox.alert("You can not delete safety of the month for which scorecard entry is disabled");
                                }
                                else {
                                    this.getInfoByEmployeeId();
                                }
                            }, this.onFailure);
                            break;
                        default:
                            break;
                    };
                    this.toUpdate = true;
                }
            });
        }

        public setIndividualKpiExempts = () => {
            if (this.isKpiExempted) {
                angular.forEach(this.itemList, (item) => {
                    item.isExempt = true;
                });
            }
            else {
                angular.forEach(this.itemList, (item) => {
                    item.isExempt = false;
                });
            }
        }

        public loadAddPopUps = () => {
            switch (this.module) {
                case 'attendance':
                    this.loadAttendenceModal(this.employee);
                    break;

                case 'conduct':
                    this.loadConductModal(this.employee);
                    break;

                case 'professionalism':
                    this.loadProfessionalismModal(this.employee);
                    break;


                case 'report it':
                    this.loadOwnershipModal();
                    break;

                case 'safety':
                    this.loadSafetyModal();
                    break;

                case 'complaints':
                    this.loadComplaintsNewModal();
                    break;

                default:
                    break;
            }
        }

        public loadAttendenceModal = (employee: trainingInterfaces.IEmployeeInfo) => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: employee.EmployeeId,
                Name: employee.Name,
                JobDescription: employee.JobDescription,
                ProfileImage: employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.attendance.entry.tmpl.html',
                controller: 'Training.AttendanceModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    title: () => { return ''; },
                    employee: () => { return sendEmployee; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    this.toUpdate = true;
                    this.getInfoByEmployeeId();
                }
            });

        }

        public loadConductModal = (employee: trainingInterfaces.IEmployeeInfo) => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: employee.EmployeeId,
                Name: employee.Name,
                JobDescription: employee.JobDescription,
                ProfileImage: employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.conduct.entry.tmpl.html',
                controller: 'Training.ConductModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    title: () => { return ''; },
                    employee: () => { return sendEmployee; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    this.toUpdate = true;
                    this.getInfoByEmployeeId();
                }
            });
        }

        public loadOwnershipModal = () => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: this.employee.EmployeeId,
                Name: this.employee.Name,
                JobDescription: this.employee.JobDescription,
                ProfileImage: this.employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.ownership.entry.tmpl.html',
                controller: 'Training.OwnershipModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    employee: () => { return sendEmployee; }
                }
            });
        }

        public loadSafetyModal = () => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: this.employee.EmployeeId,
                Name: this.employee.Name,
                JobDescription: this.employee.JobDescription,
                ProfileImage: this.employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.safety.entry.tmpl.html',
                controller: 'Training.SafetyModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    title: () => { return ''; },
                    employee: () => { return sendEmployee; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    this.toUpdate = true;
                    this.getInfoByEmployeeId();
                }
            });
        }

        public loadProfessionalismModal = (employee: trainingInterfaces.IEmployeeInfo) => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: employee.EmployeeId,
                Name: employee.Name,
                JobDescription: employee.JobDescription,
                ProfileImage: employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.sep.professionalism.entry.tmpl.html',
                controller: 'Training.AuditModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    title: () => { return ''; },
                    employee: () => { return sendEmployee; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    this.toUpdate = true;
                    this.getInfoByEmployeeId();
                }
            });

        }

        public loadComplaintsNewModal = () => {
            var sendEmployee = <IEmployeeAdd>{
                EmployeeId: this.employee.EmployeeId,
                Name: this.employee.Name,
                JobDescription: this.employee.JobDescription,
                ProfileImage: this.employee.ProfileImage,
                IsKpiExempted: this.isKpiExempted
            };
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/Personnel/personnel.complaintsNew.entry.tmpl.html',
                controller: 'Training.ComplaintsNewModalCtrl as vm',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    title: () => { return ''; },
                    employee: () => { return sendEmployee; }
                }
            }).result.then(toUpdate => {
                if (toUpdate) {
                    this.toUpdate = true;
                    this.getInfoByEmployeeId();
                }
            });
        }

    }

    angular.module("app").controller('Common.SEPBoxScoreModalCtrl', SEPBoxScoreModalCtrl);

}