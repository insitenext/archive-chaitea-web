﻿module ChaiTea.Common.Safety.Enums {

    export enum SafetyIncidentType {
        Unknown = 0,
        WorkplaceInjury = 1,
        NonWorkplaceInjury = 2,
        NearMiss = 3,
        StopTheJob = 4,
        GeneralLiability = 5
    }
    export class StaticIncidentTypes {
        public static AsList =
        [{
            IncidentTypeId: Safety.Enums.SafetyIncidentType.WorkplaceInjury,
            Code: "SAFETY_WORKPLACEINJURY"
        },
        {
            IncidentTypeId: Safety.Enums.SafetyIncidentType.NonWorkplaceInjury,
            Code: "SAFETY_NONWORKPLACEINJURY"
        },
        {
            IncidentTypeId: Safety.Enums.SafetyIncidentType.NearMiss,
            Code: "SAFETY_NEARMISS"
        },
        {
            IncidentTypeId: Safety.Enums.SafetyIncidentType.StopTheJob,
            Code: "SAFETY_STOPTHEJOB"
        },
        {
            IncidentTypeId: Safety.Enums.SafetyIncidentType.GeneralLiability,
            Code: "SAFETY_GENERALLIABILITY"
        }];
    }
}