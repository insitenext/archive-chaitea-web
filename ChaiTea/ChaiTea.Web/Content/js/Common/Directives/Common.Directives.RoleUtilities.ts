﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    import commonSvc = ChaiTea.Common.Services;

    var commonHiddenVisible = (name: string, isHide: boolean = true) => {
        
        var hiddenVisible = function(
            $log: angular.ILogService,
            userInfo: IUserInfo): angular.IDirective {

            return <ng.IDirective>{
                restrict: 'AC',
                link: link
            };

            function link($scope, element, attrs) {
                if (!userInfo.mainRoles) return;

                //showIfRole/skip
                if (userInfo.mainRoles[name] == false && isHide == false) {
                    
                    //[DW] Fixes issue when both managers and training admins are able for visibility
                    //not ideal way.  Should be more dynamic.
                    if ((name === 'managers' && userInfo.mainRoles.managers)
                        || (name === 'trainingadmins' && userInfo.mainRoles.trainingadmins)
                        || ((name === 'trainingadmins') && userInfo.mainRoles.managers && _.includes(attrs.$attr, 'visible-managers'))
                        || ((name === 'managers') && userInfo.mainRoles.trainingadmins && _.includes(attrs.$attr, 'visible-training-admins'))) {
                        return;
                    }

                    if (element) {
                        angular.element(element).replaceWith("<!-- hidden from " + name + "-->");
                    }
                //hideIfRole
                } else if (userInfo.mainRoles[name] && isHide) {
                    //Skip if is Manager AND Employee. **Should be temporary**
                    if ((name == 'employees' && userInfo.mainRoles.managers)) {
                        return;
                    }

                    if (element) {
                        angular.element(element).replaceWith("<!-- hidden from " + name + "-->");
                        element.remove();
                    }
                }
            }
        }
        hiddenVisible.$inject = ['$log', 'userInfo'];
        return hiddenVisible;
    }

    angular.module('app')
        .directive('hiddenEmployees', commonHiddenVisible('employees',true))
        .directive('hiddenCustomers', commonHiddenVisible('customers',true))
        .directive('hiddenManagers', commonHiddenVisible('managers', true))
        .directive('hiddenTrainingAdmins', commonHiddenVisible('trainingadmins', true))
        .directive('hiddenCrazyprogrammers', commonHiddenVisible('crazyprogrammers',true))
        //visible
        .directive('visibleEmployees', commonHiddenVisible('employees',false))
        .directive('visibleCustomers', commonHiddenVisible('customers',false))
        .directive('visibleManagers', commonHiddenVisible('managers', false))
        .directive('visibleTrainingAdmins', commonHiddenVisible('trainingadmins', false))
        .directive('visibleCrazyprogrammers', commonHiddenVisible('crazyprogrammers',false));
}