﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import qualitySvc = ChaiTea.Quality.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    
    /**
     * @description Details filter directive.
     */
    detailsFilter.$inject = [
        '$log',
        '$q',
        'sitesettings'];
    function detailsFilter(
        $log: angular.ILogService,
        $q: angular.IQService,
        sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            replace: true,
            scope: {
                isCollapsed: '=?',              // set visibility, 2-way
                model: '=ngModel',              // bound to the filtered data list
                filterOptions: '=',             // array of filter options
                filterApplyFn: '&',             // fn to invoke filter and refresh data list
                filterClearFn: '&',             // fn to clear filter and refresh data list
                filterToggler: '@',             // toggle component's visibility
                filter: '='                     // filter criteria, 2-way
            },
            bindToController: true,
            controller: ['$scope',function ($scope) {
                // Set vm variables
                this.isCollapsed = this.isCollapsed || false;
                this.onFilterTypeChange = onFilterTypeChange;
                this.onClearFilter = onClearFilter;
                this.onFilterResults = onFilterResults;
                this.isEmpty = isEmpty;

                // Attach toggler
                angular.element(this.filterToggler).bind('click',($event) => {
                    $event.preventDefault();
                    $scope.$apply(() => {
                        this.isCollapsed = !this.isCollapsed;
                    });
                    return false;
                });

                if (this.filter.filterType != null) {
                    this.onFilterTypeChange();
                }
            }],
            controllerAs: 'vm',
            templateUrl: `${sitesettings.basePath}Content/js/Common/Templates/detailsFilter.tmpl.html`,
            link: ['scope', 'element', 'attrs', 'ctrl', link]
        };

        
        function link(scope, element: angular.IRootElementService, attrs: angular.IAttributes, ctrl: angular.INgModelController) {
            scope.$watch(() => { return scope.model;},() => {
                onModelChange(scope, ctrl);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            var mdl = model.$viewValue;

        }

        /**
         * @description Handler when filter type changes.
         */
        function onFilterTypeChange() {
            var selected = this.filter.filterType;
            this.activeFilterOptions = {};
            this.filter.filterCriteria = this.filter.filterCriteria ? this.filter.filterCriteria : '';

            var filterOptns = _.find(this.filterOptions, { Key: selected });
            if (filterOptns) {
                this.activeFilterOptions = filterOptns;
            }
        }

        /**
         * @description Handler to refresh data based on filter criteria.
         */
        function onFilterResults($event) {
            if (!this.filter.filterCriteria) return;

            this.filterApplyFn();
        }
        
        /**
         * @description Handler to clear the filter.
         */
        function onClearFilter($event) {
            // Reset the filter
            this.filter.filterType = null;
            this.filter.filterCriteria = null;
            this.activeFilterOptions = {};

            this.filterClearFn();
        }

        /**
         * @description Helper to check if object is empty.
         */
        function isEmpty(obj) {
            return _.isEmpty(obj);
        }

    }
    angular.module('app').directive('sbmDetailsFilter', detailsFilter);
}