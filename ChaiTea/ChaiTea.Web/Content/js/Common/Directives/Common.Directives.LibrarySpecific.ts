﻿namespace ChaiTea.Common.Directives {


    /*
    * @description ** FOR USE WITH UI Select in UI Grid **
    * @description Fixes issue of dropdown NOT closing when clicking off.
    */
    var uiSelectWrap = function($document, uiGridEditConstants) {
        return function link($scope, $elm, $attr) {
            $document.on('click', docClick);

            function docClick(evt) {
                if ($(evt.target).closest('.ui-select-container').length === 0) {
                    $scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
                    $document.off('click', docClick);
                }
            }
        };
    }

    uiSelectWrap.$inject = ['$document', 'uiGridEditConstants'];
    angular.module('app').directive('uiSelectWrap', uiSelectWrap);

    /*
    * @description ** Used for Zooming in and out of images **
    * @description Plugin Name: Zooming ; link : https://github.com/kingdido999/zooming
    */
    var imageZoom = function ($window) {
        return function link($scope, $elm, $attr) {
            if ($window.Zooming) {
                if ($elm) {
                    if ($elm.length && $elm.length > 0) {
                        $window.Zooming.listen($elm[0]);
                    }
                    else {
                        $window.Zooming.listen($elm);
                    }
                }
            }
        };
    }

    imageZoom.$inject = ['$window'];
    angular.module('app').directive('imageZoom', imageZoom);

    var configImage = function ($window): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'A',
            scope: {
                scale: '='
            },
            link: link
        };
        function link($scope, $elm, $attr) {
            $window.Zooming.config({
                'scaleBase': $scope.scale,
                'bgOpacity': 0.9,
                'scaleExtra': 1
            });
        };
    }

    configImage.$inject = ['$window'];
    angular.module('app').directive('configImage', configImage);

}