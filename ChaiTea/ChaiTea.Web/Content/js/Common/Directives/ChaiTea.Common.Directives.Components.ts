﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    import coreSvc = Core.Services;
    import commonSvc = Common.Services;
    import commonInterfaces = Common.Interfaces;

    enum KEYCODES {
        backspace = 8,
        tab = 9,
        zero = 48,
        nine = 57,
        backtick = 96,
        i = 105,
        dot = 190
    }

    //#region Date Picker
    /**
     * @description Date picker element wrapper to eliminate redundant declaration
     */
    sbmDatePicker.$inject = ['$log', '$timeout', '$compile'];
    function sbmDatePicker(
        $log: angular.ILogService,
        $timeout: angular.ITimeoutService,
        $compile: angular.ICompileService): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            //require: 'ngModel',
            scope: {
                model: '=ngModel',
                minDate: '=?',
                maxDate: '=?',
                valDateGreaterThan: '=?',
                isRequired: '@',
                id: '@',
                placeholder: '@',
                initDate: '@',
            },
            link: link,
            template:
            `<div class="input-group date-picker"><input id="{{id}}" name="{{id}}" type="text" class="form-control"
                    data-ng-required="{{isRequired}}"
                    data-ng-model="model"
                    datepicker-append-to-body="true"
                    uib-datepicker-popup="MM/dd/yyyy"
                    placeholder="{{placeholder}}"
                    is-open="isOpen" close-text="Close" min-date="minDate" max-date="maxDate" init-date="initDate" data-ng-blur="checkDateAge(model)" data-ng-click="openDatePicker($event)"/>
                    <span class="input-group-addon" data-ng-click="openDatePicker($event)">
                        <i class="glyphicon-calendar glyphicon"></i>
                    </span>
                </div>`
        };

        function link($scope, element, attrs) {
            $scope.model = $scope.model;
            $scope.isOpen = false;
            //this.oldModelValue = new Date();
            // Additional validation hooks
            // Has performance issue so might not be worth keeping
            //var $input = $element.children('input');
            //$scope.$watch('valDateGreaterThan',(val) => {
            //    $input.attr('val-date-greater-than', val);
            //    $compile($input)($scope);
            //});

            // Additional events
            $scope.openDatePicker = openDatePicker;

            $scope.checkDateAge = function () {
                if (!$scope.model) return;

                var parsedDate = new Date($scope.model); //Date.parse(newVal);
                var nowYear = new Date().getFullYear();
                if (parsedDate && (parsedDate.getFullYear() < nowYear - 2)) {
                    //TODO display message to of WHY date year failed
                    var fixedDate = parsedDate.setFullYear(nowYear);
                    $scope.model = fixedDate;
                }
            };
        }

        /**
         * @description Open the datepicker.
         */
        function openDatePicker($event): void {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                angular.element($event.currentTarget).prev('input').click();
            }, 0, false);
            this.isOpen = true;
        }


    }
    angular.module('app').directive('sbmDatePicker', sbmDatePicker);    
    //#endregion

    //#region Numeric only
    /**
     * @description Decorator directive to only allow numeric values, non-float.
     */
    sbmNumericOnly.$inject = ['$log'];
    function sbmNumericOnly(
        $log: angular.ILogService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            scope: true,
            link: link
        };

        function link($scope, $element, $attrs) {
            $element.bind('keydown', event => {
                var charCode = (event.which) ? event.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        }
    }
    angular.module('app').directive('sbmNumericOnly', sbmNumericOnly);    
    //#endregion

    //#region Monthly Date Picker
    /**
     * @description Monthly Date picker element wrapper
     */
    sbmMonthlyPicker.$inject = [
        '$log',
        '$compile',
        'userInfo',
        'chaitea.core.services.messagebussvc',
        'chaitea.common.services.localdatastoresvc',
        'chaitea.common.services.notificationsvc'
    ];

    function sbmMonthlyPicker(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        userInfo: IUserInfo,
        messageBusSvc: Core.Services.IMessageBusSvc,
        localDataStoreSvc: Common.Services.ILocalDataStoreSvc,
        notificationSvc: Common.Services.INotificationSvc): angular.IDirective {
        var that = this;

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            scope: {
                model: '=ngModel',
                onChangeFn: '=',
                hideButtons:'@'
            },
            template: `
                <div class="monthly-date-picker">
                    <a href="javascript:void(0);" class="prev-month pull-left" ng-click="prevMonth($event)" ng-hide="hideButtons" ng-if="showBackButton">
                        <i class="fa-chevron-left"></i>
                    </a>
				    <span class="text-uppercase">{{currentMonth | date: 'MMMM yyyy'}}</span>
					<a href="javascript:void(0);" class="next-month pull-right" ng-click="nextMonth($event)" ng-hide="hideButtons" ng-if="showForwardButton">
                        <i class="fa-chevron-right"></i>
                    </a>
                </div>
            `,
            link:  link
        };

        

        function link($scope, element: angular.IRootElementService, attrs: angular.IAttributes) {

            var date: any;
            var initDate: any;
            $scope.currentMonth;
            $scope.showBackButton = $scope.showForwardButton = true;
            if (!$scope.model) {
                $scope.model = $scope.$parent.vm.dateRange;
            }
            date = moment($scope.model.endDate);

            $scope.currentMonth = initDate = date.toDate();
            
            $scope.prevMonth = ($event) => {
                $scope.currentMonth = moment($scope.currentMonth).subtract(1, 'month').endOf('month').toDate();
                $scope.model.endDate = $scope.currentMonth;
                broadCastChange('components.monthpicker:datechange',
                    {
                        endDate: $scope.model.endDate
                    });
            }

            $scope.nextMonth = ($event) => {
                $scope.currentMonth = moment($scope.currentMonth).add(1, 'month').endOf('month').toDate();
                
                if ((($scope.currentMonth.getMonth() > moment().month()) && ($scope.currentMonth.getFullYear() >= moment().year()))
                    || ($scope.currentMonth.getFullYear() > moment().year())) {
                    $scope.currentMonth = moment($scope.currentMonth).subtract(1, 'month').endOf('month').toDate();
                    notificationSvc.displayToast({
                        message: 'You cannot go further ahead than the current month.',
                        type: Common.Services.NOTIFICATION_TYPE.MESSAGE,
                        display: "warning"
                    }, {})
                    return false;
                }else{
                    $scope.model.endDate = $scope.currentMonth;
                    broadCastChange('components.monthpicker:datechange',
                        {
                            endDate: $scope.model.endDate
                        });
                }
            }

            //Used from SEP index to hide back button when there are no employees to list
            messageBusSvc.onMessage('components.headerpanel:monthly-date:toggle-backbutton', $scope,(event, obj) => {
                console.log('sep mess',event,obj)
                $scope.showBackButton = obj.show;
            });


            function broadCastChange(eventName, obj: any) {
                localDataStoreSvc.setObject('month.date.selection-'+userInfo.userId, obj);
                messageBusSvc.emitMessage(eventName, obj);
            }
        }



    }

    angular.module('app').directive('sbmMonthlyPicker', sbmMonthlyPicker);
    //#endregion

    //#region Date picker formatter
    /**
     * @description Decorator directive to automatically format date picker inputs.
     */
    function datepickerPopup() {
        return <ng.IDirective>{
            restrict: 'EAC',
            require: 'ngModel',
            link: ($scope, $element, $attrs, controller:any) => {
                controller.$formatters.shift();
            }
        }
    }
    angular.module('app').directive('datepickerPopup', datepickerPopup);
    //#endregion

    //#region Disable animate for an element
    /**
    * @description Decorator to disable animate.
    */
    disableAnimation.$inject = ['$animate'];
    function disableAnimation($animate) {
        return <ng.IDirective>{
            restrict: 'A',
            link: ($scope, $element, $attrs) => {
                $attrs.$observe('disableAnimation', value => {
                    $animate.enabled(!value, $element);
                });
            }
        }
    }
    angular.module('app').directive('disableAnimation', disableAnimation);   
    //#endregion

    //#region Grid sorting
    /**
    * @description Decorator to add sorting to a grid.
    */
    sortableGrid.$inject = [];
    function sortableGrid() {

        return <ng.IDirective>{
            restrict: 'A',
            scope: {
                order: '=',
                by: '=',
                reverse: '='
            },
            transclude: true,
            template:
            `<a ng-click="sort()">
                    <span ng-transclude></span>
                </a>&nbsp;
                <i class="glyphicon" ng-class="{\'fa fa-sort-asc\' : order===by && !reverse,  \'fa fa-sort-desc\' : order===by && reverse}"></i>`,
            link: link
        }

        function link($scope, $element, $attrs) {
            $scope.sort = () => {
                if ($scope.order === $scope.by) {
                    $scope.reverse = !$scope.reverse;
                } else {
                    $scope.by = $scope.order;
                    $scope.reverse = false;
                }
            }
        }
    }
    angular.module('app').directive('sortableGrid', sortableGrid);    
    //#endregion

    //#region Score input
    /**
    * @description Component directive for score input.
    */
    scoreInput.$inject = [];
    function scoreInput() {

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                isRequired: '=',
                isReadonly: '=',
                scaleMax: '=',
                increment: '=',
                placeHolder: '@',
                onDecrement: '&',
                onIncrement: '&'
            },
            transclude: true,
            template:
            `<div class="input-group col-md-11 col-xs-11 score-input-group">
                <span class="input-group-btn">
                    <button data-ng-click="decrementScore($event)" type="button" class="btn btn-orange ">
                        <span class="glyphicon glyphicon-minus"></span>
                    </button>
                </span>
                <input type="number" data-ng-required="isRequired" data-ng-readonly="isReadonly" ng-model="model" class="form-control" placeholder="{{::placeHolder}}" data-ng-keydown="onKeyDown($event)" />
                <span class="input-group-btn">
                    <button data-ng-click="incrementScore($event)" type="button" class="btn btn-orange">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </span>
            </div>`,
            link: link
        }

        function link($scope, $element, $attrs, ngModel) {
            var min = 0,
                max = $scope.scaleMax,
                increment = $scope.increment;

            // Decrement score
            $scope.decrementScore = $event => {
                if ($event) $event.preventDefault();

                var scoreNum = $scope.model || min;

                if (scoreNum > min) {
                    $scope.model = (scoreNum - increment);
                } else if (scoreNum == min) {
                    $scope.model = null;
                }
                $scope.onDecrement($scope.model);
            };

            // Increment score
            $scope.incrementScore = $event => {
                if ($event) $event.preventDefault();

                var scoreNum = $scope.model || min;

                if (scoreNum < max) {
                    scoreNum = scoreNum * 1 + increment;
                    $scope.model = scoreNum;
                }
                $scope.onIncrement($scope.model);
            };

            $scope.$watch('model',(newVal, oldVal) => {
                if (newVal > max || newVal == 0) {
                    ngModel.$setViewValue(oldVal);
                    ngModel.$render();
                }
            });

            $scope.onKeyDown = $event => {
                var key = $event.which || $event.keyCode;
                var modelValue = $scope.model || 0;
          
                // Allow numbers and backspace
                if (key != 9 && key != 8 && (key < 48 || key > 57) && (key < 96 || key > 105)) {
                    $event.preventDefault();
                    return false;
                }

                return true;
            }

        }

    }
    angular.module('app').directive('scoreInput', scoreInput);
    //#endregion

    //#region Score input with goal.
    /**
    * @description Component directive for score input with goal button. Increment is pre-defined based on max scale.
    * increment = 0.5 for max <=10, increment = 5 for max > 10
    */
    scoreInputWithGoal.$inject = [];
    function scoreInputWithGoal() {

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                isRequired: '=',
                isReadonly: '=',
                scaleMax: '=',
                scaleMin: '=',
                goal: '=',
                placeHolder: '@',
                onDecrement: '&',
                onIncrement: '&'
            },
            transclude: true,
            template:
            `<div class="numb-field">
                <div class="row">
                    <div class="col-xs-6">
                        <a data-ng-click="decrementScore($event)" class="ai-numb-field-btn btn btn-primary desbtn"><span class="glyphicon glyphicon-minus"></span></a>
                        <input type="number" data-ng-required="isRequired" data-ng-readonly="isReadonly" ng-model="model" class="basic-input" placeholder="{{::placeHolder}}" data-ng-keydown="onKeyDown($event)" />
                        <a data-ng-click="incrementScore($event)" class="ai-numb-field-btn btn btn-primary addbtn"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>
                    <div class="col-xs-6">
                        <a class="ai-numb-field-goal btn btn-success" href="javascript:void(0);" data-ng-click="setToGoal($event)">Meet Goal</a>
                    </div>
                </div>
            </div>`,
            link: link
        }

        function link($scope, $element, $attrs, ngModel) {
            var min = $scope.scaleMin || 0,
                max = $scope.scaleMax,
                increment = $scope.scaleMax <= 10 ? 0.5 : 5;
            
            // Decrement score
            $scope.decrementScore = $event => {
                if ($event) $event.preventDefault();

                var scoreNum = $scope.model || min;

                if (scoreNum > min) {
                    scoreNum = scoreNum * 1;
                    $scope.model = ((scoreNum % increment) !== 0 ? (scoreNum - (scoreNum % increment)) : (scoreNum - increment));
                } else if (scoreNum <= min) {
                    $scope.model = min;
                }

                $scope.onDecrement($scope.model);
            };

            // Increment score
            $scope.incrementScore = $event => {
                if ($event) $event.preventDefault();

                var scoreNum = $scope.model || min - increment || 0;

                if (scoreNum < max) {
                    scoreNum = scoreNum * 1;
                    $scope.model = ((scoreNum % increment) !== 0 ? ((scoreNum - (scoreNum % increment)) + increment) : (scoreNum + increment));
                }
                $scope.onIncrement($scope.model);
            };

            $scope.$watch('model', (newVal, oldVal) => {
                //DW Posible that newVal = 0 == true
                if (newVal > max || newVal == 0 || newVal === undefined) {
                    ngModel.$setViewValue(oldVal);
                    ngModel.$render();
                }
            });
            $scope.onKeyDown = $event => {
                var key = $event.which || $event.keyCode;
                var modelValue = $scope.model || 0;
                if ($scope.model < min && key == KEYCODES.dot) {
                    $event.preventDefault();
                    return true;
                }
                // Allow numbers and backspace && decimal point
                if (key != KEYCODES.tab && key != KEYCODES.backspace && key != KEYCODES.dot && (key < KEYCODES.zero || key > KEYCODES.nine) && (key < KEYCODES.backtick || key > KEYCODES.i)) {
                    $event.preventDefault();
                    return false;
                }
                return true;
            }

            $scope.setToGoal = $event => {
                if ($event) $event.preventDefault();
                $scope.model = $scope.goal;
            }
        }

    }
    angular.module('app').directive('scoreInputWithGoal', scoreInputWithGoal);
    //#endregion

    //#region Panel collapse
    /**
     * @description Component directive to collapse panel.
     */
    sbmPanelCollapser.$inject = ['$log', '$window', '$timeout', 'chaitea.core.services.messagebussvc'];
    function sbmPanelCollapser(
        $log: angular.ILogService,
        $window: angular.IWindowService,
        $timeout: angular.ITimeoutService,
        messageBusSvc: coreSvc.IMessageBusSvc): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            transclude: true,
            template:
            `<div class="panel panel-default">
                    <h3 class="panel-header-txt">{{ title }}
                        <a class="btn btn-default btn-sm add-btn pull-right" href="javascript:void(0);" data-ng-click="collapseIt($event)">
                            <i style="color: #000;" class="fa fa-1" data-ng-class="{ \'fa-chevron-right\': isCollapsed, \'fa-chevron-down\': !isCollapsed }"></i>
                        </a>
                    </h3>
                    <div uib-collapse="isCollapsed">
                        <ng-transclude></ng-transclude>
                    </div>
                </div>`,
            scope: {
                isCollapsed: '=',
                title: '@'
            },
            link: link
        };

        function link($scope, $element, $attrs) {
            var $win: JQuery = $($window);

            // Collapse/uncollapse a panel individually
            $scope.collapseIt = ($event) => {
                $event.preventDefault();
                $scope.isCollapsed = !$scope.isCollapsed;

                // Trigger resize handler so components can re-adjust
                $timeout(() => { $win.triggerHandler('resize') });
                return false;
            }

            // Toggle collapse state from external broadcasts
            messageBusSvc.onMessage('components.panel:collapse', $scope,(event, obj) => {
                $scope.$apply(() => {
                    $scope.isCollapsed = obj.isCollapsed;
                });
            });
        }

    }
    angular.module('app').directive('sbmPanelCollapser', sbmPanelCollapser);
    //#endregion

    //#region Panel collapser
    /**
    * @description Component directive to attach a collapse event to a button.
    */
    sbmPanelCollapserCaller.$inject = ['$log', 'chaitea.core.services.messagebussvc'];
    function sbmPanelCollapserCaller(
        $log: angular.ILogService,
        messageBusSvc: coreSvc.IMessageBusSvc): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'E',
            scope: {
                isCollapsed: '=?'
            },
            transclude: true,
            replace: true,
            template:
            `<a class="btn btn-default btn-sm add-btn" href="javascript:void(0);">
                    <i style="color: #000;" class="fa fa-1" data-ng-class="{ \'fa-chevron-down\': isCollapsed, \'fa-chevron-right\': !isCollapsed }"></i>
                    <ng-transclude></ng-transclude>
                </a>`,
            link: link
        };

        function link($scope, $element, $attrs) {
            $scope.isCollapsed = $scope.isCollapsed || false;

            // Broadcast a message to collapse/uncollapse to all listeners
            $element.bind('click', $event => {
                $event.preventDefault();
                $scope.isCollapsed = !$scope.isCollapsed;

                messageBusSvc.emitMessage('components.panel:collapse', { isCollapsed: $scope.isCollapsed });
                return true;
            });
        }
    }
    angular.module('app').directive('sbmPanelCollapserCaller', sbmPanelCollapserCaller);    
    //#endregion

    //#region Resize an element
    /**
     * @description Resize an element based on another element or component.
     */
    resizeApply.$inject = ['$log', '$window'];
    function resizeApply(
        $log: angular.ILogService,
        $window: angular.IWindowService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            scope: true,
            link: link
        };

        function link($scope, $element, $attrs) {
            var $win = angular.element($window),
                offset = $attrs.resizeOffset || +0,
                maxWidth = parseInt($attrs.resizeMaxWidth),
                otherElem = angular.element($attrs.resizeApply);

            var newWidth = () => calculateWidth($win, offset, otherElem, maxWidth);

            $element.css('width', newWidth());

            // Apply resize on window resize
            $win.bind('resize', t => {
                $scope.$apply(() => {
                    $element.css('width', newWidth());
                });
            });
        }

        /**
         * @description Calculate the new width based on an element and given offset.
         */
        function calculateWidth($window, offset, $otherElem, maxWidth) {
            var otherElemWidth = $otherElem.width(),
                newWidth = eval(otherElemWidth + offset);

            return (newWidth >= maxWidth) ? maxWidth : newWidth;
        }

    }
    angular.module('app').directive('resizeApply', resizeApply);
    //#endregion

    //#region Quick pager
    /**
     * @description Quick pager with previous and next buttons.
     */
    paginator.$inject = ['$log'];
    function paginator(
        $log: angular.ILogService,
        $window: angular.IWindowService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'E',
            require: 'ngModel',
            template:
            `<nav>
                    <ul class="pager">
                    <li><a href="javascript:void(0);" data-ng-click="previous($event)">Previous</a></li>
                    <li><a href="javascript:void(0);" data-ng-click="next($event)">Next</a></li>
                    </ul>
                </nav>`,
            scope: {
                model: '=ngModel',
                top: '@',
                pagePrev: '&',
                pageNext: '&'
            },
            link: link
        };

        function link($scope, $element, $attrs) {
            $scope.previous = previous;
            $scope.next = next;
            $scope.reset = reset;

            $scope.$watch('model',(newVal, oldVal) => {

            });
        }

        function reset() {

        }

        function previous($event) {
            if ($event) $event.preventDefault();
            this.pagePrev();
        }

        function next($event) {
            if ($event) $event.preventDefault();
            this.pageNext();
        }

    }
    angular.module('app').directive('paginator', paginator);
    //#endregion

    //#region sbmCard
    /**
    * @description Component directive for cards on Complaints, Compliments and Customer Survey.
    */
    sbmCard.$inject = ['sitesettings'];
    function sbmCard(sitesettings: ISiteSettings) {

        return <ng.IDirective>{
            restrict: 'E',
            replace: false,
            //require: 'ngModel',
            scope: {
                model: '=ngModel',
                isOpen: '=?',
                header: '=?',
                headerSubtext: '=?',
                footer: '=?',
                footerSubtext: '=?',
                refUrl: '=?'
            },
            transclude: true,
            templateUrl: `${sitesettings.basePath}Content/js/Common/Templates/sbmCard.tmpl.html`,
            link: link
        }

        function link($scope, $element, $attrs, ngModel) {
        }

    }
    angular.module('app').directive('sbmCard', sbmCard);
    //#endregion

    //#region Select picker
    /**
     * @description Select picker plug-in
     * https://github.com/silviomoreto/bootstrap-select
     */
    selectpicker.$inject = ['$log', '$timeout', '$parse'];
    function selectpicker(
        $log: angular.ILogService,
        $timeout: angular.ITimeoutService,
        $parse: angular.IParseService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            scope: {
                selectpicker: '='
            },
            link: link
        };

        function link($scope, $element, $attrs) {
            angular.element($element)['selectpicker']();

            var watcher: Function = $scope.$watch('selectpicker',(val, oldVal) => {
                $timeout(() => {
                    // Set value and refresh UI
                    angular.element($element)['selectpicker']('val', val);
                    angular.element($element)['selectpicker']('refresh');
                });
            });
        }
    }
    angular.module('app').directive('selectpicker', selectpicker);
    //#endregion

    //#region Image Lightbox Popup
    /**
     * @description Default Image Bootstrap Popup using modal
     */
    //imageLightbox.$inject = ['$log', '$uibModal','sitesettings'];
    //function imageLightbox(
    //    $log: angular.ILogService,
    //    $uibModal: angular.ui.bootstrap.IModalService,
    //    sitesettings:ISiteSettings
    //): angular.IDirective {
    //    return <ng.IDirective>{
    //        restrict: 'EA',
    //        link: link
    //    };

    //    function link($scope, $element, $attrs) {
    //        var _imageUrl = $element.attr('src');

    //        if ($scope.imageURL)
    //            _imageUrl = $scope.imageURL;

    //        //var offset = ($(window).height() - $dialog.height()) / 2;
    //        $uibModal.open(<ng.ui.bootstrap.IModalSettings>{
    //            templateUrl: `${sitesettings.basePath}Content/js/Common/Templates/modal.imageLightbox.tmbl.html`,
    //            controller:function($scope) {
                    
    //            }
    //        })
    //    }
    //}
    //#endregion

    //#region sbmToggleSwitch
    /**
     * @description Toggle switch. 
     * loosely based off of https://github.com/JumpLink/angular-toggle-switch
     */

    function sbmToggleSwitch() {
        var buttonClicked = false;

        return <ng.IDirective>{
            restrict: 'EA',
            replace: false,
            transclude: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                fieldName: '@',
                onLabel: '@',
                onCount: '@',
                offLabel: '@',
                offCount: '@',
                knobLabel: '@',
                trueItemValue: '@',
                falseItemValue: '@',
                filterApplyFn: '&', // fn to invoke filter and refresh data list
                
            },
            bindToController: true,
            template:
            `<span ng-transclude></span>
                <div class="btn-group radio-btns" data-toggle="buttons">
                        <label ng-repeat="button in vm.buttons" class="col-xs-6 btn btn-primary toggle-switch-style" data-ng-class="{'active': button.active}" data-ng-click="vm.onFilterResults($event, button.value);">
                            <input name="toggleSwitchInput" type="radio" class="cus-radio ng-pristine ng-untouched ng-valid" ng-value="button.value" data-ng-model="vm.model"  autocomplete="off" >
                            {{button.label}}
                            <span class="badge badge-default" ng-if="button.count">{{button.count}}</span>
                        </label>
                    </div>`,
            controller: ['$scope', function ($scope) {
                //set vm vars
                this.onFilterResults = onFilterResults;
                this.fieldName = this.fieldName || 'toggleSwitch-' + this.$id;
                this.trueItemValue = this.trueItemValue || true;
                this.falseItemValue = this.falseItemValue || false;
                this.onLabel = this.onLabel || 'On';

                this.model = $scope.vm.model;

                setButtons($scope);
                
                $scope.$watch(() => { return $scope.vm.model; }, (data) => {
                    onModelChange($scope, data);
                    setButtons($scope);
                    });
            }],
            controllerAs: 'vm'
        };

        
        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            var mdl = model.$viewValue;
            if (model != null && buttonClicked) {
                scope.vm.filterApplyFn();
            }
        }
         /**
         * @description Sets button values
         * @param {obj} scope - The current scope.
         */
        function setButtons(scope) {

            scope.vm.buttons = [
                {
                    label: scope.vm.onLabel,
                    value: scope.vm.trueItemValue,
                    count: scope.vm.onCount,
                    active: (scope.vm.model == eval(scope.vm.trueItemValue))
                }
            ];

            // Hide 2nd button if offLabel is empty
            if (scope.vm.offLabel) {
                scope.vm.buttons.push({
                    label: scope.vm.offLabel,
                    value: scope.vm.falseItemValue,
                    count: scope.vm.offCount,
                    active: (scope.vm.model == eval(scope.vm.falseItemValue))
                });
            }
        }

        /**
         * @description Handler to refresh data based on filter criteria.
         */
        function onFilterResults($event, btnVal: boolean) {
            buttonClicked = true; //fix for init model change
            this.model = (String(btnVal) === 'true' ? true : false); //fix for value type
        }
    }

    angular.module('app').directive('sbmToggleSwitch', sbmToggleSwitch);
    //#endregion

    cfImage.$inject = ['chaitea.common.services.imagesvc'];
    function cfImage(imageSvc: ChaiTea.Common.Services.IImageSvc) {

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            transclude: true,
            template: "<img ng-transclude/>",
            scope: {
                publicId: '@',
                width: '@',
                fullscreen: '@'
            },
            link: link
        };


        function link($scope, $element: angular.IRootElementService, $attrs: angular.IAttributes) {

            var url = imageSvc.get($scope.publicId);

            $element.attr("width", $scope.width);
            $element.attr('src', url);

            if ($scope.fullscreen === 'true') {
                $element.attr("style", 'margin: 0 auto;');
            }

            $attrs.$observe('publicId', value => {
                var url = imageSvc.get($scope.publicId);
                $element.attr('src', url);
            });

        }
    }

    angular.module('app').directive('cfImage', cfImage);


    //#region SBM SignaturePad
    function sbmSignaturePad() {
        link.$inject = ['$scope', 'element', 'attrs'];
        
        return <ng.IDirective>{
            restrict: 'A',
            replace: true,
            require: 'ngModel',
            scope: {
               model: '=ngModel'
            },
            link: link
        };

        
        function link($scope, element: angular.IRootElementService, attrs: angular.IAttributes) {
            var pad_content = angular.element('<canvas class="full-screen-signature-pad" ng-signature-pad="signature"></canvas>');


        }
    }

    //#endregion



    ngEnter.$inject = ['$log', '$compile'];
    function ngEnter(
        $log: angular.ILogService,
        $compile: angular.ICompileService): angular.IDirective {
        return <ng.IDirective>{
            link: ($scope: ng.IScope, $element, $attrs: any): void => {
                $element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        $scope.$apply(function () {
                            $scope.$eval($attrs.ngEnter);
                        });

                        event.preventDefault();
                    }
                });
            }
        };

    }
    angular.module('app').directive('ngEnter', ngEnter);


    


}
