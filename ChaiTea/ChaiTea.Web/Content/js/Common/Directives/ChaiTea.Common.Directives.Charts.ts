﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';
    /**
     * @description Line chart.
     */

    //#region Line chart
    sbmHcLineChart.$inject = ['$log', 'sitesettings'];
    function sbmHcLineChart(
        $log: angular.ILogService, sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            scope: {
                id: '@',
                height: '@',
                model: '=ngModel',
                legendClickDisable: '@',
                toolTipFormat: '@'
            },
            template: '<div id="{{ id }}-line-chart" style="width:100%; min-width: 220px; height: {{height}}px; margin: 0 auto"></div>',
            link: link
        };

        function link($scope, element: angular.IRootElementService, attrs: angular.IAttributes, ngModel: angular.INgModelController) {
            $scope.height = ($scope.height ? parseInt($scope.height) : 350);
            $scope.$watch('model',() => {
                onModelChange($scope, ngModel);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            scope.chartData = model.$viewValue;



            if (scope.chart == null) {
                createChart(scope, model.$viewValue);
            } else {
                scope.chart.options = setGlobalAndMobileValues(scope.chart.options, sitesettings);
                
                // Update the series data and categories.
                scope.chart.xAxis[0].update({
                    categories: model.$viewValue.categories
                }, false);

                var newSeries = model.$viewValue.series;
                for (var i = 0; i < newSeries.length; i++) {
                    if (scope.chart.series && scope.chart.series.length)
                        scope.chart.series[i].update(newSeries[i], false);
                }

                // Update the plot lines
                scope.chart.yAxis[0].update({
                    plotLinesAndBands: model.$viewValue.plotLines
                }, false);



                scope.chart.redraw();
            }
        }

        /**
         * @description Generate a chart.
         * @param {obj} scope
         */
        function createChart(scope, data) {
            if (data == null) return;
            var chartData = data;
            var axisStyle = {
                color: '#393f44',
                fontSize: '12px',
                fontWeight: '600'
            };
            var toolTipVal = scope.toolTipFormat ? <HighchartsTooltipOptions>{
                                                        borderRadius: 15,
                                                        style: <HighchartsCSSObject>{
                                                            padding: '15',
                                                            fontWeight: 'bold'
                                                        },
                                                        formatter: function () {
                                                            var s = '<b>' + this.x + '</b>';

                                                            s += '<br/>' + this.series.name + ': ' +
                                                                this.y.toFixed(2) + '%';
                                                            return s;
                                                        }
                                                    }
                                            : <HighchartsTooltipOptions>{
                                                borderRadius: 15,
                                                style: <HighchartsCSSObject>{
                                                    padding: '15',
                                                    fontWeight: 'bold'
                                                }
                                            };
            var options = <HighchartsOptions>{
                chart: {
                    type: 'line',
                    marginTop: 30,
                    height: scope.height,
                    renderTo: scope['id'] + '-line-chart',
                    backgroundColor: chartData.backgroundColor
                },
                colors: [sitesettings.colors.secondaryColors.teal,
                    sitesettings.colors.secondaryColors.blue,
                    sitesettings.colors.secondaryColors.lime,
                    sitesettings.colors.secondaryColors.lavendar,
                    sitesettings.colors.secondaryColors.orange,
                    sitesettings.colors.secondaryColors.blueSteel,
                    sitesettings.colors.secondaryColors.pink,
                    sitesettings.colors.secondaryColors.tealDark,
                    sitesettings.colors.secondaryColors.blueDark,
                    sitesettings.colors.secondaryColors.limeDark,
                    sitesettings.colors.secondaryColors.lavendarDark,
                    sitesettings.colors.secondaryColors.orangeDark,
                    sitesettings.colors.secondaryColors.blueSteelDark,
                    sitesettings.colors.secondaryColors.pinkDark],
                title: {
                    text: '',
                },
                credits: {
                    enabled: false,
                },
                //legend: {
                //    align: 'right',
                //    borderWidth: 0,
                //    verticalAlign: 'top',
                //    floating: true,
                //    x: 0,
                //    y: -10
                //},
                legend: chartData.legend || {},
                tooltip: toolTipVal,
                plotOptions: {
                    series: {
                        marker: {
                            symbol: 'circle',
                            fillColor: '#FFFFFF',
                            lineWidth: 2,
                            radius: 5,
                            lineColor: null
                        },
                        events: {
                            legendItemClick: function (event) {
                                if (scope.legendClickDisable) {
                                    return false;
                                }
                            }
                        }
                    }
                },
                yAxis: {
                    title: '',
                    allowDecimals: false,
                    lineWidth: 0,
                    gridLineDashStyle: 'shortdash',
                    gridLineColor: '#e4e4e4',
                    plotLines: chartData.yAxis ? chartData.yAxis.plotLines : [],
                    min: 0,
                    max: chartData.yAxis ? chartData.yAxis.max : undefined,
                    minRange: chartData.yAxis ? chartData.yAxis.minRange : undefined,
                    labels: chartData.yAxis ? chartData.yAxis.labels ? chartData.yAxis.labels : {
                        style: axisStyle
                    } : axisStyle
                },
                xAxis: {
                    tickColor: '#fff',
                    categories: chartData.categories,
                    plotLines: chartData.xAxis ? chartData.xAxis.plotLines : [],
                    labels: chartData.xAxis ? chartData.xAxis.labels ? chartData.xAxis.labels : {
                        style: axisStyle
                    } : axisStyle
                },
                series: chartData.series
            };
            options = setGlobalAndMobileValues(options, sitesettings);


            scope.chart = new Highcharts.Chart(options);
        }

    }
    angular.module('app').directive('sbmHcLineChart', sbmHcLineChart);
    //#endregion

    /**
     * @description Bar chart.
     */
    //#region Bar chart
    sbmHcBarChart.$inject = ['sitesettings'];
    function sbmHcBarChart(sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            scope: {
                id: '@',
                model: '=ngModel',
                scaleMin: '@',  // to set min scale manually
                scaleMax: '@',  // to set max scale manually
                step: '@'       // to set step manually
            },
            template: '<div id="{{ id }}-bar-chart" style="width:100%; min-width: 250px; margin: 0 auto"></div>',
            link: link
        };

        function link($scope, element, attrs, ngModel) {
            $scope.$watch('model',() => {
                onModelChange($scope, ngModel);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            scope.chartData = model.$viewValue;

            var min = scope.scaleMin || 0,
                max = scope.scaleMax,
                step = parseInt(scope.step) || 1,
                windowWidth = $(window).width();

            if (scope.chart == null) {
                createChart(scope, model.$viewValue);
            }
            else if (!scope.chart.series.length){
                createChart(scope, model.$viewValue);
            }
            else {
                // Update the series data and categories.
                scope.chart.xAxis[0].update({
                    categories: model.$viewValue.xAxis.categories
                }, false);

                var newSeries = model.$viewValue.series;
                if (scope.chart.series.length) {
                    for (var i = 0; i < newSeries.length; i++) {
                        scope.chart.series[i].update(newSeries[i], true);
                    }
                }

                // Adjust steps/tick interval
                //scope.chart.yAxis[0].options.tickInterval = step;
                
                // Adjust the min and max for scale manually.
                //scope.chart.yAxis[0].setExtremes(min, max);

                //Check & Set Legend Values if Mobile/Tablet
                scope.chart.options = setGlobalAndMobileValues(scope.chart.options, sitesettings);



                scope.chartData.height = scope.chartData.height < 225 ? 225 : scope.chartData.height;
                if (scope.chart.chartWidth && scope.chartData.height) {
                    scope.chart.setSize(scope.chart.chartWidth, scope.chartData.height);
                }

                scope.chart.redraw();
            }
        }

        /**
         * @description Generate a chart.
         * @param {obj} scope
         */
        function createChart(scope, data) {
            if (data == null) return;
            var chartData = data;

            var options = <HighchartsOptions>{
                chart: {
                    type: 'bar',
                    renderTo: scope['id'] + '-bar-chart',
                    height: chartData.height,
                    backgroundColor: chartData.backgroundColor
                },
                colors:  [sitesettings.colors.secondaryColors.teal,
                sitesettings.colors.secondaryColors.blue,
                sitesettings.colors.secondaryColors.lime,
                sitesettings.colors.secondaryColors.lavendar,
                sitesettings.colors.secondaryColors.orange,
                sitesettings.colors.secondaryColors.blueSteel,
                sitesettings.colors.secondaryColors.pink,
                sitesettings.colors.secondaryColors.tealDark,
                sitesettings.colors.secondaryColors.blueDark,
                sitesettings.colors.secondaryColors.limeDark,
                sitesettings.colors.secondaryColors.lavendarDark,
                sitesettings.colors.secondaryColors.orangeDark,
                sitesettings.colors.secondaryColors.blueSteelDark,
                sitesettings.colors.secondaryColors.pinkDark],
                title: {
                    text: ''
                },
                credits: {
                    enabled: false,
                },
                xAxis: chartData.xAxis || {},
                yAxis: chartData.yAxis || {},

                legend: chartData.legend || {},
                plotOptions: chartData.plotOptions || {},
                series: chartData.series || [],
                tooltip: chartData.tooltip || {}
            };

            //Check & Set Legend Values if Mobile/Tablet
            options = setGlobalAndMobileValues(options, sitesettings);

            scope.chart = new Highcharts.Chart(options);
        }



    }
    angular.module('app').directive('sbmHcBarChart', sbmHcBarChart);
    //#endregion

    /**
     * @description Pie chart.
     */
    //#region Pie chart
    sbmHcPieChart.$inject = ['$compile', 'sitesettings'];
    function sbmHcPieChart($compile: angular.ICompileService, sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            scope: {
                id: '@',
                model: '=ngModel',
                customlegend: '@?',
                suppliesLegend: '@?',
                suppliesBillableGreenLegend: '@?',
                suppliesLegendProgram: '@?',
                goToDetails: '&',
                messageStatsLegend: '@?',
                legendLeastOpened: '@?',
                legendAllOpenedMessages: '@?',
                billableLegend: '@?',
                height: '@',
                minwidth: '@',
                loadTrend: '&',
                clickLegend: '@?',
                turnoverReasonsLegend: '@?',
                voluntaryLegend: '@?',
                qdAttendanceLegend: '@?',
                qdProfessionalismLegend: '@?',
                pieColors: '@',
                redrawChart: '@',
                toolTipFormat: '=',
                personnelLegend: '@?',
                billablePaidVsUnpaidLegend: '@?',
                drilldownChart: '&'
                
            },
            template: '<div id="{{ id }}-pie-chart" style="width:100%; min-width: {{minwidth}}px; height: {{height}}px; margin: 0 auto"></div>',
            link: link
        };
        
        function link($scope, element, attrs, ngModel) {
            $scope.height = ($scope.height ? parseInt($scope.height) : 350);
            $scope.minwidth = ($scope.minwidth ? $scope.minwidth : 220);
            $scope.$watch('model',() => {
                onModelChange($scope, ngModel);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            scope.chartData = model.$viewValue;
            if (scope.redrawChart) {
                scope.chart = null;
            }
                
            if (scope.chart == null) {
                createChart(scope, model.$viewValue);

            } else {

                // Update the series data.
                var newSeries = model.$viewValue.series;
                for (var i = 0; i < newSeries.length; i++) {
                    scope.chart.series[i].update(newSeries[i], false);
                }
                scope.chart.setSize(scope.chart.chartWidth, scope.chart.chartHeight, scope.doAnimation = true);
                scope.chart.redraw();
            }

            var chartSeries = scope.chart;
            var chartLegendItems: number = chartSeries.series[0].data.length;
            var seriesData: HighchartsDataPoint;
            var strHtml = "";



            if (scope['customlegend']) {
                strHtml = "";
                var top = 5;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemCustomA = '<div style="width:100%;margin-left:25px;top: ' + top + 'px" class="legend-items"><span class="table-legend  legend-slice-color" style="background:' + seriesData.color + '; "></span><span class="legend-txt-custom">' + seriesData.name + '</span><span class="legend-val-y col-md-3 pull-right">$' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ useGrouping: true, minimumFractionDigits: 2 }) + ' </span></div>';
                    var strHtmlItemCustomB = "<div class='row'>" + strHtmlItemCustomA + "</div>";

                    var strHtmlItemCustomC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemCustomC + strHtmlItemCustomB;
                    top = top + 2;
                }
                $('#legendSpotCustom').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['billableLegend']) {
                strHtml = "";
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];
                    
                    var strHtmlItemA = '<div class="billable-legend"  ng-click="goToDetails({programId: \'' + seriesData['programId'] + '\'})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt pointer">' + seriesData.name + '</span><span class="legend-val">$' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ useGrouping: true, minimumFractionDigits: 2 }) + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = angular.element('#billableLegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            else if (scope['suppliesLegend']) {
                strHtml = "";
                var isSubCategory = false;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];
                    
                    var strHtmlItemA = '<div class="supplies-legend" ng-click="goToDetails({param: \'' + seriesData.name + '\',isSubCategory: '+ isSubCategory + '})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(1) + '%</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = angular.element('#supplieslegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            else if (scope['suppliesBillableGreenLegend']) {
                strHtml = "";
                var isSubCategory = true;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];
                    var strHtmlItemA = '<div class="supplies-legend" ng-click="goToDetails({param: \'' + seriesData.name + '\',isSubCategory: \'' + isSubCategory + '\'})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(1) + '%</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = $('#suppliesBillableGreenlegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            else if (scope['suppliesLegendProgram']) {
                strHtml = "";
              
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemA = '<div class="supplies-legend"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt-program">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(1) + '%</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#supplieslegendProgramSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['messageStatsLegend']) {
                strHtml = "";
                var top = 5;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemCustomA = '<div class="legend-items"><span class="table-legend  legend-slice" style="background:' + seriesData.color + '; "></span><span class="legend-txt-custom">' + seriesData.name + '</span><span class="legend-val-y col-md-3 pull-right">' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 }) + ' </span></div>';
                    var strHtmlItemCustomB = "<div class='row'>" + strHtmlItemCustomA + "</div>";

                    var strHtmlItemCustomC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemCustomC + strHtmlItemCustomB;
                    top = top + 2;
                }
                $('#legendSpotCustom').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['legendLeastOpened']) {
                strHtml = "";
                var top = 5;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemCustomA = '<div class="legend-items"><span class="table-legend  legend-slice" style="background:' + seriesData.color + '; "></span><span class="legend-txt-custom">' + seriesData.name + '</span><span class="legend-val-y col-md-3 pull-right">' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 }) + ' </span></div>';
                    var strHtmlItemCustomB = "<div class='row'>" + strHtmlItemCustomA + "</div>";

                    var strHtmlItemCustomC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemCustomC + strHtmlItemCustomB;
                    top = top + 2;
                }
                $('#legendSpotCustomLeastOpened').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['legendAllOpenedMessages']) {
                strHtml = "";
                var top = 5;
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemCustomA = '<div class="legend-items"><span class="table-legend  legend-slice" style="background:' + seriesData.color + '; "></span><span class="legend-txt-custom">' + seriesData.name + '</span><span class="legend-val-y col-md-3 pull-right">' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ minimumIntegerDigits: 2 }) + ' </span></div>';
                    var strHtmlItemCustomB = "<div class='row'>" + strHtmlItemCustomA + "</div>";

                    var strHtmlItemCustomC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemCustomC + strHtmlItemCustomB;
                    top = top + 2;
                }
                $('#legendCustomAllOpenedMessages').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['clickLegend']) {
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];
                    var strHtmlItemA = '<div class="type-chart-legend" ng-click="loadTrend({id: ' + seriesData.id + ',color: \'' + seriesData.color
                        + '\',name:\'' + seriesData.name + '\'})"><i class="fa fa-circle legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">'
                        + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(1) + '%</span><div class="clearfix"></div></div>';

                    var strHtmlItemB = "<div class='col-xs-12 col-md-4 col-lg-4'>" + strHtmlItemA + "</div>";
                   

                    var strHtmlItemC = ((i - 1) % 3 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = angular.element('#legendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            else if (scope['turnoverReasonsLegend']) {
                strHtml = "";

                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemA = '<div class=""><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt-name">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(2) + '%</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#turnoverReasonslegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['voluntaryLegend']) {
                strHtml = "";

                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                
                    seriesData = chartSeries.series[0].data[j];
                    if (seriesData.name != 'VOLUNTARY' && seriesData.name != 'INVOLUNTARY') {
                        var strHtmlItemA = '<i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt-name">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(2) + '%</span>';
                        var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                        var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                        strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                    }
                }
                $('#voluntarylegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['qdAttendanceLegend'] || scope['qdProfessionalismLegend']) {
                strHtml = "";

                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {

                    seriesData = chartSeries.series[0].data[j];
                    var strHtmlItemA = '<i class="fa fa-square padding-5" style="color:' + seriesData.color + ';"></i><span class="legend-txt-name">' + seriesData.name + '</span>';
                    var strHtmlItemB = '<span class="padding-horizontal-5">' + strHtmlItemA + '</span>';

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row text-center\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                if (scope['qdAttendanceLegend']) {
                    $('#qd-att-legendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                } else {
                    $('#qd-Prof-legendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                }
            }
            else if (scope['personnelLegend']) {
                strHtml = "";

                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemA = '<div class=""><div class="legend-icon"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i></div><div class="legend-txt-name">' + seriesData.name + '</div></div>';
                    var strHtmlItemB = "<div class='col-xs-6 pull-left padding-right-0'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row padding-left-5\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#personnelLegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            else if (scope['billablePaidVsUnpaidLegend']) {
                strHtml = "";
                for (var i = 1, j = chartLegendItems - 1; j >= 0; i++ , j--) {
                    seriesData = chartSeries.series[0].data[j];

                    var strHtmlItemA = '<div class="billable-paidVsUnpaid-legend"  ng-click="drilldownChart({type: \'' + seriesData.name + '\'})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt pointer">' + seriesData.name + '</span><span class="legend-val">$' + seriesData['y'].toLocaleString('en', <Intl.NumberFormatOptions>{ useGrouping: true, minimumFractionDigits: 2 }) + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = angular.element('#billablePaidVsUnpaidLegendspot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            else {
                for (var i = 1, j = 0; j < chartLegendItems; i++ , j++) {
                    seriesData = chartSeries.series[0].data[j];
                    var strHtmlItemA = '<div style="width:100%;font-size:12px;padding:2px 0;"><i class="fa fa-circle legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val">' + seriesData['percentage'].toFixed(1) + '%</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-4 col-lg-4'>" + strHtmlItemA + "</div>";
                    
                    var strHtmlItemC = ((i - 1) % 3 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#legendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            

        }

        /**
         * @description Generate a chart.
         * @param {obj} scope
         */
        function createChart(scope, data) {
            if (data == null) return;
            var chartData = data;
            var colors = scope.pieColors ? scope.pieColors.split(",") : null;
            var options = <HighchartsOptions>{
                colors: scope.pieColors ? colors : [sitesettings.colors.secondaryColors.teal,
                    sitesettings.colors.secondaryColors.blue,
                    sitesettings.colors.secondaryColors.lime,
                    sitesettings.colors.secondaryColors.lavendar,
                    sitesettings.colors.secondaryColors.orange,
                    sitesettings.colors.secondaryColors.blueSteel,
                    sitesettings.colors.secondaryColors.pink,
                    sitesettings.colors.secondaryColors.tealDark,
                    sitesettings.colors.secondaryColors.blueDark,
                    sitesettings.colors.secondaryColors.limeDark,
                    sitesettings.colors.secondaryColors.lavendarDark,
                    sitesettings.colors.secondaryColors.orangeDark,
                    sitesettings.colors.secondaryColors.blueSteelDark,
                    sitesettings.colors.secondaryColors.pinkDark],
                chart: {
                    renderTo: scope['id'] + '-pie-chart',
                    height: chartData.chart ? chartData.chart.height ? chartData.chart.height : 400 : 400,
                    plotBackGroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor: chartData.backgroundColor,
                },
                title: {
                    text: '',
                },
                credits: {
                    enabled: false,
                },
                legend: {
                    enabled: false,
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'top',
                    //floating: true,
                    width: 550,
                    borderWidth: 0,
                    useHTML: true,
                    itemMarginBottom: 5,
                    borderRadius: 8,
                    //itemHeight: 15,
                    //adjustChartSize: true,
                    labelFormatter: function () {
                        var total = 0, count = '', percentage;

                        $.each(this.series.data, function () {
                            total += this.y;
                        });

                        percentage = ((this.y / total) * 100).toFixed(1);
                        count += this.y;

                        var nLen = this.name.length;
                        var cLen = count.length;
                        var pLen = percentage.length;


                        var offset = nLen + pLen + cLen;
                        var dotcount = 40 - offset;

                        for (var dotstring = '', i = 0; i < dotcount; i++) {
                            dotstring += '.';
                        }

                        //if (enableLegend)
                        //    return '<div style="width:220px"><span style="float:left;font-size:14px">' + this.name + '</span><span style="float:right;display:block;width:40px;text-align:right;font-weight:normal;font-size:14px">' + percentage + '%</span><span style="float:right;">' + count + '</span></div>';
                        //else
                        return '<div style="width:220px"><span style="float:left;">' + this.name + '</span><span style="float:right;display:block;width:40px;text-align:right;font-weight:normal;">' + percentage + '%</span><span style="float:right;">' + count + '</span></div>';
                    },
                    navigation: {
                        activeColor: '#3E576F',
                        animation: true,
                        arrowSize: 12,
                        inactiveColor: '#CCC',
                        style: {
                            fontWeight: 'bold',
                            color: '#333',
                            fontSize: '12px'
                        }
                    }
                },
                tooltip: {
                    pointFormat: scope["toolTipFormat"] ? '{series.name}: <b>{point.y}</b>' : '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true,
                        borderWidth: 0,
                        slicedOffset: 0
                    }
                },
                series: chartData.series,
                showInLegend: true,
                yAxis: {
                    allowDecimals: false,
                    min: 0
                }
            };
            
            scope.chart = new Highcharts.Chart(options);
        }

    }
    angular.module('app').directive('sbmHcPieChart', sbmHcPieChart);    
    //#endregion

    /**
     * @description Circle chart.
     */
    //#region Circle chart
    sbmCircleChart.$inject = ['$log'];
    function sbmCircleChart(
        $log: angular.ILogService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                ngModel: '=',
                failColor: '=',
                goal: '@',
                subText: '@',
                valueFormatter: '&' // formatter definition fn
            },
            template: '<div class="circle-chart"><strong></strong><p>{{ subText }}</p></div>',
            link: link
        };

        function link($scope: angular.IScope, element: angular.IRootElementService, attrs: angular.IAttributes, ngModel: angular.INgModelController) {

            /**
             * @description Callback when model changes.
             */
            var onModelChange = (scope) => {
                var val = ngModel.$viewValue,
                    color = attrs['color'],
                    failColor = $scope['failColor'],
                    size = attrs['size'],
                    operator = attrs['operator'] || '<',
                    goal = $scope['goal'],
                    emptyFill = 'rgba(0, 0, 0, .1)';

                var outOf = attrs['outof'],
                    circleChartVal = (val / outOf) || 0;

                element['circleProgress']({
                    value: circleChartVal,
                    size: size,
                    thickness: 15,
                    lineCap: 'sqaure',
                    animation: 1200,
                    animationStartValue: 0,
                    startAngle: -1.5,
                    emptyFill: emptyFill,
                    fill: {
                        color: {
                            '>': () => ((val >= goal) ? color : failColor),
                            '<': () => ((val < goal) ? failColor : color)
                        }[operator]()
                    }
                }).on('circle-animation-progress',(event, progress, stepValue) => {
                    //var val = stepValue.toString().substr(1) * outOf || 0;
                    var val = stepValue * outOf || 0;
                    var formatterFunc = scope.valueFormatter();

                    if (angular.isFunction(formatterFunc)) {
                        val = formatterFunc(val);
                    }

                    $(element).find('strong').text(val);
                });
            }

            // Redraw when any of the property changes
            $scope.$watchGroup(['ngModel', 'goal'],() => {
                onModelChange($scope);
            });
        }
    }
    angular.module('app').directive('sbmCircleChart', sbmCircleChart);
    //#endregion

    /**
     * @description Column chart.
     */
    //#region Column chart
    sbmHcColumnChart.$inject = ['sitesettings', '$compile'];
    function sbmHcColumnChart(sitesettings: ISiteSettings, $compile: angular.ICompileService): angular.IDirective {
        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            scope: {
                id: '@',
                model: '=ngModel',
                redrawChart: '@',
                height: '@',
                minwidth: '@',
                scaleMin: '@',  // to set min scale manually
                scaleMax: '@',  // to set max scale manually
                step: '@',       // to set step manually
                suppliesMonthlyLegend: '@?',
                suppliesMonthlyProgramLegend: '@?',
                messageChartsTotalRecipientsLegend: '@?',
                goToDetails: '&',
                showByProgram: '&',
                billingMonthlyLegend:'=?',
                billingMonthlyByProgramLegend: '=?',
                billablePaidOrUnpaidLegend: '@?',
                skipColors:'@?',
                columnColors: '@',
                messagesSent: '@?'
            },
            template: '<div id="{{ id }}-column-chart" style="width:100%; min-width: {{minwidth}}px; margin: 0 auto; height: {{height}}px;"></div>',
            link: link
        };

        function link($scope, element, attrs, ngModel) {
            $scope.height = ($scope.height ? parseInt($scope.height) : 350);
            $scope.minwidth = ($scope.minwidth ? $scope.minwidth : 250);
            $scope.$watchGroup(['model', 'scaleMax', 'step'],() => {
                onModelChange($scope, ngModel);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope, model) {
            scope.chartData = model.$viewValue;
            var min = scope.scaleMin || 0,
                max = scope.scaleMax,
                step = parseInt(scope.step) || 1;


            if (scope.redrawChart) {
                scope.chart = null;
            }
            if (scope.chart == null) {          
                createChart(scope, model.$viewValue);
            } else {
                var newSeries = model.$viewValue.series;
                if (newSeries.length != scope.chart.series.length) {
                    createChart(scope, model.$viewValue);
                }
                else if (scope.id == "SuppliesMonthlyProductTrend") {
                    createChart(scope, model.$viewValue);
                }
                else if (scope.id == "MessageStatsSentMessages") {
                    createChart(scope, model.$viewValue);
                }
                else if (scope.id == "MonthlyTrends") {
                    createChart(scope, model.$viewValue);
                }
                else {
                    // Update the series data and categories.
                    scope.chart.xAxis[0].update({
                        categories: model.$viewValue.xAxis.categories
                    }, false);

                    var newSeries = model.$viewValue.series;
                    for (var i = 0; i < newSeries.length; i++) {
                        scope.chart.series[i].update(newSeries[i], true);
                    }

                    // Adjust steps/tick interval
                    scope.chart.yAxis[0].options.tickInterval = step;
                
                    // Adjust the min and max for scale manually.
                    scope.chart.yAxis[0].setExtremes(min, max);

                    scope.chartData.height = scope.chartData.height < 225 ? 225 : scope.chartData.height;
                    //scope.chart.setSize(scope.chart.chartWidth, scope.chartData.height);

                    scope.chart.options = setGlobalAndMobileValues(scope.chart.options, sitesettings);

                    scope.chart.redraw();
                }
            }
            var chartSeries = scope.chart;
            var chartLegendItems: number = chartSeries.series.length;
            var seriesData;
            var strHtml = "";

            if (scope['suppliesMonthlyLegend']) {
                strHtml = "";
                var isSubCategory = false;
                for (var i = chartLegendItems-1; i >= 0; i--) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="supplies-legend" ng-click="goToDetails({param: \'' + seriesData.name + '\',isSubCategory: ' + isSubCategory + '})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val supplies-monthly-legend-value-align">$' + seriesData.options['amount'].toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = $('#suppliesMonthlylegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            if (scope['suppliesMonthlyProgramLegend']) {
                strHtml = "";
               
                for (var i = chartLegendItems - 1; i >= 0; i--) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="supplies-program-legend"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val supplies-monthly-legend-value-align">$' + seriesData.options['amount'].toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#suppliesMonthlyProgramlegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }
            if (scope['messageChartsTotalRecipientsLegend']) {
                strHtml = "";
                
                for (var i = 0; i < chartSeries.series[0].data.length; i++) {
                    seriesData = chartSeries.series[0].data[i];
                    var strHtmlItemA = '<div class="legend-items"><i class="fa fa-square legend-slice-color" style="color:' + chartSeries.series[1].data[i].color + ';"></i><span class="legend-txt pull-left">' + seriesData.category + '</span><span class="legend-val-y pull-right">' + (seriesData.total - seriesData.y) + '/' + seriesData.total + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12 legend-padding'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                $('#messageChartsTotalRecipientsLegendSpot').html('<div class=\'row\'>' + strHtml + '</div>');
            }

            if (scope['messagesSent']) {
                strHtml = "";
                for (var i = chartLegendItems - 1; i >= 0; i--) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="legend-items"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt pull-left">' + seriesData.name + '</span><span class="legend-val-y pull-right">' + seriesData.options['amount'] + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    var strHtmlItemC = ((i - 1) % 4 == 0) ? '</div><div class=\'row\'>' : '';

                    strHtml = strHtml + strHtmlItemC + strHtmlItemB;
                }
                var el = $('#legendCustomSentMessages').html('<div class=\'row\'>' + strHtml + '</div>');
                $compile(el)(scope);
            }
            if (scope['billingMonthlyLegend']) {
                strHtml = "";
                for (var i = 0; i < chartLegendItems; i++) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="billing-monthly-trend-legend" ng-click="showByProgram({categoryType: \'' + seriesData.name + '\'})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    strHtml = strHtml + strHtmlItemB;
                }
                var el = $('#billingMonthlyLegendSpot').html(strHtml);
                $compile(el)(scope);
        }
            if (scope['billingMonthlyByProgramLegend']) {
                strHtml = "";
                var isSubCategory = false;
                for (var i = chartLegendItems - 1; i >= 0; i--) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="supplies-legend" ng-click="goToDetails({programId: \'' + seriesData.options['id'] + '\'})"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</div>';
                    var strHtmlItemB = "<div class='col-xs-6 col-md-6 col-lg-6'>" + strHtmlItemA + "</div>";

                    strHtml = strHtml + strHtmlItemB;
                }
                var el = $('#billingMonthlyByProgramLegendSpot').html(strHtml);
                $compile(el)(scope);
            }
            if (scope['billablePaidOrUnpaidLegend']) {
                strHtml = "";
                for (var i = chartLegendItems - 1; i >= 0; i--) {
                    seriesData = chartSeries.series[i];
                    var strHtmlItemA = '<div class="billing-monthly-trend-legend" ng-click="goToDetails()"><i class="fa fa-square legend-slice-color" style="color:' + seriesData.color + ';"></i><span class="legend-txt">' + seriesData.name + '</span><span class="legend-val supplies-monthly-legend-value-align">$' + seriesData.options['amount'].toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</span></div>';
                    var strHtmlItemB = "<div class='col-xs-12 col-md-12 col-lg-12'>" + strHtmlItemA + "</div>";

                    strHtml = strHtml + strHtmlItemB;
                }
                var el = $('#billingPaidOrUnpaidLegendSpot').html(strHtml);
                $compile(el)(scope);
            }
        }

        /**
         * @description Generate a chart.
         * @param {obj} scope
         */
        function createChart(scope, data) {
            if (data == null) return;
            var chartData = data;
            var plotOptions = {
                column: {
                    borderWidth: 0
                }
            }
            chartData.plotOptions ? (chartData.plotOptions.column ? chartData.plotOptions.column.borderWidth = 0 : chartData.plotOptions.column = plotOptions.column) : chartData.plotOptions = plotOptions;
            var columnColors = scope.columnColors ? scope.columnColors.split(",") : [];
            var colors = _.union(columnColors, [sitesettings.colors.secondaryColors.teal,
                    sitesettings.colors.secondaryColors.blue,
                    sitesettings.colors.secondaryColors.lime,
                    sitesettings.colors.secondaryColors.lavendar,
                    sitesettings.colors.secondaryColors.orange,
                    sitesettings.colors.secondaryColors.blueSteel,
                    sitesettings.colors.secondaryColors.pink,
                    sitesettings.colors.secondaryColors.tealDark,
                    sitesettings.colors.secondaryColors.blueDark,
                    sitesettings.colors.secondaryColors.limeDark,
                    sitesettings.colors.secondaryColors.lavendarDark,
                    sitesettings.colors.secondaryColors.orangeDark,
                    sitesettings.colors.secondaryColors.blueSteelDark,
                    sitesettings.colors.secondaryColors.pinkDark]);
            if (!scope.columnColors && scope.skipColors) {
                colors = colors.slice(scope.skipColors, colors.length + 1)
            }
            var options = <HighchartsOptions>{
                chart: {
                    type: 'column',
                    marginTop: 40,
                    height: scope.height,
                    renderTo: scope['id'] + '-column-chart'
                },
                colors: colors,
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                legend: chartData.legend,
                tooltip: chartData.tooltip || {},
                plotOptions: chartData.plotOptions || {},
                yAxis: chartData.yAxis || {
                    labels: {
                        style: {
                            fontWeight: '600'
                        }
                    }
                },
                xAxis: chartData.xAxis || {},
                series: chartData.series || []
            };

            options = setGlobalAndMobileValues(options, sitesettings);

            scope.chart = new Highcharts.Chart(options);
        }



    }
    angular.module('app').directive('sbmHcColumnChart', sbmHcColumnChart);
    
    //#endregion

    /**
    * @description Area Spline chart.
    */
    //#region Spline chart
    sbmHcAreaSplineChart.$inject = ['sitesettings'];
    function sbmHcAreaSplineChart(sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            require: 'ngModel',
            restrict: 'E',
            scope: {
                id: '@',
                model: '=ngModel'
            },
            template: '<div id="{{ id }}-areaspline-chart" style="width:100%; min-width: 250px; margin: 0 auto; height: 300px;"></div>',
            link: link
        };

        function link($scope: any, element: any, attrs: any, ngModel: any) {
            $scope.$watch('model',() => {
                onModelChange($scope, ngModel);
            });
        }

        /**
         * @description Gets invoked when the model within a context changes.
         * @param {obj} scope - The current scope.
         * @param {obj} model - The ngModel value set for the directive.
         */
        function onModelChange(scope: any, model: any) {
            scope.chartData = model.$viewValue;

            if (scope.chart == null) {
                createChart(scope, model.$viewValue);
            } else {
                var newSeries = model.$viewValue.series[0].data;
                if (newSeries.length != scope.chart.series.length) {
                    createChart(scope, model.$viewValue);
                }
                else {
                    // Update the series data and categories.
                    scope.chart.xAxis[0].update({
                        categories: model.$viewValue.categories
                    }, false);


                    for (var i = 0; i < newSeries.length; i++) {
                        scope.chart.series[0].data[i].update(newSeries[i], true);
                    }

                    scope.chartData.height = scope.chartData.height < 225 ? 225 : scope.chartData.height;

                    //scope.chart.setSize(scope.chart.chartWidth, scope.chartData.height);

                    scope.chart.redraw();
                }
            }
        }

        /**
         * @description Generate a chart.
         * @param {obj} scope
         */
        function createChart(scope, data) {
            if (data == null) return;
            var chartData = data;
            var options = <HighchartsOptions>{
                chart: {
                    height: chartData.height,
                    type: 'areaspline',
                    marginTop: 40,
                    renderTo: scope['id'] + '-areaspline-chart'
                },
                colors: [sitesettings.colors.secondaryColors.teal,
                    sitesettings.colors.secondaryColors.blue,
                    sitesettings.colors.secondaryColors.lime,
                    sitesettings.colors.secondaryColors.lavendar,
                    sitesettings.colors.secondaryColors.orange,
                    sitesettings.colors.secondaryColors.blueSteel,
                    sitesettings.colors.secondaryColors.pink,
                    sitesettings.colors.secondaryColors.tealDark,
                    sitesettings.colors.secondaryColors.blueDark,
                    sitesettings.colors.secondaryColors.limeDark,
                    sitesettings.colors.secondaryColors.lavendarDark,
                    sitesettings.colors.secondaryColors.orangeDark,
                    sitesettings.colors.secondaryColors.blueSteelDark,
                    sitesettings.colors.secondaryColors.pinkDark],                                                                                                                                                                                                                                                        
                title: chartData.title || {},
                credits: {
                    enabled: false,
                },
                xAxis: chartData.xAxis || {},
                yAxis: chartData.yAxis || {},

                legend: {
                    enabled: chartData.isLegendEnabled,
                    reversed: true
                },
                plotOptions: chartData.plotOptions || {},
                series: chartData.series || {},
                tooltip: chartData.tooltip || {}
            };

            scope.chart = new Highcharts.Chart(options);
        }

    }
    angular.module('app').directive('sbmHcAreaSplineChart', sbmHcAreaSplineChart);
    //#endregion


    /**
     * @description Utility funcitons
     */
    //#region Utility functions
    function setGlobalAndMobileValues(options: HighchartsOptions, siteSettings: ISiteSettings) {
        
        //Disable/Hide Legends if only one series. DW
        if (options.series && options.series.length <= 1) {
            if (!options.legend) options.legend = { enabled: false };
            if (options.legend.enabled != true)
                options.legend.enabled = false;
        } else { //else set mobile values for Legend
            if (siteSettings.windowSizes.isTiny) {
                if (siteSettings.windowSizes.isExtraTiny) {
                    options.legend.useHTML = false;
                    options.legend.layout = "horizontal";
                }
                options.legend.align = 'center';
                options.legend.verticalAlign = "top";
            }
        }

        /*
         *  BAR CHART OVERRIDES
         */
        if (options.chart.type === 'bar') {
            //detect if bar; add if not
            if (!options.plotOptions.bar) {
                options.plotOptions.bar = { pointWidth: null, groupPadding: null }
            }
            //detect if pointWidth; add if not
            options.plotOptions.bar.pointWidth = options.plotOptions.bar.pointWidth || siteSettings.chartSettings.barWidth || 35;
            
            //detect if groupPadding; add if not
            options.plotOptions.bar.groupPadding = options.plotOptions.bar.groupPadding || siteSettings.chartSettings.groupPadding || .2;

            if ((options.xAxis.categories && options.xAxis.categories.length > 1) && //fixes charts with single categories from being cut off
                (options.chart.height && siteSettings.windowSizes.isTiny)
                ) {

                if (options.xAxis.categories.length > 1)
                    options.chart.height = options.chart.height / 1.1;
                else
                    options.chart.height = options.chart.height / 1.5;
            }

            //Fix for charts that render too short
            if (options.chart.height <= 100)
                options.chart.height = options.chart.height + 200;

        }



        options.chart.marginTop = 50;

        return options;
    }
    //#endregion
}