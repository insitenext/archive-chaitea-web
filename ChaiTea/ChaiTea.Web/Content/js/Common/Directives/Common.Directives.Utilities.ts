﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    fixToTop.$inject = ['$window'];

    function fixToTop($window: angular.IWindowService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            link: function link($scope, element, attrs) {
                var window = angular.element($window);
                var topOffset = element.offset().top;

                window.on('scroll', function (e) {
                    element[(window.scrollTop() >= topOffset) ? 'addClass' : 'removeClass']('fix-to-top');
                });

            }
        };
    }
    angular.module('app').directive('fixToTop', fixToTop);

    fixToBottom.$inject = ['$window'];

    function fixToBottom($window: angular.IWindowService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            link: function link($scope, element, attrs) {
                var window = angular.element($window);
                var topOffset = element.offset().top;

                window.on('scroll', function (e) {
                    element[(window.scrollTop() >= topOffset) ? 'addClass' : 'removeClass']('fix-to-bottom');
                });

            }
        };
    }
    angular.module('app').directive('fixToBottom', fixToBottom);

    compile.$inject = ['$compile'];
    function compile($compile: angular.ICompileService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            replace: true,
            link: function link(scope, element, attrs) {

                scope.$watch(attrs['compile'], (html: any) => {

                    element.html(html);
                    $compile(element.contents())(scope);
                });

            }
        };
    }
    angular.module('app').directive('compile', compile);

    /**
     * @deascription Equivalent to adding $event.stopPropagation() to ng-click
     */
    function stopEvent() {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('click', function (e) {
                    e.stopPropagation();
                });
            }
        };
    }
    angular.module('app').directive('stopEvent', stopEvent);

    function touchStartEvent() {
        return {
            restrict: 'A',
            link :function (scope, element, attr) {
                element.bind('touchstart', (e) => {
                    var method = element.attr('touch-start');
                    scope.$apply(method);
                });
            }
        };
    }
    angular.module('app').directive('touchStart', touchStartEvent);

    /*
     * @description Directive to have a "fallback" image if err-src image url fails
    */
    var errSrc = function() {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    }

    angular.module('app').directive('errSrc', errSrc);

    /*
    * @description Directive to validate minimum password complexity requirements
    */
    function complexPassword() {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function (password) {
                    var hasUpperCase = /[A-Z]/.test(password);
                    var hasLowerCase = /[a-z]/.test(password);
                    var hasNumbers = /\d/.test(password);
                    var hasNonalphas = /\W/.test(password);

                    if ((password.length >= 6) && hasUpperCase && hasLowerCase && hasNumbers && hasNonalphas) {
                        ctrl.$setValidity('complexity', true);
                        return password;
                    }
                    else {
                        ctrl.$setValidity('complexity', false);
                        return undefined;
                    }
                });
            }
        }
    }
    angular.module('app').directive('complexPassword', complexPassword);
}