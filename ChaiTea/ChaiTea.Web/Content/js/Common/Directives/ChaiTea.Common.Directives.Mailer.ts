﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    import commonSvc = ChaiTea.Common.Services;
    /**
     * @description Directive to show a mail modal based on type.
     */
    mailer.$inject = ['$log', 'chaitea.common.services.mailsvc'];
    function mailer(
        $log: angular.ILogService,
        mailSvc: commonSvc.IMailSvc): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            scope: {
                type: '@mailer',
                message: '@message',
                alertType: '@',
                params: '=params'
            },
            link: link
        };

        function link($scope, element, attrs) {
            element.bind('click', function (event) {
                event.preventDefault();

                mailSvc.openForm($scope.type, $scope.params);
            });
        }
    }
    angular.module('app').directive('mailer', mailer);
} 