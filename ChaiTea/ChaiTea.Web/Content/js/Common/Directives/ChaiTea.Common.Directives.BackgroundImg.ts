﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Common.Directives {
    'use strict';

    import commonSvc = ChaiTea.Common.Services;
    /**
     * @description Directive to show background image.
     */
    backgroundImg.$inject = ['$log'];
    function backgroundImg(
        $log: angular.ILogService): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'A',
            scope: {
            },
            link: link
        };

        function link($scope, element, attrs) {
            attrs.$observe('backgroundImg', function (value) {
                element.css({
                    'background-image': 'url(' + value + ')',
                    'background-size': attrs.type
                });
            });
        }
    }
    angular.module('app').directive('backgroundImg', backgroundImg);
} 