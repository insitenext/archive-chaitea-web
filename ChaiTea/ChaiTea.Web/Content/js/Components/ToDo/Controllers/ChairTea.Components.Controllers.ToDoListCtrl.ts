﻿/// <reference path="../../../_libs.ts" />
 
module ChaiTea.Components.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;
    import commonInterfaces = ChaiTea.Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;

    interface IToDoListItem {
        TodoItemId: number;
        OrgUserId: number;
        Comment: string;
        DueDate: string;
        IsActive: boolean;
        FloorId: number;
    }

    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }

    class ToDoListCtrl {
        siteSettings: ISiteSettings;
        clientId: number = 0;
        siteId: number = 0;
        programId: number = 0;
        modalInstance;
        userId: number;
        minDate: Date;

        todo = <IToDoListItem>{};
        datepickers = {
            createDate: false
        };
        buildingId: number;
        options = {
            buildings: [],
            floors: [],
            employees: []
        };
        basePath: string;
        isDueInTwentyFourHours: boolean = false;
        dueDate: Date;
        static $inject = [
            '$scope',
            '$uibModalInstance',
            'sitesettings',
            '$log',
            '$filter',
            '$uibModal',
            '$http',
            'chaitea.common.services.lookuplistsvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.localdatastoresvc',
            'userContext',
            'blockUI',
            'userInfo',
            Common.Services.DateSvc.id
            ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private $filter: any,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $http: angular.IHttpService,
            private commonLookupListSvc: commonSvc.ILookupListSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc,
            userContext: IUserContext,
            private blockUI,
            userInfo: IUserInfo,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {

            this.basePath = sitesettings.basePath;
            this.siteSettings = sitesettings;
            this.clientId = userContext.Client.ID;
            this.siteId = userContext.Site.ID;
            this.programId = userContext.Program.ID;
            this.userId = userInfo.userId;

            this.modalInstance = this.$uibModal;
            this.getEmployeeList();
            this.getBuildings();
            this.minDate = new Date();
        }

        /**
         * @description Dismiss the modal instance.
         */
        cancel = (): void=> {
            this.$uibModalInstance.dismiss('cancel');
        };

        /**
        * @description Handle errors.
        */
        onFailure = (response: any): void => {
            this.$log.error(response);
            if (this.blockUI) this.blockUI.stop();
        }

        /**
         * @description Success callback.
         */
        onSuccess = (response: any): void=> {
            this.$uibModalInstance.close(<IToDoListItem>this.todo);

            mixpanel.track("Created Todo", {TodoItemId: this.todo.TodoItemId});

            bootbox.alert('To-Do Item was saved successfully!', this.redirect);
        }

        /** @description Redirect to Entry View page. */
        redirect = (): void=> {
            window.location.replace(this.basePath + 'Quality/ToDo/Details' );
        }

        /**
         * @description Create a new profile and pass variable to parent controller.
         */
        saveTodo = (): void => {
            this.todo.Comment = this.$filter('htmlToPlaintext')(this.todo.Comment);
            if (!this.isDueInTwentyFourHours) {
                this.dueDate = moment(this.dueDate).endOf('day').toDate();
            }
            this.todo.DueDate = this.dateSvc.setFilterDatesToSitesDatetimeOffset(this.dateFormatterSvc.formatDateFull(this.dueDate));
            this.apiSvc.save(this.todo, serviceInterfaces.ApiEndpoints[serviceInterfaces.ApiEndpoints.ToDoItem]).then(this.onSuccess, this.onFailure);
        }

        setDueDate = () => {
            this.isDueInTwentyFourHours = !this.isDueInTwentyFourHours;
            this.dueDate = this.isDueInTwentyFourHours?  moment().add(1, 'd').toDate() : null;
        }
        /**
         * @description Flag a todo item for delete.
         */
        deleteTodo = (): void => {
            this.todo.IsActive = false;
            this.$uibModalInstance.close(<IToDoListItem> this.todo);
        }

        /** 
         * @description Handle opening datepicker. 
         * @param {evt} $event
         * @param {string} which - The name of the datepicker.
         */
        openDatePicker = ($event, which): void => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;
        }

        getEmployeeList = (): void  => {
            this.apiSvc.getLookupList('LookupList/Employees', true).then((result) => {
                this.options.employees = _.sortBy(result.Options, (o: any) => { return o.Value; });
                var empId = this.userId;
                this.options.employees = _.remove(this.options.employees, function (employee) {
                    return employee.Key != empId;
                });
            });
        }

        getBuildings = (): angular.IPromise<any> => {
            return this.apiSvc.getLookupList('LookupList/Buildings', true).then((res) => {
                this.options.buildings = res.Options;
            });
        }
    
        onBuildingChange = (buildingId: number): void => {
            if (buildingId > 0) {

                var param = <IODataPagingParamModel>{
                    $orderby: 'Name asc',
                    $filter: 'ParentId eq ' + buildingId
                };
                this.apiSvc.getByOdata(param, 'Floor').then((res) => {
                    var floors = _.map(res, (floor) => {
                        return { Key: floor['Id'], Value: floor['Name'] }
                    });
                    this.options.floors = floors;
                    if (this.blockUI) this.blockUI.stop();
                });

            } else {
                this.options.floors = [];
            }
        }
    }
    angular.module('app').controller('Components.ToDoListCtrl', ToDoListCtrl);
}
