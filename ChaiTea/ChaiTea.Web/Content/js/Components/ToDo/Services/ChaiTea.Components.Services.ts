﻿/// <reference path="../../../_libs.ts" />

/**
 * @description Send email service and mail delivery factory.
 */
module ChaiTea.Components.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IToDoListSvc {
        openForm(resolveObj?: Object);
    }

    class ToDoListSvc implements angular.IServiceProvider {
        modalInstance;
        basePath: string;
        tag: string = 'sbminsite';
        siteId: number;
        static $inject = ['$q', '$http', '$uibModal', 'sitesettings', 'userContext', '$timeout'];
        constructor(
            private $q: angular.IQService,
            private $http: angular.IHttpService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private $timeout: angular.ITimeoutService) {

            this.basePath = sitesettings.basePath;
            this.modalInstance = this.$uibModal; 
            this.siteId = userContext.Site.ID;           
        }

        public $get(): IToDoListSvc {
            return {
                
                openForm: (resolveObj?) => { return this.openForm(resolveObj); }
            }
        }


        /**
         * @description Instantiate a modal instance and provide an interface to send an email based on a mail type.
         * @param {string} mailType - pre-defined mail types
         * @param {object} resolveObj - any object dependency to be resolved
         */
        openForm = (resolveObj?: Object) => {
            if (this.siteId == 0) {
                this.$timeout(() => {
                    angular.element("#client-context").trigger("click");
                }, 0);
                return false;
            }
            else {
                this.modalInstance = this.$uibModal.open({
                    templateUrl: this.basePath + 'Content/js/Components/ToDo/Templates/toDoList.html',
                    controller: 'Components.ToDoListCtrl as vm',
                    size: 'md',
                    resolve: {

                    }
                });

                return this.modalInstance.result;
            }
        }
    }

    angular.module('app').service('chaitea.components.services.todolistsvc', ToDoListSvc);
}
