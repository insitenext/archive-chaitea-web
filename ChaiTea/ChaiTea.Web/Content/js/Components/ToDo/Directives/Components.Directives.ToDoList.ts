﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {

    'use strict';

    toDoList.$inject = ['$log',
        'sitesettings',
        'chaitea.common.services.alertsvc',
        '$translate',
        'chaitea.components.services.todolistsvc'];
    function toDoList(
        $log: angular.ILogService,
        sitesettings: ISiteSettings,
        alertSvc: ChaiTea.Common.Services.IAlertSvc,
        $translate: angular.translate.ITranslateService,
        toDoListSvc: ChaiTea.Components.Services.IToDoListSvc): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'A',
            scope: {
                checksite : '='
            },
            link: link
        };

        function link($scope, element, attrs) {
                element.bind('click', function (event) {
                    event.preventDefault();
                    if ($scope.checksite) {
                        $translate('TODO_SITE_REQUIRED').then((translation) => {
                            alertSvc.alertWithCallback(translation);
                        });
                        return;
                    }
                    if (!toDoListSvc.openForm()) {
                        $scope.$apply(function () {

                            $scope.checksite = false;
                        });
                    }
                });
            }
    }
    angular.module('app').directive('toDoList', toDoList);
}