﻿module ChaiTea.Components.Interfaces {
    
    export interface IColumnDrilldownList {
        Columns: Array<IColumnDrilldownListColumn>;
    }

    export interface IColumnDrilldownListColumn {
        Title: string;
        Items: Array<any>;
        Selected: any;
    }
} 