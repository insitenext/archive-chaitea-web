﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Components.Directives {
    'use strict';

    columnDrilldownList.$inject = ['$log', '$compile', 'sitesettings'];
    function columnDrilldownList(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {

        var selectColumnItem = (columnIndex, column: Interfaces.IColumnDrilldownListColumn, item) => {
            this.$log.log(column, item);

            column.Selected = item;
        }

        return <ng.IDirective>{
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                classRowClick: '&rowClick',
                id: '@',
            },
            templateUrl: `${sitesettings.basePath}Content/js/Components/ColumnDrilldownList/Templates/columnDrilldownList.html`,
            link: ($scope: any, $element, $attrs): void => {
                //TODO Needs to be finished being turned into a directive
            }

        };
    }
    angular.module('app').directive('columnDrilldownList', columnDrilldownList);

}