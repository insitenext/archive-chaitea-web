﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    class PdfViewerModalCtrl {

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'sitesettings',
            'pdfUrl'
        ];
        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings,
            private pdfUrl: string) {

            this.$scope['pdfUrl'] = pdfUrl;

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {
            this.$modalInstance.close();
        };

    }

    angular.module('app').controller('components.map.pdfviewermodalctrl', PdfViewerModalCtrl);
}