﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Components {
    'use strict';

    class PdfViewerCtrl {

        modalInstance;

        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            '$uibModal',
            'sitesettings'];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private $modal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings) {
        }


        private $onInit = () => {

        }

        public loadPdf = (pdfUrl: string, course: any) => {

            var url = this.sitesettings.basePath + 'Content/js/Components/KnowledgeCenter/Templates/pdf.tmpl.html';
            var template = `<div class="modal-body">
                                <ng-pdf template-url="${url}" debug="false" scale="page-fit"></ng-pdf>
                            </div>
                            <div class="modal-footer" ng-hide="vm.hideFooter">
                                <a ng-click="vm.close($event)" class="lightbox-btn close-light pull-right pri-color btn btn-primary btn-lg btn-block">Close</a>
                            </div>`;

            this.modalInstance = this.$modal.open({
                template: template,
                controller: 'components.map.pdfviewermodalctrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    pdfUrl: () => { return pdfUrl; }
                }
            });

            this.modalInstance.result.then((res: any) => {

                var scope: any = this.$scope;
                if (scope.$ctrl.onClose) {
                    scope.$ctrl.onClose();
                }
                
            }, () => { });

        }

    }


    angular.module('app').component('pdfViewer', {
        controller: PdfViewerCtrl,
        bindings: {
            buttonText: '@',
            onClose: '&',
            pdfUrl: '<',
            course: '<',
        },
        template: `<a href="javascript:void(0);" ng-click="$ctrl.loadPdf($ctrl.pdfUrl, $ctrl.course)">{{$ctrl.buttonText}}</a>`
    });

}