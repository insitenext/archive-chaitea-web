﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {

    'use strict';

    old.$inject = ['sitesettings', '$compile'];

    function old(sitesettings: ISiteSettings, $compile: angular.ICompileService): angular.IDirective {

        return <ng.IDirective>{
            transclude: true,
            restrict: 'EA',
            scope: {
               
            },
            link: link,
            templateUrl: sitesettings.basePath + 'Content/js/Components/Old/Templates/Old.html',
            controller: ['$scope', '$compile', '$http', ($scope, $compile, $http): void => {
            }]
        };
        function link($scope, element, attrs) {}

    }
    angular.module('app').directive('old', old);
}