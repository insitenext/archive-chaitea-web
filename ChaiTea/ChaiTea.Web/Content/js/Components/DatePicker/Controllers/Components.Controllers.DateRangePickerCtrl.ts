﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Controllers {
    'use strict';

    import coreSvc = ChaiTea.Core.Services;
    import commonSvc = ChaiTea.Common.Services;

    export interface IInitializeOptions {
        isDatePickerOff: boolean;
    }

    export interface IDateRangePicker {
        initialize(options): void;
        onFailure(response: any): void;
        startDateOnChange(): void;
        onSwitchMonthsRange(month: number, $event): void;
        onSwitchDateRange(): void;
        onClearSessionDate(): void;
        broadCastDateChange(dateRange:IDateRange): void;
    }

    export interface IDateRange {
        startDate?: Date;
        endDate?: Date;
    }

    class DateRangePickerCtrl implements IDateRangePicker {
        defaultDateFormat = "MM-DD-YYYY";

        dateRange: IDateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month').toDate(),
            endDate: new Date()
        }

        userId: number = 0;
        isDatePickerOff: boolean = false;
        datepickers = {
            startDate: false,
            endDate: false
        };
        minDate = moment(new Date()).subtract(3, 'years').format();
        maxDate = new Date();
        monthActive: number;

        calendarOptions: angular.ui.bootstrap.IDatepickerConfig;

        headerIconSrc: string;

        static $inject = [
            '$scope',
            '$filter',
            'userContext',
            'userInfo',
            'chaitea.core.services.messagebussvc',
            '$uibModalInstance',
            'chaitea.core.services.dateformattersvc',
            '$log',
            'chaitea.common.services.localdatastoresvc',
            '$translate',
            'sitesettings',
            'chaitea.common.services.apibasesvc',
            ChaiTea.Common.Services.AwsSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $filter: angular.IFilterService,
            userContext: IUserContext,
            userInfo: IUserInfo,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private $log: angular.ILogService,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private $translate: angular.translate.ITranslateService,
            private sitesettings: ISiteSettings,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc
        ) {

            this.userId = userInfo.userId;

            // Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('date.selection-' + this.userId.toString());
            if (dateSelection) {
                this.dateRange = {
                    startDate: moment(dateSelection.startDate).toDate(),
                    endDate: moment(dateSelection.endDate).toDate()
                }
                //this.onSwitchDateRange();
            }

            //init 
            this.onClearSessionDate();
            this.monthActive = this.getMonthsByDateRange(this.dateRange);

            this.headerIconSrc = this.awsSvc.getUrl('public/icons/calendar.svg');

            this.calendarOptions = <angular.ui.bootstrap.IDatepickerConfig>{
                showWeeks: false
            }
        }

        /** @description Initializer for the controller. */
        initialize = (options: IInitializeOptions): void => {
            options = options || { isDatePickerOff: false };
            this.isDatePickerOff = options.isDatePickerOff;
        }

        /**
         * @description Returns the number of months between a date range.
         */
        getMonthsByDateRange = (dateRange: IDateRange): number=> {
            return moment(dateRange.endDate).diff(moment(dateRange.startDate), 'month') + 1;
        }

        /**
         * @description Triggered when the fromDate datepicker changes (onBlur).
         */
        startDateOnChange = (): void => {
            if (this.dateRange.startDate) {
                this.monthActive = 0;

                if (this.dateRange.endDate) return;
                this.dateRange.endDate = new Date();
            } else {
                this.dateRange.endDate = null;
                this.monthActive = 6; // reset
            }
        }

        /**
         * @description Set active button and broadcast current month.
         * @description Convert the month into a fromDate and toDate date range.
         * @param {int} month
         * @param {obj} $event
         */
        onSwitchMonthsRange = (month: number, $event): void=> {
            if (month == this.monthActive) return;

            this.monthActive = month;
            this.dateRange = {
                startDate: moment(new Date()).subtract(month - 1, 'months').startOf('month').toDate(),
                endDate: new Date()
            }
            this.broadCastDateChange({
                    startDate: moment(this.dateRange.startDate).startOf('month').toDate(),
                    endDate: this.dateRange.endDate
                }
            );
            this.$uibModalInstance.close(this.dateRange);
        }
        
        /**
         * @description Handle date range submit button.
         */
        onSwitchDateRange = (): void=> {
            if (this.dateRange.endDate) {
                this.broadCastDateChange({
                        startDate: moment(this.dateRange.startDate).startOf('month').toDate(),
                        endDate: this.dateRange.endDate
                    });
                this.$uibModalInstance.close(this.dateRange);
            }


        }

        onClearSessionDate = (): void => {
            this.messageBusSvc.onMessage('sessionStorage:clear-date', null,() => {
                this.localDataStoreSvc.setObject('date.selection-' + this.userId.toString(), "");
            })
        }

        cancel = (): void => {
            this.$uibModalInstance.close(this.dateRange);
        }
        /**
         * @description Broadcast wrapper.
         * @param {string} eventName
         * @param {obj} obj - data to pass to subscribers.
         */
        broadCastDateChange = (obj:IDateRange): void=> {
           
            // Set active month and store in local storage
            this.monthActive = this.getMonthsByDateRange(obj);
            this.localDataStoreSvc.setObject('date.selection-' + this.userId.toString(), JSON.stringify(obj));

            this.messageBusSvc.emitMessage(Common.Interfaces.MessageBusMessages.HeaderDateChange, obj);
        }

        /** 
          * @description Handle opening datepicker. 
          * @param {obj} $event
          * @param {string} which - The name of the datepicker.
          */
        openDatePicker = ($event, which) => {
            $event.preventDefault();
            $event.stopPropagation();

            this.datepickers[which] = true;
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        }
    }

    angular.module('app').controller('DateRangePickerCtrl', DateRangePickerCtrl);
}