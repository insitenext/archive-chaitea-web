﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Components.Directives {
    import commonSvc = ChaiTea.Common.Services;

    class DateRangePickerCtrl implements angular.IComponentController {
        endDate;
        startDate;
        headerIconSrc: string;

        static $inject = ['$log', 'sitesettings', 'userInfo', 'chaitea.common.services.localdatastoresvc', '$uibModal'];
        constructor(private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private localDataStoreSvc: commonSvc.ILocalDataStoreSvc,
            private $uibModal: angular.ui.bootstrap.IModalService) {
        }

        $onInit() {
            this.endDate = new Date();
            //by default on load setting it to 5 months 
            this.startDate = moment(new Date()).subtract(5, 'months').startOf('month').toDate();

            //Try setting date range from session storage.
            var dateSelection = this.localDataStoreSvc.getObject('date.selection-' + this.userInfo.userId.toString());
            if (dateSelection) {
                this.startDate = moment(dateSelection.startDate).toDate(),
                    this.endDate = moment(dateSelection.endDate).toDate()
            }
        }

        public openDateRangePickerModal = () => {
            this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Components/DatePicker/Templates/dateRangePickerPanel.html',
                controller: 'DateRangePickerCtrl',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {

                }
            }).result.then(dateRange => {
                this.endDate = dateRange.endDate;
                this.startDate = dateRange.startDate;
            });
        }
    }

   
    angular.module('app').component('dateRangePicker', {
        controller: DateRangePickerCtrl,
        transclude:true,
        template: `
                <a class="{{$ctrl.buttonClass || 'btn-date-range'}}" ng-click="$ctrl.openDateRangePickerModal()">
                    <span class="fa fa-calendar"></span>
                    <span class="visible-md-inline-block visible-lg-inline-block">
                        <span class="date"> {{ $ctrl.startDate | date: 'MMM y' }} </span> - <span class="date" > {{ $ctrl.endDate | date: 'MMM y' }} </span>
                    </span>
                </a>`,
        bindings: {
            buttonClass:'@'
        }
    });
}