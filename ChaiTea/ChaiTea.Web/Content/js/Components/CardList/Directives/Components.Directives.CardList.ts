﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Components.Directives {
    'use strict';

    
    export interface ICardListScope extends ng.IScope {
        model: any;
        linkClass: string;
        title: string;
        searchPlaceholderText: string;
        maxChars: number;
        loadRowTemplate(): void;
        onRowClick(index: number): void;
        classRowClick(index: any): void;
        defaultProfileImage: string;
        searchFilter: string;
        hasTranscluded?:boolean;
    }

    /**
    * @description Wrapper directive for the card list
    */
    cardList.$inject = ['$log', '$compile', 'sitesettings'];
    function cardList(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            transclude: true,
            restrict: 'E',
            scope: {
                id: '@',
                searchFilter: '=',
                customClass: '@',
                hideHeader: '@',
                hideFilter: '@'
        },
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/cardList.html`,
            controller: ['$scope','$compile','$http',($scope, $compile, $http): void => {
            }],
            link: ($scope: any, $element, $attrs: any): void => {
                $scope.title = $attrs.title || '';
                $scope.searchPlaceholderText = $attrs.searchPlaceholderText || 'Search List';
                
                if ($attrs.hideHeader == 'true')
                    $scope.hideHeader = true;
                else
                    $scope.hideHeader = false;

            }
        };
    }
    angular.module('app').directive('cardList', cardList);

    /**
    * @description Profile image row
    */
    cardRowProfileImage.$inject = ['$log', '$compile', 'sitesettings'];
    function cardRowProfileImage(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'E',
            replace: false,
            //require: 'ngModel',
            require: '^cardList',
            scope: {
                model: '=ngModel',
                classRowClick: '&rowClick',
                id: '@'
            },
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/cardRowImage.html`,
            link: ($scope: ICardListScope, $element, $attrs: any, controllerInstance): void => {

                $scope.$parent.$parent.$watch('searchFilter', function (value:string) {
                    $scope.searchFilter = value;
                });

                $scope.defaultProfileImage = '~/Content/img/user.jpg';

                //call row click function in type script class
                $scope.onRowClick = (index: number) => {
                    return $scope.classRowClick({ index: index });
                };
                
                //get attrs and set defaults
                $scope.linkClass = $attrs.linkClass || 'fa-angle-right';
                $scope.title = $attrs.title || '';
                $scope.searchPlaceholderText = $attrs.searchPlaceholderText || 'Search List';
                $scope.maxChars = $attrs.maxChars || 200;

            },
        };

    }
    angular.module('app').directive('cardRowProfileImage', cardRowProfileImage);


    /**
    * @description Profile Empty row
    */
    cardRow.$inject = ['$log', '$compile', 'sitesettings'];
    function cardRow(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            transclude: true,
            restrict: 'E',
            replace: true,
            //require: 'ngModel',
            require: ['^cardList'],
            scope: {
                customClass: '@',
                classRowClick: '&rowClick',
                rowClickHref:'@',
                id: '@'
            },
            //scope:true,
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/cardRow.html`,
            link: ($scope: any, $element, $attrs, controllerInstance): void => {
                if ($scope.rowClickHref) {
                    $element.addClass('clickable');
                    $element.click(() => { window.location.href = $scope.rowClickHref; });
                }
                
                $scope.$watch(() => {
                     return $scope.$parent.$parent.searchFilter
                },
                    (newVal) => {
                         $scope.parentFilter = newVal;
                    })

            },
            controller: ['$scope','$compile','$http',($scope, $compile, $http): void => {}]
        };

    }
    angular.module('app').directive('cardRow', cardRow);

   /**
   * @description First section of row
   */
    rowLeft.$inject = ['$log', '$compile', 'sitesettings'];
    function rowLeft(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            transclude:true,
            restrict: 'E',
            require: ['^cardRow'],
            scope: {
                customClass: '@',
                imgSrc: '=imgSrc',
                imgAlt:'@imgAlt'
            },
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/rowLeft.html`,
            link: ($scope: ICardListScope, $element, $attrs, controllerInstance, $transclude): void => {
                $scope.defaultProfileImage = sitesettings.defaultProfileImage;
                
                $transclude(function (clone) {
                    if (clone.length) {
                        $scope.hasTranscluded = true;
                    }
                });
                
            },

        }
    }

    /**
   * @description Middle section of row
   */
    rowMiddle.$inject = ['$log', '$compile', 'sitesettings'];
    function rowMiddle(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            transclude: true,
            restrict: 'E',
            require: ['^cardRow'],
            scope: {
                customClass:'@',
                mainTitle: '@',
                subTitle: '@',
                subSubTitle: '@'
            },
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/rowMiddle.html`,
            link: ($scope: ICardListScope, $element, $attrs, controllerInstance, $transclude): void => {
                $transclude(function (clone) {
                    if (clone.length) {
                        $scope.hasTranscluded = true;
                    }
                });

            },

        }
    }

   /**
   * @description Right section of row
   */
    rowRight.$inject = ['$log', '$compile', 'sitesettings'];
    function rowRight(
        $log: angular.ILogService,
        $compile: angular.ICompileService,
        sitesettings: ISiteSettings): angular.IDirective {
        return <ng.IDirective>{
            transclude: true,
            restrict: 'E',
            require: ['^cardRow'],
            templateUrl: `${sitesettings.basePath}Content/js/Components/CardList/Templates/rowRight.html`,
            link: ($scope: ICardListScope, $element, $attrs, controllerInstance, $transclude): void => {
                $transclude(function (clone) {
                    if (clone.length) {
                        $scope.hasTranscluded = true;
                    }
                });
            },

        }
    }

    angular.module('app')
        .directive('rowLeft', rowLeft)
        .directive('rowMiddle', rowMiddle)
        .directive('rowRight', rowRight);
}