﻿module ChaiTea.Components.CardList.Interfaces {
    
    export interface IBasicRow {
        FirstHeading: string;
        SecondHeading: string;
        ThirdHeading: string;
    }

} 