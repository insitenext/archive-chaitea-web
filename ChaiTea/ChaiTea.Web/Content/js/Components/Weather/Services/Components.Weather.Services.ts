﻿module ChaiTea.Components.Services {
    export interface IWeatherService {
        setLatLong(latitude: string, longitude: string): void;
        currentForecast(callback);
        autoSetLatLong():angular.IPromise<any>;
    }

    class WeatherService {
        LocalStoreWeatherKey = "sbm.geolocation";

        apiKey = '62dc8902c4fcf046cccaf23db2e36ea6';
        lat:string = '38.5556';
        lon:string = '-121.478851';
        interval = 1000 * 60 * 15; //15 minutes
        cachedForecast;
        static $inject = ['$log','$http', '$interval','$window','$q', 'chaitea.common.services.localdatastoresvc'];
        constructor(
            private $log:angular.ILogService,
            private $http: angular.IHttpService,
            private $interval: angular.IIntervalService,
            private $window: angular.IWindowService,
            private $q:angular.IQService,
            private localDataStoreSvc: ChaiTea.Common.Services.ILocalDataStoreSvc) {  }

        public $get(): IWeatherService {
            return {
                currentForecast: (callback) => (this.currentForecast(callback)),
                setLatLong: (latitude: string, longitude: string) => (this.setLatLong(latitude, longitude)),
                autoSetLatLong: () => (this.autoSetLatLong())
            }
        }

        public currentForecast = (callback) => {
            var that = this;
            if (!this.cachedForecast) {
                this.pollForecastIO(function (err, data) {
                    that.cachedForecast = data;
                    callback(null, that.cachedForecast);
                    that.startInterval();
                })
            } else {
                callback(null, this.cachedForecast);
            }
        }

        private pollForecastIO = (callback) => {
            var url = ['https://api.forecast.io/forecast/', this.apiKey, '/', this.lat, ',', this.lon, '?callback=JSON_CALLBACK'].join('');
            var $self = this;
            this.$http.jsonp(url)
                .success(function (data) {
                callback(null, data);
            })
                .error(function (error) {
                $self.$log.error('There was an error: ' + error);
                callback(error);
            });
        }

        public autoSetLatLong = ():angular.IPromise<any> => {
            var def = this.$q.defer();
            var that = this;

            var localStore = this.localDataStoreSvc.getObject(this.LocalStoreWeatherKey);
            if (localStore) {
                this.setLatLong(localStore.latitude, localStore.longitude);
                console.log('local');
                def.resolve();
            }

            this.$window.navigator.geolocation.getCurrentPosition(function (position) {
                that.setLatLong(position.coords.latitude, position.coords.longitude);
                that.localDataStoreSvc.setObject(that.LocalStoreWeatherKey, { latitude: position.coords.latitude, longitude: position.coords.longitude});
                def.resolve();
            },(error)=> {
                this.$log.error(error);
                this.$log.log('Weather using default lat and long');               
                def.resolve();
            });
            return def.promise;
        }

        public setLatLong = (latitude, longitude) => {
            this.lat = latitude;
            this.lon = longitude;
        }

        private startInterval = () => {
            // poll on an interval to update forecast
            var that = this;
            this.$interval(function () {
                that.pollForecastIO(function (err, data) {
                    console.log('updated forecast');
                    that.cachedForecast = data;
                });
            }, that.interval);
        }
    }
    angular.module('app').service('WeatherService', WeatherService);
}