﻿module ChaiTea.Components.Directives {
    class WeatherComponentCtrl implements angular.IComponentController {
        size: string; // binding
        currentWeather;
        static $inject = ['$log', 'WeatherService', 'sitesettings'];
        constructor(private $log: angular.ILogService,
            private WeatherService: ChaiTea.Components.Services.IWeatherService,
            private sitesettings: ISiteSettings) { }

        $onInit() {
            this.WeatherService.autoSetLatLong().then(() => {
                this.WeatherService.currentForecast((err, data) => {
                    if (!data) {
                        this.$log.error('No weather data');
                        return;
                    }
                    this.currentWeather = data.currently;
                    this.currentWeather._icon = this.currentWeather.icon.toLowerCase().indexOf('cloud') > 0 ? 'fa-cloud2' : 'fa-sun';
                });
            }, this.$log.error);
        }
    }
    angular.module('app').component('weatherComponent', {
        templateUrl: SiteSettings.basePath + 'Content/js/Components/Weather/Templates/weather-component.tmpl.html',
        controller: WeatherComponentCtrl,
        bindings: {
            size:'@'
        }
    });
}