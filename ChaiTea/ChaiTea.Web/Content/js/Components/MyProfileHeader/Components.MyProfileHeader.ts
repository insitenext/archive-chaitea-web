﻿module ChaiTea.Components {

    import commonServices = ChaiTea.Common.Services;

    interface IMyProfileUserInfo extends IUserInfo {
        HireDate: Date;
        JobDescription: { Name: string };
        AboutMe: string;
        Email: string;
        FunFacts: string;
        PhoneNumber: number;
    }

    class MyProfileHeaderController{
        activeLink: string; //from bindings

        isActive: boolean;
        currentEmployeeId: number;
        employee: IMyProfileUserInfo = <any>{};
        modalInstance;

        socialPictureFiles = [];
        backgroundPictureFiles = [];

        canEditItems: boolean = false;
        isEmployee: boolean = false;
        isLoggedInUserOrManager: boolean = false;
        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            'userContext',
            'userInfo',
            'sitesettings',
            '$uibModal',
            commonServices.ApiBaseSvc.id,
            commonServices.UtilitySvc.id,
            commonServices.UserInfoSvc.id,
            commonServices.ImageSvc.id,
            commonServices.NotificationSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs,
            private $log: angular.ILogService,
            private userContext: IUserContext,
            private userInfo: IUserInfo,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private apiSvc: commonServices.IApiBaseSvc,
            private utilitySvc: commonServices.IUtilitySvc,
            private userInfoSvc: commonServices.IUserInfoSvc,
            private imageSvc: commonServices.IImageSvc,
            private notificationSvc: commonServices.INotificationSvc
        ) {
            this.isEmployee = this.userInfo.mainRoles.employees;
        }

        public $onInit = () => {
            //Check for new CE_ID query param
            this.currentEmployeeId = this.utilitySvc.getQueryParamFromUrl(Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString());
            //If no CE_ID look for route id
            if (!this.currentEmployeeId) {
                this.currentEmployeeId = this.utilitySvc.getIdParamFromUrl(null,true);
            }
            this.currentEmployeeId = (this.currentEmployeeId || this.userInfo.userId);
            this.isLoggedInUserOrManager = (this.currentEmployeeId == this.userInfo.userId) || this.userInfo.mainRoles.managers;
            this.$scope.$watch(() => this.employee.backgroundPicture, (newVal, oldVal) => {
                if (newVal == oldVal) return;
                //Set custom background image
                if (oldVal != newVal) {
                    this.$element.css('background-image', `url(${this.employee.backgroundPicture})`);
                }
            }, true);

            if (this.currentEmployeeId) {
                this.isActive = true;
                this.userInfoSvc.getUserInfoByOrgUserId(this.currentEmployeeId)
                    .then((result: IUserInfo) => {
                        //TODO fix issue with uploading etc.
                        if (!result.employeeId && !result.orgUserId) {
                            this.employee = _.merge(this.userInfo, this.employee);
                            this.employee.userId = this.userContext.UserId;
                            this.employee.currentPicture = this.imageSvc.getUserImageFromAttachments(this.employee.userAttachments, true);
                            this.employee.socialPictures = this.imageSvc.getSocialImagesFromAttachments(this.employee.userAttachments);
                            this.canEditItems = (this.employee.orgUserId == this.userInfo.userId)
                                || this.userInfo.mainRoles.managers
                                || this.userInfo.mainRoles.crazyprogrammers;
                            if (_.some(this.employee.userAttachments, { 'UserAttachmentType': 'BackgroundPicture' })) {
                                this.employee.backgroundPicture = this.imageSvc.getFromAttachments(this.employee.userAttachments, Common.Interfaces.ALL_ATTACHMENT_TYPES.BackgroundPicture);
                                this.$element.css('background-image', `url(${this.employee.backgroundPicture})`);
                            }                            
                            return;
                        }
                        this.apiSvc.getById(result.userId, 'UserInfo').then((user) => {
                            this.employee.AboutMe = user.AboutMe;
                            this.employee.Email = user.Email;
                            this.employee.FunFacts = user.FunFacts;
                            this.employee.PhoneNumber = user.PhoneNumber;
                        }, this.handleFailure);
                        this.employee.employeeId = result.employeeId || this.currentEmployeeId;
                        this.employee.userId = result.userId;
                        this.employee.orgUserId = this.currentEmployeeId;
                        this.employee.name = result.firstName + ' ' + result.lastName;
                        this.employee.currentPicture = this.imageSvc.getUserImageFromAttachments(result.userAttachments, true);

                        this.employee.socialPictures = this.imageSvc.getSocialImagesFromAttachments(result.userAttachments);
                        this.employee.backgroundPicture = SiteSettings.defaultBackgroundImage;
                        if (_.some(result.userAttachments, { 'UserAttachmentType': 'BackgroundPicture' })) {
                            this.employee.backgroundPicture = this.imageSvc.getFromAttachments(result.userAttachments, Common.Interfaces.ALL_ATTACHMENT_TYPES.BackgroundPicture);
                        } 
                        this.employee = angular.extend(this.employee, result);
                        this.canEditItems = (this.employee.orgUserId == this.userInfo.userId)
                            || this.userInfo.mainRoles.managers
                            || this.userInfo.mainRoles.crazyprogrammers;
                    }, this.handleFailure)
                    .then(() => {
                        var params = {
                            $filter: `EmployeeId eq ${this.currentEmployeeId}`,
                            $expand: 'JobDescription',
                            $select: 'HireDate, JobDescription/Name'
                        };
                        this.apiSvc.getByOdata(params, Common.ApiServices.Interfaces.ApiEndpoints[Common.ApiServices.Interfaces.ApiEndpoints.TrainingEmployee])
                            .then((res: { HireDate: Date, JobDescription: { Name: string } }) => {
                                if (res[0] !== undefined) {
                                    this.employee.HireDate = res[0].HireDate;
                                    this.employee.JobDescription = res[0].JobDescription;
                                }
                            }, this.handleFailure);
                    });
            } else {
                this.employee = _.merge(this.userInfo, this.employee);
            }
        }

        /**
        * @description function to save image to AWS/S3 and use returned ID to insite db.
        * @param {string} fileName
        */
        public addSocialPicture = (user, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length && files[0]) {
                var file = files[0];

                this.imageSvc.readImage(file).then((data: any) => {
                    //error check the image here
                    this.cropImage(user, data.dataUrl, Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]);
                }, this.handleFailure);
            }
        }

        public addBackgroundPicture = (user, files, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if (files && files.length) {
                var file = files[0];

                this.imageSvc.readImage(file).then((data: any) => {
                    //error check the image here
                    this.cropImage(user, data.dataUrl, Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.BackgroundPicture]);
                }, this.handleFailure);
            }
        }

        public removeSocialPicture = ($index, uniqueId) => {
            if (!uniqueId) {
                this.$log.warn("Cannot delete image");
                return;
            }

            bootbox.confirm('Are you sure you want to remove this photo?', (result) => {
                if (result) {
                    _.remove(this.employee.socialPictures, item => (item.uniqueId == uniqueId));

                    //TODO fix finding item and removing from userAttachments
                    _.remove(this.employee.userAttachments, (item) => (item.UniqueId == uniqueId));
                    this.userInfoSvc.updateUserInfo(this.employee.userId, this.employee).then((data) => {
                        this.notificationSvc.successToastMessage("Information has been updated.");
                    }, this.handleFailure);
                }
            });
        }

        private cropImage = (user: any, file: any, userAttachmentType?: any) => {

            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'Common.ImageCropCtrl as vm',
                size: 'md',
                resolve: {
                    file: () => { return file; },
                    needDescription: () => { return userAttachmentType == Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]; }
                },
                backdrop: 'static'
            });

            this.modalInstance.result.then((modalRes: any) => {

                if (!modalRes) return;

                this.imageSvc.save(modalRes.originalImage, false, true).then((res: Common.Interfaces.IAwsObjectDetails) => {

                    var tempAttach = {
                        UniqueId: res.Name,
                        UserAttachmentType: userAttachmentType,
                        AttachmentType: 'Image',
                        Description: modalRes.imageDescription
                    }

                    user = this.utilitySvc.mapKeysToCapital(user);

                    user.UserAttachments = _.isEmpty(user.UserAttachments) ? [] : user.UserAttachments;
                    user.UserAttachments.push(tempAttach);
                    this.imageSvc.save(modalRes.croppedImage, false, true, this.imageSvc.getThumbnailName(res.Guid)).then((res: Common.Interfaces.IAwsObjectDetails) => {
                        this.userInfoSvc.updateUserInfo(user.UserId, user).then((res) => {
                            if (!res) throw Error('No result');
                            this.notificationSvc.successToastMessage('Image saved successfully.')
                            if (userAttachmentType == Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.SocialPicture]) {
                                this.employee.socialPictures = [] = angular.extend([], this.imageSvc.getSocialImagesFromAttachments(res.UserAttachments));
                            } else if (userAttachmentType == Common.Interfaces.ALL_ATTACHMENT_TYPES[Common.Interfaces.ALL_ATTACHMENT_TYPES.BackgroundPicture]) {
                                this.employee.backgroundPicture = this.imageSvc.getFromAttachments(res.UserAttachments, Common.Interfaces.ALL_ATTACHMENT_TYPES.BackgroundPicture);
                                this.$element.css('background-image', `url(${this.employee.backgroundPicture})`);
                            }
                        }, this.handleFailure);

                    }, (error) => {
                        this.notificationSvc.errorToastMessage(error);
                    });

                }, (error) => {
                    this.notificationSvc.errorToastMessage(error);
                });

            }, () => {

            });

        }

        private handleFailure = (err) => {
            this.$log.error(err);
        }
    }

    angular.module('app').component('myProfileHeader', {
        bindings: {
            activeLink: '@'
        },
        controller: MyProfileHeaderController,
        templateUrl: SiteSettings.basePath + 'Content/js/Components/MyProfileHeader/Templates/myProfileHeader.tmpl.html'
    });

    angular.module('app').config(['$stateProvider', 'sitesettings', '$locationProvider', '$provide', function ($stateProvider, sitesettings, $locationProvider, $provide) {
        

        $stateProvider
            .state('userProfile', {
                url: `/User/Profile/Index?${Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId}`,
                external: true
            })
            .state('userReportCard', {
                url: `/Personnel/MyProfile/ReportCard?${Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId}`,
                external: true
            })
            .state('userTranscript', {
                url: `/Training/Transcript?${Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId}`,
                external: true
            })
            .state('userJob', {
                url: `/Training/PositionProfile/EntryView?${Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId}`,
                external: true
            })
            .state('userFinancials', {
                url: `/Personnel/MyProfile/Index?${Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId}`,
                external: true
            })
            .state('team', {
                url: `/Personnel/MyProfile/TeamMembers`,
                external: true
            });
    }])
}