﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Components {
    'use strict';
    const componentPath = ChaiTea.SiteSettings.relativeModulePaths.Components + 'MessageBuilder/';
    const wzTemplatePath = componentPath + "Templates/wizard.tmpl.html";

    export interface MessageBuilderScope extends angular.IScope {
        isReuse: boolean;
        messageId?: number;
        reviewMode: boolean;
        editMode: boolean;
    }

    class MessageBuilderCtrl {
        messageId:number; //from bindings
        isReuse: boolean; //from bindings
        reviewMode: boolean; //from bindings
        editMode: boolean; //from bindings

        messageBuilder: ChaiTea.Settings.Interfaces.IMessageBuilder;
        department: ChaiTea.Settings.Interfaces.IDepartment;
        
        sites = [];
        positions = [];
        departments = [];

        selectedSites = [];
        selectedPositions = [];
        selectAllSites: boolean = false;
        selectAllPositions: boolean = false;

        //wysiwyg menu
        customMenu = [
            ['bold'], ['italic'], ['underline'], ['ordered-list'], ['unordered-list'], ['link']
        ];
        
        static $inject = ['$scope', '$element', '$attrs', '$log',
            '$window',
            '$uibModal',
            'chaitea.settings.services.jobs',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            Common.Services.NotificationSvc.id,
            'WizardHandler'];
        constructor(
            private $scope: MessageBuilderScope,//angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: ng.ILogService,
            private $window: angular.IWindowService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private jobSvc: ChaiTea.Settings.Services.IJobSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private notificationSvc: Common.Services.NotificationSvc,
            private WizardHandler: any        ) {

        }

        public $onInit = (): void => {
            this.resetMessage();

            if (this.messageId) {
                this.apiSvc.getById(this.messageId, 'TrainingMessage').then((res) => {
                    this.messageBuilder = res;

                    if (this.isReuse) this.messageBuilder.TrainingMessageId == 0;

                    this.department = res.Department.DepartmentId;
                    this.selectedSites = res.Sites;
                    this.selectedPositions = res.Jobs;

                    if (this.WizardHandler) {
                        var wzStartIndex = this.reviewMode ? 0 : 4;
                        this.WizardHandler.wizard().goTo(wzStartIndex);
                    }
                }, this.onFailure);
            }
        }

        /**
        * @description Gets and sets list of positions
        */
        public getPositions = (): void => {
            if (this.positions.length) return;

            this.apiSvc.getByOdata({ $select: 'JobId,Name', $orderby: 'Name asc' }, ChaiTea.Services.Interfaces.ApiEndpoints[ChaiTea.Services.Interfaces.ApiEndpoints.Job])
                .then((positions): any => {
                    if (!positions.length) return this.$log.log('No positions available.');

                    this.positions = [];

                    this.positions = positions;
                },this.onFailure);
        }

        /**
       * @description Gets and sets list of client sites
       */
        private getClientSite = (): void => {
            if (this.sites.length) return;

            var siteName;
            this.apiSvc.getByOdata({ $filter: `Type eq 'Site'`, $orderby: 'Name asc' }, 'client/orgs').then((res) => {
                this.sites = res;
            }, this.onFailure);
        }

        /**
       * @description Add item to this.selectedSites
       */
        public addSite = (item) => {
            if (_.any(this.selectedSites, item)) return;

            this.selectedSites.push(item);
        }

        /**
       * @description Remove item from this.selectedSites
       */
        public removeSite = (item) => {
            var existItem = _.find(this.selectedSites, item);
            if (existItem) {
                _.pull(this.selectedSites, existItem);
            }
        }

        /**
       * @description Add or remove position on check or uncheck
       */
        private addPosition = (item) => {
            if (_.any(this.selectedPositions, item)) return;

            this.selectedPositions.push(item);
        }

        /**
       * @description Remove item from this.selectedPosition
       */
        public removePosition = (item) => {
            var existItem = _.find(this.selectedPositions, item);
            if (existItem) {
                _.pull(this.selectedPositions, existItem);
            }
        }

        /**
       * @description Select all sites
       */
        private checkAllSites = () => {
            this.selectedSites = [];
            if (this.selectAllSites) {
                this.selectAllSites = true;
            } else {
                this.selectAllSites = false;
            }

            angular.forEach(this.sites, (site) => {
                site.selected = this.selectAllSites;
                if (this.selectAllSites) {
                    this.selectedSites.push(site);
                }
            });

        }

        /**
       * @description Select all positions
       */
        private checkAllPositions = () => {
            this.selectedPositions = [];
            if (this.selectAllPositions) {
                this.selectAllPositions = true;
            } else {
                this.selectAllPositions = false;
            }

            angular.forEach(this.positions, (position) => {
                position.selected = this.selectAllPositions;
                if (this.selectAllPositions) {
                    this.selectedPositions.push(position);
                }
            });

        }

        /**
        * @description Gets and sets list of departments
        */
        private getDepartments = () => {
            this.apiSvc.getByOdata({ $orderby: 'Name asc' }, ChaiTea.Services.Interfaces.ApiEndpoints[ChaiTea.Services.Interfaces.ApiEndpoints.Department])
                .then((departments): any => {
                if (!departments.length) return this.$log.log('No departments available.');

                this.departments = [];

                this.departments = departments;
                }, this.onFailure);
        }

        /**
       * @description to save message and position
       */
        public send = (): void => {
            this.messageBuilder.Jobs = [];
            this.messageBuilder.Jobs = this.selectedPositions;
            this.messageBuilder.Sites = this.selectedSites;
            this.messageBuilder.Department = _.find(this.departments, { 'DepartmentId': this.department });

            if (!this.messageBuilder.Text || this.selectedSites.length == 0 || !this.messageBuilder.Department) {
                bootbox.alert("Provide all required fields");
            }
            else if (this.selectedPositions.length == 0 && !this.messageBuilder.IsCustomer) {
                bootbox.alert("Select either a postion or check the Send to Customer option");
            }
            else {
                this.apiSvc.save(this.messageBuilder, 'TrainingMessage').then((res) => {
                    this.openFinishMessage();
                    this.messageBusSvc.emitMessage(Common.Interfaces.MessageBusMessages.MessageBuilderCreated, res);

                }, this.onFailure);
            }
        }

        public reUse = (): void => {
            this.isReuse = true;
            this.reviewMode = false;
            this.editMode = true;
            this.getDepartments();
            this.WizardHandler.wizard().goTo(4);
        }

        private openFinishMessage = () => {
            
            var instance = this.$uibModal.open({
                backdrop: 'static',
                size: 'sm',
                controller: ['$scope',($scope) => {
                    $scope.newMessage = () => {
                        this.resetMessage();
                        $scope.$close();
                    }

                    $scope.reUseMessage = () => {
                        $scope.$close();
                        this.isReuse = true;
                        this.WizardHandler.wizard().goTo(4);
                    }
                }],
                template: `
                            <div class="message-sent text-center padding-left-20 padding-right-20 padding-bottom-20">
                                <h1 class="text-center text-primary">
                                   <span class="fa fa-sent fa-2x"></span>
                                    <br/>
                                   Message Sent!
                                </h1>
                                <p class="margin-bottom-30">Thank you! Have a wonderful day.</p>

                                <button class="btn btn-lg btn-block btn-success margin-bottom-10" ng-click="newMessage()">NEW MESSAGE</button>
                                <button class="btn btn-lg btn-block btn-primary margin-bottom-10" ng-click="reUseMessage()">RE-USE MESSAGE</button>
                                <button class="btn btn-lg btn-block btn-dark margin-bottom-10" ng-click="$dismiss()">CLOSE</button>
                            </div>`
            });

            instance.result.then((res) => { },
                () => {
                    this.WizardHandler.wizard().finish();
                });
        }

        public resetMessage = () => {
            this.messageBuilder = {
                Jobs: [],
                Title: '',
                Text: '',
                Sites: [],
                Department: 0,
                IsCustomer: false,
                TrainingMessageId: 0
            };
            this.sites = [];
            this.selectedSites = [];
            this.positions = [];
            this.selectedPositions = [];
            this.selectAllSites = false;
            this.selectAllPositions = false;
            this.departments = [];
            this.department = null;

            this.getDepartments();

            if (this.WizardHandler.wizard()) {
                this.WizardHandler.wizard().reset();
            }
        }

        public wzGoTo = (step: number) => {
            this.WizardHandler.wizard().goTo(step);
        }


        //Data manipulation 
        public getSelectedSiteNames = () => {
            if (!this.selectedSites || !this.selectedSites.length) return;

            return _.pluck(this.selectedSites, 'Name').join(", ");
        }

        public getSelectedPositionNames = () => {
            if (!this.selectedPositions || !this.selectedPositions.length) return;

            return _.pluck(this.selectedPositions, 'Name').join(", ");
        }

        public getDepartmentById = (id: number) => {
            if (!id || !this.departments.length) return;
            return _.find(this.departments, { 'DepartmentId': id }).Name;
        }

        /**
        * @description Handle errors.
        */
        private onFailure = (response: any): void => {
            this.$log.error(response);
        }
    }

    angular.module('app').component('messageBuilder', {
        controller: MessageBuilderCtrl,
        bindings: {
            messageId: '<',
            isReuse: '<',
            reviewMode: '<',
            editMode: "<",
            onFinish: "&",
            onCancel: "&"
        },
        template: `
            <wizard on-finish="$ctrl.onFinish()" template="${wzTemplatePath}" edit-mode="($ctrl.editMode || false)" hide-indicators="$ctrl.reviewMode">
                <div ng-include="'${componentPath}Templates/wizard-steps.tmpl.html'"></div>
            </wizard>`
    });
    
   
}
