﻿module ChaiTea.Components {
    'use strict';

    class OpenMessageBuilderModalCtrl {
        static $inject = ['$scope', '$element', '$attrs', '$log', '$uibModal'];
        constructor(
            private $scope: MessageBuilderScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService) {

            var element = angular.element(this.$element);

            //Add cursor to hover state.
            element.addClass('clickable');

            element.click(() => {
                this.open();
            });
        }

        public open = () => {
            var modalInstance = this.$uibModal.open({
                backdrop: 'static',
                resolve: {
                    messageId: () => {
                        return this.$scope.messageId;
                    },
                    isReuse: () => {
                        return this.$scope.isReuse;
                    },
                    reviewMode: () => {
                        return this.$scope.reviewMode;
                    }
                },
                controller: ['$scope', 'messageId','reviewMode','isReuse', ($scope, messageId, reviewMode, isReuse) => {
                    $scope.messageId = messageId;
                    $scope.reviewMode = reviewMode;
                    $scope.isReuse = isReuse;
                }],
                controllerAs: '$ctrl',
                template: `
                    <a href="javascript:void(0)" class="pull-right padding-top-10 padding-right-10" ng-click="$close()">
                        <span class="fa fa-times fa-lg text-muted"></span>
                    </a>
                    <message-builder message-id="messageId" review-mode="reviewMode" is-reuse="isReuse" on-cancel="$dismiss()" on-finish="$close()" modal-instance="" edit-mode="true"></message-builder>
                `,
                size: 'lg'
            });

            modalInstance.result.then(function () {

            }, function () {
                this.$log.info('Modal dismissed at: ' + new Date());
            });

        }

    }


    angular.module('app').directive('openMessageBuilder', function (): angular.IDirective {
        return {
            restrict: 'EA',
            controller: OpenMessageBuilderModalCtrl,
            controllerAs: '$ctrl',
            scope: {
                onFinish: '&',
                messageId: '@',
                isReuse: '@',
                reviewMode: '@'
            }
        }
    });
}