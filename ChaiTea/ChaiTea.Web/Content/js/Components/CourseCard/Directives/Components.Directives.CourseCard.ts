﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {

    'use strict';

    courseCard.$inject = ['sitesettings'];

    function courseCard(sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'EA',
            require: 'ngModel',
            scope: {
                course: '=ngModel',
                loadModal: '&'
            },
            link: link,
            templateUrl: sitesettings.basePath + 'Content/js/Components/CourseCard/Templates/courseCard.html'
        };
        function link($scope, element, attrs) {}

    }
    angular.module('app').directive('courseCard', courseCard);
}