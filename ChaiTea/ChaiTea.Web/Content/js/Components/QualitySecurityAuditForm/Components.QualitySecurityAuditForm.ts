﻿module ChaiTea.Components {
    class AuditSecurityCriteriaModalModalCtrl implements angular.IComponentController {
        criterias = [
            {
                title: 'Guard Appearance',
                items: [
                    'Proper uniform not worn. Signs of wash needed. Tears in uniform. Patches are missing. Personal clothing items used.',
                    'Proper uniform is not worn. Signs of wash needed. No tears. Patches are missing. Personal clothing items used.',
                    'Proper uniform is worn, with extra personal clothing items. No signs of wash needed. No tears. Missing company patches.',
                    'Proper uniform is worn, but adjustments needed. No wash needed. No visible tears. Patches displayed. Neat appearance.',
                    'Proper uniform is worn. No wash needed. No visible tears. Patches displayed. Excellent appearance.'
                ],
                open:true
            },
            {
                title: 'Identification Credentials',
                items: [
                    `Does not have any applicable ID\'s (client  ID, company ID, and security license)`,
                    `Does not have all applicable ID\'s (client ID, company ID, and security license) and presents the ID\'s they do have.`,
                    `Has all applicable ID's (client ID, company ID, and security license).Not visible at first request and not presented`,
                    `Has all applicable ID's (client ID, company ID, and security license). Not visible when requested but presented after.`,
                    `Has all applicable ID's (client ID, company ID, and security license) and visibly present.`,
                ]
            },
            {
                title: 'Daily Activity Report Compliance',
                items: [
                    'Report is not used at all where applicable.',
                    'Report is not used where applicable. Missing hourly line items and is not dated and timed correctly. ',
                    'Report is used but not updated every hour where applicable. Report is not dated and timed correctly. ',
                    'Report is used and updated every hour with adequate information where applicable. Report is dated and timed correctly.',
                    'Report is used and updated every hour with detailed information where applicable. Report is dated and timed. '
                ]
            },
            {
                title: 'Post Orders Compliance',
                items: [
                    'Post Orders do not exist. ',
                    'Readily accessible upon request. Updated within last 3 years. Missing Manager sign off and officer acknowledgement.',
                    'Readily accessible upon request. Updated within last 2 years. Missing Manager sign off and officer acknowledgement.',
                    'Readily accessible upon request. Updated within last 12 months. Manager sign off and Officer acknowledgements visible.',
                    'Readily accessible upon request. Updated within last 6 months. Manager sign off and Officer acknowledgments visible.',
                ]
            },
            {
                title: 'Professionalism/Customer Service',
                items: [
                    'Displays lack of professionalism and customer service skills with employees, visitors, and contractors.',
                    'Displays below average professionalism and customer service skills with employees, visitors, and contractors.',
                    'Displays average professionalism and customer service skills with employees, visitors, and contractors.',
                    'Displays adequate professionalism and customer service skills with employees, visitors, and contractors.',
                    'Displays excellent professionalism and customer service skills with employees, visitors, and contractors.',
                ]
            },
            {
                title: 'Post Condition',
                items: [
                    'Not in suitable working condition with deep cleaning and organization required.',
                    'In below average condition with cleaning and organization required.',
                    'In average condition with cleaning and organization recommended.',
                    'In ideal condition with minimal mess and/or clutter.',
                    'In ideal condition free of mess and/or clutter.'
                ]
            }
        ];
        updateDetailScore; //binding
        scoreCardId;
        infoContent = {
            templateUrl: SiteSettings.basePath + 'Content/js/Components/QualitySecurityAuditForm/Templates/quality.securityAuditFormInfo.tmpl.html'
        }
        siteSettings: ISiteSettings;
        audit;
        static $inject = [
            '$scope',
            'sitesettings',
            '$log',
            Common.Services.ApiBaseSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private $log: angular.ILogService,
            private apiSvc: Common.Services.IApiBaseSvc) {

            this.siteSettings = sitesettings;
            this.getAuditById(this.scoreCardId);

        }

        public getAuditById = (scoreCardId: number): void => {
            if (scoreCardId == 0) return;

           
            this.apiSvc.getById(scoreCardId, 'ScoreCard', false).then((result) => {
                this.audit = result;
            }, this.onFailure);

        }

        public onFailure = (response: any): void => {
            this.$log.error(response);
        }

        public getCheckScore = (item): any => {
            var section: any;
            if (this.audit) {
                var sectionIndex = _.findIndex(this.audit.ScorecardSections, function (section: any) {
                    var name: string = section.AreaClassificationName;
                    return name.indexOf(item.title) == -1 ? false : true;
                });
                section = this.audit.ScorecardSections[sectionIndex].ScorecardDetails[0];
                if (section) {
                    if (section.Score) {
                        item.selected = 5 - section.Score;
                    }
                    else {
                        item.selected = null;
                    }
                    return section.Score;
                }
                else {
                    return null;
                }
            }
        }
        public selectCriteria = (criteria, criteriaIndex, $index) => {
            let score = null;
            var areaClassificationName = this.criterias[criteriaIndex].title;
            var sectionIndex = _.findIndex(this.audit.ScorecardSections, function (section: any) {
                                    var name: string = section.AreaClassificationName;
                                    return name.indexOf(criteria.title) == -1 ? false : true;
                                });
            var section: any = this.audit.ScorecardSections[sectionIndex].ScorecardDetails[0];
            if (!_.isNull($index)) {
                criteria.selected = $index;
                criteria.notApplicable = null;
                score = (5 - $index);
                if (section) {
                    section.Score = score;
                }
            } else {
                if (section && criteria.notApplicable) {
                    section.Score = null;
                    score = -1;
                }
                if (section && !criteria.notApplicable) {
                    section.Score = null;
                    score = null;
                }
                criteria.selected = null;
            }
            this.updateDetailScore({ score: score, sectionIndex: sectionIndex, detailIndex: 0 });
        }
    }

    angular.module('app').component('qualitySecurityAuditForm', {
        templateUrl: SiteSettings.basePath + 'Content/js/Components/QualitySecurityAuditForm/Templates/quality.securityAuditForm.tmpl.html',
        controller: AuditSecurityCriteriaModalModalCtrl,
        controllerAs:'vm',
        bindings: {
            goal: '=',
            scoreCardId: '=',
            updateDetailScore: '&'
        }
    });
}