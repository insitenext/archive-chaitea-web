﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Components {

    export enum MESSAGE_TOGGLE_TYPE {
        my,
        team
    }

    export enum NOTIFICATION_FEED_VIEWS {
        messages = <any>'messages',
        activity = <any>'activity',
        innerMessage = <any>'inner-message'
    }
    export enum NOTIFICATION_FEED_VIEWS_ICON {
        messages = <any>'fa-message',
        activity = <any>'fa-bell-o',
    }

    interface IScope extends angular.IScope {
        messageCounts: number;
        activityCounts: number;
    }

    export class NotificationFeedCtrl {
        currentView = {
            items: [],
            name: NOTIFICATION_FEED_VIEWS.activity,
            icon: NOTIFICATION_FEED_VIEWS_ICON.activity,
            isMessages: false,
            allItemsLoaded: false,
            showBtns:false
        };

        isInnerMessage: boolean = false;

        viewModel = {
            messages: {
                filter: '',
                filterItems: [],
                items: <Array<Interfaces.INotification>>[],
                allItemsLoaded: false,
                isData: true
            },
            activity: {
                items: <Array<Interfaces.INotification>>[],
                filter: '',
                filterItems: [],
                allItemsLoaded: false,
                isData: true
            },
            todaysDate: ''
        }

        allNotifications: Array<Interfaces.INotification> = [];

        tabs = [
            {
                heading: 'to-dos',
                count: 0,
                disabled: false
            },
            {
                heading: 'notifications',
                headingMobile: 'notifs',
                count: 0,
                disabled: false
            },
            {
                heading: 'messages',
                count: 0,
                disabled: false
            }
        ];

        pagingObjects = {
            messages: {
                isBusy: false,
                isAllLoaded: false,
                top: 20,
                toSkip: 0
            },
            activity: {
                isBusy: false,
                isAllLoaded: false,
                top: 20,
                toSkip: 0
            }
        };

        radioMessagesModel = MESSAGE_TOGGLE_TYPE.my;
        toggleMessageType = MESSAGE_TOGGLE_TYPE;
        activeToggleType = MESSAGE_TOGGLE_TYPE.my;
        private markingIsBusy;

        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            '$http',
            '$q',
            'sitesettings',
            'userInfo',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.utilssvc',
            'chaitea.common.services.imagesvc'];
        constructor(
            private $scope: IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private $http: angular.IHttpService,
            private $q: ng.IQService,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private serviceMessageBus: ChaiTea.Core.Services.IMessageBusSvc,
            private utilitySvc: ChaiTea.Common.Services.IUtilitySvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc) {

            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsActivityFeed, this.$scope, (event, data) => {
                
                this.getTotalMessages();
                this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.activity, true);

                this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.messages, true);
            });

            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsTodoCount, this.$scope, (event, data) => {
                this.tabs[0].count = _.isObject(data) ? 0 : data;
            });
            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsActivityCount, this.$scope, (event, data) => {
                this.tabs[1].count = _.isObject(data) ? 0 : data;
            });
            this.serviceMessageBus.onMessage(Common.Interfaces.MessageBusMessages.NotificationsMessagesCount, this.$scope, (event, data) => {
                this.tabs[2].count = _.isObject(data) ? 0 : data;
            });
        }

        private $onInit = () => {
            this.viewModel.todaysDate = moment().format('ddd, MMM DD');
            this.getTotalMessages();

            this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.messages, true);
            this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.activity);
        }

        public viewMore = () => {
            if (this.currentView.name === NOTIFICATION_FEED_VIEWS.messages) {
                this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.messages);
            } else {
                this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.activity);
            }
        }

        public onRadioMessageChange = (toggleType: any) => {
            if (toggleType !== this.activeToggleType) {
                this.activeToggleType = toggleType;
                this.radioMessagesModel = toggleType;
                this.viewModel.messages.items = [];
                this.viewModel.messages.filterItems = [];
                this.pagingObjects.messages.toSkip = 0;
                this.pagingObjects.messages.top = 20;
                this.pagingObjects.messages.isAllLoaded = false;
                this.pagingObjects.messages.isBusy = false;
                this.viewModel.messages.isData = true;
                this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.messages);
            }
        }

        private getNotifications = (isTypeActivity: boolean, source, param): angular.IPromise<any> => {

            var def = this.$q.defer();

            if (isTypeActivity) {
                this.apiSvc.getByOdata(param, source).then((res) => {
                    def.resolve(res);
                }, (error) => {
                    def.reject(error);
                });
            } else {
                var url = source + '?top=' + param.$top + '&skip=' + param.$skip + '&userId=' + param.userId; 
                this.apiSvc.get(url).then((res) => {
                    def.resolve(res);
                }, (error) => {
                    def.reject(error);
                });
            }

            return def.promise;

        }

        /**
        * @description Get and process Notifications by Type
        */
        private getNotificationsByType = (type: NOTIFICATION_FEED_VIEWS, fromBus?: boolean) => {
            if ((this.pagingObjects[type].isBusy || this.pagingObjects[type].isAllLoaded)
                || (!this.viewModel[type].items.length && fromBus)) return;
            this.pagingObjects[type].isBusy = true;

            var isTypeActivity = type === NOTIFICATION_FEED_VIEWS.activity;

            var param = isTypeActivity ? this.getActivityPagingParams() : this.getMessagePagingParams();
            var source = isTypeActivity ? 'Notification/Notifications' : 'Notification/Messages';

            if (fromBus === true) {
                var item = this.viewModel[type].items[0];
                var latestItemDate = moment(item.CreateDate).toISOString();
                param.$skip = 0;
                param.$top = 20;
                param.$filter += ` and CreateDate gt DateTime'${latestItemDate}'`;
            }

            this.getNotifications(isTypeActivity, source, param).then((res) => {
                if (!res) return this.$log.log(type + ' response is null');
                
                if (res.length == 0 && !fromBus) {
                    if (this.viewModel[type].items.length <= 0) {
                        this.viewModel[type].isData = false;
                    }
                    this.pagingObjects[type].isAllLoaded = true;
                    return;
                }

                angular.forEach(res, (item: Interfaces.IMessageNotification, key) => {
                    
                    if (isTypeActivity) {
                        item._summaryPreview = item.Summary;

                        item._icon = Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE[item.Type]] == Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE.Other]
                            ? "alert2" : item.Type.toString().toLowerCase();

                        switch (item.Type.toString()) {
                            case "Report It": item._labelColor = _.kebabCase(Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE[item.Type.toString()
                                .replace(" ", "_")]]);
                                item._icon = "proactive";
                                break;

                            case "To-Do": item._labelColor = _.kebabCase(Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE[item.Type.toString()
                                .replace("-", "_")]]);
                                break;

                            default: item._labelColor = _.kebabCase(Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE[item.Type]]);
                                break;
                        }
                    } else {
                        if (!item._dept) {
                            item._dept = {};
                        }

                        //Custom display for long Messages from Message Builder
                        if (Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE[item.Type]] == Interfaces.NOTIFICATION_TYPE[Interfaces.NOTIFICATION_TYPE.Message]) {
                            var deptName = item.Department ? item.Department.Name : "OTHER"
                            if (!item.Department) {
                                item.Department = {
                                    DepartmentId: 0,
                                    Name: deptName
                                }
                            }
                            item.Summary = item.Detail;
                            item._summaryPreview = this.utilitySvc.removeHtmlFromString(item.Detail, true);
                            item._dept.senderName = deptName;

                            item._labelColor = _.kebabCase(deptName);

                            item._dept.profileImage = this.getDepartmentImage(item._labelColor);
                        }
                    }

                    //Adds the date above the first item of the day.
                    this.checkToAddDaySpacer(item, key, res);

                    //If item does not exist in viewModel
                    if (isTypeActivity) {
                        if (!_.any(this.viewModel[type].items, (mess: Interfaces.IMessageNotification) => (mess.NotificationId == item.NotificationId))) {
                            if (fromBus === true) {
                                this.viewModel[type].items.unshift(item);
                            } else {
                                this.viewModel[type].items.push(item);
                            }
                        }
                    } else {
                        if (!_.any(this.viewModel[type].items, (mess: Interfaces.IMessageNotification) => (mess.MessageId == item.MessageId))) {
                            if (fromBus === true) {
                                this.viewModel[type].items.unshift(item);
                            } else {
                                this.viewModel[type].items.push(item);
                            }
                        }
                    }
                });


                if (!fromBus) {
                    this.pagingObjects[type].toSkip += this.pagingObjects[type].top;
                } else {
                    this.calculateAndSendCounts(type);
                }

                if (isTypeActivity) {
                    //Set Activity Filter Items
                    this.viewModel.activity.filterItems = _.uniq(_.pluck(this.viewModel.activity.items, 'Type'));
                } else {
                    //Set Message Filter Items
                    this.viewModel.messages.filterItems = _.uniq(_.filter(_.map(this.viewModel.messages.items, 'Department'), (item) => (item)), 'DepartmentId')
                }

                this.pagingObjects[type].isBusy = false;
                
            }, this.handleError);
        }

        /**
        * @description Calculate counts for each Notification type and send through MessageBus
        */
        public calculateAndSendCounts = (type: NOTIFICATION_FEED_VIEWS, isSubtract?: boolean) => {
            if (!this.viewModel[type].items.length) {
                this.$log.log('No items were found');
                return;
            }
            var tabIndex = type === NOTIFICATION_FEED_VIEWS.activity ? 1 : 2;

            this.getTotalMessages();
        }

        private checkToAddDaySpacer = (item: Interfaces.IMessageNotification, indx:number, res:Array<Interfaces.IMessageNotification>) => {
            
                var prevItem = res[indx == 0 ? indx : indx - 1];
                var dateFormat = 'MM/DD/YYYY';
                var itemDate = moment(item.CreateDate).format(dateFormat);

                if ((itemDate == moment().format(dateFormat))) {
                    if (indx == 0) {
                        item._displayDate = 'TODAY\'S ' + this.currentView.name.toString().toUpperCase();
                    }
                    item._dateFormat = 'hh:mma';
                    item._isTodays = true;
                } else {
                    if (prevItem._isTodays) {
                        item._displayDate = 'PREVIOUS ' + this.currentView.name.toString().toUpperCase();
                    }
                    item._dateFormat = 'MM/dd/yy';
                }
                
        }

        private getTotalMessages = () => {
            async.parallel([
                () => {
                    // Activity notifications/messages turned off due to too many and Design not knowing what to do.
                    var actparam = {
                        $filter: `Type ne 'Message' and IsNew eq true`,
                        $select: 'NotificationId, IsNew'
                    };
                    this.apiSvc.getByOdata(actparam, 'Notification/Notifications').then((res) => {
                        this.tabs[1].count = res.length; //activities

                        this.serviceMessageBus.emitMessage(Common.Interfaces.MessageBusMessages.NotificationsActivityCount, this.tabs[1].count);
                    });
                },
                () => {
                    //get message 
                    var messagesParam = {
                        $filter: `Type eq 'Message' and MessageId ne null and IsNew eq true and OrgUserId eq ${this.userInfo.userId}`,
                        $select: 'NotificationId'
                    };
                    this.apiSvc.getByOdata(messagesParam, 'Notification', false).then((res) => {
                        this.tabs[2].count = res.length; // messages
                        this.serviceMessageBus.emitMessage(Common.Interfaces.MessageBusMessages.NotificationsMessagesCount, this.tabs[2].count);
                    });
                }],
                (callback) => {

                });
        }

        public switchCurrentView = (viewName: NOTIFICATION_FEED_VIEWS) => {
            this.currentView.name = viewName;
            this.currentView.icon = viewName == NOTIFICATION_FEED_VIEWS.activity ? NOTIFICATION_FEED_VIEWS_ICON.activity : NOTIFICATION_FEED_VIEWS_ICON.messages;
            this.currentView.isMessages = false;
            if (this.userInfo.mainRoles.managers || this.userInfo.mainRoles.customers) {
                // hide from Activity notification tab
                this.currentView.showBtns = (viewName != NOTIFICATION_FEED_VIEWS.activity);
                
            }
            if (viewName === NOTIFICATION_FEED_VIEWS.messages) {
                this.currentView.isMessages = true;

                if (this.viewModel.messages.items.length == 0) {
                    this.getNotificationsByType(NOTIFICATION_FEED_VIEWS.messages);
                }
            }
        }

        private getActivityPagingParams = () => {
            //Start getting more messages at a time.
            if (this.viewModel.activity.items.length && (this.viewModel.activity.items.length > (this.pagingObjects.activity.top * 3))) {
                this.pagingObjects.activity.top = 60;
            }
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: this.pagingObjects.activity.top,
                $skip: this.pagingObjects.activity.toSkip,
                $orderby: 'CreateDate desc',
                $filter: `Type ne 'Message'`
            };
            return params;
        }

        private getMessagePagingParams = () => {
            //Start getting more messages at a time.
            if (this.viewModel.messages.items.length && (this.viewModel.messages.items.length > (this.pagingObjects.messages.top * 3))) {
                this.pagingObjects.messages.top = 60;
            }

            if (!this.userInfo.mainRoles.managers && this.userInfo.mainRoles.employees) {
                this.radioMessagesModel = 0;
            }

            var filter = `Type eq 'Message'`;
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: this.pagingObjects.messages.top,
                $skip: this.pagingObjects.messages.toSkip,
                $orderby: 'CreateDate desc',  
                $filter: (this.radioMessagesModel == 0 ? `Type eq 'Message' and UserId eq ${this.userContext.UserId}` : filter),
                userId: (this.radioMessagesModel == 0 ? this.userContext.UserId : 0)
            };

            return params;
        }

        public disableTabsForInnerMessage = (isDisable: boolean) => {
            angular.forEach(this.tabs, (tab) => {
                tab.disabled = isDisable;
            });
        }

        private getDepartmentImage = (department) => {
            switch (department) {
                case 'benefits':
                    department = 'hr';
                    break;
                case 'recruiting':
                    department = 'hr';
                    break;

            }

            if (department == 'general') {
                return this.imageSvc.getByKey('public/image/user.jpg');
            } else {
                return this.imageSvc.getByKey('public/image/sbm-corporate-profiles/' + department + '.jpg');
            }
        }

        private handleError = (error) => {
            return this.$log.error('There was an error', error);
        }
    }

    angular.module('app').component('notificationFeed', {
        templateUrl: SiteSettings.basePath + 'Content/js/Components/NotificationFeed/Templates/notificationfeed.main.tmpl.html',
        controller: NotificationFeedCtrl
    });

    /**
     *
     * NOTIFCATION FEED VIEWS
     *
     */
    export interface NotifcationFeedViewItem extends Components.Interfaces.INotification {
        _dept?: {
            color: string;
            profileImage: string;
            senderName: string;
            senderTitle: string;
        },
        _isDeleted?: boolean;
        pendingDelete: any;
    }

    class NotificationFeedViews {
        parent: NotificationFeedCtrl; //bindings
        items: Array<NotifcationFeedViewItem>; //bindings

        templatePath = '';
        mainViewTemplatePath: string;
        useStrictFilter: boolean = false;
        currentInnerMessage;
        showBtns: boolean = false;
        markingIsBusy = false;

        static $inject = [
            '$scope',
            '$timeout',
            '$log',
            'sitesettings',
            'userInfo',
            'userContext',
            'chaitea.common.services.apibasesvc',
            Common.Services.NotificationSvc.id];
        constructor(
            private $scope: angular.IScope,
            private $timeout: angular.ITimeoutService,
            private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc, private notificationSvc: Common.Services.INotificationSvc) {
            this.templatePath = this.mainViewTemplatePath = this.sitesettings.relativeModulePaths.Components + 'NotificationFeed/Templates/notificationfeed.view.tmpl.html';
        }
        public $onInit = () => {
        }
        /**
        * Gets next set of data for current list
        */
        public viewMore = () => {
            this.parent.viewMore();
        }
        /**
        * Filters current list by label clicked
        */
        public labelClickFilter = (labelName) => {
            if (this.parent.viewModel[this.parent.currentView.name].filter != labelName) {
                this.parent.viewModel[this.parent.currentView.name].filter = labelName;
            }
        }
        /**
        * In Messages, moves the view to the "inner message" or details of the clicked message.
        */
        public viewMessage = (item: Components.Interfaces.INotification) => {
            if (this.parent.currentView.name == NOTIFICATION_FEED_VIEWS.messages) {
                this.currentInnerMessage = item;
                this.parent.disableTabsForInnerMessage(true);
                this.templatePath = this.sitesettings.relativeModulePaths.Components + 'NotificationFeed/Templates/notificationfeed.innermessage.tmpl.html';
                if (item.IsNew && item.NotificationId > 0) this.markNotificationItemIsNotNew(item);
                mixpanel.track('Notification item was clicked', { NotificationId: item.NotificationId });
            } else if (this.parent.currentView.name == NOTIFICATION_FEED_VIEWS.activity) {
                this.markNotificationItemIsNotNew(item);
                mixpanel.track('Notification item was clicked', { NotificationId: item.NotificationId });
            }
        }

        private markNotificationItemIsNotNew = (notificationItem: Components.Interfaces.INotification) => {
            if (notificationItem.NotificationId > 0) {
                notificationItem.IsNew = false;
                this.apiSvc.update(notificationItem.NotificationId, notificationItem, "Notification").then((res) => {
                    /// temp removal of navigating to notifications
                    //if (notificationItem.Url) {
                    //    this.goToUrl(notificationItem.Url);
                    //}

                    this.parent.calculateAndSendCounts(this.parent.currentView.name);
                }, (error) => {
                    notificationItem.IsNew = true;
                }).then((res) => {
                    this.markingIsBusy = false;
                });
            }
        }

        private goToUrl = (url) => {
            if (!url) return;
            //remove '/' from end to work with URLs from DB
            var base = (url[0] == '/') ? this.sitesettings.basePath.slice(0, -1) : this.sitesettings.basePath;
            window.location.href = base + url;
        }

        public backToMainView = () => {
            this.currentInnerMessage = null;
            this.parent.disableTabsForInnerMessage(false);
            this.templatePath = this.mainViewTemplatePath;
        }

        /**
        * @description Remove notification item. 5 second delay for user to "Undo" deletion.
        */
        public deleteItem = (itemIndex: number, item: NotifcationFeedViewItem) => {
            //Temporarily hide item
            item._isDeleted = true;
            var timeout = angular.copy(this.$timeout(() => {
                this.apiSvc.delete(item.NotificationId, 'Notification').then((res) => {
                    //Remove item from list
                    _.remove(this.items, { 'NotificationId': item.NotificationId });
                    this.parent.calculateAndSendCounts(this.parent.currentView.name);
                }, (err) => {
                    //Revert hide and notify user
                    var message = 'There was an error deleting the Notification: ';
                    item._isDeleted = false;
                    this.notificationSvc.errorToastMessage(`${message} ${item.Detail.substring(0, 50)} ...`);
                    this.$log.error(message + item.NotificationId, err);
                });
            }, 3500, true), item.pendingDelete);
            item.pendingDelete = timeout;
        }

        /**
        * @description Undo deletion of notification item
        */
        public undoDelete = (item: NotifcationFeedViewItem) => {
            if (item.pendingDelete) {
                this.$timeout.cancel(item.pendingDelete);
                item._isDeleted = false;
            }
        }

        /**
         * Used to switch filters on main ng-repeat in notificationfeed.view.tmpl.html
         * @param {NotificationFeedCtrl} parentCtrl
         * @returns string
         */
        public filterByActivityOrMessages = (parentCtrl: NotificationFeedCtrl): any => {
            if (parentCtrl.currentView.isMessages) {                  
                if (parentCtrl.viewModel.messages.filter === null || parentCtrl.viewModel.messages.filter === "") {
                    this.useStrictFilter = false;
                 } else {
                    this.useStrictFilter = true;
                }

                var messageFilter: any = {
                    Department: {
                        Name: parentCtrl.viewModel.messages.filter || ""
                    }
                };

                return messageFilter;
            } else if (!this.parent.currentView.isMessages) {
                return { Type: parentCtrl.viewModel.activity.filter || "" };
            } else {
                return null;
            }
        }
    }

    angular.module('app').component('notificationFeedView', {
        template: `
                <div class="notification-feed-views notification-content" ng-class="{'inner-message':$ctrl.currentInnerMessage}">
                    <div ng-include="$ctrl.templatePath" class="slide-right"></div>    
                </div>
                `,
        require: {
            parent: '^notificationFeed'
        },
        bindings: {
            items: '='
        },
        controller: NotificationFeedViews,
    });
}