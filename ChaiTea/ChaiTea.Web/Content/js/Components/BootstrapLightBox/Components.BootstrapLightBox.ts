﻿module ChaiTea.Components {

    class BootstrapLightboxController {
        static $inject = ['$scope', '$attrs'];
        constructor(
            private $scope: angular.IScope,
            private $attrs
        ) {
          
        }

        public $onInit = () => {
            angular.element(document).delegate('[data-toggle="lightbox"]', 'click', (event) => {
                event.preventDefault();
                var ele: any = angular.element(event.currentTarget);
                ele.ekkoLightbox();
            });
        }

    }

    angular.module('app').component('bootstrapLightbox', {
        bindings: {
            items: "=",
            galleryName: "@",
            canEditItems: "=",
            removeItem:"&",
            customLinkClass: "@",
            customImageClass: "@",
        },
        controller: BootstrapLightboxController,
        templateUrl: SiteSettings.basePath + 'Content/js/Components/BootstrapLightBox/Templates/bootstrapLightBox.tmpl.html'
    });

}