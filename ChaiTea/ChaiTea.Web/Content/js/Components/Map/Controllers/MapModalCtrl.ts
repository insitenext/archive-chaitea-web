﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Admin.Controllers {
    'use strict';

    class MapModalCtrl {

        basePath: string;
        modalTitle: string = 'Search Map';
        modalBodyTemplate: string = '';
        modalFooterTemplate: string = '';
        searchTmp: string = 'searchbox.tpl.html';
        latitude: number = 45;
        longitude: number = -73;
        mapInstance: any = null;
        map = {
            center: {
                latitude: this.latitude,
                longitude: this.longitude
            },
            zoom: 8,
            options: {
                styles: [],
            },
            searchbox: {
                parentdiv: 'search-container',
                template: 'searchbox.tpl.html',
                events: {}
            },
            markers: []
        };
        containerStyle = {
            opacity: 0.1
        };

        static $inject = [
            '$scope',
            '$uibModalInstance',
            'blockUI',
            'sitesettings',
            'mapOptions',
            'uiGmapIsReady'
        ];

        constructor(
            private $scope: angular.ui.bootstrap.IModalScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private blockUI,
            private sitesettings: ISiteSettings,
            private mapOptions: ChaiTea.Common.Interfaces.IMap,
            private uiGmapIsReady: any) {

            this.basePath = sitesettings.basePath;
            this.modalBodyTemplate = this.basePath + 'Content/js/Components/Map/Templates/map.modal.tmpl.html';

            this.blockUI.start();

            var $self = this;
            uiGmapIsReady.promise(1).then(function (instances) {
                instances.forEach(function (inst) {

                    $self.mapInstance = inst.map;

                    //apply setting overrides
                    $self.addMarker(0, mapOptions.center.latitude, mapOptions.center.longitude);

                    if (mapOptions.center) {
                        $self.map.center = mapOptions.center;
                        $self.latitude = mapOptions.center.latitude;
                        $self.longitude = mapOptions.center.longitude;
                    }
                    if (mapOptions.zoom) {
                        $self.map.zoom = mapOptions.zoom;
                    }

                    if (mapOptions.options && mapOptions.options.styles) {
                        $self.map.options.styles = mapOptions.options.styles;
                    }

                    $self.blockUI.stop();
                    $self.containerStyle.opacity = 1;

                });
            });

            

            //setup map events
            this.map.searchbox.events = {
                places_changed: this.onLocationChange
            }

        }

        /**
        * @description Dismiss the modal instance.
        */
        public close = (): void => {
            this.$uibModalInstance.close(this.map.markers[0].coords);
        };

        /**
        * @description Update Lat and Long on drag end.
        */
        public onLocationChange = (searchBox): void => {

            var place = searchBox.getPlaces();
            if (!place || place == 'undefined' || place.length == 0) {
                return;
            }

            //update location
            this.map.zoom = 16;
            this.map.center.latitude = place[0].geometry.location.lat();
            this.map.center.longitude = place[0].geometry.location.lng();

            //add marker
            this.updateMarker(0, this.map.center.latitude, this.map.center.longitude);
        };

        /**
        * @description Update marker location.
        */
        public onDragEnd = (marker, eventName, args): void => {

            this.updateMarker(marker.id, this.map.markers[marker.id].coords.latitude, this.map.markers[marker.id].coords.longitude)

        };

        /**
        * @description Builds marker obj and adds to map.
        */
        private addMarker = (id, latitude, longitude): void => {

            var marker: any = {
                id: id,
                coords: {
                    latitude: latitude,
                    longitude: longitude
                },
                options: {
                    draggable: true,
                    labelContent: "lat: " + latitude + ' ' + 'lon: ' + longitude,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                },
                events: {
                    dragend: this.onDragEnd
                }
            };

            this.map.markers.push(marker);
        };

        /**
        * @description Update marker obj.
        */
        private updateMarker = (id, lat, long): void => {

            this.map.markers[id].coords.latitude = lat;
            this.map.markers[id].coords.longitude = long;

            this.map.markers[id].options = {
                draggable: true,
                labelContent: "lat: " + lat + ' ' + 'lon: ' + long,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
            };
        };

    }

    angular.module('app').controller('components.map.mapmodalctrl', MapModalCtrl);

}