﻿/// <reference path="../../_libs.ts" />


module ChaiTea.Components {
    'use strict';

    class MapSearchModalCtrl {

        modalInstance;
        latitude: number;
        longitude: number;

        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            '$uibModal',
            'sitesettings',
            'chaitea.common.services.mapssvc'];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private sitesettings: ISiteSettings,
            private mapSvc: ChaiTea.Common.Services.IMapsSvc) {
        }

        private $onInit = () => {

        }

        public searchMap = () => {

            var styles: ChaiTea.Common.Interfaces.IMapStyles = this.mapSvc.getStyles();
            var mapOptions: ChaiTea.Common.Interfaces.IMap = {
                center: {
                    latitude: 40.7127837,
                    longitude: -74.00594130000002
                },
                zoom: 8,
                options: {
                    styles: styles.Apple
                }
            };

            //get location
            if (this.latitude && this.longitude) {
                mapOptions.center.latitude = this.latitude;
                mapOptions.center.longitude = this.longitude;
            } else {
                this.mapSvc.getGeoLocation().then((coords: ChaiTea.Common.Interfaces.IGeoLocation) => {

                    if (coords) {
                        mapOptions.center.latitude = coords.latitude;
                        mapOptions.center.longitude = coords.longitude;
                    }

                });
            }

            //load google maps modal
            this.modalInstance = this.$uibModal.open({
                templateUrl: this.sitesettings.basePath + 'Content/js/Common/Templates/modal.tmpl.html',
                controller: 'components.map.mapmodalctrl as vm',
                size: 'md',
                resolve: {
                    title: () => { return ''; },
                    mapOptions: () => { return mapOptions; },
                }
            });

            this.modalInstance.result.then((coords: any) => {

                this.latitude = coords.latitude;
                this.longitude = coords.longitude;

            }, () => { });

        }
       
    }

    angular.module('app').component('mapSearchModal', {
        controller: MapSearchModalCtrl,
        bindings: {
            buttonText: '@',
            latitude: '=',
            longitude: '=',
        },
        template: `<a href="javascript:void(0);" class="btn btn-xs btn-primary" ng-click="$ctrl.searchMap()">{{$ctrl.buttonText}}</a>`
    });

}