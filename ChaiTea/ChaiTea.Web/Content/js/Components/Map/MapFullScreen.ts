﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Components {
    'use strict';

    class MapFullScreenCtrl {

        modalInstance;
        markers;
        circles;
        style;
        refresh;
        zoom;
        locations;
        mapOptions;

        mapInstance = null;
        boundsSet: boolean = false;

        map: ChaiTea.Common.Interfaces.IMap = {
            center: {
                latitude: 45,
                longitude: -73
            },
            zoom: 4,
            options: {
                styles: [],
            }
        };

        maxRetries: number = 3;
        retry: number = 0;

        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            '$uibModal',
            '$timeout',
            'sitesettings',
            'uiGmapIsReady',
            'chaitea.common.services.mapssvc'];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: angular.IAttributes,
            private $log: angular.ILogService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $timeout: angular.ITimeoutService,
            private sitesettings: ISiteSettings,
            private uiGmapIsReady: any,
            private mapSvc: ChaiTea.Common.Services.IMapsSvc) {
        }

        private $onInit = () => {

            var $self = this;
            this.uiGmapIsReady.promise(1).then(function (instances) {
                instances.forEach(function (inst) {

                    $self.mapInstance = inst.map;
                    $self.updateBounds();

                });
            });

            //apply option overrides
            this.map.options.styles = (this.style ? this.style : this.mapSvc.getStyles().BlueWater);
            this.map.zoom = (this.zoom ? this.zoom : this.map.zoom);

            //add options
            if (this.mapOptions) {
                angular.extend(this.map.options, this.mapOptions);
            }
        }

        private updateBounds = () => {

            var $self = this;

            if (this.markers.length && this.mapInstance && !this.boundsSet) {

                //center map to markers
                var bounds = new google.maps.LatLngBounds();
                angular.forEach(this.markers, (marker) => {
                    var coord = new google.maps.LatLng(marker.coords.latitude, marker.coords.longitude);
                    bounds.extend(coord);
                });

                this.mapInstance.setCenter(bounds.getCenter());
                this.mapInstance.fitBounds(bounds);

                //add some padding
                var zoom = this.mapInstance.getZoom();
                this.map.zoom = (this.markers.length == 1 ? 6 : zoom);

                this.boundsSet = ((this.markers.length == this.locations.length) ? true : false);

            }
        }

        private $onChanges = (changesObj) => {
            if (changesObj.markers.currentValue.length &&
                (changesObj.markers.currentValue.length === this.locations.length) ||
                (changesObj.markers.currentValue.length === 5)) {
                this.updateBounds();
            }
        }

    }

    angular.module('app').component('mapFullScreen', {
        controller: MapFullScreenCtrl,
        bindings: {
            style: '<',
            zoom: '<',
            markers: '<',
            circles: '<',
            refresh: '<',
            locations: '<',
            mapOptions: '<'
        },
        template: `<div class="map-container">
                    <ui-gmap-google-map center='$ctrl.map.center' zoom='$ctrl.map.zoom' options="$ctrl.map.options" refresh="$ctrl.refresh">
                        <ui-gmap-marker ng-repeat="m in $ctrl.markers" coords="m.coords" icon="m.icon" idkey="m.id" options= "m.options" events="m.events">
                            <ui-gmap-window show="m.window.show" options="m.window.options" templateUrl="m.window.template" templateParameter="m.window" ></ui-gmap-window>
                        </ui-gmap-marker>
                        <ui-gmap-circle ng-repeat="c in $ctrl.circles track by c.id" center="c.center" stroke="c.stroke" fill="c.fill" radius="c.radius"
                            visible="c.visible" geodesic="c.geodesic" editable="c.editable" draggable="c.draggable" clickable="c.clickable" control="c.control">
                        </ui-gmap-circle
                    </ui-gmap-google-map>
                </div>`
    });

}