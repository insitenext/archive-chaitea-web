﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {

    'use strict';

    starRating.$inject = ['sitesettings'];

    function starRating(sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            restrict: 'EA',
            scope: {
                maxScore: "@",
                score: "=",
                forDisplay:"@"
            },
            link: link,
            controller: ['$scope',controllerFn],
            templateUrl: sitesettings.basePath + 'Content/js/Components/StarRating/Templates/starRating.html'
        };

        function controllerFn($scope) {
            $scope.isForDisplay = $scope.forDisplay == 'true' ? true : false;
            $scope.range = [];

            //set mac score to 5 if not supplied
            if (!$scope.maxScore)
                $scope.maxScore = 5;

            for (var i = 0; i < $scope.maxScore; i++) {
                $scope.range.push(i);
            }
            $scope.mark = function (index) {
                $scope.marked = $scope.marked == index ? index - 1 : index;
                $scope.score = $scope.marked + 1;
            };
            $scope.isMarked = function (index) {
                if (!$scope.isForDisplay) {
                    if (index <= $scope.marked) {
                        return 'fa-star';
                    }
                    else {
                        return 'fa-star-o';
                    }
                }
                else {
                    if ($scope.score >= index + 1) {
                        return 'fa-star';
                    }
                    else if ($scope.score > index && $scope.score < index + 1) {
                        return 'fa-star-half-empty';
                    }
                    else {
                        return 'fa-star-o';
                    }
                }
            };
        }
        function link($scope, element, attrs) {}

       
    }
    angular.module('app').directive('starRating', starRating);
}