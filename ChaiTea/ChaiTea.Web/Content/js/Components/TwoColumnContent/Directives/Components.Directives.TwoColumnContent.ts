﻿/// <reference path="../../../_libs.ts" />
module ChaiTea.Components.Directives {
    'use strict';
    TwoColumnContent.$inject = ['$log']
    function TwoColumnContent($log:angular.ILogService) {
        return <angular.IDirective>{
            restrict: 'E',
            transclude: true,
            replace: true,
            template: `<div class="two-column-content" ng-transclude></div>`
        }
    }

    TwoColumnHeader.$inject = ['$log']
    function TwoColumnHeader($log: angular.ILogService) {
        return <angular.IDirective>{
            restrict: 'E',
            require:'^twoColumnContent',
            transclude: true,
            replace:true,
            scope: {
                customClass:'@'
            },
            template: `<div class="{{customClass || 'bg-primary'}} table-head two-column-header"><ng-transclude></ng-transclude> <div class="clearfix"></div></div>`
        }
    }

    TwoColumnBody.$inject = ['$log']
    function TwoColumnBody($log: angular.ILogService) {
        return <angular.IDirective>{
            restrict: 'E',
            transclude: true,
            replace: true,
            require: '^twoColumnContent',
            template: `<div class="row well two-column-body" ng-transclude></div>`
        }
    }

    TwoColumnLeft.$inject = ['$log']
    function TwoColumnLeft($log: angular.ILogService) {
        return <angular.IDirective>{
            restrict: 'E',
            transclude: true,
            replace: true,
            require:'^twoColumnBody',
            template: `<div class="col-md-8 two-column-left" ng-transclude></div>`
        }
    }

    TwoColumnRight.$inject = ['$log']
    function TwoColumnRight($log: angular.ILogService) {
        return <angular.IDirective>{
            restrict: 'E',
            transclude: true,
            replace: true,
            require: '^twoColumnBody',
            template: `<div class="col-md-4 two-column-right side-block" ng-transclude></div>`
        }
    }

    angular.module('app').directive('twoColumnContent', TwoColumnContent)
        .directive('twoColumnHeader', TwoColumnHeader)
        .directive('twoColumnBody', TwoColumnBody)
        .directive('twoColumnLeft', TwoColumnLeft)
        .directive('twoColumnRight', TwoColumnRight);
}