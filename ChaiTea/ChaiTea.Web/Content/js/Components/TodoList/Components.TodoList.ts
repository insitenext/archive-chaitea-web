﻿
/// <reference path="../../_libs.ts" />
module ChaiTea.Components.Directives {
    'use strict';

    export interface ITodoNotification extends ChaiTea.Components.Interfaces.INotification {
        TodoItemId: number;
        isPassedDue: boolean;
        OrgUserId: number;
        CreateDate: Date;
        DueDate: Date;
        FloorName: string;
        FloorId: number;
        BuildingName: string;
        Comment: string;
        IsComplete: boolean;
        LastSeenDate: Date;
        CreateByName: string;
        CreateByAttachments: Array<Common.ApiServices.Interfaces.IUserAttachment>;
        _assignedByImagePath: string;
        UserAttachment: Array<any>;
        ProfileImg: string;
    }

    enum TODO_TYPE {
        todays,
        upcoming,
        completed
    }

    export enum TOGGLE_TYPE {
        my,
        team
    }


    class TodoListController {
        parent: NotificationFeedCtrl; //bindings
        externalCounts; //from bindings
        todoItemTemplate: string;
        todoInnerMessageTemplate: string;

        todos;
        categorizedTodos = {
            todays: [],
            upcoming: [],
            completed: []
        };

        todoCounts;
        pagingObjects = {
            isTodosBusy: false,
            isAllLoaded: false,
            allTodaysLoaded: false,
            topTodos: 20,
            todoRecordsToSkip: 0,
            allUpcomingLoaded: false,
            topUpcomingTodos: 20,
            todoUpcomingRecordsToSkip: 0,
            allCompletedLoaded: false,
            topCompletedTodos: 20,
            todoCompletedRecordsToSkip: 0
        };

        isCompletedTodoListShown = false;

        innerTodo: any;
        radioModel = TOGGLE_TYPE.my;
        showBtns: boolean = false;
        toggleType = TOGGLE_TYPE; 
        isOriginalText: boolean = false;
        activeToggleType = TOGGLE_TYPE.my;
        private basePath: string;
        loadingData: boolean = true;
        static $inject = [
            '$scope',
            '$element',
            '$attrs',
            '$log',
            '$window',
            '$q',
            'sitesettings',
            'userInfo',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            Common.Services.ImageSvc.id,
            Common.Services.NotificationSvc.id,
            'chaitea.common.services.awssvc',
            '$http',
            Common.Services.DateSvc.id
        ];
        constructor(
            private $scope: angular.IScope,
            private $element: angular.IAugmentedJQuery,
            private $attrs: any,
            private $log: angular.ILogService,
            private $window: angular.IWindowService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private imageSvc: Common.Services.IImageSvc,
            private notificationSvc: Common.Services.INotificationSvc,
            private awsSvc: Common.Services.IAwsSvc,
            private $http: angular.IHttpService,
            private dateSvc: ChaiTea.Common.Services.DateSvc) {
            this.basePath = sitesettings.basePath;
            this.todoItemTemplate = sitesettings.basePath + 'Content/js/Components/TodoList/Templates/todo-list.item.tmpl.html';
            this.todoInnerMessageTemplate = sitesettings.basePath + 'Content/js/Components/TodoList/Templates/todo-list.innertodo.tmpl.html';
        }

        public $onInit = () => {
            if (this.userInfo.mainRoles.managers || this.userInfo.mainRoles.customers) {
                this.showBtns = true;
            } else {
                this.radioModel = TOGGLE_TYPE.my;
            }
            this.$scope.$watch(() => { return this.todos }, (newVal, oldVal) => {
                if (newVal == oldVal) return;
                this.getLoggedInUsersOpenTodoCounts();
            });

            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.NotificationsTodo, this.$scope, (event, data) => {
                this.getTodos(true)
                    .then(() => (this.getTodos(true, TODO_TYPE.upcoming)));
                this.getLoggedInUsersOpenTodoCounts();
            });

            this.getTodos(false)
                .then(() => (this.getTodos(false, TODO_TYPE.upcoming)));
            this.getLoggedInUsersOpenTodoCounts();
        }

        private getLoggedInUsersOpenTodoCounts = () => {
            var params = {
                $select: 'TodoItemId',
                $filter: `IsComplete eq false`
            };
            this.apiSvc.getByOdata(params, 'TodoItem/ByEmployee').then((result) => {
                this.messageBusSvc.emitMessage(Common.Interfaces.MessageBusMessages.NotificationsTodoCount, result.length);
            });
        }

        public getTodos = (fromBus: boolean, typeToGet?: TODO_TYPE, toggleType?: TOGGLE_TYPE) => {
            var defer = this.$q.defer();
            if (this.pagingObjects.isTodosBusy) {
                defer.resolve();
            }
            this.pagingObjects.isTodosBusy = true;

            var todosObject: Array<ITodoNotification> = [];
            var param;

            var todoEndpoint: string = '';
            var scorecardEndpoint: string = '';

            switch (typeToGet) {
                case TODO_TYPE.upcoming:
                    todosObject = this.categorizedTodos.upcoming;
                    param = this.getUpcomingTodoPagingParams();
                    break;
                case TODO_TYPE.completed:
                    todosObject = this.categorizedTodos.completed;
                    param = this.getCompletedTodoPagingParams();
                    break;
                default:
                    todosObject = this.categorizedTodos.todays;
                    param = this.getTodaysTodoPagingParams();
            }

            switch (this.radioModel) {
                case TOGGLE_TYPE.my:
                    todoEndpoint = 'TodoItem/ByEmployee';
                    break;

                case TOGGLE_TYPE.team:
                    todoEndpoint = 'TodoItem';
                    break;

                default:
                    todoEndpoint = 'TodoItem/ByEmployee';
                    break;
            }
            if (fromBus === true) {
                param.$skip = 0;
                param.$top = 20;
                if (todosObject) {
                    var latestItemDate = _.takeRight(_.sortBy(todosObject, 'CreateDate'), 1)[0].CreateDate;
                    param.$filter += ` and CreateDate gt DateTime'${latestItemDate}'`;
                }
            }

            // TodoItems
            this.apiSvc.getByOdata(param, todoEndpoint).then((result): any => {
                this.loadingData = false;
                if ((result.length == 0) && !fromBus) {
                    switch (typeToGet) {
                        case TODO_TYPE.upcoming:
                            this.pagingObjects.allUpcomingLoaded = true;
                            break;
                        case TODO_TYPE.completed:
                            this.pagingObjects.allCompletedLoaded = true;
                            break;
                        default:
                            this.pagingObjects.allTodaysLoaded = true;
                    }
                    if (this.pagingObjects.allTodaysLoaded && this.pagingObjects.allUpcomingLoaded) {
                        this.pagingObjects.isAllLoaded = true;
                    }
                    defer.resolve();
                }

                var now = new Date();
                now.setHours(0, 0, 0, 0);

                if (!todosObject) todosObject = [];

                angular.forEach(result, (item: ITodoNotification) => {
                    item.isPassedDue = moment(item.DueDate).isBefore(now) && !item.IsComplete;
                    item.ProfileImg = (item.UserAttachment ? this.imageSvc.getUserImageFromAttachments(item.UserAttachment, true) : SiteSettings.defaultProfileImage);
                    todosObject.push(item);
                });
                todosObject.sort(function (a, b) {
                    return b.DueDate > a.DueDate ? -1 : b.DueDate < a.DueDate ? 1 : 0;
                });
                //Update Paging Objects
                if (!fromBus) {
                    switch (typeToGet) {
                        case TODO_TYPE.upcoming:
                            this.pagingObjects.todoUpcomingRecordsToSkip += this.pagingObjects.topUpcomingTodos;
                            break;
                        case TODO_TYPE.completed:
                            this.pagingObjects.todoCompletedRecordsToSkip += this.pagingObjects.topCompletedTodos;
                            break;
                        default:
                            this.pagingObjects.todoRecordsToSkip += this.pagingObjects.topTodos;
                    }
                }

                this.pagingObjects.isTodosBusy = false;

                defer.resolve();
            }, this.handleFailure);
            return defer.promise;
        }

        public toggleMineTeam = (toggleType: any) => {
            if (this.activeToggleType !== toggleType) {
                this.loadingData = true;
                this.activeToggleType = toggleType;
                this.radioModel = toggleType;
                this.categorizedTodos.todays = [];
                this.categorizedTodos.upcoming = [];
                this.categorizedTodos.completed = [];
                this.pagingObjects = {
                    isTodosBusy: false,
                    isAllLoaded: false,
                    allTodaysLoaded: false,
                    topTodos: 20,
                    todoRecordsToSkip: 0,
                    allUpcomingLoaded: false,
                    topUpcomingTodos: 20,
                    todoUpcomingRecordsToSkip: 0,
                    allCompletedLoaded: false,
                    topCompletedTodos: 20,
                    todoCompletedRecordsToSkip: 0
                }
                this.isCompletedTodoListShown = false;
                this.getTodos(false, TODO_TYPE.todays, this.radioModel).then((res) => {
                    this.getTodos(false, TODO_TYPE.upcoming, this.radioModel).then((res) => { }, this.handleFailure);
                }, this.handleFailure);
            }
        }

        public getMoreUpcomingTodos = () => {
            this.getTodos(false, TODO_TYPE.upcoming, this.radioModel).then((res) => { }, this.handleFailure);
        }

        public getMoreCompletedTodos = () => {
            if (!this.isCompletedTodoListShown) this.isCompletedTodoListShown = true;
            if (this.categorizedTodos.completed.length && this.pagingObjects.allCompletedLoaded) return;
            this.getTodos(false, TODO_TYPE.completed, this.radioModel).then((res) => { }, this.handleFailure);
        }

        public showInnerTodo = (todo: ITodoNotification) => {
            this.parent.disableTabsForInnerMessage(true);
            todo._assignedByImagePath = this.imageSvc.getUserImageFromAttachments(todo.CreateByAttachments, true);
            this.innerTodo = todo;
            this.markHasBeenSeen(todo);
        }

        public hideInnerTodo = () => {
            this.parent.disableTabsForInnerMessage(false);
            this.innerTodo = undefined;
        }

        public viewOriginalTodo = () => {                        
            var url = this.basePath + 'api/Services/todoItem';
            var todoItemId = this.innerTodo.TodoItemId;            
            var languageId = '';
            if (!this.isOriginalText) {
                languageId = this.innerTodo.SourceLanguageId;
            }
            this.$http({
                method: 'GET', url: url + '?id=' + todoItemId,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'LanguageId': languageId
                }
            }).then((response) =>  {      
                var data: any = response.data;          
                this.innerTodo.Comment = data.Comment;
                if (this.isOriginalText) {
                    this.isOriginalText = false;
                } else {
                    this.isOriginalText = true;
                }
            });                                   
        }

        public markHasBeenSeen = (todo: ITodoNotification) => {
            if (todo.OrgUserId == this.userInfo.userId) {
                var endpointToUse = this.getEndpointToUse(todo);

                this.apiSvc.save({ id: todo.TodoItemId }, endpointToUse + '/MarkTodoItemAsRead')
                    .then(res => {
                        todo.IsNew = false;
                        todo.LastSeenDate = moment().toDate();
                    }, this.handleFailure);
            }
        }

        public markTodoAsCompleted = () => {
            if (!this.innerTodo) return;
            var todo: ITodoNotification = this.innerTodo;

            var endpointToUse = this.getEndpointToUse(todo);
            if (this.radioModel == TOGGLE_TYPE.my && this.userInfo.activeRoles.indexOf('managers') < 0) {
                endpointToUse = endpointToUse + '/PutByEmployee';
            }
            
            todo.IsComplete = true;

            this.apiSvc.update(todo.TodoItemId, todo, endpointToUse)
                .then(res => {
                    this.notificationSvc.successToastMessage('To-Do marked as completed.');
                    this.innerTodo.UpdateDate = res.UpdateDate;
                    this.getLoggedInUsersOpenTodoCounts();
                    var index = _.findIndex(this.categorizedTodos.todays, function (todays) {
                        return todays.TodoItemId === res.TodoItemId;
                    });
                    if (index != -1) {
                        this.categorizedTodos.todays[index].UpdateDate = res.UpdateDate;
                        this.categorizedTodos.todays[index].IsComplete = true;
                    }
                    index = _.findIndex(this.categorizedTodos.upcoming, function (upcoming) {
                        return upcoming.TodoItemId === res.TodoItemId;
                    });
                    if (index != -1) {
                        this.categorizedTodos.upcoming[index].UpdateDate = res.UpdateDate;
                        this.categorizedTodos.upcoming[index].IsComplete = true;
                    }
                    this.hideInnerTodo();
                }).catch(err => {
                    todo.IsComplete = false;
                    this.notificationSvc.errorToastMessage("There was an issue completing your To-Do.");
                });
        }

        public refreshTodaysList = () => {
            this.categorizedTodos.todays = [];
            this.pagingObjects.todoRecordsToSkip = 0;
            this.getTodos(false)
                .then(() => (this.getTodos(false, TODO_TYPE.upcoming)));
            this.getLoggedInUsersOpenTodoCounts();
        }

        private checkToAddDaySpacer = () => {
            angular.forEach(this.todos, (item, key) => {
                var prevItem = this.todos[key == 0 ? key : key - 1];
                var dateFormat = 'MM/DD/YYYY';
                var itemDate = moment(item.DueDate).format(dateFormat);

                if (key == 0 && (itemDate == moment().format(dateFormat))) {
                    item._displayDate = 'TODAY';
                } else if (itemDate !== moment(prevItem.DueDate).format(dateFormat)) {
                    item._displayDate = itemDate;
                }
            });
        }

        private getTodaysTodoPagingParams = () => {
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: this.pagingObjects.topTodos,
                $skip: this.pagingObjects.todoRecordsToSkip,
                $orderby: 'DueDate',
                $filter: `IsComplete eq false and DueDate le DateTime'${this.dateSvc.setFilterDatesToSitesDatetimeOffset(moment().endOf('day').toISOString())}'`
            };
            if (this.radioModel == TOGGLE_TYPE.my) {
                params['$filter'] += ` and OrgUserId eq ${this.userInfo.userId} `;
            }
            return params;
        }

        private getUpcomingTodoPagingParams = () => {
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: this.pagingObjects.topUpcomingTodos,
                $skip: this.pagingObjects.todoUpcomingRecordsToSkip,
                $orderby: 'DueDate',
                $filter: `IsComplete eq false and DueDate gt DateTime'${this.dateSvc.setFilterDatesToSitesDatetimeOffset(moment().endOf('day').toISOString())}'`
            };
            if (this.radioModel == TOGGLE_TYPE.my) {
                params['$filter'] += ` and OrgUserId eq ${this.userInfo.userId} `;
            }
            return params;
        }

        private getCompletedTodoPagingParams = () => {
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: this.pagingObjects.topCompletedTodos,
                $skip: this.pagingObjects.todoCompletedRecordsToSkip,
                $orderby: 'DueDate desc',
                $filter: `IsComplete eq true`
            };
            if (this.radioModel == TOGGLE_TYPE.my) {
                params['$filter'] += ` and OrgUserId eq ${this.userInfo.userId} `;
            }
            return params;
        }

        private getEndpointToUse = (todo: ITodoNotification): string => {
            return Common.ApiServices.Interfaces.ApiEndpoints[Common.ApiServices.Interfaces.ApiEndpoints.ToDoItem];
        }

        private handleFailure = (err) => {
            return this.$log.error(err);
        }

    }

    angular.module('app').component('todoList', {
        templateUrl: SiteSettings.basePath + 'Content/js/Components/TodoList/Templates/todo-list.tmpl.html',
        require: {
            parent: '^notificationFeed'
        },
        bindings: {
            externalCounts: '='
        },
        controller: TodoListController
    });
}