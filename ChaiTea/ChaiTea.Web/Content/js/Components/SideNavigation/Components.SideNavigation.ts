﻿module ChaiTea.Components {
    class SideNavigationController implements angular.IComponentController {
        isMobile: boolean = false;
        basePath: string;
        mainNavItems = [];
        SubMenuItems = {
            settings: []
        };

        bottomNavItems = [];
        static $inject = ['$log', '$scope', 'userInfo', 'userContext', 'sitesettings', '$translate', 'navigationconfig'];
        constructor(
            private $log: angular.ILogService,
            private $scope: angular.IScope,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private sitesettings: ISiteSettings,
            private $translate: angular.translate.ITranslateService,
            private navigationConfig: any) {
            this.basePath = sitesettings.basePath;
            this.isMobile = !sitesettings.windowSizes.isLargeDesktop;
        }

        $onInit = () => {
            var isManager = this.userInfo.mainRoles.managers;
            var isEmployee = this.userInfo.mainRoles.employees;
            var isCustomer = this.userInfo.mainRoles.customers;
            var isTrainingAdmin = this.userInfo.mainRoles.trainingadmins;
            var isHrAdmin = this.userInfo.mainRoles.hradmins;
            var isSafety = this.userInfo.mainRoles.claimsmanagers || this.userInfo.mainRoles.ehsmanagers;
            var isOrgStructure = this.userInfo.mainRoles.orgstructureuser;
            var isFinancialViewer = isManager || this.userInfo.mainRoles.timeclockviewers || this.userInfo.mainRoles.suppliesviewers || this.userInfo.mainRoles.billingviewers;

            this.SubMenuItems.settings = [

            ]

            this.mainNavItems = [
                {
                    Name: 'Home',
                    IconName: 'fa-home2',
                    Url: this.getWebLink('/dashboard'),
                    ChildrenItems: [],
                    Visible: !isEmployee
                },
                {
                    Name: 'My Profile',
                    IconName: 'fa-profile',
                    Url: 'Personnel/MyProfile/Index',
                    ChildrenItems: [],
                    Visible: isEmployee
                },
                {
                    Name: 'Quality',
                    IconName: 'fa-quality',
                    Url: '', //this.basePath + 'Quality/Audit',
                    isCollapsed: true,
                    isMobileCollapsed: true,
                    Visible: !isEmployee,
                    ChildrenItems: [
                        {
                            Name: 'Dashboard',
                            Url: this.getWebLink('/quality/dashboard'),
                            Visible: true
                        },
                        {
                            Name: 'Audits',
                            Url: this.getWebLink('/quality/audit'),
                            Visible: true
                        },
                        {
                            Name: 'Complaints',
                            Url: this.getWebLink('/quality/complaint'),
                            Visible: true
                        },
                        {
                            Name: 'Compliments',
                            Url: this.getWebLink('/quality/compliment'),
                            Visible: true
                        },
                        {
                            Name: 'Surveys',
                            Url: this.getWebLink('/quality/surveys'),
                            Visible: true
                        },
                        {
                            Name: 'TO-DOs',
                            Url: this.getWebLink('/quality/to-do'),
                            Visible: true
                        },
                        {
                            Name: 'Work Order',
                            Url: this.getWebLink('/quality/work-order'),
                            Visible: true
                        },
                        {
                            Name: 'Report It',
                            Url: this.getWebLink('/quality/report-it'),
                            Visible: true
                        },
                        {
                            Name: 'Attendance',
                            Url: this.getWebLink('/quality/attendance'),
                            Visible: !isCustomer
                        },
                        {
                            Name: 'Conduct',
                            Url: this.getWebLink('/quality/conduct'),
                            Visible: !isCustomer
                        },
                        {
                            Name: 'Professionalism',
                            Url: this.getWebLink('/quality/professionalism'),
                            Visible: true
                        }
                    ]
                },
                {
                    Name: 'Personnel',
                    IconName: 'fa-personnel',
                    Url: '',//this.basePath + 'Personnel/BoxScore',
                    isCollapsed: true,
                    isMobileCollapsed: true,
                    ChildrenItems: [
                        {
                            Name: 'Team Stats',
                            Url: this.getWebLink('/personnel/team-stats'),
                            Visible: true
                        },
                        {
                            Name: 'SEP Scorecard',
                            Url: this.getWebLink('/personnel/sep'),
                            Visible: true
                        },
                        {
                            Name: 'Position Profiles',
                            Url: this.getWebLink('/personnel/position-profile'),
                            Visible: true
                        },
                        {
                            Name: 'Turnover',
                            Url: this.getWebLink('/personnel/turnover'),
                            Visible: true
                        },
                        /*{
+                            Name: 'Safety',
+                            Url: this.basePath + 'Personnel/Safety',
+                            Visible: true
+                        },*/
                    ],
                    Visible: !isEmployee
                },
                {
                    Name: 'Financials',
                    IconName: 'fa-financials',
                    Url: '',
                    isCollapsed: true,
                    isMobileCollapsed: true,
                    Visible: isFinancialViewer,
                    ChildrenItems: [
                        {
                            Name: 'Dashboard',
                            Url: this.getWebLink('/financials/dashboard'),
                            Visible: true
                        },
                        {
                            Name: 'Billing',
                            Url: this.getWebLink('/financials/billing-overview'),
                            Visible: this.userInfo.mainRoles.billingviewers
                        },
                        {
                            Name: 'Supplies',
                            Url: this.getWebLink('/financials/supplies'),
                            Visible: this.userInfo.mainRoles.suppliesviewers
                        },
                        {
                            Name: 'Hours',
                            Url: this.getWebLink('/financials/time-clock'),
                            Visible: isManager || this.userInfo.mainRoles.timeclockviewers
                        },
                        {
                            Name: 'Invoices',
                            Url: this.getWebLink('/financials/invoices'),
                            Visible: this.userInfo.mainRoles.billingviewers
                        },
                        {
                            Name: 'Past Due',
                            Url: this.getWebLink('/financials/past-due'),
                            Visible: this.userInfo.mainRoles.billingviewers
                        },
                    ],
                },
                {
                    Name: 'Knowledge Center',
                    IconName: 'fa-training',
                    Url: this.getWebLink('/training/knowledge-center'),
                    ChildrenItems: [],
                    Visible: true
                },
                {
                    Name: 'Create Report It',
                    IconName: 'fa-add-work-order',
                    Url: this.basePath + 'Personnel/Ownership/Entry',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && isEmployee
                },
                {
                    Name: 'Governance',
                    IconName: 'fa-governance',
                    Url: '',
                    isCollapsed: true,
                    isMobileCollapsed: true,
                    ChildrenItems: [
                        {
                            Name: 'Org Structure',
                            Url: this.getWebLink('/governance/org-structure'),
                            Visible: isOrgStructure
                        },
                        {
                            Name: 'Maps',
                            Url: this.getWebLink('/governance/map'),
                            Visible: true
                        },
                    ],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isEmployee || isManager || isCustomer || isTrainingAdmin)
                },
                {
                    Name: 'Safety',
                    IconName: 'fa-safety',
                    Url: '',
                    isCollapsed: true,
                    isMobileCollapsed: true,
                    ChildrenItems: [
                        {
                            Name: 'Trends',
                            Url: this.getWebLink('/safety/recordable-rates'),
                            Visible: true
                        },
                        {
                            Name: 'TIR',
                            Url: this.getWebLink('/safety/incident-rates'),
                            Visible: true
                        },
                        {
                            Name: 'Incidents',
                            Url: this.getWebLink('/safety/injury-report/details'),
                            Visible: true
                        },
                        {
                            Name: 'Historical',
                            Url: this.basePath + 'Personnel/Safety/Details',
                            Visible: true
                        }
                    ],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isSafety)
                },

                // Not sure where this one went
                //{
                //    Name: 'Contact',
                //    IconName: 'fa-phone',
                //    Url: this.basePath + 'Contact/ContactPage',
                //    ChildrenItems: [],
                //    Visible: !this.sitesettings.isTriPartyHackUser //true
                //}
            ]

            this.bottomNavItems = [
                {
                    Name: 'Site Settings',
                    IconName: 'fa-cog2',
                    Url: '',
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isTrainingAdmin || isHrAdmin),
                    isCollapsed: true,
                    ChildrenItems: [
                        {
                            Name: 'SEP Bonus Config',
                            Url: this.getWebLink('/personnel/bonus-config'),
                            Visible: isManager || isTrainingAdmin
                        },
                        {
                            Name: 'Course Admin',
                            Url: this.basePath + 'Training/Course/Index',
                            Visible: isTrainingAdmin
                        },
                        {
                            Name: 'Position Profile Builder',
                            Url: this.getWebLink('/personnel/settings/position-profile-builder'),
                            Visible: isTrainingAdmin
                        },
                        {
                            Name: 'Position Purpose Builder',
                            Url: this.getWebLink('/personnel/settings/position-purpose-builder'),
                            Visible: isTrainingAdmin || isHrAdmin
                        },
                        {
                            Name: 'KPI Builder',
                            Url: this.getWebLink('/settings/kpi-builder/create'),
                            Visible: true
                        },
                    ]
                },
                {
                    Name: 'Messaging',
                    IconName: 'fa-messaging2',
                    Url: '',
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isTrainingAdmin || isHrAdmin || isCustomer),
                    isCollapsed: true,
                    ChildrenItems: [
                        {
                            Name: 'Message Builder',
                            Url: this.getWebLink('/settings/message-builder'),
                            Visible: true
                        },
                        {
                            Name: 'Messages Sent',
                            Url: this.getWebLink('/settings/messages-sent'),
                            Visible: isManager || isCustomer
                        },
                    ]
                },
                {
                    Name: 'Help Center',
                    IconName: 'fa-help-center',
                    Url: 'https://help.sbminsite.com',
                    Visible: !this.sitesettings.isTriPartyHackUser(), //true
                    isCollapsed: true
                },
                {
                    Name: 'Shop',
                    IconName: 'fa-shop',
                    Url: 'http://b2b.sbmstore.com/system/login.php',
                    ChildrenItems: [],
                    Visible: !this.sitesettings.isTriPartyHackUser() && (isManager || isTrainingAdmin),
                    isCollapsed: true
                },
            ]

            this.translateMenuItemNames(this.mainNavItems);
            this.translateMenuItemNames(this.bottomNavItems);
        }

        private getCookie = (name) => {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) {
                return parts.pop().split(";").shift();
            }
            return null;
        }

        private getWebLink = (stateName: string) => {

            var url: string = '';
            // get base domain
            url += 'https://' + this.navigationConfig.WebDomain;
            // add page that will handle and auth and redirect
            url += '/' + this.navigationConfig.WebDomainRedirector;
            // add jwt token
            url += '?token=' + this.getCookie('r2sbminsite');

            if (stateName === '/safety/injury-report/details') {
                if (this.userInfo.mainRoles.claimsmanagers) {
                    stateName = '/safety/claims-investigate/details';
                } else if (this.userInfo.mainRoles.ehsmanagers) {
                    stateName = '/safety/injury-investigate/details';
                }
            }

            // add state to redirect too
            url += '&redirect=' + encodeURIComponent(stateName);

            return url;

        }

        public menuItemClick = ($event, $index) => {
            _.forEach(this.mainNavItems, (val, key) => {
                if (key == $index) {
                    val.isCollapsed = !val.isCollapsed;
                    val.isMobileCollapsed = !val.isMobileCollapsed;
                } else {
                    val.isCollapsed = true;
                    val.isMobileCollapsed = true;
                }
            });
        }

        public bottomMenuItemClick = ($event, $index) => {
            _.forEach(this.bottomNavItems, (val, key) => {
                if (key == $index) {
                    val.isCollapsed = !val.isCollapsed;
                    val.isActive = !val.isActive;
                } else {
                    val.isCollapsed = true;
                    val.isActive = false;
                }
            });
        }

        private translateMenuItemNames = (menuItems) => {
            angular.forEach(menuItems, (menuItem) => {
                this.$translate(menuItem.Name).then((translation) => {
                    menuItem.Name = translation;
                });
                menuItem.isSubLinkActive = _.any(menuItem.ChildrenItems, (item: any) => (_.contains(window.location.pathname, item.Url)));
                menuItem.isActive = window.location.pathname == menuItem.Url || menuItem.isSubLinkActive;
                menuItem.isCollapsed = !menuItem.isActive;
                angular.forEach(menuItem.ChildrenItems, (childMenuItem) => {
                    childMenuItem.isActive = _.contains(window.location.pathname, childMenuItem.Url);
                    this.$translate(childMenuItem.Name).then((translation) => {
                        childMenuItem.Name = translation;
                    });
                });
            });
        }
    }

    angular.module('app').component('sideNavigation', {
        templateUrl: SiteSettings.basePath + 'Content/js/Components/SideNavigation/Templates/sideNavigation.main.tmpl.html',
        controller: SideNavigationController
    });
}