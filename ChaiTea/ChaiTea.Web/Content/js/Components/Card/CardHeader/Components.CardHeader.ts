﻿module ChaiTea.Components.Card {

    class CardHeaderController {
        static $inject = ['$scope'];
        constructor(
            private $scope: angular.IScope
        ) {

        }

        public $onInit = () => {
           
        }

    }

    angular.module('app').component('cardHeader', {
        transclude: true,
        controller: CardHeaderController,
        //templateUrl: SiteSettings.basePath + 'Content/js/Components/CardHeader/Templates/CardHeader.tmpl.html'
    });

}