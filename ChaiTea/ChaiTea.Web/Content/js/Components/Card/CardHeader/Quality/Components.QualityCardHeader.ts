﻿module ChaiTea.Components.Card.Quality {

    class QualityCardHeaderController {
        static $inject = ['$scope', 'userContext'];
        constructor(
            private $scope: angular.IScope,
            private userContext: IUserContext
        ) {

        }

        public $onInit = () => {

        }

    }

    angular.module('app').component('qualityCardHeader', {
        bindings: {
            module: '=',
            headerDate: '=',
            headerText: '=',
            viewLinkFn: '&',
            itemId: '=',
            formatDate: '=',
            highLight: '=',
            daysLeft: '=',
            daysTag: '=',
            redirectionLink: '=',
            formatDatetimeOffset: '=',
            customFormatDate: '='
        },
        controller: QualityCardHeaderController,
        templateUrl: SiteSettings.basePath + 'Content/js/Components/Card/CardHeader/Quality/QualityCardHeader.tmpl.html'
    });

}