﻿module ChaiTea.Components.Card {

    class CardBodyController {
        static $inject = ['$scope'];
        constructor(
            private $scope: angular.IScope
        ) {

        }

        public $onInit = () => {
            
        }

    }

    angular.module('app').component('cardBody', {
        transclude: true,
        controller: CardBodyController,
        //templateUrl: SiteSettings.basePath + 'Content/js/Components/CardBody/Templates/CardBody.tmpl.html'
    });

}