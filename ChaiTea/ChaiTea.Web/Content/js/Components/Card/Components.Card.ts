﻿module ChaiTea.Components {

    class CardController {
        static $inject = ['$scope'];
        constructor(
            private $scope: angular.IScope
        ) {
          
        }

        public $onInit = () => {
        }

    }

    angular.module('app').component('card', {
        transclude: true,
        controller: CardController,
        //templateUrl: SiteSettings.basePath + 'Content/js/Components/Card/Templates/Card.tmpl.html'
    });

}