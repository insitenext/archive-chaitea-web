﻿module ChaiTea.Components.Card {

    class CardFooterController {
        static $inject = ['$scope'];
        constructor(
            private $scope: angular.IScope
        ) {

        }

        public $onInit = () => {

        }

    }

    angular.module('app').component('cardFooter', {
        transclude: true,
        controller: CardFooterController,
        //templateUrl: SiteSettings.basePath + 'Content/js/Components/CardFooter/Templates/CardFooter.tmpl.html'
    });

}