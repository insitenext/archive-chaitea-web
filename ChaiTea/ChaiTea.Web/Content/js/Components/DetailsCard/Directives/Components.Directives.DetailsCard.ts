﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {

    'use strict';

    detailsCard.$inject = ['sitesettings'];

    function detailsCard(sitesettings: ISiteSettings): angular.IDirective {

        return <ng.IDirective>{
            transclude: true,
            restrict: 'EA',
            require: '^old',
            scope: {
                module: '=',
                headerDate: '=',
                viewLink: '=',
                itemId: '=',
                isDropDown: '='
            },
            link: link,
            templateUrl: sitesettings.basePath + 'Content/js/Components/DetailsCard/Templates/detailsCard.html'
        };
        function link($scope, element, attrs) {}

    }
    angular.module('app').directive('detailsCard', detailsCard);
}