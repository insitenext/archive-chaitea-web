﻿ module ChaiTea.Components.Interfaces {
     'use strict';
     
     export interface INotificationList {
         notifcations: Array<INotification>;
     }

     export enum NOTIFICATION_TYPE {
         Other = 1,
         Audit = 2,
         Complaint = 3,
         Compliment = 4,
         Safety = 5,
         To_Do = 6,
         Message = 7,
         Conduct = 8,
         Attendance = 9,
         Professionalism = 10,
         Training = 11,
         Report_It = 12,
         WorkOrder = 13
     }

     export enum NOTIFICATION_EVENT {
         Other = 1,
         Created = 2,
         Closed = 3,
         Failed = 4
     }

     export interface INotification {
         _icon: string;

         CreateById: number;
         //CreateByName: string;
         CreateDate: Date;
         Detail: string;
         Department: { DepartmentId: number; Name: string; };
         Event: NOTIFICATION_EVENT;
         IsNew: boolean;
         NotificationId: number;
         OrgUserId: number;
         Summary: string;
         Type: NOTIFICATION_TYPE;
         Url: string;
         MessageId?: number;
     }

     export interface IMessageNotification extends INotification {
         _labelColor?: string;
         _displayDate: string;
         _isTodays?: boolean;
         _summaryPreview?: string;
         _dateFormat?: string;
         _dept?: {
             profileImage?: string;
             senderName?: string;
             senderTitle?: string;
         }
     }


 }