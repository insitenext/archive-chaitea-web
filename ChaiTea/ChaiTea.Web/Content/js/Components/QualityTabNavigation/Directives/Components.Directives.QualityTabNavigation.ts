﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Components.Directives {
    'use strict';

    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import commonSvc = ChaiTea.Common.Services;
    const currentEmployeeIdQueryParam = Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString();

    qualityTabNavigation.$inject = [
        'sitesettings',
        'userContext',
        'userInfo',
        '$timeout',
        '$translate',
        Common.Services.UtilitySvc.id
    ];
    
    function qualityTabNavigation(
        sitesettings: ISiteSettings,
        userContext: IUserContext,
        userInfo: IUserInfo,
        $timeout: angular.ITimeoutService,
        $translate: angular.translate.ITranslateService,
        utilitySvc: Common.Services.IUtilitySvc
    ): angular.IDirective {
        return <ng.IDirective>{
            restrict: 'E',
            scope: {
                active: '@',
                iconSubNavigation: '@'                
            },
            controller: ['$scope', '$translate', qualityTabNavigationCtrl],
            templateUrl: sitesettings.basePath + 'Content/js/Components/QualityTabNavigation/Templates/qualityTabNavigation.html',
            link: link
        };


        var $currentSlideSizeRight
        var $currentSlideSizeLeft;
        var $windowWidth = $('#main').width();
        var $titleWidth = $('#title-width').width();
        var tempSize;

        function link($scope) {
            
                $timeout(onLoad, 100);
            
            var $moveTo;
            function onLoad() {
                $scope.slidingPanelWidth = sitesettings.windowSizes.isTablet ? 125 : 105;
                $scope.slidingPanelWidth = $scope.tabWidth * $scope.list.length;
                var $slidingPanelWidth = $scope.slidingPanelWidth;
                tempSize = $slidingPanelWidth;
                    var activeTab = $scope.active;
                    activeTab = '#' + activeTab;
                    $titleWidth = $('#title-width').width();
                    var $activeTabPos = $(activeTab).position();
                    $windowWidth = $('#main').width();
                    var $wrappingWidth = $('#wrapping-div').position();
                    if (($windowWidth - $titleWidth  - 40) < $slidingPanelWidth + 40 ) {
                        $moveTo = $activeTabPos.left - $wrappingWidth.left;

                        if (($slidingPanelWidth - $moveTo) < ($windowWidth - $wrappingWidth.left)) {
                            $('#right-arrow').hide();
                        }

                        if ($moveTo != 0) {
                            if (($windowWidth - $titleWidth) < $slidingPanelWidth) {
                                $('#sliding-panel').animate({
                                    marginLeft: "-=" + $moveTo + "px"
                                }, 1000);
                                $('#left-arrow').show();
                            }
                            else {
                                $('#left-arrow').hide();
                            }
                        }
                        
                        $currentSlideSizeRight = $moveTo + ($windowWidth - $wrappingWidth.left);
                        $currentSlideSizeLeft = $moveTo;
                        if ($moveTo <= 0) {
                            $('#left-arrow').hide();
                        }
                    }
                    else {
                        $currentSlideSizeLeft = 0;
                        $currentSlideSizeRight = 0;
                        var $testW = $windowWidth - $wrappingWidth.left;
                        var $moveAgain = $testW - $slidingPanelWidth;

                        $('#sliding-panel').animate({
                            marginLeft: "0"
                        }, 0)
                        $('#wrapping-div').addClass('pull-right');
                        $('#left-arrow').hide();
                        $('#right-arrow').hide();
                    }

                               
            }

            var $windowWidth;
            var afterResize;

            $(window).resize(function () {

                var $ww = $(window).width();
                $scope.tabWidth = $ww >= 768 ? 125 : 105;
                $scope.slidingPanelWidth = $scope.tabWidth * $scope.list.length;
                var $slidingPanelWidth = $scope.slidingPanelWidth;

                $scope.$apply();

                clearTimeout(afterResize);
                afterResize = setTimeout(function () {

                    $windowWidth = $('#main').width();
                    var $wrappingWidth = $('#wrapping-div').position();
                    var $newWindowWidth = $('#main').width();
                    $titleWidth = $('#title-width').width();

                    if (($newWindowWidth - $titleWidth - 40) < $slidingPanelWidth + 40) {
                        
                        $('#wrapping-div').removeClass('pull-right');

                        var activeTab = $scope.active;
                        activeTab = '#' + activeTab;
                        var $activeTabPos = $(activeTab).position();
                        $wrappingWidth = $('#wrapping-div').position();

                        $moveTo = Math.abs($activeTabPos.left - $wrappingWidth.left);

                        if ($activeTabPos.left > $wrappingWidth.left) {
                            $('#sliding-panel').animate({
                                marginLeft: "-=" + $moveTo + "px"
                            }, 500);
                            if (tempSize == $slidingPanelWidth) {
                                $currentSlideSizeRight = $moveTo + ($newWindowWidth - $wrappingWidth.left) + $currentSlideSizeLeft;
                                $currentSlideSizeLeft += $moveTo;
                            }
                            else {
                                $currentSlideSizeLeft = $moveTo;
                                $currentSlideSizeRight = $moveTo + ($newWindowWidth - $wrappingWidth.left);
                            }
                           

                        }
                        else {
                            $('#sliding-panel').animate({
                                marginLeft: "+=" + $moveTo + "px"
                            }, 500);
                            if (tempSize == $slidingPanelWidth) {
                                $currentSlideSizeRight = ($newWindowWidth - $wrappingWidth.left) - $moveTo + $currentSlideSizeLeft;
                                $currentSlideSizeLeft -= $moveTo;
                            }
                            else {
                                $currentSlideSizeLeft = $moveTo;
                                $currentSlideSizeRight = ($newWindowWidth - $wrappingWidth.left) + $moveTo;
                            }
                            
                        }
                    
                        if ($currentSlideSizeRight >= $slidingPanelWidth) {
                            $('#right-arrow').hide();
                        }
                        else {
                            $('#right-arrow').show();
                        }

                        if ($currentSlideSizeLeft <= 0) {
                            $('#left-arrow').hide();
                        }
                        else {
                            $('#left-arrow').show();
                        }
                    }
                    else {
                        $currentSlideSizeLeft = 0;
                        //$currentSlideSizeRight = 0;
                        var $testW = $windowWidth - $wrappingWidth.left;
                        var $moveAgain = $testW - $slidingPanelWidth;
                        
                        $('#sliding-panel').animate({
                            marginLeft: "0"
                        }, 0)
                        $('#wrapping-div').addClass('pull-right');
                        $('#left-arrow').hide();
                        $('#right-arrow').hide();
                    }
                    tempSize = $slidingPanelWidth;
                },500);
                
            });
        }
        

        function qualityTabNavigationCtrl($scope, $translate) {
            var empId = userInfo.userId;
            var currentEmpIdForUrl = empId ? '/' + empId : null;

            var empParamId = utilitySvc.getIdParamFromUrl(null, true);
            var paramEmpIdForUrl = empParamId ? '/' + empParamId : null;

            var empQueryParamId = utilitySvc.getQueryParamFromUrl(Common.Enums.QUERY_PARAMETERS.CurrentEmployeeId.toString());
            var empQueryParamForUrl = empQueryParamId ? '/' + empQueryParamId : null;
            

            // Filling up the array
            $scope.list = [];

            // Quality
            if ($scope.iconSubNavigation == 'Quality') {
                $scope.list.push({
                    subIcon: 'fa-quality',
                    subLink: sitesettings.basePath + 'Quality/Audit/Index',
                    subText: 'AUDITS',
                    subActive: $scope.active == 'audits' ? 'true' : 'false',
                    subId: 'audits'
                });
                $scope.list.push({
                    subIcon: 'fa-complaints',
                    subLink: sitesettings.basePath + 'Quality/Complaint/Index',
                    subText: 'COMPLAINTS',
                    subActive: $scope.active == 'complaints' ? 'true' : 'false',
                    subId: 'complaints'
                });
                $scope.list.push({
                    subIcon: 'fa-ownership-old',
                    subLink: sitesettings.basePath + 'Quality/Compliment/Index',
                    subText: 'COMPLIMENTS',
                    subActive: $scope.active == 'compliments' ? 'true' : 'false',
                    subId: 'compliments'
                });
                $scope.list.push({
                    subIcon: 'fa-quiz',
                    subLink: sitesettings.basePath + 'Quality/CustomerSurvey/Index',
                    subText: 'SURVEYS',
                    subActive: $scope.active == 'surveys' ? 'true' : 'false',
                    subId: 'surveys'
                });
                    $scope.list.push({
                        subIcon: 'fa-tasks2',
                        subLink: sitesettings.basePath + 'Quality/ToDo/Index',
                        subText: "TO-DOs",
                        subActive: $scope.active == 'todos' ? 'true' : 'false',
                        subId: 'todos'
                    });
                $scope.list.push({
                    subIcon: 'fa-work-order',
                    subLink: sitesettings.basePath + 'Quality/WorkOrder/Index',
                    subText: 'WORK ORDERS',
                    subActive: $scope.active == 'workOrders' ? 'true' : 'false',
                    subId: 'workOrders'
                });
                $scope.list.push({
                    subIcon: 'fa-proactive',
                    subLink: sitesettings.basePath + 'Personnel/Ownership/Index',
                    subText: 'REPORT IT',
                    subActive: $scope.active == 'reportIts' ? 'true' : 'false',
                    subId: 'reportIts'
                });
            }

            // Financials
            if ($scope.iconSubNavigation == 'Financials') {

                if (userInfo.mainRoles.billingviewers) {
                    $scope.list.push({
                        subIcon: 'fa-bonus',
                        subLink: sitesettings.basePath + 'Financials/BillingOverview/Index',
                        subText: 'BILLING',
                        subActive: $scope.active == 'billing' ? 'true' : 'false',
                        subId: 'billing'
                    });
                }
                if (userInfo.mainRoles.suppliesviewers) {
                    $scope.list.push({
                        subIcon: 'fa-equipment-supplies',
                        subLink: sitesettings.basePath + 'Financials/Supplies/Index',
                        subText: 'SUPPLIES',
                        subActive: $scope.active == 'supplies' ? 'true' : 'false',
                        subId: 'supplies'
                    });
                }   
                if (userInfo.mainRoles.customers || userInfo.mainRoles.managers || userInfo.mainRoles.trainingadmins || userInfo.mainRoles.timeclockviewers) {
                    $scope.list.push({
                        subIcon: 'fa-hours',
                        subLink: sitesettings.basePath + 'Financials/TimeClock/Index',
                        subText: 'HOURS',
                        subActive: $scope.active == 'hours' ? 'true' : 'false',
                        subId: 'hours'
                    });
                }
                if (userInfo.mainRoles.billingviewers) {
                    $scope.list.push({
                        subIcon: 'fa-invoices',
                        subLink: sitesettings.basePath + 'Financials/Invoices/Index',
                        subText: 'INVOICES',
                        subActive: $scope.active == 'invoices' ? 'true' : 'false',
                        subId: 'invoices'
                    });
                }
                if (userInfo.mainRoles.billingviewers) {
                    $scope.list.push({
                        subIcon: 'fa-late',
                        subLink: sitesettings.basePath + 'Financials/AccountsReceivableAging/Index',
                        subText: 'PAST DUE',
                        subActive: $scope.active == 'pastDue' ? 'true' : 'false',
                        subId: 'pastDue'
                    });
                }
            }

            // Personnel
            if ($scope.iconSubNavigation == 'Personnel') {
                if (userInfo.mainRoles.managers || userInfo.mainRoles.customers || userInfo.mainRoles.trainingadmins) {
                    $scope.list.push({
                        subIcon: 'fa-team-stats',
                        subLink: sitesettings.basePath + 'Personnel/Boxscore',
                        subText: 'TEAM STATS',
                        subActive: $scope.active == 'teamStats' ? 'true' : 'false',
                        subId: 'teamStats'
                    });
                }
                if (userInfo.mainRoles.managers || userInfo.mainRoles.customers || userInfo.mainRoles.trainingadmins) {
                    $scope.list.push({
                        subIcon: 'fa-sep',
                        subLink: sitesettings.basePath + 'Training/ServiceExcellence',
                        subText: 'SEP',
                        subActive: $scope.active == 'sep' ? 'true' : 'false',
                        subId: 'sep'
                    });
                }
            }

            // Governance
            if ($scope.iconSubNavigation == 'Governance') {
                if (userInfo.mainRoles.managers || userInfo.mainRoles.customers || userInfo.mainRoles.trainingadmins) {
                    $scope.list.push({
                        subIcon: 'fa-personnel',
                        subLink: sitesettings.basePath + 'Governance/PositionFTE',
                        subText: 'Org Structure',
                        subActive: $scope.active == 'orgStructure' ? 'true' : 'false',
                        subId: 'orgStructure'
                    });
                }
                if (userInfo.mainRoles.managers || userInfo.mainRoles.customers || userInfo.mainRoles.trainingadmins) {
                    $scope.list.push({
                        subIcon: 'fa-leadership',
                        subLink: sitesettings.basePath + 'Governance/Map',
                        subText: 'Maps',
                        subActive: $scope.active == 'maps' ? 'true' : 'false',
                        subId: 'maps'
                    });
                }
                if (userInfo.mainRoles.managers) {
                    $scope.list.push({
                        subIcon: 'fa-re-use',
                        subLink: sitesettings.basePath + 'Governance/Turnover',
                        subText: 'Turnover',
                        subActive: $scope.active == 'turnover' ? 'true' : 'false',
                        subId: 'turnover'
                    });
                }
            }

            //my profile
            if ($scope.iconSubNavigation == 'My Profile') {
                var isLoggedInUser = false;
                var tempUserId = (empParamId || empQueryParamId);
                isLoggedInUser = ((tempUserId || userInfo.userId) == userInfo.userId);
                $scope.list.push({
                    subIcon: 'fa-profile',
                    subLink: sitesettings.basePath + `User/Profile/Index?${currentEmployeeIdQueryParam}=${userInfo.orgUserId || userInfo.userId}`,
                    subText: userInfo.mainRoles.employees ? 'MY PROFILE' : 'PROFILE',
                    subActive: $scope.active == 'myProfile' && isLoggedInUser ? 'true' : 'false',
                    subId: 'myProfile'
                });
                if (!userInfo.mainRoles.managers) {
                    $scope.list.push({
                        subIcon: 'fa-to-do',
                        subLink: sitesettings.basePath + 'Quality/ToDo/Index',
                        subText: 'MY TO-DOs',
                        subActive: $scope.active == 'toDos' ? 'true' : 'false',
                        subId: 'toDos'
                    });
                }
                
                $scope.list.push({
                    subIcon: 'fa-personnel',
                    subLink: sitesettings.basePath + 'Personnel/MyProfile/TeamMembers',
                    subText: userInfo.mainRoles.employees ? 'MY TEAM' : 'TEAM',
                    subActive: $scope.active == 'myTeam' || !isLoggedInUser ? 'true' : 'false',
                    subId: 'myTeam'
                });
            }


            angular.forEach($scope.list, (listItem) => {
                $translate(listItem.subText).then((translation) => {
                    listItem.subText = translation;
                });
            });


            $scope.tabWidth = sitesettings.windowSizes.isTablet ? 125 : 105; 

            $scope.slidingPanelWidth = $scope.tabWidth * $scope.list.length;
            var $slidingPanelWidth = $scope.slidingPanelWidth;

            var $pos = $('#sliding-panel').position();
            var $posOff = $('#sliding-panel').offset();
            var $rightPos = $('#right-arrow').position();
            var $windowWidth = $('#main').width();
            var $wrappingWidth = $('#wrapping-div').width();
            var $wrappingPos = $('#wrapping-div').position();
            var $titleWidth = $('#title-width').width();

                $('#sliding-panel').on('touchmove', function (e) {

                    var $wrapWidth = $('#wrapping-div').position();
                    var $slidingPanelPos = $('#sliding-panel').position();
                    var $slidingPanelWidthTouch = $('#sliding-panel').width();
                    var windowW = $(window).width();
                    var $titleW = $('#title-width').width();
                    if (($slidingPanelWidthTouch - $(window).width()) == Math.ceil(Math.abs($slidingPanelPos.left))) {
                        $('#right-arrow').hide();
                    }
                    else {
                        $('#right-arrow').show();
                    }
                    if ($wrapWidth.left == $slidingPanelPos.left) {
                        $('#left-arrow').hide();
                    }
                    else {
                        $('#left-arrow').show();
                    }

                });

                // Hidinng arrows

                if ($currentSlideSizeRight > $slidingPanelWidth) {
                    $('#right-arrow').hide();
                }

                if ($currentSlideSizeLeft <= $titleWidth) {
                    $('#left-arrow').hide();
                }


                $("#left-arrow").click(function () {
                    var $ww = $(window).width();
                    $scope.tabWidth = $ww >= 768 ? 125 : 105;
                    $scope.slidingPanelWidth = $scope.tabWidth * $scope.list.length;
                    var $slidingPanelWidth = $scope.slidingPanelWidth;

                        if (($currentSlideSizeLeft) < 150) {
                            var toLeft = $currentSlideSizeLeft;
                                $('#sliding-panel').animate({
                                    marginLeft: "+=" + toLeft + "px"
                                }, 500);
                            $currentSlideSizeRight -= ($currentSlideSizeLeft);
                            $currentSlideSizeLeft -= ($currentSlideSizeLeft);
                        }
                        else {
                            $('#sliding-panel').animate({
                                marginLeft: "+=150px"
                            }, 500);
                            $currentSlideSizeLeft -= 150;
                            $currentSlideSizeRight -= 150;
                        }
                        if ($currentSlideSizeLeft == 0) {
                            $('#left-arrow').hide();
                        }
                        if ($currentSlideSizeRight < $slidingPanelWidth) {
                            $('#right-arrow').show();
                        }
                });



                $("#right-arrow").click(function () {
                    var $ww = $(window).width();
                    $scope.tabWidth = $ww >= 768 ? 125 : 105;
                    $scope.slidingPanelWidth = $scope.tabWidth * $scope.list.length;
                    var $slidingPanelWidth = $scope.slidingPanelWidth;
                        $('#left-arrow').show();

                        if (($slidingPanelWidth - $currentSlideSizeRight) < 150) {
                            var toRight = ($slidingPanelWidth - $currentSlideSizeRight);
                            $('#sliding-panel').animate({
                                marginLeft: "-=" + toRight + "px"
                            }, 500);
                            $currentSlideSizeLeft += ($slidingPanelWidth - $currentSlideSizeRight);
                            $currentSlideSizeRight += ($slidingPanelWidth - $currentSlideSizeRight);
                        }
                        else {
                            $('#sliding-panel').animate({
                                marginLeft: "-=150px"
                            }, 500);
                            $currentSlideSizeRight += 150;
                            $currentSlideSizeLeft += 150;
                        }

                        if ($currentSlideSizeRight >= $slidingPanelWidth) {
                            $('#right-arrow').hide();
                        }
                });
        }

    }

    angular.module('app').directive('qualityTabNavigation', qualityTabNavigation);
}