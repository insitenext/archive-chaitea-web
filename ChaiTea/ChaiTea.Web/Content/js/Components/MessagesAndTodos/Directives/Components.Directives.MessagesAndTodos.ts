﻿/// <reference path="../../../_libs.ts" />
module ChaiTea.Components.Directives {
    'use strict';
    import compInterfaces = ChaiTea.Components.Interfaces;

    MessagesAndTodos.$inject = ['$log', '$window','userInfo','userContext', 'sitesettings', 'chaitea.common.services.apibasesvc', 'chaitea.core.services.messagebussvc'];
    function MessagesAndTodos(
        $log: angular.ILogService,
        $window: angular.IWindowService,
        userInfo: IUserInfo,
        userContext: IUserContext,
        sitesettings: ISiteSettings,
        apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
        serviceMessageBus: ChaiTea.Core.Services.IMessageBusSvc) {

        var markingIsBusy = false;
        
        return <angular.IDirective>{
            restrict: 'E',
            replace:true,
            scope: {
                todoCounts: '=todoCounts',
                messageCounts: '=messageCounts',
                allCounts:'=allCounts',
                showAllColumn: '=showAllColumn',
                externalNotes: '=',
                externalTodos: '=',
                hideTitle: '@'
            },
            controller: ['$scope', controllerFn],
            link: function ($scope: any, $element: any, $attrs: any) {
                $scope.$watch(() => ($scope.messages), (newVal, oldVal) => {
                    if (newVal == oldVal) return;

                    if ($scope.showAllColumn) {
                        $scope.allCounts = $scope.messageCounts + $scope.todoCounts;
                    }
                },true);

                $scope.$watch(() => ($scope.todos),(newVal, oldVal) => {
                    if (newVal == oldVal) return;
                    $scope.todoCounts = _.filter($scope.todos, (item: ITodoNotification) => (!item.IsComplete)).length;
                    if ($scope.showAllColumn) {
                        $scope.allCounts = $scope.messageCounts + $scope.todoCounts;
                    }
                },true);
            },
            templateUrl: sitesettings.basePath + 'Content/js/Components/MessagesAndTodos/Templates/messages-and-todos.tmpl.html'
        }

        function controllerFn($scope) {
            $scope.pagingObjects = {
                isMessagesBusy: false,
                isTodosBusy: false,
                isAllLoaded: false,
                allTodosLoaded: false,
                allMessagesLoaded: false,
                topTodos: 20,
                todoRecordsToSkip: 0,
                topMessages: 20,
                messageRecordsToSkip: 0
            };
            $scope.currentSiteName = userContext.Site.Name;
            $scope.messages = [];

            $scope.actFilter = $scope.showAllColumn ? 'all' : 'message';
            $scope.isAllSites = !userContext.Site.ID;

            $scope.noteTemplateUrl = sitesettings.basePath + 'Content/js/Components/NotificationList/Templates/notification-list.tmpl.html';
            $scope.noteItemTemplate = sitesettings.basePath + 'Content/js/Components/NotificationList/Templates/notification-list.item.tmpl.html';

            $scope.todoTemplateUrl = sitesettings.basePath + 'Content/js/Components/TodoList/Templates/todo-list.tmpl.html';
            $scope.todoItemTemplate = sitesettings.basePath + 'Content/js/Components/TodoList/Templates/todo-list.item.tmpl.html';

            $scope.allTemplateUrl = sitesettings.basePath + 'Content/js/Components/MessagesAndTodos/Templates/messages-and-todos.allitems.tmpl.html';

            getTotalMessages($scope);

            getMessageNotifications($scope);

            getTodos($scope);

            serviceMessageBus.onMessage('notification:activity-feed', $scope, (message) => {
                console.log('act feed', message);
                getTotalMessages($scope);
                getMessageNotifications($scope,true);
            });
            serviceMessageBus.onMessage('notification:todo', $scope,(message) => {
                getTodos($scope);
            });

            $scope.goToTodos = goToTodos;
            $scope.onMessageItemClick = onItemClick;
            $scope.getMessagesClick = () => { getMessageNotifications($scope); };
            $scope.getTodosClick = () => { getTodos($scope) };
        }

        function createAllList($scope) {
            $scope.allItems = _.union($scope.messages, $scope.todos);
            if ($scope.showAllColumn) {
                $scope.allCounts = $scope.messageCounts + $scope.todoCounts;
            }
        }

        function getTotalMessages($scope) { 
            var param = {
                $filter: 'IsNew eq true',
                $select: 'NotificationId'
            };
            apiSvc.getByOdata(param, 'Notification').then((res) => {
                if (!res.length) return $log.log('No new messages for count');

                $scope.messageCounts = res.length;
            });
        }

        //#region Notification
        function getMessageNotifications($scope, fromBus?:boolean) {
            if ($scope.pagingObjects.isMessagesBusy || $scope.pagingObjects.allMessagesLoaded) return;
            $scope.pagingObjects.isMessagesBusy = true;

            var param = getMessagePagingParams($scope);

            if (fromBus === true) {
                var latestItemDate = moment($scope.messages[0].CreateDate).toISOString();
                param.$skip = 0;
                param.$top = 20;
                param.$filter = `CreateDate gt DateTime'${latestItemDate}'`;
            }

            apiSvc.getByOdata(param, 'Notification').then((res) => {
                if (!res) return $log.log('message response is null');

                if (res.length == 0 && !fromBus) {
                    $scope.pagingObjects.allMessagesLoaded = true;
                    return;
                }
                
                angular.forEach(res, (item: compInterfaces.IMessageNotification) => {
                    item._icon = item.Type == compInterfaces.NOTIFICATION_TYPE.Other
                        ? "alert2" : item.Type.toString().toLowerCase();

                    if (item.Department && item.Department['Name'] != 'General') {
                        item._icon = 'dpt-' + item.Department['Name'].toLowerCase().replace(' ','-');
                    }
                    //Custom display for long Messages from Message Builder
                    if (item.Department &&
                        item.Type == compInterfaces.NOTIFICATION_TYPE.Message) {
                        item.Summary = "Message from " + item.Department['Name'];
                    }
                    //If item does not exist in message
                    if (!_.any($scope.messages, (mess: compInterfaces.IMessageNotification) => (mess.NotificationId == item.NotificationId))) {
                        if (fromBus === true) {
                            $scope.messages.unshift(item);
                        } else {
                            $scope.messages.push(item);
                        }
                    }
                });

                if (!fromBus) {
                    $scope.pagingObjects.messageRecordsToSkip += $scope.pagingObjects.topMessages;
                }

                $scope.pagingObjects.isMessagesBusy = false;

                createAllList($scope);
            });
        }


        function onItemClick(notificationItem: compInterfaces.INotification) {
            if (!notificationItem || markingIsBusy) {
                return;
            }
            markingIsBusy = true;

            mixpanel.track('Notification item was clicked', { NotificationId: notificationItem.NotificationId });

            if (!notificationItem.IsNew) {
                goToUrl(notificationItem.Url);
                markingIsBusy = false;
                return;    
            }

            notificationItem.IsNew = false;
            apiSvc.update(notificationItem.NotificationId, notificationItem, "Notification").then((res) => {
                if (notificationItem.Url) {
                    goToUrl(notificationItem.Url);
                }
            },(error) => {
                    notificationItem.IsNew = true;
                }).then((res) => {
                markingIsBusy = false;
            });
        }
        
        function getTodos($scope) {
            if ($scope.pagingObjects.isTodosBusy || $scope.pagingObjects.allTodosLoaded) {
                return;
            }
            $scope.pagingObjects.isTodosBusy = true;

            var param = getTodoPagingParams($scope);
            apiSvc.getByOdata(param, 'TodoItem/ByEmployee').then((res) => {
                if (!res) {
                    return;
                }

                if (res.length == 0) {
                    $scope.pagingObjects.allTodosLoaded = true;
                    return;
                }

                var now = new Date();
                now.setHours(0, 0, 0, 0);

                if (!$scope.todos) {
                    $scope.todos = [];
                }

                angular.forEach(res, (item: ITodoNotification) => {
                    item.isPassedDue = moment(item.DueDate).isBefore(now);
                    $scope.todos.push(item);
                });
                $scope.todoCounts = $scope.todos.length;

                $scope.pagingObjects.isTodosBusy = false;
                $scope.pagingObjects.todoRecordsToSkip += $scope.pagingObjects.topTodos;

                createAllList($scope);
            });
        }

        function goToTodos(todo) {
            var todoParams = '';
            todoParams = todo ? `EntryView/?id=${todo.TodoItemId}` : '';
            $window.location.href = sitesettings.basePath + 'Quality/ToDo/' + todoParams;
        }
        //#endregion

        function goToUrl(url) {
            if (!url) {
                return;
            }
            //remove '/' from end to work with URLs from DB
            var base = (url[0] == '/') ? sitesettings.basePath.slice(0, -1) : sitesettings.basePath;
            window.location.href = base + url;
        }

        function getTodoPagingParams($scope) {
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $orderby: 'CreateDate desc',
                $filter: `IsComplete eq false`
            };

            return params;
        }

        function getMessagePagingParams($scope) {
            //Start getting more messages at a time.
            if ($scope.messages.length && ($scope.messages.length > ($scope.pagingObjects.topMessages * 3))) {
                $scope.pagingObjects.topMessages = 60;
            }
            var params = <Common.Interfaces.IODataPagingParamModel>{
                $top: $scope.pagingObjects.topMessages,
                $skip: $scope.pagingObjects.messageRecordsToSkip,
                $orderby: 'CreateDate desc'
            };

            return params;
        }
        
    }

    angular.module('app').directive('messagesAndTodos', MessagesAndTodos);
} 