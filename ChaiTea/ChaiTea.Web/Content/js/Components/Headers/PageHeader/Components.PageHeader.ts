module ChaiTea.Components{
    class PageHeaderCtrl implements angular.IComponentController{
        s3IconName: string; // binding
        iconUrl: string; // binding

        popOverPlacement: string = "bottom-left";
        static $inject = ['sitesettings',ChaiTea.Common.Services.AwsSvc.id];
        constructor(
            private sitesettings: ISiteSettings,
            private awsSvc: ChaiTea.Common.Services.IAwsSvc) {
            if (this.sitesettings.windowSizes.isNotDesktop) {
                this.popOverPlacement = 'auto';
            }
            
            if (this.s3IconName && !this.iconUrl) {
                this.iconUrl = this.awsSvc.getUrl('public/icons/' + this.s3IconName);
            }
        }
        $onInit() {
        }
    }
    
    angular.module('app').component('pageHeader',{
        controller: PageHeaderCtrl,
        transclude:true,
        templateUrl: SiteSettings.basePath + 'Content/js/Components/Headers/PageHeader/page-header.tmpl.html',
        bindings: {
            title: '@',
            infoText: '@',
            iconUrl: '@',
            s3IconName:'@'
        }
    });
}