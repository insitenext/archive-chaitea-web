﻿module ChaiTea.Component {
    export class TabFilterItem {
        public title: string;
        public onClickFn: Function;
        public count?: number = null;
        public url?: string;
    }


    class TabFilterHeader implements angular.IComponentController{
        tabItems; // bindings
        tabType = 'tabs';

        isMobile = false;
        static $inject = ['sitesettings']
        constructor(private sitesettings: ISiteSettings) {
            
        }

        $onInit() {
            if (this.sitesettings.windowSizes.isNotDesktop) {
                this.isMobile = true;
                this.tabType = 'pills';
            }
        }

        public runClickFn = ($event: angular.IAngularEvent, tabItem: TabFilterItem) => {
            if (!$event) {
                return;
            }
            if (_.isFunction(tabItem.onClickFn)) {
                return tabItem.onClickFn();
            } else if(tabItem.url){
                return window.location.href = SiteSettings.basePath + tabItem.url;
            }
        }
    }

    angular.module('app').component('tabFilterHeader', {
        controller: TabFilterHeader,
        bindings: {
            tabItems: '<'
        },
        template: `
            <uib-tabset type="{{$ctrl.tabType}}">
                <uib-tab ng-repeat="tab in $ctrl.tabItems" select="$ctrl.runClickFn($event,tab)" class="{{(tab.count !== undefined)?'has-count':''}}">
                    <uib-tab-heading>
                        <span class="count" ng-if="tab.count !== null">{{tab.count}}</span>
                        <span class="title">{{tab.title}}</span>
                    </uib-tab-heading>
                </uib-tabs>
            </uib-tabset>
        `
    });
}