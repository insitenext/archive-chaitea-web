﻿module ChaiTea.Components {

    class UtilityHeaderCtrl implements angular.IComponentController {
        tabItems: { name: string; href: string; }[]; //binding
        static $inject = ['$scope','$log','$transclude'];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService) { }

        $onInit() {
            
            // check that tabItems objects are correct
            if (this.tabItems && !_.isArray(this.tabItems)) {
                this.$log.error('UtilityHeader: tabItems property require an array of {name:string; href:string;}.');
                this.tabItems = null;
                return false;
            }
        }
    }

    angular.module('app').component('utilityHeader', {
        controller: UtilityHeaderCtrl,
        transclude: {
            filters: '?filters',
            customButtons: '?customButtons',
            tabs: '?tabs'
        },
        bindings: {
            tabItems: '<'
        },
        templateUrl: SiteSettings.basePath + 'Content/js/Components/Headers/UtilityHeader/utility-header.tmpl.html'
    });


    
}