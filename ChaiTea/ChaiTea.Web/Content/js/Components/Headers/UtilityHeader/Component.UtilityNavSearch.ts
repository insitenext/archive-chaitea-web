﻿module ChaiTea.Components {

    class UtilityNavSearch implements angular.IComponentController {
        searchModel;
        placeholder:string;

        isActive = false;
        isMobile = false;

        popoverTemplateUrl = `${SiteSettings.basePath}Content/js/Components/Headers/UtilityHeader/utility-nav-search.tmpl.html`;
        static $inject = ['sitesettings', '$scope', '$uibModal'];

        constructor(
            private sitesettings: ISiteSettings,
            private $scope: angular.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService) {
            this.isMobile = sitesettings.windowSizes.isNotDesktop;
        }
        $onInit() {
            this.$scope.$watch(() => (this.searchModel), (newVal, oldVal) => {
                if (newVal === oldVal) return;
                this.isActive = newVal.length > 0;
            });
        }

        public popMobileModal = () => {
            if (!this.isMobile) {
                return;
            }
            this.$uibModal.open({
                windowClass: 'utility-search-modal',
                templateUrl: this.popoverTemplateUrl,
                scope: this.$scope,
                controllerAs: '$ctrl'
            });
        }
        
    }


    angular.module('app').component('utilityNavSearch', {
        controller: UtilityNavSearch,
        template: `<button class="{{$ctrl.buttonClasses}} {{$ctrl.isActive?'active':''}}" 
                    uib-popover-template="$ctrl.popoverTemplateUrl"
                    popover-class="search-bar"
                    popover-placement="auto bottom"
                    popover-append-to-body="true"
                    popover-trigger="'outsideClick'"
                    popover-enable="{{!$ctrl.isMobile}}"
                    ng-click="$ctrl.popMobileModal()">
                        <span class="fa fa-search2"></span> {{'SEARCH'|translate}}
                    </button>`,
        bindings: {
            searchModel: '=',
            buttonClasses: '@',
            placeholder: '@?'
        }
    });
}