﻿module ChaiTea.Components {

    class UtilityNavFilterTagsCtrl implements angular.IComponentController {
        filters: any[] = [];
        static $inject = [
            '$scope',
            '$log',
            'sitesettings',
            ChaiTea.Core.Services.MessageBusSvc.id];
        constructor(
            private $scope: angular.IScope,
            private $log: angular.ILogService,
            private sitesettings: ISiteSettings,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc) {
        }

        $onInit() {
            this.messageBusSvc.onMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterChange, this.$scope, (event, filterTagArray: Components.IFilterTag[]) => {
                this.filters = filterTagArray;
            });
        }

        public clearTag = () => {
            this.messageBusSvc.emitMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterClear, {});
        }
    }

    angular.module('app').component('utilityNavFilterTags', <angular.IComponentOptions>{
        controller: UtilityNavFilterTagsCtrl,
        template: `<div ng-if="$ctrl.filters.length">
                       <strong>Filters: </strong>
                       <ul class="list-inline">
                            <li ng-repeat="tag in $ctrl.filters" class="btn-group" ng-click="$ctrl.clearTag()">
                                <button class="btn btn-xs custom-button">{{tag.filter}}: {{tag.option}}</button>
                                <button class="btn btn-xs custom-button padding-horizontal-10">&times;</button>
                            </li>
                       </ul>
                   </div>`
    })
}