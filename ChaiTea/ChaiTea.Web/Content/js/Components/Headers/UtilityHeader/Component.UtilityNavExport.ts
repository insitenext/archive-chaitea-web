﻿module ChaiTea.Components {
    class UtilityNavExportCtrl implements angular.IComponentController {
        exportOptions;
        exportFn;
        popoverTemplateUrl = SiteSettings.basePath + 'Content/js/Components/Headers/UtilityHeader/utility-nav-export.tmpl.html';
        isMobile: boolean = false;
        exportId: number = null;
        isOpen = false;

        static $inject = [
            '$scope',
            '$log',
            '$q',
            'sitesettings',
            '$uibModal'
        ];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService
        ) {
        }

        $onInit() {
            this.isMobile = this.sitesettings.windowSizes.isNotDesktop;
        }

        public export = () => {
            this.exportFn();
        }

        public cancel = () => {
            this.exportId = null;
            this.isOpen = false;
        }

        public popMobileModal = (templateUrl?) => {
            this.isOpen = true;
            if (!this.isMobile) {
                return;
            }
            this.$uibModal.open({
                windowClass: 'utility-export-modal',
                templateUrl: templateUrl || this.popoverTemplateUrl,
                scope: this.$scope,
                controllerAs: '$ctrl'
            });
        }
    }

    angular.module('app').component('utilityNavExport', {
        controller: UtilityNavExportCtrl,
        transclude: true,
        template: `<button  class="{{$ctrl.buttonClasses}}"
                            popover-placement="auto bottom"
                            popover-append-to-body="true"
                            popover-trigger="'outsideClick'"
                            uib-popover-template="'{{$ctrl.popoverTemplateUrl}}'"
                            popover-enable="{{!$ctrl.isMobile}}"
                            popover-is-open="$ctrl.isOpen"
                            ng-click="$ctrl.popMobileModal()">
                        <span class="fa fa-download"></span>
                        Export  
                    </button>`,
        bindings: {
            exportOptions: '<',
            exportFn: '&',
            buttonClasses: '@',
            exportId: '='
        }
    })

}