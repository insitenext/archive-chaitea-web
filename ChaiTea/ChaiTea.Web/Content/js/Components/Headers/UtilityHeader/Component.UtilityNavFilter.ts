﻿module ChaiTea.Components {
    export interface IFilterTag {
        filter: string;
        option: string;
    }

    class UtilityNavFilterCtrl implements angular.IComponentController {
        filter; //binding
        model; //binding
        filterOptions; //binding
        filterApplyFn; //binding
        filterClearFn; //binding

        isActive = false;
        
        activeFilterOptions;
        popoverTemplateUrl = SiteSettings.basePath + 'Content/js/Components/Headers/UtilityHeader/utility-nav-filter.tmpl.html';
        hasTranscluded = false;
        isMobile: boolean = false;

        static $inject = [
            '$scope',
            '$log',
            '$q',
            'sitesettings',
            '$uibModal',
            ChaiTea.Core.Services.MessageBusSvc.id];
        constructor(
            private $scope,
            private $log: angular.ILogService,
            private $q: angular.IQService,
            private sitesettings: ISiteSettings,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc) {
        }

        $onInit() {
            this.isMobile = this.sitesettings.windowSizes.isNotDesktop;
            if (this.filter && this.filter.filterType != null) {
                this.onFilterTypeChange();
                if (this.filter.filterCriteria) {   
                    this.isActive = true;
                }
            }
            // Wait and watch for filterOptions to be supplied by parent
            this.$scope.$watch(() => (this.activeFilterOptions), (newVal, oldVal) => {
                if (!this.filter.filterType) {
                    return;
                }
                this.emitFilterTags();
            }, true)
            this.$scope.$watch(() => (this.filter), () => {
                this.isActive = false;
                if (this.filter.filterType && this.filter.filterCriteria) {
                    this.isActive = true;
                }
            }, true)
            // Will be used to clear from utility-nav-filter-tags
            this.messageBusSvc.onMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterClear, this.$scope, this.clearFilter);
        }

        public onFilterTypeChange() {
            var selected = this.filter.filterType;
            this.activeFilterOptions = null;;
            this.filter.filterCriteria = this.filter.filterCriteria ? this.filter.filterCriteria : '';

            var filterOptns = _.find(this.filterOptions, { Key: selected });
            if (filterOptns) {
                this.activeFilterOptions = filterOptns;
            }
        }

        /**
         * @description Handler to refresh data based on filter criteria.
         */
        public onFilterResults($event) {
            if (!this.filter.filterCriteria) return;
            this.isActive = true;
            this.filterApplyFn();
            this.emitFilterTags();
        }

        /**
         * @description Emits message containing filters that have been set.
         */
        private emitFilterTags = () => {
            if (this.filter.filterType && this.filter.filterCriteria) {

                let activeFilter: any = _.find(this.filterOptions, { Key: this.filter.filterType });
                let activeOption: any = activeFilter ? _.find(activeFilter.Options || this.activeFilterOptions.Options, { Key: this.filter.filterCriteria }) : null;
                let filterTags: IFilterTag[] = [];

                if (activeFilter && activeOption) {
                    filterTags = [{
                        filter: activeFilter.Value,
                        option: activeOption.Value
                    }];
                }
                this.messageBusSvc.emitMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterChange, filterTags);
            }
            else {
                this.messageBusSvc.emitMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterChange, null);
            }
        }

        /**
         * @description Handler to clear the filter.
         */
        private onClearFilter($event?) {
            // Reset the filter
            this.filter.filterType = null;
            this.filter.filterCriteria = null;
            this.activeFilterOptions = null;
            this.isActive = false;
            this.filterClearFn();
            this.emitFilterTags();
           
        }

        private clearFilter = () => {
            this.filter.filterType = null;
            this.filter.filterCriteria = null;
            this.activeFilterOptions = null;
            this.isActive = false;
            this.messageBusSvc.emitMessage(ChaiTea.Common.Interfaces.MessageBusMessages.DetailsFilterChange, null);
            this.filterClearFn();
        }

        /**
         * @description Helper to check if object is empty.
         */
        public isEmpty(obj) {
            return _.isEmpty(obj);
        }
        
        public popMobileModal = (templateUrl?) => {
            if (!this.isMobile) {
                return;
            }
            this.$uibModal.open({
                windowClass:'utility-filter-modal',
                templateUrl: templateUrl || this.popoverTemplateUrl,
                scope:this.$scope,
                controllerAs: '$ctrl'
            });
        }
    }

    angular.module('app').component('utilityNavFilter', {
        controller: UtilityNavFilterCtrl,
        transclude: true,
        template: `<button class="{{$ctrl.buttonClasses}} {{$ctrl.isActive ? 'active':''}}" 
                            popover-placement="auto bottom"
                            popover-append-to-body="true"
                            popover-trigger="'outsideClick'"
                            uib-popover-template="'{{$ctrl.popoverTemplateUrl}}'"
                            popover-enable="{{!$ctrl.isMobile}}"
                            ng-click="$ctrl.popMobileModal()">
                        <span class="fa fa-sliders"></span>
                        Filter By
                        <span class="fa fa-sort margin-left-20 visible-md-inline-block visible-lg-inline-block"></span>
                    </button>`,
        bindings: {
            model: '<ngModel',              // bound to the filtered data list
            filterOptions: '<',             // array of filter options
            filterApplyFn: '&',             // fn to invoke filter and refresh data list
            filterClearFn: '&',             // fn to clear filter and refresh data list
            filter: '=',                    // filter criteria, 2-way
            buttonClasses: '@'
        }
    })

}