﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Configs.Admin {
    'use strict';

    import adminSvc = ChaiTea.Admin.Services;
    import commonSvc = ChaiTea.Common.Services;

    /**
     * @description Resolver object definitions.
     */
    var cachedOrgs = [],
        cachedClients = [],
        cachedPrograms = [],
        cachedAreas = [];

    tenantManagementResolver.$inject = ['chaitea.admin.services.tenantmanagementresolversvc'];
    function tenantManagementResolver(tenantManagementResolver: adminSvc.ITenantManagementResolverSvc) {
        return {
            getOrgs: tenantManagementResolver.getOrgs(),
            getClients: tenantManagementResolver.getClients(),
            getPrograms: tenantManagementResolver.getPrograms(),
            getAreas: tenantManagementResolver.getAreas(),
            getUsers: tenantManagementResolver.getUsers()
        }
    }

    // Orgs
    getOrgs.$inject = [
        '$q',
        '$stateParams',
        'chaitea.common.services.tenantmanagementsvc'];
    function getOrgs(
        $q: angular.IQService,
        $stateParams: angular.ui.IStateParamsService,
        tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        return new $q((resolve, reject) => {
            if (cachedOrgs.length) {
                resolve(cachedOrgs);
            } else {
                tenantManagementSvc.getOrgsByOData().then(data => {
                    cachedOrgs = data;
                    resolve(data);
                });
            }
        });
    };

    // Clients
    getClients.$inject = [
        '$q',
        '$stateParams',
        'chaitea.common.services.tenantmanagementsvc'];
    function getClients(
        $q: angular.IQService,
        $stateParams: angular.ui.IStateParamsService,
        tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        return new $q((resolve, reject) => {
            if (cachedClients.length) {
                resolve(cachedClients);
            } else {
                tenantManagementSvc.getClientsByOData().then(data => {
                    cachedClients = data;
                    resolve(data);
                });
            }
        });
    };    
    
    // Programs
    getPrograms.$inject = [
        '$q',
        '$stateParams',
        'chaitea.common.services.tenantmanagementsvc'];
    function getPrograms(
        $q: angular.IQService,
        $stateParams: angular.ui.IStateParamsService,
        tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        return new $q((resolve, reject) => {
            if (cachedPrograms.length) {
                resolve(cachedPrograms);
            } else {
                tenantManagementSvc.getProgramsByOData({ $orderby : 'Name' }).then(data => {
                    cachedPrograms = data;
                    resolve(data);
                });
            }
        });
    };

    // Areas
    getAreas.$inject = [
        '$q',
        '$stateParams',
        'chaitea.common.services.tenantmanagementsvc'];
    function getAreas(
        $q: angular.IQService,
        $stateParams: angular.ui.IStateParamsService,
        tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        return new $q((resolve, reject) => {
            if (cachedAreas.length) {
                resolve(cachedAreas);
            } else {
                tenantManagementSvc.getAreasByOData().then(data => {
                    cachedAreas = data;
                    resolve(data);
                });
            }
        });
    };

    // Users
    getUsers.$inject = [
        '$q',
        '$stateParams',
        'chaitea.common.services.tenantmanagementsvc'];
    function getUsers(
        $q: angular.IQService,
        $stateParams: angular.ui.IStateParamsService,
        tenantManagementSvc: commonSvc.ITenantManagementSvc) {

        return new $q((resolve, reject) => {
            tenantManagementSvc.getUsers().then(data => {
                resolve(data);
            });
        });
    };

    var tabs = [
        { heading: "Tenant Node", route: "manage.tenantnode", active: false },
        { heading: "Global Programs", route: "manage.globalprograms", active: false },
        { heading: "Global Areas", route: "manage.globalareas", active: false },
        { heading: "Programs", route: "manage.programs", active: false },
        { heading: "Areas", route: "manage.areas", active: false },
        { heading: "Users", route: "manage.users", active: false },
        { heading: "Area Classifications", route: "manage.areaclassifications", active: false }
    ];

    /**
     * @description Routing configuration for admin tenant management.
     */
    tenantManagementConfig.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        'sitesettings'];

    function tenantManagementConfig(
        $stateProvider: angular.ui.IStateProvider,
        $urlRouterProvider: angular.ui.IUrlRouterProvider,
        sitesettings: ISiteSettings) {

        var templatePath = ` ${sitesettings.basePath}Content/js/Admin/Templates/`;

        /**
         * @description Define the parent route.
         */
        $stateProvider.state('manage', {
            url: '/manage',
            templateUrl: 'admin.tenantmanagement.main.tmpl.html',
            controller: 'Admin.TenantManagementMainCtrl',
            controllerAs: 'vm',
            resolve: {
                tabs: () => {
                    return tabs;
                }
            }
        });

        /**
         * @description Define the child routes.
         */
        // Tenant node
        $stateProvider
            .state('manage.tenantnode', {
            url: '/tenantnode',
            controller: 'Admin.TenantManagementNodeCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.tenantnode.tmpl.html',
            resolve: {
                clients: getClients
            }
        });

        // Global programs
        $stateProvider
            .state('manage.globalprograms', {
            url: '/globalprograms',
            controller: 'Admin.TenantManagementGlobalProgramsCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.globalprograms.tmpl.html'
        });

        // Global areas
        $stateProvider
            .state('manage.globalareas', {
            url: '/globalareas',
            controller: 'Admin.TenantManagementGlobalAreasCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.globalareas.tmpl.html'
        });

        // Programs
        $stateProvider
            .state('manage.programs', {
            url: '/programs',
            controller: 'Admin.TenantManagementProgramsCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.programs.tmpl.html',
            resolve: {
                clients: getClients,
                programs: getPrograms
            }
        });

        // Areas
        $stateProvider
            .state('manage.areas', {
            url: '/areas',
            controller: 'Admin.TenantManagementAreasCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.areas.tmpl.html',
            resolve: {
                clients: getClients,
                areas: getAreas
            }
        });

        // Users
        $stateProvider
            .state('manage.users', {
            url: '/users',
            controller: 'Admin.TenantManagementUsersCtrl',
            controllerAs: 'vm',
            templateUrl: templatePath + 'admin.tenantmanagement.users.tmpl.html',
            resolve: {
                orgs: getOrgs,
                clients: getClients
            }
        });

        // Area Classifications
        $stateProvider
            .state('manage.areaclassifications', {
                url: '/areaclassifications',
                controller: 'Admin.TenantManagementAreaClassificationCtrl',
                controllerAs: 'vm',
                templateUrl: templatePath + 'admin.tenantmanagement.areaclassification.tmpl.html'
            });
    }

    /**
     * Get cookie value (moved from nested function below).
     *
     * @param cookie
     * @param key
     */
    function getCookieVal(cookie, key) {
        var cookieIndex = cookie.indexOf(key);

        if (cookieIndex === -1)
            return undefined;

        var substr = cookie.substring(cookieIndex + key.length + 1);
        var end = substr.indexOf('; ');

        if (end === -1)
            return substr;

        return substr.substring(0, end);
    }

    /**
     * @description Register the route config and initialize the routes.
     */
    angular.module('app').config(tenantManagementConfig);
    angular.module('app').run(['$rootScope', '$state', function ($rootScope, $state) {
        if (window.location.hash) {

            $state.go(window.location.hash.replace('#/', '').replace('/', '.'));
        }
        else {
            $state.go('manage.tenantnode');
        }
        $rootScope.$on('$stateChangeSuccess', function (e, to) {
            //console.log(e, to);
        });

    }]);
} 