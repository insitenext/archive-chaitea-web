﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Configs.Training {
    'use strict';

    import trainingSvc = ChaiTea.Training.Services;

    var tabs = [
        { heading: "Step 1", route: "builder.step1", active: false },
        { heading: "Step 2", route: "builder.step2", active: false },
        { heading: "Step 3", route: "builder.step3", active: false }
    ];

    /**
     * @description Routing configuration for training builder.
     */
    class TrainingBuilderConfig {
        public static $inject = [
            '$stateProvider',
            '$urlRouterProvider',
            'sitesettings'];

        constructor($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, sitesettings: ISiteSettings) {

            var templatePath = `${sitesettings.basePath}Content/js/Training/Templates/`;

            // Define the parent route.
            $stateProvider.state('builder', {
                url: '/builder',
                templateUrl: 'training.trainingbuilder.main.tmpl.html',
                controller: 'Training.TrainingBuilderMainCtrl',
                controllerAs: 'vm',
                resolve: {
                    tabs: () => { return tabs; }
                }
            });

            // Define the child routes.
            $stateProvider
                .state('builder.step1', {
                    url: '/step1',
                    controller: 'Training.TrainingBuilderStep1Ctrl',
                    controllerAs: 'vm',
                    templateUrl: templatePath + 'training.trainingbuilder.step1.tmpl.html'
                });

            $stateProvider
                .state('builder.step2', {
                    url: '/step2',
                    controller: ['$scope', '$state', ($scope: angular.IScope, $state: angular.ui.IState) => {
                        var current = $state['current'].name.split('.');
                        var stateName: string = current.length === 3 ? `SELECT ${current[current.length - 1].toUpperCase()}S`: '';        
                        $scope['currentState'] = stateName;
                    }],
                    templateUrl: templatePath + 'training.trainingbuilder.step2.tmpl.html'
                });

            $stateProvider
                .state('builder.step2.employee', {
                    url: '/employee',
                    controller: 'Training.TrainingBuilderAssignByEmployeeCtrl',
                    controllerAs: 'vm',
                    templateUrl: templatePath + 'training.trainingbuilder.assigncoursebyemployee.tmpl.html'
                });

            $stateProvider
                .state('builder.step2.position', {
                    url: '/position',
                    controller: 'Training.TrainingBuilderAssignByPositionCtrl',
                    controllerAs: 'vm',
                    templateUrl: templatePath + 'training.trainingbuilder.assigncoursebyposition.tmpl.html'
                });

            $stateProvider
                .state('builder.step3', {
                    url: '/step3/:employeeIds',
                    controller: 'Training.TrainingBuilderStep3Ctrl',
                    controllerAs: 'vm',
                    templateUrl: templatePath + 'training.trainingbuilder.step3.tmpl.html'
                });
        }
    } // Register the route config and initialize the routes.
    angular.module('app').config(TrainingBuilderConfig);
    angular.module('app').run(['$rootScope', '$state', ($rootScope, $state) => {
        var hash: string = window.location.hash;

        if (hash) {
            var state: string = hash.substr(2).split('/').filter(Boolean).join('.');

            $state.go($state.includes(state) ? state : 'builder.step1');         
        }
        else {
            $state.go('builder.step1', null, {reload: true});
        }
        $rootScope.$on('$stateChangeSuccess', (e, to) => {
            //console.log(e, to);
        });

    }]);
}  