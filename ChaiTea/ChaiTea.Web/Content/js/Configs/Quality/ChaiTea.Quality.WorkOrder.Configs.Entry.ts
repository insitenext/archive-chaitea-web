﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Configs.Quality {
    'use strict';

    /**
     * @description Routing configuration for work order entry.
     */
    class WorkOrderEntryConfig {
        public static $inject: string[] = [
            '$stateProvider',
            '$urlRouterProvider',
            'sitesettings'];

        constructor($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, sitesettings: ISiteSettings) {

            var templatePath: string = `${sitesettings.basePath}Content/js/Quality/Templates/`;

            // Define the parent route.
            $stateProvider.state('form', {
                url: '/form',
                templateUrl: 'quality.workorder.mainworkorder.tmpl.html',
                controller: 'Quality.WorkOrderEntryCtrl',
                resolve: {},
                controllerAs: 'vm'
            });

            // Define the child routes.
            var steps: string[] = ['step1', 'step2', 'review', 'finalize', 'submitmessage'];
            angular.forEach(steps, step => {
                $stateProvider
                    .state(`form.${step}`, {
                    url: `/${step}`,
                    templateUrl: templatePath + 'quality.workorder.' + step + '.tmpl.html'
                });
            });

            //$urlRouterProvider.otherwise('/form/step1');
        }
    } // Register the route config and initialize the routes.
    angular.module('app').config(WorkOrderEntryConfig);
    angular.module('app').run(['$rootScope', '$state', ($rootScope, $state) => {
        var hash: string = window.location.hash;

        if (hash) {
            var state: string = hash.substr(2).split('/').filter(Boolean).join('.');
            $state.go(state);
            //$state.go($state.includes(state) ? state : 'form.step1');
        }
        else {
            $state.go('form.step1', null, { reload: true });
        }
        $rootScope.$on('$stateChangeSuccess',(e, to) => {

            //console.log(e, to);
        });

    }]);
}