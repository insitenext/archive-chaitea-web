﻿/// <reference path="../../_libs.ts" />

module ChaiTea.Governance.Services {
    'use strict';

    import commonInterfaces = ChaiTea.Common.Interfaces;

    export interface IReportsSvc {
        getTurnoverReasons(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getTurnoverMonthlyTrends(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getVoluntaryVsInvoluntary(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getBusinessConductStandards(params?: commonInterfaces.IDateRange): angular.IPromise<any>;
        getLatestUpdateDate(): angular.IPromise<any>;
        
    }
    export interface IODataPagingParamModel {
        $orderby: string;
        $top: number;
        $skip: number;
        $filter: string;
    }
    export interface IEmployeeTimeClockResource extends ng.resource.IResourceClass<any> {
        getByOData(Object): ng.resource.IResource<any>;
    }

    class ReportsSvc implements angular.IServiceProvider {
        private basePath: string;

        static $inject = ['$resource', 'sitesettings'];
        /**
         * @description Constructor.
         * @param {ng.resource.IResourceService} $resource
         * @param {obj} sitesettings
         */
        constructor(
            private $resource: ng.resource.IResourceService,
            sitesettings: ISiteSettings) {

            this.basePath = sitesettings.basePath;
        }

        public $get(): IReportsSvc {
            return {
                getTurnoverReasons: (params) => {
                    return this.getTurnoverReasons(params);
                },
                getTurnoverMonthlyTrends: (params) => {
                    return this.getTurnoverMonthlyTrends(params);
                },
                getVoluntaryVsInvoluntary: (params) => {
                    return this.getVoluntaryVsInvoluntary(params);
                },
                getBusinessConductStandards: (params) => {
                    return this.getBusinessConductStandards(params);
                },
                getLatestUpdateDate: () => {
                    return this.getLatestUpdateDate();
                }
            }
        }

        /**
         * @description Complaint Reports.
         */
        public getTurnoverReasons = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'Turnover/TopTurnoverTypes/:startDate/:endDate').query(params).$promise;
        }

        public getTurnoverMonthlyTrends = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'Turnover/MonthlyTrend/:startDate/:endDate').query(params).$promise;
        }

        public getVoluntaryVsInvoluntary = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'Turnover/TurnoverByCategory/:startDate/:endDate').query(params).$promise;
        }

        
        public getBusinessConductStandards = (params?: commonInterfaces.IDateRange): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'Turnover/BusinessStandards/:startDate/:endDate').query(params).$promise;
        }

        public getLatestUpdateDate = (): angular.IPromise<any> => {
            return this.$resource(this.basePath +
                'Turnover/LastUpdatedDate').get().$promise;
        }

    }

    angular.module('app').service('chaitea.governance.services.reportssvc', ReportsSvc);
}

