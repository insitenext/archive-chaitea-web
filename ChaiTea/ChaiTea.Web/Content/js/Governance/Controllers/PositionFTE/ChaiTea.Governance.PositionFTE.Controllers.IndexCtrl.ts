﻿module ChaiTea.Governance.PositionFTE.Controllers {

    import commonInterfaces = ChaiTea.Common.Interfaces;
    import governanceInterfaces = ChaiTea.Governance.Interfaces;

    class PositionFTEIndexCtrl implements commonInterfaces.INgController {

        positionFTEData = [];
        jobHourlyRates: Array < governanceInterfaces.IJobHourlyRate > = [];
        highestRateOutOfAllPrograms = 0;
        highestMonthlyCostOutOfAllPrograms = 0;
        sectionHeight: number = 0;
        noOfMonths: number = 0;
        isProgramSelect: boolean = false;
        positionData: any;
        totalHeadCount: number = 0;
        totalHours: number = 0;
        avgRate: number = 0;
        monthlyCost: number = 0;
        HourlyRateRadius: number = 0;
        Radius: number = 0;
        overlayRange: number = 0;
        selectedProgram: any;
        isNotDesktop: boolean = false;
        isMedium: boolean = false;
        isIpad: boolean = false;
        fromDateChange: boolean = false;
        static $inject = [
            '$scope',
            '$log',
            'blockUI',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.utilssvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.common.services.localdatastoresvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private blockUI,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private utilitySvc: Common.Services.IUtilitySvc,
            private messageBusSvc: Core.Services.IMessageBusSvc,
            private dateFormatSvc: Core.Services.IDateFormatterSvc,
            private localStorageSvc: ChaiTea.Common.Services.ILocalDataStoreSvc) {

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage(commonInterfaces.MessageBusMessages.HeaderDateChange, this.$scope, (event, obj) => {
                this.noOfMonths = Math.abs(moment(obj.startDate).diff(moment(obj.endDate), 'months')) + 1;
                this.getPositionFTEData({
                    startDate: this.dateFormatSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatSvc.formatDateShort(obj.endDate)
                });
            });
            this.isNotDesktop = (this.sitesettings.windowSizes.isPhone || (this.sitesettings.windowSizes.isNotDesktop));
            this.isMedium = this.sitesettings.windowSizes.isMedium;
            this.isIpad = this.sitesettings.windowSizes.isTablet && !this.sitesettings.windowSizes.isLargeDesktop;

        }

        public initialize = (): void => {}

        private getPositionFTEData = (dateRange: commonInterfaces.IDateRange): void => {

            this.apiSvc.get('JobHourlyRate').then((data: Array < governanceInterfaces.IJobHourlyRate > ) => {
                this.jobHourlyRates = data;

                this.apiSvc.query(dateRange, 'FinanceReport/PositionFTE/:startDate/:endDate').then((result: Array < governanceInterfaces.IPositionFTETrendItem > ) => {

                    angular.forEach(result, (position) => {
                        var jobHoulyrate = < governanceInterfaces.IJobHourlyRate > {};

                        var hasProgramId = _.some(this.jobHourlyRates, {
                            'JobId': position.JobId,
                            'ProgramId': position.ProgramId
                        });

                        if (hasProgramId) {

                            jobHoulyrate = _.find(this.jobHourlyRates, {
                                JobId: position.JobId,
                                'ProgramId': position.ProgramId
                            });

                            if (!this.userContext.Site.ID && jobHoulyrate) {
                                var jobRatesForAllSites = _.filter(this.jobHourlyRates, {
                                    JobId: position.JobId,
                                    'ProgramId': position.ProgramId
                                });
                                jobHoulyrate.HourlyRate = _.sum(_.pluck(jobRatesForAllSites, "HourlyRate")) / jobRatesForAllSites.length;
                            }
                        } else {

                            jobHoulyrate = _.find(this.jobHourlyRates, {
                                JobId: position.JobId
                            });

                            if (!this.userContext.Site.ID && jobHoulyrate) {
                                var jobRatesForAllSites = _.filter(this.jobHourlyRates, {
                                    JobId: position.JobId
                                });
                                jobHoulyrate.HourlyRate = _.sum(_.pluck(jobRatesForAllSites, "HourlyRate")) / jobRatesForAllSites.length;
                            }

                        }

                        _.defaults(position, jobHoulyrate, {
                            HourlyRate: null
                        });
                    });

                    angular.forEach(result, (position) => {
                        position.MonthlyCost = parseFloat((position.HourlyRate * position.TotalHours).toFixed(2));
                    });


                    //setting max and min radius for Avg Monthly Cost bubbles 
                    var maxRadius = 150; //size for highest monthly cost bubvle
                    var programMaxRadius = 165;//just to make sure bubble size fits in screen
                    var allProgramMaxRadius = 185;
                    var programMinRadius = 5;
                    var positionMinRadius = 5;//position min radius equal to maxAVgRateRadius for overlapping

                    //setting max and min radius for Avg Hourly Rate bubbles 
                    var maxAvgRateRadius = 100;
                    var programAvgRateMinRadius = 5;
                    var positionAvgRateMinRadius = 5;

                    if (this.sitesettings.windowSizes.isMedium) {
                        maxRadius = 120
                        programMaxRadius = 150;
                        programMinRadius = 5;
                    }
                    else if (this.isNotDesktop) {
                        maxRadius = 70
                        programMaxRadius = 80;
                        allProgramMaxRadius = 100;
                        programMinRadius = 5;
                    }

                    var totalMonthlyHoursOfAllPrograms = _.sum(_.pluck(result, "TotalHours"));
                    var totalMonthlyCostOfAllPrograms = _.sum(_.pluck(result, "MonthlyCost"));

                    if (this.jobHourlyRates.length && result.length > 0) {
                        this.highestRateOutOfAllPrograms = _.max(_.pluck(result, "HourlyRate"));
                        this.highestMonthlyCostOutOfAllPrograms = _.sortByOrder(result, ['MonthlyCost'], ['desc'])[0].MonthlyCost;
                    }

                    this.positionFTEData = _.chain(result)
                        .groupBy('ProgramId')
                        .map((value, key) => {

                            var totalHours = 0; // _.sum(_.pluck(value, "TotalHours"));
                            var hourlyRates = [];
                            angular.forEach(value, (v) => {
                                if (v.HourlyRate) {
                                    hourlyRates.push(v.HourlyRate);
                                    totalHours = totalHours + v.TotalHours;
                                }
                            });
                            var avgHourlyRate = hourlyRates.length ? (_.sum(hourlyRates) / hourlyRates.length) : 0;
                            var averageMonthlyCost = (totalHours * avgHourlyRate);
                            var totalMonthlyCostOfProgram = _.sum(_.pluck(value, "MonthlyCost"));

                            var programRadius = 0;
                            var programHourlyRateRadius = 0;

                            if (this.highestMonthlyCostOutOfAllPrograms) {
                                programRadius = (averageMonthlyCost * maxRadius) / this.highestMonthlyCostOutOfAllPrograms;
                            }

                            programRadius = (programRadius < programMinRadius) ? programMinRadius : programRadius;
                            programRadius = (programRadius > programMaxRadius) ? programMaxRadius : programRadius;

                            if (this.highestRateOutOfAllPrograms) {
                                programHourlyRateRadius = (avgHourlyRate * maxAvgRateRadius) / this.highestRateOutOfAllPrograms;
                            }
                            programHourlyRateRadius = (programHourlyRateRadius < programAvgRateMinRadius) ? programAvgRateMinRadius : programHourlyRateRadius;

                            var jobs = _.sortByOrder(value, ['EmployeeCount'], ['desc'])


                            angular.forEach(jobs, (job: any) => {
                                job.Radius = ((maxRadius * job.MonthlyCost) / this.highestMonthlyCostOutOfAllPrograms);
                                job.Radius = (job.Radius < positionMinRadius) ? positionMinRadius : job.Radius;
                                job.MonthlyCost = parseFloat(job.MonthlyCost.toFixed(2));

                                job.HourlyRateRadius = (maxAvgRateRadius * job.HourlyRate) / this.highestRateOutOfAllPrograms;
                                job.HourlyRateRadius = (job.HourlyRateRadius < positionAvgRateMinRadius) ? positionAvgRateMinRadius : job.HourlyRateRadius;
                                job.HourlyRate = job.HourlyRate ? parseFloat(job.HourlyRate.toFixed(2)) : job.HourlyRate;
                                job.TotalPercentage = ((job.MonthlyCost / totalMonthlyCostOfProgram)* 100).toFixed(0);
                            });


                            return {
                                Id: key,
                                Name: value[0].ProgramName,
                                EmployeeCount: _.sum(_.pluck(value, "EmployeeCount")),
                                TotalHours: parseFloat(totalHours.toFixed(2)),
                                AverageHourlyRate: parseFloat(avgHourlyRate.toFixed(2)),
                                AverageMonthlyCost: parseFloat(averageMonthlyCost.toFixed(2)),
                                HourlyRateRadius: programHourlyRateRadius,
                                Radius: programRadius,
                                TotalPercentage: ((totalMonthlyCostOfProgram / totalMonthlyCostOfAllPrograms) * 100).toFixed(0),
                                Jobs: jobs,
                            };
                        }).value();

                    this.overlayRange = 7 / (this.positionFTEData.length * 10);
                    angular.forEach(this.positionFTEData, (data,index) => {

                        var monthlyCostzindex = 0;
                        var hourlyRatezindex = 0;
                        var monthlyCosts = _.pluck(_.sortByOrder(data.Jobs, ['MonthlyCost'], ['desc']), "MonthlyCost");
                        angular.forEach(monthlyCosts, (cost) => {
                            var index = _.findIndex(data.Jobs, (o: any) => {
                                return o.MonthlyCost == cost;
                            });
                            data.Jobs[index].monthlyCostzindex = monthlyCostzindex + 1;
                            monthlyCostzindex++;
                        });

                        var hourlyRates = _.pluck(_.sortByOrder(data.Jobs, ['HourlyRate'], ['desc']), "HourlyRate");
                        angular.forEach(hourlyRates, (rate) => {
                            var index = _.findIndex(data.Jobs, (o: any) => {
                                return o.HourlyRate == rate;
                            });
                            data.Jobs[index].hourlyRatezindex = hourlyRatezindex + 1;
                            hourlyRatezindex++;
                        });

                        data.Jobs = _.sortByOrder(data.Jobs, ['MonthlyCost'], ['desc']);
                    });
                    this.totalHeadCount = this.positionFTEData.reduce(((sum, item) => {
                        return sum + item.EmployeeCount;
                    }), 0);
                    this.totalHours = this.positionFTEData.reduce(((sum, item) => {
                        return sum + item.TotalHours;
                    }), 0);
                    var avgHourlyRates = [];
                    angular.forEach(this.positionFTEData, (v) => {
                        if (v.AverageHourlyRate) {
                            avgHourlyRates.push(v.AverageHourlyRate);
                        }
                    });
                    this.avgRate = _.sum(avgHourlyRates) / avgHourlyRates.length;
                    this.monthlyCost = this.positionFTEData.reduce(((sum, item) => {
                        return sum + item.AverageMonthlyCost;
                    }), 0)
                    if (this.highestRateOutOfAllPrograms) {
                        this.HourlyRateRadius = (this.avgRate * maxAvgRateRadius) / this.highestRateOutOfAllPrograms;
                    }
                    this.HourlyRateRadius = (this.HourlyRateRadius < programAvgRateMinRadius) ? programAvgRateMinRadius : this.HourlyRateRadius;
                    if (this.highestMonthlyCostOutOfAllPrograms) {
                        this.Radius = (this.monthlyCost * maxRadius) / this.highestMonthlyCostOutOfAllPrograms;
                    }

                    this.Radius = (this.Radius < programMinRadius) ? programMinRadius : this.Radius;
                    this.Radius = (this.Radius > allProgramMaxRadius) ? allProgramMaxRadius : this.Radius;
                    var biggestSection = _.max(this.positionFTEData, _.property('Radius'));
                    this.sectionHeight = biggestSection.Radius;
                    this.positionFTEData = _.sortByOrder(this.positionFTEData, ['AverageMonthlyCost'], ['desc'])
                    var lightness = 41;
                    angular.forEach(this.positionFTEData, (data, index) => {
                        lightness -= 5;
                        data.OverlayColor = 'hsl(190,100%,' + lightness + '%)';
                    });
                    if (this.userContext.Program.ID) {
                        this.selectedProgram = this.positionFTEData[0];
                        this.positionData = this.positionFTEData[0];
                    }
                    else if (this.isProgramSelect) {
                        this.fromDateChange = true;
                        this.selectedProgram = _.find(this.positionFTEData, (data: any) => { return data.Id == this.selectedProgram.Id });
                        this.selectProgram(this.selectedProgram);
                    }
                    else {
                        this.selectedProgram = null;
                    }
                    
                }, this.onFailure);
            }, this.onFailure);
        }

        public getNumber = (type: string, object: any) => {
            if (object) {
            if (object.Radius > 0 || object.HourlyRateRadius > 0) {
                var value = object.Radius > object.HourlyRateRadius ? object.Radius : object.HourlyRateRadius;
                var returnVal = parseInt(value.toString(), 10);
                return returnVal > 20 ? returnVal : 20;
            }
            else {
                return 20;
            }
            }
        };

        public getArrayForLooping = (count: number) => {
            return new Array(count);
        }

        private onFailure = (response: any): void => {
            this.$log.error(response);
        }

        public selectProgram = (program: any) => {
         
            if (!this.userContext.Program.ID) {
                if (!this.fromDateChange) {
                    this.isProgramSelect = !this.isProgramSelect;
                }
                if (this.isProgramSelect) {
                    this.selectedProgram = program;
                    var data = _.filter(this.positionFTEData, { 'Id': program.Id });
                    this.positionData = data[0];
                }
                else {
                    this.selectedProgram = null;
                }
            }

            this.fromDateChange = false;
        }
    }

    angular.module('app').controller('Governance.PositionFTEIndexCtrl', PositionFTEIndexCtrl);
}