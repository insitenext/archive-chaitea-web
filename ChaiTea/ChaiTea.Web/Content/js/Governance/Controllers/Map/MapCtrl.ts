﻿/// <reference path="../../../_libs.ts" />


module ChaiTea.Gevernance.Controllers {
    'use strict';

    enum Tabs {
        Billing,
        //Hours,
        Supplies,
        Complaints,
        Audits,
        ReportIts,
        Surveys,
        Turnover,
        Safety
    }

    interface ITabItem {
        name: string;
        markers: Array<any>;
        icon: string;
        endpoint: string;
        active?: boolean;
        showLegend?: boolean;
        toggles?: IToggles[];
        activeToggle?: number;
        gridEndpoint: string;
    }

    interface IToggles {
        text: string;
        endpoint: string;
    }

    class MapIndexCtrl {

        locations: Array<ChaiTea.Common.Interfaces.IMapLocation> = [];
        style: any = [];
        windowSizes: any = {};
        refresh: boolean = false;
        turnoverGoal: number = 36;

        tabs: Array<ITabItem> = [
            {
                name: "Billing",
                markers: [],
                icon: 'fa-bonus',
                endpoint: 'FinanceReport/BillingBySiteMonthly/',
                gridEndpoint: 'FinanceReport/BillingBySiteMonthly/:startDate/:endDate'
            },
            {
                name: "Supplies",
                markers: [],
                icon: 'fa-dpt-procurement',
                endpoint: 'POItem/SuppliesAmountBySite/',
                activeToggle: 0,
                toggles: [
                    {
                        text: 'All',
                        endpoint: ''
                    },
                    {
                        text: 'Green',
                        endpoint: ''
                    },
                    {
                        text: 'Billable',
                        endpoint: ''
                    }
                ],
				gridEndpoint: 'FinanceReport/SpendBySiteMonthly/:startDate/:endDate'
            },
            {
                name: "Complaints",
                markers: [],
                icon: 'fa-proactive',
                endpoint: 'Complaints/SiteCounts/',
                showLegend: true,
                gridEndpoint: 'Complaints/ComplaintsCountBySiteByMonth/:startDate/:endDate'
            },
            {
                name: "Audits",
                markers: [],
                icon: 'fa-quality',
                endpoint: 'Report/AuditTrendsBySite/',
                showLegend: true,
                activeToggle: 0,
                toggles: [
                    {
                        text: 'Internal site score',
                        endpoint: 'Report/AuditTrendsBySite/'
                    },
                    {
                        text: 'Joint site score',
                        endpoint: ''
                    }
                ],
				gridEndpoint: 'Report/AuditGridByProgram/:startDate/:endDate/:programId'
            },
            {
                name: "Report Its",
                markers: [],
                icon: 'fa-proactive',
                endpoint: 'Ownership/SiteCounts/',
				gridEndpoint: 'Ownership/OwnershipCountBySiteByMonth/:startDate/:endDate'
            },
            {
                name: "Surveys",
                markers: [],
                icon: 'fa-survey',
                endpoint: 'Surveys/SurveyTrendsBySite',                
                showLegend: true,
                gridEndpoint: 'Surveys/SurveyAveragesBySiteByMonth'
            },
            {
                name: "Turnover",
                markers: [],
                icon: 'fa-re-use',
                endpoint: 'Turnover/TurnoversBySite/',                
                gridEndpoint: 'Turnover/TurnoverAveragesBySiteByMonth/:startDate/:endDate'
            },
            {
                name: "Safety",
                markers: [],
                icon: 'fa-training-due',
                endpoint: 'Personnel/Safety/SiteCounts/',
                showLegend: true,
				gridEndpoint: 'Personnel/Safety/SafetyCountBySiteByMonth/:startDate/:endDate'
            }
            
        ];
        startDate: string = '';
        endDate: string = '';
        currentTab: number;
        monthsRangeList = [];
        gridData = [];
        programs = [];
        totalTitle: string = 'Total';
        tabEnum: Tabs;
        isAudits: boolean = false;
        isSurveys: boolean = false;
        staticTabs = Tabs;
        markerData = [];
        isMarkerClicked: boolean = false;
        clickedMarkerTitle: string = '';
        hovered: boolean = false;
        static $inject = [
            '$scope',
            '$log',
            '$q',
            'blockUI',
            'sitesettings',
            'userInfo',
            'userContext',
            'chaitea.core.services.dateformattersvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.common.services.apibasesvc',
            'chaitea.common.services.mapssvc',
            'chaitea.common.services.notificationsvc',
            'chaitea.common.services.imagesvc',
            'chaitea.common.services.reportsvc',
            '$timeout',
            'chaitea.common.services.localdatastoresvc'
        ];

        constructor(
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $q: ng.IQService,
            private blockUI,
            private sitesettings: ISiteSettings,
            private userInfo: IUserInfo,
            private userContext: IUserContext,
            private dateFormatSvc: ChaiTea.Core.Services.IDateFormatterSvc,
            private messageBusSvc: ChaiTea.Core.Services.IMessageBusSvc,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private mapSvc: ChaiTea.Common.Services.IMapsSvc,
            private notificationSvc: ChaiTea.Common.Services.INotificationSvc,
            private imageSvc: ChaiTea.Common.Services.IImageSvc,
            private reportSvc: ChaiTea.Common.Services.IReportSvc,
            private $timeout: angular.ITimeoutService,
            private localDataStoreSvc: ChaiTea.Common.Services.ILocalDataStoreSvc) {

            this.currentTab = this.localDataStoreSvc.getObject(Common.Enums.LOCAL_STORAGE_KEYS.GOVERNANCE_MAP_CURRENT_TAB.toString());            
            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage(Common.Interfaces.MessageBusMessages.HeaderDateChange, this.$scope, (event, obj) => {
            
                //clear cached markers
                angular.forEach(this.tabs, function (tab) {
                    tab.markers = [];
                });

                this.startDate = this.dateFormatSvc.formatDateShort(moment(obj.startDate));
                this.endDate = this.dateFormatSvc.formatDateShort(moment(obj.endDate).add(1, 'd'));

                this.getMapData(userContext, this.startDate, this.endDate);
                this.setMonthsRange();
                if (this.currentTab == Tabs.Audits) {
                    this.isAudits = true;
                    this.getPrograms();
                }
                else {
                    this.isAudits = false;
                    this.isSurveys = (this.currentTab == Tabs.Surveys) ? true : false;
                    this.getGridData();
                }
            });

        }

        public initialize = (): void => {

            this.windowSizes = this.sitesettings.windowSizes;

            //override map settings
            this.style = this.mapSvc.getStyles().Insite;

            this.setInitialActiveTab();
        }

        public tabChange = (tabIndex?: number): void => {
            if (tabIndex === undefined) {
                tabIndex = this.currentTab;
            }

            this.localDataStoreSvc.setObject(Common.Enums.LOCAL_STORAGE_KEYS.GOVERNANCE_MAP_CURRENT_TAB.toString(), tabIndex);
            this.currentTab = tabIndex;

            this.buildMarkers(tabIndex, this.userContext, this.startDate, this.endDate);
            if (this.currentTab == Tabs.Audits) {
                this.isAudits = true;
                this.getPrograms();
            }
            else {
                this.isAudits = false;
                this.isSurveys = (this.currentTab == Tabs.Surveys) ? true : false;
                this.getGridData();
            }
        }

        public toggleChange = (toggleIndex?: number): void => {
            this.buildMarkers(this.currentTab, this.userContext, this.startDate, this.endDate);
        }

        public setInitialActiveTab = (): void => {
            var savedIndex = this.localDataStoreSvc.getObject(Common.Enums.LOCAL_STORAGE_KEYS.GOVERNANCE_MAP_CURRENT_TAB.toString());
            if (savedIndex) {
                this.currentTab = savedIndex;
            }
            else {
                this.currentTab = 0;
            }
        }

        /**
         * @description Gets data and adds markers to map
         */
        private getMapData = (userContext: IUserContext, startDate: string, endDate: string): void => {

            async.parallel({
                //get locations
                orgs: (callback) => {
                    this.getOrgLocations(userContext.Client.ID, userContext.Site.ID).then((orgs: ChaiTea.Common.Interfaces.IOrg) => {
                        callback(null, orgs);
                    }, (err) => { callback(err, null); });
                },
                //get report settings
                setting: (callback) => {
                    if (this.userContext.Site.ID) {
                        this.reportSvc.getReportSettings().then((setting: ChaiTea.Common.Interfaces.IReportSettings) => {
                            callback(null, setting);
                        }, (err) => { callback(err, null); });
                    } else {
                        this.reportSvc.getAllSitesCombinedSiteReportSettings().then((setting: ChaiTea.Common.Interfaces.IReportSettings) => {
                            callback(null, setting);
                        }, (err) => { callback(err, null); });
                    }
                }
            }, (err: any, res: any) => {

                if (err) {
                    this.onFailure(err);
                } else {
                    //save locations
                    if (userContext.Site.ID > 0) {
                        if (res.orgs.Latitude && res.orgs.Longitude) {
                            this.locations.push({
                                latitude: res.orgs.Latitude,
                                longitude: res.orgs.Longitude,
                                name: userContext.Site.Name,
                                control: res.setting.ScoringProfileGoal,
                                siteId: res.orgs.Id,
                                complaintControlLimit: res.setting.ComplaintControlLimit
                            });
                        }
                    } else {
                        angular.forEach(res.orgs, (org) => {
                            if (org.Latitude && org.Longitude) {
                                this.locations.push({
                                    latitude: org.Latitude,
                                    longitude: org.Longitude,
                                    name: org.Name,
                                    control: res.setting.ScoringProfileGoal,
                                    siteId: org.Id,
                                    complaintControlLimit: res.setting.ComplaintControlLimit
                                });
                            }
                        });
                    }

                    if (this.locations.length == 0) {
                        this.notificationSvc.infoToastMessage('No markers to display. Make sure you have location data set on all sites');
                    } else if (this.locations.length > 0) {
                        //load first tab
                        this.buildMarkers(this.currentTab, userContext, startDate, endDate);
                    }

                }

            });

        }

        /**
         * @description Loop locations and get marker data
         */
        private buildMarkers = (tabIndex, userContext, startDate, endDate) => {
            if (this.locations.length) {
                // return if is NOT toggle and markers are empty.
                if (!this.tabs[tabIndex].toggles && this.tabs[tabIndex].markers.length) {
                    return;
                }

                // get top number of location
                // sort by highest to lowest
                var max = 3;
                var min = 0.5;
                var markers = [];

                this.getMarkerData(tabIndex, startDate, endDate).then((markerData) => {                    
                    angular.forEach(this.locations, (location, key) => {
                        var windowBuild = this.buildWindow(tabIndex, userContext, location.siteId, location.name, location.control, markerData, location.complaintControlLimit);
                        var marker = this.addMarker(tabIndex, 0, location.latitude, location.longitude, windowBuild.metricLabel, windowBuild.passing, windowBuild, windowBuild.customColor);
                        markers.push(
                            marker
                        );

                        // wait for last item to be added
                        if ((key + 1) === this.locations.length) {                            
                            // Dynamic resizing of bubbles
                            if (tabIndex != Tabs.Surveys && tabIndex != Tabs.Audits) {
                                this.calculateMarkerSize(markers);
                            }
                            this.tabs[tabIndex].markers = markers;
                        }
                    });
                    this.tabs[tabIndex].markers.sort(function (a, b) {
                        return a.window.metric - b.window.metric
                    });
                    var total = this.tabs[tabIndex].markers.length;
                    angular.forEach(this.tabs[tabIndex].markers, (marker, key) => {
                        marker.options.zIndex = total;
                        total -= 1;
                    });
                });


            }

        }

        /**
         * @description Gets location data for markers
         */
        private getOrgLocations = (clientId: number, siteId: number): angular.IPromise<any> => {
            var def = this.$q.defer();
            var endpoint: string = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Site] + '/UserSites/' + clientId;
            var isArray: boolean = true;

            if (siteId > 0) {
                endpoint = ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints[ChaiTea.Common.ApiServices.Interfaces.ApiEndpoints.Site] + '/' + siteId;
                isArray = false;
            }

            this.apiSvc.query({}, endpoint, false, isArray).then((res) => {
                def.resolve(res);
            }, (err) => {
                def.reject(err);
            });

            return def.promise;
        }

        /**
         * @description Queries data needed for marker
         */
        private getMarkerData = (tabIndex: number, startDate: string, endDate: string, isToggle = false): angular.IPromise<any> => {

            var def = this.$q.defer();
            var params = {};
            var endpoint = this.tabs[tabIndex].endpoint;
            if (tabIndex == Tabs.Surveys) {
                params = {
                    startDate: this.startDate,
                    endDate: this.endDate
                };
            }
            else {
                endpoint += startDate + '/' + endDate;
            }

            // Get endpoint from toggles if none top level
            if (!this.tabs[tabIndex].endpoint) {
                endpoint = this.tabs[tabIndex].toggles[this.tabs[tabIndex].activeToggle].endpoint + startDate + '/' + endDate;
            }

            if (tabIndex == Tabs.Audits || tabIndex == Tabs.Surveys || tabIndex == Tabs.Turnover) {
                this.apiSvc.query(params, endpoint, false, false).then((res) => {
                    def.resolve(res);
                });
            } else {
                this.apiSvc.query(params, endpoint, false, true).then((res) => {
                    this.markerData = res;
                    def.resolve(res);
                });
            }

            return def.promise;

        }


        /**
         * @description Adds marker to map
         */
        private addMarker = (tabIndex: number,
            id: number,
            latitude: number,
            longitude: number,
            label: string,
            passing: boolean,
            window?: ChaiTea.Common.Interfaces.IMapMarkerWindow, customColor?: string): any => {

            var icon: ChaiTea.Common.Interfaces.IMapSVGIcon = {
                path: 'M-12,0a12,12 0 1,0 24,0a12,12 0 1,0 -24,0',
                fillColor: (passing ? this.sitesettings.colors.coreColors.successLight : this.sitesettings.colors.coreColors.dangerLight),
                fillOpacity: .6,
                strokeColor: this.sitesettings.colors.coreColors.default, 
                strokeWeight: 1,
                scale: (tabIndex == Tabs.Surveys) || (tabIndex == Tabs.Audits) ? .75 : 1
            };

            if (customColor) {
                icon.fillColor = customColor;
            }

            //if label is false show grey color marker.
            if (!_.parseInt(label) && (tabIndex == Tabs.Audits || tabIndex == Tabs.Supplies || tabIndex == Tabs.ReportIts || tabIndex == Tabs.Surveys)) {
                icon.fillColor = this.sitesettings.colors.coreColors.base;
                icon.fillOpacity = .6;
                label = '';
            }

            var labelAnchor = "12 8";
            if (label.length == 1) {
                labelAnchor = "4 8";
            } else if (label.length == 2) {
                labelAnchor = "8 8";
            } else if (label.length == 3) {
                labelAnchor = "10 8";
            } else if (label.length > 4) {
                labelAnchor = "16 8";
            }

           
            var marker: ChaiTea.Common.Interfaces.IMapMarker = {
                id: id,
                coords: {
                    latitude: latitude,
                    longitude: longitude
                },
                options: {
                    draggable: false,
                    icon: icon,
                    labelContent: '',
                    labelAnchor: labelAnchor,
                    labelClass: "map-label", // the CSS class for the label
                    labelInBackground: false
                },
                events: {
                    mouseover: (marker, eventName, model, args) => {
                        angular.forEach(this.tabs[this.currentTab].markers, (m) => {
                            m.window.show = false;
                            m.options.icon.strokeColor = this.sitesettings.colors.coreColors.default;
                        });
                        //if(this.isMarkerClicked && this.clickedMarkerTitle != window.title){
                        //    this.isMarkerClicked = false;
                        //}
                        window.show = true;
                        marker.model.options.icon.strokeColor = (passing ? this.sitesettings.colors.coreColors.successDark : this.sitesettings.colors.coreColors.dangerDark);
                        if (customColor) {
                            icon.strokeColor = this.sitesettings.colors.secondaryColors.orangeDark;
                        }
                        if (!_.parseInt(label) && (tabIndex == Tabs.Audits || tabIndex == Tabs.Supplies || tabIndex == Tabs.ReportIts || tabIndex == Tabs.Surveys || tabIndex == Tabs.Billing)) {
                            icon.strokeColor = this.sitesettings.colors.coreColors.baseDark;
                        }
                        this.hovered = true;
                        this.$scope.$apply();
                    },
                    //mouseout: (marker, eventName, model, args) => {
                    //    if (!this.isMarkerClicked) {
                    //        this.$timeout(() => {
                    //            if (!this.hovered) {
                    //                window.show = false;
                    //                this.$scope.$apply();
                    //                console.log("out");
                    //            }
                    //            this.hovered = false;
                    //        }, 0);
                    //    }
                    //},
                    //click: (marker, eventName, model, args) => {
                    //    angular.forEach(this.tabs[this.currentTab].markers, (m) => {
                    //        m.window.show = false;
                    //    });
                    //    this.clickedMarkerTitle = window.title;
                    //    window.show = true;
                    //    this.isMarkerClicked = true;
                    //    this.$scope.$apply();
                    //}
                }
            };

            if (window) {
                marker.window = window;
            }

            return marker;
        };

        /**
         * @description Get the data needed for the marker window
         */
        private buildWindow = (
            tabIndex: number,
            userContext: IUserContext,
            siteId: number,
            siteName: string,
            control: number,
            data: any,
            complaintControlLimit: number): ChaiTea.Common.Interfaces.IMapMarkerWindow => {

            var windowData: ChaiTea.Common.Interfaces.IMapMarkerWindow = {
                title: siteName,
                metric: 0,
                metricLabel: '',
                data: {},
                link: '',
                template: '',
                class: 'pass',
                passing: true,
                options: {
                    maxWidth: 214,
                    disableAutoPan: false,
                    pixelOffset: new google.maps.Size(0, 20)
                },
                customColor: '',
                show: false
            };

            var templateData: any = {};

            //Supplies
            if (tabIndex == Tabs.Supplies) {
                templateData = {
                    metric: 0,
                    metricShort: '',
                    text: "",
                    footerText: "Supply Spend",
                    max: control,
                    metricLabel: ''
                };

                var index = 0;        
                if (userContext.Site.ID == 0) {
                    var index = _.findIndex(data, function (metric: any) {
                        return metric.SiteId == siteId;
                    });
                }

                if (data[index]) {

                    // All toggle 
                    templateData.metric = (data[index] ? numeral(data[index].TotalAmount).format('$0,0.00') : 0);
                    templateData.metricShort = data[index] ? data[index].TotalAmount / 1000 : 0;
                    templateData.metricLabel = angular.uppercase(numeral(templateData.metricShort * 1000).format('0a'));
                    // Green toggle
                    if (this.tabs[tabIndex].activeToggle == 1) {

                        templateData.metric = (data[index] ? numeral(data[index].IsGreenAmount).format('$0,0.00') : 0);
                        templateData.metricShort = data[index] ? data[index].IsGreenAmount / 1000 : 0;
                        templateData.metricLabel = angular.uppercase(numeral(templateData.metricShort * 1000).format('0a'));
                    }
                    //Billable toggle
                    if (this.tabs[tabIndex].activeToggle == 2 && !data[index].IsBillableAmount) {

                        templateData.metric = (data[index] ? numeral(data[index].IsBillableAmount).format('$0,0.00') : 0);
                        templateData.metricShort = data[index] ? data[index].IsBillableAmount / 1000 : 0;
                        templateData.metricLabel = angular.uppercase(numeral(templateData.metricShort * 1000).format('0a'));


                    } 
                }

                windowData.customColor = data[index] ? this.sitesettings.colors.coreColors.warningLight : this.sitesettings.colors.coreColors.base;
                windowData.data = templateData;
                windowData.metric = templateData.metricShort;
                windowData.metricLabel = templateData.metricLabel;
                windowData.link = null;
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';
                windowData.class = "supplies";
            }

            //audits
            if (tabIndex == Tabs.Audits) {

                templateData = {
                    averageInternal: 0,
                    averageInternalCount:0,
                    averageJoint: 0,
                    averageJointCount: 0,
                    averageInternalText: 'Average Internal Site Score',
                    averageJointText: 'Average Joint Site Score',
                    max: control,
                    isInternalToggle: (this.tabs[this.currentTab].activeToggle === 0)
                };

                if (userContext.Site.ID > 0) {
                    //get average from all the buildings
                    angular.forEach(data.Categories, (cat, key) => {
                        if (cat != 'All Buildings') {
                            if (data.InternalData[key]) {
                                templateData.averageInternal += data.InternalData[key];
                                templateData.averageInternalCount++;
                            }
                            if (data.JointData[key]) {
                                templateData.averageJoint += data.JointData[key];
                                templateData.averageJointCount++;
                            }
                        }
                    });
                    // Internal site score
                    if (templateData.isInternalToggle) {
                        templateData.averageInternal = _.round((templateData.averageInternal / templateData.averageInternalCount), 2);
                    }
                    // Joint site score
                    if (!templateData.isInternalToggle) {
                        templateData.averageJoint = _.round((templateData.averageJoint / templateData.averageJointCount), 2);
                    }
                } else {
                    //find site by name
                    var index = _.findIndex(data.Categories, function (cat) {
                        return cat == siteName;
                    });
                    // Internal site score
                    if (templateData.isInternalToggle) {
                        templateData.averageInternal = (data.InternalData[index] ? data.InternalData[index] : 0);
                    }
                    // Joint site score
                    if (!templateData.isInternalToggle) {
                        templateData.averageJoint = (data.JointData[index] ? data.JointData[index] : 0);
                    }
                }

                var dataToRound = (templateData.isInternalToggle) ? templateData.averageInternal : templateData.averageJoint;
                
                windowData.data = templateData;
                windowData.metric = _.round(dataToRound, 2);
                windowData.metricLabel = windowData.metric.toString();
                windowData.link = this.sitesettings.basePath + 'Quality/Audit';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/audit.window.tmpl.html';

                //check for pass fail
                if (windowData.metric < control) {
                    windowData.class = "fail";
                    windowData.passing = false;
                }

                windowData.class = windowData.class + " audits";

            }

            //turnover
            if (tabIndex == Tabs.Turnover) {
                templateData = {
                    metric: 0,
                    metricShort: '',
                    text: "Turnover Rate",
                    footerText: "Goal under " + this.turnoverGoal + "%",
                    max: control
                };                           

                var index = 0;
                if (userContext.Site.ID == 0) {
                    var index = _.findIndex(data.Categories, function (metric: any) {
                        return metric == siteName;
                    });
                }

                if (data.Averages[index]) {
                    templateData.metric = (data.Averages[index] ? numeral(data.Averages[index] / 100).format('0.00%') : '0.00%');
                    templateData.metricShort = data.Averages[index] ? data.Averages[index] : 0.00;
                } else {
                    templateData.metric = '0.00%';
                    templateData.metricShort = 0.00;
                }             
                
                windowData.data = templateData;
                windowData.metric = templateData.metricShort;
                windowData.metricLabel = templateData.metric ? numeral(windowData.metric / 100).format('0.00%') : '0.00%';                
                windowData.link = this.sitesettings.basePath + 'Governance/Turnover';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';               

                // check for pass fail                
                if ((windowData.metric != 0.00) && (windowData.metric > this.turnoverGoal)) {
                    windowData.class = "fail";
                    windowData.passing = false;
                }

                windowData.class = windowData.class + " turnover";

            }

            //safety
            if (tabIndex == Tabs.Safety) {
                templateData = {
                    metric: 0,
                    text: "Incidents",
                    footerText: "Goal: Less than " + control + " Incidents",
                    max: control
                };

                if (userContext.Site.ID > 0 && data.length > 0) {
                    templateData.metric = data[0].Count;
                } else {
                    var index = _.findIndex(data, function (metric: any) {
                        return metric.SiteId == siteId;
                    });
                    templateData.metric = data[index] ? data[index].Count : 0;
                }

                windowData.data = templateData;
                windowData.metric = templateData.metric;
                windowData.metricLabel = windowData.metric.toString();
                windowData.link = this.sitesettings.basePath + 'Personnel/Safety';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';

                if (windowData.metric >= control) {
                    windowData.class = "fail";
                    windowData.passing = false;
                }

                windowData.class = windowData.class + " safety";

            }

            //complaints
            if (tabIndex == Tabs.Complaints) {
                templateData = {
                    metric: 0,
                    text: "Total Complaints",
                    footerText: "Goal: Less than " + complaintControlLimit + " per month",
                    max: complaintControlLimit
                };

                if (userContext.Site.ID > 0 && data.length > 0) {
                    templateData.metric = data[0].Count;
                } else {
                    var index = _.findIndex(data, function (metric: any) {
                        return metric.SiteId == siteId;
                    });
                    templateData.metric = data[index] ? data[index].Count : 0;
                }

                windowData.data = templateData;
                windowData.metric = templateData.metric;
                windowData.metricLabel = windowData.metric.toString();
                windowData.link = this.sitesettings.basePath + 'Quality/Complaint/Index';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';

                if (windowData.metric > complaintControlLimit) {
                    windowData.class = "fail";
                    windowData.passing = false;
                }

                windowData.class = windowData.class + " complaints";

            }

            //report its
            if (tabIndex == Tabs.ReportIts) {
                var siteData = _.filter(this.markerData, { 'SiteId': siteId });
                if (siteData.length > 0) {
                    var acceptanceRate: number = siteData[0].AcceptanceRate;
                    templateData = {
                        metric: 0,
                        text: "Report Its Submitted",
                        footerText: acceptanceRate.toFixed(0) + "% Acceptance Rate",
                        max: control
                    };

                    if (userContext.Site.ID > 0 && data.length > 0) {
                        templateData.metric = data[0].Count;
                    } else {
                        var index = _.findIndex(data, function (metric: any) {
                            return metric.SiteId == siteId;
                        });
                        templateData.metric = data[index] ? data[index].Count : 0;
                    }
                }
                else {
                    templateData = {
                        metric: 0,
                        text: "Report Its Submitted",
                        footerText: '',
                        max: control
                    };
                }
                

                windowData.customColor = siteData.length > 0 ? this.sitesettings.colors.coreColors.warningLight : this.sitesettings.colors.coreColors.base;
                windowData.data = templateData;
                windowData.metric = templateData.metric;
                windowData.metricLabel = windowData.metric.toString();
                windowData.link = this.sitesettings.basePath + 'Personnel/Ownership/Index';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';
                windowData.class = "report-its";

            }

            //billing
            if (tabIndex == Tabs.Billing) {
                templateData = {
                    metric: 0,
                    metricShort: '',
                    text: "",
                    footerText: "Current Billing",
                    max: control
                };

                var index = 0;
                if (userContext.Site.ID == 0) {
                    var index = _.findIndex(data, function (metric: any) {
                        return metric.SiteName == siteName;
                    });
                }

                if (data[index]) {
                    templateData.metric = (data[index] ? numeral(data[index].Total).format('$0,0.00') : 0);
                    templateData.metricShort = data[index] ? data[index].Total / 1000 : 0;
                    templateData.metricLabel = angular.uppercase(numeral(templateData.metricShort * 1000).format('0a'));
                }

                windowData.customColor = templateData.metricLabel ? this.sitesettings.colors.coreColors.warningLight : this.sitesettings.colors.coreColors.base;
                windowData.data = templateData;
                windowData.metric = templateData.metricShort;
                windowData.metricLabel = templateData.metricLabel ? templateData.metricLabel : '';
                windowData.link = this.sitesettings.basePath + 'Financials/BillingOverview';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/common.window.tmpl.html';
                windowData.class = "billing";
            }

            //Survey
            if (tabIndex == Tabs.Surveys) {
                templateData = {
                    count: 0,
                    metric: 0,
                    metricShort: '',
                    text: "",
                    footerText: "Average Survey Score",
                    max: control
                };

                var index = 0;
                if (userContext.Site.ID == 0) {
                    var index = _.findIndex(data.Categories, function (metric: any) {
                        return metric == siteName;
                    });
                }

                if (data.Averages[index]) {
                    templateData.metric = data.Averages[index];
                }
                else {
                    templateData.metric = null;
                }
                windowData.data = templateData;
                windowData.metric = templateData.metric;
                windowData.metricLabel = templateData.metric ? windowData.metric.toString() : '--';
                windowData.link = this.sitesettings.basePath + 'Quality/CustomerSurvey/Index';
                windowData.template = this.sitesettings.basePath + 'Content/js/Common/Templates/Map/survey.window.tmpl.html';

                //check for pass fail
                if (windowData.metric && (windowData.metric < control)) {
                    windowData.class = "fail";
                    windowData.passing = false;
                }
                else if (!windowData.metric) {
                    windowData.class = "no-data";
                }

            }

            return windowData;

        };

        private calculateMarkerSize = (markers: any[]) => {
            var unscaledNums = _.pluck(markers, ['window', ['metric']])
                .filter(item => !_.isNaN(item))            
            var maxRange = Math.max.apply(Math, unscaledNums);
            var minRange = Math.min.apply(Math, unscaledNums);

            for (var i = 0; i < unscaledNums.length; i++) {
                var unscaled = unscaledNums[i];
                var scaled = this.scaleBetween(unscaled, 0.75, 3, minRange, maxRange);

                if (_.isFinite(scaled)) {
                    // hide label if less than 1
                    if (scaled < 1) {
                        markers[i].options.labelContent = '';
                    }
                    //set new scale
                    markers[i].options.icon.scale = scaled;
                }

            }
        }

        private scaleBetween(oldValue, newMin, newMax, oldMin, oldMax) {
            return (newMax - newMin) * (oldValue - oldMin) / (oldMax - oldMin) + newMin;
        }

        /**
         * @description Handle errors.
         */
        onFailure = (response: any): void => {
            this.$log.error(response);
        };

        private setMonthsRange = (): void => {
            this.monthsRangeList = [];
            for (var i = moment(new Date(this.startDate)); i < moment(new Date(this.endDate)); i.add(1, 'month')) {
                this.monthsRangeList.push({
                    Month: i.month() + 1,
                    Year: i.year(),
                    Value: i.format('MMM \'YY')
                });
            }
        }

        private getGridData = () => {
            var params = {
                startDate: this.startDate,
                endDate: this.endDate
            };
            this.apiSvc.query(params, this.tabs[this.currentTab].gridEndpoint).then((result) => {
                if (this.currentTab == Tabs.Billing) {
                    this.gridData = _.filter(result, {'IsSiteActive' : true});
                } else {
                    this.gridData = result;
                }
                
                angular.forEach(this.gridData, (item: any, index) => {
                    if (this.currentTab == Tabs.Billing || this.currentTab == Tabs.Supplies) {
                        item.Total = item.Months ? '$' + item.Total.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '--';
                        this.fillMissingMonthsData(index, item.Months);
                    }
                    else {
                        if (this.currentTab == Tabs.Surveys) {
                            item.Total = item.CountsByMonth && item.TotalAverage != null ? item.TotalAverage.toFixed(2) : '--';                            
                            this.totalTitle = 'Total Average';
                        } else if (this.currentTab == Tabs.Turnover) {
                            item.Total = item.CountsByMonth ? numeral(item.TotalAverage.toFixed(2)/100).format('0.00%') : '0.00%';                                                        
                            this.totalTitle = 'Total Average';                            
                        }
                        this.fillMissingMonthsData(index, item.CountsByMonth)
                    }
                });
            }, this.onFailure);
        }

        private fillMissingMonthsData = (index: number, countsByMonth: any) => {
            var tempCountsByMonth = [];
            for (var i = 0; i < this.monthsRangeList.length; i++) {
                var obj: any;
                if (this.currentTab == Tabs.Billing || this.currentTab == Tabs.Supplies) {
                    obj = _.find(countsByMonth, { Month: this.monthsRangeList[i].Value });
                }
                else {
                    obj = _.find(countsByMonth, { Month: this.monthsRangeList[i].Month, Year: this.monthsRangeList[i].Year });
                }
                if (obj) {
                    if (this.currentTab == Tabs.Billing || this.currentTab == Tabs.Supplies) {
                        tempCountsByMonth.push({
                            Count: '$' + obj.Amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        });
                    }
                    else if (this.currentTab == Tabs.Surveys) {
                        tempCountsByMonth.push({
                            Count: obj.Average ? obj.Average.toFixed(2) : '--'
                        });
                    }
                    else if (this.currentTab == Tabs.Turnover) {
                        tempCountsByMonth.push({
                            Count: obj.Average > 0 ? numeral(obj.Average.toFixed(2)/100).format('0.00%') : '0.00%'
                        });
                    }
                    else {
                        tempCountsByMonth.push({
                            Count: obj.Count
                        });
                    }
                }
                else {
                    if (this.currentTab == Tabs.Billing || this.currentTab == Tabs.Supplies || this.currentTab == Tabs.Surveys || this.currentTab == Tabs.Turnover) {
                        tempCountsByMonth.push({
                            Count: '--'
                        });
                    }                  
                    else {
                        tempCountsByMonth.push({
                            Count: 0
                        });
                    }
                }
            }
            this.gridData[index].CountsByMonth = [];
            this.gridData[index].CountsByMonth = tempCountsByMonth;
        }

        private getPrograms = () => {
            this.programs = [];
            if (this.userContext.Program.ID > 0) {
                this.programs.push({ Key: this.userContext.Program.ID, Value: this.userContext.Program.Name });
            } else {
                this.apiSvc.query({}, 'LookupList/Programs', false, false).then((result) => {
                    this.programs = result.Options;
                }, this.onFailure);
            }
        }

        private getAuditsDataByProgram = ($event, program) => {
            this.getLocationsResultsByProgram(program.Key);

            program.IsToggled = !program.IsToggled;
            angular.forEach(_.reject(this.programs,
                p => (p.Key == program.Key)),
                p => { p.IsToggled = false; });
        }

        getLocationsResultsByProgram = (programId: number) => {
            var program = _.find(this.programs, { Key: programId });

            if (program.Locations) return;
            var params = {
                startDate: this.startDate,
                endDate: this.endDate,
                programId: programId
            };
            this.apiSvc.query(params, this.tabs[this.currentTab].gridEndpoint, false, false).then((result) => {
                program.Locations = result;
            }, this.onFailure);
        }

    }

    angular.module('app').controller('Governance.MapIndexCtrl', MapIndexCtrl);

}
