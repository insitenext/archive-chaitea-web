﻿/// <reference path="../../../_libs.ts" />

module ChaiTea.Governance.Turnover.Controllers {
    "use strict";

    import coreSvc = ChaiTea.Core.Services;
    import commonInterfaces = Common.Interfaces;
    import serviceInterfaces = ChaiTea.Services.Interfaces;
    import governanceSvc = ChaiTea.Governance.Services;
    import commonSvc = ChaiTea.Common.Services;

    interface IBusinessStandard {
        SiteName: string;
        I9Compliance: number;
        BackgroundCheck: number;
        DrugScreen: number;
        TurnoverPercentage: number;
		ForMonth: string;
    }

    enum TurnoverCategories {
        Voluntary,
        Involuntary
    }
   
    class TurnoverIndexCtrl {

        moment = moment;
        lastUpdatedDate: string;
		RecentMonth: string;
        dateRange = {
            startDate: moment(new Date()).subtract(5, 'months').startOf('month'),
            endDate: moment()
        };
        basePath: string;
        goalLineWidth: number = 4;
        turnoverGoal: number = 36;
		TurnoverCategory: string ;
        chartSettings: IChartSettings;
       
        siteName: string = "All Sites";
        programName: string = "All Programs";
        businessStandard: Array<IBusinessStandard> = [];
        jsonBaseFilePath = this.sitesettings.basePath + 'Content/data/turnover';
        noOfMonths: number = 0;
        valueFormatter = function (val) {
            return val.toFixed(0) + '%';
        };

        reportsConfig = {
            goalLine: 36,
            turoverReasonsLength: 0,
            turnoverMonthlyLength: 0,
            turnoverReasons: {},
            turnoverMonthlyTrend: {},
            voluntaryVsInvoluntary: {},
            voluntaryVsInvoluntaryTypes: {},
        };

        totalTurnoverCount: number = 0;
        voluntaryInvoluntaryPercent: number = 0;
        voluntaryVsInvoluntaryData = [];
        voluntaryVsInvoluntaryAggregateData = [];
        turnoverCategories = {
            voluntary: {
                data: [],
                turnOverPercentage: 0
            },
            inVoluntary: {
                data: [],
                turnOverCount: 0,
                turnOverPercentage: 0
            }
        }

        isVoluntary: boolean = false;
        isInVoluntary: boolean = false;
        chartColors = {
            topTurnoverReasonsChart: '',
            voluntaryVsInvoluntaryChart: '',
            turnoversByCateroryChart: ''
        }
        redraw: boolean = false;
        turnOverReasons = [];
        colors = [];

        static $inject = [
            '$scope',
            'sitesettings',
            'userContext',
            'chaitea.common.services.apibasesvc',
            'chaitea.core.services.messagebussvc',
            'chaitea.core.services.dateformattersvc',
            'chaitea.governance.services.reportssvc',
            'chaitea.common.services.notificationsvc',
            '$http',
            '$log',
            '$window',
            '$uibModal'
        ];

        constructor(
            private $scope: angular.IScope,
            private sitesettings: ISiteSettings,
            private userContext: IUserContext,
            private apiSvc: ChaiTea.Common.Services.IApiBaseSvc,
            private messageBusSvc: coreSvc.IMessageBusSvc,
            private dateFormatterSvc: coreSvc.IDateFormatterSvc,
            private chartsSvc: governanceSvc.IReportsSvc,
            private notificationSvc: commonSvc.INotificationSvc,
            private $http: angular.IHttpService,
            private $log: angular.ILogService,
            private $window: angular.IWindowService,
            private $uibModal: angular.ui.bootstrap.IModalService) {
            this.turnOverReasons = [
                {
                    name: 'Performance',
                    colorCode: this.sitesettings.colors.secondaryColors.limeDark
                },
                {
                    name: 'Gross Misconduct',
                    colorCode: this.sitesettings.colors.secondaryColors.lavendarDark
                },
                {
                    name: 'Safety Violation',
                    colorCode: this.sitesettings.colors.secondaryColors.orangeDark
                },
                {
                    name: 'Attendance',
                    colorCode: this.sitesettings.colors.secondaryColors.blueSteelDark
                },
                {
                    name: 'Failed Probation',
                    colorCode: this.sitesettings.colors.secondaryColors.pinkDark
                },
                {
                    name: 'Denied Site Access',
                    colorCode: this.sitesettings.colors.secondaryColors.tealDark
                },
                {
                    name: 'Other',
                    description: '',
                    colorCode: this.sitesettings.colors.secondaryColors.muted
                },
                {
                    name: 'Job Dissatisfaction',
                    colorCode: this.sitesettings.colors.secondaryColors.lime
                },
                {
                    name: 'Pay',
                    colorCode: this.sitesettings.colors.secondaryColors.lavendar
                },
                {
                    name: 'Child Care',
                    colorCode: this.sitesettings.colors.secondaryColors.orange
                },
                {
                    name: 'Transportation',
                    colorCode: this.sitesettings.colors.secondaryColors.blueSteel
                },
                {
                    name: 'Moving',
                    colorCode: this.sitesettings.colors.secondaryColors.pink
                },
                {
                    name: 'Job Abandonment',
                    colorCode: this.sitesettings.colors.secondaryColors.teal
                }
            ];
            this.basePath = sitesettings.basePath;
            this.chartSettings = sitesettings.chartSettings;

            this.reportsConfig.turnoverReasons = this.chartConfigBuilder('turnoverReasons', {});
            this.reportsConfig.turnoverMonthlyTrend = this.chartConfigBuilder('turnoverMonthlyTrend', {});
            this.reportsConfig.voluntaryVsInvoluntary = this.chartConfigBuilder('voluntaryVsInvoluntary', {});
            this.reportsConfig.voluntaryVsInvoluntaryTypes = this.chartConfigBuilder('voluntaryVsInvoluntaryTypes', {});

            // Event listener when the header panel's date range changes.
            this.messageBusSvc.onMessage('components.headerpanel:datechange', this.$scope, (event, obj) => {
                this.dateRange.startDate = obj.startDate;
                this.dateRange.endDate = obj.endDate;
                this.noOfMonths = Math.abs(moment(this.dateRange.startDate).diff(moment(this.dateRange.endDate), 'months')) + 1;
                this.refreshCharts({
                    startDate: this.dateFormatterSvc.formatDateShort(obj.startDate),
                    endDate: this.dateFormatterSvc.formatDateShort(obj.endDate)
                });
            });
            this.getLatestUpdateDate();

        }

        /** @description Initializer for the controller. */
        private initialize = (): void => { }

        /**
         * @description Handle errors.
         */
        private onFailure = (response: any): void => {
            this.$log.error(response);
            this.notificationSvc.errorToastMessage();
        }


        /**
         * @description turnover reasons.
         */
        private getTurnoverReasons = (dateRange: commonInterfaces.IDateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
            this.apiSvc.query(params, 'Turnover/TopTurnoverTypes/:startDate/:endDate', false, true).then((result: any[]) =>{
               
                var data = result;

                _.forEach(data, (item: any) => {
                    var index = _.findIndex(this.turnOverReasons, (o) => { return o.name.toLowerCase() == item.TurnoverTypeName.toLowerCase() });
                    if (index >= 0) {
                        item.Color = this.turnOverReasons[index].colorCode;
                    } else {
                        item.Color = this.sitesettings.colors.secondaryColors.muted;
                    }
                });

                var colors = _.pluck(data, 'Color');
                this.chartColors.topTurnoverReasonsChart = colors.join();

                this.reportsConfig.turnoverReasons = this.chartConfigBuilder('turnoverReasons', {
                    series: [
                        {
                            type: 'pie',
                            innerSize: '60%',
                            name: 'Turnover Reasons',
                            data: _.map(data, (d, index) => {
                                var sliceName = d['TurnoverTypeName'],
                                    sliceY = d['TurnoverTypePercentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                    height: 300,
                });
                this.reportsConfig.turoverReasonsLength = data.length;
            });
        }

        /**
         * @description Monthly Trend.
         */
        private getTurnoverMonthlyTrends = (dateRange: commonInterfaces.IDateRange) => {
            //Temporary: Getting data from a json file. This will be in place till a decision is made regarding the 
            //backend; post which the backend would be built
            var params= {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
            this.apiSvc.query(params, 'Turnover/TurnoverMonthlyTrend/:startDate/:endDate', false, true).then((result) => {
                if (!result)
                    return;
                var data = result;
                var categories = [];
                var AnnualizedTurnoverPercentage = [];
                var itemCount = 0;
                _.forEach(data, (item: any) => {
                    categories.push(moment().month(item.Month - 1).year(item.Year).format('MMM \'YY'));
                    AnnualizedTurnoverPercentage.push(item.AnnualizedTurnoverPercentage);
                    itemCount++;
                });
                var MaxAnnualizedTurnoverPercentage = _.max(AnnualizedTurnoverPercentage);
                this.reportsConfig.turnoverMonthlyLength = itemCount;
                this.reportsConfig.turnoverMonthlyTrend = this.chartConfigBuilder('turnoverMonthlyTrend', {

                    categories: categories,
                    yAxis: {
                        min: 0,
                        max: MaxAnnualizedTurnoverPercentage < 36 ? 50 : MaxAnnualizedTurnoverPercentage+10,
                        plotLines: [{
                            value: this.turnoverGoal,
                            color: this.sitesettings.colors.coreColors.warning,
                            width: this.goalLineWidth,
                            zIndex: 4,
                            label: {
                                text: '',
                                style: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            }
                        }],
                    },
                    xAxis: {
                        labels: {
                            style: {
                                fontSize: '12px',
                                fontWeight: '600',
                            }
                        }
                    },
                    series: [
                        {
                            name: 'GOAL UNDER 36%',
                            lineWidth: this.goalLineWidth,
                            color: this.sitesettings.colors.coreColors.warning,
                            type: 'line',
                            marker: {
                                enabled: false
                            }

                        },
                        {
                            name: 'ANNUALIZED TURNOVER RATE',
                            lineWidth: 3,
                            data: AnnualizedTurnoverPercentage
                        }
                    ],

                    legend: {
                        enabled: true,
                        align: 'left',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: -15,
                        itemStyle: {
                            fontSize: '12px',
                            fontWeight: '600',
                        }
                    },
                });
            }, this.onFailure);
        }


        private showTurnoverReasons = () => {
           this.$uibModal.open({
               templateUrl: this.sitesettings.basePath + 'Content/js/Governance/Templates/turnover.reasons.tmpl.html',
               controller: 'TurnOverReasonsCtrl as vm',
                size: 'md',
                resolve: {}
            });
        }
        /**
         * @description toggle isVoluntary and diable Involuntary flag.
         */
        private showVoluntaryCategories = () => {
            this.isVoluntary = !this.isVoluntary;
            this.isInVoluntary = false;
            if (this.isVoluntary) {
                this.chartColors.voluntaryVsInvoluntaryChart = this.sitesettings.colors.coreColors.border + ',' + this.sitesettings.colors.secondaryColors.blue;
            }  else {
                this.chartColors.voluntaryVsInvoluntaryChart = this.sitesettings.colors.secondaryColors.teal + ',' + this.sitesettings.colors.secondaryColors.blue;
            }
            this.redraw = true;
            this.voluntaryInvoluntaryPercent = this.turnoverCategories.voluntary.turnOverPercentage;
            this.getVoluntaryVsInvoluntary();
            this.showTurnoverTypesByCategory(TurnoverCategories.Voluntary);
        }

        /**
         * @description toggle isInVoluntary and diable Voluntary flag.
         */
        private showInVoluntaryCategories = () => {
            this.isInVoluntary = !this.isInVoluntary;
            this.isVoluntary = false;
            if (this.isInVoluntary) {
                this.chartColors.voluntaryVsInvoluntaryChart = this.sitesettings.colors.secondaryColors.teal + ',' + this.sitesettings.colors.coreColors.border;
            } else {
                this.chartColors.voluntaryVsInvoluntaryChart = this.sitesettings.colors.secondaryColors.teal + ',' + this.sitesettings.colors.secondaryColors.blue;
            }
            this.redraw = true;
            this.voluntaryInvoluntaryPercent = this.turnoverCategories.inVoluntary.turnOverPercentage;
            this.getVoluntaryVsInvoluntary();
            this.showTurnoverTypesByCategory(TurnoverCategories.Involuntary);
        }

        getVoluntaryVsInvoluntaryData = (dateRange: commonInterfaces.IDateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
            this.apiSvc.query(params, 'Turnover/TurnoverByCategory/:startDate/:endDate', false, true).then((result: any) => {
                if (!result) {
                    return;
                }
                this.voluntaryVsInvoluntaryData = result;
                this.getVoluntaryVsInvoluntary();
            }, this.onFailure);
        }
        /**
         * @description Voluntary Involuntary.
         */
        private getVoluntaryVsInvoluntary = () => {

            this.totalTurnoverCount = _.sum(_.pluck(this.voluntaryVsInvoluntaryData, "TurnoverCount"));

            _.forEach(this.voluntaryVsInvoluntaryData, (item: any) => {
                    item.Percentage = (100 * item.TurnoverCount) / this.totalTurnoverCount;
                    var index = _.findIndex(this.turnOverReasons, (o) => { return o.name.toLowerCase() == item.TurnoverTypeName.toLowerCase() });
                    if (index >= 0) {
                        item.Color = this.turnOverReasons[index].colorCode;
                    } else {
                        item.Color = this.sitesettings.colors.secondaryColors.muted;
                    }
                });

            this.turnoverCategories.voluntary.data = _.filter(this.voluntaryVsInvoluntaryData, (o: any) => { return o.IsVoluntary });
            this.turnoverCategories.inVoluntary.data = _.filter(this.voluntaryVsInvoluntaryData, (o: any) => { return !o.IsVoluntary });
                this.turnoverCategories.voluntary.turnOverPercentage = _.sum(_.pluck(this.turnoverCategories.voluntary.data, "Percentage"));
                this.turnoverCategories.inVoluntary.turnOverPercentage = _.sum(_.pluck(this.turnoverCategories.inVoluntary.data, "Percentage"));

                var dataToPlot = [
                    { Name: 'Voluntary', Percentage: this.turnoverCategories.voluntary.turnOverPercentage },
                    { Name: 'InVoluntary', Percentage: this.turnoverCategories.inVoluntary.turnOverPercentage }];

                this.voluntaryVsInvoluntaryAggregateData = dataToPlot;

                //Build the chart
                this.reportsConfig.voluntaryVsInvoluntary = this.chartConfigBuilder('voluntaryVsInvoluntary', {
                    series: [
                        {
                            type: 'pie',
                            name: 'Voluntary vs Involuntary',
                            innerSize: '60%',
                            events: {
                                click: (event) => {
                                    if (event.point.name.toLowerCase() == 'voluntary') {
                                        this.showVoluntaryCategories();
                                    } else {
                                        this.showInVoluntaryCategories();
                                    }
                                    this.getVoluntaryVsInvoluntary();
                                    this.$scope.$apply();
                                }
                            },
                            data: _.map(dataToPlot, (d, index) => {
                                var sliceName = d['Name'],
                                    sliceY = d['Percentage'];
                                return {
                                    name: sliceName,
                                    y: sliceY,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle'
                                    }
                                }
                            })
                        }],
                });
        }

        /**
         * @description Display the subType chart
         */
        private showTurnoverTypesByCategory = (catergory: TurnoverCategories) => {
            this.reportsConfig.voluntaryVsInvoluntaryTypes = {};
            var selectedCategoryTypes = [];

            var OtherCategoryTurnoverPercent = 0;
            var otherCategoryName = '';
            if (catergory == TurnoverCategories.Voluntary) {
                selectedCategoryTypes = _.clone(this.turnoverCategories.voluntary.data);
                OtherCategoryTurnoverPercent = this.turnoverCategories.inVoluntary.turnOverPercentage;
                otherCategoryName = 'INVOLUNTARY';
            } else {
                selectedCategoryTypes = _.clone(this.turnoverCategories.inVoluntary.data);
                OtherCategoryTurnoverPercent = this.turnoverCategories.voluntary.turnOverPercentage;
                otherCategoryName = 'VOLUNTARY';
            }

            var unSelectedCategoryType = { TurnoverTypeName: otherCategoryName, Percentage: OtherCategoryTurnoverPercent, Color: this.sitesettings.colors.coreColors.border };

            var dataToFeedToChart = [];
            if (this.isVoluntary) {
                dataToFeedToChart = selectedCategoryTypes;
                dataToFeedToChart.push(unSelectedCategoryType);
            } else {
                dataToFeedToChart.push(unSelectedCategoryType);
                angular.forEach(selectedCategoryTypes, (type) => {
                    dataToFeedToChart.push(type);
                });
                
            }

            var colors = _.pluck(dataToFeedToChart, 'Color');
            this.chartColors.turnoversByCateroryChart = colors.join();
           this.TurnoverCategory =  dataToFeedToChart[0]['IsVoluntary']  == true ? 'Voluntary': 'Involuntary';
            // Build the chart
            this.reportsConfig.voluntaryVsInvoluntaryTypes = this.chartConfigBuilder('voluntaryVsInvoluntaryTypes', {

                series: [
                    {
                        type: 'pie',
                        innerSize: '60%',
                        data: _.map(dataToFeedToChart, (d, index) => {
                            var sliceName = d['TurnoverTypeName'],
                                sliceY = d['Percentage'];
                            return {
                                name: sliceName,
                                y: sliceY,
                                marker: {
                                    enabled: true,
                                    symbol: 'circle'
                                }
                            }
                        }),
						 name: this.TurnoverCategory
                    }],
            });

        }

        /**
         * @description Businesss Standards
         */
        private getBusinessConductStandards = (dateRange: commonInterfaces.IDateRange) => {
            var params = {
                startDate: dateRange.startDate,
                endDate: dateRange.endDate
            }
			
            this.apiSvc.query(params, 'Turnover/BusinessConductStandards/:startDate/:endDate', false, true).then((result) => {
                if (result.length <= 0)
                    return;
                _.forEach(result, (item: any) => {
					item.ForMonth = moment(item.ForMonth).format('MMM \'YY');
                    this.businessStandard.push(item);
                });
				this.RecentMonth = this.businessStandard[0].ForMonth;
            }, this.onFailure);
        }
        
        /**
         * @description Refresh the graph data.
         * @description Call the data service and fetch the data based on params.
         */
        private refreshCharts = (dateRange: commonInterfaces.IDateRange): void => {
            this.resetCharts();
            this.getTurnoverReasons(dateRange);
            this.getTurnoverMonthlyTrends(dateRange);
            this.getBusinessConductStandards(dateRange);
            this.getVoluntaryVsInvoluntaryData(dateRange);

        }

        /**
         * @description Reset charts
         */
        private resetCharts = (): void => {
            this.businessStandard = [];
            this.reportsConfig.turnoverReasons = this.chartConfigBuilder('turnoverReasons', {});
            this.reportsConfig.turnoverMonthlyTrend = this.chartConfigBuilder('turnoverMonthlyTrend', {});
            this.reportsConfig.voluntaryVsInvoluntary = this.chartConfigBuilder('voluntaryVsInvoluntary', {});
            this.reportsConfig.voluntaryVsInvoluntaryTypes = this.chartConfigBuilder('voluntaryVsInvoluntaryTypes', {});
            this.isInVoluntary = false;
            this.isVoluntary = false;
        }
        /**
         * @description Defines overridable chart options.
         */
        private chartConfigBuilder = (name, options) => {
            
            Highcharts.setOptions({
                lang: {
                    decimalPoint: '.',
                    thousandsSep: ','
                },
               
             
            });

                    var chartOptions = {

                        
                        // Pie Chart
                        turnoverReasons: {
                            chart: {
                                height: 300
                            },
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: true,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        turnoverMonthlyTrend: <HighchartsOptions>{
                            categories: [],
                            yAxis: {
                                min: 0,
                                plotLines: [{
                                    value: this.turnoverGoal,
                                    color: this.sitesettings.colors.coreColors.warning,
                                    width: this.goalLineWidth,
                                    zIndex: 4,
                                    label: { text: '' },
                                    
                                }],
                                labels: {
                                    style: {
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }

                            },
                            xAxis: {
                                labels: {
                                    style: {
                                        fontSize: '12px',
                                        fontWeight: '600',
                                    }
                                }
                            },

                            series: [{}, {}],
                            plotLines: [{}],
                            legend: {
                                itemStyle: {
                                    fontSize: '12px',
                                    fontWeight: '600',
                                }
                            },
                         
                        },

                        voluntaryVsInvoluntary: {
                            chart: {
                                height: 300
                            },
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    size: 100,
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        // Pie chart
                        voluntaryVsInvoluntaryTypes: {
                            chart: {
                                height: 300
                            },
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false,
                            },
                            legend: {
                                enabled: false,
                                type: '',
                            },
                            tooltip: {
                                style: {
                                    padding: 15,
                                    fontWeight: 'bold'

                                },

                                legend: {
                                    enabled: false,
                                },
                                borderRadius: 15,
                                pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: false,
                                },
                                series: {
                                    allowPointSelect: true,
                                    slicedOffset: 0
                                }
                            },
                            series: [
                                {
                                    type: 'pie',
                                    name: '',
                                    data: []
                                }
                            ]
                        },

                        
                    }
                    return _.assign(chartOptions[name], options);
        }

       
        private getLatestUpdateDate = (): void => {
            //Temporary: Setting static data. This will be in place till a decision is made regarding the 
            //backend; post which the backend would be built
            this.apiSvc.query({}, 'Turnover/LastUpdatedDate',false,false).then((result) => { this.lastUpdatedDate = result.LastUpdatedDate });
                   

        }


    }

    class TurnOverReasonsCtrl {
        turnOverReasons = {
            inVoluntaryReasons: [],
            voluntaryReasons: []
        }
        static $inject = ['$scope', '$uibModalInstance', 'sitesettings'];

        constructor(
            private $scope: angular.IScope,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            private sitesettings: ISiteSettings) {
            this.turnOverReasons.inVoluntaryReasons = [
                {
                    name: 'Performance',
                    description: 'Employee is being terminated for performance and/or conduct and the employee has exhausted the progressive discipline policy.',
                    colorCode: this.sitesettings.colors.secondaryColors.limeDark
                },
                {
                    name: 'Gross Misconduct',
                    description: 'Employee Terminated for behaviors which are so egregious and destroys the employer/employee relationship, and merits immediate dismissal.',
                    colorCode: this.sitesettings.colors.secondaryColors.lavendarDark
                },
                {
                    name: 'Safety Violation',
                    description: 'Employee violates the safety policy.',
                    colorCode: this.sitesettings.colors.secondaryColors.orangeDark
                },
                {
                    name: 'Attendance',
                    description: 'Repeated violations of the attendance policy.',
                    colorCode: this.sitesettings.colors.secondaryColors.blueSteelDark
                },
                {
                    name: 'Failed Probation',
                    description: 'Termination for any reason why the Employee is still under their probation period.',
                    colorCode: this.sitesettings.colors.secondaryColors.pinkDark
                },
                {
                    name: 'Denied Site Access',
                    description: 'Termination initiated by Employer for the reason such as: a Customer request, Employee did not meet site qualifications or the employee has their access denied. Employee was not able to be placed elsewhere.',
                    colorCode: this.sitesettings.colors.secondaryColors.tealDark
                },
                {
                    name: 'Other',
                    description: '',
                    colorCode: this.sitesettings.colors.secondaryColors.muted
                }
            ];
            this.turnOverReasons.voluntaryReasons = [
                {
                    name: 'Job Dissatisfaction',
                    description: 'Employee is dissatisfied with job. This could be for reasons such as: Employee disliked the work, schedules, Superviosr, or feels there are no opportunities for advancement, etc.',
                    colorCode: this.sitesettings.colors.secondaryColors.lime
                },
                {
                    name: 'Pay',
                    description: 'Employee is dissatisfied with compensation, could be rate of pay, benifits, and/or bonus.',
                    colorCode: this.sitesettings.colors.secondaryColors.lavendar
                },
                {
                    name: 'Child Care',
                    description: 'Employee is leaving to care for child(ren).',
                    colorCode: this.sitesettings.colors.secondaryColors.orange
                },
                {
                    name: 'Transportation',
                    description: 'Employee is unable to secure transportation or employee is dissatisfied with commute time.',
                    colorCode: this.sitesettings.colors.secondaryColors.blueSteel
                },
                {
                    name: 'Moving',
                    description: 'Employee is moving outside of the current geographic area and cannot continue employement.',
                    colorCode: this.sitesettings.colors.secondaryColors.pink
                },
                {
                    name: 'Job Abandonment',
                    description: 'Employee left without notification nor providing reason/cause.',
                    colorCode: this.sitesettings.colors.secondaryColors.teal
                },
                {
                    name: 'Other',
                    description: '',
                    colorCode: this.sitesettings.colors.secondaryColors.muted
                }
            ];
        }

        close = (): void => {
            this.$uibModalInstance.dismiss('close');
        };
    }
    angular.module("app").controller("TurnOverReasonsCtrl", TurnOverReasonsCtrl);
    angular.module("app").controller("TurnoverIndexCtrl", TurnoverIndexCtrl);
}
