﻿module ChaiTea.Governance.Interfaces {

    export interface IPositionFTETrendItem {
        ProgramId: number;
        ProgramName: string;
        JobId: number;
        JobName: string;
        TotalHours: number;
        EmployeeCount: number;
        HourlyRate: number;
        MonthlyCost: number;
    }

    export interface IJobHourlyRate {
        JobHourlyRateId: number;
        SiteId: number;
        ProgramId: number;
        JobId: number;
        HourlyRate: number;
    }
}