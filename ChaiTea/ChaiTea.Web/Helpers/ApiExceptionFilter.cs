﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace ChaiTea.Web.Helpers
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is NotImplementedException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
            }
            else if (context.Exception is KeyNotFoundException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else if (context.Exception is ArgumentException ||
                    context.Exception is ArgumentNullException ||
                    context.Exception is ArgumentOutOfRangeException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = context.Exception.Message.Replace(Environment.NewLine, " ") };
            }
            else if (context.Exception is UnauthorizedAccessException)
            {
                if (HttpContext.Current?.User.Identity.IsAuthenticated == true)
                { context.Response = new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = context.Exception.Message.Replace(Environment.NewLine, " ") }; }
                else
                { context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = context.Exception.Message.Replace(Environment.NewLine, " ") }; }
            }
            else
            {
#if DEBUG
                context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = context.Exception.Message.Replace(Environment.NewLine, " ") };
#else
                context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
#endif
            }
        }
    }
}