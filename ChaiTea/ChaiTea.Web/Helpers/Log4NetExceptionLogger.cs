﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace ChaiTea.Web.Helpers
{
    public class Log4NetExceptionLogger : ExceptionLogger, IExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            if (context?.Exception != null)
            {
                if (log4net.LogicalThreadContext.Properties["requestGuid"] == null)
                {
                    log4net.LogicalThreadContext.Properties["requestGuid"] = Guid.NewGuid().ToString();
                }
                var additionalInfo = new Dictionary<string, string>
                {
                    {"Url:", context?.RequestContext?.Url?.Request?.RequestUri?.OriginalString },
                    {"Principal Identity Name", context?.RequestContext?.Principal?.Identity?.Name }
                };
                if (context?.Request?.Headers?.Any() == true)
                {
                    foreach (var header in context?.Request?.Headers)
                    {
                        if (header.Key.Equals("Cookie"))
                        {
                            int i = 1;
                            foreach (var cookieHeader in header.Value)
                            {
                                var cookies = cookieHeader.Split(new char[] { ';' });
                                foreach (var cookie in cookies)
                                {
                                    var cookieParts = cookie.Split(new char[] { '=' });
                                    var cookieName = cookieParts?[0];
                                    var cookieValue = cookieParts?[1];
                                    var keyName = "Header-Cookie-" + cookieName?.Trim();
                                    if (additionalInfo.ContainsKey(keyName))
                                    {
                                        additionalInfo.Add(keyName + "-" + i, cookieValue);
                                        i++;
                                    }
                                    else
                                    { additionalInfo.Add(keyName, cookieValue); }
                                }
                            }
                        }
                        else
                        {
                            var headerValue = String.Join(",", header.Value);
                            additionalInfo.Add("Header-" + header.Key, headerValue);
                        }
                    }
                }
                
                var additionalAsJson = String.Empty;
                if (additionalInfo.Any())
                {
                    additionalAsJson = "{ \"" + String.Join("\"," + System.Environment.NewLine + "\"", additionalInfo.Select(kv => kv.Key.Replace("\"", "'") + ": " + kv.Value.Replace("\"", "'"))) + "\"}";
                }

                var level = "ERROR";
                if (context?.Exception is ArgumentException ||
                    context?.Exception is ArgumentNullException ||
                    context?.Exception is ArgumentOutOfRangeException ||
                    context?.Exception is KeyNotFoundException ||
                    context?.Exception is NotImplementedException)
                {
                    level = "INFO";
                }
                else if (context?.Exception is UnauthorizedAccessException)
                {
                    level = "WARN";
                }
                else if (context?.Exception is HttpException)
                {
                    var code = (context?.Exception as HttpException)?.GetHttpCode() ?? 500;
                    if (code == 404)
                    {
                        level = "INFO";
                    }
                    if (code < 500)
                    {
                        level = "WARN";
                    }
                }

                switch (level)
                {
                    case "INFO":
                        MvcApplication.Logger.Info($"API Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                    case "WARN":
                        MvcApplication.Logger.Warn($"API Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                    default:
                        MvcApplication.Logger.Error($"API Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                }
            }
        }
    }
}