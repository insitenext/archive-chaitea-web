﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ChaiTea.Web.Helpers
{
    public class HandleAntiforgeryTokenErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                if(filterContext.Exception is HttpAntiForgeryException)
                {
                    filterContext.ExceptionHandled = true;
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "Account" }));
                }
            }
        }
    }
}