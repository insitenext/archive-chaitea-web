﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExceptionContext = System.Web.Mvc.ExceptionContext;

namespace ChaiTea.Web.Helpers
{
    public class ErrorHandlerAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            base.OnException(context);
            if (context?.Exception != null)
            {
                if (log4net.LogicalThreadContext.Properties["requestGuid"] == null)
                {
                    log4net.LogicalThreadContext.Properties["requestGuid"] = Guid.NewGuid().ToString();
                }

                var additionalInfo = new Dictionary<string, string>
                {
                    {"Url", context?.RequestContext?.HttpContext?.Request?.Url?.OriginalString },
                    {"Principal Identity Name", context?.RequestContext?.HttpContext?.User?.Identity?.Name }
                };

                if (context?.RequestContext?.HttpContext?.Request?.Headers?.AllKeys.Any() == true)
                {
                    foreach (var header in context?.RequestContext?.HttpContext?.Request?.Headers.AllKeys)
                    {
                        if (header.Equals("Cookie"))
                        {
                            var cookieHeader = context?.RequestContext?.HttpContext?.Request?.Headers[header];
                            var cookies = cookieHeader.Split(new char[] { ';' });
                            int i = 1;
                            foreach (var cookie in cookies)
                            {
                                var cookieParts = cookie.Split(new char[] { '=' });
                                var cookieName = cookieParts?[0];
                                var cookieValue = cookieParts?[1];
                                var keyName = "Header-Cookie-" + cookieName?.Trim();
                                if (additionalInfo.ContainsKey(keyName))
                                {
                                    additionalInfo.Add(keyName + "-" + i, cookieValue);
                                    i++;
                                }
                                else
                                { additionalInfo.Add(keyName, cookieValue); }
                            }
                        }
                        else
                        {
                            var headerValue = context?.RequestContext?.HttpContext?.Request?.Headers[header];
                            additionalInfo.Add("Header-" + header, headerValue);
                        }
                    }
                }

                var additionalAsJson = String.Empty;
                if (additionalInfo.Any())
                {
                    additionalAsJson = "{ \"" + String.Join("\"," + System.Environment.NewLine + "\"", additionalInfo.Select(kv => kv.Key.Replace("\"", "'") + "\": \"" + kv.Value.Replace("\"", "'"))) + "\"}";
                }

                var level = "ERROR";

                if (context?.Exception is ArgumentException ||
                    context?.Exception is ArgumentNullException ||
                    context?.Exception is ArgumentOutOfRangeException ||
                    context?.Exception is KeyNotFoundException ||
                    context?.Exception is NotImplementedException)
                {
                    level = "INFO";
                }
                else if (context?.Exception is UnauthorizedAccessException)
                {
                    level = "WARN";
                }
                else if (context?.Exception is HttpException)
                {
                    var code = (context?.Exception as HttpException)?.GetHttpCode() ?? 500;
                    if (code == 404)
                    {
                        level = "INFO";
                    }
                    if (code < 500)
                    {
                        level = "WARN";
                    }
                }

                switch (level)
                {
                    case "INFO":
                        MvcApplication.Logger.Info($"Attribute Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                    case "WARN":
                        MvcApplication.Logger.Warn($"Attribute Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                    default:
                        MvcApplication.Logger.Error($"Attribute Exception: [{context?.Exception?.GetType().FullName}] Additional: {additionalAsJson}", context?.Exception);
                        break;
                }
            }
        }
    }
}