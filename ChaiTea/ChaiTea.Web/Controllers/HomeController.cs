﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace ChaiTea.Web.Controllers
{
    public class HomeController : MvcControllerBase
    {
        [AuthorizedRoles]
        public ActionResult Index()
        {
            var contextService = new UserContextService(UserCtx);
            //Redirect Employees to MyProfile page
            if (contextService.IsInRole(Roles.Employees) && !contextService.IsInRole(Roles.Managers) && !contextService.IsInRole(Roles.Customers))
                return RedirectToAction("Index", "Profile", new { Area="User" });

            return View();
        }

        public ActionResult GetApi()
        {
            var config = new HttpConfiguration();
            // IApiExplorer apiExplorer = config.Services.GetApiExplorer();
            IApiExplorer apiExplorer = GlobalConfiguration.Configuration.Services.GetApiExplorer();

            foreach (ApiDescription api in apiExplorer.ApiDescriptions)
            {
                Response.Write(string.Format("Controller name: {0}, Controller virtual path: {1} <br />",
                    api.ActionDescriptor.ControllerDescriptor.ControllerName,
                    api.ActionDescriptor.ControllerDescriptor.Configuration.VirtualPathRoot));

                Response.Write(string.Format("Uri path: {0} <br />", api.RelativePath));
                Response.Write(string.Format("HTTP method: {0} <br />", api.HttpMethod));
                foreach (ApiParameterDescription parameter in api.ParameterDescriptions)
                {
                    Response.Write(string.Format("Parameter: {0} - {1} <br />", parameter.Name, parameter.Source));
                }
                Response.Write("<br />");
            }
            Response.Write(string.Format("All done @{0} ! <br />", AppUtility.UtcNow().ToString()));
            Response.End();
            return null;
        }

        /// <summary>
        /// Displays the version number of the code being run.
        /// </summary>
        /// <returns>null after writing the version number to the output stream</returns>
        public ActionResult Version()
        {
            var assembly = Assembly.GetExecutingAssembly();

            Response.Write(string.Format("Current Date: {0} <br />", AppUtility.UtcNow().ToString("yyyy-MM-dd HH:mm:ss \"GMT\"zzz")));
            
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);

            AssemblyFileVersionAttribute attribute = null;
            if (attributes.Length == 0)
            {
                Response.Write("AssemblyFileVersionAttribute not found.");
                Response.End();
                return null;                                
            }
            
            attribute = attributes[0] as AssemblyFileVersionAttribute;
            Response.Write(string.Format("Version: {0} <br />", attribute.Version));
            Response.Write($"Instance Version: {MvcApplication.MvcApplicationInstanceIdentifier}");
            Response.End();
            return null;
        }
    }
}