﻿using System.Web.Mvc;

namespace ChaiTea.Web.Controllers
{
    /// <summary>
    ///     Author: Shahzad Shafique
    ///     Remark Date: 07/28/2015
    ///     Description:  Custom error pages.
    /// </summary> 
    public class ErrorController : Controller
    {
        public ActionResult Generic()
        {
            return View("Generic");
        }
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View("NotFound");
        }

        public ActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View("Forbidden");
        }
    }
}