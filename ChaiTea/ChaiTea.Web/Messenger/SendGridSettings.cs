﻿using ChaiTea.BusinessLogic.AppUtils.Settings;
using System;

namespace ChaiTea.Web.Messenger
{
    public static class SendGridSettings
    {
        private const string User_KeyName = "SendGridUser";
        private static string User_Value;
        public static string User
        {
            get
            {
                if (string.IsNullOrWhiteSpace(User_Value))
                {
                    var gev = Environment.GetEnvironmentVariable(EnvironmentSettings.Prefix + User_KeyName);
                    if (!string.IsNullOrWhiteSpace(gev))
                    { User_Value = gev; }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[User_KeyName];
                        if (!string.IsNullOrWhiteSpace(asv))
                        { User_Value = asv; }
                    }
                }
                return User_Value;
            }
        }

        private const string Key_KeyName = "SendGridKey";
        private static string Key_Value;
        public static string Key
        {
            get
            {
                if(string.IsNullOrWhiteSpace(Key_Value))
                {
                    var gev = Environment.GetEnvironmentVariable(EnvironmentSettings.Prefix + Key_KeyName);
                    if (!string.IsNullOrWhiteSpace(gev))
                    { Key_Value = gev; }
                    else
                    {
                        var asv = System.Configuration.ConfigurationManager.AppSettings[Key_KeyName];
                        if (!string.IsNullOrWhiteSpace(asv))
                        { Key_Value = asv; }
                    }
                }
                return Key_Value;
            }
        }
    }
}
