﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using ChaiTea.BusinessLogic.Settings;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.AWS;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChaiTea.Web.Messenger
{
    public class EmailMessenger : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            MailMessageVm vm = new MailMessageVm()
            {
                ToEmail = message.Destination,
                Message = message.Body,
                Subject = message.Subject,
                Tag = "ApplicationIdentity"
            };
            await EmailMessenger.SendEmailAsync(vm);
        }

        public static async Task SendEmailAsync(MailMessageVm mail)
        {
            if(string.IsNullOrWhiteSpace(mail.FromEmail))
            {
                mail.FromEmail = "SBMinsite@sbminsite.com";
            }
            if(string.IsNullOrWhiteSpace(mail.FromName))
            {
                mail.FromName = "SBM Insite Support";
            }

            var region = RegionEndpoint.GetBySystemName(AwsSettings.SesRegion);
            using (var client = new AmazonSimpleEmailServiceClient(region))
            {
                var emailRequest = new SendEmailRequest($"{mail.FromName} <{mail.FromEmail}>",
                                                        new Destination()
                                                        {
                                                            ToAddresses = new List<string>()
                                                            { $"{mail.ToName} <{mail.ToEmail}>" }
                                                        },
                                                        new Message(new Content(mail.Subject),
                                                                    new Body() { Html = new Content(mail.Message) }
                                                                )
                                                        )
                {
                    ReplyToAddresses = new List<string>() { "support@sbminsitehelp.zendesk.com" }
                };
                
                var response = await client.SendEmailAsync(emailRequest);
            }
        }
    }
}
