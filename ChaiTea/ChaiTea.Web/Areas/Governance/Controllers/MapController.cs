﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Governance.Controllers
{
    [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
    public class MapController : MvcControllerBase
    {
        public MapController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}