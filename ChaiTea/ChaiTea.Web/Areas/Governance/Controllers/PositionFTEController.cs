﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Governance.Controllers
{
    [AuthorizedRoles(Roles = Roles.OrgStructureUser + "," + Roles.Managers + "," + Roles.Customers)]
    public class PositionFTEController : MvcControllerBase
    {
        public PositionFTEController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}