﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Governance.Controllers
{
    [AuthorizedRoles(Roles = Roles.TurnoverViewers)]
    public class TurnoverController : MvcControllerBase
    {
        public TurnoverController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}