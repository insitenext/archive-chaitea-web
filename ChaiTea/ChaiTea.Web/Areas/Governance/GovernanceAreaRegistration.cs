﻿using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Governance
{
    public class GovernanceAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Governance";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Governance_default",
                "Governance/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}