﻿using ChaiTea.Web.Areas.User.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.User.Controllers
{
    [AuthorizedRoles]
    public class SettingsController : MvcControllerBase
    {
        // GET: User/Settings
        public ActionResult Index(int? id = 0)
        {
            return View(new UserSettingVm
            {
                Id = UserCtx.UserId
            });
        }
    }
}