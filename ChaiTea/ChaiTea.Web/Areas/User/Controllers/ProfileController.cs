﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.User.Controllers
{
    [AuthorizedRoles]
    public class ProfileController : MvcControllerBase
    {
        // GET: User/Profile
        public ActionResult Index()
        {
            return View();
        }
    }
}