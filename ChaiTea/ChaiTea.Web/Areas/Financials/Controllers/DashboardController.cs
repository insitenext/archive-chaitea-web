﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles]
    public class DashboardController : MvcControllerBase
    {
        // GET: Financials/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}