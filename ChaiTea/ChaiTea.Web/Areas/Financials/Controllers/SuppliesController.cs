﻿using ChaiTea.Web.Areas.Financials.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles(Roles = Roles.SuppliesViewers)]
    public class SuppliesController : MvcControllerBase
    {
        public SuppliesController()
        {
            ViewBag.Icon = "fa-quality";
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string isBillable, string category, string subCategory, string isGreen)
        {
            return View(new FilterObjectVm
            {
                isBillable = isBillable,
                category = category,
                subCategory = subCategory,
                isGreen = isGreen
            });
        }

    }
}