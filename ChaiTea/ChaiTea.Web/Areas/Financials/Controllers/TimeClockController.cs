﻿using ChaiTea.Web.Areas.Financials.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.TimeclockViewers)]
    public class TimeClockController : MvcControllerBase
    {
        public TimeClockController()
        {
            ViewBag.Icon = "fa-quality";
        }

        // GET : Financials/TimeClock
        
        public ActionResult Index(int? tabIndex = 0)
        {
            return View( new TimeClockVm()
            {
                TabIndex = tabIndex
            });
        }
        public ActionResult Details(int id)
        {
            return View(new TimeClockDetailsVm
            {
                Id = id
            });
        }

        public ActionResult Overview()
        {
            return View();
        }
    }
}