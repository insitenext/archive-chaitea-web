﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles]
    public class MyTimeClockController: MvcControllerBase
    {
        // GET : Financials/MyTimeClock
        public ActionResult Index()
        {
            return View();
        }

        // GET: Financials/TimeClock/VacationRequestEntry
        public ActionResult VacationRequestEntry()
        {
            return View();
        }
        // GET: Financials/TimeClock/ChangeOfAddress
        public ActionResult ChangeOfAddressRequest()
        {
            return View();
        }
    }
}