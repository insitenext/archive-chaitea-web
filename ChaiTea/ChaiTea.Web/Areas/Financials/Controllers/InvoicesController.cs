﻿using ChaiTea.Web.Areas.Financials.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles]
    public class InvoicesController : MvcControllerBase
    {
        public InvoicesController()
        {
            ViewBag.Icon = "fa-quality";
        }

        // GET : Financials/Invoices
        [AuthorizedRoles(Roles = Roles.BillingViewers)]
        public ActionResult Index()
        {
            return View(new InvoicesVm{
            });
        }

        // GET: Financials/Invoices/EntryView
        [AuthorizedRoles(Roles = Roles.BillingViewers)]
        public ActionResult EntryView(int? id = 0)
        {
            return View(new InvoicesVm
            {
                Id = id
            });
        }
    }
}