﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles]
    public class AccountsReceivableAgingController : MvcControllerBase
    {
        public AccountsReceivableAgingController()
        {
            ViewBag.Icon = "fa-quality";
        }

        // GET : Financials/AccountsReceivableAging
        [AuthorizedRoles(Roles = Roles.BillingViewers)]
        public ActionResult Index()
        {
            return View();
        }
    }
}