﻿using ChaiTea.Web.Areas.Financials.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Financials.Controllers
{
    [AuthorizedRoles]
    public class BillingOverviewController : MvcControllerBase
    {
        public BillingOverviewController()
        {
            ViewBag.Icon = "fa-quality";
        }

        // GET : Financials/Invoices
        [AuthorizedRoles(Roles = Roles.BillingViewers)]
        public ActionResult Index()
        {
            return View();
        }

        // GET : Financials/Invoices/Details
        [AuthorizedRoles(Roles = Roles.BillingViewers)]
        public ActionResult Details(int programId, string paidStatus)
        {
            return View(new InvoiceItemsVm
            {
                ProgramId = programId,
                PaidStatus = paidStatus
            });
        }
    }
}