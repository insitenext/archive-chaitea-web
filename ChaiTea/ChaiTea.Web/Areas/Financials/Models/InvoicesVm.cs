﻿namespace ChaiTea.Web.Areas.Financials.Models
{
    public class InvoicesVm
    {
        public int? Id { get; set; }
    }

    public class TimeClockVm
    {
        public int? TabIndex { get; set; }
    }

    public class TimeClockDetailsVm
    {
        public int Id { get; set; }
    }
   
    public class FilterObjectVm
    {
        public string isBillable { get; set; }
        public string category { get; set; }
        public string subCategory { get; set; }
        public string isGreen { get; set; }
    }

    public class InvoiceItemsVm
    {
        public int? ProgramId { get; set; }
        public string PaidStatus { get; set; }
    }
}