﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles(Roles = Roles.Admins)]
    public class KPIBuilderController : MvcControllerBase
    {
        public KPIBuilderController()
        {
           
        }

        // GET: Settings/KPIBuilder
        public ActionResult Index()
        {
            return View();
        }
    }
}