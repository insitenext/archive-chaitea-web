﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles(Roles = Roles.HrAdmins)]
    public class PositionPurposeBuilderController : MvcControllerBase
    {
        // GET: Settings/PositionPurposeBuilder
        public ActionResult Index()
        {
            return View();
        }
    }
}