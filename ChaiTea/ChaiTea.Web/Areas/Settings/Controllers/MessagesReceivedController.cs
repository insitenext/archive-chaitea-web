﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
    public class MessagesReceivedController : MvcControllerBase
    {
        // Get: Settings/MessagesReceived
        public ActionResult Index()
        {
            return View();
        }
        
    }
}