﻿using ChaiTea.Web.Areas.Settings.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles(Roles = Roles.Managers)]
    public class CourseAdminController : MvcControllerBase
    {
        public CourseAdminController()
        {
           
        }

        // GET: Settings/CourseAdmin/Assign
        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Assign(bool isAssign, bool isByEmployee)
        {
            return View(new CourseAdminVm
            {
                isAssign = isAssign,
                isByEmployee = isByEmployee
            });
        }
    }
}