﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles]
    public class MessageBuilderController : MvcControllerBase
    {
        public MessageBuilderController()
        {
            ViewBag.Icon = "icon-books2";
        }

        // GET: Settings/MessageBuilder
        [AuthorizedRoles(Roles = Roles.MessageBuilders)]
        public ActionResult Index()
        {
            return View();
        }
        // GET: Settings/MessageBuilder/Entry
        [AuthorizedRoles(Roles = Roles.MessageBuilders)]
        public ActionResult Entry()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.MessageBuilders)]
        public ActionResult Stats()
        {
            return View();
        }
    }
}