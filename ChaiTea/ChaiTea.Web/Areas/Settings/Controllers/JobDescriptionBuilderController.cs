﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Settings.Controllers
{
    [AuthorizedRoles]
    public class JobDescriptionBuilderController : MvcControllerBase
    {
        public JobDescriptionBuilderController()
        {
            ViewBag.Icon = "icon-books2";
        }

        // GET: Settings/JobDescriptionBuilder
        [AuthorizedRoles(Roles = Roles.HrAdmins)]
        public ActionResult Index()
        {
            return View();
        }
    }
}