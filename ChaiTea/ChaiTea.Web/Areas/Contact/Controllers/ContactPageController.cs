﻿using System.Web.Mvc;
using ChaiTea.Web.Areas.User.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Helpers;

namespace ChaiTea.Web.Areas.Contact.Controllers
{
    public class ContactPageController : MvcControllerBase
    {
        public ContactPageController()
        {
            ViewBag.Icon = "";
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}