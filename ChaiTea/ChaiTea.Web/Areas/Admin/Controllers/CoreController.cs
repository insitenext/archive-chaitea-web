﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    [AuthorizedRoles]
    public class CoreController : MvcControllerBase
    {
        [AuthorizedRoles(Roles = Roles.HrAdmins)]
        public ActionResult Index()
        {
            return View();
        }
    }
}