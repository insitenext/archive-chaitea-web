﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    [AuthorizedRoles]
    public class ReportController : MvcControllerBase
    {

        public ReportController()
        {

        }

        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Index()
        {
            return View();
        }

    }

}