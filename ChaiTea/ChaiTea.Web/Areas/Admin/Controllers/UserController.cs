﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    [AuthorizedRoles]
    public class UserController : MvcControllerBase
    {
        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Edit()
        {
            return View();
        }
    }
}