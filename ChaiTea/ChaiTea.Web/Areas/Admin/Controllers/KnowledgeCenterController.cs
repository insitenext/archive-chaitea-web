﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    public class KnowledgeCenterController : MvcControllerBase
    {
        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult CourseTypes()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public RedirectResult CourseAdmin()
        {
            return Redirect("../../Training/Course/Index");
        }
    }
}