﻿using System.Web.Mvc;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    public class ClientController : MvcControllerBase
    {

        public ClientController()
        {

        }

        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Index()
        {
            return View();
        }

    }
}