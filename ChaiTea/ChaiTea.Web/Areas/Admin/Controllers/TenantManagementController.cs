﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Admin.Controllers
{
    [AuthorizedRoles(Roles = Roles.CrazyProgrammers)]
    public class TenantManagementController : MvcControllerBase
    {
        // GET: TenantManagement/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}