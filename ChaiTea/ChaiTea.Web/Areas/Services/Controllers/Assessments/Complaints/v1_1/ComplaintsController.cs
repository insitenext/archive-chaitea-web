﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Assessments.Complaints.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Assessments;
using ChaiTea.BusinessLogic.ViewModels.Assessments.Complaints;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.Complaints.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Complaints")]
    public class Complaints_v11Controller : BaseApiController
    {
        /// <summary>
        /// Get complaints to which the logged in user has permissions, matching the supplied
        /// search criteria.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <param name="isCompleted">If set, return only complaints that have a completion date</param>
        /// <returns>An enumerable list of <see cref="ComplaintVm"/></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<ComplaintVm>))]
        public async Task<IHttpActionResult> Get(DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default, bool? isCompleted = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                return Ok(await complaintsSvc.GetAsync(pagingFilter, dateFilter, isCompleted, null));
            }
        }

        /// <summary>
        /// Get complaints to which the logged in user has permissions, matching the supplied
        /// search criteria.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <param name="isCompleted">If set, return only complaints that have a completion date</param>
        /// <param name="tagList">The list of building tags to match against</param>
        /// <returns>An enumerable list of <see cref="ComplaintVm"/></returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<ComplaintVm>))]
        public async Task<IHttpActionResult> GetByTags([FromBody] List<string> tagList, DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default, bool? isCompleted = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                return Ok(await complaintsSvc.GetAsync(pagingFilter, dateFilter, isCompleted, tagList));
            }
        }
        
        /// <summary>
        /// Get a single Complaint by Id.
        /// </summary>
        /// <returns>A <see cref="ComplaintVm">Complaint</see> with the matching Id (if found)</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ComplaintVm))]
        public IHttpActionResult Get(int id)
        {
            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                return Ok(complaintsSvc.Get(id));
            }
        }

        /// <summary>
        /// Create a new Complaint.
        /// </summary>
        /// <returns>A JSON object with the Id of the newly created Complaint's Id</returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Post(ComplaintVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                var newId = await complaintsSvc.CreateAsync(vm);

                return Ok(new { FeedbackId = newId });
            }
        }

        /// <summary>
        /// Update an existing Complaint (by Id)
        /// </summary>
        /// <param name="id">The Id of the Complaint to be updated.</param>
        /// <param name="vm">The <see cref="ComplaintVm">Complaint</see> data</param>
        /// <returns>A JSON object indicating the success (or failure) of the update operation</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Put(int id, ComplaintVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.FeedbackId = id;

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                var success = await complaintsSvc.UpdateAsync(vm);

                if (success)
                {
                    return Ok(new { Success = true });
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Delete an existing Complaint (by Id) (soft-delete)
        /// </summary>
        /// <param name="id">The Id of the Complaint to deleted.</param>
        /// <returns>A JSON object indicating the success (or failure) of the update operation</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(object))]
        public virtual IHttpActionResult Delete(int id)
        {
            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                var success = complaintsSvc.Delete(id);

                if (success)
                {
                    return Ok(new {Success = true});
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpPut] // Todo: Switch this to a Post
        [ResponseType(typeof(void))]
        [Route("ExemptComplaint")]
        public async Task<IHttpActionResult> UpdateExemptKpi(FeedbackEmployeeVm vm)
        {
            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                var statusOk = await complaintsSvc.UpdateExemptComplaintAsync(vm);

                if (statusOk)
                {
                    return Ok(new {Success = true});
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<ComplaintSiteCountVm>))]
        public async Task<IHttpActionResult> GetComplaintCounts(DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                return Ok(await complaintsSvc.GetComplaintsCountBySitesAsync(dateFilter));
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("ComplaintsCountBySiteByMonth")]
        public async Task<IHttpActionResult> GetComplaintsCountBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var complaintsSvc = new ComplaintsService(UserCtx))
            {
                return Ok(await complaintsSvc.GetComplaintsCountBySiteByMonthAsync(dateFilter));
            }
        }
        
        /// <summary>
        /// Returns a summary of complaints grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching complaints</param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        /// <param name="startDate">Beginning of the Complaint range</param>
        /// <param name="endDate">End of the Complaint range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<ComplaintSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new ComplaintsService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, dateFilter.StartDate, dateFilter.EndDate));
            }
        }

        /// <summary>
        /// Returns a summary of complaints by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the Complaint range</param>
        /// <param name="endDate">End of the Complaint range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(PaginatedData<ComplaintSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new ComplaintsService(UserCtx))
            {
                return Ok(await svc.GetSiteSummary(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }
        
        /// <summary>
        /// Returns a summary of complaints
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching complaints</param>
        /// <param name="startDate">Beginning of the Complaint range</param>
        /// <param name="endDate">End of the Complaint range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllSitesSummaryByTags")]
        [ResponseType(typeof(IEnumerable<ComplaintSiteSummary>))]
        public async Task<IHttpActionResult> GetAllSitesSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new ComplaintsService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummaryByTagsAsync(tags, dateFilter.StartDate, dateFilter.EndDate));
            }
        }        
    }
}