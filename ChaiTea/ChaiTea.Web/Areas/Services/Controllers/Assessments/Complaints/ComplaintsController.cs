﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.Assessments.Complaints;
using ChaiTea.BusinessLogic.ViewModels.Assessments;
using ChaiTea.BusinessLogic.ViewModels.Assessments.Complaints;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.Complaints
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Complaints")]
    public class ComplaintsController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<ComplaintVm> Get()
        {
            return new ComplaintsService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ComplaintVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new ComplaintsService(UserCtx).Get(id));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Post(ComplaintVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newId = new ComplaintsService(UserCtx).Create(vm);

            return Ok(new
                      {
                          FeedbackId = newId
                      });
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, ComplaintVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.FeedbackId = id;

            var success = new ComplaintsService(UserCtx).Update(vm);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public virtual IHttpActionResult Delete(int id)
        {
            var success = new ComplaintsService(UserCtx).Delete(id);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpPut] // Todo: Switch this to a Post
        [ResponseType(typeof(void))]
        [Route("ExemptComplaint")]
        public IHttpActionResult UpdateExemptKpi(FeedbackEmployeeVm vm)
        {
            var statusOk = new ComplaintsService(UserCtx).UpdateExemptComplaint(vm);

            if (statusOk)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<ComplaintSiteCountVm>))]
        public IHttpActionResult GetComplaintCounts(DateTime startDate, DateTime endDate)
        {
            return Ok(new ComplaintsService(UserCtx).GetComplaintsCountBySites(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("ComplaintsCountBySiteByMonth")]
        public IHttpActionResult GetComplaintsCountBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            return Ok(new ComplaintsService(UserCtx).GetComplaintsCountBySiteByMonth(startDate, endDate));
        }
    }
}