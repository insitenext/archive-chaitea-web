﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Assessments.Compliments.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Assessments.Compliments;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.Compliments.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Compliments")]
    public class Compliments_v11Controller : BaseApiController
    {
        /// <summary>
        /// Searches for compliments based on the supplied criteria limiting the search by tags.
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="skip">Skip n Records</param>
        /// <param name="take">Take n Records</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<ComplimentVm>))]
        public async Task<IHttpActionResult> Get(DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                return Ok(await complimentSvc.GetAsync(null, pagingFilter, dateFilter));
            }
        }
        
        /// <summary>
        /// Searches for compliments based on the supplied criteria limiting the search by tags.
        /// </summary>
        /// /// <param name="tagList">Array of building tags to limit the search to</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="skip">Skip n Records</param>
        /// <param name="take">Take n Records</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<ComplimentVm>))]
        public async Task<IHttpActionResult> Get([FromBody] List<string> tagList, DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                return Ok(await complimentSvc.GetAsync(tagList, pagingFilter, dateFilter));
            }
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ComplimentVm))]
        public IHttpActionResult Get(int id)
        {
            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                return Ok(complimentSvc.Get(id));
            }
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Post(ComplimentVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                var newId = await complimentSvc.CreateAsync(vm);

                return Ok(new { FeedbackId = newId });
            }
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Put(int id, ComplimentVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.FeedbackId = id;

            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                var success = await complimentSvc.UpdateAsync(vm);

                if (success)
                {
                    return Ok(new { Success = true });
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                var success = complimentSvc.Delete(id);

                if (success)
                {
                    return Ok(new { Success = true });
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(IEnumerable<ComplimentSiteCountVm>))]
        public IHttpActionResult GetComplimentCounts(DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var complimentSvc = new ComplimentsService(UserCtx))
            {
                return Ok(complimentSvc.GetComplimentsCountBySites(dateFilter));
            }
        }

        /// <summary>
        /// Returns a summary of compliments grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments</param>
        /// <param name="take">Number of sites to retrieve</param>
        /// <param name="skip">Number of sites to skip</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<ComplimentSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new ComplimentsService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, dateFilter.StartDate, dateFilter.EndDate));
            }
        }

        /// <summary>
        /// Returns a summary of compliments by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummary")]
        [ResponseType(typeof(ComplimentSiteSummary))]
        public async Task<IHttpActionResult> GetAllSitesSummaryByTags(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new ComplimentsService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }
        
        /// <summary>
        /// Returns a summary of compliments grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllSitesSummaryByTags")]
        [ResponseType(typeof(ComplimentSiteSummary))]
        public async Task<IHttpActionResult> GetAllSitesSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new ComplimentsService(UserCtx))
            {
                return Ok(await svc.GetAllSitesSummaryByTagsAsync(tags, dateFilter.StartDate, dateFilter.EndDate));
            }
        }
    }
}