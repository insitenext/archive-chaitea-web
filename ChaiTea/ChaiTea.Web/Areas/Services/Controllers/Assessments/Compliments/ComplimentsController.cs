﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.Assessments.Compliments;
using ChaiTea.BusinessLogic.ViewModels.Assessments.Compliments;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.Compliments
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Compliments")]
    public class ComplimentsController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<ComplimentVm> Get()
        {
            return new ComplimentsService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ComplimentVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new ComplimentsService(UserCtx).Get(id));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Post(ComplimentVm vm)
        {
            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            var newId = new ComplimentsService(UserCtx).Create(vm);
            return Ok(new { FeedbackId = newId });
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, ComplimentVm vm)
        {
            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            vm.FeedbackId = id;
            
            var success = new ComplimentsService(UserCtx).Update(vm);
            if(success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
            
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            var success = new ComplimentsService(UserCtx).Delete(id);
            if (success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
        }

        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<ComplimentSiteCountVm>))]
        public IHttpActionResult GetComplimentCounts(DateTime startDate, DateTime endDate)
        {
            return Ok(new ComplimentsService(UserCtx).GetComplimentsCountBySites(startDate, endDate));
        }
    }
}