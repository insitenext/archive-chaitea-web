﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.Entities.Assessments.ProfessionalReviews;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Assessments.ProfessionalReviews;
using ChaiTea.BusinessLogic.ViewModels.Assessments.ProfessionalReviews;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.ProfessionalReviews;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.ProfessionalReviews
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeAudit")]
    public class EmployeeAuditController : BaseApiController
    {
        [HttpGet]
        [Route("Employee")]
        [ResponseType(typeof(IQueryable<EmployeeAuditVm>))]
        [EnableQuery]
        public IQueryable<EmployeeAuditVm> GetByEmployeeId()
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            return new EmployeeAuditService(UserCtx).Get().Where(ea => ea.EmployeeId == employeeId);
        }

        [HttpGet]
        [Route("Employee/{id:int}")]
        [ResponseType(typeof(EmployeeAuditVm))]
        public IHttpActionResult GetByEmployeeId(int id)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            var result = new EmployeeAuditService(UserCtx).Get().FirstOrDefault(ea => ea.EmployeeId == employeeId &&
                                                                                      ea.EmployeeAuditId == id);

            if (result == null)
            {
                throw new KeyNotFoundException($"Entity with id [{id}] for current user not found");
            }
            return Ok(result);
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<EmployeeAuditVm>))]
        [EnableQuery]
        public IQueryable<EmployeeAuditVm> Get()
        {
            return new EmployeeAuditService(UserCtx).Get();
        }
        
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(EmployeeAuditVm))]
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult Get(int id)
        {
            return Ok(new EmployeeAuditService(UserCtx).Get(id));
        }
        
        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(EmployeeAuditVm))]
        public async Task<IHttpActionResult> Post(EmployeeAuditVm vm)
        {
            return Ok(await new EmployeeAuditService(UserCtx).CreateAsync(vm));
        }

        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(EmployeeAuditVm))]
        public async Task<IHttpActionResult> Put(int id, EmployeeAuditVm vm)
        {
            return Ok(await new EmployeeAuditService(UserCtx).UpdateAsync(vm));
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(EmployeeKpiEditableCheckVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = await new EmployeeAuditService(UserCtx).DeleteAsync(id);

            EmployeeKpiEditableCheckVm employeeKpiEditable = new EmployeeKpiEditableCheckVm()
                                                             {
                                                                 IsClosed = !result
                                                             };

            return Ok(employeeKpiEditable);
        }

        [HttpGet]
        [Route("EmployeesAuditsIsPassCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesAuditsIsPassCount(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeesAuditsIsPassCount(filterToDate));
        }

        /// <summary>
        /// Get Employee Audit Stats
        /// </summary>
        /// <returns>EmployeeAuditStatsVm</returns>
        [HttpGet]
        [Route("EmployeeAuditStats")]
        [ResponseType(typeof(EmployeeAuditStatsVm))]
        public IHttpActionResult GetEmployeeAuditStats(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAuditStats(filterToDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeAuditsMonthlyTrendVm>))]
        [Route("EmployeeAuditsMontlyTrend")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetEmployeeAuditsMontlyTrend(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAuditsMontlyTrend(filterToDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeAuditsPerEmployeeVm>))]
        [Route("EmployeeAuditsInfoPerEmployee")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetEmployeeAuditsInfoPerEmployee(DateTime startDate, DateTime endDate, int sortById, bool asc, int? top = null, int? skip = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAuditsInfoPerEmployee(filterToDate, filterToPage, (EmployeeAuditSortBy)sortById, asc));
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeAuditsTabsCountsVm))]
        [Route("EmployeeAuditsTabCounts")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetEmployeeAuditsTabCounts(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAuditsTabCounts(filterToDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeAuditsInfoCardsVm>))]
        [Route("EmployeeAudits")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetEmployeeAudits(DateTime startDate, DateTime endDate, bool getPassingData, bool getNotPassingData, int? top = null, int? skip = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAudits(filterToDate, filterToPage, getPassingData, getNotPassingData));
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeAuditInfoVm))]
        [Route("EmployeeAuditById/{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetEmployeeAuditById(int id)
        {
            return Ok(new EmployeeAuditService(UserCtx).GetEmployeeAuditById(id));
        }
    }
}