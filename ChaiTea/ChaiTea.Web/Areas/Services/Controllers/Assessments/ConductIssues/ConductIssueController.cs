﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Entities.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.ViewModels.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.ConductIssues
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Conduct")]
    public class ConductIssueController : BaseApiController
    {

        [HttpGet]
        [Route("Employee")]
        [ResponseType(typeof(IQueryable<ConductVm>))]
        [EnableQuery]
        public IQueryable<ConductVm> GetByEmployeeId()
        {
            int employeeId;

            using (var usrContextSvc = new UserContextService(UserCtx))
            {
                employeeId = usrContextSvc.GetOrgUserId();
            }

            return new ConductService(UserCtx).Get()
                                              .Where(c => c.OrgUserId == employeeId);
        }

        /// <summary>
        /// Retrieves conduct based on employeeid 
        /// </summary>
        /// <param name="id">The Employeeid</param>
        /// <returns>ConductVm</returns>
        [HttpGet]
        [Route("Employee/{id:int}")]
        [ResponseType(typeof(ConductVm))]
        public IHttpActionResult GetByEmployeeId(int id)
        {
            int employeeId;

            using (var usrContextSvc = new UserContextService(UserCtx))
            {
                employeeId = usrContextSvc.GetOrgUserId();
            }

            ConductVm result;

            using (ConductService service = new ConductService(UserCtx))
            {
                result = service.Get()
                                .FirstOrDefault(c => c.OrgUserId == employeeId &&
                                                     c.ConductId == id);
            }

            if (result == null)
            {
                throw new KeyNotFoundException($"Entity with id [{id}] for current user not found");
            }

            return Ok(result);
        }

        /// <summary>
        /// Retrieves all conducts
        /// </summary>
        /// <returns>collection of ConductVm</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<ConductVm>))]
        //[AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [EnableQuery]
        public IQueryable<ConductVm> Get()
        {
            return new ConductService(UserCtx).Get();
        }

        /// <summary>
        /// Retrieves conduct based on Id 
        /// </summary>
        /// <param name="id">The Employeeid</param>
        /// <returns>ConductVm</returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ConductVm))]
        //[AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult Get(int id)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.Get(id));
            }
        }

        /// <summary>
        /// Creates a Conduct as described by the ConductVm provided
        /// </summary>
        /// <param name="vm">The ConductVm</param>
        /// <returns>ConductVm</returns>
        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(ConductVm))]
        public async Task<IHttpActionResult> Post(ConductVm vm)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.CreateAsync(vm));
            }
        }

        /// <summary>
        /// Updates a Conduct as described by the ConductVm provided
        /// </summary>
        /// <param name="id">The conduct ID</param>
        /// <param name="vm">The ConductVm</param>
        /// <returns>ConductVm</returns>
        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(ConductVm))]
        public async Task<IHttpActionResult> Put(int id, ConductVm vm)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.UpdateAsync(vm));
            }
        }

        /// <summary>
        /// Logically (soft) deletes a Conduct.
        /// </summary>
        /// <param name="id">The conduct ID</param>
        /// <returns>EmployeeKpiEditableCheckVm</returns>
        [HttpDelete]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(EmployeeKpiEditableCheckVm))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                var result = await service.DeleteAsync(id);

                EmployeeKpiEditableCheckVm employeeKpiEditable = new EmployeeKpiEditableCheckVm()
                                                                 {
                                                                     IsClosed = !result
                                                                 };

                return Ok(employeeKpiEditable);
            }
        }

        /// <summary>
        /// Get list of employees conducts count 
        /// </summary>
        /// <returns>EmployeeKpiCountVm</returns>
        [HttpGet]
        [Route("EmployeesConductCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesConductCount(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetEmployeesConductCount(filterToDate));
            }
        }

        /// <summary>
        /// Get Conduct data
        /// </summary>
        /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="id"></param>
        /// <param name="getWarning"></param>
        /// <param name="getActionTaken"></param>
        /// <param name="employeeId"></param>
        /// <param name="conductTypeId"></param>
        /// <returns>ConductInfoVm</returns>
        [HttpGet]
        [ResponseType(typeof(List<ConductInfoVm>))]
        [Route("ConductInfo")]
        public IHttpActionResult GetConductInfo(int top, int skip, DateTime? startDate = null, DateTime? endDate = null,int? id = null, bool? getWarning = null, bool? getActionTaken = null, int? employeeId = null, int? conductTypeId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductInfo(filterToPage, filterToDate, id, getWarning, getActionTaken, employeeId, conductTypeId));
            }
        }

        /// <summary>
        /// Get Conduct data
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ConductInfoVm</returns>
        [HttpGet]
        [ResponseType(typeof(ConductInfoVm))]
        [Route("ConductById/{id:int}")]
        public IHttpActionResult GetConductById(int id)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductInfoById(id));
            }
        }

        /// <summary>
        /// Get conduct type counts
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>ConductTypeCountVm</returns>
        [HttpGet]
        [Route("ConductTypeCount")]
        [ResponseType(typeof(List<ConductTypeCountVm>))]
        public IHttpActionResult GetConductTypeCounts(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductTypeCounts(filterToDate));
            }
        }
        
        

        /// <summary>
        /// Get conduct type counts grouped by month
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>ConductTypeCountVm</returns>
        [HttpGet]
        [Route("ConductTypeCountsByMonth")]
        [ResponseType(typeof(List<ConductTypeCountMonthStats>))]
        public IHttpActionResult GetConductTypeCountsByMonth(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var service = new ConductService(UserCtx))
                return Ok(service.GetConductTypeCountsByMonth(filterToDate));
        }
            
        /// <summary>
        /// Get the conduct counts
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>int</returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("ConductCount")]
        public IHttpActionResult GetConductCount(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            int result;

            using (ConductService service = new ConductService(UserCtx))
            {
                result = service.GetConductCount(filterToDate);
            }

            return Ok(new { ConductCount = result });
        }

        /// <summary>
        /// Get the Last Updated Date
        /// </summary>
        /// <returns>DateTime representing the date a change was last made</returns>
        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastModifiedDate")]
        public IHttpActionResult GetLastUpdateDate()
        {
            DateTime? result;

            using (ConductService service = new ConductService(UserCtx))
            {
                result = service.GetLastModifiedDate();
            }

            return Ok(new { LastModifiedDate = result });            
        }

        /// <summary>
        /// Get Monthly trend for Conduct
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// /// <param name="conductTypeId"></param>
        /// <returns>ConductCountsByPeriodVm</returns>
        [HttpGet]
        [ResponseType(typeof(List<ConductCountsByPeriodVm>))]
        [Route("MonthlyTrend")]
        public IHttpActionResult GetMonthlyTrend(DateTime startDate, DateTime endDate, int conductTypeId)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductMonthlyTrend(filterToDate, conductTypeId));
            }
        }

        /// <summary>
        /// Get Conduct Counts By Employee
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        ///  /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <param name="sortById"></param>
        /// <param name="asc"></param>
        /// <returns>ConductCountsByEmployeeVm</returns>
        [HttpGet]
        [ResponseType(typeof(List<ConductCountsByEmployeeVm>))]
        [Route("ConductCountsByEmployee")]
        public IHttpActionResult GetConductCountsByEmployee(DateTime startDate, DateTime endDate, int top, int skip, int sortById, bool asc)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductCountsByEmployee(filterToDate, filterToPage, (ConductSortBy) sortById, asc));
            }
        }

        /// <summary>
        /// Get conduct stats
        /// </summary>
        ///  <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="employeeId"></param>
        /// <param name="conductTypeId"></param>
        /// <returns>ConductStatsVm</returns>
        [HttpGet]
        [Route("ConductStats")]
        [ResponseType(typeof(ConductStatsVm))]
        public IHttpActionResult GetConductStats(DateTime startDate, DateTime endDate, int? employeeId = null, int? conductTypeId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductStats(filterToDate, employeeId, conductTypeId));
            }
        }
    }
}