using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Entities.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Assessments.ConductIssues.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Assessments.ConductIssues.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Conduct")]
    public class ConductIssue_v11Controller : BaseApiController
    {
        /// <summary>
        /// Get list of employees conducts count 
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param> 
        /// <returns>EmployeeMetricVm</returns>
        [HttpPost]
        [Route("EmployeesConductCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        public async Task<IHttpActionResult> GetEmployeesConductCount([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.GetEmployeesConductCount(filterToDate, tags));
            }
        }        
        
        /// <summary>
        /// Get Conduct data
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="id"></param>
        /// <param name="getWarning"></param>
        /// <param name="getActionTaken"></param>
        /// <param name="employeeId"></param>
        /// <param name="conductTypeId"></param>
        /// <returns>ConductInfoVm</returns>
        [HttpPost]
        [ResponseType(typeof(List<ConductInfoVm>))]
        [Route("ConductInfo")]
        public async Task<IHttpActionResult> GetConductInfo([FromBody] ICollection<string> tags, int top, int skip, DateTime? startDate = null, DateTime? endDate = null,int? id = null, bool? getWarning = null, bool? getActionTaken = null, int? employeeId = null, int? conductTypeId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.GetConductInfoAsync(filterToPage, filterToDate, id, getWarning, getActionTaken, employeeId, conductTypeId, tags));
            }
        }

        /// <summary>
        /// Get Conduct data
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ConductInfoVm</returns>
        [HttpGet]
        [ResponseType(typeof(ConductInfoVm))]
        [Route("ConductById/{id:int}")]
        public async Task<IHttpActionResult> GetConductById(int id)
        {
            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.GetConductInfoByIdAsync(id));
            }
        }

        /// <summary>
        /// Get conduct type counts
        /// </summary>
        /// <param name="tags"></param> 
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>ConductTypeCountVm</returns>
        [HttpPost]
        [Route("ConductTypeCount")]
        [ResponseType(typeof(IEnumerable<ConductTypeCountVm>))]
        public IHttpActionResult GetConductTypeCounts([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductTypeCounts(filterToDate, tags));
            }
        }

        /// <summary>
        /// Get the conduct counts
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>int</returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("ConductCount")]
        public async Task<IHttpActionResult> GetConductCount([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            int? result;

            using (ConductService service = new ConductService(UserCtx))
            {
                result = await service.GetConductCountAsync(filterToDate, tags);
            }

            return Ok(new { ConductCount = result });
        }

        /// <summary>
        /// Get the Last Updated Date
        /// </summary>
        /// <param name="tags"></param>
        /// <returns>DateTime representing the date a change was last made</returns>
        [HttpPost]
        [ResponseType(typeof(DateTime?))]
        [Route("LastModifiedDate")]
        public async Task<IHttpActionResult> GetLastUpdateDate(ICollection<string> tags)
        {
            DateTime? result;

            using (ConductService service = new ConductService(UserCtx))
            {
                result = await service.GetLastModifiedDateAsync(tags);
            }

            return Ok(new { LastModifiedDate = result });            
        }

        /// <summary>
        /// Get Monthly trend for Conduct
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="conductTypeId"></param>
        /// <returns>ConductCountsByPeriodVm</returns>
        [HttpPost]
        [ResponseType(typeof(List<ConductCountsByPeriodVm>))]
        [Route("MonthlyTrend")]
        public async Task<IHttpActionResult> GetMonthlyTrend([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate, int conductTypeId)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.GetConductMonthlyTrendAsync(filterToDate, conductTypeId, tags));
            }
        }

        /// <summary>
        /// Get Conduct Counts By Employee
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <param name="sortById"></param>
        /// <param name="asc"></param>
        /// <returns>ConductCountsByEmployeeVm</returns>
        [HttpPost]
        [ResponseType(typeof(List<ConductCountsByEmployeeVm>))]
        [Route("ConductCountsByEmployee")]
        public async Task<IHttpActionResult> GetConductCountsByEmployee([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate, int top, int skip, int sortById, bool asc)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(await service.GetConductCountsByEmployeeAsync(filterToDate, filterToPage, (ConductSortBy) sortById, asc, tags));
            }
        }

        /// <summary>
        /// Get conduct stats
        /// </summary>
        /// <param name="tags"></param> 
        ///  <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="employeeId"></param>
        /// <param name="conductTypeId"></param>
        /// <returns>ConductStatsVm</returns>
        [HttpPost]
        [Route("ConductStats")]
        [ResponseType(typeof(ConductStatsVm))]
        public IHttpActionResult GetConductStats([FromBody] ICollection<string> tags, DateTime startDate, DateTime endDate, int? employeeId = null, int? conductTypeId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (ConductService service = new ConductService(UserCtx))
            {
                return Ok(service.GetConductStats(filterToDate, employeeId, conductTypeId, tags));
            }
        }

        /// <summary>
        /// Returns a summary of compliments grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments</param>
        /// <param name="take">Number of sites to retrieve</param>
        /// <param name="skip">Number of sites to skip</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<ConductSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new ConductService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, dateFilter));
            }
        }

        /// <summary>
        /// Returns a summary of compliments by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(ConductSiteSummary))]
        public async Task<IHttpActionResult> GetSiteSummaryById(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new ConductService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter));
            }
        }

        /// <summary>
        /// Returns a summary of compliments grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments</param>
        /// <param name="startDate">Beginning of the FeedbackDate range</param>
        /// <param name="endDate">End of the FeedbackDate range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllSitesSummaryByTags")]
        [ResponseType(typeof(ConductSiteSummary))]
        public async Task<IHttpActionResult> GetAllSitesSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new ConductService(UserCtx))
            {
                return Ok(await svc.GetAllSitesSummaryByTagsAsync(tags, dateFilter));
            }
        }
    }
}