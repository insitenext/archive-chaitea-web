﻿using ChaiTea.BusinessLogic.Reporting.Financials;
using ChaiTea.BusinessLogic.Reporting.Financials.ViewModels;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/FinanceReport")]
    public class FinanceReportController : BaseApiController
    {       
        
        /// <summary>
        /// Get Monthly Trends
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>ReportMonthlyTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportMonthlyTrendsVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("MonthlyTrends")]
        public IHttpActionResult MonthlyTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).MonthlyTrends(startDate, endDate));
        }

        /// <summary>
        /// Get each program base and above-base amount from invoice in the given time period
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBillingMonthlyTrendByProgram>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("BillingMonthlyTrendsByProgram")]
        public IHttpActionResult GetBillingMonthlyTrendsByProgram(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).GetMonthlyTrendsByProgram(startDate, endDate));
        }

        /// <summary>
        /// Get paid and unpaid amount by monthly for the given time period
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBillingPaidVsUnpaidVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("BillingMonthlyPaidVsUpaidAmount")]
        public IHttpActionResult GetBillingMonthlyPaidVsUpaidAmount(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).GetPaidVsUnpaidInvoicesReport(startDate, endDate));
        }

        /// <summary>
        /// Aggregates the monthly trends.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>ReportAggregateMonthlyTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportAggregateMonthlyTrendsVm))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("AggregateMonthlyTrends")]
        public IHttpActionResult AggregateMonthlyTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AggregateMonthlyTrends(startDate, endDate));
        }

        /// <summary>
        /// Billing the by site
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>ReportBillingBySiteVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBillingBySiteVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("BillingBySite")]
        public IHttpActionResult BillingBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).BillingBySite(startDate, endDate));
        }

        /// <summary>
        /// Billings by program.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>ReportBillingByProgramVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBillingByProgramVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("BillingByProgram")]
        public IHttpActionResult BillingByProgram(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).BillingByProgram(startDate, endDate));
        }

        /// <summary>
        /// Billings by site monthly.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBillingBySiteMonthlyVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("BillingBySiteMonthly")]
        public IHttpActionResult BillingBySiteMonthly(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).BillingBySiteMonthly(startDate, endDate));
        }

        /// <summary>
        /// Accounts Receivable Aging
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportAccountsReceivableAgingVm>))]
        [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
        [Route("AccountsReceivableAging")]
        public IHttpActionResult AccountsReceivableAging()
        {
            return Ok(new ReportService(UserCtx).AccountsReceivableAging());
        }

        [HttpGet]
        [ResponseType(typeof(ReportHourMonthlyTrendsVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.TimeclockViewers)]
        [Route("HourMonthlyTrends")]
        public IHttpActionResult HourMonthlyTrends(DateTime startDate, DateTime endDate, int? supervisorId = null)
        {
            return Ok(new ReportService(UserCtx).HourMonthlyTrends(startDate, endDate, supervisorId));
        }

        [HttpGet]
        [ResponseType(typeof(ReportWeeklyTotalHoursVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.TimeclockViewers)]
        [Route("WeeklyTotalHours")]
        public IHttpActionResult WeeklyTotalHours(DateTime selectedDate, int? supervisorId = null)
        {
            return Ok(new ReportService(UserCtx).WeeklyTotalHours(selectedDate, supervisorId));
        }

        [HttpGet]
        [ResponseType(typeof(ReportEmployeeMonthlyHoursVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.TimeclockViewers)]
        [Route("EmployeeMonthlyHours")]
        public IHttpActionResult EmployeeMonthlyHours(int year, int month, int? supervisorId = null)
        {
            return Ok(new ReportService(UserCtx).EmployeeMonthlyHours(year, month, supervisorId));
        }


        /// <summary>
        /// Get monthly total hours by SupervisorId
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="supervisorId">The SupervisorId</param>
        /// <returns>ReportMonthlyTotalHoursVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportMonthlyTotalHoursVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.TimeclockViewers)]
        [Route("MonthlyTotalHours")]
        public IHttpActionResult MonthlyTotalHours(DateTime startDate, DateTime endDate, int? supervisorId = null)
        {
            return Ok(new ReportService(UserCtx).MonthlyTotalHours(startDate, endDate, supervisorId));
        }

        /// <summary>
        /// Get yearly bonus for employee
        /// </summary>
        /// <param name="employeeId">the employeeid</param>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <returns>ReportMonthlyTotalHoursVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportYearlyBonusVm))]
        [Route("YearlyBonus")]
        public IHttpActionResult YearlyBonus(int employeeId, int year, int month)
        {
            if(!UserCtx.SiteId.HasValue)
            { return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.BadRequest) { ReasonPhrase = "No Site Provided" }); }
            return Ok(new ReportService(UserCtx).YearlyBonus(employeeId, year, month));
        }

        /// <summary>
        /// Get spends data by IsBillable, start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="isBillable">IsBillable flag</param>
        /// <returns>collection of ReportSpendByIsBillableVms</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByIsBillableVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByIsBillable")]
        public IHttpActionResult SpendByIsBillable(DateTime startDate, DateTime endDate, bool? isBillable = null)
        {
            return Ok(new ReportService(UserCtx).SpendByIsBillable(startDate, endDate, isBillable));
        }

        /// <summary>
        /// Get spends data by Program, start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <returns>collection of ReportSpendByProgramVms</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByProgramVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByProgram")]
        public IHttpActionResult SpendByProgram(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).SpendByProgram(startDate,endDate));
        }

        /// <summary>
        /// Get spends data by Program, start date and end date and program
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="programId">program id</param>
        /// <returns>collection of ReportSpendByProgramMonthlyVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByProgramMonthlyVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByProgramTrend")]
        public IHttpActionResult SpendByProgramTrend(DateTime startDate, DateTime endDate, int programId)
        {
            return Ok(new ReportService(UserCtx).SpendByProgramTrend(startDate, endDate, programId));
        }

        /// <summary>
        /// Get spends data by Site, start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <returns>collection of ReportSpendBySiteVms</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendBySiteVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendBySite")]
        public IHttpActionResult SpendBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).SpendBySite(startDate, endDate));
        }

        /// <summary>
        /// Spends by site monthly.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendBySiteMonthlyVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendBySiteMonthly")]
        public IHttpActionResult SpendBySiteMonthly(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).SpendBySiteMonthly(startDate, endDate));
        }

        /// <summary>
        /// Spends by program monthly.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByProgramMonthlyVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByProgramMonthly")]
        public IHttpActionResult SpendByProgramMonthly(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).SpendByProgramMonthly(startDate, endDate));
        }

        /// <summary>
        /// Get spends data by commodity, IsBillable, start date and end date
        /// </summary>
        /// <param name="commodityId">commodity Id</param>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="isBillable">IsBillable flag</param>
        /// <returns>collection of ReportSpendBySubCommodityVms</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendBySubCommodityVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendBySubCommodityByIsBillable")]
        public IHttpActionResult SpendBySubCommodityByIsBillable(DateTime startDate, DateTime endDate, int commodityId, bool isBillable)
        {
            return Ok(new ReportService(UserCtx).SpendBySubCommodityByIsBillable(startDate, endDate, commodityId, isBillable));
        }

        /// <summary>
        /// Get spends data by IsGreen, start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="isGreen">IsBillable</param>
        /// <returns>collection of ReportSpendByIsGreenVms</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByIsGreenVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByIsGreen")]
        public IHttpActionResult SpendByIsGreen(DateTime startDate, DateTime endDate, bool? isGreen = null)
        {
            return Ok(new ReportService(UserCtx).SpendByIsGreen(startDate, endDate, isGreen));
        }

        /// <summary>
        /// Get spends data by CommodityId, IsGreen, start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="isGreen">IsBillable flag</param>
        /// <param name="commodityId">commodity Id</param>
        /// <returns>collection of ReportSpendBySubCommodityVms</returns>a
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendBySubCommodityVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendBySubCommodityByIsGreen")]
        public IHttpActionResult SpendBySubCommodityByIsGreen(DateTime startDate, DateTime endDate, int commodityId, bool isGreen)
        {
            return Ok(new ReportService(UserCtx).SpendBySubCommodityByIsGreen(startDate, endDate, commodityId, isGreen));
        }

        /// <summary>
        /// Get spends data by Category based on start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <returns>collection of ReportSpendByCategoryVms</returns>a
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportSpendByCategoryVm>))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("SpendByCategory")]
        public IHttpActionResult SpendByCategory(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).SpendByCategory(startDate, endDate));
        }

        /// <summary>
        /// Get Monthly Product Trends based on start date and end date
        /// </summary>
        /// <param name="endDate">End Data</param>
        /// <param name="startDate">Start Date</param>
        /// <returns>collection of ReportMonthlyProductTrendsVms</returns>a
        [HttpGet]
        [ResponseType(typeof(ReportMonthlyProductTrendsVm))]
        [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
        [Route("MonthlyProductTrends")]
        public IHttpActionResult MonthlyProductTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).MonthlyProductTrends(startDate, endDate));
        }

        /// <summary>
        /// Retrieves PositionFTE data for a given date range
        /// </summary>
        /// <returns>ReportPositionFTEVm</returns>
        [HttpGet]
        [Route("PositionFTE")]
        [ResponseType(typeof(IEnumerable<ReportPositionFTEVm>))]
        public IHttpActionResult PositionFTE(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).PositionFTETrends(startDate, endDate));
        }
    }
}