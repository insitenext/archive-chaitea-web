﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Personnel;
using ChaiTea.BusinessLogic.Reporting.Personnel.Models;
using ChaiTea.BusinessLogic.Reporting.Personnel.Vm;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/PersonnelReport")]
    public class PersonnelReportController : BaseApiController
    {
        [HttpGet]
        [Route("Scorecard")]
        [ResponseType(typeof(List<ReportScorecardNumbersVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Scorecard(DateTime startDate, DateTime endDate)
        {
            DateTimeRequiredRange dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var svc = new PersonnelReportService(UserCtx))
            {
                return Ok(svc.GetScorecardTrendsBySite(dateRange, UserCtx.ClientId.GetValueOrDefault(), UserCtx.SiteId));
            }
        }
        
        [HttpGet]
        [Route("Attendance")]
        [ResponseType(typeof(List<ReportAttendanceIssueNumbersVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Attendance(DateTime startDate, DateTime endDate)
        {
            DateTimeRequiredRange dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var svc = new PersonnelReportService(UserCtx))
            {
                return Ok(svc.GetAttendanceIssueNumbersBySite(dateRange, UserCtx.ClientId.GetValueOrDefault(), UserCtx.SiteId));
            }
        }
        
        [HttpGet]
        [Route("Conduct")]
        [ResponseType(typeof(List<ConductNumbers>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Conduct(DateTime startDate, DateTime endDate)
        {
            DateTimeRequiredRange dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var svc = new PersonnelReportService(UserCtx))
            {
                return Ok(svc.GetConductNumbersBySite(dateRange, UserCtx.ClientId.GetValueOrDefault(), UserCtx.SiteId));
            }
        }
    }
}