﻿using ChaiTea.BusinessLogic.Reporting.Quality;
using ChaiTea.BusinessLogic.Reporting.Quality.ViewModels;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Report")]
    public class QualityReportController : BaseApiController
    {
        /// <summary>
        /// Retrieves in-scope and out-of-scope complaint trends data sets for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly. The data returned will can be filtered based on a combination 
        /// of values in the user context and the date range.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplaintTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportComplaintTrendsVm))]
        [Route("ComplaintTrends")]
        public IHttpActionResult ComplaintTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplaintTrends(startDate, endDate));
        }

        /// <summary>
        /// Retrieves in-scope and out-of-scope complaint trends data sets for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly. The data returned will can be filtered based on a combination 
        /// of values in the user context, the date range and the values of trendId.
        /// </summary>
        /// <param name="trendId">Can be null, can represent a programId or a complaintTypeId depending on the user context. When null, data sets returned are filtered for the programId in the user context, if any.  If the value
        /// is not null, it represents a filter for a programId when no programId is in the user context or a filter for a complaintTypeId when a program has been selected in the user context.</param>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplaintTrendsVm</returns>        
        [HttpGet]
        [ResponseType(typeof(ReportComplaintTrendsVm))]
        [Route("ComplaintTrendsByTypeOrProgram")]
        public IHttpActionResult ComplaintTrendsByTypeOrProgram(int trendId, DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplaintTrendsByTypeOrProgram(trendId, startDate, endDate));
        }

        /// <summary>
        /// Retrieves in-scope complaints by type for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportFeedbackTypeOrProgramVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportFeedbackTypeOrProgramVm))]
        [Route("ComplaintsByType")]
        public IHttpActionResult ComplaintsByType(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplaintsByType(startDate, endDate));
        }
        

        /// <summary>
        /// Retrieves in-scope complaints by site for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplaintTrendsBySiteVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportComplaintTrendsBySiteVm))]
        [Route("ComplaintTrendsBySite")]
        public IHttpActionResult ComplaintTrendsBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplaintTrendsBySite(startDate, endDate));
        }

        /// <summary>
        /// Retrieves in-scope and out-of-scope compliment trends data sets for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly. The data returned will can be filtered based on a combination 
        /// of values in the user context and the date range.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplimentTrendsVm</returns>        
        [HttpGet]
        [ResponseType(typeof(ReportComplimentTrendsVm))]
        [Route("ComplimentTrends")]
        public IHttpActionResult ComplimentTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplimentTrends(startDate, endDate));
        }

        /// <summary>
        /// Retrieves in-scope and out-of-scope compliment trends data sets for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly. The data returned will can be filtered based on a combination 
        /// of values in the user context, the date range and the values of trendId.
        /// </summary>
        /// <param name="trendId">Can be null, can represent a programId or a complimentTypeId depending on the user context. When null, data sets returned are filtered for the programId in the user context, if any.  If the value
        /// is not null, it represents a filter for a programId when no programId is in the user context or a filter for a complimentTypeId when a program has been selected in the user context.</param>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplimentTrendsVm</returns> 
        [HttpGet]
        [ResponseType(typeof(ReportComplimentTrendsVm))]
        [Route("ComplimentTrendsByTypeOrProgram")]
        public IHttpActionResult ComplimentTrendsByTypeOrProgram(int trendId, DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplimentTrendsByTypeOrProgram(trendId, startDate, endDate));
        }

        /// <summary>
        /// Retrieves in-scope compliments by type for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportFeedbackTypeOrProgramVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportFeedbackTypeOrProgramVm))]
        [Route("ComplimentsByType")]
        public IHttpActionResult ComplimentsByType(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplimentsByType(startDate, endDate));
        }
        

        /// <summary>
        /// Retrieves in-scope compliments by site for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportComplimentTrendsBySiteVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportComplimentTrendsBySiteVm))]
        [Route("ComplimentTrendsBySite")]
        public IHttpActionResult ComplimentTrendsBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).ComplimentTrendsBySite(startDate, endDate));
        }


        /// <summary>
        /// Retrieves audit trends data for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportAuditTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportAuditTrendsVm))]
        [Route("AuditTrends")]
        public IHttpActionResult AuditTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AuditTrends(startDate, endDate));
        }

        /// <summary>
        /// Retrieves audits trends by building based on start date and end date
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>collection of ReportAuditsByBuildingVms</returns>
        [HttpGet]
        [ResponseType(typeof(List<ReportAuditsByBuildingVm>))]
        [Route("AuditsByBuilding")]
        public IHttpActionResult AuditsByBuilding(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AuditsByBuilding(startDate, endDate));
        }

        /// <summary>
        /// Retrieves audits trends by floor based on Building, start date and end date
        /// </summary>
        /// <param name="buildingId">Building Id</param>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>collection of ReportAuditsByFloorVms</returns>
        [HttpGet]
        [ResponseType(typeof(List<ReportAuditsByFloorVm>))]
        [Route("AuditsByFloor")]
        public IHttpActionResult AuditsByFloor(DateTime startDate, DateTime endDate, int? buildingId = null)
        {
            return Ok(new ReportService(UserCtx).AuditsByFloor(startDate, endDate, buildingId));
        }

        /// <summary>
        /// Retrieves audit trends by site for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportAuditTrendsBySiteVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportAuditTrendsBySiteVm))]
        [Route("AuditTrendsBySite")]
        public IHttpActionResult AuditTrendsBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AuditTrendsBySite(startDate, endDate));
        }

        /// <summary>
        /// Retrieves audit trends by program for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportAuditTrendsByProgramVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportAuditTrendsByProgramVm))]
        [Route("AuditTrendsByProgram")]
        public IHttpActionResult AuditTrendsByProgram(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AuditTrendsByProgram(startDate, endDate));
        }

        /// <summary>
        /// Retrieves audit trends by area classification for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportAuditTrendsByAreaClassficationVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportAuditTrendsByAreaClassificationVm))]
        [Route("AuditTrendsByAreaClassification")]
        public IHttpActionResult AuditTrendsByAreaClassification(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).AuditTrendsByAreaClassification(startDate, endDate));
        }

        /// <summary>
        /// Retrieves audits by user for the given programId.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <param name="programId">Unique identifier for program.</param>
        /// <returns>ReportAuditGridByProgramVm</returns>
        [HttpGet]
        [Route("AuditGridByProgram")]
        [ResponseType(typeof(ReportAuditGridByProgramVm))]
        public IHttpActionResult AuditGridByProgram(DateTime startDate, DateTime endDate, int programId)
        {
            if (UserCtx.ProgramId.HasValue && UserCtx.ProgramId != programId)
            {
                throw new ArgumentOutOfRangeException(nameof(programId), "User context program selection does not match requested program.");
            }

            return Ok(new ReportService(UserCtx).AuditGridByProgram(startDate, endDate, programId));
        }

        /// <summary>
        /// Retrieves employee audits average based on start date and end date
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>        
        /// <returns>collection of GetEmployeeAuditsAvgVm</returns>
        [HttpGet]
        [Route("EmployeeAuditsAvg")]
        [ResponseType(typeof(List<GetEmployeeAuditsAvgVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult EmployeeAuditsAvg(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).EmployeeAuditsAvg(startDate, endDate));
        }

        /// <summary>
        /// Retrieves employee audits average by Areas based on start date and end date
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>        
        /// <returns>collection of GetEmployeeAuditsAvgVm</returns>
        [HttpGet]
        [Route("EmployeeAuditsAvgByArea")]
        [ResponseType(typeof(List<GetEmployeeAuditsAvgVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult EmployeeAuditsAvgByArea(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).EmployeeAuditsAvgByArea(startDate, endDate));
        }

        /// <summary>
        /// Retrieves employee audits average by Areas based on employeeid, start date and end date
        /// </summary>
        /// <param name="employeeId">Employeeid</param>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>        
        /// <returns>collection of GetEmployeeAuditsAvgVm</returns>
        [HttpGet]
        [Route("EmployeeAuditsAvgByAreaByEmployee")]
        [ResponseType(typeof(List<GetEmployeeAuditsAvgVm>))]
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult EmployeeAuditsAvgByAreaByEmployee(DateTime startDate, DateTime endDate, int employeeId)
        {
            return Ok(new ReportService(UserCtx).EmployeeAuditsAvgByAreaByEmployee(startDate, endDate, employeeId));
        }

        /// <summary>
        /// Retrieves employee audits average by Areas based on employeeid, start date and end date
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>        
        /// <returns>collection of GetEmployeeAuditsAvgVms</returns>
        [HttpGet]
        [Route("EmployeeAuditsAvgByAreaByEmployee")]
        [ResponseType(typeof(List<GetEmployeeAuditsAvgVm>))]
        [AuthorizedRolesApi]
        public IHttpActionResult EmployeeAuditsAvgByAreaByEmployee(DateTime startDate, DateTime endDate)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            return Ok(new ReportService(UserCtx).EmployeeAuditsAvgByAreaByEmployee(startDate, endDate, employeeId));
        }

        /// <summary>
        /// Retrieves work order trends data for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportWorkOrderTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportWorkOrderTrendsVm))]
        [Route("WorkOrderTrends")]
        public IHttpActionResult WorkOrderTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).WorkOrderTrends(startDate, endDate));
        }

        /// <summary>
        /// Retrieves work orders by site for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportWorkOrderTrendsBySiteVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportWorkOrderTrendsBySiteVm))]
        [Route("WorkOrdersBySite")]
        public IHttpActionResult WorkOrdersBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).WorkOrdersBySite(startDate, endDate));
        }

        /// <summary>
        /// Retrieves work orders by the program in context for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportWorkOrderTypeOrProgramVm</returns>
        [HttpGet]
        [Route("WorkOrdersByProgramOrType")]
        [ResponseType(typeof(ReportWorkOrderTypeOrProgramVm))]
        public IHttpActionResult WorkOrdersByProgramOrType(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).WorkOrdersByProgramOrType(startDate, endDate));
        }

        /// <summary>
        /// Retrieves work order trends by type for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <param name="trendId">Unique identifier for typeId</param>
        /// <returns>ReportWorkOrderTrendsVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportWorkOrderTrendsVm))]
        [Route("WorkOrderTrendsByProgramOrType")]
        public IHttpActionResult WorkOrderTrendsByProgramOrType(DateTime startDate, DateTime endDate, int trendId)
        {
            return Ok(new ReportService(UserCtx).WorkOrderTrendsByProgramOrType(startDate, endDate, trendId));
        }

        /// <summary>
        /// Retrieves work orders by employee for a given date range. If date range is less than 12 months, data is summarized monthly. If date range is greater than 12 months, data is summarized quarterly.
        /// </summary>
        /// <param name="startDate">Start date for date range.</param>
        /// <param name="endDate">End date for date range.</param>
        /// <returns>ReportWorkOrderGridByEmployeeVm</returns>
        [HttpGet]
        [Route("WorkOrdersByEmployee")]
        [ResponseType(typeof(ReportWorkOrderGridByEmployeeVm))]
        public IHttpActionResult WorkOrdersByEmployee(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).WorkOrdersByEmployee(startDate, endDate));
        }
    }
}
