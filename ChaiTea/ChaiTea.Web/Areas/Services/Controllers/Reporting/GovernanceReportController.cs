﻿using ChaiTea.BusinessLogic.Reporting.Financials;
using ChaiTea.BusinessLogic.Reporting.Financials.ViewModels;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;


namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/GovernanceReports")]
    public class GovernanceReportController : BaseApiController
    {
        /// <summary>
        /// Retrieves PositionFTE data for a given date range
        /// </summary>
        /// <returns>ReportPositionFTEVm</returns>
        [HttpGet]
        [Route("PositionFTE")]
        [ResponseType(typeof(IEnumerable<ReportPositionFTEVm>))]
        public IHttpActionResult PositionFTE(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).PositionFTETrends(startDate, endDate));
        }
    }
}