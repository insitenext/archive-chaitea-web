﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Reporting.Training;
using ChaiTea.BusinessLogic.Reporting.Training.ViewModels;
using ChaiTea.BusinessLogic.Services.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TrainingReport")]
    public class TrainingReportController : BaseApiController
    {
        /// <summary>
        /// Gets ownership count based on startdate and enddate
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <param name="isEmployee">startdate</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [Route("Ownership/Count")]
        public IHttpActionResult OwnershipCount(DateTime startDate, DateTime endDate, bool isEmployee)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            return Ok(new OwnershipService(UserCtx).GetOwnershipsCount(startDate, endDate, (isEmployee ? (int?)employeeId : null)));
        }

        /// <summary>
        /// Gets ownership monthly trends based on startdate and enddate
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportOwnershipMonthlyTrendsVm))]
        [Route("Ownership/MonthlyTrends")]
        public IHttpActionResult OwnershipMonthlyTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).OwnershipMonthlyTrends(startDate, endDate));
        }

        /// <summary>
        /// Gets ownership trends by site based on startdate and enddate
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportOwnershipTrendsBySiteVm))]
        [Route("Ownership/TrendsBySite")]
        public IHttpActionResult OwnershipTrendsBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).OwnershipTrendsBySite(startDate, endDate));
        }

        /// <summary>
        /// Gets ownership by employees
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportOwnershipByEmployeeVm>))]
        [Route("Ownership/ByEmployee")]
        public IHttpActionResult OwnershipByEmployee(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).OwnershipByEmployee(startDate, endDate));
        }

        /// <summary>
        /// Gets ownership based on report type
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportOwnershipsByReportTypeVm>))]
        [Route("Ownership/ByReportType")]
        public IHttpActionResult OwnershipByReportType(DateTime startDate, DateTime endDate)
        {
            return Ok(new ReportService(UserCtx).OwnershipByReportType(startDate, endDate));
        }

        /// <summary>
        /// Gets ownership trends based on reporttype, startdate and enddate
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <param name="reportTypeId">reporttype id</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportOwnershipTrendsByReportTypeVm))]
        [Route("Ownership/TrendsByReportType")]
        public IHttpActionResult OwnershipTrendsByReportType(DateTime startDate, DateTime endDate, int reportTypeId)
        {
            return Ok(new ReportService(UserCtx).OwnershipTrendsByReportType(startDate, endDate, reportTypeId));
        }

        /// <summary>
        /// Gets ownership trends based on reporttype, startdate and enddate
        /// </summary>
        /// <param name="endDate">enddate</param>
        /// <param name="startDate">startdate</param>
        /// <param name="classificationId">Classification id (Good Catch = 1, Associate Initiated = 2)</param>
        /// <returns>TodoItemVm</returns>
        [HttpGet]
        [ResponseType(typeof(ReportOwnershipTrendsByClassificationIdVm))]
        [Route("Ownership/TrendsByClassificationId")]
        public IHttpActionResult OwnershipTrendsByClassificationId(DateTime startDate, DateTime endDate, int classificationId)
        {
            return Ok(new ReportService(UserCtx).OwnershipTrendsByClassificationId(startDate, endDate, classificationId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportGetPositionsVm>))]
        [Route("HrAdmin/Positions/{applyFilter:bool}/{jobId:int?}")]
        [AuthorizedRolesApi(Roles = Roles.HrAdmins)]
        public IHttpActionResult Positions(bool applyFilter, int? jobId = null)
        {
            return Ok(new ReportService(UserCtx).Positions(applyFilter, jobId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportCoursesViewCountVm>))]
        [Route("CoursesViewCount/{isDescending:bool}")]
        public IHttpActionResult CoursesViewCount(bool isDescending)
        {
            return Ok(new ReportService(UserCtx).CoursesViewCount(isDescending));
        }
    }
}