﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Reporting.Safety;
using ChaiTea.BusinessLogic.Reporting.Safety.Vm;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Reporting
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SafetyReport")]
    public class SafetyReportController : BaseApiController
    {
        [HttpGet]
        [Route("InjuryReportNumbers")]
        [ResponseType(typeof(List<ReportInjuryReportNumbersVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Scorecards(DateTime startDate, DateTime endDate)
        {
            using (var svc = new SafetyReportService(UserCtx))
                return Ok(svc.GetInjuryReportNumbersBySite(startDate, endDate, UserCtx.ClientId.GetValueOrDefault(), UserCtx.SiteId));
        }
    }
}