﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Client")]
    public class ClientController : BaseCrudApiController<ClientVm>
    {
        protected override Func<ClientVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<ClientVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<ClientVm> CreateService
        {
            get { return new ClientService(UserCtx); }
        }

        /// <summary>
        /// Retrieves a list of clients and sites.
        /// </summary>
        /// <returns>Collection of ClientVms</returns>
        [HttpGet]
        [ResponseType(typeof(IQueryable<ClientSiteVm>))]
        [Route("Orgs")]
        [EnableQuery]
        public IQueryable<ClientSiteVm> Orgs()
        {
            return (Service as ClientService).GetOrgs();
        }
    }
}