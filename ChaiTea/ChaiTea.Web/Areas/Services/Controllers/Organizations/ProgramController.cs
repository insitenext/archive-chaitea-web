﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Program")]
    public class ProgramController : BaseCrudApiController<ProgramVm>
    {
        protected override Func<ProgramVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<ProgramVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<ProgramVm> CreateService
        {
            get { return new ProgramService(UserCtx, true, true); }
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Programs")]
    public class ProgramLookupController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(ProgramLookupDTO))]
        [Route("ProgramLookup/{programId:int}")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult GetProgramLookup(int programId)
        {
            return Ok(new ProgramService(UserCtx).GetProgramLookupDTO(programId));
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("ProgramLookup/")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult SetProgramLookup(ProgramLookupDTO programLookupDto)
        {
            var success = new ProgramService(UserCtx).SaveProgramLookup(programLookupDto);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}