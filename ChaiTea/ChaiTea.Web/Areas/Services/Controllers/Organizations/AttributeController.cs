﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi(Roles = Roles.HrAdmins)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Attribute")]
    public class AttributeController : BaseCrudApiController<AttributeVm>
    {
        protected override Func<AttributeVm, int> GetIdentifier
        {
            get { return vm => vm.AttributeId; }
        }

        protected override Action<AttributeVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.AttributeId = id; }
        }

        protected override ICrudService<AttributeVm> CreateService
        {
            get { return new AttributeService(UserCtx); }
        }

    }
}