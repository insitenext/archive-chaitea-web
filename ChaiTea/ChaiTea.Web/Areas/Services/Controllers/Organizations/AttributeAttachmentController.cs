﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi(Roles = Roles.HrAdmins)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AttributeAttachment")]
    public class AttributeAttachmentController : BaseCrudApiController<AttributeAttachmentVm>
    {

        protected override Func<AttributeAttachmentVm, int> GetIdentifier
        {
            get { return vm => vm.AttributeAttachmentId; }
        }

        protected override Action<AttributeAttachmentVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.AttributeAttachmentId = id; }
        }

        protected override ICrudService<AttributeAttachmentVm> CreateService
        {
            get { return new AttributeAttachmentService(UserCtx); }
        }

        [HttpGet]
        [ResponseType(typeof(AttributeAttachmentCountVm))]
        [Route("AttachmentCount/{attributeId}/{attributeValue}")]
        public IHttpActionResult GetAttachmentCount(int attributeId, string attributeValue = null)
        {
            var attachmentService = new AttributeAttachmentService(UserCtx);

            var attributeAttachmentCount = attachmentService.Get().Where(aa => aa.AttributeId == attributeId && aa.AttributeValue == attributeValue).Count();
               
            return Ok(new AttributeAttachmentCountVm() { AttributeAttachmentCount = attributeAttachmentCount });
        }
    }
}