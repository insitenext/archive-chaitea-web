﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/CustomerRepresentative")]
    public class CustomerRepresentativeController : BaseCrudApiController<CustomerRepresentativeVm>
    {
        protected override Func<CustomerRepresentativeVm, int> GetIdentifier
        {
            get { return vm => vm.CustomerRepresentativeId; }
        }

        protected override Action<CustomerRepresentativeVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.CustomerRepresentativeId = id; }
        }

        protected override ICrudService<CustomerRepresentativeVm> CreateService
        {
            get { return new CustomerRepresentativeService(UserCtx); }
        }
    }
}
