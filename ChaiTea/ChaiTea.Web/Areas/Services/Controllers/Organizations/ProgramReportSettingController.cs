﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ProgramReportSetting")]
    public class ProgramReportSettingController : BaseCrudApiController<ProgramReportSettingVm>
    {
        protected override Func<ProgramReportSettingVm, int> GetIdentifier
        {
            get { return vm => vm.ProgramReportSettingId; }
        }

        protected override Action<ProgramReportSettingVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.ProgramReportSettingId = id; }
        }

        protected override ICrudService<ProgramReportSettingVm> CreateService
        {
            get { return new ProgramReportSettingService(UserCtx); }
        }

        [HttpGet]
        [Route("{id:int}")]
        [AuthorizedRolesApi]
        [ResponseType(typeof(ProgramReportSettingVm))]
        public override IHttpActionResult Get(int id)
        {
            Service = new ProgramReportSettingService(UserCtx);
            return base.Get(id);
        }

        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Post(ProgramReportSettingVm vm)
        {
            Service = new ProgramReportSettingService(UserCtx);
            return base.Post(vm);
        }

        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Put(int id, ProgramReportSettingVm vm)
        {
            Service = new ProgramReportSettingService(UserCtx);
            return base.Put(id, vm);
        }

        [HttpDelete]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Delete(int id)
        {
            Service = new ProgramReportSettingService(UserCtx);
            return base.Delete(id);
        }

        [HttpGet]
        [ResponseType(typeof(ProgramReportSettingVm))]
        [Route("{programId:int}/{orgId:int}")]
        public IHttpActionResult GetReportSettings(int programId, int orgId)
        {
            return Ok(new ProgramReportSettingService(UserCtx).GetByOrgId(programId, orgId));
        }

    }

}