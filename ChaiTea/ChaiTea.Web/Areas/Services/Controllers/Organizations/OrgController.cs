﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Org")]
    public class OrgController : BaseCrudApiController<OrgSettingVm>
    {
        protected override Func<OrgSettingVm, int> GetIdentifier
        {
            get { return e => e.Id; }
        }

        protected override Action<OrgSettingVm, int> SetIdentifier
        {
            get { return (e, id) => e.Id = id; }
        }

        protected override ICrudService<OrgSettingVm> CreateService
        {
            get { return new OrgSettingService(UserCtx); }
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<OrgSettingVm>))]
        [Route("Clients/{clientId:int?}/{isSite:bool?}")]
        public IHttpActionResult Clients(int? clientId = null, bool? isSite = null)
        {
            return Ok((Service as OrgSettingService).GetClients(clientId, isSite));
        }

        [HttpGet]
        [ResponseType(typeof(string))]
        [Route("Timezone/{orgId:int}")]
        public IHttpActionResult GetTimezone(int orgId)
        {
            var orgSetting = new OrgSettingService(UserCtx).Get(orgId);
            var timezone = "";
            if (orgSetting != null)
            {
                timezone = orgSetting.Timezone;
            }
            return Ok(new { Timezone = timezone });
        }

        public class TimeZoneInfoDTO
        {
            public string Id { get; set; }
            public string DisplayName { get; set; }
            public string StandardName { get; set; }
            public string DaylightName { get; set; }
            public TimeSpan BaseUtcOffset { get; set; }
            public bool SupportsDaylightSavingTime { get; set; }
        }

        public IEnumerable<TimeZoneInfoDTO> GetTimeZones()
        {
            var tzList = TimeZoneInfo.GetSystemTimeZones()
                                     .GroupBy(o => o.Id)
                                     .Select(c => c.First());

            var tzInfoDTOList = new List<TimeZoneInfoDTO>();

            foreach (var tzItemZoneInfo in tzList.OrderBy(tz => tz.BaseUtcOffset.TotalMinutes))
            {
                tzInfoDTOList.Add(new TimeZoneInfoDTO
                                  {
                                      Id = tzItemZoneInfo.Id,
                                      DisplayName = tzItemZoneInfo.DisplayName,
                                      StandardName = tzItemZoneInfo.StandardName,
                                      DaylightName = tzItemZoneInfo.DaylightName,
                                      BaseUtcOffset = tzItemZoneInfo.BaseUtcOffset,
                                      SupportsDaylightSavingTime = tzItemZoneInfo.SupportsDaylightSavingTime
                                  });
            }

            return tzInfoDTOList;
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<TimeZoneInfoDTO>))]
        [Route("Timezones")]
        public IHttpActionResult GetTimezones()
        {
            return Ok(GetTimeZones());
        }
    }
}
