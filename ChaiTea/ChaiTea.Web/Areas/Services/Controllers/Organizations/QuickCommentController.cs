﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/QuickComment")]
    public class QuickCommentController : BaseCrudApiController<QuickCommentVm>
    {
        protected override Func<QuickCommentVm, int> GetIdentifier
        {
            get { return vm => vm.CommentId; }
        }

        protected override Action<QuickCommentVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.CommentId = id; }
        }

        protected override ICrudService<QuickCommentVm> CreateService
        {
            get { return new QuickCommentService(UserCtx); }
        }
    }
}
