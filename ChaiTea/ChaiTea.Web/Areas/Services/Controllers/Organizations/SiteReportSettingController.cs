﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SiteReportSetting")]
    public class SiteReportSettingController : BaseCrudApiController<SiteReportSettingVm>
    {
        protected override Func<SiteReportSettingVm, int> GetIdentifier
        {
            get { return vm => vm.SiteId; }
        }

        protected override Action<SiteReportSettingVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.SiteId = id; }
        }

        protected override ICrudService<SiteReportSettingVm> CreateService
        {
            get { return new SiteReportSettingService(UserCtx); }
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(SiteReportSettingVm))]
        [AuthorizedRolesApi]
        public override IHttpActionResult Get(int id)
        {
            Service = new SiteReportSettingService(UserCtx);
            return base.Get(id);
        }

        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Post(SiteReportSettingVm vm)
        {
            Service = new SiteReportSettingService(UserCtx);
            return base.Post(vm);
        }

        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Put(int id, SiteReportSettingVm vm)
        {
            Service = new SiteReportSettingService(UserCtx);
            return base.Put(id, vm);
        }

        [HttpDelete]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Delete(int id)
        {
            Service = new SiteReportSettingService(UserCtx);
            return base.Delete(id);
        }

        [HttpGet]
        [ResponseType(typeof(AllSitesReportSettingsVm))]
        [Route("AllByClient/{clientId:int}")]
        public IHttpActionResult GetAllSiteReportSettings(int clientId)
        {
            return Ok(new SiteReportSettingService(UserCtx).GetAllByClientId(clientId));
        }
    }
}