﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Department")]
    public class DepartmentController : BaseCrudApiController<DepartmentVm>
    {
        protected override Func<DepartmentVm, int> GetIdentifier
        {
            get { return vm => vm.DepartmentId; }
        }

        protected override Action<DepartmentVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.DepartmentId = id; }
        }

        protected override ICrudService<DepartmentVm> CreateService
        {
            get { return new DepartmentService(UserCtx); }
        }
    }
}