﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/ServiceAreaTag")]
    public class ServiceAreaTagController : BaseApiController
    {
        /// <summary>
        /// Searches tags where the name contains the searched name.
        /// </summary>
        /// <param name="name">The name search on.</param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ServiceAreaTagVm>))]
        [Route("SearchByName")]
        public async Task<IHttpActionResult> SearchByName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name), "Name cannot be null or empty.");
            }
            
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.SearchTagsByName(name));
            }
        }

        /// <summary>
        /// Searches tag collections for the current user.
        /// </summary>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserServiceAreaTagCollectionVm>))]
        [Route("TagCollectionByCurrentUser")]
        public async Task<IHttpActionResult> TagCollectionByCurrentUser()
        {
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.GetTagCollectionByCurrentUser());
            }
        }

        /// <summary>
        /// Searches tag collections for matching specified name.
        /// </summary>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserServiceAreaTagCollectionVm>))]
        [Route("TagCollectionByName")]
        public async Task<IHttpActionResult> TagCollectionByName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name), "Name cannot be null or empty.");
            }
            
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.GetTagCollectionByName(name));
            }
        }

        /// <summary>
        /// Creates a collection of Service Area Tags based on the logged in user.
        /// </summary>
        /// <param name="request">The object to create the collection from.</param>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("CreateTagCollection")]
        public async Task<IHttpActionResult> CreateTagCollection(UserServiceAreaTagCollectionCreateRequest request)
        {
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.CreateTagCollection(request));
            }
        }
        
        /// <summary>
        /// Creates a collection of Service Area Tags based on the logged in user.
        /// </summary>
        /// <param name="request">The object to create the collection from.</param>
        [HttpPut]
        [ResponseType(typeof(bool))]
        [Route("UpdateTagCollection")]
        public async Task<IHttpActionResult> UpdateTagCollection([FromBody]UserServiceAreaTagCollectionUpdateRequest request)
        {
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.UpdateTagCollection(request));
            }
        }

        /// <summary>
        /// Delete tag collection by Id. The Id passed must be owned by the current user.
        /// </summary>
        /// <param name="Id">Tag collection Id.</param>
        [HttpDelete]
        [ResponseType(typeof(bool))]
        [Route("DeleteTagCollection/{Id:int}")]
        public async Task<IHttpActionResult> DeleteTagCollectionById(int Id)
        {
            using (var svc = new ServiceAreaTagService(UserCtx))
            {
                return Ok(await svc.DeleteTagCollectionById(Id));
            }
        }
    }
}