﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using static ChaiTea.BusinessLogic.Services.Organizations.ClientReportSettingService;

namespace ChaiTea.Web.Areas.Services.Controllers.Organizations
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ClientReportSetting")]
    public class ClientReportSettingController : BaseCrudApiController<ClientReportSettingVm>
    {
        protected override Func<ClientReportSettingVm, int> GetIdentifier
        {
            get { return vm => vm.ClientId; }
        }

        protected override Action<ClientReportSettingVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.ClientId = id; }
        }

        protected override ICrudService<ClientReportSettingVm> CreateService
        {
            get { return new ClientReportSettingService(UserCtx); }
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ClientReportSettingVm))]
        [AuthorizedRolesApi]
        public override IHttpActionResult Get(int id)
        {
            Service = new ClientReportSettingService(UserCtx);
            return base.Get(id);
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(ClientReportSettingVm))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Post(ClientReportSettingVm vm)
        {
            Service = new ClientReportSettingService(UserCtx);
            return base.Post(vm);
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(ClientReportSettingVm))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override Task<IHttpActionResult> Put(int id, ClientReportSettingVm vm)
        {
            Service = new ClientReportSettingService(UserCtx);
            return base.Put(id, vm);
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public override async Task<IHttpActionResult> Delete(int id)
        {
            await new ClientReportSettingService(UserCtx).DeleteAsync(id);
            return Ok();
        }

        [HttpPut]
        [ResponseType(typeof(bool))]
        [Route("UpdateCreateAllSitesClientReportSetting/{clientId:int}")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult UpdateCreateAllSitesClientReportSetting(int clientId)
        {
            return Ok(new ClientReportSettingService(UserCtx).UpdateCreateAllSitesClientReportSetting(clientId));
        }

        [HttpPut]
        [ResponseType(typeof(bool))]
        [Route("UpdateAllScoringProfiles")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult UpdateAllScoringProfiles(ScoringProfileUpdateDto spDto)
        {
            return Ok(new ClientReportSettingService(UserCtx).UpdateAllScoringProfiles(spDto));
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ReportSetting")]
    public partial class ReportSettingController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(ReportSettingVm))]
        [Route("{clientId:int}/{siteId:int}/{programId:int}")]
        public IHttpActionResult GetReportSettings(int clientId, int siteId, int programId)
        {
            return Ok(new ClientReportSettingService(UserCtx).GetAll(clientId, siteId, programId));
        }
    }
}