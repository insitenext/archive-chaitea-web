﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.NoticedIssues;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.NoticedIssues
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/OwnershipCustomerRepresentative")]
    public class OwnershipCustomerRepresentativeController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<OwnershipCustomerRepresentativeVm> Get()
        {
            return new OwnershipCustomerRepresentativeService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(OwnershipCustomerRepresentativeVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new OwnershipCustomerRepresentativeService(UserCtx).Get(id));
        }


        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Post(OwnershipCustomerRepresentativeVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newId = new OwnershipCustomerRepresentativeService(UserCtx).Create(vm);
            return Ok(new { CustomerRepresentativeId = newId });
        }
    }
}