﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Training.Models;
using ChaiTea.BusinessLogic.Services.ActionRequests.NoticedIssues.v1_1;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.NoticedIssues.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Ownership")]
    public class Ownership_v11Controller : BaseApiController
    {
        /// <summary>
        /// Get Ownership (ReportIt) records to which the logged in user has permissions, matching the supplied
        /// search criteria.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <param name="filterOnStatus">Filter on the status of the ReportIt? (default: false)</param>
        /// <param name="status">If filterOnStatus is true, the value to filter on should be supplied.</param>
        /// <returns>An enumerable list of <see cref="OwnershipVm"/></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<OwnershipVm>))]
        public async Task<IHttpActionResult> Get(DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default, bool filterOnStatus = default, bool? status = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(await svc.GetByTagsAsync(pagingFilter, dateFilter, filterOnStatus, status, null));
            }
        }

        /// <summary>
        /// Get Ownership (ReportIt) records to which the logged in user has permissions, matching the supplied
        /// search criteria.
        /// </summary>
        /// <param name="tags">The list of building tags to match against - list cannot be empty</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <param name="filterOnStatus">Filter on the status of the ReportIt? (default: false)</param>
        /// <param name="status">If filterOnStatus is true, the value to filter on should be supplied.</param>
        /// <returns>An enumerable list of <see cref="OwnershipVm"/></returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<OwnershipVm>))]
        public async Task<IHttpActionResult> GetByTags([FromBody] List<string> tags, DateTime? startDate = null, DateTime? endDate = null, int? skip = null, int? take = null, bool filterOnStatus = false, bool? status = default)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(await svc.GetByTagsAsync(pagingFilter, dateFilter, filterOnStatus, status, tags));
            }
        }

        /// <summary>
        /// Returns a summary of Ownership (ReportIt) records grouped by Site ID, based on the tag list provided.
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching ReportIts - cannot be empty.</param>
        /// <param name="startDate">Beginning of the range.</param>
        /// <param name="endDate">End of the range.</param>
        /// <param name="take">Number of rows to take.</param>
        /// <param name="skip">Number of pages to skip.</param>
        /// <returns>An <see cref="OwnershipSummary">OwnershipSummary</see> object with the summary data.</returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<OwnershipSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate, int? take = null, int? skip = null)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(await svc.GetSiteSummariesByTagsAsync(tags, pagingFilter, dateFilter));
            }
        }

        /// <summary>
        /// Returns a summary of Ownership (ReportIt) by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning date of the Ownership range</param>
        /// <param name="endDate">End date of the Complaint range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(OwnershipSiteSummary))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            
            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(await svc.GetSiteSummary(id, dateFilter));
            }
        }
        
        /// <summary>
        /// Returns a summary of the Ownership (ReportIt) records for all sites.
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments - cannot be empty.</param>
        /// <param name="startDate">Beginning of the FeedbackDate range.</param>
        /// <param name="endDate">End of the FeedbackDate range.</param>
        /// <returns>An <see cref="OwnershipSummary">OwnershipSummary</see> object with the summary data.</returns>
        [HttpPost]
        [Route("AllSiteSummariesByTags")]
        [ResponseType(typeof(OwnershipSummary))]
        public async Task<IHttpActionResult> GetAllSiteSummariesByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummariesByTagsAsync(tags, dateFilter));
            }
        }

        /// <summary>
        /// Get a single Ownership (ReportIt) by Id.
        /// </summary>
        /// <returns>A <see cref="OwnershipVm">ReportIt</see> with the matching Id (if found)</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(OwnershipVm))]
        public IHttpActionResult Get(int id)
        {
            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(svc.Get(id));
            }
        }

        /// <summary>
        /// Create a new Ownership (ReportIt).
        /// </summary>
        /// <returns>A JSON object with the Id of the newly created ReportIt's Id</returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(object))]
        public IHttpActionResult Post(OwnershipVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var svc = new OwnershipService(UserCtx))
            {
                var newId = svc.Create(vm);

                return Ok(new
                          {
                              OwnershipId = newId
                          });
            }
        }

        /// <summary>
        /// Update an existing Ownership (ReportIt) record (by Id)
        /// </summary>
        /// <param name="id">The Id of the ReportIt to be updated.</param>
        /// <param name="vm">The <see cref="OwnershipVm">ReportIt</see> data</param>
        /// <returns>A JSON object indicating the success (or failure) of the update operation</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Put(int id, OwnershipVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.OwnershipId = id;

            using (var svc = new OwnershipService(UserCtx))
            {
                if (svc.Update(vm))
                {
                    return Ok();
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
            }
        }

        /// <summary>
        /// Get the count of ReportIts on a per-site basis, per employee.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <returns></returns>
        [HttpGet]
        [Route("EmployeesOwnershipsCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        public IHttpActionResult GetEmployeesOwnershipsCount(DateTime startDate, DateTime endDate, int? skip = default, int? take = default)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(svc.GetOwnershipsCountByEmployees(pagingFilter, dateFilter));
            }
        }

        /// <summary>
        /// Get the count of ReportIts on a per-site basis.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<OwnershipSiteCountVm>))]
        public IHttpActionResult GetOwnershipCounts(DateTime startDate, DateTime endDate, int? skip = default, int? take = default)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(svc.GetOwnershipsCountBySites(pagingFilter, dateFilter));
            }
        }

        /// <summary>
        /// Get the count of ReportIts on a per-site basis, per month.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("OwnershipCountBySiteByMonth")]
        public IHttpActionResult GetOwnershipCountBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            using (var svc = new OwnershipService(UserCtx))
            {
                return Ok(svc.GetOwnershipCountBySiteByMonth(startDate, endDate));
            }
        }

        /// <summary>
        /// Delete a single ReportIt.
        /// </summary>
        /// <param name="id">Id of Ownership/ReportIt record to [soft] delete.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var svc = new OwnershipService(UserCtx))
            {
                var success = await svc.Delete(id);

                if (success)
                {
                    return Ok(new {Success = true});
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }
    }
}