﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.NoticedIssues
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Ownership")]
    public class OwnershipController : BaseApiController
    {
        /// <summary>
        /// Get all active ReportIts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<OwnershipVm> Get()
        {
            return new OwnershipService(UserCtx).Get();
        }

        /// <summary>
        /// Get ReportIt by Id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(OwnershipVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new OwnershipService(UserCtx).Get(id));
        }

        /// <summary>
        /// Create a new ReportIt.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Post(OwnershipVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newId = new OwnershipService(UserCtx).Create(vm);

            return Ok(new
                      {
                          OwnershipId = newId
                      });
        }

        /// <summary>
        /// Update an existing ReportIt.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, OwnershipVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.OwnershipId = id;

            var success = new OwnershipService(UserCtx).Update(vm);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpGet]
        [Route("EmployeesOwnershipsCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesOwnershipsCount(DateTime startDate, DateTime endDate)
        {
            return Ok(new OwnershipService(UserCtx).GetOwnershipsCountByEmployees(startDate, endDate));
        }

        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<OwnershipSiteCountVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetOwnershipCounts(DateTime startDate, DateTime endDate)
        {
            return Ok(new OwnershipService(UserCtx).GetOwnershipsCountBySites(startDate, endDate));
        }

        /// <summary>
        /// Get the ReportIts (Ownership) counts per month.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("OwnershipCountBySiteByMonth")]
        public IHttpActionResult GetOwnershipCountBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            return Ok(new OwnershipService(UserCtx).GetOwnershipCountBySiteByMonth(startDate, endDate));
        }

        /// <summary>
        /// Delete request (ownership).
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            return Ok(await new OwnershipService(UserCtx).Delete(id));
        }
    }
}