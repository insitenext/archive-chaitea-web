﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.ActionRequests.WorkOrders;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.WorkOrders;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.WorkOrders
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/WorkOrders")]
    public class WorkOrdersController: BaseCrudApiController<WorkOrderVm>
    {
        protected override Func<WorkOrderVm, int> GetIdentifier
        {
            get
            {
                return vm => vm.WorkOrderId;
            }
        }

        protected override Action<WorkOrderVm, int> SetIdentifier
        {
            get
            {
                return (vm, id) => vm.WorkOrderId = id;
            }            
        }

        protected override ICrudService<WorkOrderVm> CreateService
        {
            get
            {
                return new WorkOrderService(UserCtx);
            }
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("SubmittedCount")]
        public IHttpActionResult GetSubmittedCount(DateTime startDate, DateTime endDate)
        {
            return Ok(new { SubmittedCount = new WorkOrderService(UserCtx).GetSubmittedCount(startDate, endDate) });
        }                     
    }    
}