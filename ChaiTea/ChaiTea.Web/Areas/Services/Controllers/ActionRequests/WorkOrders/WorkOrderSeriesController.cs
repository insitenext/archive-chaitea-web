﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.ActionRequests.WorkOrders;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.WorkOrders;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.WorkOrders
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/WorkOrderSeries")]
    public class WorkOrderSeriesController : BaseCrudApiController<WorkOrderVm>
    {
        protected override Func<WorkOrderVm, int> GetIdentifier
        {
            get
            {
                return vm => (int)vm.WorkOrderSeriesId;
            }
        }

        protected override Action<WorkOrderVm, int> SetIdentifier
        {
            get
            {
                return (vm, id) => vm.WorkOrderSeriesId = id;
            }
        }

        protected override ICrudService<WorkOrderVm> CreateService
        {
            get
            {
                return new WorkOrderSeriesService(UserCtx);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<WorkOrderVm>))]
        [Route("UpcomingWorkOrders/{top:int}/{skip:int}")]
        public IHttpActionResult GetSubmittedCount(int top, int skip)
        {
            return Ok(new WorkOrderSeriesService(UserCtx).GetUpcomingWorkOrders(top, skip));
        }
    }

}