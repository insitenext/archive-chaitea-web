﻿using ChaiTea.BusinessLogic.Services.ActionRequests.WorkOrders;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.WorkOrders;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.WorkOrders
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/WorkOrderPriority")]
    public class WorkOrderPriorityController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IEnumerable<PriorityVm>))]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(new PriorityService(UserCtx).Get());
        }
    }    
}