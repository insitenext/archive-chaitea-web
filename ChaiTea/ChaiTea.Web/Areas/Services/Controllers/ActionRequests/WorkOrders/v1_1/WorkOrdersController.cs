﻿using ChaiTea.BusinessLogic.Services.ActionRequests.WorkOrders.v1_1;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.WorkOrders;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.WorkOrders.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/WorkOrders")]
    public class WorkOrders_v11Controller: BaseApiController
    {
        /// <summary>
        /// Get a list of WorkOrders by tags
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="tagList"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<WorkOrderVm>))]
        [Route("ByTags")]
        public async Task<IHttpActionResult> GetListByTags([FromBody] List<string> tagList, DateTime? startDate = default, DateTime? endDate = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            using (var workOrderService = new WorkOrderService(UserCtx))
            {
                var list = await workOrderService.GetList(dateFilter, tagList);
                return Ok(list);
            }
        }

        /// <summary>
        /// Gets a list of WorkOrders
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<WorkOrderVm>))]
        [Route("")]
        public async Task<IHttpActionResult> GetList(DateTime? startDate = default, DateTime? endDate = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            using (var workOrderService = new WorkOrderService(UserCtx))
            {
                var list = await workOrderService.GetList(dateFilter);
                return Ok(list);
            }
        }

        /// <summary>
        /// Gets a single WorkOrder
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(WorkOrderVm))]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            using (var workOrderService = new WorkOrderService(UserCtx))
            {
                var vm = await workOrderService.Get(id);
                return Ok(vm);
            }
        }

        /// <summary>
        /// Gets submitted count for date range
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("SubmittedCount")]
        public async Task<IHttpActionResult> GetSubmittedCount(DateTime startDate, DateTime endDate)
        {
            using (var workOrderService = new WorkOrderService(UserCtx))
            {
                var SubmittedCount = await workOrderService.GetSubmittedCount(startDate, endDate);
                return Ok(SubmittedCount);
            }
        }

        /// <summary>
        /// Gets submitted count by tags for date range
        /// </summary>
        /// <param name="tagList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("SubmittedCountByTags")]
        public async Task<IHttpActionResult> GetSubmittedCountByTags([FromBody] List<string> tagList, DateTime startDate, DateTime endDate)
        {
            using (var workOrderService = new WorkOrderService(UserCtx))
            {
                var SubmittedCount = await workOrderService.GetSubmittedCountByTags(tagList, startDate, endDate);
                return Ok(SubmittedCount);
            }
        }
        
        /// <summary>
        /// Gets a summary of Work Orders grouped by Site
        /// </summary>
        /// <param name="tags">Tags to use for searching complaints</param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        [HttpPost]
        [ResponseType(typeof(WorkOrderSummary))]
        [Route("SiteSummariesByTags")]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
         
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new WorkOrderService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, range.StartDate, range.EndDate));
            }
        }
        
        /// <summary>
        /// Returns a summary of Work Orders by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(PaginatedData<WorkOrderSummary>))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new WorkOrderService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }        
        
        /// <summary>
        /// Gets a summary of site summaries
        /// </summary>
        /// <param name="tags">Tags to use for searching Work Orders</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        [HttpPost]
        [ResponseType(typeof(WorkOrderSummary))]
        [Route("AllSitesSummaryByTags")]
        public async Task<IHttpActionResult> GetAllSiteSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new WorkOrderService(UserCtx))
            {
                return Ok(await svc.GetAllSitesSummaryAsync(tags, range.StartDate, range.EndDate));
            }
        }
    }    
}