﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.ActionRequests.Tasks;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.Tasks;

using ChaiTea.Web.Identity;

using log4net;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.Tasks
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TodoItem")]
    public class TodoItemController : BaseApiController
    {
        private readonly ILog Logger = LogManager.GetLogger(nameof(TodoItemController));

        [HttpGet]
        [ResponseType(typeof(IQueryable<TodoItemVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        [EnableQuery]
        public IQueryable<TodoItemVm> Get()
        {
            return new TodoItemService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(TodoItemVm))]
        public IHttpActionResult Get(int id)
        {
            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var todoItem = todoItemService.Get(itemId:id)
                                              .FirstOrDefault();

                if (todoItem == null)
                {
                    Logger.Warn($"Entity of type [{typeof(TodoItemVm).Name}] with id [{id}] not found");
                    throw new ArgumentOutOfRangeException(nameof(id), $"TodoItem with key:{id} not found.");
                }

                return Ok(todoItem);
            }
        }

        [HttpGet]
        [AuthorizedRolesApi]
        [ResponseType(typeof(IQueryable<TodoItemVm>))]
        [Route("ByEmployee")]
        [EnableQuery]
        public IQueryable<TodoItemVm> GetByEmployee()
        {
            int employeeId;

            using (var userSvc = new UserContextService(UserCtx))
            {
                employeeId = userSvc.GetOrgUserId();
            }

            return new TodoItemService(UserCtx).Get(orgUserId:employeeId);
        }

        [HttpGet]
        [ResponseType(typeof(TodoItemVm))]
        [Route("ByEmployee/{id:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetByEmployeeById(int id)
        {
            using (var userSvc = new UserContextService(UserCtx))
            {
                int employeeId = userSvc.GetOrgUserId();

                using (var tdiService = new TodoItemService(UserCtx))
                {
                    var vm = tdiService.Get(orgUserId:id).FirstOrDefault();

                    if (vm == null ||
                        vm.OrgUserId == employeeId) // TODO: convert to employeeid
                    {
                        throw new UnauthorizedAccessException($"{nameof(GetByEmployeeById)} endpoint only allows todo's assigned to current user " +
                                                              $"(Id:{UserCtx.UserId}, OrgUserId:{employeeId}).");
                    }

                    return Ok(vm);
                }
            }
        }

        [HttpPost]
        [ResponseType(typeof(int))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult PostAsync(TodoItemVm vm)
        {
            if (!ModelState.IsValid)
            {
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.BadRequest));
            }

            if (vm.OrgUserId == 0 &&
                vm.EmployeeId.HasValue)
            {
                vm.OrgUserId = vm.EmployeeId.Value;
            }

            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var newId = todoItemService.Create(vm);

                return Ok(new
                          {
                              TodoItemId = newId
                          });
            }
        }

        [HttpPut]
        [ResponseType(typeof(TodoItemVm))]
        [Route("PutByEmployee/{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Employees)]
        public IHttpActionResult PutByEmployee(int id, TodoItemVm vm)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            if (vm.OrgUserId == 0 &&
                vm.EmployeeId.HasValue)
            {
                vm.OrgUserId = vm.EmployeeId.Value;
            }

            if (vm.OrgUserId != employeeId) // Todo: replace with Employee Id instead of id (which is OrgUserId)
            {
                throw new UnauthorizedAccessException($"Users are only able to update their own To-Do Items.");
            }

            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var success = todoItemService.Update(vm);

                if (success)
                {
                    return Ok(vm);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("MarkTodoItemAsRead/{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public IHttpActionResult MarkTodoItemAsRead(int id)
        {
            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var success = todoItemService.MarkTodoAsRead(id);

                if (success)
                {
                    return Ok();
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Put(int id, TodoItemVm vm)
        {
            if (!ModelState.IsValid)
            {
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.BadRequest));
            }

            vm.TodoItemId = id;

            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var success = todoItemService.Update(vm);

                if (success)
                {
                    return Ok();
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var success = todoItemService.Delete(id);

                if (success)
                {
                    return Ok();
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }

        [HttpGet]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("AllCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetAllCounts(DateTime startDate, DateTime endDate, bool? filterByFailedInspectionItem = null, bool? filterByReAudit = null, int? buildingId = null)
        {
            return Ok(new TodoItemService(UserCtx).GetAllCounts(startDate, endDate, buildingId, null, filterByFailedInspectionItem, filterByReAudit));
        }

        [HttpGet]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("EmployeeCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetEmployeeCounts(DateTime startDate, DateTime endDate, int employeeId, bool? filterByFailedInspectionItem = null, bool? filterByReAudit = null, int? buildingId = null)
        {
            return Ok(new TodoItemService(UserCtx).GetAllCounts(startDate, endDate, buildingId, employeeId, filterByFailedInspectionItem, filterByReAudit));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsByPeriodVm>))]
        [Route("MonthlyTrend")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMonthlyTrend(DateTime startDate, DateTime endDate)
        {
            int loggedInOrgUserId = new UserContextService(UserCtx).GetOrgUserId();
            int? employeeId = null;

            if (!new OrgUserService(UserCtx).Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
            {
                if (new EmployeeService(UserCtx).Get().Any(e => e.EmployeeId == loggedInOrgUserId))
                {
                    employeeId = loggedInOrgUserId;
                }
            }

            var fromUtilTodo = new TodoItemService(UserCtx).GetMonthlyTrend(startDate, endDate, employeeId);

            return Ok(fromUtilTodo.OrderBy(t => t.Year).ThenBy(t => t.Month));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsByEmployeeVm>))]
        [Route("EmployeeInfo")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetEmployeeInfo(DateTime startDate, DateTime endDate)
        {
            int loggedInOrgUserId = new UserContextService(UserCtx).GetOrgUserId();
            int? employeeId = null;

            if (!new OrgUserService(UserCtx).Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
            {
                if (new EmployeeService(UserCtx).Get().Any(e => e.EmployeeId == loggedInOrgUserId))
                {
                    employeeId = loggedInOrgUserId;
                }
            }

            return Ok(new TodoItemService(UserCtx).GetEmployeeInfo(startDate, endDate, employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsBySiteVm>))]
        [Route("SiteCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetSiteCounts(DateTime startDate, DateTime endDate)
        {
            var fromUtilTodo = new TodoItemService(UserCtx).GetSiteCounts(startDate, endDate);

            var sites = new SiteService(UserCtx).Get()
                                                .Select(t => t.Id)
                                                .Distinct()
                                                .ToList();

            var result = new List<ToDoCountsBySiteVm>();

            foreach (var site in sites)
            {
                var utilTodoCount = fromUtilTodo.FirstOrDefault(t => t.SiteId == site);

                if (utilTodoCount == null)
                {
                    result.Add(new ToDoCountsBySiteVm()
                               {
                                   SiteId = site,
                                   Count = 0
                               });
                }
                else
                {
                    result.Add(new ToDoCountsBySiteVm()
                               {
                                   SiteId = site,
                                   Count = utilTodoCount.Count
                               });
                }
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("ToDos")]
        [ResponseType(typeof(IQueryable<TodoItemVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        [EnableQuery]
        public IQueryable<TodoItemVm> GetToDos(bool? filterByFailedInspectionItem = null)
        {
            return new TodoItemService(UserCtx).Get(filterByFailedInspectionItem);
        }

        [HttpGet]
        [Route("ReRunAuditToDo/{scorecardId:int}")]
        [ResponseType(typeof(TodoItemVm))]
        public IHttpActionResult GetReRunAuditToDo(int scorecardId)
        {
            return Ok(new TodoItemService(UserCtx).GetReRunAuditToDo(scorecardId));
        }

        [HttpGet]
        [ResponseType(typeof(ReAuditsInfoVm))]
        [Route("ReAuditsInfo")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        public IHttpActionResult GetReAuditsInfo()
        {
            return Ok(new TodoItemService(UserCtx).GetReAuditsInfo());
        }
    }
}