﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.ActionRequests.Tasks.v1_1;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.Tasks;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;
using log4net;
using Roles = ChaiTea.Web.Identity.Roles;

namespace ChaiTea.Web.Areas.Services.Controllers.ActionRequests.Tasks.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/TodoItem")]
    public class ToDoItem_v11Controller : BaseApiController
    {
        /// <summary>
        /// Retrieves an item by it's ID
        /// </summary>
        /// <param name="id">The ToDoItemId</param>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(TodoItemVm))]
        public IHttpActionResult Get(int id)
        {
            using (var todoItemService = new TodoItemService(UserCtx))
            {
                var todoItem = todoItemService.GetById(id);

                if (todoItem != null) 
                    return Ok(todoItem);
                
                throw new ArgumentOutOfRangeException(nameof(id), $"TodoItem with key:{id} not found.");
            }
        }

        /// <summary>
        /// Retrieves items for the specified criteria.
        /// </summary>
        /// <param name="take">Number of records to take</param>
        /// <param name="skip">Number of records to skip</param>
        /// <param name="startDate">Start range for DueDate</param>
        /// <param name="endDate">End range for DueDate</param>
        /// <param name="isComplete">Search completed or incomplete items</param>
        /// <param name="sortByColumn">DueDate,CreateDate</param>
        /// <param name="ascending">Order of sort</param>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<TodoItemVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        public IHttpActionResult Get(int? take, int? skip, DateTime startDate, DateTime endDate, bool? isComplete = null, string sortByColumn = null, bool ascending = true)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.Get(null, paging.take, paging.skip, isComplete, null, range.StartDate, range.EndDate, sortByColumn, ascending));
            }
        }
        
        /// <summary>
        /// Retrieves items for the specified criteria.
        /// </summary>
        /// <param name="tags">Tags to limit search</param>
        /// <param name="take">Number of records to take</param>
        /// <param name="skip">Number of records to skip</param>
        /// <param name="startDate">Start range for DueDate</param>
        /// <param name="endDate">End range for DueDate</param>
        /// <param name="isComplete">Search completed or incomplete items</param>
        /// <param name="sortByColumn">DueDate,CreateDate</param>
        /// <param name="ascending">Order of sort</param>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<TodoItemVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        public IHttpActionResult GetByTags([FromBody] string[] tags, int? take, int? skip, DateTime startDate, DateTime endDate, bool? isComplete = null, string sortByColumn = null, bool ascending = true)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, take);

            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.Get(tags, paging.take, paging.skip, isComplete, null, range.StartDate, range.EndDate, sortByColumn, ascending));
            }
        }
        
        /// <summary>
        /// Retrieve an item by it's Scorecard ID.
        /// </summary>
        /// <param name="scorecardId">The scorecard ID of the ToDoItem</param>
        /// <returns></returns>
        [HttpGet]
        [Route("ByScorecard/{scorecardId:int}")]
        [ResponseType(typeof(TodoItemVm))]
        public IHttpActionResult GetReRunAuditToDo(int scorecardId)
        {
            using (var svc = new TodoItemService(UserCtx))
                return Ok(svc.GetByScorecardId(scorecardId));
        }
        
        /// <summary>
        /// Retrieves items for the logged in user.
        /// </summary>
        /// <param name="take">Number of records to take</param>
        /// <param name="skip">Number of records to skip</param>
        /// <param name="startDate">Start range for DueDate</param>
        /// <param name="endDate">End range for DueDate</param>
        /// <param name="isComplete">Search completed or incomplete items</param>
        /// <param name="sortByColumn">DueDate,CreateDate</param>
        /// <param name="ascending">Order of sort</param>
        [HttpGet]
        [AuthorizedRolesApi]
        [ResponseType(typeof(IEnumerable<TodoItemVm>))]
        [Route("ByEmployee")]
        public IHttpActionResult GetByEmployee(int? take, int? skip, DateTime startDate, DateTime endDate, bool? isComplete = null, string sortByColumn = null, bool ascending = true)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, take);

            using (var userSvc = new UserContextService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var orgUserId = userSvc.GetOrgUserId();
                return Ok(svc.Get(null, paging.take, paging.skip, isComplete, orgUserId, range.StartDate, range.EndDate, sortByColumn, ascending));
            }
        }
        
        /// <summary>
        /// Retrieves a ToDoItem assigned to the logged in User
        /// </summary>
        /// <param name="id">The ID of the item assigned</param>
        [HttpGet]
        [AuthorizedRolesApi]
        [ResponseType(typeof(TodoItemVm))]
        [Route("ByEmployee/{id:int}")]
        public IHttpActionResult GetByEmployeeAndById(int id)
        {
            using (var userSvc = new UserContextService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var orgUserId = userSvc.GetOrgUserId();
                var todoItem = svc.GetById(id);
                if (todoItem == null)
                    throw new ArgumentOutOfRangeException(nameof(id), $"TodoItem with key:{id} not found.");
                
                if (todoItem.OrgUserId != orgUserId)
                    throw new UnauthorizedAccessException($"Endpoint only allows ToDos assigned to current user (Id:{UserCtx.UserId}, OrgUserId:{orgUserId}).");

                return Ok(todoItem);
            }
        }
        
        /// <summary>
        /// Retrieves counts for items based on search criteria
        /// </summary>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        /// <param name="filterByReAudit">Limit by if Audits exist or not</param>
        /// <param name="buildingId">The specific building of the task</param>
        [HttpGet]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("AllCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetAllCounts(DateTime startDate, DateTime endDate, bool? filterByReAudit = null, int? buildingId = null)
        {
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetAllCounts(null, startDate, endDate, buildingId, null, filterByReAudit));
            }
        }
        
        /// <summary>
        /// Retrieves counts for items based on search criteria
        /// </summary>
        /// /// <param name="tags">Tags to limit search</param>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        /// <param name="filterByReAudit">Limit by if Audits exist or not</param>
        /// <param name="buildingId">The specific building of the task</param>
        [HttpPost]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("AllCountsByTags")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetAllCounts([FromBody] string[] tags, DateTime startDate, DateTime endDate, bool? filterByReAudit = null, int? buildingId = null)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetAllCounts(tags, startDate, endDate, buildingId, null, filterByReAudit));
            }
        }
        
        /// <summary>
        /// Retrieves counts for a specific employee
        /// </summary>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        /// <param name="employeeId">The specific employee to see</param>
        /// <param name="filterByReAudit">Limit by if Audits exist or not</param>
        /// <param name="buildingId">The specific building of the task</param>
        [HttpGet]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("EmployeeCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetAllCounts(DateTime startDate, DateTime endDate, int employeeId, bool? filterByReAudit = null, int? buildingId = null)
        {
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetAllCounts(null, startDate, endDate, buildingId, employeeId, filterByReAudit));
            }
        }
        
        /// <summary>
        /// Monthly based stats
        /// </summary>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsByPeriodVm>))]
        [Route("MonthlyTrend")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMonthlyTrend(DateTime startDate, DateTime endDate)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using(var userSvc = new UserContextService(UserCtx))
            using(var orgUserSvc = new OrgUserService(UserCtx))
            using(var employeeSvc = new EmployeeService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var loggedInOrgUserId = userSvc.GetOrgUserId();
                int? employeeId = null;
                
                if (!orgUserSvc.Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
                {
                    if (employeeSvc.Get(loggedInOrgUserId) != null)
                        employeeId = loggedInOrgUserId;
                }

                return Ok(svc.GetMonthlyTrend(null, range.StartDate, range.EndDate, employeeId));
            }
        }
        
        /// <summary>
        /// Monthly based stats
        /// </summary>
        /// <param name="tags">Tags to limit search</param>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ToDoCountsByPeriodVm>))]
        [Route("MonthlyTrendByTags")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMonthlyTrend([FromBody] string[] tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using(var userSvc = new UserContextService(UserCtx))
            using(var orgUserSvc = new OrgUserService(UserCtx))
            using(var employeeSvc = new EmployeeService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var loggedInOrgUserId = userSvc.GetOrgUserId();
                int? employeeId = null;
                
                if (!orgUserSvc.Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
                {
                    if (employeeSvc.Get(loggedInOrgUserId) != null)
                        employeeId = loggedInOrgUserId;
                }

                return Ok(svc.GetMonthlyTrend(tags, range.StartDate, range.EndDate, employeeId));
            }
        }
        
        /// <summary>
        /// Retrieves Counts for all employees
        /// </summary>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsByEmployeeVm>))]
        [Route("EmployeeInfo")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetEmployeeInfo(DateTime startDate, DateTime endDate)
        {
            using(var userSvc = new UserContextService(UserCtx))
            using(var orgUserSvc = new OrgUserService(UserCtx))
            using(var employeeSvc = new EmployeeService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var loggedInOrgUserId = userSvc.GetOrgUserId();
                int? employeeId = null;

                if (!orgUserSvc.Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
                {
                    if (employeeSvc.Get(loggedInOrgUserId) != null)
                        employeeId = loggedInOrgUserId;
                }

                return Ok(svc.GetEmployeeInfo(null, startDate, endDate, employeeId));
            }
        }
        
        /// <summary>
        /// Retrieves Counts for all employees
        /// </summary>
        /// <param name="tags">Tags to limit search</param>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ToDoCountsByEmployeeVm>))]
        [Route("EmployeeInfoByTags")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetEmployeeInfo([FromBody] string[] tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using(var userSvc = new UserContextService(UserCtx))
            using(var orgUserSvc = new OrgUserService(UserCtx))
            using(var employeeSvc = new EmployeeService(UserCtx))
            using (var svc = new TodoItemService(UserCtx))
            {
                var loggedInOrgUserId = userSvc.GetOrgUserId();
                int? employeeId = null;

                if (!orgUserSvc.Get(loggedInOrgUserId).Roles.Contains(Roles.Managers))
                {
                    if (employeeSvc.Get(loggedInOrgUserId) != null)
                        employeeId = loggedInOrgUserId;
                }

                return Ok(svc.GetEmployeeInfo(tags, startDate, endDate, employeeId));
            }
        }
        
        /// <summary>
        /// Counts for all sites the user has access to
        /// </summary>
        /// <param name="startDate">Start range for CreateDate</param>
        /// <param name="endDate">End range for CreateDate</param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ToDoCountsBySiteVm>))]
        [Route("SiteCounts")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetSiteCounts(DateTime startDate, DateTime endDate)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetSiteCounts(range.StartDate, range.EndDate));
            }
        }

        /// <summary>
        /// Retrieves all items that have audits
        /// </summary>
        [HttpGet]
        [ResponseType(typeof(ReAuditsInfoVm))]
        [Route("ReAuditsInfo")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        public IHttpActionResult GetReAuditsInfo()
        {
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetReAudits(null));
            }
        }

        /// <summary>
        /// Retrieves all items that have audits
        /// </summary>
        /// <param name="tags">Tags to limit search</param>
        [HttpPost]
        [ResponseType(typeof(ReAuditsInfoVm))]
        [Route("ReAuditsInfoByTags")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ToDoViewers + "," + Roles.Customers)]
        public IHttpActionResult GetReAuditsInfo([FromBody] string[] tags)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(svc.GetReAudits(tags));
            }
        }

        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching complaints</param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        [HttpPost]
        [ResponseType(typeof(ToDoSiteSummary))]
        [Route("SiteSummariesByTags")]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
         
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, range.StartDate, range.EndDate));
            }
        }
        
        /// <summary>
        /// Returns a summary of To Dos by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the To Do range</param>
        /// <param name="endDate">End of the To Do range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(PaginatedData<ToDoSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }        
        
        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching To Do's</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        [HttpPost]
        [ResponseType(typeof(ToDoSiteSummary))]
        [Route("AllSitesSummaryByTags")]
        public async Task<IHttpActionResult> GetAllSiteSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            var range = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new TodoItemService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummaryByTagsAsync(tags, range.StartDate, range.EndDate));
            }
        }
    }
}