﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.Services.ActionRequests.WorkOrders;
using ChaiTea.BusinessLogic.Services.Assessments.Complaints;
using ChaiTea.BusinessLogic.Services.Assessments.Compliments;
using ChaiTea.BusinessLogic.Services.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.Services.PurchaseOrders;
using ChaiTea.BusinessLogic.Services.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.ActionRequests.NoticedIssues;
using ChaiTea.BusinessLogic.ViewModels.Assessments.ConductIssues;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.Utils;

using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/LookupList")]
    public class LookupListController : BaseApiController
    {
        [HttpGet]
        [Route("Error")]
        public IHttpActionResult ErrorGen(string message = "")
        {
            throw new ApplicationException(message);
        }

        /// <summary>
        /// Retrieves all complaint types.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("ComplaintTypes")]
        public LookupListVm<int, string> ComplaintTypes()
        {
            return (new ComplaintTypesService(UserCtx)).Get(pct => true);
        }

        /// <summary>
        /// Retrieves all complaint types.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("ComplaintTypes/Complaint/{complaintId:int}")]
        public LookupListVm<int, string> ComplaintTypesForComplaint(int complaintId)
        {
            int programId = new ComplaintsService(UserCtx).Get(complaintId).ProgramId;

            return (new ComplaintTypesService(UserCtx)).Get(pct => pct.ProgramId == programId);
        }

        /// <summary>
        /// Retrieves all complaint types by program id.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("ComplaintTypes/Program/{programId:int}")]
        public LookupListVm<int, string> ComplaintTypesForProgram(int programId)
        {
            return (new ComplaintTypesService(UserCtx)).Get(pct => pct.ProgramId == programId);
        }

        /// <summary>
        /// Retrieves all compliment types.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("ComplimentTypes")]
        public LookupListVm<int, string> ComplimentTypes()
        {
            return (new ComplimentTypesService()).Get();
        }

        /// <summary>
        /// Retrieves all preventable statuses.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("PreventableStatuses")]
        public LookupListVm<int, string> PreventableStatuses()
        {
            return (new PreventableStatusService()).Get();
        }

        /// <summary>
        /// Retrieves all classifications.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Classifications")]
        public LookupListVm<int, string> Classifications()
        {
            return (new ChaiTea.BusinessLogic.Services.Assessments.Complaints.ClassificationService()).Get();
        }

        /// <summary>
        /// Retrieves employees for client applicable to the given complaint id; if none is provided (new complaint) client is determined from context.
        /// </summary>
        /// <param name="complaintId">Complaint ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Employees/Complaint/{complaintId:int}")]
        public LookupListVm<int, string> EmployeesForComplaint(int complaintId)
        {
            int siteId = new ComplaintsService(UserCtx).Get(complaintId).SiteId;

            return (new EmployeeService(UserCtx.OverrideSite(siteId))).Get().CreateList("SiteEmployees", k => k.EmployeeId, v => v.FirstName + " " + v.LastName);
        }

        /// <summary>
        /// Retrieves employees for client applicable to the given compliment id; if none is provided (new compliment) client is determined from context.
        /// </summary>
        /// <param name="complimentId">Compliment ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Employees/Compliment/{complimentId:int}")]
        public LookupListVm<int, string> EmployeesForCompliment(int complimentId)
        {
            int siteId = new ComplimentsService(UserCtx).Get(complimentId).SiteId;

            return (new EmployeeService(UserCtx.OverrideSite(siteId))).Get().CreateList("SiteEmployees", k => k.EmployeeId, v => v.FirstName + " " + v.LastName);
        }

        /// <summary>
        /// Retrieves employees.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Employees")]
        public LookupListVm<int, string> Employees()
        {
            return (new EmployeeService(UserCtx)).Get().CreateList("SiteEmployees", k => k.EmployeeId, v => v.FirstName + " " + v.LastName);
        }

        /// <summary>
        /// Retrieves all buildings for a given site in context.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Buildings")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult Buildings()
        {
            if (!UserCtx.SiteId.HasValue)
            { return BadRequest("Site not selected."); }
            return Ok(new BuildingService(UserCtx).Get(b => true));
        }

        /// <summary>
        /// Retrieves all buildings for a given site id.
        /// </summary>
        /// <param name="siteId">Site ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Buildings/Site/{siteId:int}")]
        public LookupListVm<int, string> Buildings(int siteId)
        {
            return new BuildingService(UserCtx, true).Get(b => b.ParentId == siteId);
        }

        /// <summary>
        /// Retrieves buildings for site applicable to the given complaint id; if none is provided (new complaint) site is determined from context.
        /// </summary>
        /// <param name="complaintId">Complaint ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Buildings/Complaint/{complaintId:int}")]
        public LookupListVm<int, string> BuildingsForComplaint(int complaintId)
        {
            int siteId = new ComplaintsService(UserCtx).Get(complaintId).SiteId;
            return Buildings(siteId);
        }

        /// <summary>
        /// Retrieves buildings for site applicable to the given compliment id; if none is provided (new compliment) site is determined from context.
        /// </summary>
        /// <param name="complimentId">Compliment ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Buildings/Compliment/{complimentId:int}")]
        public LookupListVm<int, string> BuildingsForCompliment(int complimentId)
        {
            int siteId = new ComplimentsService(UserCtx).Get(complimentId).SiteId;
            return Buildings(siteId);
        }

        /// <summary>
        /// Retrieves all floors for a given building id.
        /// </summary>
        /// <param name="buildingId">Building ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Floors/Building/{buildingId:int}")]
        public LookupListVm<int, string> Floors(int buildingId)
        {
            return new FloorService(UserCtx).Get(f => f.ParentId == buildingId);
        }

        /// <summary>
        /// Retrieves all floor areas for a given floor id.
        /// </summary>
        /// <param name="floorId">Floor ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Areas/{floorId:int}")]
        public LookupListVm<int, string> Areas(int floorId)
        {
            return new FloorAreaService(floorId, UserCtx).Get(fa => true);
        }

        /// <summary>
        /// Retrieves area classifications available for the profile/area/program combination.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("AreaClassifications/{profileId:int}/{areaId:int}/{profileSectionId:int?}")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult AreaClassifications(int profileId, int areaId, int profileSectionId = 0)
        {
            if (profileSectionId == 0) // new profile section, not created
            {
                return Ok(new AreaClassificationService(UserCtx).Get(profileId, areaId));
            }

            return Ok(new AreaClassificationService(UserCtx).Get(profileId, profileSectionId, areaId));
        }

        /// <summary>
        /// Retrieves all area classifications.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("AreaClassifications")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult AreaClassifications()
        {
            return Ok(new AreaClassificationService(UserCtx).Get());
        }

        /// <summary>
        /// Retrieves all area classification tasks.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("AreaClassificationTasks")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult AreaClassificationTasks()
        {
            var service = new AreaClassificationTaskService(UserCtx);

            return Ok(service.GetAreaClassificationTaskList());
        }

        /// <summary>
        /// Retrieves inspection items for an area classification.
        /// </summary>
        /// <param name="areaClassificationId">Area Classification ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("AreaClassificationInspectionItems/{areaClassificationId:int}")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult AreaClassificationInspectionItems(int areaClassificationId)
        {
            return Ok(new AreaClassificationInspectionItemService(areaClassificationId, UserCtx).Get());
        }

        /// <summary>
        /// Retrieves programs for the site in the user context
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Programs")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult Programs()
        {
            var list = new ProgramService(UserCtx).Get()
                    .OrderBy(p => p.Name)
                    .CreateList("Programs", e => e.Id, e => e.Name);

            return Ok(list);
        }

        /// <summary>
        /// Retrieves programs for a given site
        /// </summary>
        /// <param name="siteId">Site ID</param>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("Programs/Site/{siteId:int}")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult Programs(int siteId)
        {
            var list = new ProgramService(UserCtx.OverrideSite(siteId)).Get().OrderBy(p => p.Name)
                                .CreateList("Programs", e => e.Id, e => e.Name);
            return Ok(list);
        }

        /// <summary>
        /// Retrieves scoring profiles
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("ScoringProfiles")]
        public LookupListVm<int, string> ScoringProfiles()
        {
            return (new ScoringProfileService()).Get();
        }

        /// <summary>
        /// Retrieves all work order service types.
        /// </summary>
        /// <returns>LookupListVm</returns>
        [HttpGet]
        [Route("WorkOrderServiceTypes/{ProgramId:int}")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult WorkOrderServiceTypes(int ProgramId)
        {
            return Ok(new WorkOrderServiceTypeService().Get(p => p.ProgramId == ProgramId));
        }

        [HttpGet]
        [Route("ExemptionTypes")]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult ExemptionTypes()
        {
            return Ok(new ExemptionTypeService(UserCtx).Get().CreateList("ExemptionTypes", e => e.ExemptionTypeId, e => e.Name));
        }

        [HttpGet]
        [Route("IssueTypes")]
        [ResponseType(typeof(IEnumerable<IssueTypeVm>))]
        public IHttpActionResult IssueTypes()
        {
            return Ok(new IssueTypeService(UserCtx).Get());
        }

        [HttpGet]
        [Route("ConductTypes")]
        [ResponseType(typeof(IEnumerable<ConductTypeVm>))]
        public IHttpActionResult ConductTypes()
        {
            return Ok(new ConductTypeService(UserCtx).Get());
        }

        /// <summary>
        /// Retrieves KpiOptions KpiSubOptions
        /// </summary>
        /// <returns>IEnumerable KpiSubOptions</returns>
        [HttpGet]
        [Route("EmployeeAuditOptions")]
        [ResponseType(typeof(IEnumerable<KpiSubOptionVm>))]
        public IHttpActionResult EmployeeAuditOptions()
        {
            return Ok(new KpiSubOptionService(UserCtx).Get());
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<LookupListOption<int, string>>))]
        [Route("Jobs/ByClient/{clientId:int}")]
        public IEnumerable<LookupListOption<int, string>> JobsByClient(int clientId)
        {
            return (new TrainingEmployeeService(UserCtx.OverrideSettings(clientId, null, null))).GetSiteJobsByEmployees().CreateList(k => k.JobId, v => v.Name);
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<LookupListOption<int, string>>))]
        [Route("Jobs/BySite/{clientId:int}/{siteId:int}")]
        public IEnumerable<LookupListOption<int, string>> JobsBySite(int clientId, int siteId)
        {
            return (new TrainingEmployeeService(UserCtx.OverrideSettings(clientId, siteId, 0))).GetSiteJobsByEmployees().CreateList(k => k.JobId, v => v.Name);
        }

        [HttpGet]
        [Route("Employees/ByPosition/{clientId:int}/{siteId:int}/{jobId:int}")]
        public IEnumerable<LookupListOption<int, string>> EmployeesByPosition(int clientId, int siteId, int jobId)
        {
            return (new TrainingEmployeeService(UserCtx.OverrideSettings(clientId, siteId, 0))).Get()
                .Where(m => m.JobDescription.JobId == jobId)
                .CreateList(k => k.EmployeeId, v => v.Name);
        }

        [HttpGet]
        [Route("Courses")]
        [ResponseType(typeof(IEnumerable<LookupListOption<int, string>>))]
        public IHttpActionResult Courses()
        {
            return Ok(new CourseService(UserCtx).Get().CreateList(k => k.CourseId, v => v.Name));
        }

        [HttpGet]
        [Route("CourseTypes")]
        [ResponseType(typeof(IEnumerable<CourseTypeVm>))]
        public IHttpActionResult CourseTypes()
        {
            return Ok(new CourseTypeService(UserCtx).Get());
        }

        [HttpGet]
        [Route("Course/CourseAttachment/{id:int}")]
        [ResponseType(typeof(CourseAttachmentVm))]
        public IHttpActionResult CourseAttachment(int id)
        {
            return Ok(new CourseService(UserCtx).GetAttachmentById(id));
        }

        [HttpGet]
        [Route("Ownership/Classifications")]
        [ResponseType(typeof(IEnumerable<ClassificationVm>))]
        public IHttpActionResult OwnershipClassifications()
        {
            return Ok(new BusinessLogic.Services.ActionRequests.NoticedIssues.ClassificationService().Get());
        }

        [HttpGet]
        [Route("Ownership/RejectionTypes")]
        [ResponseType(typeof(IEnumerable<RejectionTypeVm>))]
        public IHttpActionResult RejectionTypes()
        {
            return Ok(new RejectionTypeService().Get());
        }

        [HttpGet]
        [Route("Ownership/ReportTypes")]
        [ResponseType(typeof(IEnumerable<LookupListOption<int, string>>))]
        public IHttpActionResult ReportTypes()
        {
            return Ok(new ReportTypeService().Get());
        }

        [HttpGet]
        [Route("PurchaseOrder/Manufacturers")]
        [ResponseType(typeof(KeyValuePair<int, string>))]
        public IHttpActionResult Manufacturers(string name = null)
        {
            var service = new ManufacturerService();
            return !string.IsNullOrWhiteSpace(name) ? Ok(service.Get(m => m.Name.StartsWith(name))) : Ok(service.Get());
        }

        [HttpGet]
        [Route("PurchaseOrder/Commodities")]
        [ResponseType(typeof(KeyValuePair<int, string>))]
        public IHttpActionResult Commodities(string name = null)
        {
            var service = new CommodityService();
            return !string.IsNullOrWhiteSpace(name) ? Ok(service.Get(c => c.Name.StartsWith(name))) : Ok(service.Get());
        }

        [HttpGet]
        [Route("PurchaseOrder/SubCommodities/{commodityId:int?}")]
        [ResponseType(typeof(KeyValuePair<int, string>))]
        public IHttpActionResult SubCommodities(int? commodityId = null, string name = null)
        {
            var service = new SubCommodityService();

            return !commodityId.HasValue ? Ok(service.Get()) : string.IsNullOrWhiteSpace(name)
                                         ? Ok(service.Get(s => s.Commodity.CommodityId == commodityId.Value))
                                         : Ok(service.Get(s => s.Commodity.CommodityId == commodityId.Value && s.Name.StartsWith(name)));
        }

    }
}