﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Training;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

using EmployeeCourseService = ChaiTea.BusinessLogic.Services.Courses.v1_1.EmployeeCourseService;
using EmployeeCourseSnapshotService = ChaiTea.BusinessLogic.Services.Courses.v1_1.EmployeeCourseSnapshotService;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/EmployeeCourse")]
    public class EmployeeCourse_v11Controller : BaseApiController
    {
        /// <summary>
        /// Get employee course details for a named employee
        /// </summary>
        /// <param name="employeeId">Employee Id</param>
        /// <param name="orderByDueDate">Return the list ordered by DueDate.</param>
        /// <param name="filterBySelfAssigned">Only return self-assigned courses?</param>
        /// <param name="skip">Skip this many records.</param>
        /// <param name="take">Take this many records.</param>
        /// <param name="filterByCourseCompletion">Only return completed courses?</param>
        /// <returns>An enumerable list of <see cref="EmployeeCourseDetailDTO"/> objects.</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeCourseDetailDTO>))]
        [Route("AllByEmployeeId")]
        public async Task<IHttpActionResult> GetAllByEmployeeId(int employeeId, bool orderByDueDate, bool filterBySelfAssigned, int? skip = null, int? take = null, bool? filterByCourseCompletion = null)
        {
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetCourseDetailsForEmployeeId(employeeId,
                                                                                pagingFilter,
                                                                                orderByDueDate,
                                                                                filterBySelfAssigned,
                                                                                filterByCourseCompletion));
            }
        }
        
        /// <summary>
        /// Get a summary of sites by tags
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<CourseComplianceSummary>))]
        [Route("SiteSummariesByTags")]
        public async Task<IHttpActionResult> GetSiteSummaries([FromBody] string[] tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetSummariesByTagsAsync(tags, null, startDate, endDate));
            }
        }
        
        /// <summary>
        /// Get a summary of site summaries from for date range
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<CourseComplianceSummary>))]
        [Route("AllSitesSummaryByTags")]
        public async Task<IHttpActionResult> GetAllSitesSummary([FromBody] string[] tags, DateTime startDate, DateTime endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetAllSitesSummaryByTags(startDate, endDate, null, tags));
            }
        }
        
        /// <summary>
        /// Get a summary for a specific site.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<CourseComplianceSummary>))]
        [Route("SiteSummary")]
        public async Task<IHttpActionResult> GetAllSitesSummary(int id, DateTime startDate, DateTime endDate)
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetAllSitesSummaryByTags(startDate, endDate, id));
            }
        }

        /// <summary>
        /// Get all extended course info for a specified employee
        /// </summary>
        /// <param name="employeeId">Employee (OrgUserId)</param>
        /// <param name="orderByDueDate">Order result set by dueDate?</param>
        /// <param name="skip">Skip this many records.</param>
        /// <param name="take">Take this many records.</param>
        /// <param name="filterByCourseCompletion">Filter by whether course is completed.</param>
        /// <param name="filterBySelfAssigned">Filter by self-assigned courses.</param>
        /// <returns><see cref="EmployeeCourseExtendedVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(PaginatedData<EmployeeCourseExtendedVm>))]
        [Route("AllByEmployeeId/{employeeId:int}")]
        public async Task<IHttpActionResult> GetExtendedCoursesByEmployeeId(int employeeId, bool? orderByDueDate = null, int? skip = null, int? take = null, bool? filterByCourseCompletion = null, bool? filterBySelfAssigned = null)
        {
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetAllByEmployeeId(employeeId, orderByDueDate, pagingFilter, filterByCourseCompletion, filterBySelfAssigned));
            }
        }

        /// <summary>
        /// Get course compliance stats
        /// </summary>
        /// <returns><see cref="SiteComplianceStatsDTO"/></returns>
        [HttpGet]
        [ResponseType(typeof(SiteComplianceStatsDTO))]
        [Route("ComplianceStats")]
        public IHttpActionResult GetComplianceStats()
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(employeeCourseSvc.GetSiteComplianceStats());
            }
        }

        /// <summary>
        /// Get compliance stats for employee.
        /// </summary>
        /// <param name="employeeId">Employee (OrgUserId).</param>
        /// <returns><see cref="EmployeeComplianceStatsDTO"/></returns>
        [HttpGet]
        [ResponseType(typeof(EmployeeComplianceStatsDTO))]
        [Route("EmployeeComplianceStats/{employeeId:int}")]
        public async Task <IHttpActionResult> GetEmployeeComplianceStats(int employeeId)
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetEmployeeComplianceStats(employeeId));
            }
        }

        /// <summary>
        /// Get all courses with matching course Id
        /// </summary>
        /// <param name="id">CourseId</param>
        /// <returns>An enumerable list of <see cref="EmployeeCourseVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeCourseVm>))]
        [Route("AllByCourseId/{id:int}")]
        public async Task<IHttpActionResult> GetAllByCourseId(int id)
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetAllByCourseId(id));
            }
        }

        /// <summary>
        /// Get course info for course snapshot
        /// </summary>
        /// <param name="id">EmployeeCourseId</param>
        /// <returns><see cref="EmployeeCourseExtendedVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(EmployeeCourseExtendedVm))]
        [Route("WithSnapshots/{id:int}")]
        public async Task<IHttpActionResult> GetWithSnapshots(int id)
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(await employeeCourseSvc.GetWithSnapshots(id));
            }
        }

        /// <summary>
        /// Get a list of SnapshotEmployeeCourses by EmployeeCourseId
        /// </summary>
        /// <param name="id">EmployeeCourseId</param>
        /// <returns>Enumerable list of <see cref="SnapshotEmployeeCourseVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<SnapshotEmployeeCourseVm>))]
        [Route("EmployeeCourseSnapshots/{id:int}")]
        public IHttpActionResult GetEmployeeCourseSnapshot(int id)
        {
            using (var employeeCourseSnapshotSvc = new EmployeeCourseSnapshotService(UserCtx))
            {
                return Ok(employeeCourseSnapshotSvc.GetByEmployeeCourseId(id));
            }
        }

        /// <summary>
        /// Mark a SnapshotEmployeeCourse as inactive (delete)
        /// </summary>
        /// <param name="snapshotEmployeeCourseId">SnapshotEmployeeCourse Id.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("UnAcknowledgeEmployeeCourse/{snapshotEmployeeCourseId:int}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult UnAcknowledgeEmployeeCourse(int snapshotEmployeeCourseId)
        {
            using (var employeeCourseSnapshotSvc = new EmployeeCourseSnapshotService(UserCtx))
            {
                if (employeeCourseSnapshotSvc.UnAcknowledgeEmployeeCourse(snapshotEmployeeCourseId))
                {
                    return Ok(new { IsDeleted = true });
                }
                
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
            }
        }
        
        /// <summary>
        /// Get a list of EmployeeCourse records.
        /// </summary>
        /// <returns>An enumerable list of <see cref="ConciseEmployeeCourse"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ConciseEmployeeCourse>))]
        [Route("ConciseEmployeeCourse")]
        public async Task<IHttpActionResult> GetConciseJobCourses()
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                var selectedCourses = await employeeCourseSvc.GetConciseEmployeeCourses();

                return Ok(selectedCourses);
            }
        }

        /// <summary>
        /// Get a list of EmployeeCourse records (using tags)
        /// </summary>
        /// <param name="tags">List of site tags</param>
        /// <returns>An enumerable list of <see cref="ConciseEmployeeCourse"/></returns>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ConciseEmployeeCourse>))]
        [Route("ConciseEmployeeCourseByTags")]
        public async Task<IHttpActionResult> GetConciseJobCourses([FromBody] string[] tags)
        {
            if (tags?.Any() != true)
            {
                throw new ArgumentException("Tags are required to use this endpoint.");
            }

            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                var selectedCourses = await employeeCourseSvc.GetConciseEmployeeCourses(tags);

                return Ok(selectedCourses);
            }
        }
        /// <summary>
        /// Get a list of all EmployeeCourse records.
        /// </summary>
        /// <returns>An enumerable list of <see cref="EmployeeByCourseDTO"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeByCourseDTO>))]
        [Route("EmployeeCourseInfo")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetEmployeeCourseInfo()
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(employeeCourseSvc.GetEmployeeCourseInfo());
            }
        }

        /// <summary>
        /// Get a list of all EmployeeCourse records (using tags).
        /// </summary>
        /// <param name="tags">List of site tags</param>
        /// <returns>An enumerable list of <see cref="EmployeeByCourseDTO"/></returns>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<EmployeeByCourseDTO>))]
        [Route("EmployeeCourseInfoByTags")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetEmployeeCourseInfoWithTags([FromBody] string[] tags)
        {
            if (tags?.Any() != true)
            {
                throw new ArgumentException("Tags are required to use this endpoint.");
            }

            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(employeeCourseSvc.GetEmployeeCourseInfo(tags));
            }
        }
        /// <summary>
        /// Get a list of EmployeeCourseCompliance records.
        /// </summary>
        /// <param name="employeeId">EmployeeId (OrgUserId)</param>
        /// <returns>An enumerable list of <see cref="EmployeeCourseComplianceDTO"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeCourseComplianceDTO>))]
        [Route("EmployeeCourseComplianceInfo")]
        public IHttpActionResult GetEmployeeCourseComplianceInfo(int employeeId)
        {
            using (var employeeCourseSvc = new EmployeeCourseService(UserCtx))
            {
                return Ok(employeeCourseSvc.GetEmployeeCourseComplianceInfo(employeeId));
            }
        }
    }
}