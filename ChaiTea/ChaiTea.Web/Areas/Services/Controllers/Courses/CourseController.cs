﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Course")]
    public class CourseController : BaseCrudApiController<CourseVm>
    {
        protected override Func<CourseVm, int> GetIdentifier
        {
            get { return vm => vm.CourseId; }
        }

        protected override Action<CourseVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.CourseId = id; }
        }

        protected override ICrudService<CourseVm> CreateService
        {
            get { return new CourseService(UserCtx); }
        }
    }
}