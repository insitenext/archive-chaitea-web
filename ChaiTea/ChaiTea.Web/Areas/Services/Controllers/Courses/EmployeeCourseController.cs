﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.Web.Identity;

using EmployeeCourseService = ChaiTea.BusinessLogic.Services.Courses.EmployeeCourseService;
using EmployeeCoursesService = ChaiTea.BusinessLogic.Services.Courses.EmployeeCoursesService;
using EmployeeCourseSnapshotService = ChaiTea.BusinessLogic.Services.Courses.EmployeeCourseSnapshotService;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses
{
    // Removing the previous : [AuthorizedRolesApi(Roles = Roles.Managers)]
    // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
    // the MVC version doesn't work on WebApiControllers. it was a bug. However, many of our front-end
    // pages depend on being able to access this endpoint regardless of the stated AuthorizedRoles.Roles
    // and so our program is dependent on the bug! instead of reverting, Documenting here and we'll
    // readdress as feature
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeCourse")]
    public class EmployeeCourseController : BaseCrudApiController<EmployeeCourseVm>
    {
        protected override Func<EmployeeCourseVm, int> GetIdentifier
        {
            get { return vm => vm.EmployeeCourseId; }
        }

        protected override Action<EmployeeCourseVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.EmployeeCourseId = id; }
        }

        protected override ICrudService<EmployeeCourseVm> CreateService => new EmployeeCourseService(UserCtx);

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeCourseDetailDTO>))]
        [Route("AllByEmployeeId")]
        public IHttpActionResult GetAllByEmployeeId(int employeeId, int top, int skip, bool orderByDueDate, bool filterBySelfAssigned, bool? filterByCourseCompletion = null)
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetCourseDetailsForEmployeeId(employeeId, top, skip, orderByDueDate, filterBySelfAssigned, filterByCourseCompletion));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<EmployeeCourseExtendedVm>))]
        [Route("AllByEmployeeId/{id:int}")]
        [EnableQuery]
        public IHttpActionResult GetAllByEmployeeId(int id)
        {
            return Ok(new EmployeeCourseService(UserCtx).GetAllByEmployeeId(id));
        }

        [HttpGet]
        [ResponseType(typeof(SiteComplianceStatsDTO))]
        [Route("ComplianceStats")]
        public IHttpActionResult GetComplianceStats()
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetSiteComplianceStats());
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeComplianceStatsDTO))]
        [Route("EmployeeComplianceStats/{employeeId:int}")]
        public IHttpActionResult GetEmployeeComplianceStats(int employeeId)
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetEmployeeComplianceStats(employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<EmployeeCourseVm>))]
        [Route("AllByCourseId/{id:int}")]
        public IHttpActionResult GetAllByCourseId(int id)
        {
            return Ok(new EmployeeCourseService(UserCtx).GetAllByCourseId(id));
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeCourseExtendedVm))]
        [Route("WithSnapshots/{id:int}")]
        public IHttpActionResult GetWithSnapshots(int id)
        {
            return Ok(new EmployeeCourseService(UserCtx).GetWithSnapshots(id));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<SnapshotEmployeeCourseVm>))]
        [Route("EmployeeCourseSnapshots/{id:int}")]
        public IHttpActionResult GetEmployeeCourseSnapshot(int id)
        {
            return Ok(new EmployeeCourseSnapshotService(UserCtx).GetByEmployeeCourseId(id));
        }

        [HttpPost]
        [Route("Snapshot")]
        [ResponseType(typeof(SnapshotEmployeeCourseVm))]
        public async Task<IHttpActionResult> PostSnapshot(SnapshotEmployeeCourseVm vm)
        {
            return Ok(await new EmployeeCourseSnapshotService(UserCtx).CreateAsync(vm));
        }

        [HttpPost]
        [Route("Course/Snapshot")]
        [ResponseType(typeof(SnapshotCourseVm))]
        public async Task<IHttpActionResult> PostSnapshot(SnapshotCourseVm vm)
        {
            return Ok(await new EmployeeCourseSnapshotService(UserCtx).CreateCourseSnapshot(vm));
        }

        [HttpDelete]
        [Route("UnAcknowledgeEmployeeCourse/{snapshotEmployeeCourseId:int}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult UnAcknowledgeEmployeeCourse(int snapshotEmployeeCourseId)
        {
            return Ok(new { IsDeleted = new EmployeeCourseSnapshotService(UserCtx).UnAcknowledgeEmployeeCourse(snapshotEmployeeCourseId) });
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<ConciseEmployeeCourse>))]
        [Route("ConciseEmployeeCourse")]
        public IHttpActionResult GetConciseJobCourse()
        {
            var empCourseSvc = new EmployeeCourseService(UserCtx);
            var courses = empCourseSvc.Get();
            var selectedCourses = courses.Select(g => new ConciseEmployeeCourse()
                                                      {
                                                          EmployeeCourseId = g.EmployeeCourseId,
                                                          OrgUserId = g.OrgUserId,
                                                          CourseId = g.CourseId,
                                                          Course = new ConciseCourseVm()
                                                                   {
                                                                       CourseId = g.CourseId,
                                                                       Name = g.Course.Name,
                                                                       CourseType = g.Course.CourseType,
                                                                       CourseTypeId = g.Course.CourseTypeId
                                                                   },
                                                          Employee = new ConciseEmployeeNameVm()
                                                                     {
                                                                         Name = g.Employee.Name,
                                                                     }
                                                      });

            return Ok(selectedCourses);
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeByCourseDTO>))]
        [Route("EmployeeCourseInfo")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetEmployeeCourseInfo()
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetEmployeeCourseInfo());
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeCourseComplianceDTO>))]
        [Route("EmployeeCourseComplianceInfo")]
        public IHttpActionResult GetEmployeeCourseComplianceInfo(int employeeId)
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetEmployeeCourseComplianceInfo(employeeId));
        }
    }
}