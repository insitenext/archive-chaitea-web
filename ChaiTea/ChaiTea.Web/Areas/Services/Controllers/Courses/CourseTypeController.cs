﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/CourseType")]
    public class CourseTypeController : BaseCrudApiController<CourseTypeVm>
    {
        protected override Func<CourseTypeVm, int> GetIdentifier
        {
            get { return vm => vm.CourseTypeId; }
        }

        protected override Action<CourseTypeVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.CourseTypeId = id; }
        }

        protected override ICrudService<CourseTypeVm> CreateService
        {
            get { return new CourseTypeService(UserCtx); }
        }
    }
}