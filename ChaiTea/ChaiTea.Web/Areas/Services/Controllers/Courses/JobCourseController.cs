﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses
{
    // [AuthorizedRolesApi(Roles = Roles.Managers)]
    // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
    // the MVC version doesn't work on WebApiControllers. it was a bug.
    // However, many of our front-end pages depend on being able to access this endpoint regardless
    // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
    // instead of reverting, Documenting here and we'll readdress as feature
    [RoutePrefix(WebApiConfig.UrlPrefix + "/JobCourse")]
    public class JobCourseController : BaseCrudApiController<JobCourseVm>
    {
        protected override Func<JobCourseVm, int> GetIdentifier
        {
            get { return vm => vm.JobCourseId; }
        }

        protected override Action<JobCourseVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.JobCourseId = id; }
        }

        protected override ICrudService<JobCourseVm> CreateService
        {
            get { return new JobCourseService(UserCtx); }
        }

        public class ConciseCourseVm
        {

            public int CourseId { get; set; }
            public string Name { get; set; }
            public CourseTypeVm CourseType { get; set; }
            public int CourseTypeId { get; set; }
        };

        public class ConciseJobCourseVm
        {
            public int JobCourseId { get; set; }
            public int JobId { get; set; }
            public int CourseId { get; set; }
            public ConciseCourseVm Course { get; set; }
            public string JobName { get; set; }
            public SiteVm Site { get; set; }
            public ClientVm Client { get; set; }
        };

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ConciseJobCourseVm>))]
        [Route("ConciseJobCourse")]
        public IHttpActionResult GetConciseJobCourse()
        {
            return Ok(new JobCourseService(UserCtx).Get().Select(
                g => new ConciseJobCourseVm()
                {
                    JobCourseId = g.JobCourseId,
                    JobId = g.JobId,
                    CourseId = g.CourseId,
                    Course = new ConciseCourseVm()
                    {
                        CourseId = g.CourseId,
                        Name = g.Course.Name,
                        CourseType = g.Course.CourseType,
                        CourseTypeId = g.Course.CourseTypeId
                    },
                    JobName = g.Job.Name,
                    Site = g.Site,
                    Client = g.Client
                }));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ConciseJobCourseVm>))]
        [Route("ConciseJobCourseByJobId/{jobId:int}/{clientId:int?}/{siteId:int?}")]
        public IHttpActionResult GetConciseJobCourseByJobId(int jobId, int clientId = 0, int siteId = 0)
        {
            var query = new JobCourseService(UserCtx).Get().Where(m=>m.JobId == jobId);

            if (clientId > 0)
            { query = query.Where(m => m.Client.Id == clientId); }

            if (siteId > 0)
            { query = query.Where(m => m.Site.Id == siteId); }
            

            return Ok(query.Select(
                g => new ConciseJobCourseVm()
                {
                    JobCourseId = g.JobCourseId,
                    JobId = g.JobId,
                    CourseId = g.CourseId,
                    Course = new ConciseCourseVm()
                    {
                        CourseId = g.CourseId,
                        Name = g.Course.Name,
                        CourseType = g.Course.CourseType,
                        CourseTypeId = g.Course.CourseTypeId
                    },
                    JobName = g.Job.Name,
                    Site = g.Site,
                    Client = g.Client
                }));
        }

    }
}
