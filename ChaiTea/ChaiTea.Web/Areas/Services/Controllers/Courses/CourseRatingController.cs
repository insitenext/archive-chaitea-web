﻿using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Courses
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/CourseRating")]
    public class CourseRatingController : BaseApiController
    {

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<CourseRatingVm>))]
        [EnableQuery]
        public IQueryable<CourseRatingVm> Get()
        {
            return new CourseRatingService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(CourseRatingVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new CourseRatingService(UserCtx).Get(id));
        }

        [HttpGet]
        [ResponseType(typeof(decimal))]
        [Route("AvgByCourse/{id:int}")]
        public IHttpActionResult GetAvgByCourse(int id)
        {
            return Ok(new { Average = new CourseRatingService(UserCtx).GetAvgRatingByCourse(id) });
        }

        [HttpGet]
        [ResponseType(typeof(decimal))]
        [Route("AvgByCourseAttachment/{CourseId:int}/{CourseAttachmentId:int}")]
        public IHttpActionResult GetAvgByCourseAttachment(int CourseId, int CourseAttachmentId)
        {
            return Ok(new { Average = new CourseRatingService(UserCtx).GetAvgRatingByCourseAttachment(CourseId, CourseAttachmentId) });
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(CourseRatingVm))]
        public async Task<IHttpActionResult> Post(CourseRatingVm vm)
        {
            return Ok(await new CourseRatingService(UserCtx).CreateAsync(vm));
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(CourseRatingVm))]
        public async Task<IHttpActionResult> Put(int id, CourseRatingVm vm)
        {
            return Ok(await new CourseRatingService(UserCtx).UpdateAsync(vm));
        }
    }
}