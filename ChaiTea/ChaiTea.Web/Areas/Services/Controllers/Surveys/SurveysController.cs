﻿using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.Services.Surveys;
using ChaiTea.BusinessLogic.ViewModels.Surveys;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Surveys
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Surveys")]
    public class SurveysController : BaseApiController
    {
        [HttpGet]
        [Route("Template")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(SurveyTemplateVm))]
        public IHttpActionResult GetTemplate()
        {
            return Ok(SurveyTemplateService.GetExistingOrCreateDefault(UserCtx));
        }

        [HttpPut]
        [Route("TemplateQuestions/{ProgramId:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpdateTemplateQuestions(int ProgramId, SurveyTemplateService.UpdateTemplateQuestionsDTO dto)
        {
            var sucess = SurveyTemplateService.UpdateTemplateQuestions(UserCtx.OverrideProgram(ProgramId), dto);
            if (sucess)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
        }

        [HttpGet]
        [Route("Programs")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(LookupListVm<int, string>))]
        public IHttpActionResult GetTemplatedPrograms()
        {
            return Ok(new LookupListVm<int, string>()
            {
                Name = "Program",
                Options = SurveyTemplateService.GetTemplatedPrograms(UserCtx.OverrideProgram((int?)null))
            });
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<RecipientInvitationVm>))]
        public IHttpActionResult Get(DateTime? startDate = null, DateTime? endDate = null, int? skip = 0, int? top = null, string filterByName = "", bool? filterByCompletion = null, bool? filterByPointOfContact = null)
        {
            RecipientInvitationService.CompletedStatus? _filterByCompletion = null;
            if (filterByCompletion.HasValue)
            {
                if (filterByCompletion.Value)
                { _filterByCompletion = RecipientInvitationService.CompletedStatus.Completed; }
                else
                { _filterByCompletion = RecipientInvitationService.CompletedStatus.Pending; }
            }

            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            return Ok(RecipientInvitationService.Get(UserCtx, _filterToDate, filterByName, _filterByCompletion, _filterToPage, filterByPointOfContact));
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("{token:guid}")]
        [ResponseType(typeof(RecipientInvitationVm))]
        public IHttpActionResult GetByInvitationToken(Guid token)
        {
            return Ok(RecipientInvitationService.GetByToken(UserContext.AnonymousUserContext(), token.ToString("N")));
        }

        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(void))]
        public IHttpActionResult CreateSurvey(SurveyService.CreateOrUpdateSurveyDTO dto, int? ProgramId = null)
        {
            var Uc = UserCtx;
            if(ProgramId.HasValue)
            {
                if (ProgramId.Value > 0)
                {
                    Uc = Uc.OverrideProgram(ProgramId);
                }
            }
            var success = SurveyService.CreateOrUpdate(Uc, dto);
            if (success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
        }

        
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("PendingCount")]
        public IHttpActionResult GetPendingSurveyRecipientsCount(DateTime? startDate, DateTime? endDate)
        {
            var _filterToDates = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            return Ok(new { PendingCount = RecipientInvitationService.GetPendingCount(UserCtx, _filterToDates) });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("SurveyAveragesBySiteByMonth")]
        public IHttpActionResult GetSurveysAverageBySiteByMonth(DateTime? startDate, DateTime? endDate)
        {
            var _filterToDates = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            return Ok(RecipientInvitationService.GetSurveysAverageBySiteByMonth(UserCtx, _filterToDates));
        }

        [HttpGet]
        [ResponseType(typeof(SurveyTrendsReport))]
        [Route("SurveyTrends")]
        public IHttpActionResult SurveyTrends(DateTime startDate, DateTime endDate)
        {
            var _filterToDates = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            return Ok(SurveyService.SurveyTrends(UserCtx, _filterToDates));
        }

        [HttpGet]
        [ResponseType(typeof(SurveyTrendsBySiteReport))]
        [Route("SurveyTrendsBySite")]
        public IHttpActionResult SurveyTrendsBySite(DateTime startDate, DateTime endDate)
        {
            var _filterToDates = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            return Ok(SurveyService.SurveyTrendsBySite(UserCtx, _filterToDates));
        }

        [HttpGet]
        [ResponseType(typeof(SurveyTrendsByProgramReport))]
        [Route("SurveyTrendsByProgram")]
        public IHttpActionResult SurveyTrendsByProgram(DateTime startDate, DateTime endDate)
        {
            var _filterToDates = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            return Ok(SurveyService.SurveyTrendsByProgram(UserCtx, _filterToDates));
        }

        [HttpGet]
        [ResponseType(typeof(MostRecentSurveyDashboardWidgetVm))]
        [Route("MostRecentDashboardWidget")]
        public IHttpActionResult GetMostRecentSurveyDashboardWidget()
        {
            return Ok(RecipientInvitationService.GetMostRecentSurveyDashboardWidget(UserCtx));
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("Responses")]
        public IHttpActionResult SubmitResponses(RecipientInvitationService.SubmitResponsesDTO dto)
        {
            return Ok(RecipientInvitationService.SubmitResponses(UserCtx, dto));
        }

        [HttpGet]
        [Route("Invitations/{recipientInvitationId:int}")]
        [ResponseType(typeof(RecipientInvitationVm))]
        public IHttpActionResult GetByInvitationId(int recipientInvitationId)
        {
            return Ok(RecipientInvitationService.GetById(UserCtx, recipientInvitationId));
        }

        
        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("Invitations")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult ResendInvitation(RecipientInvitationService.ResendInvitationDTO dto)
        {
            RecipientInvitationService.ResendInvitation(UserCtx, dto);
            return Ok();
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("Invitations/{recipientInvitationId:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult UpdateReportingDate(int recipientInvitationId, RecipientInvitationService.UpdateReportingDateDTO dto)
        {
            dto.recipientInvitationId = recipientInvitationId;
            var success = RecipientInvitationService.UpdateReportingDate(UserCtx, dto);
            if (success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("Invitations/{recipientInvitationId:int}")]
        [AuthorizedRolesApi(Roles = Roles.SurveyAdmins)]
        public IHttpActionResult DeleteRecipientInvitation(int recipientInvitationId)
        {
            var success = RecipientInvitationService.Delete(UserCtx, recipientInvitationId);
            if (success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
        }
    }
}