﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Surveys.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Surveys;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Surveys.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Surveys")]
    public class Surveys_v11Controller : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<RecipientInvitationVm>))]
        public IHttpActionResult Get(DateTime? startDate = null, DateTime? endDate = null, int? skip = 0, int? top = null, string filterByName = "", bool? filterByCompletion = null, bool? filterByPointOfContact = null)
        {
            RecipientInvitationService.CompletedStatus? status = null;
            if (filterByCompletion.HasValue)
            {
                status = filterByCompletion.Value
                    ? RecipientInvitationService.CompletedStatus.Completed
                    : RecipientInvitationService.CompletedStatus.Pending;
            }

            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, top);

            using (var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.Get(null, dateRange.StartDate, dateRange.EndDate, filterByName, status, paging, filterByPointOfContact));
        }
        
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<RecipientInvitationVm>))]
        public IHttpActionResult GetByTags([FromBody] string[] tags, DateTime? startDate = null, DateTime? endDate = null, int? skip = 0, int? top = null, string filterByName = "", bool? filterByCompletion = null, bool? filterByPointOfContact = null)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            RecipientInvitationService.CompletedStatus? status = null;
            if (filterByCompletion.HasValue)
            {
                status = filterByCompletion.Value
                    ? RecipientInvitationService.CompletedStatus.Completed
                    : RecipientInvitationService.CompletedStatus.Pending;
            }

            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, top);

            using (var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.Get(tags, dateRange.StartDate, dateRange.EndDate, filterByName, status, paging, filterByPointOfContact));
        }
        
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("PendingCount")]
        public IHttpActionResult GetPendingSurveyRecipientsCount(DateTime? startDate, DateTime? endDate)
        {
            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetPendingCount(null, dateRange.StartDate, dateRange.EndDate));
        }
        
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("PendingCountByTags")]
        public IHttpActionResult GetPendingSurveyRecipientsCountByTags([FromBody] string[] tags, DateTime? startDate, DateTime? endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetPendingCount(tags, dateRange.StartDate, dateRange.EndDate));
        }
        
        [HttpPost]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("MonthlySurveyAveragesBySiteByTags")]
        public IHttpActionResult GetSurveysAverageBySiteByMonthByTags([FromBody] string[] tags, DateTime? startDate, DateTime? endDate)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetSurveysAverageBySiteByMonth(tags, dateRange.StartDate, dateRange.EndDate));
        }
        
        [HttpGet]
        [ResponseType(typeof(MostRecentSurveyDashboardWidgetVm))]
        [Route("MostRecentDashboardWidget")]
        public IHttpActionResult GetMostRecentSurveyDashboardWidget()
        {
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetMostRecentSurveyDashboardWidget(null));
        }
        
        [HttpPost]
        [ResponseType(typeof(MostRecentSurveyDashboardWidgetVm))]
        [Route("MostRecentDashboardWidgetByTags")]
        public IHttpActionResult GetMostRecentSurveyDashboardWidget([FromBody] string[] tags)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetMostRecentSurveyDashboardWidget(tags));
        }
        
        [HttpGet]
        [Route("Invitations/{recipientInvitationId:int}")]
        [ResponseType(typeof(RecipientInvitationVm))]
        public IHttpActionResult GetByInvitationId(int recipientInvitationId)
        {
            using(var recipientSvc = new RecipientInvitationService(UserCtx))
                return Ok(recipientSvc.GetById(recipientInvitationId));
        }
        
       /// <summary>
        /// Returns a summary of Surveys grouped by Site ID
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching Survey</param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        /// <param name="startDate">Beginning of the Complaint range</param>
        /// <param name="endDate">End of the Complaint range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<SurveySiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new RecipientInvitationService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, dateFilter.StartDate, dateFilter.EndDate));
            }
        }

        /// <summary>
        /// Returns a Summary of surveys by Site ID
        /// </summary>
        /// <param name="id">id of the Site</param>
        /// <param name="startDate">Beginning of the Survey range</param>
        /// <param name="endDate">End of the Survey range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(IEnumerable<SurveySiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummaryAsync(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new RecipientInvitationService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }                

        /// <summary>
        /// Returns a summary of Survey
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching Survey</param>
        /// <param name="startDate">Beginning of the Complaint range</param>
        /// <param name="endDate">End of the Complaint range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllSitesSummaryByTags")]
        [ResponseType(typeof(IEnumerable<SurveySiteSummary>))]
        public async Task<IHttpActionResult> GetAllSitesSummaryByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new RecipientInvitationService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummaryByTagsAsync(tags, dateFilter.StartDate, dateFilter.EndDate));
            }
        }                
    }
}