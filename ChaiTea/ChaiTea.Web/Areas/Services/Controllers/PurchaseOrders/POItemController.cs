﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Reporting.Employment.ViewModels;
using ChaiTea.BusinessLogic.Services.PurchaseOrders;
using ChaiTea.BusinessLogic.ViewModels.PurchaseOrders;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.PurchaseOrders
{
    [AuthorizedRolesApi(Roles = Roles.SuppliesViewers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/POItem")]
    public class POItemController : BaseCrudApiController<POItemVm>
    {
        protected override Func<POItemVm, int> GetIdentifier
        {
            get { return vm => vm.POItemId; }
        }

        protected override Action<POItemVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.POItemId = id; }
        }

        protected override ICrudService<POItemVm> CreateService
        {
            get { return new POItemService(UserCtx); }
        }

        [HttpGet]
        [Route("LastUpdatedDate")]
        [ResponseType(typeof(DateTime?))]
        public IHttpActionResult GetLastUpdateDate()
        {
            return Ok(new { LastUpdatedDate = new POItemService(UserCtx).GetLastUpdateDate() });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SuppliesAmountBySiteVm>))]
        [Route("SuppliesAmountBySite")]
        public IHttpActionResult GetSuppliesAmountBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new POItemService(UserCtx).GetSuppliesAmountBySite(startDate, endDate));
        }
    }
}