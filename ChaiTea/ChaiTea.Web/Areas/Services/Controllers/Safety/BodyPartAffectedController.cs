﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/BodyPartAffected")]
    public class BodyPartAffectedController : BaseApiController
    {
        /// <summary>
        /// Create new body part entry, for AreaOfBodyAffected.
        /// </summary>
        /// <param name="vm"><see cref="BodyPartAffectedVm"/></param>
        /// <returns>BodyPartAffectedId - Id of newly created BodyPartAffected record.</returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Post(BodyPartAffectedVm vm)
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                return Ok(new { BodyPartAffectedId = await bpSvc.CreateAsync(vm) });
            }
        }

        /// <summary>
        /// List of possible body parts.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<BodyPartAffectedVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Get()
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                var bodyPart = await bpSvc.GetAsync(null, null);
                
                return Ok(bodyPart);
            }
        }

        /// <summary>
        /// Get specific BodyPartAffected record.
        /// </summary>
        /// <param name="bodyPartAffectedId">Record Id (bodyPartAffectedId)</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(BodyPartAffectedVm))]
        [Route("{bodyPartAffectedId:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Get(int bodyPartAffectedId)
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                return Ok(await bpSvc.GetAsync(bodyPartAffectedId));
            }
        }

        /// <summary>
        /// Update specific BodyPartAffected record
        /// </summary>
        /// <param name="vm"><see cref="BodyPartAffectedVm"/>The BodyPartAffected record to update.</param>
        /// <returns><see cref="BodyPartAffectedVm"/>The updated BodyPartAffected record.</returns>
        [HttpPut]
        [Route("")]
        [ResponseType(typeof(BodyPartAffectedVm))]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Put(BodyPartAffectedVm vm)
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                var affectedBodyPart = await bpSvc.UpdateAsync(vm);

                if (affectedBodyPart != null)
                {
                    return Ok(affectedBodyPart);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Delete specific BodyPartAffected record
        /// </summary>
        /// <param name="bodyPartAffectedId">Id of the BodyPartAffected record to delete.</param>
        /// <returns></returns>
        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{bodyPartAffectedId:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Delete(int bodyPartAffectedId)
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                var success = await bpSvc.DeleteAsync(bodyPartAffectedId);

                if (success)
                {
                    return Ok();
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        /// <summary>
        /// Get a list of BodyPartAffected records related to area of body (AreaOfBodyAffected).
        /// </summary>
        /// <param name="areaOfBodyAffectedId">Record Id</param>
        /// <returns>IEnumerable&lt;BodyPartAffectedVm&gt;<see cref="BodyPartAffectedVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<BodyPartAffectedVm>))]
        [Route("ByAreaOfBodyAffectedId/{areaOfBodyAffectedId:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> GetByAreaOfBodyAffectedIdAsync(int areaOfBodyAffectedId)
        {
            using (var bpSvc = new BodyPartAffectedService(UserCtx))
            {
                return Ok(await bpSvc.GetAsync(areaOfBodyAffectedId, null));
            }
        }
    }
}