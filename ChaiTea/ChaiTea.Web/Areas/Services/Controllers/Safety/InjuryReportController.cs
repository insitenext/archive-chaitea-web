﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Entities.Injuries;
using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/InjuryReport")]
    public class InjuryReportController : BaseApiController
    {
        /// <summary>
        /// Create a new InjuryReport.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns>int - The Id of the newly created InjuryReport</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Post(InjuryReportVm vm)
        {
            if (!vm.AnyWitnesses.HasValue ||
                !vm.WasPPEWorn.HasValue ||
                !vm.WereSafetyRulesFollowed.HasValue ||
                !vm.WasSafetyEquipmentProvided.HasValue ||
                !vm.WasDefectiveTool.HasValue)
            {
                throw new ArgumentNullException("Must provide non-null values for all of the following:" +
                                                "AnyWitnesses, WasPPEWorn, WereSafetyRulesFollowed, " +
                                                "WasSafetyEquipmentProvided & WasDefectiveTool.");
            }

            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(new
                          {
                              InjuryReportId = irs.Create(vm)
                          });
            }
        }

        /// <summary>
        /// Return an InjuryReport(s) matching the specified criteria.
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <param name="isCompleted">Filter on isCompleted state</param>
        /// <param name="top">Take/return this many records.</param>
        /// <param name="skip">Skip this many records before returning records.</param>
        /// <returns>IEnumerable&lt;InjuryReportCardsViewVm&gt;</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryReportCardsViewVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Get(DateTime startDate, DateTime endDate, bool isCompleted, int? top = null, int? skip = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.Get(filterToDate, filterToPage, true, null, isCompleted, null));
            }
        }

        /// <summary>
        /// Return an InjuryReport with the specified Id (if it exists).
        /// </summary>
        /// <param name="id">InjuryReportId value</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(InjuryReportVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.Get(id));
            }
        }

        /// <summary>
        /// Get the set of allowed values for InjuryClassificationId.
        /// </summary>
        /// <returns>IEnumerable&lt;KeyValuePair&lt;int,string&gt;&gt;</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<KeyValuePair<int,string>>))]
        [Route("InjuryClassifications")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetInjuryClassifications()
        {
            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.GetInjuryClassifications());
            }
        }

        /// <summary>
        /// Update the specified InjuryReport with the supplied payload data.
        /// </summary>
        /// <param name="id">InjuryReportId value</param>
        /// <param name="vm">Data to modify.</param>
        /// <returns>The modified InjuryReport as an <see cref="InjuryReportVm"/>.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPut]
        [ResponseType(typeof(InjuryReportVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Put(int id, InjuryReportVm vm)
        {
            if (!vm.AnyWitnesses.HasValue ||
                !vm.WasPPEWorn.HasValue ||
                !vm.WereSafetyRulesFollowed.HasValue ||
                !vm.WasSafetyEquipmentProvided.HasValue ||
                !vm.WasDefectiveTool.HasValue)
            {
                throw new ArgumentNullException("Must provide non-null values for all of the following:" +
                                                "AnyWitnesses, WasPPEWorn, WereSafetyRulesFollowed, " +
                                                "WasSafetyEquipmentProvided & WasDefectiveTool.");
            }

            using (var irs = new InjuryReportService(UserCtx))
            {
                var injuryReport = irs.Update(vm);

                if (injuryReport != null)
                {
                    return Ok(injuryReport);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Delete the specified InjuryReport.
        /// </summary>
        /// <param name="id">InjuryReportId value</param>
        [HttpDelete]
        [ResponseType(typeof(int))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Delete(int id)
        {
            using (var irs = new InjuryReportService(UserCtx))
            {
                var success = irs.Delete(id);

                if (success)
                {
                    return Ok(id);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        /// <summary>
        /// Get InjuryReports for all clients using the provided filter values.
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <param name="top">Take/return this many records.</param>
        /// <param name="skip">Skip this many records before returning records.</param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryReportCardsViewVm>))]
        [Route("ForAllClients")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetForAllClients(DateTime startDate, DateTime endDate, int? top = null, int? skip = null, int? siteId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.Get(filterToDate, filterToPage, false, siteId, null, (int) InjuryReportStatus.Injury_Reported));
            }
        }

        /// <summary>
        /// Get all InjuryReports, grouped by classification type using the provided filter values.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>IEnumerable&lt;InjuryClaimCountsVm&gt;</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryClaimCountsVm>))]
        [Route("InjuryByClassification")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetInjuryByClassification(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.GetInjuryByClassification(filterToDate));
            }
        }

        /// <summary>
        /// Create a (somewhat readable) unique Id to be used in injury reporting.
        /// </summary>
        /// <returns>A unique identifier : Up to 8 characters long.</returns>
        [HttpGet]
        [ResponseType(typeof(string))]
        [Route("CreateUniqueIncidentId")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult CreateUniqueIncidentId()
        {
            return Ok(new { IncidentId = InjuryReportService.CreateUniqueIncidentId() });
        }

        /// <summary>
        /// Get the employee info related to a specific InjuryReport
        /// </summary>
        /// <param name="injuryReportId">The Id of the InjuryReport.</param>
        /// <returns>InjuredEmployeeInfoVm</returns>
        [HttpGet]
        [ResponseType(typeof(InjuredEmployeeInfoVm))]
        [Route("InjuredEmployeeInfoByInjuryReportId/{injuryReportId:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetInjuredEmployeeInfoByInjuryReportId(int injuryReportId)
        {
            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.GetInjuredEmployeeInfoByInjuryReportId(injuryReportId));
            }
        }

        /// <summary>
        /// Return a enumerable list of injury classifications. 
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <param name="injuryClassificationId">The injury classification Id</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryClassificationsVm>))]
        [Route("SubInjuryByClassificationsByInjuryClassificationId")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult SubInjuryById(DateTime startDate, DateTime endDate, int injuryClassificationId)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var irs = new InjuryReportService(UserCtx))
            {
                return Ok(irs.GetSubInjuryByClassificationsByInjuryClassificationId(filterToDate, injuryClassificationId));
            }
        }
    }
}