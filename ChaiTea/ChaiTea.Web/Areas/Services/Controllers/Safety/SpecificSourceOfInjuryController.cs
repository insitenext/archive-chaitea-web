﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SpecificSourceOfInjury")]
    public class SpecificSourceOfInjuryController : BaseApiController
    {
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Post(SpecificSourceOfInjuryVm vm)
        {
            return Ok(new { SpecificSourceOfInjuryId = new SpecificSourceOfInjuryService(UserCtx).Create(vm) });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SpecificSourceOfInjuryVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get()
        {
            return Ok(new SpecificSourceOfInjuryService(UserCtx).Get(null, null));
        }

        [HttpGet]
        [ResponseType(typeof(SpecificSourceOfInjuryVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new SpecificSourceOfInjuryService(UserCtx).Get(id));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Put(SpecificSourceOfInjuryVm vm)
        {
            var success = new SpecificSourceOfInjuryService(UserCtx).Update(vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new SpecificSourceOfInjuryService(UserCtx).Delete(id);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SpecificSourceOfInjuryVm>))]
        [Route("BySourceOfInjuryCategoryId/{sourceOfInjuryCategoryId:int?}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult GetBySourceOfInjuryCategoryId(int? sourceOfInjuryCategoryId = null)
        {
            return Ok(new SpecificSourceOfInjuryService(UserCtx).Get(sourceOfInjuryCategoryId, null));
        }
    }
}