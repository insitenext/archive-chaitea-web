﻿using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AreaOfBodyAffected")]
    public class AreaOfBodyAffectedController : BaseApiController
    {
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Post(AreaOfBodyAffectedVm vm)
        {
            return Ok(new { AreaOfBodyAffectedId = new AreaOfBodyAffectedService(UserCtx).Create(vm) });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AreaOfBodyAffectedVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get()
        {
            return Ok(new AreaOfBodyAffectedService(UserCtx).Get(null));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AreaOfBodyAffectedVm>))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new AreaOfBodyAffectedService(UserCtx).Get(id));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Put(AreaOfBodyAffectedVm vm)
        {
            var success = new AreaOfBodyAffectedService(UserCtx).Update(vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new AreaOfBodyAffectedService(UserCtx).Delete(id);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}