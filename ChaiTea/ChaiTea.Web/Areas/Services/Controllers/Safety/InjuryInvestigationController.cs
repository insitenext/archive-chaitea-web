﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Entities.Injuries;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/InjuryInvestigation")]
    public class InjuryInvestigationController : BaseApiController
    {
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers)]
        public IHttpActionResult Post(InjuryInvestigationVm vm)
        {
            return Ok(new
                      {
                          InjuryInvestigationId = new InjuryInvestigationService(UserCtx).Create(vm)
                      });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryInvestigationCardsViewVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Get(DateTime startDate, DateTime endDate, bool filterByReportStatusId, int? top = null, int? skip = null, int? siteId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            return Ok(new InjuryInvestigationService(UserCtx).Get(filterToDate, filterToPage, filterByReportStatusId, siteId));
        }

        [HttpGet]
        [ResponseType(typeof(InjuryInvestigationVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new InjuryInvestigationService(UserCtx).Get(id));
        }

        [HttpPut]
        [ResponseType(typeof(InjuryInvestigationVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Put(int id, InjuryInvestigationVm vm)
        {
            var success = new InjuryInvestigationService(UserCtx).Update(vm);

            if (success != null)
            {
                return Ok(success);
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Update an injury investigation - limited update. Only modifies NoFurtherEvalNeeded and related audit fields.
        /// Only applicable if the related InjuryReport record is a InjuryClassification.Near_Miss or
        /// InjuryClassification.Non_Work_Related.<see cref="InjuryClassification"/>
        /// </summary>
        /// <param name="id">ID of injury investigation</param>
        /// <param name="noFurtherEvalNeeded">True or False indicating whether further eval is needed.</param>
        /// <returns>Updated <see cref="InjuryInvestigationVm"/></returns>        
        [HttpPut]
        [ResponseType(typeof(InjuryInvestigationVm))]
        [Route("NoFurtherEvalNeeded")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> UpdateNoFurtherEval(int id, bool noFurtherEvalNeeded)
        {
            InjuryInvestigationVm updatedInvestigation;

            using (var investigationSvc = new InjuryInvestigationService(UserCtx))
            {
                updatedInvestigation = await investigationSvc.UpdateNoFurtherEvalAsync(id, noFurtherEvalNeeded);
            }

            if (updatedInvestigation != null)
            {
                return Ok(updatedInvestigation);
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new InjuryInvestigationService(UserCtx).Delete(id);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpGet]
        [ResponseType(typeof(InjuredEmployeeInfoVm))]
        [Route("InjuredEmployeeInfoByInjuryInvestigationId/{injuryInvestigationId:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetInjuredEmployeeInfoByInjuryInvestigationId(int injuryInvestigationId)
        {
            return Ok(new InjuryInvestigationService(UserCtx).GetInjuredEmployeeInfoByInjuryInvestigationId(injuryInvestigationId));
        }
    }
}