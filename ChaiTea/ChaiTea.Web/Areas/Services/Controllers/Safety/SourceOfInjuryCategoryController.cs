﻿using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SourceOfInjuryCategory")]
    public class SourceOfInjuryCategoryController : BaseApiController
    {
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Post(SourceOfInjuryCategoryVm vm)
        {
            return Ok(new { SourceOfInjuryCategoryId = new SourceOfInjuryCategoryService(UserCtx).Create(vm) });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SourceOfInjuryCategoryVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get()
        {
            return Ok(new SourceOfInjuryCategoryService(UserCtx).Get(null));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SourceOfInjuryCategoryVm>))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new SourceOfInjuryCategoryService(UserCtx).Get(id));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Put(SourceOfInjuryCategoryVm vm)
        {
            var success = new SourceOfInjuryCategoryService(UserCtx).Update(vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new SourceOfInjuryCategoryService(UserCtx).Delete(id);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}