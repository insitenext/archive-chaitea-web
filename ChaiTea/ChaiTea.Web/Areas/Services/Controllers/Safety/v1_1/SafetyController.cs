﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.SafetyIncidents.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Personnel/Safety")]
    public class Safety_v11Controller : BaseApiController
    {
        /// <summary>
        /// Retrieves collection of Injury Reports (InjuryReportVm)
        /// </summary>
        /// <param name="take">Number of records to take</param>
        /// <param name="skip">Number of records to skip</param>
        /// <param name="startDate">Start range for DateOfInjury (or CreateDate)</param>
        /// <param name="endDate">End range for DateOfInjury (or CreateDate)</param>
        /// <param name="descending">Return data in descending order (default:true) or ascending order (false)</param>
        /// <param name="filterOnDateOfInjury">Use value in DateOfInjury (default:true) for selection and sorting or use CreateDate (false)</param>
        /// <returns>IEnumerable&lt;InjuryReportVm&gt;</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<InjuryReportVm>))]
        public async Task<IEnumerable<InjuryReportVm>> Get(DateTime startDate, DateTime endDate, int? take = null, int? skip = null, bool descending = true, bool filterOnDateOfInjury = true)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, take);
            
            using (var siSvc = new SafetyIncidentService(UserCtx))
            {
                return await siSvc.Get(null, paging, range.StartDate, range.EndDate, descending, filterOnDateOfInjury);
            }
        }

        /// <summary>
        /// Retrieves collection of Injury Reports (InjuryReportVm)
        /// </summary>
        /// <param name="tags">Tags to limit search</param>
        /// <param name="take">Number of records to take</param>
        /// <param name="skip">Number of records to skip</param>
        /// <param name="startDate">Start range for DateOfInjury (or CreateDate)</param>
        /// <param name="endDate">End range for DateOfInjury (or CreateDate)</param>
        /// <param name="descending">Return list in descending order (default: true)</param>
        /// <param name="filterOnDateOfInjury">Use value in DateOfInjury (default:true) for selection and sorting or use CreateDate (false).</param>
        /// <returns>IEnumerable&lt;InjuryReportVm&gt;</returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<InjuryReportVm>))]
        public async Task<IEnumerable<InjuryReportVm>> Get([FromBody] string[] tags, DateTime startDate, DateTime endDate, int? take = null, int? skip = null, bool descending = true, bool filterOnDateOfInjury = true)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            var paging = PagingFilter.TryValidate(skip, take);

            if (tags?.Any() != true)
            {
                throw new ArgumentException("A list of one or more tags are required to use this endpoint.");
            }
            
            using (var siSvc = new SafetyIncidentService(UserCtx))
            {
                return await siSvc.Get(tags, paging, range.StartDate, range.EndDate, descending, filterOnDateOfInjury);
            }
        }

        /// <summary>
        /// Retrieves a injury report based on injury report id 
        /// </summary>        
        /// <param name="injuryReportId">Unique Id for Injury Report.</param>
        /// <returns>InjuryReportVm</returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(InjuryReportVm))]
        public IHttpActionResult Get(int injuryReportId)
        {
            using (var siSvc = new SafetyIncidentService(UserCtx))
            {
                var result = siSvc.GetById(injuryReportId);

                return Ok(result);
            }
        }

        /// <summary>
        /// Retrieves Injury Reports based on employeeid 
        /// </summary>
        /// <param name="employeeId">Employee Id value.</param>
        /// <returns>InjuryReportVm</returns>
        [HttpGet]
        [Route("Employee/{id:int}")]
        [ResponseType(typeof(IEnumerable<InjuryReportVm>))]
        public IHttpActionResult GetByEmployeeId(int employeeId)
        {
            int orgUserId;

            using (var ucSvc = new UserContextService(UserCtx))
            {
                orgUserId = ucSvc.GetOrgUserId();
            }

            using (var siSvc = new SafetyIncidentService(UserCtx))
            {
                var result = siSvc.GetByEmployeeId(employeeId);

                if (result.EmployeeId != orgUserId)
                {
                    throw new UnauthorizedAccessException("Employees Can only view their own safety incidents.");
                }

                return Ok(result);
            }
        }

        /// <summary>
        /// Get latest injury report dates.
        /// </summary>
        /// <returns>SafetyIncidentService.SafetyIncidentDates</returns>
        [HttpGet]
        [Route("InjuryReportDates")]
        [ResponseType(typeof(SafetyIncidentService.SafetyIncidentDates))]
        public IHttpActionResult GetEmployeeConductCount(DateTime startDate, DateTime endDate)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);

            using (var siSvc = new SafetyIncidentService(UserCtx))
            {
                var result = siSvc.GetLatestIncidentDates(range);

                return Ok(result);
            }
        }
    }
}