﻿using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SpecificCauseOfInjury")]
    public class SpecificCauseOfInjuryController : BaseApiController
    {
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Post(SpecificCauseOfInjuryVm vm)
        {
            return Ok(new { SpecificCauseOfInjuryId = new SpecificCauseOfInjuryService(UserCtx).Create(vm) });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SpecificCauseOfInjuryVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get()
        {
            return Ok(new SpecificCauseOfInjuryService(UserCtx).Get(null, null));
        }

        [HttpGet]
        [ResponseType(typeof(SpecificCauseOfInjuryVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new SpecificCauseOfInjuryService(UserCtx).Get(id));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Put(SpecificCauseOfInjuryVm vm)
        {
            var success = new SpecificCauseOfInjuryService(UserCtx).Update(vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new SpecificCauseOfInjuryService(UserCtx).Delete(id);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<SpecificCauseOfInjuryVm>))]
        [Route("ByCauseOfInjuryCategoryId/{causeOfInjuryCategoryId:int?}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult GetByCauseOfInjuryCategoryId(int causeOfInjuryCategoryId)
        {
            return Ok(new SpecificCauseOfInjuryService(UserCtx).Get(causeOfInjuryCategoryId, null));
        }
    }
}