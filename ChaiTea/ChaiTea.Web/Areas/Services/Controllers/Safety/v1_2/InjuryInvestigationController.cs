﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Entities.Injuries;
using ChaiTea.BusinessLogic.Services.Injuries.v1_2;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries.v1_2;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety.v1_2
{
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_2 + "/InjuryInvestigation")]
    public class InjuryInvestigation_v12Controller : BaseApiController
    {
        /// <summary>
        /// Create a new InjuryInvestigation record.
        /// </summary>
        /// <param name="vm">Data for new record <see cref="InjuryInvestigationModel"/></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers)]
        public async Task<IHttpActionResult> Post(InjuryInvestigationModel vm)
        {
            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                return Ok(await injuryInvestigationService.CreateAsync(vm));
            }
        }

        /// <summary>
        /// Get all InjuryInvestigation records for the passed criteria.
        /// </summary>
        /// <param name="startDate">Start date range.</param>
        /// <param name="endDate">End date range.</param>
        /// <param name="filterByReportStatusId">Optionally filter on status Id value.</param>
        /// <param name="top">Return this many records.</param>
        /// <param name="skip">Skip this many records (pages).</param>
        /// <param name="siteId">Optionally filter on site Id.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<InjuryInvestigationCardsViewVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Get(DateTime startDate, DateTime endDate, bool filterByReportStatusId, int? top = null, int? skip = null, int? siteId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                return Ok(await injuryInvestigationService.GetAsync(filterToDate, filterToPage, filterByReportStatusId, siteId));
            }
        }

        /// <summary>
        /// Get a single InjuryInvestigation record, based on Id.
        /// </summary>
        /// <param name="id">Id of the record.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(InjuryInvestigationModel))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Get(int id)
        {
            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                return Ok(await injuryInvestigationService.GetByIdAsync(id));
            }
        }

        /// <summary>
        /// Update a single InjuryInvestigation record, based on Id.
        /// </summary>
        /// <param name="id">Id of record to update.</param>
        /// <param name="vm">Data for new record <see cref="InjuryInvestigationModel"/>.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(InjuryInvestigationModel))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Put(int id, InjuryInvestigationModel vm)
        {
            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                var success = await injuryInvestigationService.UpdateAsync(vm);

                if (success != null)
                {
                    return Ok(success);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Update an injury investigation - limited update. Only modifies NoFurtherEvalNeeded and related audit fields.
        /// Only applicable if the related InjuryReport record is a InjuryClassification.Near_Miss or
        /// InjuryClassification.Non_Work_Related.<see cref="InjuryClassification"/>
        /// </summary>
        /// <param name="id">ID of injury investigation</param>
        /// <param name="noFurtherEvalNeeded">True or False indicating whether further eval is needed.</param>
        /// <returns>Updated <see cref="InjuryInvestigationModel"/></returns>        
        [HttpPut]
        [ResponseType(typeof(InjuryInvestigationModel))]
        [Route("NoFurtherEvalNeeded")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> UpdateNoFurtherEval(int id, bool noFurtherEvalNeeded)
        {
            InjuryInvestigationModel updatedInvestigation;

            using (var investigationSvc = new InjuryInvestigationService(UserCtx))
            {
                updatedInvestigation = await investigationSvc.UpdateNoFurtherEvalAsync(id, noFurtherEvalNeeded);
            }

            if (updatedInvestigation != null)
            {
                return Ok(updatedInvestigation);
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Delete an InjuryInvestigation record.
        /// </summary>
        /// <param name="id">Id of record to delete.</param>
        /// <returns></returns>
        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                var success = await injuryInvestigationService.DeleteAsync(id);

                if (success)
                {
                    return Ok();
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        /// <summary>
        /// Get the Employee info associated with an InjuryInvestigation
        /// </summary>
        /// <param name="injuryInvestigationId">The Id of the InjuryInvestigation record.</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(InjuredEmployeeInfoVm))]
        [Route("InjuredEmployeeInfoByInjuryInvestigationId/{injuryInvestigationId:int}")]
        [AuthorizedRolesApi(Roles = Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetInjuredEmployeeInfoByInjuryInvestigationId(int injuryInvestigationId)
        {
            using (var injuryInvestigationService = new InjuryInvestigationService(UserCtx))
            {
                return Ok(injuryInvestigationService.GetInjuredEmployeeInfoByInjuryInvestigationId(injuryInvestigationId));
            }
        }
    }
}