using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Services.Injuries.v1_2;
using ChaiTea.BusinessLogic.ViewModels.Injuries.v1_2;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety.v1_2
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_2 + "/InjuryReport")]
    public class InjuryReport_v12Controller  : BaseApiController
    {
        /// <summary>
        /// Create a new InjuryReport.
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Create(InjuryReportEditModel model)
        {
            using (var svc = new InjuryReportService(UserCtx))
                return Ok(await svc.Create(model));
        }
        
        /// <summary>
        /// Update an InjuryReport
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(bool))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Update(int id, InjuryReportEditModel model)
        {
            if (model.InjuryReportId.GetValueOrDefault() == 0)
            {
                model.InjuryReportId = id;
            }
            else if (model.InjuryReportId != id)
            {
                throw new ArgumentException($"ID {id} does not match the supplied InjuryReport ({model.InjuryReportId})");
            }

            
            using (var svc = new InjuryReportService(UserCtx))
                return Ok(await svc.Update(model));
        }
        
        /// <summary>
        /// Delete the specified InjuryReport.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var svc = new InjuryReportService(UserCtx))
                return Ok(await svc.Delete(id));
        }
    }
}