﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Injuries;
using ChaiTea.BusinessLogic.ViewModels.Injuries;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ClaimsInvestigation")]
    public class ClaimsInvestigationController : BaseApiController
    {
        /// <summary>
        /// Creates a new claims investigation
        /// </summary>
        /// <param name="vm"><see cref="ClaimsInvestigationVm"/></param>
        /// <returns>ClaimsInvestigationId - Id of the created record.</returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Post(ClaimsInvestigationVm vm)
        {
            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                return Ok(new
                          {
                              ClaimsInvestigationId = await investigationSvc.CreateAsync(vm)
                          });
            }
        }

        /// <summary>
        /// Gets a list of claims investigations
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <param name="top">Take/return this many records.</param>
        /// <param name="skip">Skip this many records before returning records.</param>
        /// <param name="siteId">Associated site Id.</param>
        /// <returns><see cref="ClaimsInvestigationCardsViewVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ClaimsInvestigationCardsViewVm>))]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(DateTime startDate, DateTime endDate, int? top = null, int? skip = null, int? siteId = null)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                return Ok(investigationSvc.Get(filterToDate, filterToPage, siteId));
            }
        }

        /// <summary>
        /// Gets a single claims investigation
        /// </summary>
        /// <param name="id">ID of claims investigation</param>
        /// <returns><see cref="ClaimsInvestigationVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(ClaimsInvestigationVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public IHttpActionResult Get(int id)
        {
            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                return Ok(investigationSvc.Get(id));
            }
        }

        /// <summary>
        /// Update a claims investigation
        /// </summary>
        /// <param name="id">ID of claims investigation</param>
        /// <param name="vm"><see cref="ClaimsInvestigationVm"/></param>
        /// <returns>Updated <see cref="ClaimsInvestigationVm"/></returns>        
        [HttpPut]
        [ResponseType(typeof(ClaimsInvestigationVm))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Put(int id, ClaimsInvestigationVm vm)
        {
            ClaimsInvestigationVm updatedClaim;
            
            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                updatedClaim = await investigationSvc.UpdateAsync(vm);
            }

            if (updatedClaim != null)
            {
                return Ok(updatedClaim);
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Deletes a claims investigation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ResponseType(typeof(int))]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            bool deleteSuccess;
            
            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                deleteSuccess = await investigationSvc.DeleteAsync(id);
            }

            if (deleteSuccess)
            {
                return Ok(id);
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Gets recordable rates per month
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <returns><see cref="RatesVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<RatesVm>))]
        [Route("RecordableRatesPerMonth")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers + "," + Roles.EHSManagers + "," + Roles.Managers)]
        public IHttpActionResult GetRecordableRatesPerMonth(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var investigationSvc = new InjuryReportService(UserCtx))
            {
                return Ok(investigationSvc.GetRecordableRatesPerMonth(filterToDate));
            }
        }

        /// <summary>
        /// Gets safety totals
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <returns><see cref="SafetyTotalsVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<SafetyTotalsVm>))]
        [Route("SafetyTotals")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers + "," + Roles.EHSManagers + "," + Roles.Managers)]
        public IHttpActionResult GetSafetyTotals(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                return Ok(investigationSvc.GetSafetyTotals(filterToDate));
            }
        }

        /// <summary>
        /// Get claims by site
        /// </summary>
        /// <param name="startDate">Start date used for filtering.</param>
        /// <param name="endDate">End date used for filtering.</param>
        /// <returns><see cref="ClaimsInfoBySiteVm"/></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ClaimsInfoBySiteVm>))]
        [Route("ClaimsInfoBySite")]
        [AuthorizedRolesApi(Roles = Roles.ClaimsManagers + "," + Roles.EHSManagers + "," + Roles.Managers)]
        public IHttpActionResult GetClaimsInfoBySite(DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var investigationSvc = new ClaimsInvestigationService(UserCtx))
            {
                return Ok(investigationSvc.GetClaimsInfoBySite(filterToDate));
            }
        }
    }
}