﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.SafetyIncidents;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.SafetyIncidents;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Safety
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Personnel/Safety")]
    public class SafetyController : BaseApiController
    {
        /// <summary>
        /// Retrieves list of employees for creating safety issue
        /// </summary>
        /// <returns>SafetyVm</returns>
        [HttpGet]
        [Route("Employee")]
        [ResponseType(typeof(IQueryable<SafetyVm>))]
        [EnableQuery]
        public IQueryable<SafetyVm> GetByEmployeeId()
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            return new SafetyIncidentService(UserCtx).GetByEmployeeId(employeeId);
        }

        /// <summary>
        /// Retrieves safety issue based on employeeid 
        /// </summary>
        /// <param name="id">employeeid</param>
        /// <returns>SafetyVm</returns>
        [HttpGet]
        [Route("Employee/{id:int}")]
        [ResponseType(typeof(SafetyVm))]
        public IHttpActionResult GetByEmployeeId(int id)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            var result = new SafetyIncidentService(UserCtx).Get(id);

            if (result.EmployeeId != employeeId)
            {
                throw new UnauthorizedAccessException("Employees Can only view thier own safety incidents");
            }

            return Ok(result);
        }

        /// <summary>
        /// Retrieves collection of SafetyVms
        /// </summary>        
        /// <returns>SafetyVm</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<SafetyVm>))]
        [EnableQuery]
        public IQueryable<SafetyVm> Get()
        {
            return new SafetyIncidentService(UserCtx).Get();
        }

        /// <summary>
        /// Retrieves a safety issue based on safety id 
        /// </summary>        
        /// <param name="id">Safety id</param>
        /// <returns>SafetyVm</returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(SafetyVm))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new SafetyIncidentService(UserCtx).Get(id));
        }

        /// <summary>
        /// Creates a safety issue as described by the SafetyVm provided.
        /// </summary>        
        /// <param name="vm">SafetyVm</param>
        /// <returns>SafetyVm</returns>
        [HttpPost]
        [Route("")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(SafetyVm))]
        public async Task<IHttpActionResult> Post(SafetyVm vm)
        {
            if (vm == null)
            {
                return BadRequest("Safety can not be null.");
            }

            if (vm.DateOfIncident.Date > AppUtility.UtcNow().Date)
            {
                return BadRequest("Date of Incident cannot be in the future.");
            }

            using (SafetyIncidentService service = new SafetyIncidentService(UserCtx))
            {
                var newId = await service.Create(vm);

                var result = service.Get(newId);

                return Ok(result);
            }
        }

        /// <summary>
        /// Updates a safety issue as described by the SafetyVm provided.
        /// </summary>        
        /// <param name="id">safety id</param>
        /// <param name="vm">SafetyVm</param>
        /// <returns>SafetyVm</returns>
        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(SafetyVm))]
        public async Task<IHttpActionResult> Put(int id, SafetyVm vm)
        {
            if (vm == null)
            {
                return BadRequest("Safety can not be null");
            }

            if (vm.DateOfIncident.Date > AppUtility.UtcNow().Date)
            {
                return BadRequest("Date of Incident cannot be in the future.");
            }

            vm.SafetyId = id;

            using (SafetyIncidentService service = new SafetyIncidentService(UserCtx))
            {
                var success = await service.Update(vm);

                if (success)
                {
                    var newVm = service.Get(id);

                    return Ok(newVm);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Logically (soft) deletes a softy issue.
        /// </summary>        
        /// <param name="id">safety id</param>
        /// <returns>SafetyVm</returns>
        [HttpDelete]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(SafetyVm))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (SafetyIncidentService service = new SafetyIncidentService(UserCtx))
            {
                var result = await service.Delete(id);

                var employeeKpiEditable = new
                                          {
                                              IsClosed = !result
                                          };

                return Ok(employeeKpiEditable);
            }
        }

        /// <summary>
        /// Get list of employees safety count 
        /// </summary>
        /// <returns>EmployeeKpiCountVm</returns>
        [HttpGet]
        [Route("EmployeesSafetyCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        public IHttpActionResult GetEmployeeConductCount(DateTime startDate, DateTime endDate)
        {
            return Ok(new SafetyIncidentService(UserCtx).GetCountsByEmployee(startDate, endDate));
        }

        /// <summary>
        /// Gets safety counts by site
        /// </summary>
        /// <returns>SafetySiteCountVm</returns>
        [HttpGet]
        [Route("SiteCounts")]
        [ResponseType(typeof(List<SafetySiteCountVm>))]
        public IHttpActionResult GetSafetySiteCounts(DateTime startDate, DateTime endDate)
        {
            return Ok(new SafetyIncidentService(UserCtx).GetCountsBySite(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("SafetyCountBySiteByMonth")]
        public IHttpActionResult GetSafetyCountBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            return Ok(new SafetyIncidentService(UserCtx).GetSafetyCountBySiteByMonth(startDate, endDate));
        }
    }
}