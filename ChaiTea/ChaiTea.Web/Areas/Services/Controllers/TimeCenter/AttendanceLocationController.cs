﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Attendance;
using ChaiTea.BusinessLogic.Services.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.TimeCenter
{
    [AuthorizedRolesApi(Roles = Roles.AttendanceTrackerViewer + "," + Roles.AttendanceTrackerManager + "," + Roles.HoursApprover + "," + Roles.HoursManager)]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/attendance")]
    public class AttendanceLocationController : BaseApiController
    {
        /// <summary>
        /// Get summary of attendance locations
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="nameFilter"></param>
        /// <param name="statusFilter"></param>
        /// <returns>Attendance locations without times</returns>
        [HttpGet]
        [Route("employees")]
        [ResponseType(typeof(IEnumerable<AttendanceEmployeeLocationResponseModel>))]
        public IHttpActionResult EmployeesAsync(DateTime startDate, DateTime endDate, string nameFilter = null, string statusFilter = "")
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            statusFilter = statusFilter?.ToLower();

            return Ok(new AttendanceLocationService(UserCtx).GetEmployees(filterToDate, nameFilter, statusFilter));
        }

        /// <summary>
        /// Get detail of attendance locations
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="skip"></param>
        /// <param name="top"></param>
        /// <param name="nameFilter"></param>
        /// <param name="sortByColumn"></param>
        /// <param name="orderBy"></param>
        /// <param name="statusFilter"></param>
        /// <returns>Attendance locations</returns>
        [HttpGet]
        [Route("employees/detail")]
        [ResponseType(typeof(IEnumerable<AttendanceEmployeeLocationDetailResponseModel>))]
        public async Task<IHttpActionResult> EmployeesDetailAsync(DateTime startDate, DateTime endDate, int? skip = null, 
            int? top = null, string nameFilter = null, string sortByColumn = "", string orderBy = "asc", string statusFilter = "")
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            sortByColumn = sortByColumn?.ToLower();
            orderBy = orderBy?.ToLower();
            statusFilter = statusFilter?.ToLower();
            
            return Ok(await new AttendanceLocationService(UserCtx).GetEmployeesDetail(filterToDate, filterToPage, nameFilter, 
                sortByColumn, orderBy, statusFilter));
        }
    
        /// <summary>
        /// Get employee's attendance locations
        /// </summary>
        /// <param name="id">employee's id</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Attandance locations</returns>
        [HttpGet]
        [Route("employee/{id:int}")]
        [ResponseType(typeof(IEnumerable<AttendanceEmployeeLocationDetailResponseModel>))]
        public async Task<IHttpActionResult> EmployeeDetailAsync(int id, DateTime startDate, DateTime endDate)
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            
            return Ok(await new AttendanceLocationService(UserCtx).GetEmployeeDetail(id, filterToDate));
        }

        /// <summary>
        /// Get count of attendance locations
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="nameFilter"></param>
        /// <param name="statusFilter"></param>
        /// <returns>integer</returns>
        [HttpGet]
        [Route("employees/count")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> EmployeesCountAsync(DateTime startDate, DateTime endDate , string nameFilter = null, string statusFilter = "")
        {
            var filterToDate = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            statusFilter = statusFilter?.ToLower();

            return Ok(await new AttendanceLocationService(UserCtx).GetEmployeeCount(filterToDate, nameFilter, statusFilter));
        }

        /// <summary>
        /// Get a ist of valid timecard types
        /// </summary>
        /// <returns>IEnumerable of timecard types</returns>
        [HttpGet]
        [Route("TimeCardTransactionSubTypes")]
        [ResponseType(typeof(IEnumerable<string>))]
        public async Task<IHttpActionResult> GetTimeCardTypes()
        {
            using (var alSvc = new AttendanceLocationService(UserCtx))
            {
                return Ok(await alSvc.GetTimeCardTransactionSubTypes());
            }
        }

        /// <summary>
        /// Updates a comment as for a TimeCard
        /// </summary>
        /// <param name="vm">The TimeCard request model</param>
        /// <returns>status</returns>
        [HttpPut]
        [Route("Comment")]
        [AuthorizedRolesApi(Roles = Roles.HoursManager + "," + Roles.HoursApprover)]
        [ResponseType(typeof(TimeCardCommentRequestModel))]
        public async Task<IHttpActionResult> Put(TimeCardCommentRequestModel vm)
        {
            return Ok(await new AttendanceLocationService(UserCtx).UpdateAsync(vm));
        }

        /// <summary>
        /// Updates a Time Card
        /// </summary>
        /// <param name="request"></param>
        [HttpPut]
        [Route("Manual")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.HoursManager + "," + Roles.HoursApprover)]
        public async Task<IHttpActionResult> UpdateTimeCards(TimeCardUpdateRequest request)
        {
            return Ok(await new AttendanceLocationService(UserCtx).UpdateAsync(new []{request}));
        }

        /// <summary>
        /// Creates a Time Card
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [Route("Manual")]
        [ResponseType(typeof(IEnumerable<int>))]
        [AuthorizedRolesApi(Roles = Roles.HoursManager + "," + Roles.HoursApprover)]
        public async Task<IHttpActionResult> CreateTimeCards(TimeCardManualCreateRequest request)
        {
            return Ok(await new AttendanceLocationService(UserCtx).CreateManualTimeCards(new []{request}));
        }

        /// <summary>
        /// Updates Time Cards for multiple employees
        /// </summary>
        /// <param name="requests"></param>
        [HttpPut]
        [Route("ManualList")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.HoursManager + "," + Roles.HoursApprover)]
        public async Task<IHttpActionResult> UpdateTimeCardsList(IEnumerable<TimeCardUpdateRequest> requests)
        {
            return Ok(await new AttendanceLocationService(UserCtx).UpdateAsync(requests));
        }
        
        /// <summary>
        /// Creates Time Cards for multiple employees
        /// </summary>
        /// <param name="requests"></param>
        [HttpPost]
        [Route("ManualList")]
        [ResponseType(typeof(IEnumerable<int>))]
        [AuthorizedRolesApi(Roles = Roles.HoursManager + "," + Roles.HoursApprover)]
        public async Task<IHttpActionResult> CreateTimeCardsList(IEnumerable<TimeCardManualCreateRequest> requests)
        {
            return Ok(await new AttendanceLocationService(UserCtx).CreateManualTimeCards(requests));
        }        
        
        /// <summary>
        /// Delete (deactivate) a TimeCard instance. All associated TimecardSegments will also be deactivated.
        /// </summary>
        /// <param name="id">The TimeCard Id</param>
        /// <returns>true if records deleted, false if no records deleted.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If Id not found.</exception>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(IEnumerable<int>))]
        public async Task<IHttpActionResult> DeleteTimeCard(int id)
        {
            return Ok(await new AttendanceLocationService(UserCtx).DeleteTimeCard(id));
        }

        /// <summary>
        /// Retrieves summaries for sites regarding time cards. 
        /// </summary>
        /// <param name="startDate">Start Date to check Clock-In time</param>
        /// <param name="endDate">The end date to check Clock-In time</param>
        [HttpGet]
        [Route("TimeCardSummary")]
        [ResponseType(typeof(IEnumerable<TimeCardSummaryVm>))]
        public IHttpActionResult TimeCardSummary(DateTime startDate, DateTime endDate)
        {
            var dateRange = new DateTimeRequiredRange(startDate, endDate);
            using (var svc = new AttendanceLocationService(UserCtx))
            {
                return Ok(svc.GetTimeCardSummary(dateRange.StartDate, dateRange.EndDate));
            }
        }
        
        /// <summary>
        /// Gets Historical Attendance Stats By Dates
        /// </summary>
        /// <param name="dates">A list of UTC dates to calculate for</param>
        [HttpPost]
        [Route("DailyTimeCardStats")]
        [ResponseType(typeof(IEnumerable<TimeCardDailyStats>))]
        public async Task<IHttpActionResult> DailyTimeCardStats(List<DateTime> dates)
        {
            if (dates?.Any() != true)
                throw new ArgumentException("Dates are required");
            
            using (var svc = new AttendanceLocationService(UserCtx))
            {
                return Ok(await svc.GetAttendanceStatsByDays(dates));
            }
        }

        /// <summary>
        /// Finalizes time cards based on a date range
        /// </summary>
        /// <param name="startDate">Start range of ClockIns</param>
        /// <param name="endDate">End of range of ClockIns</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Finalize")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.HoursApprover)]
        public async Task<IHttpActionResult> FinalizeTimeCards(DateTime startDate, DateTime endDate)
        {
            var range = new DateTimeRequiredRange(startDate, endDate);
            using (var svc = new AttendanceLocationService(UserCtx))
            {
                return Ok(await svc.FinalizeTimeCards(range.StartDate, range.EndDate));
            }
        }
    }
}