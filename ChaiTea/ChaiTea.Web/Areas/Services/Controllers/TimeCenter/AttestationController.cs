﻿using ChaiTea.BusinessLogic.Services.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.Web.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;

namespace ChaiTea.Web.Areas.Services.Controllers.TimeCenter
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Attestation")]
    public class AttestationController : BaseApiController
    {
        /// <summary>
        /// Creates an attestation by employeeid
        /// </summary>
        /// <returns>int</returns>
        [HttpPost]
        [AuthorizedRolesApi(Roles = Roles.Users)]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> PostAttestationAsync(AttestationRequestModel request)
        {
            return Ok(await new AttestationService(UserCtx).CreateAsync(request));
        }

        /// <summary>
        /// Returns all attestations by employeeid
        /// </summary>
        /// <returns>AttestationResponseModel</returns>
        [HttpGet]
        [AuthorizedRolesApi(Roles = Roles.AttestationQuestionManager + "," + Roles.AttestationQuestionViewer)]
        [ResponseType(typeof(AttestationsResponseModel))]
        public IHttpActionResult GetAttestationByEmployeeId(int employeeId, int? skip = null, int? top = null, string status = null, string category = null)
        {
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var attestationService = new AttestationService(UserCtx))
            {
                return Ok(attestationService.GetByEmployeeId(employeeId, filterToPage, status, category));
            }
        }
        
        /// <summary>
        /// Returns all pending attestations
        /// </summary>
        /// <returns>AttestationResponseModel</returns>
        [HttpGet]
        [AuthorizedRolesApi(Roles = Roles.AttestationQuestionManager + "," + Roles.AttestationQuestionViewer)]
        [ResponseType(typeof(AttestationsResponseModel))]
        public IHttpActionResult GetPendingAttestations(int? skip = null, int? top = null, string category = null)
        {
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var attestationService = new AttestationService(UserCtx))
            {
                return Ok(attestationService.GetPending(filterToPage, category));
            }
        }

        /// <summary>
        /// Returns all attestations with filtering by status
        /// </summary>
        /// <returns>AttestationResponseModel</returns>
        [HttpGet]
        [Route("all")]
        [AuthorizedRolesApi(Roles = Roles.AttestationQuestionManager + "," + Roles.AttestationQuestionViewer)]
        [ResponseType(typeof(AttestationsResponseModel))]
        public IHttpActionResult GetAllAttestations(int? skip = null, int? top = null, string status = null, string category = null)
        {
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var attestationService = new AttestationService(UserCtx))
            {
                return Ok(attestationService.GetAll(filterToPage, status, category));
            }
        }

        /// <summary>
        /// Returns all attestations by attestationid
        /// </summary>
        /// <returns>AttestationResponseModel</returns>
        [HttpGet]
        [AuthorizedRolesApi(Roles = Roles.AttestationQuestionManager + "," + Roles.AttestationQuestionViewer)]
        [ResponseType(typeof(AttestationResponseModel))]
        public IHttpActionResult GetAttestationById(int id)
        {
            using (var attestationService = new AttestationService(UserCtx))
            {
                return Ok(attestationService.GetById(id));
            }
        }
    }
}