﻿using ChaiTea.BusinessLogic.Entities.TimeCenter;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;

namespace ChaiTea.Web.Areas.Services.Controllers.TimeCenter
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Issue")]
    public class IssueController : BaseApiController
    {
        /// <summary>
        /// Get list of employees 
        /// </summary>
        /// <returns>IssueVm</returns>
        [HttpGet]
        [Route("Employee")]
        [ResponseType(typeof(IQueryable<IssueVm>))]
        [EnableQuery]
        public IQueryable<IssueVm> GetByEmployeeId()
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            return new AttendanceIssueService(UserCtx).Get().Where(c => c.OrgUserId == employeeId);
        }

        /// <summary>
        /// Retrieves issues based on employeeid
        /// </summary>
        /// <param name="id">employeeid</param>
        /// <returns>IssueVm</returns>
        [HttpGet]
        [Route("Employee/{id:int}")]
        [ResponseType(typeof(IssueVm))]
        public IHttpActionResult GetByEmployeeId(int id)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            var result = new AttendanceIssueService(UserCtx).Get().FirstOrDefault(i => i.OrgUserId == employeeId && i.IssueId == id);
            return Ok(result);
        }

        /// <summary>
        /// Retrieves collecction of IssueVms
        /// </summary>
        /// <returns>IssueVm</returns>
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("")]
        [EnableQuery]
        [ResponseType(typeof(IQueryable<IssueVm>))]
        public IQueryable<IssueVm> Get()
        {
            return new AttendanceIssueService(UserCtx).Get();
        }

        /// <summary>
        /// Retrieves a IssueVm for the given id.
        /// </summary>
        /// <param name="id">Issue Id</param>
        /// <returns>IssueVm</returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(IssueVm))]
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult Get(int id)
        {
            return Ok(new AttendanceIssueService(UserCtx).Get(id));
        }

        /// <summary>
        /// Creates a Issue as described by the IssueVm provided.
        /// </summary>
        /// <param name="vm">IssueVm</param>
        /// <returns>IssueVm</returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(IssueVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Post(IssueVm vm)
        {
            return Ok(await new AttendanceIssueService(UserCtx).CreateAsync(vm));
        }

        /// <summary>
        /// Updates a Issue with data provided by the given IssueVm
        /// </summary>
        /// <param name="id">Issue Id</param>
        /// <param name="vm">IssueVm</param>
        /// <returns>IssueVm</returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(IssueVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Put(int id, IssueVm vm)
        {
            return Ok(await new AttendanceIssueService(UserCtx).UpdateAsync(vm));
        }

        /// <summary>
        /// Logically (soft) deletes an issue.
        /// </summary>
        /// <param name="id">Issue Id</param>
        /// <returns>IssueVm</returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(IssueVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = await new AttendanceIssueService(UserCtx).DeleteAsync(id);
            EmployeeKpiEditableCheckVm employeeKpiEditable = new EmployeeKpiEditableCheckVm()
            {
                IsClosed = !result
            };
            return Ok(employeeKpiEditable);
        }

        /// <summary>
        /// Get list of employees issues count 
        /// </summary>
        /// <returns>EmployeeKpiCountVm</returns>
        [HttpGet]
        [Route("EmployeesIssueCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeeIssueCount(DateTime startDate, DateTime endDate )
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new AttendanceIssueService(UserCtx).GetEmployeesIssueCount(dateRange));
        }

        [HttpGet]
        [ResponseType(typeof(AttendanceTabCountsVm))]
        [Route("AttendanceTabCounts")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAttendanceTabCounts(DateTime startDate, DateTime endDate, int? employeeId = null, int? issueTypeId = null, int? reasonId = null)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new AttendanceIssueService(UserCtx).GetAttendanceTabCounts(dateRange, employeeId, issueTypeId, reasonId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AttendanceAbsentReasonVm>))]
        [Route("AbsentTypesCounts")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAbsentTypesCounts(DateTime startDate, DateTime endDate)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new AttendanceIssueService(UserCtx).GetAbsentTypesCounts(dateRange));
        }
        
         
        /// <summary>
        /// Get absent reason counts grouped by month
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<AttendanceAbsentReasonMonthStats>))]
        [Route("AbsentTypeCountsByMonth")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAbsentTypesCountsByMonth(DateTime startDate, DateTime endDate)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using(var svc = new AttendanceIssueService(UserCtx))
                return Ok(svc.GetAbsentTypesCountsByMonth(dateRange));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AttendanceMonthlyTrendVm>))]
        [Route("AttendanceMonthlyTrendByReasonId")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAttendanceMonthlyTrendByReasonId(DateTime startDate, DateTime endDate, int reasonId)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new AttendanceIssueService(UserCtx).GetAttendanceMonthlyTrendByReasonId(dateRange, reasonId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AttendanceEmployeeInfoVm>))]
        [Route("AttendanceInfoPerEmployee")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAttendanceInfoPerEmployee(DateTime startDate, DateTime endDate, int top, int skip, int sortById, bool asc)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var pageFilter = PagingFilter.TryValidate(skip, top);

            return Ok(new AttendanceIssueService(UserCtx).GetAttendanceInfoPerEmployee(dateRange, pageFilter, (AttendanceSortBy)sortById, asc));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AttendanceVm>))]
        [Route("AttendanceItems")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAttendanceItems(DateTime startDate, DateTime endDate, int top, int skip, bool getAbsent, bool getPartial, int? employeeId = null, int? issueTypeId = null, int? reasonId = null)
        {
            var dateRange = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var pageFilter = PagingFilter.TryValidate(skip, top);

            return Ok(new AttendanceIssueService(UserCtx).GetAttendanceItems(dateRange, pageFilter, null, getAbsent, getPartial, employeeId, issueTypeId, reasonId));
        }

        [HttpGet]
        [ResponseType(typeof(AttendanceVm))]
        [Route("AttendanceById/{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult GetAttendanceById(int id)
        {
            return Ok(new AttendanceIssueService(UserCtx).GetAttendanceById(id));   
        }

        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastModifiedDate")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetLastModifiedDate()
        {
            return Ok(new { LastModifiedDate =  new AttendanceIssueService(UserCtx).GetLastModifiedDate() });
        }

        /// <summary>
        /// Get Issue Stats
        /// </summary>
        /// <returns>IssueStatsVm</returns>
        [HttpGet]
        [Route("IssueStats")]
        [ResponseType(typeof(IssueStatsVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetIssueStats(DateTime startDate, DateTime endDate)
        {
            var dateRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            return Ok(new AttendanceIssueService(UserCtx).GetIssueStats(dateRange));
        }
    }
}