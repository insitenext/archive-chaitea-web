﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.TimeCenter;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.TimeCenter
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TimeClock")]
    public class TimeClockController : BaseApiController
    {

        /// <summary>
        /// Retrives a TimeClockvm by employeeid
        /// </summary>
        /// <returns>TimeClockvm</returns>
        [HttpGet]
        [Route("Employee")]
        [EnableQuery]
        public IQueryable<TimeClockVm> GetByEmployeeId()
        {
            return new TimeClockService(UserCtx).GetByEmployeeId();
        }

        [HttpGet]
        [Route("Employee")]
        [ResponseType(typeof(TimeClockWithTotalVm))]
        public IHttpActionResult GetByEmployeeId(DateTime selectedDate)
        {
            var Result = new TimeClockWithTotalVm();
            var data = new TimeClockService(UserCtx).GetByEmployeeId()
                                                    .Where(tc => tc.Day == selectedDate)
                                                    .ToList();

            if (data.Any())
            {
                Result.Data = data;
                Result.TotalOfTotalTime = Math.Round(data.Sum(d => d.TotalTime), 2);
                Result.TotalOfSTE = Math.Round(data.Sum(d => d.StraightTimeEquivalent), 2);
            }

            return Ok(Result);
        }

        /// <summary>
        /// Retrives a TimeClockvm by SupervisorId
        /// </summary>
        /// <returns>TimeClockvm</returns>
        [HttpGet]
        [Route("Supervisor")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(TimeClockWithTotalVm))]
        public IHttpActionResult GetBySupervisorId(DateTime startDate, DateTime endDate, int? employeeId = null)
        {
            var Result = new TimeClockWithTotalVm();
            TimeClockService service = new TimeClockService(UserCtx);

            List<TimeClockVm> getData = employeeId == null
                    ? service.GetBySupervisorId()
                             .Where(tc => tc.Day >= startDate && tc.Day <= endDate)
                             .ToList()
                    : service.GetBySupervisorId()
                             .Where(tc => tc.Day >= startDate && tc.Day <= endDate && 
                                          tc.EmployeeId == employeeId)
                             .ToList();

            if (getData.Any())
            {
                Result.Data = getData;
                Result.TotalOfTotalTime = Math.Round(getData.Sum(d => d.TotalTime), 2);
                Result.TotalOfSTE = Math.Round(getData.Sum(d => d.StraightTimeEquivalent), 2);
            }

            return Ok(Result);
        }

        /// <summary>
        /// Retrives all TimeClockvms 
        /// </summary>
        /// <returns>TimeClockvm</returns>
        [HttpGet]
        [Route("Global")]
        [AuthorizedRolesApi(Roles = Roles.TimeclockViewers)]
        [ResponseType(typeof(TimeClockWithTotalVm))]
        public IHttpActionResult GetForAdmin(DateTime startDate, DateTime endDate, int? employeeId = null)
        {
            var Result = new TimeClockWithTotalVm();
            TimeClockService service = new TimeClockService(UserCtx);
            List<TimeClockVm> getData;

            getData = employeeId == null
                    ? service.Get().Where(tc => tc.Day >= startDate && tc.Day <= endDate).ToList()
                    : service.Get().Where(tc => tc.Day >= startDate && tc.Day <= endDate && tc.EmployeeId == employeeId).ToList();

            if (getData.Any())
            {
                Result.Data = getData;
                Result.TotalOfTotalTime = Math.Round(getData.Sum(d => d.TotalTime), 2);
                Result.TotalOfSTE = Math.Round(getData.Sum(d => d.StraightTimeEquivalent), 2);
            }

            return Ok(Result);
        }

        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdatedDate")]
        public IHttpActionResult GetLastUpdatedDate()
        {
            return Ok(new
                      {
                          LastUpdatedDate = new TimeClockService(UserCtx).GetLastUpdateDate()
                      });
        }

        [HttpPost]
        [ResponseType(typeof(int))]
        [AuthorizedRolesApi(Roles = Roles.Users)]
        [Route("Punch")]
        public async Task<IHttpActionResult> PunchAsync(TimeCardRequestModel request)
        {
            return Ok(await new TimeClockService(UserCtx).CreateAsync(request));
        }

        /// <summary>
        /// Get latest timecard segment. 
        /// </summary>
        /// <returns>TimeCardResponseModel</returns>
        [HttpGet]
        [Route("LatestPunch")]
        [ResponseType(typeof(TimeCardResponseModel))]
        [AuthorizedRolesApi(Roles = Roles.Users)]
        public IHttpActionResult GetLatestPunch()
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            return Ok(new TimeClockService(UserCtx).GetLatestPunch(employeeId));
        }
    }
}