﻿using Amazon.CloudFront;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Settings;
using ChaiTea.Web.Areas.Services.Models;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Identity;
using System;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using log4net;

namespace ChaiTea.Web.Areas.Services.Controllers.AWS
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Security")]
    public class SecurityController : BaseApiController
    {
        public static readonly ILog Logger = LogManager.GetLogger(nameof(SecurityController));

        /// <summary>
        /// Retrieves credentials
        /// </summary>
        /// <returns>SecurityVm</returns>
        [HttpGet]
        [Route("Aws/Credentials/")]
        [ResponseType(typeof(SecurityVm))]
        public IHttpActionResult GetCredentials()
        {
            try
            {
                var tempCredentials = AwsClient.GetTemporaryCredentials(AwsSettings.AccessKeyId, AwsSettings.SecretAccessKey).GetCredentials();

                return Ok(new SecurityVm
                          {
                              AccessKey = tempCredentials.AccessKey,
                              AccessSecret = tempCredentials.SecretKey,
                              AccessToken = tempCredentials.Token,
                              Endpoint = new EndpointVm()
                                         {
                                             Bucket = AwsSettings.Bucket,
                                             Region = AwsSettings.S3Region,
                                             Cdn = AwsSettings.CloudfrontUrl
                                         }
                          });
            }
            catch (Exception ex)
            {
                Logger.Error($"Error getting temporary credentials for AWS. Message: \"{ex.Message}\" " +
                             $"Endpoint: Bucket:{AwsSettings.Bucket}, Region:{AwsSettings.S3Region}, Cdn:{AwsSettings.CloudfrontUrl}.");
                throw;
            }
        }

        [HttpGet]
        [Route("Aws/SignedCloudfrontUrl/{folder}/{ext}/{file}")]
        [ResponseType(typeof(SecuritySignedUrlVm))]
        public IHttpActionResult GetSignedVideoUrl(string folder, string ext, string file)
        {

            if (string.IsNullOrEmpty(folder))
            {
                throw new ArgumentNullException(nameof(folder), "folder must be provided");
            }

            if (string.IsNullOrEmpty(file))
            {
                throw new ArgumentNullException(nameof(file), "file must be provided");
            }

            string keyPairId = AwsSettings.CloudfrontKeyPairId;
            string key = folder + '/' + file + '.' + ext;

            return Ok(new SecuritySignedUrlVm
                      {
                          Url = AmazonCloudFrontUrlSigner.GetCannedSignedURL("https://" + AwsSettings.CloudfrontSignedUrl + "/" + key,
                                                                             File.OpenText(HttpContext.Current.Server.MapPath("~/App_Data/" + keyPairId + ".pem")),
                                                                             keyPairId,
                                                                             AppUtility.UtcNow().AddMinutes(30)),
                          EndPoint = AwsSettings.Bucket,
                          Key = key
                      });
        }

        /// <summary>
        /// Retrieves a signatureSignedURL based on folder,ext,file
        /// <param name="ext">the ext</param>
        /// <param name="file">the file</param>
        /// <param name="folder">the folder</param>
        /// </summary>
        /// <returns>SecuritySignedUrlVm</returns>
        [HttpGet]
        [Route("Aws/SignedUrl/{folder}/{ext}/{file}")]
        [ResponseType(typeof(SecuritySignedUrlVm))]
        public IHttpActionResult GetSignedUrl(string folder, string ext, string file)
        {

            if (string.IsNullOrEmpty(folder))
            {
                throw new ArgumentNullException(nameof(folder), "folder must be provided");
            }

            if (string.IsNullOrEmpty(file))
            {
                throw new ArgumentNullException(nameof(file), "file must be provided");
            }

            string endPoint = AwsSettings.Bucket;
            string key = folder + '/' + file + '.' + ext;

            return Ok(new SecuritySignedUrlVm
                      {
                          Url = AwsClient.GeneratePreSignedURL(endPoint, key),
                          EndPoint = endPoint,
                          Key = key
                      });
        }
    }
}
