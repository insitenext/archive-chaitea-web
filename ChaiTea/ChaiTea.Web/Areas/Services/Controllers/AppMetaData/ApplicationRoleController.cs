﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ApplicationRole")]
    public class ApplicationRoleController : BaseCrudApiController<ApplicationUserRoleVm>
    {
        protected override Func<ApplicationUserRoleVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<ApplicationUserRoleVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<ApplicationUserRoleVm> CreateService
        {
            get { return new RoleService(UserCtx); }
        }
    }
}