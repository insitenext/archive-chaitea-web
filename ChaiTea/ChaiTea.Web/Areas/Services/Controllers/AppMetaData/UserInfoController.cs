﻿using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Utils.Localization;
using ChaiTea.BusinessLogic.Services.Utils.Translation;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.Utils.Translation;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserInfo")]
    public class UserInfoController : BaseCrudApiController<UserInfoVm>
    { 
        protected override Func<UserInfoVm, int> GetIdentifier
        {
            get { return e => e.UserId; }
        }

        protected override Action<UserInfoVm, int> SetIdentifier
        {
            get { return (e, i) => e.UserId = i; }
        }

        protected override ICrudService<UserInfoVm> CreateService
        {
            get { return new UserInfoService(UserCtx); }
        }

        /// <summary>
        /// Updates a UserInfo with data provided by the given UserInfoVm
        /// </summary>
        /// <param name="id">Unique identifier of the UserInfo to be updated.</param>
        /// <param name="vm">UserInfoVm</param>
        /// <param name="CultureId"></param>
        /// <param name="LanguageId"></param>
        /// <returns>IHttpActionResult</returns>
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UserInfoVm vm, string CultureId, string LanguageId)
        {
            LanguageVm lang = null;
            if (!string.IsNullOrWhiteSpace(LanguageId))
            {
                int lid = 0;
                if (!Int32.TryParse(LanguageId, out lid))
                {
                    lang = new LanguageService(UserCtx).Get(lid);
                    if (lang == null)
                    { return BadRequest("provided Language was not found"); }
                }
                else
                {
                    lang = new LanguageService(UserCtx).Get().FirstOrDefault(l => l.Code == LanguageId);
                    if (lang == null)
                    { return BadRequest("provided Language was not found"); }
                }
            }
            else if (!string.IsNullOrWhiteSpace(CultureId))
            {
                int cid = 0;
                if (!Int32.TryParse(CultureId, out cid))
                {
                    var culture = new CultureService(UserCtx).Get(cid);
                    if(culture == null)
                    { return BadRequest("Provided Culture not found"); }

                    lang = new LanguageService(UserCtx).Get().FirstOrDefault(l => l.Code == culture.Code);
                    if(lang == null)
                    { return BadRequest("Culture was found, but could not Convert it to language"); }
                }
                else
                {
                    var culture = new CultureService(UserCtx).Get().FirstOrDefault(c => c.Code == CultureId);
                    if (culture == null)
                    { return BadRequest("Provided Culture not found"); }

                    lang = new LanguageService(UserCtx).Get().FirstOrDefault(l => l.Code == culture.Code);
                    if (lang == null)
                    { return BadRequest("Culture was found, but could not Convert it to language"); }
                }
            }

            if (lang != null)
            {
                var newCtx = new UserContext(UserCtx.UserId).OverrideLanguage(lang.LanguageId);
                new UserContextService(UserCtx).SaveContextInDB(newCtx);
            }

            return await base.Put(id, vm);
        }

        [HttpDelete]
        [Route("Attachments")]
        public IHttpActionResult DeleteAttachment(string uniqueId)
        {
            var success = new UserInfoService(UserCtx).DeleteAttachment(uniqueId);
            if (success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
        }
    }
}
