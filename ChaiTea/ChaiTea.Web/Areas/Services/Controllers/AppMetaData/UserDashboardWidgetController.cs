﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserDashboardWidget")]
    public class UserDashboardWidgetController : BaseApiController
    {
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Post(UserDashboardWidgetVm vm)
        {
            if (vm.UserId != UserCtx.UserId)
            {
                throw new UnauthorizedAccessException($"Users are only allowed to configure their own widgets.");
            }
            return Ok(new UserDashboardWidgetService(UserCtx).Create(vm));
        }

        [HttpGet]
        [ResponseType(typeof(UserDashboardWidgetVm))]
        [Route("{id:int}")]
        public IHttpActionResult Get(int id)
        {
            return Ok(new UserDashboardWidgetService(UserCtx).Get(id));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserDashboardWidgetVm>))]
        [Route("ByUser/{userId:int}")]
        public IHttpActionResult GetByUser(int userId)
        {
            return Ok(new UserDashboardWidgetService(UserCtx).Get(null,userId,null,null));
        }
        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserDashboardWidgetVm>))]
        [Route("")]
        public IHttpActionResult GetByDashboardUser(int? userId, int? dashboardId)
        {
            return Ok(new UserDashboardWidgetService(UserCtx).Get(null, userId, null, dashboardId));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        public IHttpActionResult Put(int id, UserDashboardWidgetVm vm)
        {

            if (vm.UserId != UserCtx.UserId)
            {
                throw new UnauthorizedAccessException($"Users are only allowed to configure their own widgets.");
            }
            var success = new UserDashboardWidgetService(UserCtx).Update(vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            var returnValue = new UserDashboardWidgetService(UserCtx).Delete(id);
            if (returnValue)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}