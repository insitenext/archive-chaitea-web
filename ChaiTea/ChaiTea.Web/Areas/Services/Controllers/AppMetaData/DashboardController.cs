﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Dashboard")]
    public class DashboardController : BaseApiController
    {
        /// <summary>
        /// Gets dashboard stats
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>DashboardVm</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<DashboardVm>))]
        [Route("DashboardCount")]
        public IHttpActionResult Get(DateTime startDate, DateTime endDate)
        {
            return Ok(new DashboardService(UserCtx).GetDashboardStats(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(QualityDashboardTrendsLastUpdatedVm))]
        [Route("QualityTrendsLastUpdatedDates")]
        public IHttpActionResult GetQualityTrendsLastUpdatedDates()
        {
            return Ok(new DashboardService(UserCtx).GetQualityDashboardTrendsLastUpdated());
        }
    }
}