﻿using ChaiTea.BusinessLogic.AppMessaging.Messages;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Settings;
using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;
using ChaiTea.Web.Messenger;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ApplicationUser")]
    public class ApplicationUserController : BaseApiController
    {
        private ApplicationDbContext _db;
        protected ApplicationDbContext DbCtx
        {
            get
            {
                if (_db == null)
                {
                    _db = HttpContext.Current
                                    ?.GetOwinContext()
                                    ?.Get<ApplicationDbContext>();
                    if (_db == null)
                    {
                        _db = new ApplicationDbContext();
                    }
                }
                return _db;
            }
        }

        private ApplicationUserManager _manager;
        protected ApplicationUserManager UserManager
        {
            get
            {
                if (_manager == null)
                {
                    _manager = HttpContext.Current
                                    ?.GetOwinContext()
                                    ?.GetUserManager<ApplicationUserManager>();
                }
                return _manager;
            }
        }

        [HttpGet]
        [Route("Auditors")]
        [EnableQuery]
        public IQueryable<BaseApplicationUserVm> GetAuditors()
        {
            return new ScorecardService(UserCtx).GetAuditUsers();
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<ApplicationUserVm>))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult Get(int top, int skip)
        {
            return Ok(new ApplicationUserService().Get(top, skip));
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<ApplicationUserVm>))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult Get(int top, int skip, string phrase)
        {
            return Ok(new ApplicationUserService().Get(top, skip, phrase));
        }

        [HttpGet]
        [Route("Client")]
        [ResponseType(typeof(IEnumerable<ApplicationUserClientVm>))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult GetClient(int id)
        {
            return Ok(new ApplicationUserService().GetClient(id));
        }

        /// <summary>
        /// This Function is wide open (without authentication) due to 'requirements' by the RaizLabs mobile team
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        [Route("UserInfo")]
        public IHttpActionResult GetUserInfo(string Username = null, string Email = null, string ExternalEmployeeId = null)
        {
            return Ok(ApplicationUserService.GetUserListItems(Username, Email, ExternalEmployeeId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> Post(ApplicationUserVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var now = AppUtility.UtcNow();
            var UserId = this.UserCtx.UserId;
            ChaiTea.BusinessLogic.Entities.AppMetaData.User newUser;
            using (var db = DbCtx)
            {
                // Perform validation and alert UI

                if (string.IsNullOrEmpty(vm.UserName))
                {
                    throw new ArgumentNullException(nameof(vm.UserName), "Username is required.");
                }

                if (string.IsNullOrEmpty(vm.Password))
                {
                    throw new ArgumentNullException(nameof(vm.Password), "Password is required.");
                }

                if (db.Users.Any(u => u.IsActive && u.UserName == vm.UserName))
                {
                    throw new ArgumentOutOfRangeException(nameof(vm.UserName), "Username already exists.");
                }

                if (db.Users.Any(u => u.IsActive && u.Email == vm.Email))
                {
                    throw new ArgumentOutOfRangeException(nameof(vm.Email), "Email already exists.");
                }

                // Create initial User structure

                newUser = new BusinessLogic.Entities.AppMetaData.User()
                          {
                              UserName = vm.UserName.ToLower(),
                              Email = vm.Email.ToLower(),
                              FirstName = vm.FirstName,
                              LastName = vm.LastName,
                              SecurityStamp = Guid.NewGuid().ToString(),
                              ActiveLanguageId = vm.ActiveLanguageId,
                              OrgUsers = vm.Clients.Select(c => new BusinessLogic.Entities.AppMetaData.OrgUser()
                                                                {
                                                                    OrgId = c.Id,
                                                                    IsActive = true,
                                                                    CreateById = UserId,
                                                                    CreateDate = now
                                                                }).ToList(),
                              IsActive = true,
                              CreateById = UserId,
                              CreateDate = now
                          };

                var result = await UserManager.CreateAsync(newUser, vm.Password);

                if (!result.Succeeded)
                {
                    throw new ApplicationException("Error creating user: " + string.Join(",", result.Errors));
                }

                await db.SaveChangesAsync();

            }

            // Finalize User structure (clients and roles) in the ServiceBus
            var uiMessage = new UserInfoMessage
                            {
                                UserId = newUser.UserId,
                                MessageSentByUserId = UserId,
                                UserName = vm.UserName,
                                Email = vm.Email,
                                FirstName = vm.FirstName,
                                LastName = vm.LastName,
                                ActiveLanguageId = vm.ActiveLanguageId,
                                PhoneNumber = vm.PhoneNumber,
                                Clients = new List<UserInfoMessage.ApplicationUserClient>()
                            };

            foreach (var client in vm.Clients)
            {
                var auClient = new UserInfoMessage.ApplicationUserClient
                               {
                                   ClientId = client.Id,
                                   Roles = new List<int>()
                               };

                foreach (var role in client.Roles)
                {
                    auClient.Roles.Add(role.Id);
                }

                uiMessage.Clients.Add(auClient);
            }

            BusinessLogic.AppMessaging.Helpers.BusMessenger.Send(uiMessage);

            vm.Id = newUser.UserId;

            return Ok(vm);
        }

        /// <summary>
        /// Updates a user with data provided by the given ApplicationUserVm
        /// </summary>
        /// <param name="id">Unique identifier of the user to be updated.</param>
        /// <param name="vm">ApplicationUserVm</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult Put(int id, ApplicationUserVm vm)
        {

            if (DbCtx.Set<BusinessLogic.Entities.AppMetaData.User>().Any(u => u.IsActive && u.UserName == vm.UserName && u.UserId != vm.Id))
            {
                throw new ArgumentOutOfRangeException(nameof(vm.UserName), "Username already exists.");
            }

            if (DbCtx.Set<BusinessLogic.Entities.AppMetaData.User>().Any(u => u.IsActive && u.Email == vm.Email && u.UserId != vm.Id))
            {
                throw new ArgumentOutOfRangeException(nameof(vm.Email), "Email already exists.");
            }

            var dbUser = DbCtx.Set<BusinessLogic.Entities.AppMetaData.User>().FirstOrDefault(u => u.UserId == id);
            var now = AppUtility.UtcNow();
            var UserId = UserCtx.UserId;

            if (dbUser == null || dbUser.UserId != id)
            { throw new KeyNotFoundException("User with given id not found"); }

            dbUser.UpdateById = UserId;
            dbUser.UpdateDate = now;

            string newHash = null;

            if (!string.IsNullOrEmpty(vm.Password))
            {
                newHash = UserManager.PasswordHasher.HashPassword(vm.Password);
            }

            var uiMessage = new UserInfoMessage
                            {
                                UserId = dbUser.UserId,
                                MessageSentByUserId = UserId,
                                UserName = vm.UserName,
                                Email = vm.Email,
                                FirstName = vm.FirstName,
                                LastName = vm.LastName,
                                ActiveLanguageId = vm.ActiveLanguageId,
                                PhoneNumber = vm.PhoneNumber,
                                PasswordHash = newHash,
                                Clients = new List<UserInfoMessage.ApplicationUserClient>()
                            };

            foreach (var client in vm.Clients)
            {
                var auClient = new UserInfoMessage.ApplicationUserClient
                               {
                                   ClientId = client.Id,
                                   Roles = new List<int>()
                               };

                foreach (var role in client.Roles)
                {
                    auClient.Roles.Add(role.Id);
                }

                uiMessage.Clients.Add(auClient);
            }

            BusinessLogic.AppMessaging.Helpers.BusMessenger.Send(uiMessage);

            return Ok();
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("UpdatePassword/{userId:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult UpdatePassword(int userId, UserInfoDTO dto)
        {
            if (dto.UserId != UserCtx.UserId)
            {
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }

            dto.PasswordHash = UserManager.PasswordHasher.HashPassword(dto.Password);
            var success = new UserInfoService(UserCtx).UpdatePassword(dto);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }

        public class ForgotPasswordDTO
        {
            public string email { get; set; }
        }

        [HttpPost]
        [Route("ForgotPassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordDTO dto)
        {
            if(dto == null)
            {
                throw new ArgumentNullException(nameof(dto), "Payload with email address required");
            }
            var user = await UserManager.FindByEmailAsync(dto.email);
            if(user == null || !user.IsActive)
            { return Ok(); } // if active user with associated email not found, return OK for security reasons
            
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.UserId);
            var href = "https://" + EnvironmentSettings.WebDomain + "/reset-password" + "?resetToken=" + code + "&UserId=" + user.UserId;

            var message = $@"<div>Hello!</div>
                                          <br/>
                                          <div>To reset the password for your account, please click on the link below.
                                               If you didn't request a password reset, you can safely ignore this email.
                                          </div>
                                          <br/>
                                          <a href=""{href}"">{href}</a>
                                          <br/>
                                          <br/>
                                          <div>Have a wonderful day.</div>";

            await EmailMessenger.SendEmailAsync(new BusinessLogic.ViewModels.Utils.MailMessageVm()
                                                {
                                                    Subject = "Insite Password reset link",
                                                    Message = message,
                                                    ToEmail = user.Email,
                                                    ToName = user.FirstName
                                                });
            return Ok();
        }

        public class ResetPasswordDTO
        {
            public int UserId { get; set; }
            public string resetToken { get; set; }
            public string password { get; set; }
        }

        [HttpPost]
        [Route("ResetPassword")]
        [AllowAnonymous]
        public IHttpActionResult ResetPassword(ResetPasswordDTO dto)
        {
            if(dto == null)
            {
                throw new ArgumentNullException(nameof(dto), $"payload is required");
            }

            var result = UserManager.ResetPassword(dto.UserId, dto.resetToken, dto.password);
            if (result.Succeeded)
            {
                return Ok();
            }

            throw new ArgumentOutOfRangeException(nameof(dto.resetToken), $"ResetPassword failed for the following reasons: [{String.Join(",", result.Errors)}]");
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("Logout")]
        [AuthorizedRolesApi]
        public IHttpActionResult Logout()
        {
            HttpContext.Current?.GetOwinContext()?.Authentication?.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.Current?.GetOwinContext()?.Authentication?.SignOut();

            UserManager.UpdateSecurityStamp(User.Identity.GetUserId<int>());

            return Ok();
        }
    }
}
