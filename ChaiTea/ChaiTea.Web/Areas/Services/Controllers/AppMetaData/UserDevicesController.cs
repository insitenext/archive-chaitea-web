﻿using ChaiTea.BusinessLogic.AppUtils.Abstract;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Entities.AppMetaData;
using ChaiTea.BusinessLogic.Messengers;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;
using ChaiTea.Web.Messenger;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserDevices")]
    public class UserDevicesController : BaseApiController
    {
        private ApplicationDbContext _dbCtx;
        protected ApplicationDbContext DbCtx =>
                    _dbCtx ?? (_dbCtx = HttpContext.Current
                                                   .GetOwinContext()
                                                   .Get<ApplicationDbContext>() ?? new ApplicationDbContext());
        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("SendMessage")]
        public async Task<IHttpActionResult> SendMessage(string subj, string message)
        {
            int userId = UserContextService.CurrentUserId;
            var devices = DbCtx.Set<UserDevice>()
                              .Where(t => t.IsActive)
                              .Where(t => t.UserId == userId)
                              .ToList();
            foreach(var device in devices)
            {
                var status = MobileMessenger.SendMessage(device.DeviceEndpoint, subj, message);
                if(status == MobileMessenger.SendMessageResponse.EndpointDisabled)
                {
                    device.IsActive = false;
                    device.UpdateById = BusinessLogic.Entities.AppMetaData.User.SystemUser().UserId;
                    device.UpdateDate = AppUtility.UtcNow();
                    await DbCtx.SaveChangesAsync();
                }
            }
            return Ok();
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<UserDeviceDetailDTO>))]
        public IHttpActionResult Get()
        {
            int userId = UserContextService.CurrentUserId;
            var devices = DbCtx.Set<UserDevice>()
                               .Where(t => t.IsActive)
                               .Where(t => t.UserId == userId)
                               .ToList()
                               .Select(t => new UserDeviceDetailDTO()
                               {
                                   UserDeviceId = t.UserDeviceId,
                                   DeviceEndpoint = t.DeviceEndpoint,
                                   Platform = System.Enum.GetName(typeof(DevicePlatform), t.Platform)
                               });
            return Ok(devices);
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(UserDeviceDetailDTO))]
        public IHttpActionResult Get(int id)
        {  
            var device = DbCtx.Set<UserDevice>()
                              .Where(t => t.IsActive)
                              .Where(t => t.UserDeviceId == id)
                              .FirstOrDefault();
            if(device == null)
            { throw new KeyNotFoundException("Entity was not found"); }

            int userId = UserContextService.CurrentUserId;
            if (device.UserId != userId)
            { throw new UnauthorizedAccessException("User may only view thier own devices"); }

            return Ok(new UserDeviceDetailDTO()
            {
                UserDeviceId = device.UserDeviceId,
                DeviceEndpoint = device.DeviceEndpoint,
                Platform = System.Enum.GetName(typeof(DevicePlatform), device.Platform)
            });
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserDeviceDetailDTO))]
        public async Task<IHttpActionResult> Post(UserDeviceCreateDTO dto)
        {
            if(dto == null)
            { throw new ArgumentNullException(String.Empty, "Could not bind DTO variables."); }

            if (string.IsNullOrWhiteSpace(dto.DeviceToken))
            { throw new ArgumentNullException(nameof(dto.DeviceToken), "A non-empty Device Token must be provided"); }

            DevicePlatform plfm = DevicePlatform.Unknown;
            if (!System.Enum.TryParse<DevicePlatform>(dto.Platform, out plfm) || plfm == DevicePlatform.Unknown)
            { throw new ArgumentNullException(nameof(dto.Platform), "Unknown Platform"); }

            string endpoint = string.Empty;
            try
            {
                var response = MobileMessenger.RegisterDevice(dto.DeviceToken, plfm);
                if (response.HttpStatusCode != HttpStatusCode.OK)
                {
                    MvcApplication.Logger.Warn($"{nameof(MobileMessenger.RegisterDevice)}: Unable to Register Mobile Device [{dto.DeviceToken}] to platform [{plfm}]. Response has StatusCode [{response.HttpStatusCode}] with Message [{AppUtility.Serialize(response.ResponseMetadata)}]");
                    return ResponseMessage(new HttpResponseMessage(response.HttpStatusCode));
                }
                else if (string.IsNullOrWhiteSpace(response.EndpointArn))
                {
                    throw new ApplicationException("Device Registered but returned no endpoint");
                }
                endpoint = response.EndpointArn;
            }
            catch (Amazon.SimpleNotificationService.Model.InvalidParameterException ipe)
            {
                throw new ArgumentOutOfRangeException(nameof(dto.DeviceToken), ipe.Message);
            }

            int userId = UserContextService.CurrentUserId;

            var device = DbCtx.Set<UserDevice>()
                              .Where(t => t.DeviceEndpoint == endpoint)
                              .SingleOrDefault();
            if (device != null)
            {
                if(device.UserId != userId)
                {
                    device.UserId = userId;
                    device.IsActive = true;
                    device.UpdateById = userId;
                    device.UpdateDate = AppUtility.UtcNow();
                    await DbCtx.SaveChangesAsync();
                }

                if(!device.IsActive)
                {
                    device.IsActive = true;
                    device.UpdateById = userId;
                    device.UpdateDate = AppUtility.UtcNow();
                    await DbCtx.SaveChangesAsync();
                }

                return Ok(new UserDeviceDetailDTO()
                {
                    UserDeviceId = device.UserDeviceId,
                    DeviceEndpoint = device.DeviceEndpoint,
                    Platform = System.Enum.GetName(typeof(DevicePlatform), device.Platform)
                });
            }

            device = DbCtx.Set<UserDevice>().Create();
            device.UserId = userId;

            device.DeviceEndpoint = endpoint;
            device.Platform = plfm;

            device.IsActive = true;
            device.CreateById = userId;
            device.CreateDate = AppUtility.UtcNow();
            DbCtx.Set<UserDevice>().Add(device);
            await DbCtx.SaveChangesAsync();
                
            return Ok(new UserDeviceDetailDTO()
            {
                UserDeviceId = device.UserDeviceId,
                DeviceEndpoint = device.DeviceEndpoint,
                Platform = System.Enum.GetName(typeof(DevicePlatform), device.Platform)
            });
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(UserDeviceDetailDTO))]
        public async Task<IHttpActionResult> Put(int id)
        {
            var device = DbCtx.Set<UserDevice>().Find(id);
            if (device == null)
            { throw new KeyNotFoundException("Entity not found"); }

            int userId = UserContextService.CurrentUserId;
            if (device.UserId != userId)
            { throw new UnauthorizedAccessException("Users may only update thier own devices"); }

            device.IsActive = true;
            device.UpdateById = userId;
            device.UpdateDate = AppUtility.UtcNow();
            await DbCtx.SaveChangesAsync();

            return Ok(new UserDeviceDetailDTO()
            {
                UserDeviceId = device.UserDeviceId,
                DeviceEndpoint = device.DeviceEndpoint,
                Platform = System.Enum.GetName(typeof(DevicePlatform), device.Platform)
            });
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var device = DbCtx.Set<UserDevice>().Find(id);
            if (device == null)
            { throw new KeyNotFoundException("Entity not found"); }

            int userId = UserContextService.CurrentUserId;
            if (device.UserId != userId)
            { throw new UnauthorizedAccessException("Users may only delete thier own devices"); }

            device.IsActive = false;
            device.UpdateById = userId;
            device.UpdateDate = AppUtility.UtcNow();
            var success = await DbCtx.SaveChangesAsync() > 0;
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}
