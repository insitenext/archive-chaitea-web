﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.Services.Utils.Translation;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.Utils;

using ChaiTea.Web.Identity;


namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/User")]
    public class UserController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(UserContextVm))]
        [Route("ActiveContext")]
        public IHttpActionResult ActiveContext()
        {
            return Ok(new UserContextService(UserCtx).ToVm());
        }

        [HttpGet]
        [ResponseType(typeof(UserContextService.BootStrapDTO))]
        [Route("Bootstrap")]
        public IHttpActionResult GetBootStrapInfo()
        {
            return Ok(new UserContextService(UserCtx).GetBootStrapInfo());
        }

        private LookupListVm<int?, string> GetLanguageOptions()
        {
            return (new LanguageService(UserCtx)).Get().Select(l => new
                                                                    {
                                                                        l.LanguageId,
                                                                        l.Name
                                                                    }).CreateList("Languages", u => (int?) u.LanguageId, u => u.Name);
        }

        [HttpGet]
        [Route("Clients")]
        [ResponseType(typeof(UserContextLookupListVm))]
        public IHttpActionResult Clients()
        {
            var clientList = (new ClientService(UserCtx)).GetUserClients();

            var clientOptions = clientList.Select(uc => new
                                                        {
                                                            uc.Id,
                                                            uc.Name
                                                        }).CreateList("UserClient", u => u.Id, u => u.Name);

            var selectedClientId = UserCtx.ClientId.Value;

            var siteOptions = (new SiteService(UserCtx)).GetClientUserSites(selectedClientId).Select(us => new
                                                                                                           {
                                                                                                               us.Id,
                                                                                                               us.Name
                                                                                                           }).OrderBy(s => s.Name).CreateList("Sites", s => (int?) s.Id, s => s.Name);

            if (clientList.Any(cl => cl.IsUserMapped && cl.Id == selectedClientId))
            {
                siteOptions.Options.Insert(0,
                                           new LookupListOption<int?, string>()
                                           {
                                               Key = null,
                                               Value = "All Sites"
                                           });
            }

            LookupListVm<int?, string> programOptions;

            if (UserCtx.SiteId.HasValue)
            {
                programOptions = (new FloorProgramService(UserCtx)).GetBySite(UserCtx.SiteId.Value).Select(ps => new
                                                                                                                 {
                                                                                                                     ps.Id,
                                                                                                                     ps.Name
                                                                                                                 }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);


            }
            else
            {
                programOptions = (new FloorProgramService(UserCtx)).GetByClient(UserCtx.ClientId.Value).Select(ps => new
                                                                                                                     {
                                                                                                                         ps.Id,
                                                                                                                         ps.Name
                                                                                                                     }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);
            }

            if (programOptions.Options.Count > 1)
            {
                programOptions.Options.Insert(0,
                                              new LookupListOption<int?, string>()
                                              {
                                                  Key = null,
                                                  Value = "All Programs"
                                              });
            }

            return Ok(new UserContextLookupListVm()
                      {
                          Clients = clientOptions,
                          Sites = siteOptions,
                          Programs = programOptions,
                          Languages = GetLanguageOptions()
                      });
        }

        [HttpGet]
        [Route("Sites/{id:int}/{save:bool}")]
        [ResponseType(typeof(UserContextLookupListVm))]
        public IHttpActionResult Sites(int id, bool save)
        {
            bool isClientMappedToUser = (new ClientService(UserCtx)).GetUserClients().Where(c => c.Id == id).Any(c => c.IsUserMapped);

            var siteOptions = (new SiteService(UserCtx)).GetClientUserSites(id).Select(us => new
                                                                                             {
                                                                                                 us.Id,
                                                                                                 us.Name
                                                                                             }).OrderBy(s => s.Name).CreateList("Sites", s => (int?) s.Id, s => s.Name);

            if (isClientMappedToUser)
            {
                siteOptions.Options.Insert(0,
                                           new LookupListOption<int?, string>()
                                           {
                                               Key = null,
                                               Value = "All Sites"
                                           });
            }

            LookupListVm<int?, string> programOptions;

            if (UserCtx.SiteId.HasValue)
            {
                programOptions = (new FloorProgramService(UserCtx)).GetBySite(UserCtx.SiteId.Value).Select(ps => new
                                                                                                                 {
                                                                                                                     ps.Id,
                                                                                                                     ps.Name
                                                                                                                 }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);


            }
            else
            {
                programOptions = (new FloorProgramService(UserCtx)).GetByClient(UserCtx.ClientId.Value).Select(ps => new
                                                                                                                     {
                                                                                                                         ps.Id,
                                                                                                                         ps.Name
                                                                                                                     }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);
            }

            if (programOptions.Options.Count > 1)
            {
                programOptions.Options.Insert(0,
                                              new LookupListOption<int?, string>()
                                              {
                                                  Key = null,
                                                  Value = "All Programs"
                                              });
            }

            return Ok(new UserContextLookupListVm()
                      {
                          Sites = siteOptions,
                          Programs = programOptions,
                          Languages = GetLanguageOptions()
                      });
        }


        [HttpGet]
        [Route("Programs/{id:int}/{save:bool}")]
        [ResponseType(typeof(UserContextLookupListVm))]
        public IHttpActionResult Programs(int id, bool save)
        {
            LookupListVm<int?, string> programOptions;

            if (!save)
            {
                programOptions = (new FloorProgramService(UserCtx)).GetBySite(id).Select(ps => new
                                                                                               {
                                                                                                   ps.Id,
                                                                                                   ps.Name
                                                                                               }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);


            }
            else
            {
                programOptions = (new FloorProgramService(UserCtx)).GetByClient(id).Select(ps => new
                                                                                                 {
                                                                                                     ps.Id,
                                                                                                     ps.Name
                                                                                                 }).OrderBy(p => p.Name).CreateList("ProgramsForClient", p => (int?) p.Id, p => p.Name);
            }

            if (programOptions.Options.Count > 1)
            {
                programOptions.Options.Insert(0,
                                              new LookupListOption<int?, string>()
                                              {
                                                  Key = null,
                                                  Value = "All Programs"
                                              });
            }

            return Ok(new UserContextLookupListVm()
                      {
                          Programs = programOptions,
                          Languages = GetLanguageOptions()
                      });
        }

        [HttpGet]
        [Route("Languages")]
        [ResponseType(typeof(UserContextLookupListVm))]
        public IHttpActionResult Languages()
        {
            return Ok(new UserContextLookupListVm()
                      {
                          Languages = GetLanguageOptions()
                      });
        }

        [HttpPost]
        [Route("ActiveContext")]
        [ResponseType(typeof(void))]
        public IHttpActionResult SetContext(UserContextVm settings)
        {
            if (settings.Client != null &&
                settings.Client.ID <= 0)
            {
                settings.Client = null;
            }

            if (settings.Site != null &&
                settings.Site.ID <= 0)
            {
                settings.Site = null;
            }

            if (settings.Program != null &&
                settings.Program.ID <= 0)
            {
                settings.Program = null;
            }

            if (settings.Language != null &&
                settings.Language.ID <= 0)
            {
                settings.Language = null;
            }

            var userId = UserContextService.CurrentUserId;
            var newContext = new UserContext(userId).OverrideSettings(settings.ClientId, settings.SiteId, settings.ProgramId, settings.LanguageId);

            new UserContextService(UserCtx).SaveContextInDB(newContext);

            return Ok();
        }

        [HttpPost]
        [AuthorizedRolesApi]
        [ResponseType(typeof(HttpResponseMessage))]
        [Route("Cookie")]
        public HttpResponseMessage SetContextandReturnCookie()
        {
            var ci = User.Identity as ClaimsIdentity;

            if (ci == null)
            {
                throw new ArgumentOutOfRangeException(nameof(User.Identity), "Identity must be claims based");
            }

            new UserContextService(UserCtx).SaveContextInDB(UserCtx);

            var resp = new HttpResponseMessage();

            var issueTime = DateTime.UtcNow;
            var expiryTime = issueTime.AddMinutes(JwtSettings.TokenExpirationInMinutes);

            string jsonTicket = new ApplicationTicketFormat().Protect(issueTime, expiryTime, ci.Claims);

            var cookie = new CookieHeaderValue(CookieSettings.CookieName, jsonTicket)
                         {
                             Expires = expiryTime,
                             Path = "/"
                         };
            
#if !DEBUG
            cookie.Domain = ".sbminsite.com";
#endif

            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
            resp.Headers.Add("Access-Control-Allow-Credentials", "true");

            return resp;
        }
    }
}
