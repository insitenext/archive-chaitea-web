using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Services.AppMetaData.v1_1;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.AppMetaData.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Dashboard")]
    public class Dashboard_v11Controller : BaseApiController
    {
        /// <summary>
        /// Gets dashboard counts
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        [HttpGet]
        [ResponseType(typeof(DashboardCounts))]
        [Route("Counts")]
        public IHttpActionResult GetCounts(DateTime startDate, DateTime endDate)
        {
            using(var dashboardSvc = new DashboardService(UserCtx))
            return Ok(dashboardSvc.GetDashboardCounts(startDate, endDate));
        }
        
        /// <summary>
        /// Gets dashboard dates
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>DashboardVm</returns>
        [HttpGet]
        [ResponseType(typeof(DashboardDates))]
        [Route("Dates")]
        public IHttpActionResult GetDates(DateTime startDate, DateTime endDate)
        {
            using(var dashboardSvc = new DashboardService(UserCtx))
                return Ok(dashboardSvc.GetDashboardDates(startDate, endDate));
        }
    }
}