﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using System.Net.Http;
using System.Net;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;

using log4net;

namespace ChaiTea.Web.Areas.Services.Controllers
{
    public abstract class BaseCrudApiController<TV> : BaseApiController where TV : class
    {
        private readonly ILog Logger = LogManager.GetLogger(nameof(TV));

        protected abstract Func<TV, int> GetIdentifier { get; }

        protected abstract Action<TV, int> SetIdentifier { get; }

        protected abstract ICrudService<TV> CreateService { get; }

        private ICrudService<TV> _crudService;

        protected ICrudService<TV> Service
        {
            get => _crudService ?? (_crudService = CreateService);
            set => _crudService = value;
        }

        protected string VmName => typeof(TV).Name;

        /// <summary>
        /// Retrieves collection of VMs. Client-side OData filters on VM properties can be applied to limit collection.
        /// </summary>
        /// <returns>Collection of VMs</returns>        
        [HttpGet]
        [Route("")]
        [EnableQuery(MaxAnyAllExpressionDepth = 2)]
        public virtual IQueryable<TV> Get()
        {
            IQueryable<TV> typeList = Service.Get();

            if (!typeList.Any())
            {
                Logger.Warn($"Request for objects of type [{VmName}] returned empty list.");
            }

            return typeList;
        }

        /// <summary>
        /// Retrieves VM identified by given id.
        /// </summary>
        /// <param name="id">Unique identifier.</param>
        /// <returns>VM wrapped in IHttpActionResult</returns>
        [HttpGet]
        [Route("{id:int}")]
        public virtual IHttpActionResult Get(int id)
        {
            TV vm = Service.Get(id);

            if (vm == null)
            {
                throw new KeyNotFoundException($"Entity of type [{VmName}] with id [{id}] not found");
            }

            return Ok(vm);
        }

        /// <summary>
        /// Creates an instance of posted VM.
        /// </summary>
        /// <param name="vm">VM to be created.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPost]
        [Route("")]
        public virtual async Task<IHttpActionResult> Post(TV vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm = await Service.CreateAsync(vm);

            return Ok(vm);
        }

        /// <summary>
        /// Updates VM identified by given id.
        /// </summary>
        /// <param name="id">Unique identifier.</param>
        /// <param name="vm">VM with updated properties.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPut]
        [Route("{id:int}")]
        public virtual async Task<IHttpActionResult> Put(int id, TV vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SetIdentifier(vm, id);

            vm = await Service.UpdateAsync(vm);
            return Ok(vm);
        }

        /// <summary>
        /// Deletes VM for given id.
        /// </summary>
        /// <param name="id">Unique identifier.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpDelete]
        [Route("{id:int}")]
        public virtual async Task<IHttpActionResult> Delete(int id)
        {
            var success = await Service.DeleteAsync(id);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }        
    }
}
