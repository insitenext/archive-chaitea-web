﻿using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AuditSetting")]
    public class AuditSettingController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AuditSettingVm>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult Get()
        {
            return Ok(new AuditSettingService(UserCtx).Get());
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(AuditSettingVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
        public IHttpActionResult GetByClientOrSite(int ClientId, int? SiteId = null, bool? CascadeClient = null)
        {
            return Ok(new AuditSettingService(UserCtx).Get(ClientId, SiteId, CascadeClient));
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(AuditSettingVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Get(int id)
        {
            return Ok(new AuditSettingService(UserCtx).Get(id));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Post(AuditSettingVm vm)
        {
            var newId = new AuditSettingService(UserCtx).Create(vm);
            return Ok(new { AuditSettingId = newId });
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Put(int id, AuditSettingVm vm)
        {
            var success = new AuditSettingService(UserCtx).Update(id, vm);
            if(success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public IHttpActionResult Delete(int id)
        {
            var success = new AuditSettingService(UserCtx).Delete(id);
            if(success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
        }
    }
}
