﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AuditProfileSection")]
    public class AuditProfileSectionController : BaseCrudApiController<AuditProfileSectionVm>
    {
        protected override Func<AuditProfileSectionVm, int> GetIdentifier
        {
            get { return vm => vm.AuditProfileSectionId; }
        }

        protected override Action<AuditProfileSectionVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.AuditProfileSectionId = id; }
        }

        protected override ICrudService<AuditProfileSectionVm> CreateService
        {
            get { return new AuditProfileSectionService(UserCtx); }
        }
    }
}
