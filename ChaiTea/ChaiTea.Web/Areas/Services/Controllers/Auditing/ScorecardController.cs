﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Scorecard")]
    public class ScorecardController : BaseCrudApiController<ScorecardVm>
    {
        protected override Func<ScorecardVm, int> GetIdentifier
        {
            get { return vm => vm.ScorecardId; }
        }

        protected override Action<ScorecardVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.ScorecardId = id; }
        }

        protected override ICrudService<ScorecardVm> CreateService => new ScorecardService(UserCtx);

        [HttpPut]
        [ResponseType(typeof(ScorecardVm))]
        [Route("UpdateProgress/{scorecardId:int}")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> RevertFinalized(int scorecardId)
        {
            return Ok(await new ScorecardService(UserCtx).RevertFinalizedAsync(scorecardId));
        }

        [HttpGet]
        [ResponseType(typeof(ScorecardVm))]
        [Route("Template/{floorId:int}/{inProgress:bool}")]
        public IHttpActionResult GetScorecardTemplate(int floorId, bool inProgress)
        {
            var scorecardVm = new ScorecardVm()
            {
                FloorId = floorId,
                InProgress = inProgress
            };
            return Ok(new ScorecardService(UserCtx).ScorecardTemplate(scorecardVm));
        }

        [HttpGet]
        [ResponseType(typeof(List<FloorIdScorecardVm>))]
        [Route("Templates/{buildingId:int}")]
        public IHttpActionResult GetScorecardTemplates(int buildingId)
        {
            return Ok(new ScorecardService(UserCtx).ScorecardTemplates(buildingId));
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("FailedAuditsCount")]
        public IHttpActionResult GetFailedAuditsCount(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            var goal = 3.0;
            var goalSetByProfile = false;

            if (UserCtx.ProgramId.HasValue)
            {
                var programReportSetting = new ProgramReportSettingService(UserCtx).Get(UserCtx.ProgramId.Value);
                if (programReportSetting != null)
                {
                    goal = programReportSetting.ScoringProfileGoal;
                    goalSetByProfile = true;
                }
            }

            if (UserCtx.SiteId.HasValue && !goalSetByProfile)
            {
                var siteReportSetting = new SiteReportSettingService(UserCtx).Get(UserCtx.SiteId.Value);
                if (siteReportSetting != null)
                {
                    goal = siteReportSetting.ScoringProfileGoal;
                    goalSetByProfile = true;
                }
            }
                
            if (!goalSetByProfile)
            {
                var clientReportSetting = new ClientReportSettingService(UserCtx).Get(UserCtx.ClientId.Value);
                if (clientReportSetting != null)
                {
                    goal = clientReportSetting.ScoringProfileGoal;
                }
            }

            return Ok(new { FailedAuditsCount = new ScorecardService(UserCtx).GetFailedAuditsCount(startDate, endDate, goal) });
        }
    }
}
