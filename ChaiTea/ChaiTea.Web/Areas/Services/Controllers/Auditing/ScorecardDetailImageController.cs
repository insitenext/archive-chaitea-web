﻿using Amazon.S3;
using Amazon.S3.Model;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Identity;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Settings;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ScorecardDetailImage")]
    public class ScorecardDetailImageController : BaseCrudApiController<ScorecardDetailImageVm>
    {

        protected override Func<ScorecardDetailImageVm, int> GetIdentifier
        {
            get { return vm => vm.ImageId; }
        }

        protected override Action<ScorecardDetailImageVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.ImageId = id; }
        }

        protected override ICrudService<ScorecardDetailImageVm> CreateService
        {
            get { return new ScorecardDetailImageService(UserCtx); }
        }

        [HttpGet]
        [ResponseType(typeof(ScorecardDetailImageVm))]
        [Route("{id:int}/rotate/{degree:int}")]
        public async Task<IHttpActionResult> Rotate(int id, int degree)
        {
            //lookup id
            var scorecardDetailImageService = new ScorecardDetailImageService(UserCtx);
            ScorecardDetailImageVm scorecardDetailImage = scorecardDetailImageService.Get(id);

            //validate
            int[] validDegrees = new int[3] { 90, 180, 270 };
            var degreeExists = Array.Find(validDegrees, d => d == degree);
            if (degreeExists == 0 || degree == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(degree), "Invalid degree");
            }

            string bucket = AwsSettings.Bucket;
            string folder = "image";
            string keyName = folder + "/" + scorecardDetailImage.PublicId;
            RotateFlipType rotationType = RotateFlipType.Rotate90FlipNone;

            //get rotation
            if (degree == 180)
            {
                rotationType = RotateFlipType.Rotate180FlipNone;
            }
            if (degree == 270)
            {
                rotationType = RotateFlipType.Rotate270FlipNone;
            }

            using (IAmazonS3 client = new AmazonS3Client(Amazon.RegionEndpoint.USWest1))
            {

                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucket,
                    Key = keyName
                };

                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = bucket,
                    CannedACL = Amazon.S3.S3CannedACL.PublicRead
                };

                using (GetObjectResponse response = client.GetObject(request))
                {

                    //route image
                    Image img = System.Drawing.Image.FromStream(response.ResponseStream);
                    img.RotateFlip(rotationType);

                    //update filename
                    string[] parts = keyName.Split('.');
                    string newKey = Guid.NewGuid() + ".jpg";
                    putRequest.Key = folder + '/' + newKey;

                    //get stream and update PUT request
                    using (MemoryStream stream = new MemoryStream())
                    {
                        img.Save(stream, ImageFormat.Jpeg);
                        stream.Position = 0;
                        putRequest.InputStream = stream;
                        
                        //put new file back on s3
                        PutObjectResponse putResponse = client.PutObject(putRequest);
                    }

                    //update db
                    scorecardDetailImage.PublicId = newKey;
                    scorecardDetailImage = await scorecardDetailImageService.UpdateAsync(scorecardDetailImage);
                }
            }
            return Ok(scorecardDetailImage);
        }
    }
}
