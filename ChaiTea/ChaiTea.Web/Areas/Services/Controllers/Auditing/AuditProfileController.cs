﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AuditProfile")]
    public class AuditProfileController : BaseCrudApiController<AuditProfileVm>
    {
        protected override Func<AuditProfileVm, int> GetIdentifier
        {
            get { return vm => vm.AuditProfileId; }
        }

        protected override Action<AuditProfileVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.AuditProfileId = id; }
        }

        protected override ICrudService<AuditProfileVm> CreateService
        {
            get { return new AuditProfileService(UserCtx); }
        }
    }
}
