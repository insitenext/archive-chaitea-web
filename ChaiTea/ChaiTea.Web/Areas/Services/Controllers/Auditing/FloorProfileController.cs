﻿using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/FloorProfile")]
    public class FloorProfileController : BaseApiController
    {
        /// <summary>
        /// Retrieves floor profiles.
        /// </summary>
        /// <param name="ignoreSite">Optional flag that allows client to ignore site set in user context.</param>
        /// <returns>Collection of FloorProfileVms</returns>
        [HttpGet]
        [Route("{ignoreSite:bool=false}")]
        [ResponseType(typeof(IQueryable<FloorProfileVm>))]
        [EnableQuery]
        public IHttpActionResult GetFloorProfile(bool ignoreSite)
        {
            if (!UserCtx.ProgramId.HasValue)
            { throw new ArgumentNullException(nameof(UserCtx.ProgramId), "No program selected."); }

            return Ok(new AuditProfileService(UserCtx).GetFloorProfiles(ignoreSite));
        }

        /// <summary>
        /// Clones an audit profile to a floor.
        /// </summary>
        /// <param name="auditProfileId">Unique identifier of audit profile to clone.</param>
        /// <param name="targetFloorId">Unique identifier of floor to clone audit profile to.</param>
        /// <param name="responsibleUserId">Unique identifier (employeeId) of responsible user.</param>
        /// <returns>FloorProfileVm</returns>
        [HttpGet]
        [Route("Clone/{auditProfileId:int}/{targetFloorId:int}/{responsibleUserId:int}")]
        [ResponseType(typeof(FloorProfileVm))]
        public async Task<IHttpActionResult> Clone(int auditProfileId, int targetFloorId, int responsibleUserId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(await new AuditProfileService(UserCtx).CloneAuditProfileAsync(auditProfileId, targetFloorId, responsibleUserId));
        }
    }
}
