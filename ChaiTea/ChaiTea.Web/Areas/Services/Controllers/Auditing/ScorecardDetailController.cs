﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ScorecardDetail")]
    public class ScorecardDetailController : BaseCrudApiController<ScorecardDetailVm>
    {
        protected override Func<ScorecardDetailVm, int> GetIdentifier
        {
            get { return vm => vm.ScorecardDetailId; }
        }

        protected override Action<ScorecardDetailVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.ScorecardDetailId = id; }
        }

        protected override ICrudService<ScorecardDetailVm> CreateService
        {
            get { return new ScorecardDetailService(UserCtx); }
        }

        /// <summary>
        /// Updates a collection of scorecard details.
        /// </summary>
        /// <param name="scorecardDetails">Collection of ScorecardDetailVm</param>
        /// <returns>BatchResponseVm</returns>
        [HttpPut]
        [ResponseType(typeof(BatchResponseVm<ScorecardDetailVm>))]
        [Route("")]
        public async Task<IHttpActionResult> PutBatchScorecardDetail(List<ScorecardDetailVm> scorecardDetails)
        {
            BatchResponseVm<ScorecardDetailVm> response = new BatchResponseVm<ScorecardDetailVm>();
            foreach (var detail in scorecardDetails)
            {
                try
                {
                    var scorecardDetail = await Service.UpdateAsync(detail);
                    response.AddSuccess(scorecardDetail);
                }
                catch (KeyNotFoundException e)
                {
                    MvcApplication.Logger.Warn("PutBatchScorecardDetail encountered an issue:", e);
                    response.AddFailure(e.Message);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    MvcApplication.Logger.Warn("PutBatchScorecardDetail encountered an issue:", e);
                    response.AddFailure(e.Message);
                }
            }
            return Ok(response);
        }

    }
}
