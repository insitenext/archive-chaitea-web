﻿using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SecurityAudit")]
    public class SecurityAuditController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IEnumerable<AreaClassificationInfoVm>))]
        [Route("SecurityAuditsAreaClassificationInfo")]
        public IHttpActionResult GetAreas()
        {
            return Ok(new SecurityAuditService(UserCtx).GetSecurityAuditsAreaClassificationInfo());
        }
    }
}
