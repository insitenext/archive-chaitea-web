﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Auditing.v1_1;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Auditing.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Scorecard")]
    public class Scorecard_v11Controller : BaseApiController
    {
        /// <summary>
        /// Searches for scorecards based on the supplied criteria.
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="skip">Skip n Records</param>
        /// <param name="take">Take n Records</param>
        /// <param name="inProgress">Specify progress state</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<ScorecardVm>))]
        public async Task<IHttpActionResult> Get(DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default, bool? inProgress = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.GetAsync(pagingFilter, dateFilter, null, inProgress));
            }
        }
        
        /// <summary>
        /// Searches for scorecards based on the supplied criteria limiting the search by tags.
        /// </summary>
        /// /// <param name="tagList">Array of building tags to limit the search to</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="skip">Skip n Records</param>
        /// <param name="take">Take n Records</param>
        /// <param name="inProgress">Specify progress state</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<ScorecardVm>))]
        public async Task<IHttpActionResult> Get([FromBody] List<string> tagList, DateTime? startDate = default, DateTime? endDate = default, int? skip = default, int? take = default, bool? inProgress = default)
        {
            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.GetAsync(pagingFilter, dateFilter, tagList, inProgress));
            }
        }

        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching complaints</param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<AuditSiteSummary>))]
        public async Task<IHttpActionResult> GetSummariesByTags([FromBody] List<string> tags, int? take, int? skip, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(tags, pagingFilter, dateFilter.StartDate, dateFilter.EndDate));
            }
        }

        /// <summary>
        /// Gets a summary of a site
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(PaginatedData<AuditSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, dateFilter.StartDate, dateFilter.EndDate));
            }
        }
        
        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching complaints</param>
        /// <param name="startDate">Beginning of the range</param>
        /// <param name="endDate">End of the range</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllSitesSummaryByTags")]
        [ResponseType(typeof(PaginatedData<AuditSiteSummary>))]
        public async Task<IHttpActionResult> GetSummariesByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.GetAllSitesSummaryByTagsAsync(tags, dateFilter.StartDate, dateFilter.EndDate));
            }
        }

        [HttpPut]
        [ResponseType(typeof(ScorecardVm))]
        [Route("UpdateProgress/{scorecardId:int}")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> RevertFinalized(int scorecardId)
        {
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.RevertFinalizedAsync(scorecardId));
            }
        }

        [HttpGet]
        [ResponseType(typeof(ScorecardVm))]
        [Route("Template/{floorId:int}/{inProgress:bool}")]
        public IHttpActionResult GetScorecardTemplate(int floorId, bool inProgress)
        {
            var scorecardVm = new ScorecardVm()
                              {
                                  FloorId = floorId,
                                  InProgress = inProgress
                              };

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(svc.CreateScorecardTemplate(scorecardVm));
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<FloorIdScorecardVm>))]
        [Route("Templates/{buildingId:int}")]
        public IHttpActionResult GetScorecardTemplates(int buildingId)
        {
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(svc.ScorecardTemplates(buildingId));
            }
        }

        [HttpGet]
        [ResponseType(typeof(object))]
        [Route("FailedAuditsCount")]
        public IHttpActionResult GetFailedAuditsCount(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            var goal = 3.0;

            if (UserCtx.ProgramId.HasValue)
            {
                var programReportSetting = new ProgramReportSettingService(UserCtx).Get(UserCtx.ProgramId.Value);

                if (programReportSetting != null)
                {
                    goal = programReportSetting.ScoringProfileGoal;
                }
            }
            else if (UserCtx.SiteId.HasValue)
            {
                var siteReportSetting = new SiteReportSettingService(UserCtx).Get(UserCtx.SiteId.Value);

                if (siteReportSetting != null)
                {
                    goal = siteReportSetting.ScoringProfileGoal;
                }
            }
            else if (!UserCtx.ClientId.HasValue)
            {
                var clientReportSetting = new ClientReportSettingService(UserCtx).Get(UserCtx.ClientId.Value);

                if (clientReportSetting != null)
                {
                    goal = clientReportSetting.ScoringProfileGoal;
                }
            }

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(new
                          {
                              FailedAuditsCount = svc.GetFailedAuditsCount(startDate, endDate, goal)
                          });
            }
        }

        [HttpGet]
        [ResponseType(typeof(ScorecardVm))]
        [Route("{id:int}")]
        public virtual IHttpActionResult Get(int id)
        {
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(svc.Get(id));
            }
        }

        [HttpPost]
        [ResponseType(typeof(ScorecardVm))]
        [Route("")]
        public virtual async Task<IHttpActionResult> Post(ScorecardVm vm)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.CreateAsync(vm));
            }
        }

        [HttpPut]
        [ResponseType(typeof(ScorecardVm))]
        [Route("{id:int}")]
        public virtual async Task<IHttpActionResult> Put(int id, ScorecardVm vm)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            vm.ScorecardId = id;

            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.UpdateAsync(vm));
            }
        }

        [HttpDelete]
        [ResponseType(typeof(bool))]
        [Route("{id:int}")]
        public virtual async Task<IHttpActionResult> Delete(int id)
        {
            using (var svc = new ScorecardService(UserCtx))
            {
                return Ok(await svc.DeleteAsync(id));
            }
        }
    }
}