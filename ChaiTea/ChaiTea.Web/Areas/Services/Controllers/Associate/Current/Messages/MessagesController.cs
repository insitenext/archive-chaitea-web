﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Associates.Current.Messages
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Associates/Current/Messages")]
    public class AssociateMessagesController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.DetailDTO>))]
        public IHttpActionResult GetMessageListItems(DateTime? startDate = null, DateTime? endDate = null, int? top = null, int? skip = 0, bool? hasBeenSeen = null, int? departmentId = null, AssociateMessagesService.MessageOrderBy? orderBy = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);

            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                return Ok(assocMsgSvc.GetMessageDetails(_filterToDate,
                                                        _filterToPage,
                                                        hasBeenSeen,
                                                        departmentId,
                                                        new [] { orgUserId },
                                                        orderBy));
            }
        }

        [HttpGet]
        [Route("{messageId:int}")]
        [ResponseType(typeof(AssociateMessagesService.DetailDTO))]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMessageDetails(int messageId)
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                return Ok(assocMsgSvc.GetMessageDetails(messageId, orgUserId));
            }
        }

        public class UpdateSeenStatusDTO
        {
            public bool HasBeenSeen { get; set; }
        }

        [HttpPost]
        [Route("{messageId:int}/Seen")]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.UpdateStatusDTO>))]
        public IHttpActionResult UpdateSeenStatus(int messageId, UpdateSeenStatusDTO dto)
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                var updatedRecords = assocMsgSvc.UpdateStatus(orgUserId,
                                                              new[]
                                                              {
                                                                  new AssociateMessagesService.UpdateStatusDTO()
                                                                  {
                                                                      MessageId = messageId,
                                                                      HasBeenSeen = dto.HasBeenSeen
                                                                  }
                                                              });

                if (updatedRecords != null &&
                    updatedRecords.Any())
                {
                    return Ok(updatedRecords);
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
            }
        }

        [HttpGet]
        [Route("HasNotBeenSeenCount")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetHasNotBeenSeenCount()
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                return Ok(assocMsgSvc.GetHasNotBeenSeenCount(orgUserId));
            }
        }

        [HttpGet]
        [Route("OrderByOptions")]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService_MessageOrderByExtension.OrderByDTO>))]
        public IHttpActionResult GetValidOrderByValues()
        {
            return Ok(AssociateMessagesService_MessageOrderByExtension.OrderByValues);
        }
    }
}
