﻿using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.TimeCenter;
using ChaiTea.Web.Identity;

using log4net;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/TeamRoster")]
    public class TeamRosterController : BaseApiController
    {
        public static readonly ILog Logger = LogManager.GetLogger(nameof(TeamRosterController));

        /// <summary>
        /// Return the team roster for the current site.
        /// </summary>
        /// <param name="sortByColumn">Return results sorted on this column. Allowable values: "lastname", "firstname", "fullname", "hiredate".
        /// Default value is "fullname".</param>
        /// <param name="orderBy">Sort either ascending ("asc") or descending ("desc"). Default is "asc".</param>
        /// <param name="skip">Pagination support : skip this manu entries.</param>
        /// <param name="take">Pagination support : after skipping, return this many entries.</param>
        /// <returns>Enumerable list of TeamRosterResponseModel objects.</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(TeamRosterResponseModel))]
        public TeamRosterResponseModel GetTeamRoster(string sortByColumn = null, string orderBy = "asc", int? skip = null, int? take = null)
        {
            PagingFilter filterToPage;

            if (skip.HasValue ||
                take.HasValue)
            {
                filterToPage = PagingFilter.TryValidate(skip, take);
            }
            else
            {
                filterToPage = PagingFilter.TakeAll;
            }

            bool orderAscending = true;

            if (!string.IsNullOrEmpty(orderBy))
            {
                orderBy = orderBy.ToLower();
                orderAscending = orderBy.StartsWith("a");
            }

            if (string.IsNullOrEmpty(sortByColumn))
            {
                sortByColumn = EmployeeService.Default_RosterSortColumn;

                Logger.Warn($"No sorting specified. Using default: \"{sortByColumn}\", {(orderAscending ? "asc" : "desc")}.");
            }
            else
            {
                Logger.Info($"Using supplied sorting. Sorting on: \"{sortByColumn}\", {(orderAscending ? "asc" : "desc")}.");
            }

            using (var employeeService = new EmployeeService(UserCtx))
            {
                return employeeService.GetRoster(filterToPage, sortByColumn, orderAscending);
            }
        }
    }
}