﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.BusinessLogic.Services.Utils.Attachments;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Utils.Attachments;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TrainingEmployee")]
    public class TrainingEmployeeController : BaseCrudApiController<TrainingEmployeeVm>
    {
        protected override Func<TrainingEmployeeVm, int> GetIdentifier
        {
            get { return vm => vm.EmployeeId; }
        }

        protected override Action<TrainingEmployeeVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.EmployeeId = id; }
        }

        protected override ICrudService<TrainingEmployeeVm> CreateService => new TrainingEmployeeService(UserCtx);

        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ProfileViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<TrainingEmployeeVm>))]
        public override IQueryable<TrainingEmployeeVm> Get()
        {
            return base.Get().ToList().AsQueryable();
        }
        
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ProfileViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(TrainingEmployeeVm))]
        public override IHttpActionResult Get(int id)
        {
            return base.Get(id);
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(TrainingEmployeeVm))]
        public override async Task<IHttpActionResult> Put(int id, TrainingEmployeeVm vm)
        {
            return await base.Put(id, vm);
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ProfileViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("WithJob/{id:int}")]
        [ResponseType(typeof(TrainingEmployeeAndJobVm))]
        public IHttpActionResult GetWithJob(int id)
        {
            return Ok((Service as TrainingEmployeeService).GetWithJob(id));
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("WithCourse")]
        [ResponseType(typeof(TrainingEmployeeAndCourseVm))]
        public IHttpActionResult GetWithCourse()
        {
            return Ok((Service as TrainingEmployeeService).GetWithCourse());
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.ProfileViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpGet]
        [Route("EmployeeAreas/{id:int}")]
        [EnableQuery]
        public IQueryable<TrainingAreaVm> GetEmployeeAreas(int id)
        {
            return (Service as TrainingEmployeeService).GetEmployeeAreas(id);
        }

        // [AuthorizedRolesApi(Roles = Roles.Managers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("UpdateEmployeeCourses")]
        public async Task<IHttpActionResult> UpdateEmployeeCourses(CourseEmployeeAssignVm vm)
        {
            var coursesUpdated = await new TrainingEmployeeService(UserCtx).UpdateEmployeeCourses(vm);
            if(coursesUpdated >= 0)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                                   {
                                       ReasonPhrase = "No courses updated"
                                   });
        }

        [HttpGet]
        [ResponseType(typeof(AttachmentVm))]
        [Route("EmployeeAttachment/{employeeId:int}/{attributeId:int}")]
        public IHttpActionResult GetEmployeeAttachment(int employeeId, int attributeId)
        {
            var attachmentService = new AttachmentService(UserCtx);
            var attachment = new AttachmentVm();
            var employeeAttributes = new EmployeeService(UserCtx).Get()
                                                                 .Where(e => e.EmployeeId == employeeId)
                                                                 .Select(ea => ea.EmployeeAttributes)
                                                                 .FirstOrDefault();

            if (employeeAttributes == null)
            {
                return Ok(attachment);
            }

            var employeeAttribute = employeeAttributes.FirstOrDefault(ea => ea.AttributeId == attributeId);

            if (employeeAttribute == null)
            {
                return Ok(attachment);
            }

            var attributeAttachment = new AttributeAttachmentService(UserCtx).Get()
                                                                             .FirstOrDefault(aa => aa.AttributeId == employeeAttribute.AttributeId &&
                                                                                                   aa.AttributeValue == employeeAttribute.AttributeValue);

            if (attributeAttachment == null)
            {
                return Ok(attachment);
            }

            attachment = attachmentService.Get(attributeAttachment.AttachmentId);


            return Ok(attachment);
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeJobVm>))]
        [Route("EmployeeJob/{jobId:int?}")]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.EHSManagers + "," + Roles.ClaimsManagers)]
        public IHttpActionResult GetEmployeeJob(int? jobId = 0)
        {
            return Ok(new TrainingEmployeeService(UserCtx).GetEmployeeJob(jobId));
        }
    }
}