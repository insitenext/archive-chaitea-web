﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Entities.Utils.Translation;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/JobPurpose")]
    public class JobPurposeController : BaseCrudApiController<JobPurposeVm>
    {
        protected override Func<JobPurposeVm, int> GetIdentifier
        {
            get { return e => e.JobPurposeId; }
        }

        protected override Action<JobPurposeVm, int> SetIdentifier
        {
            get { return (e, i) => e.JobPurposeId = i; }
        }

        protected override ICrudService<JobPurposeVm> CreateService
        {
            get { return new JobPurposeService(UserCtx); }
        }

        /// <summary>
        /// Retrieves job purpose based on client and job
        /// </summary>
        /// <param name="clientId">year</param>
        /// <param name="jobId">month</param>
        /// <returns>JobPurposeVm</returns>
        [HttpGet]
        [Route("{clientId:int}/{jobId:int}")]
        [ResponseType(typeof(IQueryable<JobPurposeVm>))]
        public IHttpActionResult GetByClientAndJob(int clientId, int jobId)
        {
            return Ok(new JobPurposeService(UserCtx).GetByClientAndJob(clientId, jobId));
        }

        // [AuthorizedRolesApi(Roles = Roles.HrAdmins)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        [HttpPost]
        [ResponseType(typeof(JobPurposeVm))]
        [Route("UpdateLocalizable")]
        public async Task<IHttpActionResult> UpdateLocalizables(JobPurposeVm vm)
        {
            if (UserCtx == null || UserCtx.LanguageId <= 0)
            { throw new ArgumentOutOfRangeException(nameof(UserCtx.LanguageId), "Could not detect Current User Language"); }

            var langId = UserCtx.LanguageId;
            string langCode = null;
            using (var dbContext = new BusinessLogic.DAL.ApplicationDbContext())
            {
                langCode = dbContext.Set<Language>().Find(langId)?.Code;
            }
            if (string.IsNullOrEmpty(langCode))
            {
                langCode = "en";
            }
            await new JobPurposeService(UserCtx).UpdateCulture(vm, langCode);
            return Ok();
        }
    }
}
