﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using static ChaiTea.BusinessLogic.Services.Employment.TerminatedEmployeeService;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TerminatedEmployee")]
    public class TerminatedEmployeeController : BaseApiController
    {
        /// <summary>
        /// Runs the terminated employee report
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("RunTerminatedEmployeeImport")]
        [AuthorizedRolesApi]
        public IHttpActionResult RunTerminatedEmployeeImport()
        {
            return Ok(new TerminatedEmployeeService(UserCtx).RunTerminatedEmployeeImport());
        }

        /// <summary>
        /// Gets turnover reasons
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>TurnoverReasonsDto</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<TurnoverReasonsDto>))]
        [Route("TurnoverReasons")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetTurnoverByReason(DateTime startDate, DateTime endDate)
        {
            return Ok(new TerminatedEmployeeService(UserCtx).GetTurnoverByReason(startDate, endDate));
        }

        /// <summary>
        /// Gets turnover types
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>TurnoverTypesDto</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<TurnoverTypesDto>))]
        [Route("TurnoverTypes")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetTurnoverTypes(DateTime startDate, DateTime endDate)
        {
            return Ok(new TerminatedEmployeeService(UserCtx).GetTurnoverByType(startDate, endDate));
        }
    }
}