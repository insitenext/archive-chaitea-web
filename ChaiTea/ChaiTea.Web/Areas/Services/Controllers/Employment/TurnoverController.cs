﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Employment.ViewModels;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using static ChaiTea.BusinessLogic.Services.Employment.TurnoverService;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Turnover")]
    public class TurnoverController : BaseApiController
    {
        /// <summary>
        /// Runs the turnover report
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("RunTurnoverImport")]
        [AuthorizedRolesApi]
        public IHttpActionResult RunTurnoverImport()
        {
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                return Ok(turnoverSvc.RunTurnoverImport());
            }
        }

        /// <summary>
        /// Gets the turnover values by month
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>AnnualizedTurnoverMonthlyTrendDto</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportAnnualizedTurnoverMonthlyTrendDto>))]
        [Route("AnnualizedTurnoverMonthlyTrend")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public async Task<IHttpActionResult> AnnualizedTurnoverMonthlyTrend(DateTime startDate, DateTime endDate)
        {
            DateTimeRequiredRange dateTimeTimeRequiredRange = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);

            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                var trendList = await turnoverSvc.AnnualizedTurnoverMonthlyTrend(dateTimeTimeRequiredRange);
                return Ok(trendList);
            }
        }

        /// <summary>
        /// Returns business conduct standards
        /// </summary>
        /// <returns>TurnoverBusinessConductStandardsDto</returns>
        [HttpGet]
        [ResponseType(typeof(TurnoverBusinessConductStandardsDto))]
        [Route("BusinessConductStandards")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult BusinessConductStandards()
        {
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                return Ok(turnoverSvc.BusinessConductStandards());
            }
        }

        /// <summary>
        /// Retrieves last Update Date
        /// </summary>
        /// <returns>Date or null</returns>
        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdateDate")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetLastUpdateDate()
        {
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                return Ok(new
                          {
                              LastUpdateDate = turnoverSvc.GetLastUpdateDate()
                          });
            }
        }
    }
}