﻿using System;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Pay;
using ChaiTea.BusinessLogic.ViewModels.Pay;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/PayStub")]
    public class PayStubController : BaseApiController
    {
        /// <summary>
        /// Trigger a import process for PayStubs via ServiceBus.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("Import")]
        public IHttpActionResult RunPayStubImport()
        {
            using (var psSvc = new PayStubService(UserCtx))
            {
                return Ok(psSvc.RunPayStubImport());
            }
        }

        /// <summary>
        /// Get list of available PayStubs for given parameters.
        /// </summary>
        /// <param name="startDate">Start date for PayStubs</param>
        /// <param name="endDate">End date for PayStubs</param>
        /// <param name="skip">Skip this many records.</param>
        /// <param name="top">Take this many records.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PayStubSummariesResponseModel))]
        public IHttpActionResult GetPayStubSummaries(DateTime startDate, DateTime endDate, int? skip = null, int? top = null)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            var filterToPage = PagingFilter.TryValidate(skip, top);

            using (var psSvc = new PayStubService(UserCtx))
            {
                return Ok(psSvc.GetPayStubSummaries(dateFilter, filterToPage));
            }
        }

        /// <summary>
        /// Return detailed PayStub for the specified identifier. Only active paystubs are returned.
        /// </summary>
        /// <param name="Id">Unique identifier for specific PayStub instance.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(PayStubDetailResponseModel))]
        public IHttpActionResult GetPayStubDetails(int Id)
        {
            using (var psSvc = new PayStubService(UserCtx))
            {
                return Ok(psSvc.GetPayStubDetails(Id));
            }
        }
        
        /// <summary>
        /// Get the user's accrued hours.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("accrued")]
        [ResponseType(typeof(PayStubAccruedHoursResponseModel))]
        public IHttpActionResult GetAccruedHours()
        {
            using (var psSvc = new PayStubService(UserCtx))
            {
                return Ok(psSvc.GetAccruedHours());
            }
        }

         /// <summary>
        /// Create action - not implemented.
        /// </summary>
        /// <param name="dummy"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Create(int dummy)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update action - not implemented.
        /// </summary>
        /// <param name="dummy"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Update(int dummy)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// Delete action - not implemented.
        /// </summary>
        /// <param name="dummy"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(int dummy)
        {
            throw new NotImplementedException();
        }
    }
}