﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Organizations;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Employee")]
    public class EmployeeController : BaseCrudApiController<EmployeeVm>
    {
        protected override Func<EmployeeVm, int> GetIdentifier
        {
            get { return e => e.EmployeeId; }
        }

        protected override Action<EmployeeVm, int> SetIdentifier
        {
            get { return (e, i) => e.EmployeeId = i; }
        }

        protected override ICrudService<EmployeeVm> CreateService => new EmployeeService(UserCtx);

        /// <summary>
        /// Retrieves a collection of employees for the given set of employee ids.
        /// </summary>
        /// <param name="employeeIds">Unique identifiers for employees.</param>
        /// <returns>Collection of EmployeeVms</returns>
        [HttpPost]
        [Route("Employees")]
        [EnableQuery]
        public IQueryable<EmployeeVm> Employees([FromBody]List<int> employeeIds)
        {
            return Service.Get().Where(e => employeeIds.Contains(e.EmployeeId));
        }

        [HttpGet]
        [Route("{clientId:int}/{siteId:int}")]
        [EnableQuery]
        public IQueryable<EmployeeVm> EmployeesByClientIdSiteId(int clientId, int siteId)
        {
            return (new EmployeeService(UserCtx.OverrideSettings(clientId, siteId, null))).Get();
        }

        [HttpGet]
        [Route("Client/{clientId:int}")]
        [EnableQuery]
        public IQueryable<EmployeeVm> EmployeesByClientId(int clientId)
        {
            return (new EmployeeService(UserCtx.OverrideSettings(clientId, null, null))).Get();
        }

        [HttpGet]
        [Route("User/{employeeId:int}")]
        public EmployeeVm EmployeeByEmployeeId(int employeeId)
        {
            return (new EmployeeService(UserCtx, true, true)).Get(employeeId);
        }

        /// <summary>
        /// Returns a single employee ID based on the information provided
        /// </summary>
        /// <param name="code">The attribute code</param>
        /// <param name="value"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidOperationException">Will throw an exception if more than 1 employee ID is found.</exception>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("IdByAttribute")]
        public async Task<IHttpActionResult> GetEmployeeIdByAttribute(string code, string value)
        {
            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentException("Code is required");
            
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException("Value is required");
            
            using (var svc = new EmployeeService(UserCtx))
            {
                var result = await svc.GetEmployeeIdsByAttribute(code, value);
                var id = result.SingleOrDefault();
                return Ok(id > 0 ? id : (int?) null);
            }
        }
        
        /// <summary>
        /// Returns all employee IDs based on the information provided
        /// </summary>
        /// <param name="code"></param>
        /// <param name="value"></param>
        /// <exception cref="ArgumentException"></exception>
        [HttpGet]
        [ResponseType(typeof(int[]))]
        [Route("IdsByAttribute")]
        public async Task<IHttpActionResult> GetEmployeeIdsByAttribute(string code, string value)
        {
            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentException("Code is required");
            
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException("Value is required");

            
            using (var svc = new EmployeeService(UserCtx))
            {
                var result = await svc.GetEmployeeIdsByAttribute(code, value);
                return Ok(result);
            }
        }
        
        /// <summary>
        /// Returns all employee IDs based on the information provided
        /// </summary>
        /// <param name="request">The specific values to search for</param>
        /// <returns>A list of employee IDs and the attribute that matched.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        [HttpPost]
        [ResponseType(typeof(EmployeeAttributeLookupVm[]))]
        [Route("IdsByAttributeValues")]
        public async Task<IHttpActionResult> GetEmployeeIdsByAttributeValues(EmployeeIdByAttributeValuesRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (string.IsNullOrWhiteSpace(request.Code))
                throw new ArgumentException("Code is required");
            
            if (request.Values?.Any() != true)
                throw new ArgumentException("Values are required");
            
            using (var svc = new EmployeeService(UserCtx))
            {
                var result = await svc.GetEmployeeIdsByAttributeValues(request.Code, request.Values);
                return Ok(result);
            }
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeAttribute")]
    public class EmployeeAttributeController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<EmployeeAttributeVm> Get()
        {
            return (new EmployeeAttributeService(UserCtx)).GetByEmployeeId(UserCtx.UserId, UserCtx.SiteId);
        }
    }
}
