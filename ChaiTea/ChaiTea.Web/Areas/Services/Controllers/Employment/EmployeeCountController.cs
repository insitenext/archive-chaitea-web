﻿using ChaiTea.BusinessLogic.Reporting.Employment.ViewModels;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeCount")]
    public class EmployeeCountController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(ReportTurnoverTrendsBySiteVm))]
        [Route("TurnoversBySite")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult TurnoversBySite(DateTime startDate, DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).TurnoverTrendsBySite(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MapCountsBySiteVm>))]
        [Route("TurnoverAveragesBySiteByMonth")]
        public IHttpActionResult GetTurnoverAverageBySiteByMonth(DateTime startDate, DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).TurnoverAveragesBySiteByMonth(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportBusinessConductStandardVm>))]
        [Route("BusinessConductStandards")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetBusinessConductStandards(DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).GetBusinessStandards(endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportTurnoverMonthlyAverageVm>))]
        [Route("TurnoverMonthlyTrend")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetTurnoverMonthlyTrend(DateTime startDate, DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).GetMonthlyTrend(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportTopTurnoverReasonsVm>))]
        [Route("TopTurnoverTypes")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetTopTurnoverTypes(DateTime startDate, DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).GetTopTurnoverTypes(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdatedDate")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetLastUpdateDate()
        {
            return Ok(new { LastUpdatedDate = new EmployeeCountService(UserCtx).GetLastUpdateDate() });
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportTurnoverByCategoryVm>))]
        [Route("TurnoverByCategory")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetTurnoverByCategory(DateTime startDate, DateTime endDate)
        {
            return Ok(new EmployeeCountService(UserCtx).GetTurnoverByCategories(startDate, endDate));
        }
    }
}
