﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/JobHourlyRate")]
    public class JobHourlyRateController : BaseApiController
    {
        /// <summary>
        /// Retrives hourly rate for all jobs in a site
        /// </summary>
        /// <returns>JobHourlyRateVm</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<JobHourlyRateVm>))]
        public IHttpActionResult GetJobHourlyRates()
        {
            return Ok(new JobHourlyRateService(UserCtx).Get().ToList());
        }

    }
}