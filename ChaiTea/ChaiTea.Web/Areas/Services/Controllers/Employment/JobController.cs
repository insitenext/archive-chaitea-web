﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Entities.Utils.Translation;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.Employment;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    // [AuthorizedRolesApi(Roles = Roles.Managers)]
    // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
    // the MVC version doesn't work on WebApiControllers. it was a bug.
    // However, many of our front-end pages depend on being able to access this endpoint regardless
    // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
    // instead of reverting, Documenting here and we'll readdress as feature
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Job")]
    public class JobController : BaseCrudApiController<TrainingEmployeeJobVm>
    {
        protected override Func<TrainingEmployeeJobVm, int> GetIdentifier
        {
            get { return vm => vm.JobId; }
        }

        protected override Action<TrainingEmployeeJobVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.JobId = id; }
        }

        protected override ICrudService<TrainingEmployeeJobVm> CreateService
        {
            get { return new JobService(UserCtx); }
        }

        [HttpPost]
        [Route("JobsForSites")]
        [ResponseType(typeof(IQueryable<TrainingEmployeeJobVm>))]
        public IHttpActionResult GetJobsForSite(JobsForSitesRequestModel siteIds)
        {
            return Ok(new JobService(UserCtx).GetJobsForSites(siteIds));
        }

        /// <summary>
        /// Creates a localizables as described by the TrainingEmployeeJobVm provided.
        /// </summary>
        /// <param name="vm">TrainingEmployeeJobVm</param>
        /// <returns>TrainingEmployeeJobVm</returns>
        [HttpPost]
        [ResponseType(typeof(TrainingEmployeeJobVm))]
        [Route("UpdateLocalizable")]
        public async Task<IHttpActionResult> UpdateLocalizables(TrainingEmployeeJobVm vm)
        {
            if (UserCtx == null ||
                UserCtx.LanguageId <= 0)
            {
                throw new ArgumentNullException(nameof(UserCtx.LanguageId), "could not detect Current User Language");
            }

            var langId = UserCtx.LanguageId;
            string langCode = null;

            using (var dbContext = new BusinessLogic.DAL.ApplicationDbContext())
            {
                langCode = dbContext.Set<Language>().Find(langId)?.Code;
            }

            if (string.IsNullOrEmpty(langCode))
            {
                langCode = "en";
            }

            return Ok(await new JobService(UserCtx).UpdateCulture(vm, langCode));
        }

        /// <summary>
        /// Creates a job course as described by the CourseJobAssignVm provided.
        /// </summary>
        /// <param name="vm">CourseJobAssignVm</param>
        /// <returns>CourseJobAssignVm</returns>
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Route("UpdateMultipleJobsCourses")]
        public async Task<IHttpActionResult> UpdateJobsCourses(CourseJobAssignVm vm)
        {
            var success = await new JobService(UserCtx).UpdateMultipleJobsCourses(vm);
            if(success)
            { return Ok(); }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError) { ReasonPhrase = "No Job Courses were updated." });
        }
    }
}