﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    // [AuthorizedRolesApi(Roles = Roles.Managers)]
    // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
    // the MVC version doesn't work on WebApiControllers. it was a bug.
    // However, many of our front-end pages depend on being able to access this endpoint regardless
    // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
    // instead of reverting, Documenting here and we'll readdress as feature
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Equipment")]
    public class EquipmentController : BaseCrudApiController<EquipmentVm>
    {
        protected override Func<EquipmentVm, int> GetIdentifier
        {
            get { return e => e.EquipmentId; }
        }

        protected override Action<EquipmentVm, int> SetIdentifier
        {
            get { return (e, i) => e.EquipmentId = i; } 
        }

        protected override ICrudService<EquipmentVm> CreateService
        {
            get { return new EquipmentService(UserCtx); }
        }
    }
}
