﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Contract")]
    public class ContractController : BaseCrudApiController<ContractVm>
    {
        protected override Func<ContractVm, int> GetIdentifier
        {
            get { return e => e.ContractId; }
        }

        protected override Action<ContractVm, int> SetIdentifier
        {
            get { return (e, i) => e.ContractId = i; }
        }

        protected override ICrudService<ContractVm> CreateService
        {
            get { return new ContractService(UserCtx); }
        }
    }
}
