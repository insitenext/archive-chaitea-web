﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Reporting.Employment.ViewModels;
using ChaiTea.BusinessLogic.Reporting.Quality.Models;
using ChaiTea.BusinessLogic.Services.Employment.v1_1;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Employment.v1_1
{
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Turnover")]
    public class Turnover_v11Controller : BaseApiController
    {
        /// <summary>
        /// Gets the turnover values by month
        /// </summary>
        /// <param name="startMonth"></param>
        /// <param name="startYear"></param>
        /// <param name="endMonth"></param>
        /// <param name="endYear"></param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ReportAnnualizedTurnoverMonthlyTrendDto>))]
        [Route("AnnualizedTurnoverMonthlyTrend")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public async Task<IHttpActionResult> AnnualizedTurnoverMonthlyTrend(int startMonth, int startYear, int endMonth, int endYear)
        {
            if (!DateTime.TryParse($"{startMonth}/1/{startYear}", out var d1))
                throw new ArgumentException("Invalid Start criteria");
            
            if (!DateTime.TryParse($"{endMonth}/1/{endYear}", out var d2))
                throw new ArgumentException("Invalid End criteria");
            
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                var trendList = await turnoverSvc.AnnualizedTurnoverMonthlyTrend(null, startMonth, startYear, endMonth, endYear);
                return Ok(trendList);
            }
        }

        /// <summary>
        /// Gets the turnover values by month
        /// </summary>
        /// <param name="tags">Site based tags - Use client names</param>
        /// <param name="startMonth"></param>
        /// <param name="startYear"></param>
        /// <param name="endMonth"></param>
        /// <param name="endYear"></param>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ReportAnnualizedTurnoverMonthlyTrendDto>))]
        [Route("AnnualizedTurnoverMonthlyTrendByTags")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public async Task<IHttpActionResult> AnnualizedTurnoverMonthlyTrendByTags([FromBody] ICollection<string> tags, int startMonth, int startYear, int endMonth, int endYear)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            if (!DateTime.TryParse($"{startMonth}/1/{startYear}", out var d1))
                throw new ArgumentException("Invalid Start criteria");
            
            if (!DateTime.TryParse($"{endMonth}/1/{endYear}", out var d2))
                throw new ArgumentException("Invalid End criteria");
            
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                var trendList = await turnoverSvc.AnnualizedTurnoverMonthlyTrend(tags, startMonth, startYear, endMonth, endYear);
                return Ok(trendList);
            }
        }

        /// <summary>
        /// Retrieves last Update Date
        /// </summary>
        /// <returns>Date or null</returns>
        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdateDate")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetLastUpdateDate()
        {
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                return Ok(turnoverSvc.GetLastUpdateDate(null));
            }
        }
        
        /// <summary>
        /// Retrieves last Update Date
        /// </summary>
        /// <param name="tags">Site based tags - Use client names</param>
        [HttpPost]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdateDateByTags")]
        [AuthorizedRolesApi(Roles = Roles.TurnoverViewers)]
        public IHttpActionResult GetLastUpdateDateByTags([FromBody] ICollection<string> tags)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            using (var turnoverSvc = new TurnoverService(UserCtx))
            {
                return Ok(turnoverSvc.GetLastUpdateDate(tags));
            }
        }

        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching</param>
        /// <param name="startMonth"></param>
        /// <param name="startYear"></param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        [HttpPost]
        [ResponseType(typeof(TurnoverSiteSummary))]
        [Route("SiteSummariesByTags")]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] string[] tags, int startMonth, int startYear, int? take = null, int? skip = null)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
         
            if (!DateTime.TryParse($"{startMonth}/1/{startYear}", out var d1))
                throw new ArgumentException("Invalid Start criteria");
            
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new TurnoverService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(pagingFilter, startMonth, startYear, tags));
            }
        }

        /// <summary>
        /// Returns a summary by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startMonth"></param>
        /// <param name="startYear"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(PaginatedData<ToDoSiteSummary>))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, int startMonth, int startYear)
        {
            if (!DateTime.TryParse($"{startMonth}/1/{startYear}", out var d1))
                throw new ArgumentException("Invalid Start criteria");
            
            using (var svc = new TurnoverService(UserCtx))
            {
                return Ok(await svc.GetSiteSummaryAsync(id, startMonth, startYear));
            }
        }

        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching</param>
        /// <param name="startMonth"></param>
        /// <param name="startYear"></param>
        [HttpPost]
        [ResponseType(typeof(ToDoSiteSummary))]
        [Route("AllSitesSummaryByTags")]
        public async Task<IHttpActionResult> GetAllSiteSummaryByTags([FromBody] List<string> tags, int startMonth, int startYear)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            if (!DateTime.TryParse($"{startMonth}/1/{startYear}", out var d1))
                throw new ArgumentException("Invalid Start criteria");
            
            using (var svc = new TurnoverService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummaryByTagsAsync(tags, startMonth, startYear));
            }
        }
    }
}