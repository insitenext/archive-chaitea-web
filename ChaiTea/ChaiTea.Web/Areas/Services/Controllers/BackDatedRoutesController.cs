﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.Web.Areas.Services.Models;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.DomainModels;

using ChaiTea.BusinessLogic.Reporting.Quality.ViewModels;
using ChaiTea.BusinessLogic.Reporting.Training.ViewModels;

using ChaiTea.BusinessLogic.Services.ActionRequests.Tasks;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.Services.Personnel;

using ChaiTea.BusinessLogic.Settings;

using ChaiTea.BusinessLogic.ViewModels.ActionRequests.Tasks;
using ChaiTea.BusinessLogic.ViewModels.AppMetaData;
using ChaiTea.BusinessLogic.ViewModels.Courses;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.Personnel;

namespace ChaiTea.Web.Areas.Services.Controllers
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix)]
    public class BackDatedRoutesController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IEnumerable<TeamStatsBoxScoreVm>))]
        [Route("BoxScore/{employeeId:int}/{startDate:datetime}/{endDate:datetime}")]
        public IHttpActionResult Get(int employeeId, DateTime startDate, DateTime endDate)
        {
            return Ok(new PersonnelBoxScoreService(UserCtx).GetEmployeeDataPoints(employeeId, startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeVm))]
        [Route("Employee/GetUser/{employeeId:int}")]
        public IHttpActionResult GetUserByEmployeeId(int employeeId)
        {
            return Ok(new EmployeeService(UserCtx, true, true).Get(employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<EmployeeCourseExtendedVm>))]
        [Route("EmployeeCourse/GetAllByEmployeeId/{employeeId:int}")]
        public IHttpActionResult GetAllByEmployeeId(int employeeId)
        {
            return Ok(new EmployeeCourseService(UserCtx).GetAllByEmployeeId(employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(ReportAuditTrendsVm))]
        [Route("Report/AuditTrends/{startDate:datetime}/{endDate:datetime}")]
        public IHttpActionResult AuditTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Quality.ReportService(UserCtx).AuditTrends(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(List<ReportAuditsByBuildingVm>))]
        [Route("Report/AuditsByBuilding/{startDate:datetime}/{endDate:datetime}")]
        public IHttpActionResult AuditsByBuilding(DateTime startDate, DateTime endDate)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Quality.ReportService(UserCtx).AuditsByBuilding(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(List<ReportAuditsByFloorVm>))]
        [Route("Report/AuditsByFloor/{startDate:datetime}/{endDate:datetime}/{buildingId:int}")]
        public IHttpActionResult AuditsByFloor(DateTime startDate, DateTime endDate, int? buildingId = null)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Quality.ReportService(UserCtx).AuditsByFloor(startDate, endDate, buildingId));
        }

        [HttpGet]
        [ResponseType(typeof(ReportComplaintTrendsVm))]
        [Route("Report/ComplaintTrends/{startDate:datetime}/{endDate:datetime}")]
        public IHttpActionResult ComplaintTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Quality.ReportService(UserCtx).ComplaintTrends(startDate, endDate));
        }

        [HttpGet]
        [Route("Report/EmployeeAuditsAvgByAreaByEmployee/{startDate:datetime}/{endDate:datetime}/{employeeId:int}")]
        [ResponseType(typeof(List<GetEmployeeAuditsAvgVm>))]
        public IHttpActionResult EmployeeAuditsAvgByAreaByEmployee(DateTime startDate, DateTime endDate, int employeeId)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Quality.ReportService(UserCtx).EmployeeAuditsAvgByAreaByEmployee(startDate, endDate, employeeId));
        }

        [HttpGet]
        [Route("Security/Aws/GetCredentials")]
        [ResponseType(typeof(SecurityVm))]
        public IHttpActionResult GetCredentials()
        {
            var tempCredentials = AwsClient.GetTemporaryCredentials(AwsSettings.AccessKeyId, AwsSettings.SecretAccessKey).GetCredentials();

            return Ok(new SecurityVm
                      {
                          AccessKey = tempCredentials.AccessKey,
                          AccessSecret = tempCredentials.SecretKey,
                          AccessToken = tempCredentials.Token,
                          Endpoint = new EndpointVm()
                                     {
                                         Bucket = AwsSettings.Bucket,
                                         Region = AwsSettings.S3Region,
                                         Cdn = AwsSettings.CloudfrontUrl
                                     }
                      });
        }

        [HttpGet]
        [AuthorizedRolesApi]
        [ResponseType(typeof(IQueryable<TodoItemVm>))]
        [Route("TodoItem/GetByEmployee")]
        [EnableQuery]
        public IQueryable<TodoItemVm> GetByEmployee()
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();

            return new TodoItemService(UserCtx).Get().Where(t => t.OrgUserId == employeeId);
        }

        [HttpGet]
        [ResponseType(typeof(ToDoCountsVm))]
        [Route("TodoItem/GetEmployeeCounts/{startDate:datetime}/{endDate:datetime}/{employeeId:int}/{filterByFailedInspectionItem:bool}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetEmployeeCounts(DateTime startDate, DateTime endDate, int employeeId, bool? filterByFailedInspectionItem = null, bool? filterByReAudit = null, int? buildingId = null)
        {
            return Ok(new TodoItemService(UserCtx).GetAllCounts(startDate, endDate, buildingId, employeeId, filterByFailedInspectionItem, filterByReAudit));
        }

        [HttpGet]
        [Route("TrainingEmployee/GetEmployeeAreas/{employeeId:int}")]
        [EnableQuery]
        public IQueryable<TrainingAreaVm> GetEmployeeAreas(int employeeId)
        {
            return new TrainingEmployeeService(UserCtx).GetEmployeeAreas(employeeId);
        }


        [HttpGet]
        [Route("TrainingEmployee/GetWithJob/{employeeId:int}")]
        [ResponseType(typeof(TrainingEmployeeAndJobVm))]
        public IHttpActionResult GetWithJob(int employeeId)
        {
            return Ok(new TrainingEmployeeService(UserCtx).GetWithJob(employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(ReportOwnershipMonthlyTrendsVm))]
        [Route("TrainingReport/Ownership/MonthlyTrends/{startDate:datetime}/{endDate:datetime}")]
        public IHttpActionResult OwnershipMonthlyTrends(DateTime startDate, DateTime endDate)
        {
            return Ok(new ChaiTea.BusinessLogic.Reporting.Training.ReportService(UserCtx).OwnershipMonthlyTrends(startDate, endDate));
        }


        [HttpPost]
        [Route("User/SetContext")]
        [ResponseType(typeof(void))]
        public IHttpActionResult SetContext(UserContextVm settings)
        {
            if (settings.Client != null &&
                settings.Client.ID <= 0)
            {
                settings.Client = null;
            }

            if (settings.Site != null &&
                settings.Site.ID <= 0)
            {
                settings.Site = null;
            }

            if (settings.Program != null &&
                settings.Program.ID <= 0)
            {
                settings.Program = null;
            }

            if (settings.Language != null &&
                settings.Language.ID <= 0)
            {
                settings.Language = null;
            }

            var usrId = UserContextService.CurrentUserId;
            var newContext = new UserContext(usrId).OverrideSettings(settings.ClientId, settings.SiteId, settings.ProgramId, settings.LanguageId);
            new UserContextService(UserCtx).SaveContextInDB(newContext);

            return Ok();
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserHobbyVm>))]
        [Route("UserHobby/GetByUserId/{userId:int}")]
        public IHttpActionResult GetByUserId(int userId)
        {
            return Ok(new UserHobbyService(UserCtx).Get().Where(t => t.UserId == userId));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserStickerVm>))]
        [Route("UserSticker/GetByUserId/{userId:int}")]
        public IHttpActionResult GetStickersByUserId(int userId)
        {
            return Ok(new UserStickerService(UserCtx).Get().Where(t => t.UserId == userId));
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserTravelVm>))]
        [Route("UserTravel/GetByUserId/{userId:int}")]
        public IHttpActionResult GetTravelsByUserId(int userId)
        {
            return Ok(new UserTravelService(UserCtx).Get().Where(t => t.UserId == userId));
        }
    }
}