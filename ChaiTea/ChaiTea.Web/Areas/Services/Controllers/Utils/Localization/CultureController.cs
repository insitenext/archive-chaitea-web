﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Utils.Localization;
using ChaiTea.BusinessLogic.ViewModels.Utils.Localization;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Localization
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Culture")]
    public class CultureController : BaseCrudApiController<CultureVm>
    {
        protected override Func<CultureVm, int> GetIdentifier
        {
            get { return vm => vm.CultureId; }
        }

        protected override Action<CultureVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.CultureId = id; }
        }

        protected override ICrudService<CultureVm> CreateService
        {
            get { return new CultureService(UserCtx); }
        }
    }
}