﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Localization
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Resource")]
    public class ResourceController : BaseApiController
    {
        public class ResourcesVm
        {
            public Dictionary<string, string> Menu { get; set; }
            public Dictionary<string, string> Content { get; set; }
        }
  
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(ResourcesVm))]
        public IHttpActionResult Get()
        {
            var resources = new ResourcesVm()
            {
                Menu = GetResourceSet(Resources.Menu.ResourceManager.GetResourceSet(Thread.CurrentThread.CurrentUICulture, true, true)),
                Content = GetResourceSet(Resources.Menu.ResourceManager.GetResourceSet(Thread.CurrentThread.CurrentUICulture, true, true))
            };
            return Ok(resources);
        }

        private Dictionary<string, string> GetResourceSet(ResourceSet resourceSet)
        {
            var enumerator = resourceSet.GetEnumerator();
            var dictionary = new Dictionary<string, string>();
            while (enumerator.MoveNext() && enumerator.Current != null)
            {
                dictionary.Add(Convert.ToString(enumerator.Key), Convert.ToString(enumerator.Value));
            }

            resourceSet = Resources.Menu.ResourceManager.GetResourceSet(new CultureInfo(String.Empty), true, true);
            enumerator = resourceSet.GetEnumerator();
            while (enumerator.MoveNext() && enumerator.Current != null)
            {
                if (!dictionary.Keys.Contains(enumerator.Key.ToString()))
                { dictionary.Add(Convert.ToString(enumerator.Key), Convert.ToString(enumerator.Value)); }
            }

            return dictionary;
        }
    }
}
