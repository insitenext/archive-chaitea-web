﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Localization
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/I18N")]
    public class I18NController : ApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<KeyValuePair<string, string>>))]
        public IHttpActionResult Get(string lang)
        {
            if (String.IsNullOrEmpty(lang))
            { return BadRequest("Missing locale key"); }

            var file = HttpContext.Current.Server.MapPath(String.Format("~/Content/i18n/locale-{0}.json", lang));
            if (File.Exists(file))
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    return Ok(JsonConvert.DeserializeObject(sr.ReadToEnd()));
                }
            }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NoContent));
        }

    }
}
