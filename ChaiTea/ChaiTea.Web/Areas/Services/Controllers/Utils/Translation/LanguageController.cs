﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Utils.Translation;
using ChaiTea.BusinessLogic.ViewModels.Utils.Translation;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Translation
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Language")]
    public class LanguageController : BaseCrudApiController<LanguageVm>
    {
        protected override Func<LanguageVm, int> GetIdentifier
        {
            get { return vm => vm.LanguageId; }
        }

        protected override Action<LanguageVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.LanguageId = id; }
        }

        protected override ICrudService<LanguageVm> CreateService => new LanguageService(UserCtx);

        /// <summary>
        /// Translate a single string "on-the-fly".
        /// </summary>
        /// <param name="sourceLang"></param>
        /// <param name="destLang"></param>
        /// <param name="translateString"></param>
        /// <returns>The translated string if the translation was successful, otherwise, empty.</returns>
        [HttpGet]
        [Route("Translate")]
        [ResponseType(typeof(string))]
        public IHttpActionResult TranslateString(string sourceLang, string destLang, string translateString)
        {
            using (var languageService = new LanguageService(UserCtx))
            {
                string translatedString;

                bool result = languageService.TranslateString(sourceLang, destLang, translateString, out translatedString);

                if (result &&
                    !string.IsNullOrEmpty(translatedString))
                {
                    return Ok(translatedString);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
        }

        /// <summary>
        /// Update translations for a class/object.
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns>The translated string if the translation was successful, otherwise, empty.</returns>
        [HttpPost]
        [Route("Update")]
        [ResponseType(typeof(string))]
        public IHttpActionResult UpdateTranslation(UpdateTranslationRequestModel requestModel)
        {
            using (var languageService = new LanguageService(UserCtx))
            {
                bool result = languageService.UpdateTranslation(requestModel);

                if (result)
                {
                    return Ok(true.ToString());
                }

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.BadRequest));
            }
        }
    }
}