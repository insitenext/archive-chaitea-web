﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Passthrough
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Passthrough")]
    public class PassthroughController : BaseApiController
    {
        /// <summary>
        /// Passthrough method to stream data back from responsibility chart endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Responsibility/{*remaining}")]
        [ResponseType(typeof(Task<HttpResponseMessage>))]
        public Task<HttpResponseMessage> GetResponsibilityChart()
        {
            var uri = $"TODO{Request.RequestUri.Query}";
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            return client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
        }

        /// <summary>
        /// Passthrough method to stream data back from org chart endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("OrgChart/{*remaining}")]
        [ResponseType(typeof(Task<HttpResponseMessage>))]
        public Task<HttpResponseMessage> GetOrgChart()
        {
            var uri = $"https://sbmmanagement298181.jitterbit.net/TESTHQJB1/1.00/Insite_GetHierarchyChart_Dataload{Request.RequestUri.Query}";
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            return client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
        }

        /// <summary>
        /// Passthrough method to stream data back from Terms &amp; Conditions endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("TermsAndConditions/{*remaining}")]
        [ResponseType(typeof(Task<HttpResponseMessage>))]
        public Task<HttpResponseMessage> GetTermsAndConditions()
        {
            var uri = "https://jsonplaceholder.typicode.com/photos/1";

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            return client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
        }
        
        /// <summary>
        /// Passthrough method to stream data back from Cost Overview chart JitterBit endpoint.
        /// </summary>
        /// <returns></returns>
        /// <remarks>Logged in user must have &quot;CostOverview&quot; role.</remarks>
        [HttpGet]
        [Route("CostOverviewChart/{*remaining}")]
        [ResponseType(typeof(Task<HttpResponseMessage>))]
        [AuthorizedRolesApi(Roles = Roles.CostOverview)]
        public Task<HttpResponseMessage> GetCostOverviewChart()
        {
            var uri = "https://jsonplaceholder.typicode.com/photos/1";

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            return client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
        }
        
        /// <summary>
        /// Passthrough method to stream data back from Cost Overview details JitterBit endpoint.
        /// </summary>
        /// <returns></returns>
        /// <remarks>Logged in user must have &quot;CostOverview&quot; role.</remarks>
        [HttpGet]
        [Route("CostOverviewDetails/{*remaining}")]
        [ResponseType(typeof(Task<HttpResponseMessage>))]
        [AuthorizedRolesApi(Roles = Roles.CostOverview)]
        public Task<HttpResponseMessage> GetCostOverviewDetails()
        {
            var uri = "https://jsonplaceholder.typicode.com/photos/1";

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            return client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
        }
    }
}