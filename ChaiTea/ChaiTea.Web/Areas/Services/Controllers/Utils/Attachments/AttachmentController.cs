﻿using Amazon.S3;
using Amazon.S3.Model;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Utils.Attachments;
using ChaiTea.BusinessLogic.Settings;
using ChaiTea.BusinessLogic.ViewModels.Utils.Attachments;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Identity;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Attachments
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Attachment")]
    public class AttachmentController : BaseCrudApiController<AttachmentVm>
    {

        protected override Func<AttachmentVm, int> GetIdentifier
        {
            get { return vm => vm.AttachmentId; }
        }

        protected override Action<AttachmentVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.AttachmentId = id; }
        }

        protected override ICrudService<AttachmentVm> CreateService
        {
            get { return new AttachmentService(UserCtx); }
        }

        /// <summary>
        /// Rotates image on S3 by key and degree
        /// </summary>
        /// <param name="id">Unique identifier id.</param>
        /// <param name="degree">Rotation degree.</param>
        /// <param name="uniqueId">S3 key.</param>
        /// <returns>Collection of ProgramVms</returns>
        [HttpGet]
        [ResponseType(typeof(AttachmentVm))]
        [Route("{id:int}/rotate/{degree:int}/key/{uniqueId}")]
        public async Task<IHttpActionResult> Rotate(int id, int degree, string uniqueId)
        {
            var attachmentService = new AttachmentService(UserCtx);

            uniqueId = uniqueId.Replace("_", ".");

            AttachmentVm attachment = new AttachmentVm();
            attachment.AttachmentTypeId = 1;
            attachment.UniqueId = uniqueId;

            //lookup id
            if (id > 0)
            {
                attachment = attachmentService.Get(id);
            }

            //validate
            int[] validDegrees = new int[3] { 90, 180, 270 };
            var degreeExists = Array.Find(validDegrees, d => d == degree);
            if (attachment == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id), "Image does not exist");
            }
            if (degreeExists == 0 || degree == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(degree), "Invalid degree");
            }

            string bucket = AwsSettings.Bucket;
            string folder = "image";
            string keyName = folder + "/" + attachment.UniqueId;
            RotateFlipType rotationType = RotateFlipType.Rotate90FlipNone;

            //get rotation
            if (degree == 180)
            {
                rotationType = RotateFlipType.Rotate180FlipNone;
            }
            if (degree == 270)
            {
                rotationType = RotateFlipType.Rotate270FlipNone;
            }

            using (IAmazonS3 client = new AmazonS3Client(Amazon.RegionEndpoint.USWest1))
            {

                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucket,
                    Key = keyName
                };

                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = bucket,
                    CannedACL = Amazon.S3.S3CannedACL.PublicRead
                };

                using (GetObjectResponse response = client.GetObject(request))
                {

                    //route image and save
                    Image img = System.Drawing.Image.FromStream(response.ResponseStream);
                    img.RotateFlip(rotationType);

                    //update filename
                    string[] parts = keyName.Split('.');
                    string newKey = Guid.NewGuid() + ".jpg";
                    putRequest.Key = folder + '/' + newKey;

                    //get stream and update PUT request
                    using (MemoryStream stream = new MemoryStream())
                    {
                        img.Save(stream, ImageFormat.Jpeg);
                        stream.Position = 0;
                        putRequest.InputStream = stream;
                        
                        //put new file back on s3
                        PutObjectResponse putResponse = client.PutObject(putRequest);
                    }

                    attachment.UniqueId = newKey;

                    //update db
                    if (id > 0)
                    {
                        attachment = await attachmentService.UpdateAsync(attachment);
                    }
                        
                }

            }

            return Ok(attachment);
        }
    }
}