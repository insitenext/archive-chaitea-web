﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Utils.Tagging;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Utils.Tagging;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Utils.Tagging
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Tag")]
    public class TagController : BaseCrudApiController<TagVm>
    {
        protected override Func<TagVm, int> GetIdentifier
        {
            get { return vm => vm.TagId; }
        }

        protected override Action<TagVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.TagId = id; }
        }

        protected override ICrudService<TagVm> CreateService
        {
            get { return new TagService(UserCtx); }
        }

        /// <summary>
        /// Get a Tags. 
        /// </summary>
        /// <returns>IdNameModel</returns>
        [HttpGet]
        [Route("GetClientTag/{clientId}")]
        [ResponseType(typeof(IList<IdNameModel>))]
        public IHttpActionResult GetClientTag(int clientId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new TagService(UserCtx).GetClientTags(clientId));
        }

    }
}