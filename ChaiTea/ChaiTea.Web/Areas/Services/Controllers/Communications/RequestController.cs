﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Entities.Communications;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Utils.Pagination;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Request")]
    public class RequestController : BaseApiController
    {
        /// <summary>
        /// Returns a filtered list of requests that match the selection criteria.
        /// </summary>
        /// <param name="startDate">Start date.</param>
        /// <param name="endDate">End date.</param>
        /// <param name="userId">Filter to requests issued/created by this user.</param>
        /// <param name="status">Filter to request state - pending, approved or rejected.</param>
        /// <param name="requestType">Filter to types of request.</param>
        /// <param name="top">Return this many records.</param>
        /// <param name="skip">Skip this many records before returning matches.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(List<RequestVm>))]
        public async Task<IHttpActionResult> Get(DateTime? startDate = null, DateTime? endDate = null, int? userId = null, int? status = null, int? requestType = null, int? top = null, int? skip = null)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, top);

            using (var reqSvc = new RequestService(UserCtx))
            {
                var requestList = await reqSvc.GetAsync(dateFilter, userId, status, requestType, pagingFilter);

                foreach (var request in requestList)
                {
                    request.RequestedByProfileImages = reqSvc.GetEmployeeProfileImage(request.RequestedBy.EmployeeId);
                    request.OriginalDescription = reqSvc.GetTranslation(RequestService.RequestTranslationType.Description,
                                                                        request.RequestId,
                                                                        request.SourceLanguageId);

                    if (request.ProcessedBy != null &&
                        request.ProcessedBy.UserId > 0)
                    {
                        request.ProcessedByProfileImages = reqSvc.GetUserProfileImage(request.ProcessedBy.UserId);
                        request.OriginalProcessorComment = reqSvc.GetTranslation(RequestService.RequestTranslationType.ProcessorComment,
                                                                                 request.RequestId,
                                                                                 request.SourceLanguageId);
                    }
                }

                return Ok(requestList);
            }
        }

        /// <summary>
        /// Return a specified request, based on Id.
        /// </summary>
        /// <param name="id">RequestId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(RequestVm))]
        public IHttpActionResult Get(int id)
        {
            using (var reqSvc = new RequestService(UserCtx))
            {
                return Ok(reqSvc.GetById(id));
            }
        }

        /// <summary>
        /// Return all requests
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ResponseType(typeof(List<RequestVm>))]
        public IHttpActionResult GetAll()
        {
            using (var reqSvc = new RequestService(UserCtx))
            {
                return Ok(reqSvc.GetAll());
            }
        }

        /// <summary>
        /// Return a list of supported request types.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("RequestTypes")]
        [ResponseType(typeof(IEnumerable<KeyValue<int, string>>))]
        public IHttpActionResult GetTypes()
        {
            return Ok(RequestService.GetRequestTypes());
        }

        /// <summary>
        /// Return a summary of counts of requests in each state.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId">Counts only for this user.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("StatusCounts")]
        [ResponseType(typeof(RequestStatusCountVm))]
        public IHttpActionResult GetStatusCounts(DateTime? startDate = null, DateTime? endDate = null, int? userId = null)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);

            using (var reqSvc = new RequestService(UserCtx))
            {
                return Ok(reqSvc.GetStatusCounts(dateFilter, userId));
            }
        }

        /// <summary>
        /// Get Request records to which the logged in user has permissions, matching the supplied search criteria.
        /// </summary>
        /// <param name="tags">The list of site tags to match against - list cannot be empty</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="skip">Skip this many records</param>
        /// <param name="take">Return (at most) this many records</param>
        /// <param name="requestStatus">Filter on the status of the Request (default: null - no filter)</param>
        /// <param name="userId">Filter on RequestedById.</param>
        /// <param name="requestType">Filter on request type.</param>
        /// <returns>An enumerable list of <see cref="RequestVm"/></returns>
        [HttpPost]
        [Route("ByTags")]
        [ResponseType(typeof(IEnumerable<RequestVm>))]
        public async Task<IHttpActionResult> GetByTags([FromBody] List<string> tags, DateTime? startDate = null, DateTime? endDate = null, int? skip = null, int? take = null, int? requestStatus = null, int? userId = null, int? requestType = null)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeOptionalRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var svc = new RequestService(UserCtx))
            {
                return Ok(await svc.GetByTagsAsync(pagingFilter, dateFilter, tags, userId, requestStatus, requestType));
            }
        }

        /// <summary>
        /// Returns a summary of Requests by Site ID
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="startDate">Beginning date of the range</param>
        /// <param name="endDate">End date of the range</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SiteSummary")]
        [ResponseType(typeof(RequestSiteSummary))]
        public async Task<IHttpActionResult> GetSiteSummary(int id, DateTime startDate, DateTime endDate)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateRequired(startDate, endDate);
            
            using (var svc = new RequestService(UserCtx))
            {
                return Ok(await svc.GetSiteSummary(id, dateFilter));
            }
        }
        
        /// <summary>
        /// Returns a summary of Request records grouped by Site ID, based on the tag list provided.
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching requests - cannot be empty.</param>
        /// <param name="startDate">Beginning of the range.</param>
        /// <param name="endDate">End of the range.</param>
        /// <param name="take">Number of rows to take.</param>
        /// <param name="skip">Number of pages to skip.</param>
        /// <returns>An <see cref="RequestSummary">RequestSummary</see> object with the summary data.</returns>
        [HttpPost]
        [Route("SiteSummariesByTags")]
        [ResponseType(typeof(PaginatedData<RequestSummary>))]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate, int? take = null, int? skip = null)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeRequiredRange(startDate, endDate);
            var pagingFilter = PagingFilter.TryValidate(skip, take);

            using (var reqSvc = new RequestService(UserCtx))
            {
                return Ok(await reqSvc.GetSiteSummariesByTagsAsync(tags, pagingFilter, dateFilter));
            }
        }

        /// <summary>
        /// Returns a summary of the Request records for all sites.
        /// </summary>
        /// <param name="tags">ServiceAreaTags to use for searching compliments - cannot be empty.</param>
        /// <param name="startDate">Beginning of the FeedbackDate range.</param>
        /// <param name="endDate">End of the FeedbackDate range.</param>
        /// <returns>An <see cref="RequestSummary">RequestSummary</see> object with the summary data.</returns>
        [HttpPost]
        [Route("AllSiteSummariesByTags")]
        [ResponseType(typeof(RequestSummary))]
        public async Task<IHttpActionResult> GetAllSiteSummariesByTags([FromBody] List<string> tags, DateTime startDate, DateTime endDate)
        {
            if (tags == null ||
                tags.Count < 1)
            {
                throw new ArgumentException("No tags passed.", nameof(tags));
            }

            var dateFilter = new DateTimeRequiredRange(startDate, endDate);

            using (var svc = new RequestService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummariesByTagsAsync(tags, dateFilter));
            }
        }

        /// <summary>
        /// Allow export of requests as a CSV file.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <param name="exportType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Export")]
        public async Task<IHttpActionResult> ExportAs(DateTime? startDate = null, DateTime? endDate = null, int? userId = null, string exportType = null)
        {
            var dateFilter = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrWhiteSpace(exportType))
            {
                exportType = RequestService.RequestExport_Default;
            }

            using (var reqSvc = new RequestService(UserCtx))
            {
                var csvData = await reqSvc.ExportData(dateFilter, userId, exportType);

                if (csvData.Length > 0)
                {
                    MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(csvData));

                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                                 {
                                     Content = new ByteArrayContent(stream.ToArray())
                                 };

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                                                                {
                                                                    FileName = "RequestData.CSV"
                                                                };

                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");

                    return ResponseMessage(result);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
        }

        /// <summary>
        /// Create new request instance.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Create(RequestVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var rs = new RequestService(UserCtx))
            {
                var createdRequest = await rs.CreateAsync(vm);

                if (createdRequest != null)
                {
                    return Ok(createdRequest);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.BadRequest));
        }

        /// <summary>
        /// Update request status.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(RequestVm))]
        public async Task<IHttpActionResult> Update(int id, RequestVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vm.RequestId = id;

            using (var rs = new RequestService(UserCtx))
            {
                var updatedRequest = await rs.UpdateAsync(vm);

                if (updatedRequest != null)
                {
                    return Ok(updatedRequest);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        /// <summary>
        /// Delete request.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var rs = new RequestService(UserCtx))
            {
                var success = await rs.DeleteAsync(id);

                if (success)
                {
                    return Ok();
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }
    }
}