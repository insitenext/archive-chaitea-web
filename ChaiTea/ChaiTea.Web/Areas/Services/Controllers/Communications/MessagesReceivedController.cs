﻿using ChaiTea.BusinessLogic.DAL;
using ChaiTea.BusinessLogic.Reporting.Training.ViewModels;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using ChaiTea.BusinessLogic.Entities.Organizations;
using ChaiTea.BusinessLogic.Entities.Personnel;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/MessagesReceived")]
    [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers)]
    public class MessagesReceivedController : BaseApiController
    {
        private ApplicationDbContext _dbContext;
        protected ApplicationDbContext DbContext => _dbContext ?? (_dbContext = new ApplicationDbContext());

        [HttpGet]
        [EnableQuery]
        public IQueryable<MessageVm> Get()
        {
            var profileAttachmentQuery = DbContext.UserAttachments
                                              .Where(x => x.IsActive && x.UserId == UserCtx.UserId)
                                              .Where(x => x.UserAttachmentTypeId == (int) UserAttachmentTypes.ProfileImage);
            
            return new MessagesReceivedService(UserCtx).Get(DbContext).Select(t => new MessageVm()
            {
                TrainingMessageId = t.MessageId,
                Text = t.Text,
                Jobs = t.MessagePositions
                        .Where(j => j.IsActive)
                        .Select(j => new ConciseJobVm
                        {
                            JobId = j.JobId,
                            Name = j.Job.Name
                        })
                        .ToList(),
                Sites = t.MessageOrgs
                         .Where(mo => mo.IsActive)
                         .Select(mo => new ClientSiteVm()
                         {
                             Id = mo.OrgId,
                             Type = (OrgTypeVm)mo.Org.OrgTypeId,
                             Name = (mo.Org.OrgTypeId == (int)OrgTypeVm.Site ? mo.Org.Parents.Select(p => p.ParentOrg.Name).FirstOrDefault() + " - " : string.Empty) + mo.Org.Name
                         })
                         .ToList(),
                Department = new DepartmentVm()
                {
                    DepartmentId = t.DepartmentInfo.DepartmentId,
                    
                    DepartmentImageUrl = t.DepartmentId != (int) Departments.Me
                        ? t.DepartmentInfo.DepartmentImageUrl
                        : profileAttachmentQuery.Select(x => x.Attachment.UniqueId).FirstOrDefault(),
                    
                    Name = t.DepartmentInfo.Name
                },
                Title = t.Title,
                IsCustomer = t.SendToCustomer,
                CreateById = t.CreateById,
                CreateDate = t.CreateDate
            });

        }

        [HttpGet]
        [Route("SentMessagesCount")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetSentMessagesCount(DateTime startDate, DateTime endDate, int? departmentId = null)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            return Ok(new { SentMessagesCount = new MessagesReceivedService(UserCtx).GetSentMessagesCount(startDate, endDate, departmentId) });
        }

        [HttpGet]
        [Route("TabCounts")]
        [ResponseType(typeof(MessagesReceivedTabCountVm))]
        public IHttpActionResult GetTabCounts(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            return Ok(new MessagesReceivedService(UserCtx).GetTabCounts(startDate, endDate));
        }

        [HttpGet]
        [Route("MessagesSentInfoByDepartment")]
        [ResponseType(typeof(IEnumerable<MessagesFromDepartmentsVm>))]
        public IHttpActionResult GetMessagesSentInfoByDepartment(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesSentInfoByDepartment(startDate, endDate));
        }

        [HttpGet]
        [Route("MessagesReceivedInfoByJob")]
        [ResponseType(typeof(IEnumerable<TotalRecipientsByJobReport>))]
        public IHttpActionResult GetMessagesReceivedInfoByJob(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddMilliseconds(-1);
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesReceivedInfoByJob(startDate, endDate));
        }

        [HttpGet]
        [Route("MessageStatsById/{messageId:int}")]
        [ResponseType(typeof(MessagesReceivedStatsVm))]
        public IHttpActionResult GetMessageStatsById(int messageId)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMessageStatsById(messageId));
        }

        [HttpGet]
        [Route("OrgUserById/{messageId:int}/{top:int}/{skip:int}/{isNew:bool?}")]
        [ResponseType(typeof(IEnumerable<int>))]
        public IHttpActionResult GetOrgUserById(int messageId, int top, int skip, bool? isNew = null)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetOrgUserById(messageId, top, skip, isNew));
        }

        [HttpGet]
        [Route("AllJobsCount")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetAllJobsCount()
        {
            return Ok(new { JobsCount = new MessagesReceivedService(UserCtx).GetAllJobsCount() });
        }

        [HttpGet]
        [Route("StatsByEmployee")]
        [ResponseType(typeof(IEnumerable<MessagesReceivedStatsByEmployeeVm>))]
        public IHttpActionResult GetMessagesReceivedStatsByEmployee(DateTime startDate, DateTime endDate, int positionId, int top, int skip)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesReceivedStatsByEmployee(startDate, endDate, positionId, top, skip));
        }

        [HttpGet]
        [Route("ByEmployee")]
        [ResponseType(typeof(IEnumerable<MessagesReceivedByEmployeeVm>))]
        public IHttpActionResult GetMessagesReceivedStatsByEmployee(int employeeId, DateTime startDate, DateTime endDate, int top, int skip, bool? isNew = null)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesReceivedByEmployee(employeeId, startDate, endDate, top, skip, isNew));
        }

        [HttpGet]
        [Route("MonthlyTrendByDepartment")]
        [ResponseType(typeof(IEnumerable<MonthlyMessagesSentByDepartmentReport>))]
        public IHttpActionResult GetMonthlyTrendOfMessagesSentByDepartment(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMonthlyTrendOfMessagesSentByDepartment(startDate, endDate));
        }

        [HttpGet]
        [Route("MessagesReceivedByEmployeeStatusCounts")]
        [ResponseType(typeof(MessagesReceivedByEmployeeCountsByStatusVm))]
        public IHttpActionResult GetMessagesReceivedByEmployeeStatusCounts(int employeeId, DateTime startDate, DateTime endDate)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesReceivedByEmployeeStatusCounts(employeeId, startDate, endDate));
        }

        [HttpGet]
        [Route("ByEmployeesCounts")]
        [ResponseType(typeof(MessagesReceivedByEmployeesCountVm))]
        public IHttpActionResult GetMessagesReceivedByEmployeesCounts(DateTime startDate, DateTime endDate, int positionId)
        {
            return Ok(new MessagesReceivedService(UserCtx).GetTotalMessagesReceivedByAssociatesCount(startDate, endDate, positionId));
        }

        [HttpGet]
        [EnableQuery]
        [Route("Messages")]
        [ResponseType(typeof(IQueryable<MessagesReceivedVm>))]
        public IHttpActionResult GetMessages()
        {
            return Ok(new MessagesReceivedService(UserCtx).GetMessagesReceived(DbContext));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _dbContext?.Dispose();
            }
        }
    }
}