﻿using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Messages")]
    public class AllMessagesController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<MessagesService.MessageListItemDTO>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult GetMessageListItems(DateTime? startDate = null, DateTime? endDate = null, int? top = null, int? skip = 0, int? departmentId = null, MessagesService.GetMessageListItemsOrderBy? orderBy = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            return Ok(new MessagesService(UserCtx).GetMessageListItems(_filterToDate, _filterToPage, departmentId, orderBy));
        }

        [HttpGet]
        [Route("{messageId:int}")]
        [ResponseType(typeof(MessagesService.MessageDetailDTO))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult GetMessageDetails(int messageId)
        {
            return Ok(new MessagesService(UserCtx).GetMessageDetails(messageId));
        }

        [HttpGet]
        [Route("{messageId:int}/Recipients")]
        [ResponseType(typeof(MessagesService.MessageRecipientsDTO))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult GetMessageRecipients(int messageId)
        {
            return Ok(new MessagesService(UserCtx).GetMessageRecipients(messageId));
        }

        [HttpGet]
        [Route("TotalCount")]
        [ResponseType(typeof(int))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult GetTotalCount(int? departmentId = null)
        {
            return Ok(new MessagesService(UserCtx).GetTotalCount(departmentId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult CreateAndSendMesssage(MessagesService.CreateAndSendMesssageDTO dto)
        {
            return Ok(new MessagesService(UserCtx).CreateAndSendMesssage(dto));
        }

        [HttpGet]
        [Route("OrderByOptions")]
        [ResponseType(typeof(IEnumerable<MessagesService_GetMessageListItemsOrderByExtension.OrderByDTO>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Customers + ", " + Roles.MessageBuilders)]
        public IHttpActionResult GetValidOrderByValues()
        {
            return Ok(MessagesService_GetMessageListItemsOrderByExtension.OrderByValues);
        }
    }
}
