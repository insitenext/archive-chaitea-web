﻿using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/MessageStats")]
    public class MessageStatsController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IEnumerable<NotificationVm>))]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(new MessageStatsService(UserCtx).Get());
        }

        [HttpGet]
        [Route("{id:int}")]
        [EnableQuery]
        public IQueryable<NotificationVm> Get(int id)
        {
            return new MessageStatsService(UserCtx).Get(id);
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MessageStatsVm>))]
        [Route("MessageStats")]
        public IHttpActionResult GetMessageStats(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessageStatsService(UserCtx).GetMessageStats(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(MessageStatsVm))]
        [Route("MessageStats/{id:int}")]
        public IHttpActionResult GetMessageStats(int id)
        {
            return Ok(new MessageStatsService(UserCtx).GetMessageStats(id));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MessagesService.MessageCountsDTO>))]
        [Route("MessageCounts")]
        public IHttpActionResult GetMessageCounts(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessagesService(UserCtx).GetMessageCounts(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(EmployeeMessageVm))]
        [Route("Employee")]
        public IHttpActionResult GetEmployee(int employeeId)
        {
            return Ok(new MessageStatsService(UserCtx).GetEmployee(employeeId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<MessageStatsVm>))]
        [Route("Messages")]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetMessages(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessageStatsService(UserCtx).GetMessages(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(MessagesService.MonthlyMessagesSentReportDTO))]
        [Route("MessagesSentMonthly")]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetMessagesSentMonthly(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessagesService(UserCtx).GetMonthlyMessagesSentReport(startDate, endDate));
           
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<TotalRecipientsByJobReport>))]
        [Route("TotalRecipients")]
        public IHttpActionResult GetTotalRecipients(DateTime startDate, DateTime endDate)
        {
            return Ok(new MessageStatsService(UserCtx).GetMessageStatsTotalRecipients(startDate, endDate));
        }
    }
}