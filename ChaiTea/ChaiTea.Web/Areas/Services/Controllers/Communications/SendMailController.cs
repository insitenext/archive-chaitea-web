﻿using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;
using ChaiTea.Web.Messenger;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SendMail")]
    public class SendMailController : BaseApiController
    {
        public SendMailController()
        {
        }

        // POST: api/Services/SendMail/
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostSendMail(MailMessageVm mail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await EmailMessenger.SendEmailAsync(mail);
            return Ok();
        }
    }
}
