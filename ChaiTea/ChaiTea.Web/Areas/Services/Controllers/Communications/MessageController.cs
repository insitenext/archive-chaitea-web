﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.AppMessaging.Helpers;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [Obsolete("Use one of the other Message Controllers; this one is just here until front-end can get off of these endpoints", false)]
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/TrainingMessage")]
    public class MessageController : BaseCrudApiController<MessageVm>
    {
        protected override Func<MessageVm, int> GetIdentifier
        {
            get { return e => e.TrainingMessageId; }
        }

        protected override Action<MessageVm, int> SetIdentifier
        {
            get { return (e, i) => e.TrainingMessageId = i; }
        }

        protected override ICrudService<MessageVm> CreateService
        {
            get { return new MessageService(UserCtx); }
        }

        [HttpGet]
        [Route("")]
        public override IQueryable<MessageVm> Get()
        {
            // The Post function is overridden and thus the Route overrides the attributes in the BaseController,
            // so have to explicitly set the get HTTPGet here as well
            return base.Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        public override IHttpActionResult Get(int id)
        {
            // The Post function is overridden and thus the Route overrides the attributes in the BaseController,
            // so have to explicitly set the get HTTPGet here as well
            return base.Get(id);
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public override async Task<IHttpActionResult> Post(MessageVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var isMobile = vm.IsMobile;
            var sendToTrainingAdmin = vm.IsTrainingAdmin;

            vm = await Service.CreateAsync(vm);

            BusMessenger.Send(new BusinessLogic.AppMessaging.Commands.NotifyMessageRecipientsCommand
                              {
                                  MessageId = vm.TrainingMessageId,
                                  SendToTrainingAdmins = sendToTrainingAdmin,
                                  IsMobile = isMobile
                              });

            return Ok(vm);
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("AllCounts")]
        public IHttpActionResult GetAllCounts()
        {
            return Ok(new { Count = new MessageService(UserCtx).GetAllCounts(null) });
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("DepartmentCounts/{departmentId:int}")]
        public IHttpActionResult GetDepartmentCounts(int departmentId)
        {
            return Ok(new { Count = new MessageService(UserCtx).GetAllCounts(departmentId) });
        }

        /// <summary>
        /// This endpoind is for retrieve current user Job Title. Endpoind will be removed as soon we'll fix User/Employee stuffs.
        /// </summary>
        /// <param></param>
        /// <returns>Current User Job Title</returns>
        [HttpGet]
        [ResponseType(typeof(string))]
        [Route("GetJobTitle")]
        public IHttpActionResult GetJobTitle()
        {
            return Ok(new { JobTitle = new MessageService(UserCtx).GetJobTitle() });
        }

        /// <summary>
        /// This endpoint will do a soft delete.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(bool))]
        public override async Task<IHttpActionResult> Delete(int id)
        {
            return Ok(new { Result = await new MessageService(UserCtx).DeleteAsync(id) });
        }
    }
}
