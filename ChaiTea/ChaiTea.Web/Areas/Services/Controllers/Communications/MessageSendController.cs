﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.AppMessaging.Commands;
using ChaiTea.BusinessLogic.AppMessaging.Helpers;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Message")]
    public class MessageSendController : BaseCrudApiController<MessageVm>
    {
        protected override Func<MessageVm, int> GetIdentifier
        {
            get { return e => e.TrainingMessageId; }
        }

        protected override Action<MessageVm, int> SetIdentifier
        {
            get { return (e, i) => e.TrainingMessageId = i; }
        }

        protected override ICrudService<MessageVm> CreateService
        {
            get { return new MessageSendService(UserCtx); }
        }

        [HttpGet]
        [Route("")]
        public override IQueryable<MessageVm> Get()
        {
            // The Post function is overridden and thus the Route overrides the 
            // attributes in the BaseController, so have to explicitly set the 
            // get HTTPGet here as well

            return base.Get();
        }

        [HttpGet]
        [Route("{id:int}")]
        public override IHttpActionResult Get(int id)
        {
            // The Post function is overridden and thus the Route overrides the 
            // attributes in the BaseController, so have to explicitly set the 
            // get HTTPGet here as well

            return base.Get(id);
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public override async Task<IHttpActionResult> Post(MessageVm vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MessageVm createdMsg;
            

            using (var sendService = new MessageSendService(UserCtx))
            {
                createdMsg = await sendService.CreateAsync(vm);
            }

            BusMessenger.Send(new NotifyMessageRecipientsCommand
                              {
                                  MessageId = createdMsg.TrainingMessageId,
                                  IsMobile = vm.IsMobile
                              });

            return Ok(createdMsg);
        }
    }
}
