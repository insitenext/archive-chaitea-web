﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Communications
{

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Notification")]
    public class NotificationController : BaseCrudApiController<NotificationVm>
    {
        protected override Func<NotificationVm, int> GetIdentifier
        {
            get { return e => e.NotificationId; }
        }

        protected override Action<NotificationVm, int> SetIdentifier
        {
            get { return (n, id) => n.NotificationId = id; }
        }

        protected override ICrudService<NotificationVm> CreateService => new NotificationService(UserCtx);

        [HttpGet]
        [EnableQuery]
        [Route("Notifications")]
        public IHttpActionResult GetNotifications()
        {
            return Ok(new NotificationService(UserCtx).GetNotifications());
        }

        [HttpGet]
        [Route("Messages")]
        [ResponseType(typeof(IEnumerable<NotificationVm>))]
        public IHttpActionResult GetMessageNotifications(int? top = null, int? skip = null, int? userId = null)
        {
            return Ok(NotificationService.GetMessageNotificationsEx(UserCtx, top, skip, userId));
        }

        /// <summary>
        /// Mark all the undread messages for this user as read.
        /// </summary>
        /// <returns>True if operation was successful.</returns>
        [HttpPut]
        [Route("MarkAllAsRead")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> MarkAllNotificationsAsRead()
        {
            int orgUserId;

            using (var userContextService = new UserContextService(UserCtx))
            {
                orgUserId = userContextService.GetOrgUserId();
            }

            using (var notificationService = new NotificationService(UserCtx))
            {
                return Ok(await notificationService.MarkAllNotificationsAsRead(orgUserId));
            }
        }
    }
}
