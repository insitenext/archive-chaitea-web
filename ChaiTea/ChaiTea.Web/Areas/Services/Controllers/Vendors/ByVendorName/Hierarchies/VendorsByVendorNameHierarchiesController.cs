﻿using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Vendors.ByVendorName.Hierarchies
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Vendors/{VendorName}/Hierarchies")]
    public class VendorsByVendorNameHierarchiesController : BaseApiController
    {
        [HttpGet]
        [Route("Client/Cards")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers + "," + Roles.OrgChartViewer)]
        [ResponseType(typeof(IEnumerable<OrgChartService.CardDTO>))]
        public IHttpActionResult GetClientHierachy(string VendorName)
        {
            return Ok(new OrgChartService(UserCtx).GetClientHierarchy());
        }

        [HttpGet]
        [Route("Site/Cards")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers + "," + Roles.OrgChartViewer)]
        [ResponseType(typeof(IEnumerable<OrgChartService.CardDTO>))]
        public IHttpActionResult GetSiteHierachy(string VendorName)
        {
            return Ok(new OrgChartService(UserCtx).GetSiteHierarchy());
        }

        [HttpGet]
        [Route("Site/Employees")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers + "," + Roles.OrgChartViewer)]
        [ResponseType(typeof(IEnumerable<OrgChartService.EmployeeInfo>))]
        public IHttpActionResult GetSiteEmployees(string VendorName)
        {
            return Ok(new OrgChartService(UserCtx).GetSiteEmployees());
        }
    }
}
