﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.AppUtils.Settings;
using ChaiTea.BusinessLogic.Services.Invoices;
using ChaiTea.BusinessLogic.ViewModels.Invoices;
using ChaiTea.Web.Identity;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Invoices
{
    [AuthorizedRolesApi(Roles = Roles.BillingViewers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Invoice")]
    public class InvoiceController : BaseCrudApiController<InvoiceVm>
    {
        protected override Func<InvoiceVm, int> GetIdentifier
        {
            get { return vm => vm.InvoiceId; }
        }

        protected override Action<InvoiceVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.InvoiceId = id; }
        }

        protected override ICrudService<InvoiceVm> CreateService
        {
            get { return new InvoiceService(UserCtx); }
        }

        [HttpGet]
        [ResponseType(typeof(DateTime?))]
        [Route("LastUpdatedDate")]
        public IHttpActionResult GetLastUpdateDate()
        {
            var invoiceSvc = new InvoiceService(UserCtx);
            var lastUpdatedInvoice = invoiceSvc.Get().Where(p => p.UpdateById == DataSyncSettings.UserId).OrderByDescending(p => p.UpdateDate).FirstOrDefault();

            if (lastUpdatedInvoice != null && lastUpdatedInvoice.UpdateDate != null)
            { return Ok(new { LastUpdatedDate = lastUpdatedInvoice.UpdateDate }); }

            var lastCreatedInvoice = invoiceSvc.Get().Where(p => p.CreateById == DataSyncSettings.UserId).OrderByDescending(p => p.CreateDate).FirstOrDefault();
            if (lastCreatedInvoice != null && lastCreatedInvoice.CreateDate != null)
            { return Ok(new { LastUpdatedDate = lastUpdatedInvoice.CreateDate }); }

            return Ok(new { LastUpdatedDate = DateTime.MinValue });
        }

        [HttpGet]
        [Route("InvoiceItemsByProgram")]
        public IHttpActionResult GetInvoiceItemsByProgram(DateTime startDate, DateTime endDate, int programId, int skip, int top)
        {
            return Ok(new InvoiceService(UserCtx).GetInvoiceItemsByProgram(startDate, endDate, programId, skip, top));
        }

        [HttpGet]
        [Route("InvoiceByUnpaidFlag")]
        public IHttpActionResult GetInvoiceByUnpaidFlag(DateTime startDate, DateTime endDate, bool isUnpaid, int skip, int top)
        {
            return Ok(new InvoiceService(UserCtx).GetInvoiceByUnpaidFlag(startDate, endDate, isUnpaid, skip, top));
        }
    }
}