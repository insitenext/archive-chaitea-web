﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.BusinessLogic.ViewModels.Personnel;
using ChaiTea.Web.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Personnel
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserSticker")]
    public class UserStickerController : BaseCrudApiController<UserStickerVm>
    {
        protected override ICrudService<UserStickerVm> CreateService => new UserStickerService(UserCtx);

        protected override Func<UserStickerVm, int> GetIdentifier
        {
            get { return vm => vm.UserStickerId; } 
        }

        protected override Action<UserStickerVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.UserStickerId = id; }
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserStickerVm>))]
        [Route("ByUserId/{userId:int}")]
        public IHttpActionResult GetByUserId(int userId)
        {
            var userService = new UserStickerService(UserCtx);
            return Ok(userService.Get().Where(t => t.UserId == userId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserStickerVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public override async Task<IHttpActionResult> Post(UserStickerVm vm)
        {
            if (vm == null)
            {
                throw new ArgumentNullException(nameof(vm), "No data for UserSticker object.");
            }

            var contextService = new UserContextService(UserCtx);

            if (!contextService.IsInRole(Roles.Managers))
            {
                if (vm.UserId != UserCtx.UserId)
                {
                    throw new UnauthorizedAccessException("Users are only allowed to add to their own stickers.");
                }
            }

            UserStickerService stickerService = new UserStickerService(UserCtx);

            // Return the newly created object

            UserStickerVm userSticker = await stickerService.CreateAsync(vm);

            return Ok(userSticker);
        }

        [HttpPut]
        [ResponseType(typeof(UserStickerVm))]
        [Route("{userStickerId:int}")]
        public override async Task<IHttpActionResult> Put(int userStickerId, UserStickerVm vm)
        {
            if (vm == null)
            {
                throw new ArgumentNullException(nameof(vm), "No data for UserSticker.");
            }

            var contextService = new UserContextService(UserCtx);

            UserStickerService stickerService = new UserStickerService(UserCtx);

            // Return the newly created object

            UserStickerVm userSticker = await stickerService.UpdateAsync(vm);

            return Ok(userSticker);
        }

        public class ResponseObject
        {
            public bool Success { get; set; }
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(ResponseObject))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public override async Task<IHttpActionResult> Delete(int id)
        {
            var contextService = new UserContextService(UserCtx);
            UserStickerService stickerService = null;
            UserStickerVm userSticker = null;

            if (!contextService.IsInRole(Roles.Managers))
            {
                stickerService = new UserStickerService(UserCtx);

                if (userSticker.UserId != UserCtx.UserId)
                {
                    throw new UnauthorizedAccessException("Users are only allowed to delete their own stickers.");
                }
            }
            else
            {
                stickerService = new UserStickerService(UserCtx);
                userSticker = stickerService.Get(id);
            }

            if (userSticker != null &&
                userSticker.UserStickerId != 0)
            {
                var responseObject = new ResponseObject()
                {
                    Success = await stickerService.DeleteAsync(userSticker.UserStickerId)
                };

                return Ok(responseObject);
            }

            return NotFound();
        }
    }
}