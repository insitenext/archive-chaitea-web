﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.BusinessLogic.ViewModels.Personnel;
using ChaiTea.BusinessLogic.Entities.Personnel;

namespace ChaiTea.Web.Areas.Services.Controllers.Personnel
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserTravel")]
    public class UserTravelController : BaseCrudApiController<UserTravelVm>
    {
        protected override ICrudService<UserTravelVm> CreateService
        {
            get { return new UserTravelService(UserCtx); }
        }

        protected override Func<UserTravelVm, int> GetIdentifier
        {
            get { return vm => vm.UserTravelId; }
        }

        protected override Action<UserTravelVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.UserTravelId = id; }
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserTravelVm>))]
        [Route("ByUserId/{userId:int}")]
        public IHttpActionResult GetByUserId(int userId)
        {
            var travelService = new UserTravelService(UserCtx);
            return Ok(travelService.GetById(userId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserTravelVm>))]
        [Route("TravelByUserId/{userId:int}")]
        public IHttpActionResult GetTravelByUserId(int userId)
        {
            var travelService = new UserTravelService(UserCtx);
            return Ok(travelService.GetTravelById(userId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserTravelVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public override async Task<IHttpActionResult> Post(UserTravelVm vm)
        {
            var contextService = new UserContextService(UserCtx);

            if (!contextService.IsInRole(Roles.Managers))
            {
                if (vm.UserId != UserCtx.UserId)
                {
                    throw new UnauthorizedAccessException($"Users are only allowed to add to their own travels.");
                }
            }

            var travelService = new UserTravelService(UserCtx);

            // Return the newly created object

            UserTravelVm userTravel = await travelService.CreateAsync(vm);

            return Ok(userTravel);
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(UserTravelVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public override async Task<IHttpActionResult> Put(int id, UserTravelVm vm)
        {
            var contextService = new UserContextService(UserCtx);

            if (!contextService.IsInRole(Roles.Managers))
            {
                if (vm.UserId != UserCtx.UserId)
                {
                    throw new UnauthorizedAccessException($"Users are only allowed to update their own travel info.");
                }
            }

            var travelService = new UserTravelService(UserCtx);

            // Return the newly created object

            UserTravelVm userTravel = await travelService.UpdateAsync(vm);

            return Ok(userTravel);
        }

        [HttpGet]
        [Route("CountriesEx")]
        [ResponseType(typeof(IEnumerable<Country.CountryListItemExDTO>))]
        public IHttpActionResult GetCountryListItems()
        {
            return Ok(Country.GetListEx());
        }

        [HttpGet]
        [Route("CountriesEx/{CountryCodeOrId}")]
        [ResponseType(typeof(Country.CountryListItemExDTO))]
        public IHttpActionResult GetCountryById(string countryCodeOrId)
        {
            return Ok(Country.GetCountry(countryCodeOrId));
        }
    }
}