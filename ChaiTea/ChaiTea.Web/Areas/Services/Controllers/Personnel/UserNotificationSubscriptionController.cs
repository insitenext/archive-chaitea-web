﻿using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.BusinessLogic.ViewModels.Personnel;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Personnel
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserNotificationSubscription")]
    public class UserNotificationSubscriptionController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IEnumerable<UserNotificationSubscriptionVm>))]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(UserNotificationSubscriptionService.Get(UserCtx.UserId));
        }

        [HttpPut]
        [Route("")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Put(IEnumerable<UserNotificationSubscriptionVm> vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var success = await UserNotificationSubscriptionService.UpdateAsync(UserCtx.UserId, vm);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpDelete]
        [Route("")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Delete()
        {
            var success = await UserNotificationSubscriptionService.DeleteAsync(UserCtx.UserId);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
    }
}
