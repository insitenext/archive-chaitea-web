﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.BusinessLogic.ViewModels.Personnel;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Personnel
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/UserHobby")]
    public class UserHobbyController : BaseCrudApiController<UserHobbyVm>
    {
        protected override ICrudService<UserHobbyVm> CreateService
        {
            get { return new UserHobbyService(UserCtx); }
        }

        protected override Func<UserHobbyVm, int> GetIdentifier
        {
            get { return vm => vm.UserHobbyId; }
        }

        protected override Action<UserHobbyVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.UserHobbyId = id; }
        }

        [HttpGet]
        [ResponseType(typeof(IQueryable<UserHobbyVm>))]
        [Route("ByUserId/{userId:int}")]
        public IHttpActionResult GetByUserId(int userId)
        {
            return Ok(new UserHobbyService(UserCtx).Get().Where(t => t.UserId == userId));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        [Route("UpdateAll")]
        public async Task<IHttpActionResult> UpdateAll(List<UserHobbyVm> vm)
        {
            if (!new UserContextService(UserCtx).IsInRole(Roles.Managers))
            {
                if (vm.Any(t => t.UserId != UserCtx.UserId))
                {
                    throw new UnauthorizedAccessException($"Users are only allowed to update their own hobbies.");
                }
            }
            var hobbyService = new UserHobbyService(UserCtx);
            foreach (var item in vm)
            {
                await hobbyService.UpdateAsync(item);
            }
            return Ok();
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserHobbyVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.Employees)]
        public override async Task<IHttpActionResult> Post(UserHobbyVm vm)
        {
            var contextService = new UserContextService(UserCtx);
            if (!contextService.IsInRole(Roles.Managers))
            {
                if (vm.UserId != UserCtx.UserId)
                {
                    throw new UnauthorizedAccessException($"Users are only allowed to create their own hobbies.");
                }
            }
            return await base.Post(vm);
        }
    }
}