﻿using System;
using ChaiTea.Web.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Facilities.ResponseModel;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using System.Collections.Generic;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Personnel;
using ChaiTea.BusinessLogic.ViewModels.RequestModel;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/Route")]
    public class RouteController : BaseApiController
    {
        /// <summary>
        /// Get a route Break(s) and Lunch. 
        /// </summary>
        /// <returns>IList&lt;RouteElementListDetailResponseModel&gt;</returns>
        [HttpGet]
        [Route("GetRouteBreak/{routeId:int}")]
        [ResponseType(typeof(IList<RouteElementListDetailResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetRouteBreak(int routeId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new RouteService(UserCtx).GetRouteBreaks(routeId));
        }

        /// <summary>
        /// Retrieves a Route. 
        /// </summary>
        /// <returns>RouteResponseModel</returns>
        [HttpGet]
        [Route("GetRoute/{siteId:int}/{routeId:int}")]
        [ResponseType(typeof(RouteResponseModel))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetRoute(int siteId, int routeId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(new RouteService(UserCtx).GetRoute(siteId, routeId));
        }
        
        /// <summary>
        /// Retrieves Areas by a filter criteria.
        /// </summary>
        /// <returns>RouteAreaResponseModel</returns>
        [HttpPost]
        [Route("AreaSearch")]
        [ResponseType(typeof(IList<RouteAreaResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult AreaSearch(RouteAreaSearchRequestModel areaSearch)
        {
            return Ok(new RouteService(UserCtx).AreaSearch(areaSearch));
        }

        /// <summary>
        /// Search for a route. 
        /// </summary>
        /// <returns>SearchVM&lt;RouteResponseModel&gt;</returns>
        [HttpPost]
        [Route("RouteSearch")]
        [ResponseType(typeof(SearchVM<RouteSearchResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult RouteSearch(RouteSearchRequestModel searchModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(new RouteService(UserCtx).RouteSearch(searchModel));
        }

        /// <summary>
        /// Retrieves associates for a selected site. 
        /// </summary>
        /// <returns>IList&lt;IdNameModel&gt;</returns>
        [HttpGet]
        [Route("GetAssociatesBySite/{siteId}")]
        [ResponseType(typeof(IList<IdNameModel>))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetAssociatesBySite(int siteId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(new RouteService(UserCtx).GetAssociatesBySite(siteId));
        }
        
        /// <summary>
        /// Retrieves managers for a selected site. 
        /// </summary>
        /// <returns>IList&lt;IdNameModel&gt;</returns>
        [HttpGet]
        [Route("GetManagersBySite/{siteId}")]
        [ResponseType(typeof(IList<IdNameTitleResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetManagersBySite(int siteId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(new RouteService(UserCtx).GetManagersBySite(siteId));
        }
        /// <summary>
        /// Create a Route
        /// </summary>
        /// <returns>RouteCreateResponseModel</returns>
        [HttpPost]
        [Route("RouteCreate")]
        [ResponseType(typeof(RouteCreateResponseModel))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult RouteCreate(RouteCreateRequestModel route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new RouteService(UserCtx).RouteCreate(route));
        }
        /// <summary>
        /// Update a Route
        /// </summary>
        /// <returns>RouteUpdateRequestModel</returns>
        [HttpPut]
        [Route("RouteUpdate/{id:int}")]
        [ResponseType(typeof(RouteUpdateRequestModel))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult RouteUpdate(int id, [FromBody] RouteUpdateRequestModel route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new RouteService(UserCtx).RouteUpdate(route));
        }

        /// <summary>
        /// Update a Route Area Task as Incomplete
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("RouteAreaTaskIncomplete")]
        [ResponseType(typeof(RouteUpdateRequestModel))]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> RouteAreaTaskIncomplete(RouteAreaTaskIncompleteRequestModel routeAreaTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(await new RouteService(UserCtx).RouteAreaTaskIncomplete(routeAreaTask));
        }
        

        /// <summary>
        /// Change Status of a Route.
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Route("UpdateStatus")]
        [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
        public async Task<IHttpActionResult> UpdateStatus(UpdateStatusRequestModel usrm)
        {
            return Ok(await new RouteService(UserCtx).UpdateStatusAsync(usrm.Id,usrm.Status));
        }

        /// <summary>
        /// List all routes for current associate with details. 
        /// </summary>
        /// <returns>SearchVM&lt;RouteResponseModel&gt;</returns>
        [HttpGet]
        [Route("ForAssociate")]
        [ResponseType(typeof(SearchVM<RouteListDetailResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Users + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult RouteListForAssociate()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(new RouteService(UserCtx).RouteListWithDetail(UserCtx.SiteId.GetValueOrDefault(), UserCtx.UserId));
        }
        
        [HttpPost]
        [Route("CreateLog")]
        [ResponseType(typeof(RouteLogCreateResponseModel))]
        [AuthorizedRolesApi(Roles = Roles.Users + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult CreateLog(RouteLogCreateRequestModel route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new RouteService(UserCtx).CreateRouteLog(route));
        }
        
        [HttpPut]
        [Route("UpdateLog")]
        [ResponseType(typeof(bool))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult UpdateLog(RouteLogUpdateRequestModel route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new RouteService(UserCtx).UpdateRouteLog(route));
        }
        
        [HttpGet]
        [Route("GetLogs")]
        [ResponseType(typeof(List<RouteLogResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetLogs(DateTime startDate, DateTime endDate)
        {
            return Ok(new RouteService(UserCtx).GetRouteLogs(startDate, endDate));
        }
        
        [HttpGet]
        [Route("GetLog/{id:int}")]
        [ResponseType(typeof(List<RouteLogResponseModel>))]
        [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.CrazyProgrammers)]
        public IHttpActionResult GetLog(int id)
        {
            return Ok(new RouteService(UserCtx).GetRouteLog(id));
        }
    }
}