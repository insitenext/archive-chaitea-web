﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.BusinessLogic.ViewModels.Communications;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.Utils;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Site")]
    public class SiteController : BaseCrudApiController<SiteVm>
    {
        protected override Func<SiteVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<SiteVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<SiteVm> CreateService => new SiteService(UserCtx, true);

        [HttpGet]
        [ResponseType(typeof(IQueryable<ProgramVm>))]
        [Route("Programs/{siteId:int}")]
        [EnableQuery]
        public IQueryable<ProgramVm> GetPrograms(int siteId)
        {
            return new FloorProgramService(UserCtx).GetBySite(siteId);
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("Programs/{siteId:int}")]
        public async Task<IHttpActionResult> SetPrograms(int siteId, List<int> programIds)
        {
            List<ProgramVm> vms = new List<ProgramVm>();
            programIds.ForEach(i => vms.Add(new ProgramVm { Id = i }));

            await new FloorProgramService(UserCtx).AssociateBySiteAsync(siteId, vms);
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(HttpStatusCode.NoContent));
        }

        /// <summary>
        /// Get all sites by client id
        /// </summary>
        /// <remarks>
        /// NOTE: RETURNS ALL SITES UNDER CLIENT REGARDLESS OF USER PERMISSIONS!
        /// </remarks>
        /// <param name="clientId">Unique identifier for client.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<SiteVm>))]
        [Route("Client/{clientId:int}")]
        public IHttpActionResult GetSitesByClientId(int clientId)
        {
            return Ok(new SiteService(UserCtx).GetSitesByClientId(clientId));
        }

        /// <summary>
        /// Get all user sites by client id
        /// </summary>
        /// <remarks>
        /// NOTE: RETURNS ONLY SITES UNDER CLIENT THAT ARE ASSIGNED TO USER!
        /// </remarks>
        /// <param name="clientId">Unique identifier for client.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<SiteVm>))]
        [Route("UserSites/{clientId:int}")]
        public IHttpActionResult GetUserSitesByClientId(int clientId)
        {
            return Ok(new SiteService(UserCtx).GetClientUserSites(clientId).ToList());
        }

        [HttpGet]
        [ResponseType(typeof(SiteLookupDTO))]
        [Route("SiteLookup/{siteId:int}")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult GetSiteLookup(int siteId)
        {            
            return Ok(new SiteService(UserCtx).GetSiteLookupDTO(siteId));
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("SiteLookup")]
        [AuthorizedRolesApi(Roles = Roles.CrazyProgrammers)]
        public IHttpActionResult SetSiteLookup(SiteLookupDTO siteLookup)
        {
            var success = new SiteService(UserCtx).SaveSiteLookup(siteLookup);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpGet]
        [Route("AllSites")]
        [ResponseType(typeof(List<SiteService.SiteListItemDTO>))]
        public IHttpActionResult GetAllSites()
        {
            var roles = new List<string>()
                        {
                            Roles.CrazyProgrammers,
                            Roles.Managers
                        };

            using (var siteSvc = new SiteService(UserCtx))
            {
                return Ok(siteSvc.GetAllSites(roles));
            }
        }

        [HttpPost]
        [Route("Sites")]
        [ResponseType(typeof(SearchVM<SiteService.SiteListItemDTO>))]
        public IHttpActionResult GetSites(SiteListRequestModel searchParams)
        {
            // Ignore anything set in the Roles of the incoming params.
            // we are specifically restricting to the following list...

            searchParams.Roles = new List<string>()
                                 {
                                     Roles.CrazyProgrammers,
                                     Roles.Managers
                                 };

            using (var siteSvc = new SiteService(UserCtx))
            {
                return Ok(siteSvc.GetSites(searchParams));
            }
        }
        
        [HttpGet]
        [Route("{id:int}/Managers")]
        [ResponseType(typeof(IEnumerable<ManagerResponseModel>))]
        public IHttpActionResult GetSites(int id)
        {
            using (var siteSvc = new SiteService(UserCtx, true))
            {
                return Ok(siteSvc.GetSiteManagers(id));
            }
        }
    }
}