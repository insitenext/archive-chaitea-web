﻿using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.ViewModels.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AreaClassification")]
    public class AreaClassificationController : BaseApiController
    {
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(AreaClassificationVm))]
        public async Task<IHttpActionResult> Post(AreaClassificationVm vm)
        {
            return Ok(await new AreaClassificationService(UserCtx).CreateAsync(vm));
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(AreaClassificationVm))]
        public async Task<IHttpActionResult> Put(int id, AreaClassificationVm vm)
        {
            return Ok(await new AreaClassificationService(UserCtx).UpdateAsync(id, vm));
        }

        /// <summary>
        /// Retrieves Active/Inactive/All Area Type Count
        /// </summary>
        /// <returns>AreaClassificationSummary</returns>
        [HttpGet]
        [Route("r2/AreaTypeSummary")]
        [ResponseType(typeof(AreaClassificationSummary))]
        public IHttpActionResult GetAreaTypeSummary()
        {
            return Ok(new AreaClassificationService(UserCtx).GetAreaTypeSummary());
        }

        /// <summary>
        /// Retrieves Area Types by a search criteria 
        /// </summary>
        /// <param name="searchModel">Area Classification Search Request Model</param>
        /// <returns>SearchVM&lt;AreaTypeVm&gt;</returns>
        [HttpPost]
        [Route("r2/AreaTypes")]
        [ResponseType(typeof(SearchVM<AreaTypeVm>))]
        public IHttpActionResult AreaTypes(AreaClassificationRequestModel searchModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new AreaClassificationService(UserCtx).GetAreaTypes(searchModel));
        }
        /// <summary>
        /// AreaType Edit, retrieves associated Inspection Items 
        /// </summary>
        /// <param name="searchRequestModel"></param>
        /// <returns>SearchVM&lt;AreaTypeInspectionPointVm&gt;</returns>
        [HttpPost]
        [Route("r2/Edit")]
        [ResponseType(typeof(SearchVM<AreaTypeInspectionPointVm>))]
        public IHttpActionResult Edit(AreaClassificationInspectionItemRequestModel searchRequestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new AreaClassificationService(UserCtx).GetAreaTypeDetails(searchRequestModel));
        }
    }
}