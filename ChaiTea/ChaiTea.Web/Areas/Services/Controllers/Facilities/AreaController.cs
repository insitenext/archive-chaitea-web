﻿using ChaiTea.BusinessLogic.AppUtils.Helpers;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Facilities.ResponseModel;
using ChaiTea.BusinessLogic.ViewModels.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Area")]
    public class AreaController : BaseCrudApiController<AreaVm>
    {
        protected override Func<AreaVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<AreaVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<AreaVm> CreateService
        {
            get { return new AreaService(UserCtx); }
        }

        /// <summary>
        /// Updates an Area.
        /// </summary>
        /// <returns>AreaVm</returns>
        [HttpPut]
        [ResponseType(typeof(AreaVm))]
        public async override Task<IHttpActionResult> Put(int id, AreaVm vm)
        {
            return await base.Put(id, vm);
        }

        /// <summary>
        /// Retrieves Areas by a filter criteria.
        /// </summary>
        /// <returns><see cref="SearchVM&lt;AreaSearchResponseModel&gt;">SearchVM&lt;AreaSearchRequestModel&gt;</see></returns>
        [HttpPost]
        [Route("AreaSearch")]
        [ResponseType(typeof(SearchVM<AreaSearchResponseModel>))]
        public IHttpActionResult AreaSearch(AreaSearchRequestModel areaSearch)
        {
            return Ok(new AreaService(UserCtx).AreaSearch(areaSearch));
        }

        /// <summary>
        /// Change Status of an Area.
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Route("UpdateStatus")]
        public async Task<IHttpActionResult> UpdateStatus(UpdateStatusRequestModel usrm)
        {
            return Ok(await new AreaService(UserCtx).UpdateStatusAsync(usrm.Id, usrm.Status));
        }
    }
}