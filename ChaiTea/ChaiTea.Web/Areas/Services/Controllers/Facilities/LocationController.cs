﻿using ChaiTea.Web.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Facilities.ResponseModel;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using System.Collections.Generic;
using ChaiTea.BusinessLogic.Services.Facilities;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Location")]
    public class LocationController : BaseApiController
    {
        /// <summary>
        /// Retrieves Location Types.
        /// </summary>
        /// <returns>List&lt;LookupListOption&lt;int, string&gt;&gt;</returns>
        [HttpGet]
        [Route("GetLocationTypes")]
        [ResponseType(typeof(List<LookupListOption<int, string>>))]
        public IHttpActionResult GetLocationTypes()
        {
            return Ok(new LocationService(UserCtx).GetLocationTypes());
        }

        /// <summary>
        /// Get a edit row dialog data. 
        /// </summary>
        /// <returns>LocationArea</returns>
        [HttpGet]
        [Route("GetRow/{id:int}")]
        [ResponseType(typeof(LocationArea))]
        public IHttpActionResult GetRow(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new LocationService(UserCtx).GetLocationArea(id));
        }

        /// <summary>
        /// Get a edit row dialog data. 
        /// </summary>
        /// <returns>LocationArea</returns>
        [HttpGet]
        [Route("GetCellDetails/{locationAreaId:int}/{locationAreaTypeId:int}")]
        [ResponseType(typeof(LocationArea))]
        public IHttpActionResult GetCellDetails(int locationAreaId, int locationAreaTypeId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new LocationService(UserCtx).GetCellDetails(locationAreaId, locationAreaTypeId));
        }

        /// <summary>
        /// Create Location
        /// </summary>
        /// <returns>boolean</returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(LocationResponseModel))]
        public IHttpActionResult Post(LocationCreateRequestModel locationModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new { Result = new LocationService(UserCtx).CreateLocationAsync(locationModel) });
        }

        /// <summary>
        /// Retrieves Locations(Currently Buildings)
        /// </summary>
        /// <returns>LocationResponseModel</returns>
        [HttpPost]
        [Route("SearchLocation")]
        [ResponseType(typeof(LocationResponseModel))]
        public IHttpActionResult SearchLocation(LocationSearchRequestModel locations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new LocationService(UserCtx).GetLocations(locations));
        }

        /// <summary>
        /// Create a new Area/Floor within a selected Location/Building
        /// </summary>
        /// <returns>LocationResponseModel</returns>
        [HttpPost]
        [Route("AddRow")]
        [ResponseType(typeof(int))]
        public IHttpActionResult AddRow(LocationAreaAddRequestModel locationArea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new { RowId = new LocationService(UserCtx).CreateLocationArea(locationArea) });
        }

        /// <summary>
        /// Save a selected Row data
        /// </summary>
        /// <returns>LocationAreaSaveRequestModel</returns>
        [HttpPost]
        [Route("SaveRow")]
        [ResponseType(typeof(LocationAreaSaveRequestModel))]
        public IHttpActionResult SaveRow(LocationAreaSaveRequestModel locationArea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new LocationService(UserCtx).UpdateLocationArea(locationArea));
        }

        /// <summary>
        /// Save a selected Row data
        /// </summary>
        /// <returns>LocationAreaSaveRequestModel</returns>
        [HttpPost]
        [Route("SaveCellDetails")]
        [ResponseType(typeof(LocationAreaSaveRequestModel))]
        public IHttpActionResult SaveCellDetails(LocationDetailsSaveRequestModel locationCellDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(new LocationService(UserCtx).SaveLocationDetail(locationCellDetail));
        }

        /// <summary>
        /// Update Location
        /// </summary>
        /// <returns>boolean</returns>
        [HttpPut]
        [Route("")]
        [ResponseType(typeof(LocationResponseModel))]
        public async Task<IHttpActionResult> Put(LocationUpdateRequestModel locationModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(await new LocationService(UserCtx).UpdateLocationAsync(locationModel));
        }
    }
}