﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Floor")]
    public class FloorController : BaseCrudApiController<FloorVm>
    {
        protected override Func<FloorVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<FloorVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<FloorVm> CreateService
        {
            get { return new FloorService(UserCtx, true, true); }
        }


        /// <summary>
        /// Retrieve areas for a floor.
        /// </summary>
        /// <param name="floorId">Unique identifier of a floor.</param>
        /// <returns>Collection of AreaVms</returns>
        [HttpGet]
        [EnableQuery]
        [Route("Areas/{floorId:int}")]
        public IQueryable<AreaVm> GetAreas(int floorId)
        {
            return new FloorAreaService(floorId, UserCtx).Get();
        }
                
        /// <summary>
        /// Assoicates a set of areas to a floor.
        /// </summary>
        /// <param name="floorId">Unique identifier of a floor.</param>
        /// <param name="areaIds">Collection of identifiers of areas.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("Areas/{floorId:int}")]
        public async Task<IHttpActionResult> SetAreas(int floorId, List<int> areaIds)
        {
            List<AreaVm> vms = new List<AreaVm>();
            areaIds.ForEach(i => vms.Add(new AreaVm { Id = i }));
            await new FloorAreaService(floorId, UserCtx).AssociateAsync(vms);
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(HttpStatusCode.NoContent));
        }

    }
}