﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Net.Http;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Organizations;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Building")]
    public class BuildingController : BaseCrudApiController<BuildingVm>
    {
        protected override Func<BuildingVm, int> GetIdentifier
        {
            get { return vm => vm.Id; }
        }

        protected override Action<BuildingVm, int> SetIdentifier
        {
            get { return (vm, id) => vm.Id = id; }
        }

        protected override ICrudService<BuildingVm> CreateService => new BuildingService(UserCtx, true, true);

        /// <summary>
        /// Retrieves programs for a given building.
        /// </summary>
        /// <param name="buildingId">Unique identifier for building.</param>
        /// <returns>Collection of ProgramVms</returns>
        [HttpGet]
        [EnableQuery]
        [Route("Programs/{buildingId:int}")]
        public IQueryable<ProgramVm> GetPrograms(int buildingId)
        {
            return new FloorProgramService(UserCtx).GetByBuilding(buildingId);
        }

        /// <summary>
        /// Associates programs to a building.
        /// </summary>
        /// <param name="buildingId">Unique identifier for building.</param>
        /// <param name="programIds">Collection of program identifiers.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("Programs/{buildingId:int}")]
        public async Task<IHttpActionResult> SetPrograms(int buildingId, List<int> programIds)
        {
            List<ProgramVm> vms = new List<ProgramVm>();

            programIds.ForEach(i => vms.Add(new ProgramVm
                                            {
                                                Id = i
                                            }));

            await new FloorProgramService(UserCtx).AssociateByBuildingAsync(buildingId, vms);

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
        }
    }
}