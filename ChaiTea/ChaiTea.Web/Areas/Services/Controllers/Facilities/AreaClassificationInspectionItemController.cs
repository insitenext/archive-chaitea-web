﻿using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.BusinessLogic.Services.Auditing;
using ChaiTea.Web.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AreaClassificationInspectionItem")]
    public class AreaClassificationInspectionItemController : BaseApiController
    {
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(AreaClassificationInspectionItemVm))]
        public async Task<IHttpActionResult> Post(AreaClassificationInspectionItemVm vm)
        {
            return Ok(await new AreaClassificationInspectionItemService(vm.AreaClassificationId, UserCtx).CreateAsync(vm));            
        }

        /// <summary>
        /// This is a AreaType Edit, which will retrieve associated Inspection Items 
        /// </summary>
        /// <param name="vm"></param>
        /// <returns>Task&lt;IHttpActionResult&gt;</returns>
        [HttpPost]
        [Route("r2/AddInspectionPoints")]
        public async Task<IHttpActionResult> AddInspections(AreaClassificationInspectionBulkInsertRequestModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await new AreaClassificationInspectionItemService(vm.AreaClassificationId, UserCtx).CreateBulkAsync(vm));
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(AreaClassificationInspectionItemVm))]
        public async Task<IHttpActionResult> Put(int id, AreaClassificationInspectionItemVm vm)
        {
            return Ok(await new AreaClassificationInspectionItemService(vm.AreaClassificationId, UserCtx).UpdateAsync(id, vm));           
        }
    }
}