﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities;
using ChaiTea.BusinessLogic.ViewModels.Facilities.RequestModel;
using ChaiTea.BusinessLogic.ViewModels.Utils;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.Facilities
{
    [AuthorizedRolesApi(Roles = Roles.Admins + "," + Roles.CrazyProgrammers)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/AreaClassificationTask")]
    public class AreaClassificationTaskController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(IQueryable<AreaClassificationTaskVm>))]
        [Route("")]
        [EnableQuery]
        public IQueryable<AreaClassificationTaskVm> GetAllAreaClassificationTasks()
        {
            using (var actService = new AreaClassificationTaskService(UserCtx))
            {
                return actService.GetAllAreaClassificationTasks();
            }
        }

        [HttpGet]
        [ResponseType(typeof(AreaClassificationTaskService.AreaClassificationTaskDTO))]
        [Route("{id:int}")]
        public IHttpActionResult GetAreaClassificationTaskById(int id)
        {
            using (var actService = new AreaClassificationTaskService(UserCtx))
            {
                return Ok(actService.GetAreaClassificationTaskById(id));
            }
        }

        [HttpPost]
        [ResponseType(typeof(SearchVM<AreaClassificationTaskService.AreaClassificationTaskDTO>))]
        [Route("Paged")]
        public IHttpActionResult GetAreaClassificationTasksPaged(AreaClassificationTaskRequestModel searchModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Search model passed in?

            if (searchModel == null)
            {
                // No search model passed - default to getting the first page of 25 items.

                searchModel = new AreaClassificationTaskRequestModel()
                              {
                                  PageNumber = 1,
                                  PageSize = 25
                              };
            }

            using (var actService = new AreaClassificationTaskService(UserCtx))
            {
                return Ok(actService.GetAreaClassificationTasks(searchModel));
            }
        }

        [HttpGet]
        [ResponseType(typeof(AreaClassificationTaskVm))]
        [Route("{AreaClassificationTaskId:int}")]
        [EnableQuery]
        public AreaClassificationTaskVm GetTask(int areaClassificationTaskId)
        {
            return new AreaClassificationTaskService(UserCtx).Get(areaClassificationTaskId);
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(AreaClassificationTaskService.AreaClassificationTaskDTO))]
        public async Task<IHttpActionResult> Post(AreaClassificationTaskVm vm)
        {
            using (var taskService = new AreaClassificationTaskService(UserCtx))
            {
                return Ok(await taskService.CreateAsync(vm));
            }
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(AreaClassificationTaskService.AreaClassificationTaskDTO))]
        public async Task<IHttpActionResult> Put(int id, AreaClassificationTaskVm vm)
        {
            using (var taskService = new AreaClassificationTaskService(UserCtx))
            {
                return Ok(await taskService.UpdateAsync(id, vm));
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(AreaClassificationTaskVm))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var taskService = new AreaClassificationTaskService(UserCtx))
            {
                return Ok(await taskService.DeleteAsync(id));
            }
        }

        /// <summary>
        /// Get all Tasks by AreaId
        /// </summary>
        /// <returns>IdNameModel</returns>
        [HttpGet]
        [ResponseType(typeof(IdNameModel))]
        [Route("GetAreaTasks/{areaId:int}")]
        public IHttpActionResult GetAreaTasks(int areaId)
        {
            return Ok(new AreaClassificationTaskService(UserCtx).GetAreaTasks(areaId));
        }
    }
}