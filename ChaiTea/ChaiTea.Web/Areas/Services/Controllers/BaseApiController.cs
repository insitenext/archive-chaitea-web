﻿using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Utils.Localization;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Results;

namespace ChaiTea.Web.Areas.Services.Controllers
{
    [AuthorizedRolesApi]
    public abstract class BaseApiController : ApiController
    {
        private IUserContext _usrContext;

        protected IUserContext UserCtx
        {
            get
            {
                if (_usrContext == null)
                {
                    IEnumerable<Claim> contextItemsFromClaims = null;
                    var identity = User?.Identity;

                    if (identity != null &&
                        identity is ClaimsIdentity)
                    {
                        contextItemsFromClaims = (identity as ClaimsIdentity).Claims;
                    }

                    var contextItemsFromHeader = Request.Headers
                                                        .Select(t => new KeyValuePair<String, String>(t.Key, t.Value.First()))
                                                        .ToList();

                    _usrContext = UserContextService.BuildFromRequest(true, UserContextService.CurrentUserId, contextItemsFromHeader, contextItemsFromClaims);
                }

                return _usrContext;
            }
        }


        protected BaseApiController()
        {
            if (log4net.LogicalThreadContext.Properties["requestGuid"] == null)
            {
                log4net.LogicalThreadContext.Properties["requestGuid"] = Guid.NewGuid().ToString();
            }
        }

        protected new OkNegotiatedContentResult<T> Ok<T>(T obj)
        {
            if (typeof(T).IsClass)
            {
                var ctx = User.Identity.IsAuthenticated
                        ? UserCtx
                        : UserContext.AnonymousUserContext();

                return base.Ok(new LocalizationService(ctx).LocalizeObj(obj));
            }

            return base.Ok(obj);
        }
    }
}
