﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicators
{
    [AuthorizedRolesApi(Roles = Roles.Admins)]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/KpiSetting")]
    public class KpiSettingController : BaseCrudApiController<KpiSettingVm>
    {
        protected override Func<KpiSettingVm, int> GetIdentifier
        {
            get { return e => e.SiteJobProgramKpiId; }
        }

        protected override Action<KpiSettingVm, int> SetIdentifier
        {
            get { return (e, i) => e.SiteJobProgramKpiId = i; }
        }

        protected override ICrudService<KpiSettingVm> CreateService => new KpiSettingService(UserCtx);

        /// <inheritdoc />
        /// <summary>
        /// Updates a KPI Setting as described by the KpiSettingVm provided
        /// </summary>
        /// <param name="id">Unique identifier of the Kpi Setting to be updated.</param>
        /// <param name="vm">KpiSettingVm</param>
        /// <returns>KpiSettingVm</returns>
        [Route("{id:int}")]
#pragma warning disable 1998
        public override async Task<IHttpActionResult> Put(int id, KpiSettingVm vm)
#pragma warning restore 1998
        {
            throw new NotImplementedException("Saving/Updating Kpi Site Settings is disabled until SB is back up and running");
        }

        /// <summary>
        /// Gets the KPI Metrics for a site based on the JobId
        /// </summary>
        /// <param name="jobId">Job Id for which the metrics need to be retrieved.</param>
        /// <returns>KpiSettingVm</returns>
        [HttpGet]
        [ResponseType(typeof(KpiSettingVm))]
        [Route("KPIMetrics/{jobId:int}")]
        public IHttpActionResult GetKPIMetrics(int jobId)
        {
            if (!UserCtx.SiteId.HasValue)
            {
                return ResponseMessage(this.Request.CreateResponse(HttpStatusCode.BadRequest, "User Context Site Id Not Found!"));
            }
            
            var result = base.Get()
                             .Where(k => k.JobId == jobId && k.SiteId == UserCtx.SiteId)
                             .OrderBy(k => k.Name);
            
            return Ok(result);
        }
    }
}