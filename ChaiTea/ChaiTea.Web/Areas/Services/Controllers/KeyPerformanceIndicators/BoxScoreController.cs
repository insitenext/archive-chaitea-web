﻿using ChaiTea.BusinessLogic.Services.Assessments.Complaints;
using ChaiTea.BusinessLogic.Services.Assessments.Compliments;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.Employment;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicators
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/BoxScore")]
    public class BoxScoreController : BaseApiController
    {
        [HttpGet]
        [ResponseType(typeof(BoxScoreAllCountsVm))]
        [Route("All")]
        public IHttpActionResult GetSummaryDataPoints(DateTime startDate, DateTime endDate)
        {
            return Ok(new PersonnelBoxScoreService(UserCtx).GetSummaryDataPoints(startDate, endDate));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<TeamStatsBoxScoreVm>))]
        [Route("")]
        public IHttpActionResult Get(int id, DateTime startDate, DateTime endDate)
        {
            return Ok(new PersonnelBoxScoreService(UserCtx).GetEmployeeDataPoints(id, startDate, endDate));
        }

        [HttpGet]
        [Route("EmployeesComplaintsCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesComplaintsCount(DateTime startDate, DateTime endDate)
        {
            return Ok(new ComplaintsService(UserCtx).GetComplaintsCountByEmployees(startDate, endDate));
        }

        [HttpGet]
        [Route("EmployeesComplimentsCount")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesComplimentsCount(DateTime startDate, DateTime endDate)
        {
            return Ok(new ComplimentsService(UserCtx).GetComplimentsCountByEmployees(startDate, endDate));
        }

        [HttpGet]
        [Route("EmployeesAuditsAvgMetric")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesAuditsAvgMetric(DateTime startDate, DateTime endDate)
        {
            return Ok(new PersonnelBoxScoreService(UserCtx).GetEmployeesAuditsAvgMetric(startDate, endDate));
        }

        [HttpGet]
        [Route("EmployeesTrainingComplianceMetric")]
        [ResponseType(typeof(List<EmployeeMetricVm>))]
        [EnableQuery] // TODO: Yes, we are using OData here despite the return type being List in order for front-end to sort/skip/top
        public IHttpActionResult GetEmployeesTrainingComplianceMetric(DateTime startDate, DateTime endDate)
        {
            return Ok(new PersonnelBoxScoreService(UserCtx).GetEmployeesTrainingComplianceMetric(startDate, endDate));
        }
    }
}