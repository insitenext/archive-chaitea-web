﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicator
{
    [RoutePrefix(WebApiConfig.UrlPrefix + "/Kpi")]
    public class KpiController : BaseApiController
    {
        protected ICrudService<KpiVm> Service
        {
            get { return new KpiService(UserCtx); }
        }

        [HttpGet]
        [EnableQuery]
        [Route("")]
        public IQueryable<KpiVm> Get()
        {
            return Service.Get();
        }
    }
}
