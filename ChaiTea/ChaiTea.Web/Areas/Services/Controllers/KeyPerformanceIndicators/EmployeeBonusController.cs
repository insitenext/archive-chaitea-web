﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicator
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeBonus")]
    public class EmployeeBonusController : BaseApiController
    {

        [HttpGet]
        [ResponseType(typeof(EmployeeBonusVm))]
        [Route("Employee/{year:int}/{month:int}")]
        public IHttpActionResult GetEmployeeBonusByEmployeeId(int year, int month)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            var service = new EmployeeKpiMasterService(UserCtx);
            var result = service.Get(year, month)
                            .Project().To<EmployeeBonusVm>()
                            .FirstOrDefault(e => e.EmployeeId == employeeId);

            return Ok(result);
        }

        [HttpGet]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(IQueryable<EmployeeBonusVm>))]
        [Route("{year:int}/{month:int}")]
        [EnableQuery]
        public IQueryable<EmployeeBonusVm> GetEmployeeBonus(int year, int month)
        {
            return new EmployeeKpiMasterService(UserCtx).Get(year, month).Project().To<EmployeeBonusVm>();
        }


        [HttpGet]
        [ResponseType(typeof(EmployeeBonusVm))]
        [Route("{employeeId:int}/{year:int}/{month:int}")]
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult GetEmployeeBonus(int employeeId, int year, int month)
        {
            var service = new EmployeeKpiMasterService(UserCtx);
            var result = service.Get(year, month).Project().To<EmployeeBonusVm>().FirstOrDefault(e => e.EmployeeId == employeeId);
            return Ok(result);
        }

        [HttpPut]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(EmployeeBonusVm))]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> UpdateEmployeeBonusAsync(int id, EmployeeBonusVm vm)
        {
            if (vm == null)
            {
                throw new ArgumentNullException(nameof(vm), "Employee Bonus can not be null");
            }

            var service = new EmployeeKpiMasterService(UserCtx);
            var result = Mapper.Map<EmployeeBonusVm>(await service.UpdateEmployeeBonusAsync(vm));
            return Ok(result);
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeBonuses")]
    public class EmployeeBonusesController : BaseApiController
    {
        [HttpPut]
        [ResponseType(typeof(void))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [Route("")]
        public async Task<IHttpActionResult> UpdateListAsync(List<EmployeeBonusVm> employeeBonuses)
        {
            if (employeeBonuses == null || employeeBonuses.Count <= 0)
            {
                throw new ArgumentNullException(nameof(employeeBonuses), "List of EmployeeBonus can not be null or empty.");
            }

            var employeeKpiMasterList = Mapper.Map<List<EmployeeKpiMasterVm>>(employeeBonuses);
            // todo: dont return a list if we dont actually use it
            var result = (await new EmployeeKpiMasterService(UserCtx).UpdateEmployeeBonusesAsync(employeeKpiMasterList));
            return Ok();
        }
    }
}