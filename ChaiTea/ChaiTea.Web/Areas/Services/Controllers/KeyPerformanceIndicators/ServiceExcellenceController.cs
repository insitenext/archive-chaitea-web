﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicator
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ServiceExcellence")]
    public class ServiceExcellenceController : BaseApiController
    {
        protected ServiceExcellenceService Service => new ServiceExcellenceService(UserCtx);

        [HttpGet]
        [Route("{year:int}/{month:int}")]
        [EnableQuery]
        public virtual IEnumerable<ServiceExcellenceEmployeeVm> Get(int year, int month)
        {
            return Service.Get(year, month);
        }

        [HttpGet]
        [ResponseType(typeof(ExtendedServiceExcellenceEmployeeVm))]
        [Route("{year:int}/{month:int}/{id:int}")]
        public async Task<IHttpActionResult> Get(int year, int month, int id)
        {
            return Ok(await Service.GetExtended(year, month, id));
        }

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(ExtendedServiceExcellenceEmployeeVm))]
        public async Task<IHttpActionResult> Put(int id, ExtendedServiceExcellenceEmployeeVm vm)
        {
            return Ok(await Service.UpdateExtendedAsync(vm));
        }

        [HttpDelete]
        [ResponseType(typeof(EmployeeKpiEditableCheckVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [Route("DeleteComplaint/{id:int}")]
        public IHttpActionResult DeleteComplaint(int id)
        {
            var result = Service.DeleteComplaint(id);
            var employeeKpiEditable = new EmployeeKpiEditableCheckVm()
                                      {
                                          IsClosed = !result
                                      };

            return Ok(employeeKpiEditable);
        }

        [HttpGet]
        [ResponseType(typeof(ExtendedServiceExcellenceEmployeeVm))]
        [Route("EmployeeInfo/{year:int}/{month:int}/{id:int}")]
        public IHttpActionResult GetEmployeeInfo(int year, int month, int id)
        {
            var result = Service.Get(year, month, id);

            return Ok(result);
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("ExemptKpi/{isKpiExempted:bool}/{id:int}")]
        public  IHttpActionResult UpdateExemptKpi(int id, bool isKpiExempted)
        {
            var success = Service.UpdateExemptKpi(id, isKpiExempted);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new System.Net.Http.HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("ExemptEmployee/{id:int}")]
        public  IHttpActionResult UpdateExemptEmployee(int id)
        {
            var success = Service.UpdateExemptEmployee(id);

            if (success)
            {
                return Ok();
            }

            return ResponseMessage(new System.Net.Http.HttpResponseMessage(HttpStatusCode.NotModified));
        }

        [HttpGet]
        [ResponseType(typeof(ScorecardStatusVm))]
        [Route("ScorecardStatus/{month:int}/{year:int}")]
        public IHttpActionResult GetScorecardStatus(int month, int year)
        {
            return Ok(Service.GetScorecardStatuses(month, year));
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("ScorecardStatus")]
        public  IHttpActionResult PostScorecardStatus(ScorecardStatusVm vm)
        {
             var success = Service.CreateScorecardStatus(vm);
            
             if (success)
             {
                 return Ok();
             }
            
             return ResponseMessage(new System.Net.Http.HttpResponseMessage(HttpStatusCode.InternalServerError));
        }
     }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/ServiceExcellenceForEmployee")]
    public class ServiceExcellenceForEmployeeController : BaseApiController
    {
        protected ServiceExcellenceService Service => Service;

        [HttpGet]
        [ResponseType(typeof(ExtendedServiceExcellenceEmployeeVm))]
        [Route("{year:int}/{month:int}")]
        public  IHttpActionResult Get(int year, int month)
        {
            var employeeId = new UserContextService(UserCtx).GetOrgUserId();
            return Ok(Service.GetExtended(year, month, employeeId));
        }
    }
}