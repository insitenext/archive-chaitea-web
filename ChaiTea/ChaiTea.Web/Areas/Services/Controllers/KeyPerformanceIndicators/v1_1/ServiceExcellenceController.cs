using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators.v1_1;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicators.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/ServiceExcellence")]
    public class ServiceExcellence_v11Controller : BaseApiController
    {
        /// <summary>
        /// Gets all Service Excellence data for a month.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ServiceExcellenceEmployeeVm>))]
        [Route("{year:int}/{month:int}")]
        public async Task<IHttpActionResult> Get(int year, int month)
        {
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetAsync(year, month, null));
            }
        }
        
        /// <summary>
        /// Gets all Service Excellence data for a month by User Context.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ServiceExcellenceEmployeeVm>))]
        [Route("ByContext/{year:int}/{month:int}")]
        public async Task<IHttpActionResult> GetByContext(int year, int month)
        {
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetAsync(year, month, null, true));
            }
        }
        
        /// <summary>
        /// Gets all Service Excellence data for a month based on tags.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ServiceExcellenceEmployeeVm>))]
        [Route("ByTags/{year:int}/{month:int}")]
        public async Task<IHttpActionResult> Get([FromBody] string[] tags, int year, int month)
        {
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetAsync(year, month, tags));
            }
        }
        
        /// <summary>
        /// Get Service Excellence data for a specific employee and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="orgUserId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(ServiceExcellenceEmployeeVm))]
        [Route("EmployeeInfo/{year:int}/{month:int}/{orgUserId:int}")]
        public async Task<IHttpActionResult> GetByEmployee(int year, int month, int orgUserId)
        {
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetAsync(year, month, orgUserId));
            }
        }
        
        /// <summary>
        /// Gets the status of a specific site's monthly scorecard.
        /// </summary>
        /// <param name="site"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(ScorecardStatusVm))]
        [Route("ScorecardStatus/{site:int}/{month:int}/{year:int}")]
        public async Task<IHttpActionResult> GetScorecardStatus(int site, int month, int year)
        {
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetScorecardStatuses(site, month, year));
            }
        }
        
        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching</param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="take">Number of rows to take</param>
        /// <param name="skip">Number of pages to skip</param>
        [HttpPost]
        [ResponseType(typeof(IEnumerable<ServiceExcellenceSummary>))]
        [Route("SiteSummariesByTags")]
        public async Task<IHttpActionResult> GetSiteSummariesByTags([FromBody] string[] tags, int year, int month, int? take = null, int? skip = null)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
         
            if (!DateTime.TryParse($"{month}/1/{year}", out var d1))
                throw new ArgumentException("Invalid date criteria");
            
            var pagingFilter = PagingFilter.TryValidate(skip, take);
            
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetSummariesByTagsAsync(pagingFilter, year, month, tags));
            }
        }
        
        /// <summary>
        /// Gets a summary of site summaries from tags for date range
        /// </summary>
        /// <param name="tags">Tags to use for searching</param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        [HttpPost]
        [ResponseType(typeof(ServiceExcellenceSummary))]
        [Route("AllSitesSummaryByTags")]
        public async Task<IHttpActionResult> GetAllSiteSummaryByTags([FromBody] List<string> tags, int year, int month)
        {
            if (tags?.Any() != true)
                throw new ArgumentException("Tags are required to use this endpoint.");
            
            if (!DateTime.TryParse($"{month}/1/{year}", out var d1))
                throw new ArgumentException("Invalid date criteria");
            
            using (var svc = new ServiceExcellenceService(UserCtx))
            {
                return Ok(await svc.GetAllSiteSummaryByTagsAsync(year, month, tags));
            }
        }
    }
}