using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators.v1_1;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicators.v1_1
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix_v1_1 + "/EmployeeKpiMaster")]
    public class EmployeeKpiMaster_v11Controller : BaseApiController
    {
        /// <summary>
        /// Get Employee KPIs by year/month based on UserContext
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeKpiMasterVm>))]
        [Route("{year:int}/{month:int}")]
        public async Task<IHttpActionResult> Get(int year, int month)
        {
            if (!DateTime.TryParse($"{month}/1/{year}", out var d1))
                throw new ArgumentException("Invalid date criteria");

            using (var svc = new EmployeeKpiMasterService(UserCtx))
                return Ok(await svc.GetByMonthAsync(year, month, orgUserId: null));
        }
        
        /// <summary>
        /// Get Employee KPIs by year/month and employee ID based on UserContext
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="employeeId"></param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeKpiMasterVm>))]
        [Route("{year:int}/{month:int}/{employeeId:int}")]
        public async Task<IHttpActionResult> Get(int year, int month, int employeeId)
        {
            if (!DateTime.TryParse($"{month}/1/{year}", out var d1))
                throw new ArgumentException("Invalid date criteria");

            using (var svc = new EmployeeKpiMasterService(UserCtx))
                return Ok(await svc.GetByMonthAsync(year, month, orgUserId: employeeId));
        }
        
        /// <summary>
        /// Get Employee KPIs employee ID based on UserContext
        /// </summary>
        /// <param name="employeeId"></param>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<EmployeeKpiMasterVm>))]
        [Route("{employeeId:int}")]
        public async Task<IHttpActionResult> Get(int employeeId)
        {
            using (var svc = new EmployeeKpiMasterService(UserCtx))
                return Ok(await svc.GetByMonthAsync(null, null, orgUserId: employeeId));
        }
    }
}