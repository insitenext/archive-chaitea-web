﻿using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;
using ChaiTea.Web.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicator
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/SiteBonus")]
    public class SiteBonusController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IQueryable<SiteBonusVm>))]
        [EnableQuery]
        public IQueryable<SiteBonusVm> Get()
        {
            return new SiteBonusService(UserCtx).Get();
        }

        [HttpGet]
        [Route("{year:int}/{month:int}")]
        [ResponseType(typeof(IQueryable<SiteBonusVm>))]
        [EnableQuery]
        public IQueryable<SiteBonusVm> Get(int year, int month)
        {
            return new SiteBonusService(UserCtx).Get(year, month);
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(SiteBonusVm))]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public async Task<IHttpActionResult> Post(SiteBonusVm vm)
        {
            return Ok(await new SiteBonusService(UserCtx).CreateAsync(vm));
        }


        [HttpPut]
        [Route("{id:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(SiteBonusVm))]
        public async Task<IHttpActionResult> Put(int id, SiteBonusVm vm)
        {
            return Ok(await new SiteBonusService(UserCtx).UpdateAsync(vm));
        }
    }
}