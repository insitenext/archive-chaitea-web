﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;

using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.KeyPerformanceIndicators;
using ChaiTea.BusinessLogic.ViewModels.KeyPerformanceIndicators;

using log4net;

namespace ChaiTea.Web.Areas.Services.Controllers.KeyPerformanceIndicator
{
    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/EmployeeKpiMaster")]
    public class EmployeeKpiMasterController : BaseApiController
    {
        /// <summary>
        /// Retrieves Employee Bonus for Employee by year and month
        /// </summary>
        /// <returns>EmployeeBonusVm</returns>
        [HttpGet]
        [Route("")]
        [EnableQuery]
        public IQueryable<EmployeeKpiMasterVm> GetEmployeeKpiMaster()
        {
            return new EmployeeKpiMasterService(UserCtx).Get();
        }

        [HttpGet]
        [ResponseType(typeof(bool))]
        [Route("CheckIfKpiExemptedByEmployeeId/{employeeId:int}/{kpiId:int}")]
        public IHttpActionResult CheckIfKpiExemptedByEmployeeId(int employeeId, int kpiId)
        {
            using (var kpiSvc = new EmployeeKpiMasterService(UserCtx))
            {
                return Ok(new
                          {
                              IsExempt = kpiSvc.CheckIfKpiExemptedByEmployeeId(employeeId, kpiId)
                          });
            }
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/KpiComment")]
    public class KpiCommentController : BaseApiController
    {
        public static readonly ILog Logger = LogManager.GetLogger(nameof(KpiCommentController));

        /// <summary>
        /// Retrieves Kpi Comment by employeeid, year and month
        /// </summary>
        /// <param name="employeeId">EmployeeId</param>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <returns>KpiCommentVm</returns>
        [HttpGet]
        [ResponseType(typeof(KpiCommentVm))]
        [Route("{year:int}/{month:int}/{employeeId:int}")]
        // [AuthorizedRolesApi(Roles = Roles.Managers + "," + Roles.SepViewers)]
        // Authorization commented out because this was previously using AuthorizedRoles (the MVC version)
        // the MVC version doesn't work on WebApiControllers. it was a bug.
        // However, many of our front-end pages depend on being able to access this endpoint regardless
        // of the stated AuthorizedRoles.Roles and so our program is dependant on the bug!
        // instead of reverting, Documenting here and we'll readdress as feature
        public IHttpActionResult GetKpiComment(int year, int month, int employeeId)
        {
            using (var service = new EmployeeKpiMasterService(UserCtx))
            {
                var employeeKpiMaster = service.GetByEmployeeId(year, month, employeeId);

                if (employeeKpiMaster == null)
                {
                    Logger.Warn($"No EmployeeKpiMaster instance found for Year:{year}, Month:{month} and EmployeeId:{employeeId}.");

                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
                }

                var kpiComment = new KpiCommentVm()
                                 {
                                     EmployeeKpiMasterId = employeeKpiMaster.EmployeeKpiMasterId,
                                     Year = employeeKpiMaster.Year,
                                     Month = employeeKpiMaster.Month,
                                     EmployeeId = employeeKpiMaster.EmployeeId,
                                     Comment = employeeKpiMaster.Comment,
                                     IsPass = employeeKpiMaster.IsPass,
                                     SourceLanguageId = employeeKpiMaster.SourceLanguageId
                                 };

                return Ok(kpiComment);
            }
        }

        /// <summary>
        /// Retrieves Kpi Comment by year and month
        /// </summary>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <returns>KpiCommentVm</returns>
        [HttpGet]
        [AuthorizedRolesApi]
        [ResponseType(typeof(IQueryable<KpiCommentVm>))]
        [Route("{year:int}/{month:int}")]
        public IHttpActionResult GetKpiComment(int year, int month)
        {
            int employeeId;

            using (var usrContextSvc = new UserContextService(UserCtx))
            {
                employeeId = usrContextSvc.GetOrgUserId();
            }

            var result = new EmployeeKpiMasterService(UserCtx).Get(year, month)
                                                              .Where(k => k.EmployeeId == employeeId)
                                                              .Select(k => new KpiCommentVm()
                                                                           {
                                                                               EmployeeKpiMasterId = k.EmployeeKpiMasterId,
                                                                               Year = k.Year,
                                                                               Month = k.Month,
                                                                               EmployeeId = k.EmployeeId,
                                                                               Comment = k.Comment,
                                                                               IsPass = k.IsPass,
                                                                               SourceLanguageId = k.SourceLanguageId
                                                                           });

            if (!result.Any())
            {
                // No KpiComments for this user.

                Logger.Warn($"No EmployeeKpiMaster instance found for Year:{year}, Month:{month} and EmployeeId:{employeeId}.");
            }

            return Ok(result);
        }

        /// <summary>
        /// Updates a Kpi Comment with data provided by the given KpiCommentVm
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="vm">KpiCommentVm</param>
        /// <returns>KpiCommentVm</returns>
        [HttpPut]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(KpiCommentVm))]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> UpdateKpiCommentAsync(int id, KpiCommentVm vm)
        {
            if (id == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id), "Must pass non-null, valid Id value.");
            }

            if (vm == null)
            {
                throw new ArgumentNullException(nameof(vm), "Must pass KpiCommentVm record to update.");
            }

            using (var service = new EmployeeKpiMasterService(UserCtx))
            {
                if (vm.EmployeeKpiMasterId == 0 ||
                    vm.EmployeeKpiMasterId != id)
                {
                    vm.EmployeeKpiMasterId = id;
                }

                var employeeKpiMaster = await service.UpdateKpiCommentAsync(vm);

                if (employeeKpiMaster != null)
                {
                    return Ok(new KpiCommentVm()
                              {
                                  EmployeeKpiMasterId = employeeKpiMaster.EmployeeKpiMasterId,
                                  Year = employeeKpiMaster.Year,
                                  Month = employeeKpiMaster.Month,
                                  EmployeeId = employeeKpiMaster.EmployeeId,
                                  Comment = employeeKpiMaster.Comment,
                                  IsPass = employeeKpiMaster.IsPass,
                                  SourceLanguageId = employeeKpiMaster.SourceLanguageId
                              });
                }

                // No KpiComments for this user.

                Logger.Warn($"No EmployeeKpiMaster instance found to update with comment (Id:{id}): {vm}.");

                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
            }
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(WebApiConfig.UrlPrefix + "/KpiUpdateCheck")]
    public class KpiUpdateCheckController : BaseApiController
    {
        /// <summary>
        /// Updates a Employee Kpi Check for an employeeid, month and year
        /// </summary>
        /// <param name="employeeId">employeeid</param>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <returns>EmployeeKpiEditableCheckVm</returns>
        [HttpGet]
        [Route("{employeeId:int}/{year:int}/{month:int}")]
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        [ResponseType(typeof(IQueryable<EmployeeKpiEditableCheckVm>))]
        public IHttpActionResult UpdateEmployeeKpisCheck(int employeeId, int year, int month)
        {
            using (var service = new EmployeeKpiMasterService(UserCtx))
            {
                var result = service.Get(year, month)
                                    .Where(k => k.EmployeeId == employeeId)
                                    .Select(k => new EmployeeKpiEditableCheckVm
                                                 {
                                                     IsClosed = k.IsClosed
                                                 });

                return Ok(result);
            }
        }
    }
}