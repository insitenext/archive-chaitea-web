﻿namespace ChaiTea.Web.Areas.Services.Models
{
    public class SecurityVm
    {
        public string AccessKey { get; set; }
        public string AccessSecret { get; set; }
        public string AccessToken { get; set; }
        public string Signature { get; set; }
        public EndpointVm Endpoint { get; set; }
        public string TimeStamp { get; set; }

    }

    public class EndpointVm
    {
        public string Bucket { get; set; }
        public string Cdn { get; set; }
        public string Region { get; set; }
    }

    public class GoogleVm
    {
        public string Key { get; set; }
        public string Libraries { get; set; }
    }

    public class SecuritySignedUrlVm
    {
        public string Url { get; set; }
        public string EndPoint { get; set; }
        public string Key { get; set; }
    }

    public class WebNavigationVm
    {
        public string WebDomain { get; set; }
        public string WebDomainRedirector { get; set; }
    }

}