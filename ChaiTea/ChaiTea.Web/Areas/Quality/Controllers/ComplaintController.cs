﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class ComplaintController : MvcControllerBase
    {
        //
        // GET: /Quality/Complaint/
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers + "," + Roles.Employees)]
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Quality/Complaint/Details/
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers + "," + Roles.Employees)]
        public ActionResult Details()
        {
            return View();
        }

        //
        // GET: /Quality/Complaint/Entry/
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return id.HasValue ? View(id.Value) : View();
        }

        //
        // GET: /Quality/Complaint/EntryView/
        public ActionResult EntryView(int? id = 0)
        {
            return View(new ComplaintVm
                        {
                            Id = id
                        });
        }
    }
}
