﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class ToDoController : MvcControllerBase
    {
        // GET: Quality/ToDo
        public ActionResult Index()
        {
            return View();
        }

        // GET: Quality/ToDo/Entry
        public ActionResult Entry(int? id = 0)
        {
            return View("Entry", new AuditVm()
            {
                Id = id
            });
        }

        // GET: Quality/ToDo/EntryView
        public ActionResult EntryView(int? id = 0)
        {
            return View("EntryView", new AuditVm()
            {
                Id = id
            });
        }

        //Get: Quality/ToDo/Details
        public ActionResult Details()
        {
            return View();
        }

    }
}