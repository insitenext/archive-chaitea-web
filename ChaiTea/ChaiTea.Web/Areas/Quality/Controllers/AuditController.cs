﻿using ChaiTea.BusinessLogic.ViewModels.Auditing;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class AuditController : MvcControllerBase
    {
        // GET: Quality/Audit
        public ActionResult Index()
        {
            return View();
        }

        // GET: Quality/Audit/Details
        public ActionResult Details()
        {
            return View();
        }

        // GET: Quality/Audit/Entry
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return View("Entry", new AuditVm
            {
                Id = id
            });
        }

        // GET: Quality/Audit/EntryView
        public ActionResult EntryView(int? id = 0)
        {
            return View(new AuditVm
            {
                Id = id
            });
        }

        // GET: Quality/Audit/ProfileManager
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public ActionResult ProfileManager()
        {
            return View();
        }

        // GET: Quality/Audit/ProfileEntry
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public ActionResult ProfileEntry(int? id = 0)
        {
            // If id=0, do create a new profile
            //return View(id);
            return View(new ProfileEntryVm
            {
                Id = id
            });
        }

        // GET: Quality/Audit/ProfileSectionEntry/{auditProfileId}
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public ActionResult ProfileSectionEntryNew(int id = 0)
        {
            return View("ProfileSectionEntry", new AuditProfileSectionVm
            {
                AuditProfileId = id,
                AuditProfileSectionId = 0
            });
        }

        // GET: Quality/Audit/ProfileSectionEntry/{auditProfileSectionId}
        [AuthorizedRolesApi(Roles = Roles.Managers)]
        public ActionResult ProfileSectionEntry(int id = 0)
        {
            return View(new AuditProfileSectionVm
            {
                AuditProfileId = 0,
                AuditProfileSectionId = id
            });
        }
    }
}