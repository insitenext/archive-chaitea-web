﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class MyAuditsController : MvcControllerBase
    {
        // GET: Quality/EmployeeAuditsAvg
        public ActionResult Index()
        {
            return View();
        }
    }
}