﻿using ChaiTea.BusinessLogic.AppUtils.DomainModels;
using ChaiTea.BusinessLogic.Services.Surveys;
using ChaiTea.Web.AWS;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class CustomerSurveyController : MvcControllerBase
    {
        // GET: Quality/CustomerSurvey
        public ActionResult Index()
        {
            return View();
        }

        // GET: Quality/CustomerSurvey/Details
        public ActionResult Details()
        {
            return View();
        }

        // GET: Quality/CustomerSurvey/Entry
        public ActionResult Entry()
        {
            return View();
        }

        // GET: Quality/CustomerSurvey/Survey/:id
        [AllowAnonymous]
        public ActionResult Survey(Guid? id)
        {
            ViewBag.ImageSrc = "~/Content/img/user.jpg";
            ViewBag.ShowImage = false;
            ViewBag.TokenIsValid = false;
            ViewBag.Token = id?.ToString("N");
            var invitation = RecipientInvitationService.GetByToken(UserContext.AnonymousUserContext(), id?.ToString("N"));
            if (invitation != null)
            {
                if (!invitation.Responses.Any())
                {
                    ViewBag.TokenIsValid = true;
                }
                ViewBag.RecipientName = invitation.Recipient.Name;
                if (!String.IsNullOrWhiteSpace(invitation.InvitedByProfilePictureUniqueId))
                {
                    ViewBag.ShowImage = true;
                    ViewBag.SenderName = invitation.InvitedByName;
                    ViewBag.ImageSrc = AwsClient.GetUrlByName(invitation.InvitedByProfilePictureUniqueId, "image");
                }
            }

            return View();
        }

        // GET: Quality/CustomerSurvey/SurveyResponse/:id
        public ActionResult SurveyResponse(int id)
        {
            return View(id);
        }

        /// <summary>
        /// Returns a survey interface with minimal JS to support IE browser versions less than 9
        /// </summary>
        /// <param name="id">Survey Token</param>
        /// <param name="questionId">Question Id</param>
        /// <param name="isEdit">Indicates if coming from an Edit operation</param>
        /// <param name="isItNext">Has value if Next button has been clicked</param>
        /// <param name="isItBack">Has value if the Back button has been clicked</param>
        /// <param name="edit">Has value if the Edit button is clicked</param>
        /// <param name="isReview">Has value if the Review and Submit button is clicked</param>
        /// <returns>Survey View</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult SurveyForCustomer(Guid? id, int questionId = 0, bool isEdit = false, string isItNext = null, string isItBack = null, string isReview = null, string edit = null)
        {
            ViewBag.Edit = isEdit ? 1 : 0;

            if(!string.IsNullOrWhiteSpace(isReview) || !string.IsNullOrWhiteSpace(edit))
            {
                return RedirectToAction("SurveyConfirm", new { id = id });
            }

            ViewBag.QuestionId = questionId;
            ViewBag.QuestionToDisplay = ViewBag.QuestionId + 1;

            return View(RecipientInvitationService.GetByToken(UserContext.AnonymousUserContext(), id?.ToString("N")));
        }

        /// <summary>
        /// Decides on which question the view would display based on the buttons clicked
        /// </summary>
        /// <param name="Token">Guid</param>
        /// <param name="Question">The Question Id</param>
        /// <param name="next">Has value if the next button is clicked</param>
        /// <param name="back">Has value if the back button is clicked</param>
        /// <param name="review">Has value if the Review and Submit button is clicked</param>
        /// <param name="edit">Has value if the Edit button is clicked</param>
        /// <returns>Survey2 SurveyforCustNextQ</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult SurveyforCustNextQ(Guid? Token, int? Question, string next = null, string back = null, string review = null, string edit = null)
        {
            int currentQuestion = (Question ?? 0);

            if (!string.IsNullOrWhiteSpace(next))
            { currentQuestion = currentQuestion + 1; }
            else if (!string.IsNullOrWhiteSpace(back))
            { currentQuestion = currentQuestion - 1; }

            ViewBag.QuestionId = currentQuestion;
            ViewBag.QuestionToDisplay = currentQuestion + 1;
            return RedirectToAction("SurveyForCustomer", new
            {
                id = Token,
                questionId = currentQuestion,
                isItNext = next,
                isItBack = back,
                isReview = review,
                edit = edit
            });
        }

        // GET: Quality/CustomerSurvey/SurveyConfirm/:id
        /// <summary>
        /// Returns a summary of all the survey questions and respective answers
        /// </summary>
        /// <param name="id">Survey Token</param>
        /// <returns>Survey View</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult SurveyConfirm(Guid? id)
        {
            return View(RecipientInvitationService.GetByToken(UserContext.AnonymousUserContext(), id?.ToString("N")));
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SurveySubmit(string token, string responses, string questions)
        {
            JArray allResponses = JArray.Parse(responses);
            JArray allQuestions = JArray.Parse(questions);
            var Answers = new List<RecipientInvitationService.SubmitResponsesDTO.ResponseDTO>();
            int count = 0;
            foreach (var question in allQuestions)
            {
                Answers.Add(new RecipientInvitationService.SubmitResponsesDTO.ResponseDTO()
                {
                    questionId = question["QuestionId"].Value<int>(),
                    answerType = question["QuestionType"].Value<string>(),
                    value = allResponses[count]["Answer"].Value<string>()
                });
                count++;
            }
            var dto = new RecipientInvitationService.SubmitResponsesDTO()
            {
                token = new Guid(token).ToString("N"),
                responses = Answers
            };
            var result = RecipientInvitationService.SubmitResponses(UserContext.AnonymousUserContext(), dto);
            if (result)
            {
                var recipient = RecipientInvitationService.GetByToken(UserContext.AnonymousUserContext(), dto.token);

                ViewBag.ImageSrc = "~/Content/img/user.jpg";
                ViewBag.ShowImage = false;

                if (recipient != null && !String.IsNullOrWhiteSpace(recipient.InvitedByProfilePictureUniqueId))
                {
                    ViewBag.ShowImage = true;
                    ViewBag.SenderName = recipient.InvitedByName;
                    ViewBag.ImageSrc = AwsClient.GetUrlByName(recipient.InvitedByProfilePictureUniqueId, "image");
                }
            }
            return View();
        }
    }
}
