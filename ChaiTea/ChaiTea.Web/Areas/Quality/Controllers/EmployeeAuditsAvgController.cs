﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

using ChaiTea.BusinessLogic.ViewModels.Assessments.ProfessionalReviews;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class EmployeeAuditsAvgController : MvcControllerBase
    {
        // GET: Quality/EmployeeAuditsAvg
        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult Index()
        {
            return View(new EmployeeAuditVm{});
        }
    }
}