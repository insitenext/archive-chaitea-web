﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class DashboardController : MvcControllerBase
    {
        // GET: Quality/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}