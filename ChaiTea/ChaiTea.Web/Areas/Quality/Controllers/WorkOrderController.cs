﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class WorkOrderController : MvcControllerBase
    {
        // GET: Quality/WorkOrder
        public ActionResult Index()
        {
            return View();
        }

        // GET: Quality/WorkOrder/Details
        public ActionResult Details()
        {
            return View();
        }

        // GET: Quality/WorkOrder/Upcoming
        public ActionResult Upcoming()
        {
            return View();
        }

        // GET: Quality/WorkOrder/Entry
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0, int? workOrderSeriesId = 0)
        {
            return View(new WorkOrderVm
            {
                Id = id,
                WorkOrderSeriesId = workOrderSeriesId,
                State = WorkOrderMode.Edit
            });
        }

        // GET: Quality/WorkOrder/EntryFinalize
        public ActionResult EntryFinalize(int? id = 0, int? workOrderSeriesId = 0)
        {
            return View("Entry", new WorkOrderVm
            {
                Id = id,
                WorkOrderSeriesId = workOrderSeriesId,
                State = WorkOrderMode.Close
            });
        }

        // GET: Quality/WorkOrder/EntryView
        public ActionResult EntryView(int? id = 0)
        {
            return View(new WorkOrderVm
            {
                Id = id
            });
        }
    }
}