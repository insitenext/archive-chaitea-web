﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Quality.Controllers
{
    [AuthorizedRoles]
    public class ComplimentController : MvcControllerBase
    {
        //
        // GET: /Quality/Compliment/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Quality/Compliment/Details/
        public ActionResult Details()
        {
            return View();
        }

        //
        // GET: /Quality/Compliment/Entry/
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return id.HasValue ? View(id.Value) : View();
        }

        // GET: /Quality/Compliment/EntryView/
        public ActionResult EntryView(int? id)
        {
            return View(new ComplimentVm
                        {
                            Id = id
                        });
        }
    }
}