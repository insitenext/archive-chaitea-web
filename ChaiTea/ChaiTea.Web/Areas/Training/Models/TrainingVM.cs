﻿namespace ChaiTea.Web.Areas.Training.Models
{
    public class TrainingVm
    {
        public int? Id { get; set; }

        public string PageTitle { get; set; }

        public bool myTeam { get; set; }

        public int? UserId { get; set; }
    }
}