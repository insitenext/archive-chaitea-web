﻿using ChaiTea.Web.Areas.Training.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class CourseController : MvcControllerBase
    {
        public CourseController()
        {
            ViewBag.Icon = "icon-books2";
        }

        // GET: Training/Course
        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Training/Course/EntryView/{id}
        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public ActionResult EntryView(int? id = 0)
        {
            return View("EntryView", new TrainingVm
            {
                Id = id
            });
        }

        // GET: Training/Course/Entry
        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public ActionResult Entry(int? id = 0)
        {
            return View("Entry", new TrainingVm
            {
                Id = id
            });
        }



        // GET: Training/Course/Assign
        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public ActionResult Assign()
        {
            return View();
        }

        // GET: Training/Course/Assign
        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public ActionResult Unassign()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.TrainingAdmins)]
        public RedirectResult Settings()
        {
            return Redirect("../../Admin/KnowledgeCenter/CourseTypes");
        }
    }
}