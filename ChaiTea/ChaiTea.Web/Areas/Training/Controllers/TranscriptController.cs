﻿using ChaiTea.Web.Areas.Training.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class TranscriptController : MvcControllerBase
    {
        public TranscriptController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/Transcript
        public ActionResult Index(int? id, int? CE_ID)
        {
            if (!id.HasValue) { id = CE_ID; }

            return View();
        }

        // GET: Training/Transcript/EntryView/{id}
        public ActionResult EntryView(int? id = 0)
        {
            return View("Index", new TrainingVm
            {
                Id = id
            });
        }
    }
}