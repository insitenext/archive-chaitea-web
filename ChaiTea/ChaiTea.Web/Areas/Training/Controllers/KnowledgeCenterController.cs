﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class KnowledgeCenterController : MvcControllerBase
    {
        public KnowledgeCenterController()
        {
            ViewBag.Icon = null;
        }

        // GET: Training/KnowledgeCenter
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EntryView(int id, string pageTitle = "")
        {
            return View(new KnowledgeCenterVm{ Id = id, PageTitle = pageTitle });
        }

        public ActionResult TakeCourse(int id, int employeeId = 0, string pageTitle = "",int courseTypeId = 0)
        {
            return View(new KnowledgeCenterVm { Id = id, EmployeeId = employeeId, PageTitle = pageTitle,CourseTypeId = courseTypeId });
        }

        public ActionResult TakeQuiz(int id, int courseAttachmentId = 0)
        {
            return View(new KnowledgeCenterVm { EmployeeCourseId = id, CourseAttatchmentId = courseAttachmentId});
        }
    }

    public class KnowledgeCenterVm
    {
        public int Id { get; set; }
        public string PageTitle { get; set; }
        public int EmployeeId { get; set; }

        public int CourseId { get; set; }

        public int EmployeeCourseId { get;set; }

        public int CourseAttatchmentId { get;set; }
        public int CourseTypeId { get; set; }
    }
}