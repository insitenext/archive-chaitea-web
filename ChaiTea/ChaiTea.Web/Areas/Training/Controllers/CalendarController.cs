﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class CalendarController : MvcControllerBase
    {
        public CalendarController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/Calendar
        public ActionResult Index()
        {
            return View();
        }
    }
}