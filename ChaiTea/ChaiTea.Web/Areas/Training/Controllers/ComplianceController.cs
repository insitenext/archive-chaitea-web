﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class ComplianceController : MvcControllerBase
    {
        public ComplianceController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/TrainingCompliance
        public ActionResult Index()
        {
            return View();
        }

    }
}