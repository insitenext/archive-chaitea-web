﻿using ChaiTea.Web.Areas.Training.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class ServiceExcellenceController : MvcControllerBase
    {
        public ServiceExcellenceController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/ServiceExcellence
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Index()
        {
            return View(new TrainingVm{});
        }

        public ActionResult Entry(int? id = 0)
        {

            return View(new TrainingVm
            {
                Id = id
            });
        }

        [AuthorizedRoles(Roles = Roles.Managers)]
        public ActionResult BonusConfig()
        {
            return View();
        }

        public ActionResult Details(int id, string pageTitle)
        {
            return View(new TrainingVm
            {
                Id = id,
                PageTitle = pageTitle
            });
        }
    }
}