﻿using ChaiTea.Web.Areas.Training.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class PositionProfileController : MvcControllerBase
    {
        public PositionProfileController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/PositionProfile
        //[AuthorizedRoles(Roles = Roles.Managers + "," + Roles.ProfileViewers)]
        public ActionResult Index(int? id = 0, bool myTeam = false)
        {
            return View(new TrainingVm
            {
                Id = id,
                myTeam = myTeam
            });
        }

        // GET: Training/PositionProfile/Entry
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.SepViewers)]
        public ActionResult Entry(int? id = 0)
        {
            return View("Index", new TrainingVm
            {
                Id = id
            });
        }

        // GET: Training/PositionProfile/EntryView/{id}
        public ActionResult EntryView(int? id, int? CE_ID, bool myTeam = false, int? userId = 0)
        {
            if (!id.HasValue) { id = CE_ID; }

            return View(new TrainingVm
            {
                Id = id,
                myTeam = myTeam,
                UserId = User.Identity.GetUserId<int>() // TODO: convert to either *real* userid or employeeid
            });
        }

        //GET: Training/PositionProfile/Config
        public ActionResult Config(int id, bool myTeam = false, int? userId = 0)
        {
            return View(new TrainingVm
            {
                Id = id,
                myTeam = myTeam,
                UserId = User.Identity.GetUserId<int>() // TODO: convert to either *real* userid or employeeid
            });
        }

    }
}