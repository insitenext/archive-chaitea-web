﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class ManageReportCardsController : MvcControllerBase
    {
        public ManageReportCardsController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/ManageReportCards
        public ActionResult Index()
        {
            return View();
        }
    }
}