﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Training.Controllers
{
    [AuthorizedRoles]
    public class KPIComplianceController : MvcControllerBase
    {
        public KPIComplianceController()
        {
            ViewBag.Icon = "fa-training";
        }

        // GET: Training/KPICompliance
        public ActionResult Index()
        {
            return View();
        }
    }
}