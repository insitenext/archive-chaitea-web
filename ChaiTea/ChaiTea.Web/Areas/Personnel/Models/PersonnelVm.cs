﻿namespace ChaiTea.Web.Areas.Personnel.Models
{
    public class PersonnelVm
    {
        public int? Id { get; set; }
        public int? EmpId { get; set; }
    }

    public class ConductVM
    {
        public int? Id { get; set; }
    }

    public class SafetyVM
    {
        public int? Id { get; set; }
    }
    public class OwnershipVM
    {
        public int? Id { get; set; }
    }
}