﻿using ChaiTea.Web.Areas.Personnel.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class ConductController: MvcControllerBase
    {
        public ConductController()
        {
            ViewBag.Icon = "fa-personnel";
        }
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Details()
        {
            return View(new ConductVM{});
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return View(new ConductVM
            {
                Id = id
            });
        }

        //
        // GET: /Personnel/Conduct/EntryView/
        public ActionResult EntryView(int? id = 0)
        {
            return View(new ConductVM
            {
                Id = id
            });
        }
    }
}