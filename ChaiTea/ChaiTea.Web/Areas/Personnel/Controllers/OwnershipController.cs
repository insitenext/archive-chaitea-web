﻿using ChaiTea.Web.Areas.Personnel.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class OwnershipController : MvcControllerBase
    {
        public OwnershipController()
        {
            ViewBag.Icon = "fa-personnel";
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details()
        {
            return View();
        }

        public ActionResult Entry(int? id = 0)
        {
            return View(new OwnershipVM
            {
                Id = id
            });
        }

        //
        // GET: /Personnel/Conduct/EntryView/
        public ActionResult EntryView(int? id = 0)
        {
            return View(new OwnershipVM
            {
                Id = id
            });
        }
    }
}