﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Linq;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class MyProfileController: MvcControllerBase
    {

        public ActionResult Index()
        {
            var contextService = new UserContextService(UserCtx);
            if (contextService.IsInRole(Roles.Managers))
            {
                return RedirectToAction("Index", "Transcript", new { area = "Training" });
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult ReportCard(int? id, int? CE_ID)
        {
            if (!id.HasValue && !CE_ID.HasValue)
            { return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest,"either id or ce_id must have a value"); }

            if (!id.HasValue)
            { id = CE_ID; }

            var contextService = new UserContextService(UserCtx);

            if (contextService.IsInRole(Roles.Managers) || contextService.IsInRole(Roles.Customers))
            {
                // Managers and customers should be able to view all employee scorecards at the given client/site.
                // so lookup employee using the current user's (whom is a Manager or Customer) context
                // if employee is found using current User's context, then everything is good to go
                if (new ChaiTea.BusinessLogic.Services.Employment.EmployeeService(UserCtx).Get(id.Value) == null)
                { return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden); }

                return View();
            }

            if (contextService.IsInRole(Roles.Employees))
            {
                var orgUserId = contextService.GetOrgUserId();
                // The user with the employee role should be able to only view their own reportcard.
                if (orgUserId != id)
                { return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden); }    
            }

            return View();
        }

        public ActionResult TeamMembers()
        {
            return View();
        }
        

    }
}