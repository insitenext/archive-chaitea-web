﻿using ChaiTea.Web.Areas.Personnel.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class AttendanceController: MvcControllerBase
    {
        public AttendanceController()
        {
            ViewBag.Icon = "fa-personnel";
        }

        public ActionResult Index()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Details()
        {
            return View(new PersonnelVm{});
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return View(new PersonnelVm
            {
                Id = id
            });
        }

        //
        // GET: /Personnel/Attendance/EntryView/
        public ActionResult EntryView(int? id = 0)
        {
            return View(new PersonnelVm
            {
                Id = id
            });
        }
    }
}