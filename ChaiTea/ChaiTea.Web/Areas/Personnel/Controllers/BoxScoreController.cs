﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class BoxScoreController : MvcControllerBase
    {
        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Index()
        {
            return View();
        }
    }
}