﻿using ChaiTea.Web.Areas.Personnel.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class AuditController: MvcControllerBase
    {
        public AuditController()
        {
            ViewBag.Icon = "fa-user";
        }


        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Details()
        {
            return View();
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0,int? empId = 0)
        {
            return View(new PersonnelVm
            {
                Id = id,
                EmpId = empId
            });
        }

        //
        // GET: /Personnel/Attendance/EntryView/
        public ActionResult EntryView(int id = 0)
        {
            return View(new PersonnelVm
            {
                Id = id
            });
        }
    }
}