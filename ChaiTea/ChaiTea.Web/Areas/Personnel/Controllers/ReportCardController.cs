﻿using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class ReportCardController : MvcControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}