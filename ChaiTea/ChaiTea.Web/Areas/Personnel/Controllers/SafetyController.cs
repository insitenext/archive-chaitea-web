﻿using ChaiTea.Web.Areas.Personnel.Models;
using ChaiTea.Web.Common;
using ChaiTea.Web.Identity;
using System.Web.Mvc;

namespace ChaiTea.Web.Areas.Personnel.Controllers
{
    [AuthorizedRoles]
    public class SafetyController: MvcControllerBase
    {
        public SafetyController()
        {
            ViewBag.Icon = "fa-ehs";
        }
        public ActionResult Index()
        {
            return RedirectToAction("Details");
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Details()
        {
            return View(new SafetyVM{});
        }

        [AuthorizedRoles(Roles = Roles.Managers + "," + Roles.Customers)]
        public ActionResult Entry(int? id = 0)
        {
            return View(new SafetyVM
            {
                Id = id
            });
        }

        //
        // GET: /Personnel/Safety/EntryView/
        public ActionResult EntryView(int? id = 0)
        {
            return View(new SafetyVM
            {
                Id = id
            });
        }
    }
}