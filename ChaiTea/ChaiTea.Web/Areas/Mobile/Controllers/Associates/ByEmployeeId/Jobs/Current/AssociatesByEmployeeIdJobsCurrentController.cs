﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Jobs.Current
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Jobs/Current")]
    public class AssociatesByEmployeeIdJobsCurrentController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(JobsService.JobDTO))]
        public IHttpActionResult GetJobDetails(int EmployeeId)
        {
            return Ok(new JobsService(UserCtx).GetJobDetailsForEmployee(EmployeeId));
        }

        [HttpGet]
        [Route("Purpose")]
        [ResponseType(typeof(JobsService.PurposeDTO))]
        public IHttpActionResult GetPurpose(int EmployeeId)
        {
            return Ok(new JobsService(UserCtx).GetPurposeForEmployee(EmployeeId));
        }

        [HttpGet]
        [Route("KPIs")]
        [ResponseType(typeof(IEnumerable<JobsService.KPIListItemDTO>))]
        public IHttpActionResult GetKPIs(int EmployeeId)
        {
            return Ok(new JobsService(UserCtx).GetKPIsForEmployee(EmployeeId));
        }

        [HttpGet]
        [Route("ServiceAreas")]
        [ResponseType(typeof(IEnumerable<EmployeesService.ServiceAreasDTO>))]
        public IHttpActionResult GetServiceAreas(int EmployeeId)
        {
            return Ok(new EmployeesService(UserCtx).GetLocationsForEmployee(EmployeeId));
        }
    }
}
