﻿using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Messages
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Messages")]
    public class AssociatesByEmployeeIdMessagesController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.DetailDTO>))]
        public IHttpActionResult GetMessageDetails(int EmployeeId, DateTime? startDate = null, DateTime? endDate = null, int? top = null, int? skip = 0, bool? hasBeenSeen = null, int? departmentId = null, AssociateMessagesService.MessageOrderBy? orderBy = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            return Ok(new AssociateMessagesService(UserCtx).GetMessageDetails(_filterToDate, _filterToPage, hasBeenSeen, departmentId, new int[] { EmployeeId }, orderBy));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateMessagesService.DetailDTO))]
        [Route("{messageId:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMessageDetails(int EmployeeId, int messageId)
        {
            return Ok(new AssociateMessagesService(UserCtx).GetMessageDetails(messageId, EmployeeId));
        }
        
        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.UpdateStatusDTO>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(int EmployeeId, IEnumerable<AssociateMessagesService.UpdateStatusDTO> dto)
        {
            return Ok(new AssociateMessagesService(UserCtx).UpdateStatus(EmployeeId, dto));
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasNotBeenSeenCount")]
        public IHttpActionResult GetHasNotBeenSeenCount(int EmployeeId)
        {
            return Ok(new AssociateMessagesService(UserCtx).GetHasNotBeenSeenCount(EmployeeId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService_MessageOrderByExtension.OrderByDTO>))]
        [Route("OrderByOptions")]
        public IHttpActionResult GetValidOrderByValues()
        {
            return Ok(AssociateMessagesService_MessageOrderByExtension.OrderByValues);
        }
    }
}
