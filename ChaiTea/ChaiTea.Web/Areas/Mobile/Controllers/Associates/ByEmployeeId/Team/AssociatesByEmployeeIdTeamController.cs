﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Team
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Team")]
    public class AssociatesByEmployeeIdTeamController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<EmployeesService.EmployeeTeammateDTO>))]
        public IHttpActionResult GetAssociateTeam(int EmployeeId)
        {
            return Ok(new EmployeesService(UserCtx).GetTeam(EmployeeId));
        }
    }
}
