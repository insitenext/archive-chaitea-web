﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Notifications
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Notifications")]
    public class AssociatesByEmployeeIdNotificationsController : MobileApiController
    {
        /// <summary>
        /// Return list of Notification records.
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateNotificationsService.DetailDTO>))]
        public IHttpActionResult GetNotifications(int EmployeeId, int? top = null, int? skip = 0)
        {
            var pagingFilter = PagingFilter.TryValidate(skip, top);

            return Ok(new AssociateNotificationsService(UserCtx).GetNotificationDetails(EmployeeId, pagingFilter));
        }

        /// <summary>
        /// Set/Reset the IsNew flag of the Notification record(s). 
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <param name="dto">List of NotificationId's to be updated. With the associated status.</param>
        /// <returns>List of updated records.</returns>
        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateNotificationsService.UpdateStatusRequestResponse>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(int EmployeeId, IEnumerable<AssociateNotificationsService.UpdateStatusRequestResponse> dto)
        {
            return Ok(new AssociateNotificationsService(UserCtx).UpdateStatus(EmployeeId, dto));
        }

        /// <summary>
        /// Return the count of the number of Notification records where the IsNew flag is "True".
        /// i.e. HasNotBeenSeen == True
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <returns>Integer count.</returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasNotBeenSeenCount")]
        public IHttpActionResult GetHasNotBeenSeenCount(int EmployeeId)
        {
            return Ok(new AssociateNotificationsService(UserCtx).GetIsNewCount(EmployeeId, true));
        }

        /// <summary>
        /// Return the count of the number of Notification records where the IsNew flag is "False".
        /// i.e. HasNotBeenSeen == False
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <returns>Integer count.</returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasBeenSeenCount")]
        public IHttpActionResult GetHasBeenSeenCount(int EmployeeId)
        {
            return Ok(new AssociateNotificationsService(UserCtx).GetIsNewCount(EmployeeId, false));
        }
    }
}
