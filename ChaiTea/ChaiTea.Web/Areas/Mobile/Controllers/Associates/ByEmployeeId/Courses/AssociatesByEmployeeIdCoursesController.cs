﻿using ChaiTea.BusinessLogic.Services.Courses;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Courses
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Courses")]
    public class AssociatesByEmployeeIdCoursesController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<EmployeeCoursesService.ListItemDTO>))]
        public IHttpActionResult GetAssociateCourses(int EmployeeId)
        {
            return Ok(new EmployeeCoursesService(UserCtx).GetCourseListForEmployeeId(EmployeeId));
        }
    }
}
