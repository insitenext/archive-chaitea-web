﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Supervisors.Current
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Supervisors/Current")]
    public class AssociatesByEmployeeIdSupervisorsCurrentController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(EmployeesService.SupervisorDetailDTO))]
        public IHttpActionResult GetAssociateSupervisor(int EmployeeId)
        {
            return Ok(new EmployeesService(UserCtx).GetSupervisorForEmployee(EmployeeId));
        }
    }
}
