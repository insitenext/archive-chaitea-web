﻿using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.ActionRequests.Tasks;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Tasks
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Tasks")]
    public class AssociatesByEmployeeIdTasksController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.DetailDTO>))]
        public IHttpActionResult GetTaskListItems(int EmployeeId, DateTime? startDate = null, DateTime? endDate = null, int? skip = 0, int? top = null, bool? IsComplete = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            return Ok(new AssociateTasksService(UserCtx).GetTaskDetails(EmployeeId, _filterToDate, _filterToPage, IsComplete));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateTasksService.DetailDTO))]
        [Route("{taskId:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetTaskDetails(int EmployeeId, int taskId)
        {
            return Ok(new AssociateTasksService(UserCtx).GetTaskDetails(taskId, EmployeeId));
        }

        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.UpdateStatusDTO>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(int EmployeeId, IEnumerable<AssociateTasksService.UpdateStatusDTO> dto)
        {
            return Ok(new AssociateTasksService(UserCtx).UpdateStatus(EmployeeId, dto));
        }


        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("InCompleteCount")]
        public IHttpActionResult GetInCompleteCount(int EmployeeId)
        {
            return Ok(new AssociateTasksService(UserCtx).GetInCompleteCount(EmployeeId));
        }


        [HttpGet]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.DetailDTO>))]
        [Route("DueSoon")]
        public IHttpActionResult GetDueSoonTaskDetails(int EmployeeId)
        {
            return Ok(new AssociateTasksService(UserCtx).GetDueSoonTaskDetails(EmployeeId));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateTasksService.MyWorkDTO))]
        [Route("MyWork")]
        public IHttpActionResult GetMyWorkInfo(int EmployeeId, DateTime? startDate = null, DateTime? endDate = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            return Ok(new AssociateTasksService(UserCtx).GetMyWorkInfo(EmployeeId, _filterToDate));
        }
    }
}