﻿using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Attributes.Attachments
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Attributes/Attachments")]
    public class AssociatesByEmployeeIdAttributesAttachmentsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<EmployeeAttributeService.EmployeeAttributeAttachmentDTO>))]
        public IHttpActionResult GetAssociateAttachments(int EmployeeId)
        {
            return Ok(new EmployeeAttributeService(UserCtx).GetAttachmentsForEmployeeId(EmployeeId));
        }
    }
}
