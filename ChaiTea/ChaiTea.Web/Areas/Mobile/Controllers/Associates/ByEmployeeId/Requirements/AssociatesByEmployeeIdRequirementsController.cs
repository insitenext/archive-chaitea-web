﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.Employment;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Requirements
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Requirements")]
    public class AssociatesByEmployeeIdRequirementsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<string>))]
        public IHttpActionResult GetRequirements(int EmployeeId)
        {
            return Ok(new EmployeeRequirementsService(UserCtx).GetRequirementsForEmployeeId(EmployeeId));
        }
    }
}
