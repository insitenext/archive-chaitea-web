﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.Employment;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.ByEmployeeId.Equipment
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/{EmployeeId:int}/Equipment")]
    public class AssociatessByEmployeeIdEquipmentController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<string>))]
        public IHttpActionResult GetListItems(int EmployeeId)
        {
            return Ok(new EmployeeEquipmentService(UserCtx).GetEquipmentForEmployeeId(EmployeeId));
        }
    }
}
