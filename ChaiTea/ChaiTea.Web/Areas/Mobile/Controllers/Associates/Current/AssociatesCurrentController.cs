﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.Current
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/Current")]
    public class AssociatesCurrentController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(EmployeeService.AssociateContextDTO))]
        public IHttpActionResult GetCurrentAssociate()
        {
            var userId = UserContextService.CurrentUserId;
            return Ok(EmployeeService.GetCurrentAssociate(userId));
        }
    }
}
