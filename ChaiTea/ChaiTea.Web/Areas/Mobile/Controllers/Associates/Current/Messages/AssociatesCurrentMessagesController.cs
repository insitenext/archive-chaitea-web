﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Communications;
using ChaiTea.Web.Identity;

using log4net;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.Current.Messages
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/Current/Messages")]
    public class AssociatesCurrentMessagesController : MobileApiController
    {
        public static readonly ILog Logger = LogManager.GetLogger(nameof(AssociatesCurrentMessagesController));

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.DetailDTO>))]
        public IHttpActionResult GetMessageDetails(DateTime? startDate = null, DateTime? endDate = null, int? top = null, int? skip = 0, bool? hasBeenSeen = null, int? departmentId = null, AssociateMessagesService.MessageOrderBy? orderBy = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            var orgUserId = new UserContextService(UserCtx).GetOrgUserId();
            return Ok(new AssociateMessagesService(UserCtx).GetMessageDetails(_filterToDate, _filterToPage, hasBeenSeen, departmentId, new int[] { orgUserId }, orderBy));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateMessagesService.DetailDTO))]
        [Route("{messageId:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetMessageDetails(int messageId)
        {
            if (messageId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(messageId), $"Must pass a valid message Id value. The value passed ({messageId}) is not valid.");
            }

            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                int orgUserId;

                using (var usrContext = new UserContextService(UserCtx))
                {
                    orgUserId = usrContext.GetOrgUserId();
                }

                if (orgUserId > 0)
                {
                    return Ok(assocMsgSvc.GetMessageDetails(messageId, orgUserId));
                }

                Logger.Warn($"Current user context (UserId:{UserCtx.UserId}) does not map to a valid OrgUserId.");
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
            }
        }
        
        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService.UpdateStatusDTO>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(IEnumerable<AssociateMessagesService.UpdateStatusDTO> dto)
        {
            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                int orgUserId;

                using (var usrContext = new UserContextService(UserCtx))
                {
                    orgUserId = usrContext.GetOrgUserId();
                }

                if (orgUserId > 0)
                {
                    return Ok(assocMsgSvc.UpdateStatus(orgUserId, dto));
                }

                Logger.Warn($"Current user context (UserId:{UserCtx.UserId}) does not map to a valid OrgUserId.");
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
            }
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasNotBeenSeenCount")]
        public IHttpActionResult GetHasNotBeenSeenCount()
        {
            using (var assocMsgSvc = new AssociateMessagesService(UserCtx))
            {
                int orgUserId;

                using (var usrContext = new UserContextService(UserCtx))
                {
                    orgUserId = usrContext.GetOrgUserId();
                }

                if (orgUserId > 0)
                {
                    return Ok(assocMsgSvc.GetHasNotBeenSeenCount(orgUserId));
                }

                Logger.Warn($"Current user context (UserId:{UserCtx.UserId}) does not map to a valid OrgUserId.");
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AssociateMessagesService_MessageOrderByExtension.OrderByDTO>))]
        [Route("OrderByOptions")]
        public IHttpActionResult GetValidOrderByValues()
        {
            return Ok(AssociateMessagesService_MessageOrderByExtension.OrderByValues);
        }
    }
}
