﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.ActionRequests.Tasks;
using ChaiTea.BusinessLogic.Services.AppMetaData;

using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.Current.Tasks
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/Current/Tasks")]
    public class AssociatesCurrentTasksController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.DetailDTO>))]
        public IHttpActionResult GetTaskListItems(DateTime? startDate = null, DateTime? endDate = null, int? skip = 0, int? top = null, bool? IsComplete = null)
        {
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);
            var _filterToPage = PagingFilter.TryValidate(skip, top);
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();

            return Ok(new AssociateTasksService(UserCtx).GetTaskDetails(orgUserId, _filterToDate, _filterToPage, IsComplete));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateTasksService.DetailDTO))]
        [Route("{taskId:int}")]
        [AuthorizedRolesApi]
        public IHttpActionResult GetTaskDetails(int taskId)
        {
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();
            var entity = new AssociateTasksService(UserCtx).GetTaskDetails(taskId, orgUserId);

            return Ok(entity);
        }

        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.UpdateStatusDTO>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(IEnumerable<AssociateTasksService.UpdateStatusDTO> dto)
        {
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();

            return Ok(new AssociateTasksService(UserCtx).UpdateStatus(orgUserId, dto));
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("InCompleteCount")]
        public IHttpActionResult GetInCompleteCount()
        {
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();

            return Ok(new AssociateTasksService(UserCtx).GetInCompleteCount(orgUserId));
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<AssociateTasksService.DetailDTO>))]
        [Route("DueSoon")]
        public IHttpActionResult GetDueSoonTaskDetails()
        {
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();

            return Ok(new AssociateTasksService(UserCtx).GetDueSoonTaskDetails(orgUserId));
        }

        [HttpGet]
        [ResponseType(typeof(AssociateTasksService.MyWorkDTO))]
        [Route("MyWork")]
        public IHttpActionResult GetMyWorkInfo(DateTime? startDate = null, DateTime? endDate = null)
        {
            var orgUserId = new UserContextService(this.UserCtx).GetOrgUserId();
            var _filterToDate = DateTimeRangeFilter.TryValidateOptional(startDate, endDate);

            return Ok(new AssociateTasksService(UserCtx).GetMyWorkInfo(orgUserId, _filterToDate));
        }
    }
}