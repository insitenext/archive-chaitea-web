﻿using System.Collections;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associate.Current
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/Current")]
    public class CurrentAssociateController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [AllowAnonymous] // TODO: Remove this line to force a currently logged in user
        [ResponseType(typeof(EmployeeService.AssociateContextDTO))]
        public IHttpActionResult GetCurrentAssociate()
        {
            var userId = UserContextService.CurrentUserId;
            return Ok(EmployeeService.GetCurrentAssociate(userId));
        }

        public class MyTeamDto
        {
            public int PersonId { get; set; }
            public string Name { get; set; }
            public class JobDto
            {
                public int JobId { get; set; }
                public string Title { get; set; }
            }
            public JobDto JobInfo { get; set; }
            public bool IsSupervisor { get; set; }
        }


        [HttpGet]
        [Route("Team")]
        [ResponseType(typeof(MyTeamDto[]))]
        public IHttpActionResult GetAssociateTeam()
        {
            ArrayList myTeamList = new ArrayList();
            MyTeamDto myTeam = new MyTeamDto();
            myTeam.PersonId = 1;
            myTeam.Name = "Kirk Hammett";
            myTeam.IsSupervisor = true;

            MyTeamDto.JobDto JobInfo = new MyTeamDto.JobDto();
            JobInfo.JobId = 1;
            JobInfo.Title = "Lead Guitarist";
            myTeam.JobInfo = JobInfo;

            myTeamList.Add(myTeam);
            return Ok(myTeamList);
        }

        public class CoursesDto
        {
            public int CourseId { get; set; }
            public string CourseName { get; set; }
        }

        [HttpGet]
        [Route("Courses")]
        [ResponseType(typeof(CoursesDto[]))]
        public IHttpActionResult GetAssociateCourses()
        {
            ArrayList courseList = new ArrayList();
            CoursesDto course = new CoursesDto();
            course.CourseId = 1;
            course.CourseName = "Safety";
            courseList.Add(course);

            course = new CoursesDto();
            course.CourseId = 2;
            course.CourseName = "Audits";
            courseList.Add(course);

            return Ok(courseList);
        }

        public class SupervisorsDto
        {
            public int PersonId { get; set; }
            public string Name { get; set; }
            public class JobDto
            {
                public int JobId { get; set; }
                public string Title { get; set; }
            }
            public JobDto JobInfo { get; set; }
        }

        [HttpGet]
        [Route("Supervisors/Current")]
        [ResponseType(typeof(SupervisorsDto))]
        public IHttpActionResult GetAssociateSupervisor()
        {
            SupervisorsDto supervisor = new SupervisorsDto();
            supervisor.PersonId = 4;
            supervisor.Name = "Lars Ulrich";
            SupervisorsDto.JobDto JobInfo = new SupervisorsDto.JobDto();
            JobInfo.JobId = 5;
            JobInfo.Title = "Drummer";
            supervisor.JobInfo = JobInfo;

            return Ok(supervisor);
        }

        public class AttachmentsDto
        {
            public string Name { get; set; }
            public string MimeType { get; set; }
            public string UniqueId { get; set; }

        }

        [HttpGet]
        [Route("Attributes/Attachments")]
        [ResponseType(typeof(AttachmentsDto[]))]
        public IHttpActionResult GetAssociateAttachments()
        {
            ArrayList attachmentList = new ArrayList();
            AttachmentsDto attachment = new AttachmentsDto();
            attachment.Name = "Employee Handbook (EN)";
            attachment.MimeType = "application/pdf";
            attachment.UniqueId = "https://s3-us-west-1.amazonaws.com/sbm-insight-dev/356147dd-6ceb-9eed-cfd0-30366af98feb.jpg";
            attachmentList.Add(attachment);

            attachment = new AttachmentsDto();
            attachment.Name = "Employee Handbook (ES)";
            attachment.MimeType = "application/pdf";
            attachment.UniqueId = "https://s3-us-west-1.amazonaws.com/sbm-insight-dev/image/00006472-8755-2e80-d8b8-e45567471a0d.jpg";
            attachmentList.Add(attachment);

            attachment = new AttachmentsDto();
            attachment.Name = "Union Contract (EN) (ES)";
            attachment.MimeType = "application/pdf";
            attachment.UniqueId = "https://s3-us-west-1.amazonaws.com/sbm-insight-dev/image/00006472-8755-2e80-d8b8-e45567471a0d.jpg";
            attachmentList.Add(attachment);

            return Ok(attachmentList);
        }

    }
}
