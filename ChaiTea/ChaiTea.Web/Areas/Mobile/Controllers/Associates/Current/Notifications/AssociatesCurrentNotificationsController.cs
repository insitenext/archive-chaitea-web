﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.Web.Identity;

using ChaiTea.BusinessLogic.AppUtils.DataFilters;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Communications;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Associates.Current.Notifications
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Associates/Current/Notifications")]
    public class AssociatesCurrentNotificationsController : MobileApiController
    {
        /// <summary>
        /// Return list of Notification records.
        /// </summary>
        /// <param name="top"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<AssociateNotificationsService.DetailDTO>))]
        public IHttpActionResult GetNotifications(int? top = null, int? skip = 0)
        {
            var pagingFilter = PagingFilter.TryValidate(skip, top);

            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocNotificationSvc = new AssociateNotificationsService(UserCtx))
            {
                return Ok(assocNotificationSvc.GetNotificationDetails(orgUserId, pagingFilter));
            }
        }

        /// <summary>
        /// Set/Reset the IsNew flag of the Notification record(s). 
        /// </summary>
        /// <param name="dto">List of NotificationId's to be updated. With the associated status.</param>
        /// <returns>List of updated records.</returns>
        [HttpPut]
        [ResponseType(typeof(IEnumerable<AssociateNotificationsService.UpdateStatusRequestResponse>))]
        [Route("Status")]
        public IHttpActionResult UpdateStatus(IEnumerable<AssociateNotificationsService.UpdateStatusRequestResponse> dto)
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocNotificationSvc = new AssociateNotificationsService(UserCtx))
            {
                var result = assocNotificationSvc.UpdateStatus(orgUserId, dto);

                if (result != null)
                {
                    return Ok(result);
                }
            }

            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
        }

        /// <summary>
        /// Return the count of the number of Notification records where the IsNew flag is "True".
        /// i.e. HasNotBeenSeen == True
        /// </summary>
        /// <returns>Integer count.</returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasNotBeenSeenCount")]
        public IHttpActionResult GetHasNotBeenSeenCount()
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocNotificationSvc = new AssociateNotificationsService(UserCtx))
            {
                return Ok(assocNotificationSvc.GetIsNewCount(orgUserId, true));
            }
        }

        /// <summary>
        /// Return the count of the number of Notification records where the IsNew flag is "False".
        /// i.e. HasNotBeenSeen == False
        /// </summary>
        /// <returns>Integer count.</returns>
        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("HasBeenSeenCount")]
        public IHttpActionResult GetHasBeenSeenCount()
        {
            int orgUserId;

            using (var usrCtxSvc = new UserContextService(UserCtx))
            {
                orgUserId = usrCtxSvc.GetOrgUserId();
            }

            using (var assocNotificationSvc = new AssociateNotificationsService(UserCtx))
            {
                return Ok(assocNotificationSvc.GetIsNewCount(orgUserId, false));
            }
        }
    }
}
