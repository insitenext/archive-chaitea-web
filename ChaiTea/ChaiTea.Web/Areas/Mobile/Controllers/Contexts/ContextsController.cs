﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Employment;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Contexts
{
    [Obsolete("Use /Associates/Current endpoint instead", false)]
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Contexts")]
    public class ContextsController : MobileApiController
    {
        [HttpGet]
        [Route("CurrentAssociate")]
        [AllowAnonymous] // TODO: Remove this line to force a currently logged in user
        [ResponseType(typeof(EmployeeService.AssociateContextDTO))]
        public IHttpActionResult GetCurrentAssociate()
        {
            var userId = UserContextService.CurrentUserId;
            return Ok(EmployeeService.GetCurrentAssociate(userId));
        }
    }
}
