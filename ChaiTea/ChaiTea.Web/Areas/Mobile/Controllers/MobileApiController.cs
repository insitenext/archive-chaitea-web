﻿using ChaiTea.BusinessLogic.AppUtils.Interfaces;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Mobile.Controllers
{
    public abstract class MobileApiController : ApiController
    {
        private IUserContext _usrContext;
        protected IUserContext UserCtx
        {
            get
            {
                if (_usrContext == null)
                {
                    IEnumerable<Claim> contextItemsFromClaims = null;
                    var identity = User?.Identity;
                    if (identity != null && identity is ClaimsIdentity)
                    {
                        contextItemsFromClaims = (identity as ClaimsIdentity).Claims;
                    }

                    var contextItemsFromHeader = Request.Headers
                                                        .Select(t => new KeyValuePair<String, String>(t.Key, t.Value.First()))
                                                        .ToList();
                    _usrContext = UserContextService.BuildFromRequest(true, UserContextService.CurrentUserId, contextItemsFromHeader, contextItemsFromClaims);
                }
                return _usrContext;
            }
        }

    }
}
