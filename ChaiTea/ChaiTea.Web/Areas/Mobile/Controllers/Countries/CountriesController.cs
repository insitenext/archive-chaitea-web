﻿using ChaiTea.BusinessLogic.Entities.Personnel;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Services.Personnel;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Countries
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Countries")]
    public class CountriesController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<Country.CountryListItemDTO>))]
        public IHttpActionResult GetCountryListItems()
        {
            return Ok(Country.GetList());
        }
    }

    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/CountriesEx")]
    public class CountriesExController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<Country.CountryListItemExDTO>))]
        public IHttpActionResult GetCountryListItems()
        {
            return Ok(Country.GetListEx());
        }

        [HttpGet]
        [Route("{CountryCodeOrId}")]
        [ResponseType(typeof(Country.CountryListItemExDTO))]
        public IHttpActionResult GetCountryById(string countryCodeOrId)
        {
            return Ok(Country.GetCountry(countryCodeOrId));
        }
    }
}
