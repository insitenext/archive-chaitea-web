﻿using ChaiTea.BusinessLogic.Entities.Personnel;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.StickerSets
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/StickerSetMembers")]
    public class StickerSetMembersController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<SetMemberExtension.SetMemberDTO>))]
        public IHttpActionResult GetListItems()
        {
            return Ok(SetMemberExtension.GetList());
        }
    }
}
