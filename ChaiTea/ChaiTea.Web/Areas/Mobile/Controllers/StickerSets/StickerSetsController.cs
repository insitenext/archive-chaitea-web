﻿using ChaiTea.BusinessLogic.Entities.Personnel;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.StickerSets
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/StickerSets")]
    public class StickerSetsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<StickerSetExtension.StickerSetDTO>))]
        public IHttpActionResult GetListItems()
        {
            return Ok(StickerSetExtension.GetList());
        }
    }
}
