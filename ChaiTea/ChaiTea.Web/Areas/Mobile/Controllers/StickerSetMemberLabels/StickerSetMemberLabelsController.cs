﻿using ChaiTea.BusinessLogic.Entities.Personnel;
using ChaiTea.Web.Identity;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.StickerSets
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/StickerSetMemberLabels")]
    public class StickerSetMemberLabelsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(MemberLabelExtensions.MemberLabelDTO))]
        public IHttpActionResult GetListItems()
        {
            return Ok(MemberLabelExtensions.GetList());
        }
    }
}
