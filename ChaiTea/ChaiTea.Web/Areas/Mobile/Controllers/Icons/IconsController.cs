﻿using ChaiTea.Web.Identity;
using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.BusinessLogic.Services.Utils.Icons;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Icons
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Icons")]
    public class IconsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<IconsService.IconCategoryDTO>))]
        public IHttpActionResult GetIcons()
        {
            return Ok(IconsService.GetList());
        }

    }
}
