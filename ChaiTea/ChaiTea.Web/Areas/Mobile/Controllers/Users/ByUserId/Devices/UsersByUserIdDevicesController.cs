﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Users.ByUserId.Devices
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Users/{UserId:int}/Devices")]
    public class UsersByUserIdDevicesController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<UserDevicesService.DetailDTO>))]
        public IHttpActionResult GetDeviceDetails(int UserId)
        {
            return Ok(new UserDevicesService().GetDeviceDetails(UserId));
        }

        [HttpGet]
        [Route("{UserDeviceId:int}")]
        [ResponseType(typeof(UserDevicesService.DetailDTO))]
        public IHttpActionResult GetDeviceDetails(int UserId, int UserDeviceId)
        {
            return Ok(new UserDevicesService().GetDeviceDetails(UserId, UserDeviceId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult LinkNewDevice(int UserId, UserDevicesService.LinkNewDeviceDTO dto)
        {
            return Ok(new UserDevicesService().LinkNewDevice(UserId, dto));
        }

        [HttpDelete]
        [Route("{UserDeviceId:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UnlinkDevice(int UserId, int UserDeviceId)
        {
            var success = new UserDevicesService().UnlinkDevice(UserId, UserDeviceId);
            if (success)
            { return Ok(); }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }
    }
}
