﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.Web.Identity;
using System.Web.Http;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Users
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Users")]
    public class UsersController : MobileApiController
    {
        /// <summary>
        /// This Function is wide open (without authentication) to show info/confirmation of User account before logging in
        /// </summary>
        [HttpGet]
        [Route("")]
        [AllowAnonymous]
        public IHttpActionResult GetUserListItems(string Username = null, string Email = null, string ExternalEmployeeId = null)
        {
            return Ok(ApplicationUserService.GetUserListItems(Username, Email, ExternalEmployeeId));
        }
    }
}
