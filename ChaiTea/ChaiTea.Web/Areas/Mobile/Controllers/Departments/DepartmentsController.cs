﻿using ChaiTea.BusinessLogic.Services.Organizations;
using ChaiTea.Web.Identity;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Departments
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Departments")]
    public class DepartmentsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<DepartmentService.DepartmentListItemDTO>))]
        public IHttpActionResult GetDepartmentListItems()
        {
            return Ok(new DepartmentService(UserCtx).GetDepartmentListItems());
        }
    }
}
