﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.Web.Identity;
using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Persons.ByPersonId.Icons
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Persons/{PersonId:int}/Icons")]
    public class PersonsByPersonIdIconsController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(int[]))]
        public IHttpActionResult GetIconsForPerson(int PersonId)
        {
            return Ok(new PersonIconsService(UserCtx).GetIconsForPerson(PersonId));
        }
        
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpsertIcons(int PersonId, int[] dto)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only update thier own Icons");
            }
            new PersonIconsService(UserCtx).UpsertIcons(PersonId, dto);
            return Ok();
        }
    }
}
