﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using ChaiTea.BusinessLogic.Entities.Personnel;
using ChaiTea.Web.Identity;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Persons.ByPersonId.Countries
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Persons/{PersonId:int}/Countries")]
    public class PersonsByPersonIdCountriesController : MobileApiController
    {
        /// <summary>
        /// Return list of country objects this person has visited.
        /// </summary>
        /// <param name="PersonId">PersonId</param>
        /// <returns>IEnumerable&lt;Country.CountryListItemExDTO&gt;</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<Country.CountryListItemExDTO>))]
        public IHttpActionResult GetListItems(int PersonId)
        {
            return Ok(new PersonCountriesService(UserCtx).GetCountriesForPerson(PersonId));
        }

        /// <summary>
        /// Update the list of countries the user has visited. Accepts a string list of country codes.
        /// </summary>
        /// <param name="PersonId">PersonId</param>
        /// <param name="dto">List of codes, i.e. "UK, "US", "FR"</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpdateCountries(int PersonId, IEnumerable<string> dto)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only update their own Countries");
            }
            new PersonCountriesService(UserCtx).UpsertCountries(PersonId, dto);
            return Ok();
        }
    }
}
