﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ChaiTea.Web.Identity;
using System;
using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Persons.ByPersonId.Stickers
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Persons/{PersonId:int}/Stickers")]
    public class PersonsByPersonIdStickersController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PersonStickersService.StickerListItemDTO))]
        public IHttpActionResult GetStickersForPerson(int PersonId)
        {
            return Ok(new PersonStickersService(UserCtx).GetStickersForPerson(PersonId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpsertStickers(int PersonId, IEnumerable<PersonStickersService.UpsertStickerDTO> dto)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only update thier own Stickers");
            }
            new PersonStickersService(UserCtx).UpsertStickers(PersonId, dto);
            return Ok();
        }
    }
}
