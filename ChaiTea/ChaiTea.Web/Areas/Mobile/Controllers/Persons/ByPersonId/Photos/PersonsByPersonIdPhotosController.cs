﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Persons.ByPersonId.Photos
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Persons/{PersonId:int}/Attachments")]
    public class PersonsByPersonIdPhotosController : MobileApiController
    {
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<PersonPhotosService.PersonPhotoListItemDTO>))]
        public IHttpActionResult GetListItems(int PersonId)
        {
            return Ok(new PersonPhotosService(UserCtx).GetPhotos(PersonId));
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(int))]
        public IHttpActionResult UploadPhoto(int PersonId, PersonPhotosService.UploadPhotoDTO dto)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only upload thier own Photos");
            }

            return Ok(new PersonPhotosService(UserCtx).UploadPhoto(PersonId, dto));
        }

        [HttpDelete]
        [Route("{FullSizeUniqueId}")]
        [ResponseType(typeof(int))]
        public IHttpActionResult DeletePhoto(int PersonId, string FullSizeUniqueId)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only delete thier own Photos");
            }
            var success = new PersonPhotosService(UserCtx).DeletePhoto(PersonId, FullSizeUniqueId);
            if (success)
            {
                return Ok();
            }
            return ResponseMessage(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotModified));
        }
    }
}
