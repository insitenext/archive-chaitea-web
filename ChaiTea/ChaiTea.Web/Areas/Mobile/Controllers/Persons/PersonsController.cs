﻿using ChaiTea.BusinessLogic.Services.AppMetaData;
using ChaiTea.BusinessLogic.Services.Personnel;
using ChaiTea.Web.Identity;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ChaiTea.Web.Areas.Mobile.Controllers.Persons
{
    [AuthorizedRolesApi]
    [RoutePrefix(MobileApiConfig.UrlPrefix + "/Persons")]
    public class PersonsController : MobileApiController
    {
        [HttpGet]
        [Route("{PersonId:int}")]
        [ResponseType(typeof(PersonsService.PersonDetailsDTO))]
        public IHttpActionResult GetPersonDetails(int PersonId)
        {
            return Ok(new PersonsService(UserCtx).GetPersonDetails(PersonId));
        }

        
        [HttpPut]
        [Route("{PersonId:int}")]
        [ResponseType(typeof(int))]
        public IHttpActionResult UpdatePerson(int PersonId, PersonsService.UpdatePersonDTO dto)
        {
            var userId = UserContextService.CurrentUserId;
            if (PersonId != userId)
            {
                throw new UnauthorizedAccessException("Person may only update thier own profile");
            }
            var success = new PersonsService(UserCtx).UpdatePerson(PersonId, dto);
            if (success)
            {
                return Ok();
            }
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.NotModified));
        }
    }
}
