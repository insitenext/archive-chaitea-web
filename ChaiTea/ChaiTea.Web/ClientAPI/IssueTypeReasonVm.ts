
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {IssueTypeVm} from './IssueTypeVm'; 
import {ReasonVm} from './ReasonVm'; 

export class IssueTypeReasonVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.IssueTypeReasonVm
    
    public IssueTypeReasonId: number = 0;
    public IssueTypeId: number = 0;
    public IssueType: IssueTypeVm = null;
    public ReasonId: number = 0;
    public Reason: ReasonVm = null;
	public constructor(init?:Partial<IssueTypeReasonVm>) {
		(<any>Object).assign(this, init);
	}
}







