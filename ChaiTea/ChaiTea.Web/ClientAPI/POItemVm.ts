
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class POItemVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.PurchaseOrder.POItemVm
    
    public POItemId: number = 0;
    public PONumber: number = 0;
    public LineNumber: number = 0;
    public Quantity: number = 0;
    public Amount: number = 0;
    public Month: number = 0;
    public Year: number = 0;
    public CommitmentDate: Date = null;
    public SBMNumber: string = null;
    public ManufacturerNumber: string = null;
    public IsGreen: boolean = false;
    public IsBillable: boolean = false;
    public DescriptionOne: string = null;
    public DescriptionTwo: string = null;
    public CompanyId: number = 0;
    public CompanyName: string = null;
    public ManufacturerId: number = 0;
    public ManufacturerName: string = null;
    public SubCommodityId: number = 0;
    public SubCommodityName: string = null;
    public CommodityId: number = 0;
    public CommodityName: string = null;
    public SiteId: number = 0;
    public ProgramId: number = 0;
    public ProgramName: string = null;
	public constructor(init?:Partial<POItemVm>) {
		(<any>Object).assign(this, init);
	}
}







