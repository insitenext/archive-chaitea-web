
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class OwnershipCustomerRepresentativeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.OwnershipCustomerRepresentativeVm
    
    public CustomerRepresentativeId: number = 0;
    public ClientId: number = 0;
    public ClientName: string = null;
    public Name: string = null;
    public Email: string = null;
    public JobDescription: string = null;
    public SourceLanguageId: number = 0;
	public constructor(init?:Partial<OwnershipCustomerRepresentativeVm>) {
		(<any>Object).assign(this, init);
	}
}







