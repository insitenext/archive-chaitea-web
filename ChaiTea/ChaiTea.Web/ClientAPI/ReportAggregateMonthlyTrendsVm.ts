
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ReportAggregateMonthlyTrendsVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportAggregateMonthlyTrendsVm
    
    public Base: number = 0;
    public AboveBase: number = 0;
    public Total: number = 0;
    public BasePercentage: number = 0;
    public AboveBasePercentage: number = 0;
	public constructor(init?:Partial<ReportAggregateMonthlyTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}







