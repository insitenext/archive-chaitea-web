
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class AllMessageStatsVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.AllMessageStatsVm
    
    public SentMessages: number = 0;
    public TotalRecipients: number = 0;
    public Opened: number = 0;
    public NotOpened: number = 0;
    public Percent: number = 0;
	public constructor(init?:Partial<AllMessageStatsVm>) {
		(<any>Object).assign(this, init);
	}
}



export class MessageStatsVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.MessageStatsVm
    
    public MessageId: number = 0;
    public CreateDate: Date = null;
	public constructor(init?:Partial<MessageStatsVm>) {
		(<any>Object).assign(this, init);
	}
}


import {UserAttachmentVm} from './UserAttachmentVm'; 

export class EmployeeMessageVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeMessageVm
    
    public EmployeeId: number = 0;
    public IsActive: boolean = false;
    public Name: string = null;
    public JobDescription: string = null;
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<EmployeeMessageVm>) {
		(<any>Object).assign(this, init);
	}
}







