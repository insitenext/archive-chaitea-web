
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\WorkOrder\WorkOrderAttachmentVm.cs

// MODIFIED by DW

import {AttachmentTypeVm} from './AttachmentTypeVm'; 

export class WorkOrderAttachmentVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.WorkOrder.WorkOrderAttachmentVm
    
    public WorkOrderAttachmentId: number = 0;
    public UniqueId: string = null;
    public Name: string = null;
    public Description: string = null;
    public WorkOrderAttachmentType: WorkOrderAttachmentTypeVm = null;
    public AttachmentType: AttachmentTypeVm = null;
    public AttachmentTypeId: number = 0;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<WorkOrderAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}








export enum WorkOrderAttachmentTypeVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.WorkOrder.WorkOrderAttachmentTypeVm
    
    Request = 1,
    Resolve = 2,
}
