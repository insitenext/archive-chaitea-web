
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\AuditProfileSectionVm.cs

import {AuditProfileVm} from './AuditProfileVm'; 

export class AuditProfileSectionVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.AuditProfileSectionVm
    
    public AuditProfileSectionId: number = 0;
    public AuditProfileId: number = 0;
    public AreaId: number = 0;
    public AreaName: string = null;
    public AreaClassificationId: number = 0;
    public AreaClassificationName: string = null;
    public AssignedEmployees: number[] = [];
    public InspectionItems: number[] = [];
    public Profile: AuditProfileVm = null;
	public constructor(init?:Partial<AuditProfileSectionVm>) {
		(<any>Object).assign(this, init);
	}
}







