
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {CustomerVm} from './CustomerVm'; 
import {InvoiceItemVm} from './InvoiceItemVm'; 

export class InvoiceVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Invoice.InvoiceVm
    
    public InvoiceId: number = 0;
    public InvoiceNumber: number = 0;
    public BilledAmount: number = 0;
    public TaxAmount: number = 0;
    public InvoiceAmount: number = 0;
    public PaidAmount: number = 0;
    public OutstandingAmount: number = 0;
    public MonthOfService: string = null;
    public MonthOfServiceDate: Date = null;
    public InvoiceDate: Date = null;
    public DueDate: Date = null;
    public PO: string = null;
    public Base: number = 0;
    public AboveBase: number = 0;
    public PaymentTermsTypeId: number = 0;
    public PaymentTermsTypeName: string = null;
    public CustomerId: number = 0;
    public Customer: CustomerVm = null;
    public OrgId: number = 0;
    public InvoiceItems: InvoiceItemVm[] = [];
    public CreateById: number = 0;
    public CreateDate: Date = null;
    public UpdateById: number = 0;
    public UpdateDate: Date = null;
	public constructor(init?:Partial<InvoiceVm>) {
		(<any>Object).assign(this, init);
	}
}







