
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class DashboardVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.DashboardVm
    
    public ToDoCount: number = 0;
    public PastDueCount: number = 0;
    public SafetyCount: number = 0;
    public TotalEmployees: number = 0;
    public ProfileCompleteCount: number = 0;
    public PositionProfilePercentage: number = 0;
    public ToDoClosureRate: number = 0;
    public MessageNotifiedCount: number = 0;
    public LastSafetyDate: Date = null;
    public LastComplaintDate: Date = null;
    public LastAuditDate: Date = null;
    public LastToDoDate: Date = null;
    public LastTeamHoursDate: Date = null;
    public LastToDoUpdateDate: Date = null;
    public ToDoOldestIncompleteDate: Date = null;
    public LastTodoCloseOnTimeDate: Date = null;
    public LastMessageNotifiedCreateDate: Date = null;
	public constructor(init?:Partial<DashboardVm>) {
		(<any>Object).assign(this, init);
	}
}







