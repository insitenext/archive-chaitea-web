
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class UserHobbyVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.InsiteIdentity.UserHobbyVm
    
    public UserHobbyId: number = 0;
    public UserId: number = 0;
    public HobbyId: number = 0;
    public IsActive: boolean = false;
	public constructor(init?:Partial<UserHobbyVm>) {
		(<any>Object).assign(this, init);
	}
}







