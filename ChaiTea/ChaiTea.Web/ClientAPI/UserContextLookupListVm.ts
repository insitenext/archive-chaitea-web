
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

export class UserContextLookupListVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.UserContextLookupListVm
    
    public UserContext: UserContextVm = null;
    public Clients: any = null;
    public Sites: any = null;
    public Programs: any = null;
    public Languages: any = null;
	public constructor(init?:Partial<UserContextLookupListVm>) {
		(<any>Object).assign(this, init);
	}
}



export class BaseOrgVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.BaseOrgVm
    
    public ID: number = 0;
    public Name: string = null;
	public constructor(init?:Partial<BaseOrgVm>) {
		(<any>Object).assign(this, init);
	}
}



export class LanguageContextVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.LanguageContextVm
    
    public ID: number = 0;
    public Name: string = null;
    public Code: string = null;
	public constructor(init?:Partial<LanguageContextVm>) {
		(<any>Object).assign(this, init);
	}
}

export class UserContextVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.UserContextVm
    
    public UserId: number = 0;
    public Client: BaseOrgVm = null;
    public Site: BaseOrgVm = null;
    public Program: BaseOrgVm = null;
    public Language: LanguageContextVm = null;
    public ClientId: number = 0;
    public SiteId: number = 0;
    public ProgramId: number = 0;
    public LanguageId: number = 0;
	public constructor(init?:Partial<UserContextVm>) {
		(<any>Object).assign(this, init);
	}
}







