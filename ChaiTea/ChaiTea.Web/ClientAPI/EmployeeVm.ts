
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class EmployeeVm {
	// Fullname: ChaiTea.Modules.Tenant.Common.ViewModels.TenantEmployeeVm
    
    public EmployeeId: number = 0;
    public FirstName: string = null;
    public LastName: string = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public SiteId: number = 0;
    public SiteName: string = null;
    public UserId: number = 0;
	public constructor(init?:Partial<EmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}