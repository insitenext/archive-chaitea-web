
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\TodoItem\ToDoCountsVm.cs


export class ToDoCountsVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.TodoItem.ToDoCountsVm
    
    public Open: number = 0;
    public LastOpenDate: Date = null;
    public Closed: number = 0;
    public PastDue: number = 0;
    public OldestIncompleteDate: Date = null;
    public ClosedOnTime: number = 0;
    public LastClosedDate: Date = null;
	public constructor(init?:Partial<ToDoCountsVm>) {
		(<any>Object).assign(this, init);
	}
}







