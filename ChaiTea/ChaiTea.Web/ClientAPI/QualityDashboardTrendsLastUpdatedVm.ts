
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class QualityDashboardTrendsLastUpdatedVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.QualityDashboardTrendsLastUpdatedVm
    
    public AuditsLastUpdated: Date = null;
    public ComplimentsLastUpdated: Date = null;
    public ComplaintsLastUpdated: Date = null;
    public WorkOrdersLastUpdated: Date = null;
    public TodosLastUpdated: Date = null;
    public ReportItsLastUpdated: Date = null;
	public constructor(init?:Partial<QualityDashboardTrendsLastUpdatedVm>) {
		(<any>Object).assign(this, init);
	}
}







