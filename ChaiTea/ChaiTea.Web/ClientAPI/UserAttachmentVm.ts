
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

import {AttachmentTypeVm} from './AttachmentTypeVm'; 

export class UserAttachmentVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.UserAttachmentVm
    
    public UserAttachmentId: number = 0;
    public UniqueId: string = null;
    public Name: string = null;
    public Description: string = null;
    public UserAttachmentType: UserAttachmentTypeVm = null;
    public AttachmentType: AttachmentTypeVm = null;
    public AttachmentTypeId: number = 0;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<UserAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}








export enum UserAttachmentTypeVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.UserAttachmentTypeVm
    
    ProfilePicture = 1,
    SocialPicture = 2,
    BackgroundPicture = 3,
}
