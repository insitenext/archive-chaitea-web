
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ReportOwnershipByEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.Report.ReportOwnershipByEmployeeVm
    
    public EmployeeId: number = 0;
    public Name: string = null;
    public Total: number = 0;
    public Accepted: number = 0;
    public Rejected: number = 0;
    public Percentage: number = 0;
	public constructor(init?:Partial<ReportOwnershipByEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}







