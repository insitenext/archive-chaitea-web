
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class ConductCountsByEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.Conduct.ConductCountsByEmployeeVm
    
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public EmployeeIsActive: boolean = false;
    public UserAttachments: UserAttachmentVm[] = [];
    public WarningCount: number = 0;
    public ActionTakenCount: number = 0;
	public constructor(init?:Partial<ConductCountsByEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}







