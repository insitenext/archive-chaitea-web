
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\AuditProfileVm.cs


export class AuditProfileVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.AuditProfileVm
    
    public AuditProfileId: number = 0;
    public SiteId: number = 0;
    public SiteName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public ScoringProfileId: number = 0;
    public ScoringProfileDescription: string = null;
    public ScoringProfileMinScore: number = 0;
    public ScoringProfileMaxScore: number = 0;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public ProfileSectionCount: number = 0;
	public constructor(init?:Partial<AuditProfileVm>) {
		(<any>Object).assign(this, init);
	}
}







