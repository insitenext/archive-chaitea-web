
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class TranslatableEntityVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.TranslatableEntityVm
    
    public EntityId: number = 0;
    public Key: number = 0;
    public EntityName: string = null;
    public FieldName: string = null;
    public OriginalFieldValue: string = null;
    public TranslatedFieldValue: string = null;
	public constructor(init?:Partial<TranslatableEntityVm>) {
		(<any>Object).assign(this, init);
	}
}







