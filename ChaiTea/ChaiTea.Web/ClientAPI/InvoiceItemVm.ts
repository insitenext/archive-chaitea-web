
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class InvoiceItemVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Invoice.InvoiceItemVm
    
    public InvoiceItemId: number = 0;
    public LineNumber: number = 0;
    public Quantity: number = 0;
    public Rate: number = 0;
    public Amount: number = 0;
    public TotalAmount: number = 0;
    public IsTaxable: boolean = false;
    public IsPaid: boolean = false;
    public Description: string = null;
    public CurrencyCodeId: number = 0;
    public CurrencyCodeName: string = null;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public InvoiceId: number = 0;
    public InvoiceNumber: number = 0;
    public InvoiceDate: Date = null;
	public constructor(init?:Partial<InvoiceItemVm>) {
		(<any>Object).assign(this, init);
	}
}







