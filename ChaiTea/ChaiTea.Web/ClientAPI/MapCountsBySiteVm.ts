
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {MapCountsByMonthVm} from './MapCountsByMonthVm'; 

export class MapCountsBySiteVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.MapCountsBySiteVm
    
    public SiteId: number = 0;
    public SiteName: string = null;
    public CountsByMonth: MapCountsByMonthVm[] = [];
    public Total: number = 0;
    public TotalAverage: number = 0;
	public constructor(init?:Partial<MapCountsBySiteVm>) {
		(<any>Object).assign(this, init);
	}
}







