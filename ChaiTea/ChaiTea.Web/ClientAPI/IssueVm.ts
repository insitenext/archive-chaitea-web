
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class IssueVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.IssueVm
    
    public IssueId: number = 0;
    public DateOfOccurrence: Date = null;
    public Comment: string = null;
    public IssueTypeId: number = 0;
    public IssueTypeName: string = null;
    public ReasonId: number = 0;
    public ReasonName: string = null;
    public OrgUserId: number = 0;
    public CreateDate: Date = null;
    public IsExempt: boolean = false;
	public constructor(init?:Partial<IssueVm>) {
		(<any>Object).assign(this, init);
	}
}







