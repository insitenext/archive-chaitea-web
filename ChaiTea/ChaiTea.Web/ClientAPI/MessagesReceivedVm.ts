
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {DepartmentVm} from './DepartmentVm'; 
import {ConciseJobVm} from './TrainingEmployeeVm'; 
import {ClientSiteVm} from './ClientVm'; 

export class MessagesReceivedVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.MessagesReceived.MessagesReceivedVm
    
    public TrainingMessageId: number = 0;
    public Text: string = null;
    public Department: DepartmentVm = null;
    public Jobs: ConciseJobVm[] = [];
    public Sites: ClientSiteVm[] = [];
    public IsCustomer: boolean = false;
    public CreateById: number = 0;
    public CreateDate: Date = null;
    public Title: string = null;
    public Opened: number = 0;
    public TotalRecipients: number = 0;
	public constructor(init?:Partial<MessagesReceivedVm>) {
		(<any>Object).assign(this, init);
	}
}







