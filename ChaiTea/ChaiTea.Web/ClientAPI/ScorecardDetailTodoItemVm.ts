
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\ScorecardDetailTodoItemVm.cs

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class ScorecardDetailTodoItemVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.ScorecardDetailTodoItemVm
    
    public TodoItemId: number = 0;
    public ScorecardDetailId: number = 0;
    public OrgUserId: number = 0;
    public OrgUserName: string = null;
    public Comment: string = null;
    public DueDate: Date = null;
    public IsComplete: boolean = false;
    public IsNew: boolean = false;
    public LastSeenDate: Date = null;
    public CreateByAttachments: UserAttachmentVm[] = [];
    public CreateDate: Date = null;
    public UpdateDate: Date = null;
	public constructor(init?:Partial<ScorecardDetailTodoItemVm>) {
		(<any>Object).assign(this, init);
	}
}







