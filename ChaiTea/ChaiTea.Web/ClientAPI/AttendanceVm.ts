
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class AttendanceVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.Attendance.AttendanceVm
    
    public IssueId: number = 0;
    public DateOfOccurrence: Date = null;
    public Comment: string = null;
    public IssueTypeId: number = 0;
    public IssueTypeName: string = null;
    public ReasonId: number = 0;
    public ReasonName: string = null;
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ClientId: number = 0;
    public ClientName: string = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public IsEmployeeActive: boolean = false;
    public CreateDate: Date = null;
    public JobId: number = 0;
    public JobName: string = null;
    public CreatedByUserId: number = 0;
    public CreatedByUserName: string = null;
	public constructor(init?:Partial<AttendanceVm>) {
		(<any>Object).assign(this, init);
	}
}







