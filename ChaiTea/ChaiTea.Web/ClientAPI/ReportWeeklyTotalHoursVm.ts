
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


// MODIFIED by DW

export class ReportWeeklyTotalHoursVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportWeeklyTotalHoursVm
    
    public TotalRegularHours: number = 0;
    public TotalSTEHours: number = 0;
    public Data: WeeklyTotalHoursVm[] = [];
	public constructor(init?:Partial<ReportWeeklyTotalHoursVm>) {
		(<any>Object).assign(this, init);
	}
}



export class WeeklyTotalHoursVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.WeeklyTotalHoursVm
    
    public EmployeeId: number = 0;
    public WeeklyRegularHours: number = 0;
    public WeeklySTEHours: number = 0;
	public constructor(init?:Partial<WeeklyTotalHoursVm>) {
		(<any>Object).assign(this, init);
	}
}







