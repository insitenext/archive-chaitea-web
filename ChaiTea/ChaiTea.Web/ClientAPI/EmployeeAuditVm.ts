
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {KpiOptionWithSubVm, KpiSubOptionVm, KpiOptionVm} from './KpiVms';

export class EmployeeAuditVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeAuditVm
    
    public EmployeeAuditId: number = 0;
    public EmployeeId: number = 0;
    public IsPass: boolean = false;
    public IsEventBasedAudit: boolean = false;
    public Comment: string = null;
    public KpiOptions: KpiOptionWithSubVm[] = [];
    public CheckBoxOptions: KpiSubOptionVm[] = [];
    public CheckBoxSelections: number[] = [];
    public CreateBy: string = null;
    public CreateDate: Date = null;
    public UpdateBy: string = null;
    public UpdateDate: Date = null;
	public constructor(init?:Partial<EmployeeAuditVm>) {
		(<any>Object).assign(this, init);
	}
}

export class ExtendedEmployeeAuditVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ExtendedEmployeeAuditVm
    
    public KpiOptions: KpiOptionVm[] = [];
	public constructor(init?:Partial<ExtendedEmployeeAuditVm>) {
		(<any>Object).assign(this, init);
	}
}







