
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\MostRecentSurveyDashboardWidgetVm.cs


export class MostRecentSurveyDashboardWidgetVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.MostRecentSurveyDashboardWidgetVm
    
    public ModifiedDate: Date = null;
    public AverageScore: number = 0;
	public constructor(init?:Partial<MostRecentSurveyDashboardWidgetVm>) {
		(<any>Object).assign(this, init);
	}
}







