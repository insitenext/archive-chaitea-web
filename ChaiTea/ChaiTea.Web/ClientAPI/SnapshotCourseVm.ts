
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

export class SnapshotCourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SnapshotCourseVm
    
    public SnapshotCourseId: number = 0;
    public Name: string = null;
    public Overview: string = null;
    public DeadlineHire: number = 0;
    public DeadlineHireType: string = null;
    public RetakeAfter: number = 0;
    public RetakeAfterType: string = null;
    public OrgUserId: number = 0;
    public OrgId: number = 0;
    public CourseTypeId: number = 0;
    public SnapshotCourseQuestions: SnapshotCourseQuestionVm[] = [];
	public constructor(init?:Partial<SnapshotCourseVm>) {
		(<any>Object).assign(this, init);
	}
}

export class SnapshotCourseQuestionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SnapshotCourseQuestionVm
    
    public SnapshotCourseQuestionId: number = 0;
    public OrderPriority: number = 0;
    public Question: string = null;
    public Answer: string = null;
    public SnapshotCourseQuestionOptions: SnapshotCourseQuestionOptionVm[] = [];
    public Key: number = 0;
	public constructor(init?:Partial<SnapshotCourseQuestionVm>) {
		(<any>Object).assign(this, init);
	}
}



export class SnapshotCourseQuestionOptionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SnapshotCourseQuestionOptionVm
    
    public SnapshotCourseQuestionOptionId: number = 0;
    public Option: string = null;
    public IsAnswer: boolean = false;
    public Key: number = 0;
	public constructor(init?:Partial<SnapshotCourseQuestionOptionVm>) {
		(<any>Object).assign(this, init);
	}
}







