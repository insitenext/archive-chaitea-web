
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ReportBillingPaidVsUnpaidVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportBillingPaidVsUnpaidVm
    
    public UnpaidAmount: number = 0;
    public PaidAmount: number = 0;
    public InvoiceDate: Date = null;
    public InvoiceMonth: number = 0;
	public constructor(init?:Partial<ReportBillingPaidVsUnpaidVm>) {
		(<any>Object).assign(this, init);
	}
}







