
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {SafetyAttachmentVm} from './SafetyAttachmentVm'; 

export class SafetyVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.SafetyIncident.SafetyVm
    
    public SafetyId: number = 0;
    public EmployeeId: number = 0;
    public IsExempt: boolean = false;
    public DateOfIncident: Date = null;
    public Detail: string = null;
    public IncidentTypes: number[] = [];
    public SafetyAttachments: SafetyAttachmentVm[] = [];
	public constructor(init?:Partial<SafetyVm>) {
		(<any>Object).assign(this, init);
	}
}







