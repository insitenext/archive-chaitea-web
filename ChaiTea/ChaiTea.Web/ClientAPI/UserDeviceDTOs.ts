
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/



export class UserDeviceCreateDTO {
	// Fullname: ChaiTea.Core.Common.DTOs.UserDeviceCreateDTO
    
    public Platform: string = null;
    public DeviceToken: string = null;
	public constructor(init?:Partial<UserDeviceCreateDTO>) {
		(<any>Object).assign(this, init);
	}
}


export class UserDeviceDetailDTO {
	// Fullname: ChaiTea.Core.Common.DTOs.UserDeviceDetailDTO
    
    public UserDeviceId: number = 0;
    public DeviceEndpoint: string = null;
    public Platform: string = null;
	public constructor(init?:Partial<UserDeviceDetailDTO>) {
		(<any>Object).assign(this, init);
	}
}





