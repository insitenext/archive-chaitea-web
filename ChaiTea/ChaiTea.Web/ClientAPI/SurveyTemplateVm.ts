
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\SurveyTemplateVm.cs

import {SurveyTemplateQuestionVm} from './SurveyTemplateQuestionVm'; 

export class SurveyTemplateVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.SurveyTemplateVm
    
    public SurveyTemplateId: number = 0;
    public ClientId: number = 0;
    public ClientName: string = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public Questions: SurveyTemplateQuestionVm[] = [];
	public constructor(init?:Partial<SurveyTemplateVm>) {
		(<any>Object).assign(this, init);
	}
}







