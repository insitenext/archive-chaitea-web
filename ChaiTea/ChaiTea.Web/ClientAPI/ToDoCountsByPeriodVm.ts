
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\TodoItem\ToDoCountsByPeriodVm.cs


export class ToDoCountsByPeriodVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.TodoItem.ToDoCountsByPeriodVm
    
    public Month: number = 0;
    public Year: number = 0;
    public Closed: number = 0;
    public PastDue: number = 0;
    public ClosedOnTime: number = 0;
	public constructor(init?:Partial<ToDoCountsByPeriodVm>) {
		(<any>Object).assign(this, init);
	}
}







