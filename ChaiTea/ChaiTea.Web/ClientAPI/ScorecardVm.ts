
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\ScorecardVm.cs

import {UserAttachmentVm} from './UserAttachmentVm'; 
import {ScorecardSectionVm} from './ScorecardSectionVm'; 
import {CustomerRepresentativeVm} from './CustomerRepresentativeVm'; 

export class ScorecardVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.ScorecardVm
    
    public ScorecardId: number = 0;
    public SiteId: number = 0;
    public SiteName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public ProfileId: number = 0;
    public MinimumScore: number = 0;
    public MaximumScore: number = 0;
    public AuditorId: number = 0;
    public AuditorAttachments: UserAttachmentVm[] = [];
    public AuditorName: string = null;
    public Comment: string = null;
    public AuditDate: Date = null;
    public UpdateDate: Date = null;
    public InProgress: boolean = false;
    public IsJointAudit: boolean = false;
    public ScorecardSections: ScorecardSectionVm[] = [];
    public CustomerRepresentatives: CustomerRepresentativeVm[] = [];
    public Average: number = 0;
    public ProgramId: number = 0;
    public ProgramName: string = null;
	public constructor(init?:Partial<ScorecardVm>) {
		(<any>Object).assign(this, init);
	}
}







