
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {SnapshotCourseVm} from './SnapshotCourseVm'; 

export class SnapshotEmployeeCourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SnapshotEmployeeCourseVm
    
    public SnapshotEmployeeCourseId: number = 0;
    public OrgUserId: number = 0;
    public SnapshotCourseId: number = 0;
    public SnapshotCourse: SnapshotCourseVm = null;
    public EmployeeCourseId: number = 0;
    public CourseAttachmentId: number = 0;
    public Passed: boolean = false;
    public UniqueId: string = null;
    public Name: string = null;
    public Description: string = null;
    public CreateDate: Date = null;
    public ManagerOrgUserId: number = 0;
	public constructor(init?:Partial<SnapshotEmployeeCourseVm>) {
		(<any>Object).assign(this, init);
	}
}







