
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {ReportTypeVm} from './ReportTypeVm'; 

export class ProgramReportTypeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ProgramReportTypeVm
    
    public ProgramReportTypeId: number = 0;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public ReportTypeId: number = 0;
    public ReportType: ReportTypeVm = null;
	public constructor(init?:Partial<ProgramReportTypeVm>) {
		(<any>Object).assign(this, init);
	}
}







