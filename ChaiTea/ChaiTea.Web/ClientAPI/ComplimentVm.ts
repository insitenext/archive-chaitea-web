
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Compliment\ComplimentVm.cs

import {AccountableEmployeeVm} from './AccountableEmployeeVm'; 
import {FeedbackEmployeeVm} from './FeedbackEmployeeVm'; 

export class ComplimentVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Compliment.ComplimentVm
    
    public ComplimentId: number = 0;
    public FeedbackId: number = 0;
    public FeedbackDate: Date = null;
    public CreateDate: Date = null;
    public UpdateDate: Date = null;
    public CustomerName: string = null;
    public CustomerPhoneNumber: string = null;
    public CreateUserId: number = 0;
    public CreateUserName: string = null;
    public AccountableEmployees: number[] = [];
    public AccountableEmployeeObjects: any = [];
    public AccountableEmployeesInformation: AccountableEmployeeVm[] = [];
    public UpdateUserId: number = 0;
    public Description: string = null;
    public SourceLanguageId: number = 0;
    public BuildingId: number = 0;
    public FloorId: number = 0;
    public ProgramId: number = 0;
    public FeedbackEmployeeObjects: FeedbackEmployeeVm[] = [];
    public ProgramName: string = null;
    public ComplimentTypeId: number = 0;
    public ComplimentTypeName: string = null;
    public BuildingName: string = null;
    public SiteName: string = null;
    public SiteId: number = 0;
    public ClientId: number = 0;
	public constructor(init?:Partial<ComplimentVm>) {
		(<any>Object).assign(this, init);
	}
}

import {EmployeeVm} from './EmployeeVm'; 

export class ComplimentSiteCountVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Compliment.ComplimentSiteCountVm
    
    public SiteId: number = 0;
    public Count: number = 0;
    public RecentCompliment: ComplimentVm = null;
    public RecentComplimentEmployee: EmployeeVm = null;
	public constructor(init?:Partial<ComplimentSiteCountVm>) {
		(<any>Object).assign(this, init);
	}
}







