
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {ConductTypeVm} from './ConductTypeVm'; 
import {ConductAttachmentVm} from './ConductAttachmentVm'; 
import {UserAttachmentVm} from './UserAttachmentVm'; 

export class ConductInfoVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ConductInfoVm
    
    public ConductId: number = 0;
    public DateOfOccurrence: Date = null;
    public Reason: string = null;
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public EmployeeJobId: number = 0;
    public EmployeeJobName: string = null;
    public EmployeeIsActive: boolean = false;
    public ClientId: number = 0;
    public ClientName: string = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ConductTypes: ConductTypeVm[] = [];
    public ConductAttachments: ConductAttachmentVm[] = [];
    public CreateDate: Date = null;
    public CreatedById: number = 0;
    public CreatedByName: string = null;
    public IsExempt: boolean = false;
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<ConductInfoVm>) {
		(<any>Object).assign(this, init);
	}
}







