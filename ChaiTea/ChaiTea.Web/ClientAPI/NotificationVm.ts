
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {NotificationTypeVm} from './NotificationTypeVm'; 
import {NotificationEventVm} from './NotificationEventVm'; 
import {DepartmentVm} from './DepartmentVm'; 

export class NotificationVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.NotificationVm
    
    public NotificationId: number = 0;
    public UserId: number = 0;
    public OrgUserId: number = 0;
    public Summary: string = null;
    public Detail: string = null;
    public IsNew: boolean = false;
    public Type: NotificationTypeVm = null;
    public Event: NotificationEventVm = null;
    public Department: DepartmentVm = null;
    public Url: string = null;
    public CreateDate: Date = null;
    public CreateById: number = 0;
    public SiteId: number = 0;
    public SiteName: string = null;
    public MessageId: number = 0;
	public constructor(init?:Partial<NotificationVm>) {
		(<any>Object).assign(this, init);
	}
}







