
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {InjuryInvestigationCardsViewVm} from './InjuryInvestigationCardsViewVm'; 

export class ClaimsInvestigationCardsViewVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.ClaimsInvestigationCardsViewVm
    
    public ClaimsInvestigationId: number = 0;
    public InjuryInvestigationId: number = 0;
    public InjuryInvestigation: InjuryInvestigationCardsViewVm = null;
	public constructor(init?:Partial<ClaimsInvestigationCardsViewVm>) {
		(<any>Object).assign(this, init);
	}
}







