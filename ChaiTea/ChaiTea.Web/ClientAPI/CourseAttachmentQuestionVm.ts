
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {CourseAttachmentQuestionOptionVm} from './CourseAttachmentQuestionOptionVm'; 

export class CourseAttachmentQuestionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseAttachmentQuestionVm
    
    public CourseAttachmentQuestionId: number = 0;
    public OrderPriority: number = 0;
    public Question: string = null;
    public CourseAttachmentQuestionOptions: CourseAttachmentQuestionOptionVm[] = [];
    public Key: number = 0;
	public constructor(init?:Partial<CourseAttachmentQuestionVm>) {
		(<any>Object).assign(this, init);
	}
}







