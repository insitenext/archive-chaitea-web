
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {TrainingEmployeeVm} from './TrainingEmployeeVm'; 
import {ContractAttachmentVm} from './ContractAttachmentVm'; 

export class ContractVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ContractVm
    
    public ContractId: number = 0;
    public Employee: TrainingEmployeeVm = null;
    public ContractAttachments: ContractAttachmentVm[] = [];
    public UpdateDate: Date = null;
	public constructor(init?:Partial<ContractVm>) {
		(<any>Object).assign(this, init);
	}
}

export class JobContractVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.JobContractVm
    
    public ContractId: number = 0;
    public EmployeeId: number = 0;
    public ContractAttachments: ContractAttachmentVm[] = [];
    public UpdateDate: Date = null;
	public constructor(init?:Partial<JobContractVm>) {
		(<any>Object).assign(this, init);
	}
}







