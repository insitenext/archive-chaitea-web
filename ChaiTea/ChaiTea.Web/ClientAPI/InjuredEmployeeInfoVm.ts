
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class InjuredEmployeeInfoVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.InjuredEmployeeInfoVm
    
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public EmployeeTitle: string = null;
    public FloorId: number = 0;
    public IncidentId: string = null;
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<InjuredEmployeeInfoVm>) {
		(<any>Object).assign(this, init);
	}
}







