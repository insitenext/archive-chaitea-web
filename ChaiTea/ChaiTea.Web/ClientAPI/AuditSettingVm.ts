
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\AuditSettingVm.cs


export class AuditSettingVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.AuditSettingVm
    
    public AuditSettingId: number = 0;
    public ClientId: number = 0;
    public SiteId: number = 0;
    public ChartRoundingScale: number = 0;
    public RequireToDosInternalAudit: boolean = false;
    public ReRunAuditsInternalAudit: boolean = false;
	public constructor(init?:Partial<AuditSettingVm>) {
		(<any>Object).assign(this, init);
	}
}







