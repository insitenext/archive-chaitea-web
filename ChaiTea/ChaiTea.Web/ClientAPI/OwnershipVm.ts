
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

import {ClassificationVm} from './ClassificationVm'; 
import {OwnershipCustomerRepresentativeVm} from './OwnershipCustomerRepresentativeVm'; 
import {OwnershipAttachmentVm} from './OwnershipAttachmentVm'; 
import {UserAttachmentVm} from './UserAttachmentVm'; 

export class OwnershipVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.OwnershipVm
    
    public OwnershipId: number = 0;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public Status: StatusVm = null;
    public IsCritical: IsCriticalVm = null;
    public ReportTypeId: number = 0;
    public ReportTypeName: string = null;
    public RejectionTypeId: number = 0;
    public RejectionTypeName: string = null;
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public IsCorrectiveActionTaken: boolean = false;
    public CorrectiveActionDescription: string = null;
    public Description: string = null;
    public SourceLanguageId: number = 0;
    public StatusUpdateById: number = 0;
    public StatusUpdateByName: string = null;
    public StatusUpdateDate: Date = null;
    public CreateDate: Date = null;
    public IsExempt: boolean = false;
    public Classifications: ClassificationVm[] = [];
    public CustomerRepresentatives: OwnershipCustomerRepresentativeVm[] = [];
    public OwnershipAttachments: OwnershipAttachmentVm[] = [];
    public ReportedByAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<OwnershipVm>) {
		(<any>Object).assign(this, init);
	}
}



export class OwnershipSiteCountVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.OwnershipSiteCountVm
    
    public SiteId: number = 0;
    public Count: number = 0;
    public AcceptanceRate: number = 0;
	public constructor(init?:Partial<OwnershipSiteCountVm>) {
		(<any>Object).assign(this, init);
	}
}








export enum IsCriticalVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.IsCriticalVm
    
    NotCritical = 0,
    Critical = 1,
}

export enum StatusVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.StatusVm
    
    Rejected = 0,
    Accepted = 1,
}
