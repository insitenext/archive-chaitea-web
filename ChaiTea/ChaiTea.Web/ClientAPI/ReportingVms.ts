
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\ReportingVms.cs

// MODIFIED by DW

export class ReportFeedbackTypeOrProgramVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportFeedbackTypeOrProgramVm

	public FeedbackType: string = null;
	public Data: PieChartSliceData[] = [];
	public constructor(init?: Partial<ReportFeedbackTypeOrProgramVm>) {
		(<any>Object).assign(this, init);
	}
}

class PieChartSliceData {
	public id: number = 0;
	public name: string = '';
	public y: number = 0;
	public constructor(init?: Partial<PieChartSliceData>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportComplaintTrendsBySiteVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportComplaintTrendsBySiteVm

	public Data: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportComplaintTrendsBySiteVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportComplaintTrendsVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportComplaintTrendsVm

	public InScopeComplaintCount: number = 0;
	public OutOfScopeComplaintCount: number = 0;
	public InScopeData: number[] = [];
	public OpenInScopeData: number[] = [];
	public OpenOutOfScopeData: number[] = [];
	public RepeatInScopeData: number[] = [];
	public OutOfScopeData: number[] = [];
	public RepeatOutOfScopeData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportComplaintTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportComplimentTrendsVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportComplimentTrendsVm

	public InScopeComplaintCount: number = 0;
	public OutOfScopeComplaintCount: number = 0;
	public InScopeData: number[] = [];
	public OutOfScopeData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportComplimentTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportComplimentTrendsBySiteVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportComplimentTrendsBySiteVm

	public Data: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportComplimentTrendsBySiteVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportAuditTrendsVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditTrendsVm

	public InternalAuditData: number[] = [];
	public JointAuditData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportAuditTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportAuditsByBuildingVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditsByBuildingVm

	public BuildingId: number = 0;
	public Name: string = null;
	public ProgramId: number = 0;
	public InternalAuditScore: number = 0;
	public JointAuditScore: number = 0;
	public AverageScore: number = 0;
	public MostRecentScore: number = 0;
	public LastAuditDate: Date = null;
	public AuditorName: string = null;
	public constructor(init?: Partial<ReportAuditsByBuildingVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportAuditsByFloorVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditsByFloorVm

	public FloorId: number = 0;
	public Name: string = null;
	public ProgramId: number = 0;
	public InternalAuditScore: number = 0;
	public JointAuditScore: number = 0;
	public AverageScore: number = 0;
	public MostRecentScore: number = 0;
	public LastAuditDate: Date = null;
	public AuditorName: string = null;
	public constructor(init?: Partial<ReportAuditsByFloorVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportAuditTrendsBySiteVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditTrendsBySiteVm

	public InternalData: number[] = [];
	public Categories: string[] = [];
	public JointData: number[] = [];
	public constructor(init?: Partial<ReportAuditTrendsBySiteVm>) {
		(<any>Object).assign(this, init);
	}
}

export class ReportAuditGridByProgramVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditGridByProgramVm

	public Categories: string[] = [];
	public Data: GridRow[] = [];
	public constructor(init?: Partial<ReportAuditGridByProgramVm>) {
		(<any>Object).assign(this, init);
	}
}

interface GridRow {

	Name: string;
	TotalAuditCount: number;
	TotalAverageScore: number;
	InternalAuditCount: number;
	InternalAverageScore: number;
	JointAuditCount: number;
	JointAverageScore: number;
	LastAuditDate: Date;
}



export class GetEmployeeAuditsAvgVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.GetEmployeeAuditsAvgVm

	public EmployeeId: number = 0;
	public Name: string = null;
	public ProgramName: string = null;
	public AreaName: string = null;
	public NumberOfAreas: number = 0;
	public AverageScore: number = 0;
	public AverageScoringProfileScore: number = 0;
	public LastAuditDate: Date = null;
	public constructor(init?: Partial<GetEmployeeAuditsAvgVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportAuditTrendsByProgramVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditTrendsByProgramVm

	public InternalData: number[] = [];
	public JointData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportAuditTrendsByProgramVm>) {
		(<any>Object).assign(this, init);
	}
}



export class CategoryVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.CategoryVm

	public Id: number = 0;
	public Category: string = null;
	public constructor(init?: Partial<CategoryVm>) {
		(<any>Object).assign(this, init);
	}
}



export class AuditAndCategoryVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.AuditAndCategoryVm

	public InternalAuditData: number = 0;
	public JointAuditData: number = 0;
	public constructor(init?: Partial<AuditAndCategoryVm>) {
		(<any>Object).assign(this, init);
	}
}


export class ReportAuditTrendsByAreaClassificationVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportAuditTrendsByAreaClassificationVm

	public AuditsAndCategories: AuditAndCategoryVm[] = [];
	public constructor(init?: Partial<ReportAuditTrendsByAreaClassificationVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportWorkOrderTrendsVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportWorkOrderTrendsVm

	public OnTimeData: number[] = [];
	public LateData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportWorkOrderTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ReportWorkOrderTrendsBySiteVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportWorkOrderTrendsBySiteVm

	public OnTimeData: number[] = [];
	public LateData: number[] = [];
	public Categories: string[] = [];
	public constructor(init?: Partial<ReportWorkOrderTrendsBySiteVm>) {
		(<any>Object).assign(this, init);
	}
}


export class ReportWorkOrderTypeOrProgramVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportWorkOrderTypeOrProgramVm

	public WorkOrderType: string = null;
	public Data: PieChartSliceData[] = [];
	public constructor(init?: Partial<ReportWorkOrderTypeOrProgramVm>) {
		(<any>Object).assign(this, init);
	}
}

export class ReportWorkOrderGridByEmployeeVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.ReportWorkOrderGridByEmployeeVm

	public Data: Grid[] = [];
	public constructor(init?: Partial<ReportWorkOrderGridByEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}

interface Grid {

	EmployeeName: string;
	TotalCount: number;
	OnTimeCount: number;
	LateCount: number;
	OnTimeCompletionPercent: number;
}







