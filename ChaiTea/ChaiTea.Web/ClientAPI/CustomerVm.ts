
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class CustomerVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Invoice.CustomerVm
    
    public CustomerId: number = 0;
    public Name: string = null;
    public AddressOne: string = null;
    public AddressTwo: string = null;
    public City: string = null;
    public ZipCode: string = null;
    public Country: string = null;
	public constructor(init?:Partial<CustomerVm>) {
		(<any>Object).assign(this, init);
	}
}







