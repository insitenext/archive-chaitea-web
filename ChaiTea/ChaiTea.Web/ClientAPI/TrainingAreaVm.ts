
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class TrainingAreaVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingAreaVm
    
    public AreaId: number = 0;
    public AreaName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ClientId: number = 0;
    public ClientName: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<TrainingAreaVm>) {
		(<any>Object).assign(this, init);
	}
}







