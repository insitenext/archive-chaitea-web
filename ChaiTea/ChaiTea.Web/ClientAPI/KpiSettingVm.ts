
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class KpiSettingVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.KpiSettingVm
    
    public SiteJobProgramKpiId: number = 0;
    public KpiId: number = 0;
    public Name: string = null;
    public SiteId: number = 0;
    public JobId: number = 0;
    public ProgramId: number = 0;
    public DefaultPassingMetric: number = 0;
    public PassingMetric: number = 0;
	public constructor(init?:Partial<KpiSettingVm>) {
		(<any>Object).assign(this, init);
	}
}







