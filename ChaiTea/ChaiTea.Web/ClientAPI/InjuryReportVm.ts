
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {InjuryReportAttachmentVm} from './InjuryReportAttachmentVm'; 
import {UserAttachmentVm} from './UserAttachmentVm'; 

export class InjuryReportVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.InjuryReportVm
    
    public InjuryReportId: number = 0;
    public EmployeeId: number = 0;
    public Snapshot_EmployeeName: string = null;
    public Snapshot_EmployeeTitle: string = null;
    public DateOfInjury: Date = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ClientId: number = 0;
    public ClientName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public AreaId: number = 0;
    public AreaName: string = null;
    public InjuryClassificationId: number = 0;
    public IsWorkRelated: boolean = false;
    public WasVehicleInvolved: boolean = false;
    public WasPropertyDamaged: boolean = false;
    public StartOfShift: Date = null;
    public EndOfShift: Date = null;
    public DefectiveToolDescription: string = null;
    public WitnessDescription: string = null;
    public PPEDescription: string = null;
    public SafetyRulesFollowedDescription: string = null;
    public SafetyEquipmentProvidedDescription: string = null;
    public PreparedByEmployeeId: number = 0;
    public Snapshot_PreparedByName: string = null;
    public Snapshot_PreparedByTitle: string = null;
    public SiteManagerEmployeeId: number = 0;
    public Snapshot_SiteManagerName: string = null;
    public Snapshot_SiteManagerTitle: string = null;
    public ReportedToEmployeeId: number = 0;
    public Snapshot_ReportedToName: string = null;
    public Snapshot_ReportedToTitle: string = null;
    public DateReported: Date = null;
    public LocationDescription: string = null;
    public InjuryDescription: string = null;
    public ReportStatusId: number = 0;
    public DivisionId: number = 0;
    public RegionId: number = 0;
    public IncidentId: string = null;
    public WasMedicalTreatmentDenied: boolean = false;
    public SubInjuryClassificationId: number = 0;
    public InjuryReportAttachments: InjuryReportAttachmentVm[] = [];
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<InjuryReportVm>) {
		(<any>Object).assign(this, init);
	}
}






