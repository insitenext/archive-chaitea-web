
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {AttachmentTypeVm} from './AttachmentTypeVm'; 

export class AttributeAttachmentVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.AttributeAttachmentVm
    
    public AttributeAttachmentId: number = 0;
    public AttributeId: number = 0;
    public AttachmentId: number = 0;
    public Code: string = null;
    public AttributeValue: string = null;
    public FileName: string = null;
    public UniqueId: string = null;
    public Name: string = null;
    public Description: string = null;
    public AttachmentType: AttachmentTypeVm = null;
    public AttachmentTypeId: number = 0;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<AttributeAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}







