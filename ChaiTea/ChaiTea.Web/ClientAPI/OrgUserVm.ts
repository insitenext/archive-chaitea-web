
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class OrgUserVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.OrgUserVm
    
    public Id: number = 0;
    public Name: string = null;
    public OrgUserId: number = 0;
	public constructor(init?:Partial<OrgUserVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ExtendedOrgUserVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.ExtendedOrgUserVm
    
    public Email: string = null;
    public OrgId: number = 0;
    public Roles: string[] = [];
	public constructor(init?:Partial<ExtendedOrgUserVm>) {
		(<any>Object).assign(this, init);
	}
}







