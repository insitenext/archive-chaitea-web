
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class EmployeeBonusVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeBonusVm
    
    public EmployeeKpiMasterId: number = 0;
    public Year: number = 0;
    public Month: number = 0;
    public BonusAmount: number = 0;
    public EmployeeId: number = 0;
    public IsPass: boolean = false;
	public constructor(init?:Partial<EmployeeBonusVm>) {
		(<any>Object).assign(this, init);
	}
}







