
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {InjuryInvestigationAttachmentVm} from './InjuryInvestigationAttachmentVm'; 
import {InjuryInvestigationCorrectiveActionVm} from './InjuryInvestigationCorrectiveActionVm'; 

export class InjuryInvestigationVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.InjuryInvestigationVm
    
    public InjuryInvestigationId: number = 0;
    public InjuryReportId: number = 0;
    public RootCauseId: number = 0;
    public AdditionalRemarks: string = null;
    public RestrainingRequired: boolean = false;
    public CorrectiveActionsNecessary: boolean = false;
    public AdditionalNotes: string = null;
    public InjuryInvestigationAttachments: InjuryInvestigationAttachmentVm[] = [];
    public InjuryInvestigationCorrectiveActions: InjuryInvestigationCorrectiveActionVm[] = [];
    public PreparedByEmployeeId: number = 0;
    public Snapshot_PreparedByName: string = null;
    public Snapshot_PreparedByTitle: string = null;
	public constructor(init?:Partial<InjuryInvestigationVm>) {
		(<any>Object).assign(this, init);
	}
}







