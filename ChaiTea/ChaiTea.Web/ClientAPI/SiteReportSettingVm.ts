
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class SiteReportSettingVm {
	// Fullname: ChaiTea.Modules.Tenant.Common.ViewModels.SiteReportSettingVm
    
    public SiteReportSettingId: number = 0;
    public SiteId: number = 0;
    public ScoringProfileId: number = 0;
    public ScoringProfileMinimumScore: number = 0;
    public ScoringProfileMaximumScore: number = 0;
    public ScoringProfileGoal: number = 0;
    public ComplaintControlLimit: number = 0;
    public SurveyControlLimit: number = 0;
	public constructor(init?:Partial<SiteReportSettingVm>) {
		(<any>Object).assign(this, init);
	}
}







