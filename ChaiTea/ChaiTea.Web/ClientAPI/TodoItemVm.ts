
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\TodoItem\TodoItemVm.cs

// MODIFIED by DW

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class TodoItemVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.TodoItem.TodoItemVm
    
    public TodoItemId: number = 0;
    public OrgUserId: number = 0;
    public OrgUserName: string = null;
    public UserAttachment: UserAttachmentVm[] = [];
    public Comment: string = null;
    public SourceLanguageId: number = 0;
    public DueDate: Date = null;
    public IsComplete: boolean = false;
    public FloorId: number = 0;
    public FloorName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public ScorecardDetailId: number = 0;
    public CreateByName: string = null;
    public CreateByAttachments: UserAttachmentVm[] = [];
    public CreateDate: Date = null;
    public UpdateDate: Date = null;
    public IsEmployeeActive: boolean = false;
    public IsNew: boolean = false;
    public LastSeenDate: Date = null;
	public constructor(init?:Partial<TodoItemVm>) {
		(<any>Object).assign(this, init);
	}
}







