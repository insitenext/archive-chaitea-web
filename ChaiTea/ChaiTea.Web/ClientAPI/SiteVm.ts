
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class SiteVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.SiteVm
    
    public Id: number = 0;
    public Name: string = null;
    public Longitude: number = 0;
    public Latitude: number = 0;
    public ParentId: number = 0;
    public ChildCount: number = 0;
    public ProgramCount: number = 0;
	public constructor(init?:Partial<SiteVm>) {
		(<any>Object).assign(this, init);
	}
}







