
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\WorkOrder\WorkOrderVm.cs

import {UserAttachmentVm} from './UserAttachmentVm'; 
import {WorkOrderAttachmentVm} from './WorkOrderAttachmentVm'; 

export class WorkOrderVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.WorkOrder.WorkOrderVm
    
    public WorkOrderId: number = 0;
    public WorkOrderNumber: string = null;
    public StatusId: number = 0;
    public StatusName: string = null;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public Summary: string = null;
    public Description: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public LocationName: string = null;
    public ReportedById: number = 0;
    public ReportedByName: string = null;
    public RoomNumber: string = null;
    public ContactNumber: string = null;
    public AssignedDate: Date = null;
    public AssignedToId: number = 0;
    public AssignedToName: string = null;
    public AssignedToEmployeeAttachments: UserAttachmentVm[] = [];
    public PriorityId: number = 0;
    public PriorityName: string = null;
    public ServiceTypeId: number = 0;
    public ServiceTypeName: string = null;
    public Equipment: string = null;
    public IsProductionAffected: boolean = false;
    public IsInScope: boolean = false;
    public AllocateCost: boolean = false;
    public Cost: number = 0;
    public PoNumber: string = null;
    public IsBillable: boolean = false;
    public BillingDescription: string = null;
    public RequestImageUid: string = null;
    public ResolveImageUid: string = null;
    public WorkOrderSeriesId: number = 0;
    public Resolution: string = null;
    public RequestDueDate: Date = null;
    public DueDate: Date = null;
    public CloseDate: Date = null;
    public CreateDate: Date = null;
    public IsRecurring: boolean = false;
    public RecurrenceInfo: RecurrenceInfoVm = null;
    public WorkOrderAttachments: WorkOrderAttachmentVm[] = [];
	public constructor(init?:Partial<WorkOrderVm>) {
		(<any>Object).assign(this, init);
	}
}



export class RecurrenceInfoVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.WorkOrder.RecurrenceInfoVm
    
    public RecurrenceTypeId: number = 0;
    public RecurrenceTypeName: string = null;
    public StartDate: Date = null;
    public EndDate: Date = null;
    public DueIn: number = 0;
	public constructor(init?:Partial<RecurrenceInfoVm>) {
		(<any>Object).assign(this, init);
	}
}







