
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

export class ReportEmployeeMonthlyHoursVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportEmployeeMonthlyHoursVm
    
    public TotalRegularHours: number = 0;
    public TotalSTEHours: number = 0;
    public Data: EmployeeMonthlyHoursVm[] = [];
	public constructor(init?:Partial<ReportEmployeeMonthlyHoursVm>) {
		(<any>Object).assign(this, init);
	}
}



export class EmployeeMonthlyHoursVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.EmployeeMonthlyHoursVm
    
    public EmployeeId: number = 0;
    public MonthlyRegularHours: number = 0;
    public MonthlySTEHours: number = 0;
	public constructor(init?:Partial<EmployeeMonthlyHoursVm>) {
		(<any>Object).assign(this, init);
	}
}







