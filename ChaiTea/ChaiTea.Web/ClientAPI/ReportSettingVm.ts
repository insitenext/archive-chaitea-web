
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {ClientReportSettingVm} from './ClientReportSettingVm'; 
import {SiteReportSettingVm} from './SiteReportSettingVm'; 
import {ProgramReportSettingVm} from './ProgramReportSettingVm'; 

export class ReportSettingVm {
	// Fullname: ChaiTea.Modules.Tenant.Common.ViewModels.ReportSettingVm
    
    public ClientReportSettings: ClientReportSettingVm = null;
    public SiteReportSettings: SiteReportSettingVm = null;
    public ProgramReportSettings: ProgramReportSettingVm = null;
	public constructor(init?:Partial<ReportSettingVm>) {
		(<any>Object).assign(this, init);
	}
}







