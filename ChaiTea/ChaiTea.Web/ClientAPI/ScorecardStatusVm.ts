
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ScorecardStatusVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ScorecardStatusVm
    
    public ScorecardStatusId: number = 0;
    public SiteId: number = 0;
    public year: number = 0;
    public month: number = 0;
    public IsFinalized: boolean = false;
    public CreateDate: Date = null;
    public CreateById: number = 0;
	public constructor(init?:Partial<ScorecardStatusVm>) {
		(<any>Object).assign(this, init);
	}
}







