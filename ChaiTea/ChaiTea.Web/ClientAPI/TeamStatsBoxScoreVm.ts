
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class TeamStatsBoxScoreVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TeamStatsBoxScoreVm
    
    public AttendanceCount: number = 0;
    public ConductCount: number = 0;
    public ComplaintsCount: number = 0;
    public ComplimentCount: number = 0;
    public QualityCount: number = 0;
    public SafetyCount: number = 0;
    public OwnershipItemCount: number = 0;
    public ProfAuditsIsPassCount: boolean = false;
    public CourseCompliancePercentage: number = 0;
	public constructor(init?:Partial<TeamStatsBoxScoreVm>) {
		(<any>Object).assign(this, init);
	}
}







