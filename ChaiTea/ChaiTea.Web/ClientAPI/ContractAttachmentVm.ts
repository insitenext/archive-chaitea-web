
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {AttachmentTypeVm} from './AttachmentTypeVm'; 

export class ContractAttachmentVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ContractAttachmentVm
    
    public ContractAttachmentId: number = 0;
    public UniqueId: string = null;
    public Name: string = null;
    public Description: string = null;
    public ContractAttachmentType: ContractAttachmentTypeVm = null;
    public AttachmentType: AttachmentTypeVm = null;
    public CreateDate: Date = null;
    public UpdateDate: Date = null;
    public AttachmentTypeId: number = 0;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<ContractAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}

export enum ContractAttachmentTypeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ContractAttachmentTypeVm
    
    EmployeeSignature = 1,
    SupervisorSignature = 2,
}
