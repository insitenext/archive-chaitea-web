
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\SurveyTemplateQuestionVm.cs


export class SurveyTemplateQuestionVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.SurveyTemplateQuestionVm
    
    public SurveyTemplateQuestionId: number = 0;
    public QuestionType: string = null;
    public QuestionText: string = null;
    public Order: number = 0;
    public Required: boolean = false;
	public constructor(init?:Partial<SurveyTemplateQuestionVm>) {
		(<any>Object).assign(this, init);
	}
}







