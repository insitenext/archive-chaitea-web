
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {OrgTypeVm} from './OrgTypeVm'; 

export class OrgSettingVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.OrgSettingVm
    
    public Id: number = 0;
    public Name: string = null;
    public ParentId: number = 0;
    public Children: number[] = [];
    public Type: OrgTypeVm = null;
    public UtcOffsetInMinutes: number = 0;
    public IsDstEnabled: boolean = false;
    public ProfileImgAttachmentId: number = 0;
    public BackgroundImgAttachmentId: number = 0;
    public UtcTime: Date = null;
    public LocalTime: Date = null;
    public Timezone: string = null;
	public constructor(init?:Partial<OrgSettingVm>) {
		(<any>Object).assign(this, init);
	}
}







