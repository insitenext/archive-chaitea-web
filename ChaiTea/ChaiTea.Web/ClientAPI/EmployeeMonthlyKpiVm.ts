
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class EmployeeMonthlyKpiVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeMonthlyKpiVm
    
    public EmployeeKpiMasterId: number = 0;
    public EmployeeMonthlyKpiId: number = 0;
    public SiteJobProgramKpiId: number = 0;
    public IsPass: boolean = false;
    public Metric: number = 0;
    public Comment: string = null;
    public PositiveComment: string = null;
    public IsExempt: boolean = false;
	public constructor(init?:Partial<EmployeeMonthlyKpiVm>) {
		(<any>Object).assign(this, init);
	}
}







