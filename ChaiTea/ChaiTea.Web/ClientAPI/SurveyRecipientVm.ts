
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\SurveyRecipientVm.cs

import {SurveyVm} from './SurveyVm'; 

export class SurveyRecipientVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.SurveyRecipientVm
    
    public SurveyId: number = 0;
    public Survey: SurveyVm = null;
    public Name: string = null;
    public EmailAddress: string = null;
	public constructor(init?:Partial<SurveyRecipientVm>) {
		(<any>Object).assign(this, init);
	}
}







