
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class InjuryReportCardsViewVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.InjuryReportCardsViewVm
    
    public InjuryReportId: number = 0;
    public EmployeeId: number = 0;
    public Snapshot_EmployeeName: string = null;
    public DateOfInjury: Date = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ClientId: number = 0;
    public ClientName: string = null;
    public BuildingId: number = 0;
    public BuildingName: string = null;
    public FloorId: number = 0;
    public FloorName: string = null;
    public InjuryClassificationId: number = 0;
    public ReportStatusId: number = 0;
    public InjuryDescription: string = null;
    public IncidentId: string = null;
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<InjuryReportCardsViewVm>) {
		(<any>Object).assign(this, init);
	}
}







