
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class SubCommodityVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.PurchaseOrder.SubCommodityVm
    
    public SubCommodityId: number = 0;
    public Name: string = null;
    public Code: string = null;
    public CommodityId: number = 0;
    public CommodityName: string = null;
	public constructor(init?:Partial<SubCommodityVm>) {
		(<any>Object).assign(this, init);
	}
}







