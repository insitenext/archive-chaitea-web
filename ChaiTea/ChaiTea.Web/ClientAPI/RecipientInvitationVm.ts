
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\RecipientInvitationVm.cs

import {SurveyRecipientVm} from './SurveyRecipientVm'; 
import {InvitationResponseVm} from './InvitationResponseVm'; 

export class RecipientInvitationVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.RecipientInvitationVm
    
    public RecipientInvitationId: number = 0;
    public Recipient: SurveyRecipientVm = null;
    public Token: string = null;
    public ReportingDate: Date = null;
    public SentDate: Date = null;
    public InvitedByName: string = null;
    public InvitedByProfilePictureUniqueId: string = null;
    public AverageScore: number = 0;
    public Responses: InvitationResponseVm[] = [];
    public SubmittedDate: Date = null;
	public constructor(init?:Partial<RecipientInvitationVm>) {
		(<any>Object).assign(this, init);
	}
}







