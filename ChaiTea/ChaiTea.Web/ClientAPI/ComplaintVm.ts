
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Complaint\ComplaintVm.cs

import {AccountableEmployeesVm} from './AccountableEmployeesVm'; 
import {AccountableEmployeeVm} from './AccountableEmployeeVm'; 
import {FeedbackEmployeeVm} from './FeedbackEmployeeVm'; 

export class ComplaintVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Complaint.ComplaintVm
    
    public ComplaintId: number = 0;
    public FeedbackId: number = 0;
    public FeedbackDate: Date = null;
    public CreateDate: Date = null;
    public UpdateDate: Date = null;
    public CustomerName: string = null;
    public CustomerPhoneNumber: string = null;
    public CreateUserId: number = 0;
    public CreateUserName: string = null;
    public CreateName: string = null;
    public AccountableEmployees: AccountableEmployeesVm[] = [];
    public AccountableEmployeeObjects: any = [];
    public AccountableEmployeesInformation: AccountableEmployeeVm[] = [];
    public UpdateUserId: number = 0;
    public Description: string = null;
    public BuildingId: number = 0;
    public FloorId: number = 0;
    public ProgramId: number = 0;
    public FeedbackEmployeeObjects: FeedbackEmployeeVm[] = [];
    public IsRepeatComplaint: boolean = false;
    public Note: string = null;
    public Action: string = null;
    public SourceLanguageId: number = 0;
    public ProgramName: string = null;
    public ComplaintTypeId: number = 0;
    public ComplaintTypeName: string = null;
    public ComplaintStatusId: number = 0;
    public ClassificationId: number = 0;
    public ClassificationName: string = null;
    public PreventableStatusId: number = 0;
    public PreventableStatusName: string = null;
    public BuildingName: string = null;
    public SiteName: string = null;
    public SiteId: number = 0;
    public ClientId: number = 0;
    public CompletedDate: Date = null;
	public constructor(init?:Partial<ComplaintVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ComplaintSiteCountVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Complaint.ComplaintSiteCountVm
    
    public SiteId: number = 0;
    public Count: number = 0;
	public constructor(init?:Partial<ComplaintSiteCountVm>) {
		(<any>Object).assign(this, init);
	}
}







