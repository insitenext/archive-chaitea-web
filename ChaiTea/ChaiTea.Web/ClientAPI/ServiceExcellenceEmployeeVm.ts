
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {ConciseJobVm} from './TrainingEmployeeVm'; 
import {UserAttachmentVm} from './UserAttachmentVm'; 
import {EmployeeKpiVm} from './KpiVms'; 

export class ServiceExcellenceEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ServiceExcellenceEmployeeVm
    
    public EmployeeId: number = 0;
    public Name: string = null;
    public IsPass: boolean = false;
    public JobId: number = 0;
    public JobDescription: string = null;
    public Job: ConciseJobVm = null;
    public Month: number = 0;
    public Year: number = 0;
    public ExternalEmployeeId: string = null;
    public UpdateDate: Date = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public EmployeeKpis: EmployeeKpiVm[] = [];
    public BonusAmount: number = 0;
    public IsClosed: boolean = false;
    public HireDate: Date = null;
    public Comment: string = null;
    public EmployeeKpiMasterId: number = 0;
    public IsExempt: boolean = false;
	public constructor(init?:Partial<ServiceExcellenceEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ExtendedServiceExcellenceEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ExtendedServiceExcellenceEmployeeVm
    
    public HireDate: Date = null;
    public EmployeeComplaintsForMonth: number = 0;
    public EmployeeAuditAverage: number = 0;
    public AuditGoal: number = 0;
    public AuditProfileMax: number = 0;
    public EmployeeTrainingCompliance: number = 0;
    public TrainingGoal: number = 0;
	public constructor(init?:Partial<ExtendedServiceExcellenceEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}







