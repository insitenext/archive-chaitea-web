
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\SurveyVm.cs

import {SurveyQuestionVm} from './SurveyQuestionVm'; 
import {SurveyRecipientVm} from './SurveyRecipientVm'; 

export class SurveyVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.SurveyVm
    
    public SurveyId: number = 0;
    public ClientId: number = 0;
    public ClientName: string = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ProgramId: number = 0;
    public ProgramName: string = null;
    public Questions: SurveyQuestionVm[] = [];
    public Recipients: SurveyRecipientVm[] = [];
	public constructor(init?:Partial<SurveyVm>) {
		(<any>Object).assign(this, init);
	}
}







