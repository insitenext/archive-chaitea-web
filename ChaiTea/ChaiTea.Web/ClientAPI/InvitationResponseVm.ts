
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Surveys\InvitationResponseVm.cs


export class InvitationResponseVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Surveys.InvitationResponseVm
    
    public QuestionId: number = 0;
    public QuestionText: string = null;
    public AnswerType: string = null;
    public Value: string = null;
	public constructor(init?:Partial<InvitationResponseVm>) {
		(<any>Object).assign(this, init);
	}
}







