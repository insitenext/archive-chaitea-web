
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/







export enum NotificationTypeVm {
	// Fullname: ChaiTea.Core.Common.Enums.NotificationTypeVm
    
    Other = 1,
    Audit = 2,
    Complaint = 3,
    Compliment = 4,
    Safety = 5,
    ToDo = 6,
    Message = 7,
    Conduct = 8,
    Attendance = 9,
    Professionalism = 10,
    Training = 11,
    Ownership = 12,
    WorkOrder = 13,
}
