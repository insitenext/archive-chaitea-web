
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MANUALLY MODIFIED by DW

import {KpiLookupListOptionVm} from './KpiLookupListOptionVm'; 

export class KpiVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.KpiVm
    
    public KpiId: number = 0;
    public Name: string = null;
    public Summary: string = null;
    public Description: string = null;
    public Icon: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<KpiVm>) {
		(<any>Object).assign(this, init);
	}
}



export class KpiOptionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.KpiOptionVm
    
    public KpiOptionId: number = 0;
    public Name: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<KpiOptionVm>) {
		(<any>Object).assign(this, init);
	}
}

export class KpiOptionWithSubVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.KpiOptionWithSubVm
    
    public KpiSubOptions: KpiSubOptionVm[] = [];
	public constructor(init?:Partial<KpiOptionWithSubVm>) {
		(<any>Object).assign(this, init);
	}
}

export class KpiSubOptionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.KpiSubOptionVm
    
    public KpiSubOptionId: number = 0;
    public Name: string = null;
    public KpiOption: KpiOptionVm = null;
    public KpiName: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<KpiSubOptionVm>) {
		(<any>Object).assign(this, init);
	}
}

export class EmployeeKpiVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeKpiVm
    
    public EmployeeMonthlyKpiId: number = 0;
    public Kpi: KpiVm = null;
    public IsPass: boolean = false;
    public Metric: number = 0;
    public Comment: string = null;
    public PositiveComment: string = null;
    public ProgramName: string = null;
    public CheckBoxOptions: KpiLookupListOptionVm[] = [];
    public CheckBoxSelections: number[] = [];
    public MetaData: DataPointVm[] = [];
    public IsPassingOnData: boolean = false;
    public PassingMetric: number = 0;
    public IsExempt: boolean = false;
	public constructor(init?:Partial<EmployeeKpiVm>) {
		(<any>Object).assign(this, init);
	}
}



export class DataPointVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.DataPointVm
    
    public Label: string = null;
    public Value: string = null;
    public IsForDisplay: boolean = false;
    public DataPointType: DataPointType = null;
    public IsPass: boolean = false;
	public constructor(init?:Partial<DataPointVm>) {
		(<any>Object).assign(this, init);
	}
}

export enum DataPointType
    {
        ThresholdMetric,
        CandidateValue,
        Other
    }






