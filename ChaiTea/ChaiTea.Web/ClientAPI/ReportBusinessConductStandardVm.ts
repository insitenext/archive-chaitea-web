
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ReportBusinessConductStandardVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.ReportBusinessConductStandardVm
    
    public SiteName: string = null;
    public I9CompliancePercentage: number = 0;
    public BackgroundCheckPercentage: number = 0;
    public DrugScreenPercentage: number = 0;
    public TurnoverPercentage: number = 0;
    public ForMonth: Date = null;
	public constructor(init?:Partial<ReportBusinessConductStandardVm>) {
		(<any>Object).assign(this, init);
	}
}







