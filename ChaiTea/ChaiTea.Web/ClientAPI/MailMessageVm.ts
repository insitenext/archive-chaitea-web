
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class MailMessageVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.MailMessageVm
    
    public FromName: string = null;
    public FromEmail: string = null;
    public ToName: string = null;
    public ToEmail: string = null;
    public Subject: string = null;
    public Message: string = null;
    public Tag: string = null;
	public constructor(init?:Partial<MailMessageVm>) {
		(<any>Object).assign(this, init);
	}
}







