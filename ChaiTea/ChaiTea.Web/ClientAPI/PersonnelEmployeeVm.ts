
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

import {TrainingEmployeeJobVm} from './TrainingEmployeeVm'; 
import {UserAttachmentVm} from './UserAttachmentVm'; 
import {SupervisorVm} from './TrainingEmployeeVm'; 

export class PersonnelEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.PersonnelEmployeeVm
    
    public EmployeeId: number = 0;
    public Name: string = null;
    public PhoneNumber: string = null;
    public Email: string = null;
    public HireDate: Date = null;
    public JobDescription: TrainingEmployeeJobVm = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public SupervisorId: number = 0;
    public JobId: number = 0;
    public Supervisor: SupervisorVm = null;
    public UpdateDate: Date = null;
    public UpdatedBy: string = null;
	public constructor(init?:Partial<PersonnelEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}







