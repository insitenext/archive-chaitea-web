
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class TimeClockVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.TimeCenter.TimeClockVm
    
    public TimeClockId: number = 0;
    public EmployeeId: number = 0;
    public Day: Date = null;
    public StartTime: Date = null;
    public EndTime: Date = null;
    public RegularTime: number = 0;
    public OverTime: number = 0;
    public DoubleTime: number = 0;
    public ExtraTime: number = 0;
    public Sick: number = 0;
    public Vacation: number = 0;
    public TotalTime: number = 0;
    public StraightTimeEquivalent: number = 0;
    public TotalPay: number = 0;
    public ProgramId: number = 0;
    public CreateDate: Date = null;
	public constructor(init?:Partial<TimeClockVm>) {
		(<any>Object).assign(this, init);
	}
}







