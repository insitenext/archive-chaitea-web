
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {CourseVm} from './CourseVm'; 
import {JobVm} from './JobVm'; 
import {SiteVm} from './SiteVm'; 
import {ClientVm} from './ClientVm'; 

export class JobCourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.JobCourseVm
    
    public JobCourseId: number = 0;
    public JobId: number = 0;
    public CourseId: number = 0;
    public Course: CourseVm = null;
    public Job: JobVm = null;
    public Site: SiteVm = null;
    public Client: ClientVm = null;
	public constructor(init?:Partial<JobCourseVm>) {
		(<any>Object).assign(this, init);
	}
}







