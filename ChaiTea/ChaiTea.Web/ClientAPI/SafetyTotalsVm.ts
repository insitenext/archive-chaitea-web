
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class SafetyTotalsVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.SafetyTotalsVm
    
    public SiteId: number = 0;
    public SiteName: string = null;
    public RecordableCount: number = 0;
    public RecordableRate: number = 0;
    public DartCount: number = 0;
    public DartRate: number = 0;
    public LostTimeCount: number = 0;
    public LostTimeRate: number = 0;
    public ReportOnlyCount: number = 0;
    public FirstAidCount: number = 0;
    public DaysWithoutInjury: number = 0;
	public constructor(init?:Partial<SafetyTotalsVm>) {
		(<any>Object).assign(this, init);
	}
}







