
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {ClaimsInvestigationAttachmentVm} from './ClaimsInvestigationAttachmentVm'; 

export class ClaimsInvestigationVm {
	// Fullname: ChaiTea.Modules.Safety.Common.ViewModels.Injury.ClaimsInvestigationVm
    
    public ClaimsInvestigationId: number = 0;
    public InjuryInvestigationId: number = 0;
    public IsOnPremisesInjury: boolean = false;
    public InitialTreatmentId: number = 0;
    public MedicalTreatmentId: number = 0;
    public AreaOfBodyAffectedId: number = 0;
    public AreaOfBodyAffectedName: string = null;
    public BodyPartAffectedId: number = 0;
    public BodyPartAffectedName: string = null;
    public SideOfBodyAffectedId: number = 0;
    public NatureOfInjuryId: number = 0;
    public CauseOfInjuryCategoryId: number = 0;
    public CauseOfInjuryCategoryName: string = null;
    public SpecificCauseOfInjuryId: number = 0;
    public SpecificCauseOfInjuryName: string = null;
    public SourceOfInjuryCategoryId: number = 0;
    public SourceOfInjuryCategoryName: string = null;
    public SpecificSourceOfInjuryId: number = 0;
    public SpecificSourceOfInjuryName: string = null;
    public IsInjuryDisabling: boolean = false;
    public IsExposedToHazardousMaterials: boolean = false;
    public DidEmergencyServicesRespond: boolean = false;
    public DrugOrAlcoholTestId: number = 0;
    public IsWorkersCompFiled: boolean = false;
    public IsConsideredRecordable: boolean = false;
    public IsDartCase: boolean = false;
    public IsLostTimeCase: boolean = false;
    public WasInjuryReceivedByClaimPetition: boolean = false;
    public WasInjuryPostTerminationClaimPetition: boolean = false;
    public ModifiedDutyDays: number = 0;
    public LostTimeDays: number = 0;
    public PreparedByEmployeeId: number = 0;
    public Snapshot_PreparedByName: string = null;
    public Snapshot_PreparedByTitle: string = null;
    public ClaimsInvestigationAttachments: ClaimsInvestigationAttachmentVm[] = [];
	public constructor(init?:Partial<ClaimsInvestigationVm>) {
		(<any>Object).assign(this, init);
	}
}







