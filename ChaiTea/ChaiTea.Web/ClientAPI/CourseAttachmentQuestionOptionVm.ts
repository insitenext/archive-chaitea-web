
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class CourseAttachmentQuestionOptionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseAttachmentQuestionOptionVm
    
    public CourseAttachmentQuestionOptionId: number = 0;
    public Option: string = null;
    public IsAnswer: boolean = false;
    public Key: number = 0;
	public constructor(init?:Partial<CourseAttachmentQuestionOptionVm>) {
		(<any>Object).assign(this, init);
	}
}







