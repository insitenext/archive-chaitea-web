
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class UserNotificationSubscriptionVm {
	// Fullname: ChaiTea.Core.Common.ViewModels.UserNotificationSubscriptionVm
    
    public NotificationTypeId: number = 0;
    public NotificationEventId: number = 0;
    public IsDefault: boolean = false;
    public IsActive: boolean = false;
	public constructor(init?:Partial<UserNotificationSubscriptionVm>) {
		(<any>Object).assign(this, init);
	}
}







