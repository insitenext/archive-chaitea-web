
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

export class ReportBillingBySiteMonthlyVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportBillingBySiteMonthlyVm
    
    public SiteId: number = 0;
    public SiteName: string = null;
    public Total: number = 0;
    public Months: MonthVm[] = [];
	public constructor(init?:Partial<ReportBillingBySiteMonthlyVm>) {
		(<any>Object).assign(this, init);
	}
}



export class MonthVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.MonthVm
    
    public Month: string = null;
    public Amount: number = 0;
	public constructor(init?:Partial<MonthVm>) {
		(<any>Object).assign(this, init);
	}
}







