
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {UserAttachmentVm} from './UserAttachmentVm'; 

export class EmployeeAuditsInfoCardsVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.Professionalism.EmployeeAuditsInfoCardsVm
    
    public EmployeeAuditId: number = 0;
    public EmployeeId: number = 0;
    public EmployeeName: string = null;
    public JobId: number = 0;
    public JobName: string = null;
    public IsPass: boolean = false;
    public IsEventBasedAudit: boolean = false;
    public Comment: string = null;
    public CreateDate: Date = null;
    public SiteId: number = 0;
    public SiteName: string = null;
    public ClientId: number = 0;
    public ClientName: string = null;
    public IsEmployeeActive: boolean = false;
    public UserAttachments: UserAttachmentVm[] = [];
	public constructor(init?:Partial<EmployeeAuditsInfoCardsVm>) {
		(<any>Object).assign(this, init);
	}
}







