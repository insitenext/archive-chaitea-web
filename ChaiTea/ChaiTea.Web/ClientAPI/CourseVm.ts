
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MANUALLY MODIFIED by DW

import {OrgTypeVm} from './OrgTypeVm'; 
import {OrgVm} from './OrgVm'; 
import {TagVm} from './TagVm'; 
import {CourseTypeVm} from './CourseTypeVm'; 
import {LanguageVm} from './LanguageVm'; 
import {CourseAttachmentQuestionVm} from './CourseAttachmentQuestionVm'; 
import {AttachmentVm} from './AttachmentVm'; 

export class CourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseVm
    
    public CourseId: number = 0;
    public Name: string = null;
    public Overview: string = null;
    public DeadlineHire: number = 0;
    public DeadlineHireType: string = null;
    public RetakeAfter: number = 0;
    public RetakeAfterType: string = null;
    public OrgType: OrgTypeVm = null;
    public OrgClient: OrgVm = null;
    public OrgSite: OrgVm = null;
    public EmployeeId: number = 0;
    public EmployeeFirstName: string = null;
    public EmployeeLastName: string = null;
    public CourseAttachments: CourseAttachmentVm[] = [];
    public Tags: TagVm[] = [];
    public CourseType: CourseTypeVm = null;
    public CourseTypeId: number = 0;
    public CreateDate: Date = null;
    public IsFeatured: boolean = false;
    public AverageRating: number = 0;
	public constructor(init?:Partial<CourseVm>) {
		(<any>Object).assign(this, init);
	}
}

export class CourseAttachmentVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseAttachmentVm
    
    public CourseAttachmentId: number = 0;
    public Attachment: AttachmentVm = null;
    public AttachmentId: number = 0;
    public CourseId: number = 0;
    public CourseAttachmentType: CourseAttachmentTypeVm = null;
    public CourseAttachmentQuestions: CourseAttachmentQuestionVm[] = [];
    public CourseSubAttachments: CourseSubAttachmentVm[] = [];
    public OrderPriority: number = 0;
    public CourseAttachmentParentUniqueId: string = null;
    public LanguageId: number = 0;
    public Language: LanguageVm = null;
    public Views: number = 0;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<CourseAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}

export class CourseSubAttachmentVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseSubAttachmentVm
    
    public CourseSubAttachmentId: number = 0;
    public CourseAttachmentId: number = 0;
    public Attachment: AttachmentVm = null;
    public AttachmentId: number = 0;
    public CourseAttachmentType: CourseAttachmentTypeVm = null;
    public OrderPriority: number = 0;
    public LanguageId: number = 0;
    public Language: LanguageVm = null;
    public ExtendedAttachmentTypeId: number = 0;
	public constructor(init?:Partial<CourseSubAttachmentVm>) {
		(<any>Object).assign(this, init);
	}
}


import {EmployeeCourseVm} from './EmployeeCourseVm'; 

export class CourseEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseEmployeeVm
    
    public HireDate: Date = null;
    public EmployeeCourses: EmployeeCourseVm[] = [];
	public constructor(init?:Partial<CourseEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}



export class CourseJobAssignVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseJobAssignVm
    
    public JobIds: number[] = [];
	public constructor(init?:Partial<CourseJobAssignVm>) {
		(<any>Object).assign(this, init);
	}
}



export class CourseEmployeeAssignVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseEmployeeAssignVm
    
    public EmployeeIds: number[] = [];
	public constructor(init?:Partial<CourseEmployeeAssignVm>) {
		(<any>Object).assign(this, init);
	}
}








export enum CourseAttachmentTypeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseAttachmentTypeVm
    
    Content = 1,
    Cover = 2,
}
