
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

import {EmployeeCourseExtendedVm} from './EmployeeCourseVm'; 

export class PersonnelBoxScoreVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.PersonnelBoxScoreVm
    
    public AttendanceCount: number = 0;
    public ConductCount: number = 0;
    public ComplaintsCount: number = 0;
    public ComplimentCount: number = 0;
    public QualityCount: number = 0;
    public SafetyCount: number = 0;
    public OwnershipItemCount: number = 0;
    public ProfAuditsIsPassCount: number = 0;
    public Trainings: EmployeeCourseExtendedVm[] = [];
	public constructor(init?:Partial<PersonnelBoxScoreVm>) {
		(<any>Object).assign(this, init);
	}
}



export class BoxScoreAllCountsVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.BoxScoreAllCountsVm
    
    public Attendance: number = 0;
    public Conduct: number = 0;
    public Complaints: number = 0;
    public Compliments: number = 0;
    public Safety: number = 0;
    public Ownership: number = 0;
    public Professionalism: number = 0;
    public QualityAverage: number = 0;
    public Training: number = 0;
    public TotalEmployees: number = 0;
    public ProfileComplete: number = 0;
	public constructor(init?:Partial<BoxScoreAllCountsVm>) {
		(<any>Object).assign(this, init);
	}
}







