
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class VacationRequestVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.TimeCenter.VacationRequestVm
    
    public VacationRequestId: number = 0;
    public EmployeeId: number = 0;
    public StartDate: Date = null;
    public EndDate: Date = null;
    public Hours: number = 0;
    public IsApproved: boolean = false;
    public VacationTypeId: number = 0;
    public VacationTypeName: string = null;
	public constructor(init?:Partial<VacationRequestVm>) {
		(<any>Object).assign(this, init);
	}
}







