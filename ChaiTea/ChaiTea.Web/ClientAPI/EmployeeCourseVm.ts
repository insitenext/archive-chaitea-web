
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/

// MODIFIED by DW

import {CourseVm,CourseEmployeeVm} from './CourseVm'; 
import {SnapshotEmployeeCourseVm} from './SnapshotEmployeeCourseVm'; 

export class EmployeeCourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeCourseVm
    
    public EmployeeCourseId: number = 0;
    public OrgUserId: number = 0;
    public Employee: CourseEmployeeVm = null;
    public CourseId: number = 0;
    public Course: CourseVm = null;
    public IsCompleted: boolean = false;
    public CreateDate: Date = null;
    public DueDate: Date = null;
	public constructor(init?:Partial<EmployeeCourseVm>) {
		(<any>Object).assign(this, init);
	}
}

export class EmployeeCourseExtendedVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeCourseExtendedVm
    
    public EmployeeCourseId: number = 0;
    public OrgUserId: number = 0;
    public Employee: CourseEmployeeVm = null;
    public CourseId: number = 0;
    public Course: CourseVm = null;
    public IsCompleted: boolean = false;
    public SnapshotEmployeeCourses: SnapshotEmployeeCourseVm[] = [];
    public CreateDate: Date = null;
    public CreateById: number = 0;
    public DueDate: Date = null;
	public constructor(init?:Partial<EmployeeCourseExtendedVm>) {
		(<any>Object).assign(this, init);
	}
}

export class TrainingEmployeeCourseExtendedVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingEmployeeCourseExtendedVm
    
    public EmployeeCourseId: number = 0;
    public OrgUserId: number = 0;
    public Employee: CourseEmployeeVm = null;
    public CourseId: number = 0;
    public Course: CourseVm = null;
    public IsCompleted: boolean = false;
    public SnapshotEmployeeCourses: SnapshotEmployeeCourseVm[] = [];
    public CreateDate: Date = null;
    public CreateById: number = 0;
    public DueDate: Date = null;
    public IsCompliant: boolean = false;
    public IsSelfInitiated: boolean = false;
    public TotalCompleted: number = 0;
    public CompletedDate: Date = null;
	public constructor(init?:Partial<TrainingEmployeeCourseExtendedVm>) {
		(<any>Object).assign(this, init);
	}
}



export class CourseComplianceStatsVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.CourseComplianceStatsVm
    
    public IsCompleted: boolean = false;
    public TotalCompleted: number = 0;
    public CompletedDate: Date = null;
	public constructor(init?:Partial<CourseComplianceStatsVm>) {
		(<any>Object).assign(this, init);
	}
}








export enum EmployeeCourseExceptionTypeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.EmployeeCourseExceptionTypeVm
    
    Add = 1,
    Remove = 1,
}
