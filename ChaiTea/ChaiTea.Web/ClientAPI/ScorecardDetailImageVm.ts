
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
// More info: http://frhagn.github.io/Typewriter/

// Original File: C:\Users\dreadbeard\Projects\chaitea-web\ChaiTea\ChaiTea.Modules.Quality.Common\ViewModels\Audit\ScorecardDetailImageVm.cs


export class ScorecardDetailImageVm {
	// Fullname: ChaiTea.Modules.Quality.Common.ViewModels.Audit.ScorecardDetailImageVm
    
    public ImageId: number = 0;
    public PublicId: string = null;
    public ScorecardDetailId: number = 0;
    public Comment: string = null;
	public constructor(init?:Partial<ScorecardDetailImageVm>) {
		(<any>Object).assign(this, init);
	}
}







