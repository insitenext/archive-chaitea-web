// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


import {UserAttachmentVm} from './UserAttachmentVm'; 
import {EmployeeAttributeVm} from './EmployeeAttributeVm'; 
import { EquipmentVm } from './EquipmentVm';
import { JobContractVm } from './ContractVm';
import { EmployeeCourseExtendedVm } from './EmployeeCourseVm';

export class TrainingEmployeeVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingEmployeeVm
    
    public EmployeeId: number = 0;
    public Name: string = null;
    public Phone: string = null;
    public JobDescription: ConciseJobVm = null;
    public HireDate: Date = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public EmployeeTasks: JobTaskVm[] = [];
    public EmployeeEquipment: EquipmentVm[] = [];
    public AreaCount: number = 0;
    public SupervisorId: number = 0;
    public SupervisorPhoneNumber: string = null;
    public SupervisorEmail: string = null;
    public Email: string = null;
    public PhoneNumber: string = null;
    public FunFacts: string = null;
    public ExternalEmployeeId: string = null;
    public EmployeeAttributes: EmployeeAttributeVm[] = [];
	public constructor(init?:Partial<TrainingEmployeeVm>) {
		(<any>Object).assign(this, init);
	}
}


export class TrainingEmployeeAndJobVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingEmployeeAndJobVm
    
    public EmployeeId: number = 0;
    public Name: string = null;
    public PhoneNumber: string = null;
    public Email: string = null;
    public FunFacts: string = null;
    public HireDate: Date = null;
    public JobDescription: TrainingEmployeeJobVm = null;
    public EmployeeTasks: JobTaskVm[] = [];
    public EmployeeEquipment: EquipmentVm[] = [];
    public UserAttachments: UserAttachmentVm[] = [];
    public SupervisorId: number = 0;
    public JobId: number = 0;
    public Supervisor: SupervisorVm = null;
    public JobContract: JobContractVm = null;
    public UpdateDate: Date = null;
    public UpdatedBy: string = null;
	public constructor(init?:Partial<TrainingEmployeeAndJobVm>) {
		(<any>Object).assign(this, init);
	}
}

export class TrainingEmployeeAndCourseVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingEmployeeAndCourseVm
    
    public OrgUserId: number = 0;
    public Name: string = null;
    public Email: string = null;
    public PhoneNumber: string = null;
    public HireDate: Date = null;
    public UserAttachments: UserAttachmentVm[] = [];
    public JobDescription: ConciseJobVm = null;
    public EmployeeCourses: EmployeeCourseExtendedVm[] = [];
	public constructor(init?:Partial<TrainingEmployeeAndCourseVm>) {
		(<any>Object).assign(this, init);
	}
}

export class SupervisorVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SupervisorVm
    
    public SupervisorId: number = 0;
    public Name: string = null;
    public JobDescription: string = null;
    public PhoneNumber: string = null;
    public Email: string = null;
    public ProfilePictureUniqueId: string = null;
    public Manager: ManagerVm = null;
	public constructor(init?:Partial<SupervisorVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ManagerVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ManagerVm
    
    public ManagerId: number = 0;
    public Name: string = null;
    public JobDescription: string = null;
    public PhoneNumber: string = null;
    public Email: string = null;
    public ProfilePictureUniqueId: string = null;
	public constructor(init?:Partial<ManagerVm>) {
		(<any>Object).assign(this, init);
	}
}

export class TrainingEmployeeJobVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.TrainingEmployeeJobVm
    
    public JobId: number = 0;
    public Name: string = null;
    public Abbreviation: string = null;
    public Summary: string = null;
    public Purpose: string = null;
    public Layer: string = null;
    public ReportsTo: string = null;
    public Department: string = null;
    public ExemptionTypeId: number = 0;
    public PreparedBy: string = null;
    public ApprovedBy: string = null;
    public ApprovedDate: Date = null;
    public ExternalJobCode: string = null;
    public JobStep: string = null;
    public EssentialDutiesSummaryLabel: string = null;
    public EssentialDutiesAndResponsibilitiesLabel: string = null;
    public ExampleOfTasksLabel: string = null;
    public DutySummary: string = null;
    public SampleTaskSummary: string = null;
    public LegalDescriptionSummary: string = null;
    public WorkEnvironment: string = null;
    public UpdateDate: Date = null;
    public UpdatedBy: string = null;
    public JobDuties: DutyVm[] = [];
    public SampleTasks: SampleTaskVm[] = [];
    public LegalDescriptions: LegalDescriptionVm[] = [];
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<TrainingEmployeeJobVm>) {
		(<any>Object).assign(this, init);
	}
}



export class LegalDescriptionVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.LegalDescriptionVm
    
    public LegalDescriptionId: number = 0;
    public Name: string = null;
    public Description: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<LegalDescriptionVm>) {
		(<any>Object).assign(this, init);
	}
}



export class JobTaskVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.JobTaskVm
    
    public JobTaskId: number = 0;
    public Name: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<JobTaskVm>) {
		(<any>Object).assign(this, init);
	}
}



export class ConciseJobVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.ConciseJobVm
    
    public JobId: number = 0;
    public Name: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<ConciseJobVm>) {
		(<any>Object).assign(this, init);
	}
}



export class SampleTaskVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.SampleTaskVm
    
    public SampleTaskId: number = 0;
    public Name: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<SampleTaskVm>) {
		(<any>Object).assign(this, init);
	}
}



export class DutyVm {
	// Fullname: ChaiTea.Modules.Training.Common.ViewModels.DutyVm
    
    public DutyId: number = 0;
    public Description: string = null;
    public Key: number = 0;
    public EntityName: string = null;
	public constructor(init?:Partial<DutyVm>) {
		(<any>Object).assign(this, init);
	}
}







