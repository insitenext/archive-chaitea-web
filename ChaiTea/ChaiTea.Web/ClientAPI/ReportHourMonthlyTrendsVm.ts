
// $Classes/Enums/Interfaces(filter)[template][separator]
// filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
// template: The template to repeat for each matched item
// separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

// More info: http://frhagn.github.io/Typewriter/


export class ReportHourMonthlyTrendsVm {
	// Fullname: ChaiTea.Modules.Financials.Common.ViewModels.Report.ReportHourMonthlyTrendsVm
    
    public RegularHours: number[] = [];
    public STEHours: number[] = [];
    public OverTimeHours: number[] = [];
    public DoubleTimeHours: number[] = [];
    public Labels: string[] = [];
    public TotalRegularHours: number = 0;
    public TotalSTEHours: number = 0;
    public TotalOverTimeHours: number = 0;
    public TotalDoubleTimeHours: number = 0;
    public TotalHours: number = 0;
	public constructor(init?:Partial<ReportHourMonthlyTrendsVm>) {
		(<any>Object).assign(this, init);
	}
}







