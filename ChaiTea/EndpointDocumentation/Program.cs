﻿using ChaiTea.BusinessLogic.AppUtils.Abstract;
using ChaiTea.Web.Areas.Services.Controllers;
using ChaiTea.Web.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;

namespace ChaiTea.Web.EndpointDocumentation
{
    public class Program
    {
        private static readonly string[] MethodsToIgnore = new String[]
        {
            "Equals",
            "Dispose",
            "ExecuteAsync",
            "get_ActionContext",
            "get_Configuration",
            "get_ControllerContext",
            "get_Hub",
            "get_ModelState",
            "get_Request",
            "get_RequestContext",
            "get_Url",
            "get_User",
            "GetHashCode",
            "GetType",
            "set_ActionContext",
            "set_Configuration",
            "set_ControllerContext",
            "set_Request",
            "set_RequestContext",
            "set_Url",
            "set_User",
            "ToString",
            "Validate"
        };

        private enum RecomendedItemCategory
        {
            Add_Authorization,
            Add_Properties_To_Return_Type,
            Add_Return_Type,
            Specify_HttpMethod,
            Service_Should_Be_Private,
            Endpoint_Missing_Prefix,
            Shorten_Endpoint_Path,
            General_Exception
        };

        public static void Main(string[] args)
        {
            Dictionary<RecomendedItemCategory, List<String> > RecommendedItems = new Dictionary<RecomendedItemCategory, List<String>>();
            foreach(var v in Enum.GetNames(typeof(RecomendedItemCategory)))
            {
                RecomendedItemCategory e;
                Enum.TryParse(v, out e);
                RecommendedItems.Add(e, new List<string>());
            }

            Dictionary<Type, List<String>> EndpointsByReturnType = new Dictionary<Type, List<String>>();
            Dictionary<String, List<String>> EndpointsByHttpMethod = new Dictionary<String, List<String>>();
            Dictionary<Type, List<String>> EndpointsByAffectingType = new Dictionary<Type, List<String>>();
            Dictionary<String, List<String>> EndpointsByAffectingPropertyName = new Dictionary<String, List<String>>();

            string ServicesNamespace = "ChaiTea.Web.Areas.Services";
            List<Type> allApiControllers = typeof(BaseApiController)
                                                    .Assembly
                                                    .GetTypes()
                                                    .Where(t => t.IsSubclassOf(typeof(IHttpController)) || t.GetInterfaces().Contains(typeof(IHttpController)))
                                                    .Where(t => !t.IsInterface)
                                                    .Where(t => !t.IsAbstract)
                                                    .Where(t => t.Namespace.StartsWith(ServicesNamespace) || t.Namespace.StartsWith("ChaiTea.Web.Areas.Mobile"))
                                                    .OrderBy(t => t.FullName)
                                                    .ToList();
            var outputDirectory = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Endpoints");
            if(Directory.Exists(outputDirectory))
            { Directory.Delete(outputDirectory, true); }

            foreach(var controller in allApiControllers)
            {
                Console.WriteLine($"Processing {controller.Name}");
                var controllerNameForRouting = controller.Name.Replace("Controller", String.Empty);
                var controllerRoutePrefixValue = controller.GetAttributeValue((RoutePrefixAttribute rp) => rp.Prefix);

                var controllerRoutePrefixDefined = false;
                if (controllerRoutePrefixValue != null)
                { controllerRoutePrefixDefined = true; }
                else
                { controllerRoutePrefixValue = string.Empty; }

                var controllerRoles = controller.GetCustomAttributes<AuthorizedRolesApiAttribute>(true)
                                                .SelectMany(r => r.Roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                                .Distinct()
                                                .ToList();

                var controllersActions = controller.GetMethods()
                                                   .Where(method => method.IsPublic
                                                                 && !method.IsDefined(typeof(NonActionAttribute))
                                                                 && !MethodsToIgnore.Contains(method.Name)
                                                                 && !method.IsSpecialName
                                                                 && !method.IsStatic)
                                                   .OrderBy(method => method.Name);
                foreach (var action in controllersActions)
                {
                    if (action.ReturnType.IsSubClassOfGeneric(typeof(BaseContextService<>)))
                    {
                        RecommendedItems[RecomendedItemCategory.Service_Should_Be_Private].Add($"{controller.Name}/{action.Name}");
                        continue;
                    }
                    Console.WriteLine($"Processing {controller.Name} - {action.Name}");
                    var actionNameForRouting = action.Name;

                    var httpMethods = action.GetAttributeValue((HttpGetAttribute r) => r.HttpMethods.Select(m => m.Method))?.ToList() ?? new List<string>();
                    httpMethods.AddRange(action.GetAttributeValue((HttpPutAttribute r) => r.HttpMethods.Select(m => m.Method)) ?? new string[] { });
                    httpMethods.AddRange(action.GetAttributeValue((HttpPostAttribute r) => r.HttpMethods.Select(m => m.Method)) ?? new string[] { });
                    httpMethods.AddRange(action.GetAttributeValue((HttpDeleteAttribute r) => r.HttpMethods.Select(m => m.Method)) ?? new string[] { });
                    httpMethods = httpMethods.Distinct().ToList();
                    if (action.Name.StartsWith("Get", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!httpMethods.Any(t => t.Equals("GET", StringComparison.OrdinalIgnoreCase)))
                        {
                            httpMethods.Add("GET");
                            RecommendedItems[RecomendedItemCategory.Specify_HttpMethod].Add($"GET: {controller.Name}/{action.Name}");
                        }
                    }
                    else if (action.Name.StartsWith("Put", StringComparison.OrdinalIgnoreCase) ||
                             action.Name.StartsWith("Update", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!httpMethods.Any(t => t.Equals("PUT", StringComparison.OrdinalIgnoreCase)))
                        {
                            httpMethods.Add("PUT");
                            RecommendedItems[RecomendedItemCategory.Specify_HttpMethod].Add($"PUT: {controller.Name}/{action.Name}");
                        }
                    }
                    else if (action.Name.StartsWith("Post", StringComparison.OrdinalIgnoreCase) ||
                             action.Name.StartsWith("Create", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!httpMethods.Any(t => t.Equals("POST", StringComparison.OrdinalIgnoreCase)))
                        {
                            httpMethods.Add("POST");
                            RecommendedItems[RecomendedItemCategory.Specify_HttpMethod].Add($"POST: {controller.Name}/{action.Name}");
                        }
                    }
                    else if (action.Name.StartsWith("Delete", StringComparison.OrdinalIgnoreCase) ||
                             action.Name.StartsWith("Remove", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!httpMethods.Any(t => t.Equals("DELETE", StringComparison.OrdinalIgnoreCase)))
                        {
                            httpMethods.Add("DELETE");
                            RecommendedItems[RecomendedItemCategory.Specify_HttpMethod].Add($"DELETE: {controller.Name}/{action.Name}");
                        }
                    }

                    var actionRouteValue = action.GetAttributeValue((RouteAttribute r) => r.Template);
                    var actionRouteIsDefined = false;
                    if (actionRouteValue != null)
                    { actionRouteIsDefined = true; }
                    else
                    { actionRouteValue = string.Empty; }

                    if (!string.IsNullOrWhiteSpace(actionRouteValue))
                    {
                        var pathParts = actionRouteValue.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < pathParts.Length; i++)
                        {
                            var pathPart = pathParts[i];
                            if (pathPart.StartsWith("{") && pathPart.EndsWith("}"))
                            {
                                if (pathPart.Contains(":"))
                                { pathPart = pathPart.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[1].Replace("}", String.Empty); }
                                else
                                { pathPart = pathPart.Replace("{", String.Empty).Replace("}", String.Empty); }

                                if (pathPart.Contains("="))
                                { pathPart = pathPart.Substring(0, pathPart.LastIndexOf('=')); }
                            }
                            pathParts[i] = pathPart;
                        }
                        actionRouteValue = String.Join("/", pathParts);
                    }

                    var foldername = "UnknownActionRoute_" + controller.Name + "_" + action.Name;
                    if (controllerRoutePrefixDefined)
                    {
                        // Controller has a Route Prefix defined
                        if (actionRouteIsDefined) // action defines a Route also, so concat them together
                        { foldername = (controllerRoutePrefixValue + "/" + actionRouteValue); }
                        else // action does not have route defined, so use action name
                        { foldername = (controllerRoutePrefixValue + "/" + actionNameForRouting); }
                    }
                    else
                    {
                        // Controller does not have a Route Prefix defined
                        if (actionRouteIsDefined) // if the action has a route, it overrides the whole thing
                        { foldername = actionRouteValue; }
                        else // otherwise, we use the ControllerName/ActionName
                        { foldername = (controllerNameForRouting + "/" + actionNameForRouting); }
                    }
                    while (foldername.EndsWith("/"))
                    { foldername = foldername.Substring(0, foldername.Length - 1); }

                    foreach (var method in httpMethods)
                    {
                        if (!EndpointsByHttpMethod.ContainsKey(method))
                        { EndpointsByHttpMethod.Add(method, new List<string>()); }
                        if (!EndpointsByHttpMethod[method].Contains(foldername))
                        { EndpointsByHttpMethod[method].Add(foldername); }
                    }

                    var actionParameters = action.GetParameters();
                    var p = actionParameters.Count();
                    StringBuilder requestPayload = new StringBuilder();
                    StringBuilder requestFieldsAsJson = new StringBuilder();
                    StringBuilder actionSignature = new StringBuilder();
                    actionSignature.Append(action.Name + "(");
                    requestFieldsAsJson.AppendLine("{");
                    requestPayload.AppendLine("{");
                    foreach (var param in actionParameters)
                    {
                        var documentParamTypes = new List<Type>();
                        var documentPropNames = new List<String>();
                        actionSignature.Append(param.Name);
                        
                        requestPayload.Append($"\t\"{param.Name}\" : ");
                        if (param.ParameterType.IsSimpleType())
                        {
                            var pType = param.ParameterType;
                            requestPayload.Append($"\"{pType.Name}\"");
                            if (!EndpointsByAffectingType.Keys.Contains(pType))
                            { EndpointsByAffectingType.Add(pType, new List<string>()); }

                            if (!EndpointsByAffectingType[pType].Contains(foldername))
                            { EndpointsByAffectingType[pType].Add(foldername); }
                        }
                        else
                        {
                            requestPayload.AppendLine();
                            var paramPayload = param.ParameterType.GetPayload(ref documentParamTypes, "");
                            requestPayload.Append(paramPayload);

                            foreach (var t in documentParamTypes)
                            {
                                if (t.Namespace.StartsWith(nameof(ChaiTea)))
                                {
                                    if (!EndpointsByAffectingType.Keys.Contains(t))
                                    { EndpointsByAffectingType.Add(t, new List<string>()); }

                                    if (!EndpointsByAffectingType[t].Contains(foldername))
                                    { EndpointsByAffectingType[t].Add(foldername); }   
                                }
                            }
                        }

                        if (!EndpointsByAffectingPropertyName.Keys.Contains(param.Name))
                        { EndpointsByAffectingPropertyName.Add(param.Name, new List<string>()); }
                        if (EndpointsByAffectingPropertyName[param.Name].Contains(foldername))
                        { EndpointsByAffectingPropertyName[param.Name].Add(foldername); }

                        documentParamTypes = new List<Type>();
                        documentPropNames = new List<String>();
                        requestFieldsAsJson.Append($"\t\"{param.Name}\" : ");
                        var paramJson = param.ParameterType.GetFieldsAsJson(ref documentParamTypes, ref documentPropNames);
                        foreach (var t in documentParamTypes)
                        {
                            if (t.Namespace.StartsWith(nameof(ChaiTea)))
                            {
                                if (!EndpointsByAffectingType.Keys.Contains(t))
                                { EndpointsByAffectingType.Add(t, new List<string>()); }

                                if (!EndpointsByAffectingType[t].Contains(foldername))
                                { EndpointsByAffectingType[t].Add(foldername); }
                            }
                        }

                        foreach (var pN in documentPropNames)
                        {
                            if (!EndpointsByAffectingPropertyName.Keys.Contains(pN))
                            { EndpointsByAffectingPropertyName.Add(pN, new List<string>()); }

                            if (!EndpointsByAffectingPropertyName[pN].Contains(foldername))
                            { EndpointsByAffectingPropertyName[pN].Add(foldername); }
                        }
                        requestFieldsAsJson.Append(paramJson);
                        
                        if (p > 1)
                        {
                            actionSignature.Append(",");
                            requestPayload.AppendLine(",");
                            requestFieldsAsJson.AppendLine(",");
                        }
                        else
                        {
                            requestPayload.AppendLine();
                            requestFieldsAsJson.AppendLine();
                        }
                        p--;
                    }
                    actionSignature.Append(")");
                    requestPayload.Append("}");
                    requestFieldsAsJson.Append("}");

                    if (!foldername.ToLower().StartsWith(WebApiConfig.UrlPrefix.ToLower()))
                    { RecommendedItems[RecomendedItemCategory.Endpoint_Missing_Prefix].Add($"{controller.Name}/{action.Name}: {foldername}"); }
                    foldername = foldername.Replace(WebApiConfig.UrlPrefix + "/", String.Empty);

                    var folderParts = foldername.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if(folderParts.Length > 4)
                    {
                        RecommendedItems[RecomendedItemCategory.Shorten_Endpoint_Path].Add($"{controller.Name}/{action.Name}: {foldername}");
                        foldername = String.Join("/", folderParts.Take(3)) +  "/params";
                    }

                    foldername = foldername.Replace("/", "_").Replace(":", "-").Replace("?", "-n");
                    foldername = String.Join("", foldername.Split(Path.GetInvalidPathChars(), StringSplitOptions.RemoveEmptyEntries));
                    var folderpath = Path.Combine(outputDirectory, foldername);


                    var actionRoles = action.GetCustomAttributes<AuthorizedRolesApiAttribute>(true)
                                            .SelectMany(r => r.Roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                            .Distinct()
                                            .ToList()
                                            .Union(controllerRoles)
                                            .Distinct()
                                            .ToList();
                    if (actionRoles.Count <= 0)
                    { RecommendedItems[RecomendedItemCategory.Add_Authorization].Add($"{controller.Name}/{action.Name}"); }

                    var returnType = action.ReturnType;
                    if (returnType == typeof(IHttpActionResult) ||
                        returnType.IsSubclassOf(typeof(IHttpActionResult)) ||
                        returnType.GetInterfaces().Contains(typeof(IHttpActionResult)) ||
                        returnType == typeof(Task<IHttpActionResult>) ||
                        returnType.IsSubclassOf(typeof(Task<IHttpActionResult>)))
                    {
                        var returnTypeAttribute = action.GetAttributeValue((ResponseTypeAttribute r) => r.ResponseType);
                        if (returnTypeAttribute != null)
                        { returnType = returnTypeAttribute; }
                        else if (controller.IsSubClassOfGeneric(typeof(BaseCrudApiController<>)))
                        {
                            if (action.Name.Equals("Get", StringComparison.OrdinalIgnoreCase))
                            {
                                var controllerType = controller;
                                while (!controllerType.IsGenericType && controllerType.IsSubClassOfGeneric(typeof(BaseCrudApiController<>)))
                                { controllerType = controllerType.BaseType; }

                                var vm = controllerType.GetGenericArguments()?.FirstOrDefault();
                                if (vm != null)
                                { returnType = vm; }
                            }
                            else if (action.Name.Equals("Put", StringComparison.OrdinalIgnoreCase) ||
                                     action.Name.Equals("Post", StringComparison.OrdinalIgnoreCase) ||
                                     action.Name.Equals("Delete", StringComparison.OrdinalIgnoreCase))
                            { returnType = typeof(void); }
                        }
                    }

                    var returnedfieldsAsJson = $"\"Unknown\"";
                    if (returnType != null)
                    {
                        if (returnType.IsSimpleType())
                        {
                            returnedfieldsAsJson = $"\"{returnType.FullName}\"";
                        }
                        else
                        {
                            var typesExplored = new List<Type>();
                            var propertiesExplored = new List<String>();
                            var JsonFields = returnType.GetFieldsAsJson(ref typesExplored, ref propertiesExplored);
                            foreach(var t in typesExplored)
                            {
                                if (t.Namespace.StartsWith(nameof(ChaiTea)))
                                {
                                    if (!EndpointsByAffectingType.Keys.Contains(t))
                                    { EndpointsByAffectingType.Add(t, new List<string>()); }

                                    if (!EndpointsByAffectingType[t].Contains(foldername))
                                    { EndpointsByAffectingType[t].Add(foldername); }
                                }
                            }

                            foreach (var t in propertiesExplored)
                            {   
                                if (!EndpointsByAffectingPropertyName.Keys.Contains(t))
                                { EndpointsByAffectingPropertyName.Add(t, new List<string>()); }

                                if (!EndpointsByAffectingPropertyName[t].Contains(foldername))
                                { EndpointsByAffectingPropertyName[t].Add(foldername); }
                            }

                            if (string.IsNullOrWhiteSpace(JsonFields))
                            { RecommendedItems[RecomendedItemCategory.Add_Properties_To_Return_Type].Add($"Type {returnType} for {controller.Name}/{action.Name}"); }
                            else
                            { returnedfieldsAsJson = JsonFields; }
                        }
                    }
                    if (!EndpointsByReturnType.Keys.Contains(returnType))
                    { EndpointsByReturnType.Add(returnType, new List<string>()); }

                    if (!EndpointsByReturnType[returnType].Contains(foldername))
                    { EndpointsByReturnType[returnType].Add(foldername); }

                    if (returnType.Namespace.StartsWith(nameof(ChaiTea)))
                    {
                        if (!EndpointsByAffectingType.Keys.Contains(returnType))
                        { EndpointsByAffectingType.Add(returnType, new List<string>()); }

                        if (!EndpointsByAffectingType[returnType].Contains(foldername))
                        { EndpointsByAffectingType[returnType].Add(foldername); }
                    }

                    if (returnedfieldsAsJson == $"\"Unknown\"")
                    { RecommendedItems[RecomendedItemCategory.Add_Return_Type].Add($"{controller.Name}/{action.Name}"); }

                    var responsePayload = $"\"{typeof(void).FullName}\"";
                    if (returnType != null)
                    {
                        if (returnType.IsSimpleType())
                        { responsePayload = $"\"{returnType.FullName}\""; }
                        else
                        {
                            var typesExplored = new List<Type>();
                            var json = returnType.GetPayload(ref typesExplored);
                            if(!string.IsNullOrWhiteSpace(json))
                            { responsePayload = json; }
                        }
                    }

                    try
                    {
                        if (!Directory.Exists(folderpath))
                        { Directory.CreateDirectory(folderpath); }

                        folderpath = Path.Combine(folderpath, actionSignature.ToString());

                        if (!Directory.Exists(folderpath))
                        { Directory.CreateDirectory(folderpath); }

                        using (StreamWriter sw = File.CreateText(Path.Combine(folderpath, "signature.json")))
                        {
                            sw.WriteLine($"{{");
                            sw.WriteLine($"\"Controller\": \"{controller.Name}\",");
                            sw.WriteLine($"\"Action\" : \"{action.Name}\",");
                            if (httpMethods.Count > 0)
                            { sw.WriteLine($"\"HttpMethods\": {{\"{String.Join("\", \"", httpMethods)}\"}},"); }
                            else
                            { sw.WriteLine($"\"HttpMethods\": {{ }},"); }
                            sw.WriteLine($"\"Parameters\": ");
                            sw.WriteLine(requestFieldsAsJson.ToString() + ",");
                            sw.WriteLine($"\"Return Type\" : \"{returnType.FullName}\",");
                            sw.WriteLine($"\"Returned Fields\" :");
                            sw.WriteLine(returnedfieldsAsJson);
                            sw.WriteLine($"}}");
                        }

                        using (StreamWriter sw = File.CreateText(Path.Combine(folderpath, "request_payload.json")))
                        {
                            sw.WriteLine(requestPayload.ToString());
                        }

                        using (StreamWriter sw = File.CreateText(Path.Combine(folderpath, "response_payload.json")))
                        {
                            sw.WriteLine(responsePayload);
                        }

                        using (StreamWriter sw = File.CreateText(Path.Combine(folderpath, "roles.json")))
                        {
                            sw.WriteLine($"{{");
                            if (actionRoles.Count > 0)
                            { sw.WriteLine($"\t\"{String.Join("\",\"", actionRoles)}\""); }
                            sw.WriteLine($"}}");
                        }
                    }
                    catch (Exception e)
                    {
                        RecommendedItems[RecomendedItemCategory.General_Exception].Add($"{controller.Name}/{action.Name}: Message [{e.Message}]");
                    }
                }
            }

            if (RecommendedItems.Count > 0)
            {
                using (StreamWriter sw = new StreamWriter(Path.Combine(outputDirectory, "__RecomendedItems.json"), false))
                {
                    sw.WriteLine("{");
                    var first = true;
                    foreach(var category in RecommendedItems.Keys)
                    {
                        if (RecommendedItems[category].Count > 0)
                        {
                            if(!first)
                            { sw.WriteLine(","); }
                            sw.WriteLine($"\t\"{category}\" : [ ");
                            foreach (var item in RecommendedItems[category].OrderBy(t => t))
                            {
                                sw.WriteLine($"\t\t\"{item}\",");
                            }
                            sw.Write($"\t] ");
                            first = false;
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine("}");
                }
            }

            if(EndpointsByReturnType.Count > 0)
            {
                using (StreamWriter sw = new StreamWriter(Path.Combine(outputDirectory, "__EndpointsByReturnType.json"), false))
                {
                    sw.WriteLine("{");
                    var first = true;
                    foreach (var rType in EndpointsByReturnType.Keys.OrderBy(t => t.FullName))
                    {
                        if (EndpointsByReturnType[rType].Count > 0)
                        {
                            if (!first)
                            { sw.WriteLine(","); }
                            sw.WriteLine($"\t\"{rType.FullName}\" : [ ");
                            foreach (var ep in EndpointsByReturnType[rType].OrderBy(t => t))
                            {
                                sw.WriteLine($"\t\t\"{ep}\",");
                            }
                            sw.Write($"\t] ");
                            first = false;
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine("}");
                }
            }

            if (EndpointsByAffectingType.Count > 0)
            {
                using (StreamWriter sw = new StreamWriter(Path.Combine(outputDirectory, "__EndpointsByAffectingType.json"), false))
                {
                    sw.WriteLine("{");
                    var first = true;
                    foreach (var typ in EndpointsByAffectingType.Keys.OrderBy(t => t.FullName))
                    {
                        if (EndpointsByAffectingType[typ].Count > 0)
                        {
                            if (!first)
                            { sw.WriteLine(","); }
                            sw.WriteLine($"\t\"{typ.FullName}\" : [");
                            foreach (var ep in EndpointsByAffectingType[typ].OrderBy(t => t))
                            {
                                sw.WriteLine($"\t\t\"{ep}\",");
                            }
                            sw.Write($"\t] ");
                            first = false;
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine("}");
                }
            }

            if (EndpointsByAffectingPropertyName.Count > 0)
            {
                using (StreamWriter sw = new StreamWriter(Path.Combine(outputDirectory, "__EndpointsByAffectingPropertyName.json"), false))
                {
                    sw.WriteLine("{");
                    var first = true;
                    foreach (var propName in EndpointsByAffectingPropertyName.Keys.OrderBy(t => t))
                    {
                        if (EndpointsByAffectingPropertyName[propName].Count > 0)
                        {
                            if (!first)
                            { sw.WriteLine(","); }
                            sw.WriteLine($"\t \"{propName}\" : [ ");
                            foreach (var ep in EndpointsByAffectingPropertyName[propName].OrderBy(t => t))
                            {
                                sw.WriteLine($"\t\t\"{ep}\",");
                            }
                            sw.Write($"\t] ");
                            first = false;
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine("}");
                }
            }

            if (EndpointsByHttpMethod.Count > 0)
            {
                using (StreamWriter sw = new StreamWriter(Path.Combine(outputDirectory, "__EndpointsByHttpMethod.json"), false))
                {
                    sw.WriteLine("{");
                    var first = true;
                    foreach (var method in EndpointsByHttpMethod.Keys)
                    {
                        if (EndpointsByHttpMethod[method].Count > 0)
                        {
                            if (!first)
                            { sw.WriteLine(","); }
                            sw.WriteLine($"\t \"{method}\" : [ ");
                            foreach (var ep in EndpointsByHttpMethod[method].OrderBy(t => t))
                            {
                                sw.WriteLine($"\t\t\"{ep}\",");
                            }
                            sw.Write($"\t] ");
                            first = false;
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine("}");
                }
            }
            Console.WriteLine("Process Complete: <hit enter>");
            Console.ReadLine();
        }
    }
}
