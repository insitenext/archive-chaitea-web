﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ChaiTea.Web.EndpointDocumentation
{

    public static class ReflectionExtensions
    {
        public static string GetFieldsAsJson(this Type type, ref List<Type> reflectionTypesDocumented, ref List<String> propertyNamesEncountered, string tabs = null)
        {
            if (type == typeof(IHttpActionResult) ||
                type.IsSubclassOf(typeof(IHttpActionResult)) ||
                type.GetInterfaces().Contains(typeof(IHttpActionResult)) ||
                type == typeof(Task<IHttpActionResult>) ||
                type.IsSubclassOf(typeof(Task<IHttpActionResult>)))
            {
                return String.Empty;
            }

            if (reflectionTypesDocumented.Contains(type))
            {
                return $"\"{type.Name}\"";
            }
            else
            {
                reflectionTypesDocumented.Add(type);
            }

            StringBuilder sb = new StringBuilder();
            var currentTabs = tabs;
            if (currentTabs == null) // not doing check for empty string as that has separate meaning in this case
            { currentTabs = String.Empty; }
            else
            { currentTabs = currentTabs + "\t"; }

            if (type.IsEnum)
            {
                var isReallyString = (type.GetAttributeValue((JsonConverterAttribute jca) => jca.ConverterType) == (typeof(StringEnumConverter)));
                if (isReallyString)
                { return $"\"{typeof(string).Name}\""; }

                return $"\"{Enum.GetUnderlyingType(type).Name}\"";
            }
            else if (type == typeof(IEnumerable) ||
               type.IsSubclassOf(typeof(IEnumerable)) ||
               type.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                sb.AppendLine($"{currentTabs}{{");
                sb.AppendLine($"{currentTabs}\t\"Type\" : \"{type.FullName}\",");
                if (type.IsGenericType)
                {
                    reflectionTypesDocumented.Add(type);
                    var genericParams = type.GetGenericArguments();
                    int i = genericParams.Count();
                    sb.AppendLine($"{currentTabs}\t\"Containing\" :");
                    sb.AppendLine($"{currentTabs}\t[");
                    var multipleParams = (i > 1);
                    if (multipleParams)
                    { sb.AppendLine($"{currentTabs}\t{{"); }
                    foreach (var subtype in genericParams)
                    {
                        if (subtype.IsSimpleType())
                        { sb.Append($"{currentTabs}\"{subtype.Name}\""); }
                        else
                        { sb.Append(subtype.GetFieldsAsJson(ref reflectionTypesDocumented, ref propertyNamesEncountered, currentTabs + "\t")); }

                        sb.AppendLine((i > 1) ? "," : String.Empty);
                        i--;
                    }
                    if (multipleParams)
                    { sb.AppendLine($"{currentTabs}\t}}"); }
                    sb.AppendLine($"{currentTabs}\t]");
                }
                sb.AppendLine($"{currentTabs}}}");
            }
            else if(type.IsSimpleType())
            {
                var propType = type;
                var nullableIndicator = String.Empty;
                if (propType.IsSubClassOfGeneric(typeof(Nullable<>)))
                {
                    var trueType = propType.GetGenericArguments()?.FirstOrDefault();
                    if (trueType != null)
                    {
                        nullableIndicator = "?";
                        propType = trueType;
                    }
                }

                if (propType.IsEnum)
                {
                    var isReallyString = (propType.GetAttributeValue((JsonConverterAttribute jca) => jca.ConverterType) == (typeof(StringEnumConverter)));
                    if (isReallyString)
                    { sb.Append($"\"Enum[{propType.Name}{nullableIndicator}] as {typeof(string).Name}\""); }
                    else
                    { sb.Append($"\"Enum[{propType.Name}{nullableIndicator}] as {Enum.GetUnderlyingType(propType).Name}\""); }
                }
                else
                { sb.Append($"\"{propType.FullName}{nullableIndicator}\""); }
            }
            else
            {
                sb.AppendLine($"{currentTabs}{{");
                var properties = type.GetProperties()
                                     .OrderBy(p => p.Name)
                                     .ToList();
                int i = properties.Count();
                foreach (var propInfo in properties)
                {
                    // Only record properties that are not ignored in json transform
                    if (propInfo.GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null)
                    {
                        if (!propertyNamesEncountered.Contains(propInfo.Name))
                        { propertyNamesEncountered.Add(propInfo.Name); }

                        sb.AppendLine($"{currentTabs}\t\"{propInfo.Name}\" : ");
                        sb.AppendLine($"{currentTabs}\t{{");
                        var propType = propInfo.PropertyType;
                        var nullableIndicator = String.Empty;
                        if (propType.IsSubClassOfGeneric(typeof(Nullable<>)))
                        {
                            var trueType = propType.GetGenericArguments()?.FirstOrDefault();
                            if (trueType != null)
                            {
                                nullableIndicator = "?";
                                propType = trueType;
                            }
                        }

                        if (propType.IsEnum)
                        {
                            var isReallyString = (propType.GetAttributeValue((JsonConverterAttribute jca) => jca.ConverterType) == (typeof(StringEnumConverter)));
                            if (isReallyString)
                            { sb.AppendLine($"{currentTabs}\t\t\"Type\" : \"Enum[{propType.Name}{nullableIndicator}] as {typeof(string).Name}\","); }
                            else
                            { sb.AppendLine($"{currentTabs}\t\t\"Type\" : \"Enum[{propType.Name}{nullableIndicator}] as {Enum.GetUnderlyingType(propType).Name}\","); }
                        }
                        else
                        { sb.AppendLine($"{currentTabs}\t\t\"Type\" : \"{propType.FullName}{nullableIndicator}\","); }

                        if (propInfo.CanRead)
                        {
                            MethodInfo getAccessor = propInfo.GetMethod;
                            sb.Append($"{currentTabs}\t\t\"Get Visibility\" : \"{GetVisibility(getAccessor)}\"");
                        }
                        else
                        { sb.Append($"{currentTabs}\t\t\"Get Visibility\" : \"Unavailable\""); }

                        if (propInfo.CanWrite)
                        {
                            sb.AppendLine(",");
                            MethodInfo setAccessor = propInfo.SetMethod;
                            sb.Append($"{currentTabs}\t\t\"Set Visibility\" : \"{GetVisibility(setAccessor)}\"");
                        }
                        else
                        {
                            sb.AppendLine(",");
                            sb.Append($"{currentTabs}\t\t\"Set Visibility\" : \"Unavailable\"");
                        }

                        if (!propType.IsSimpleType())
                        {
                            sb.AppendLine(",");
                            sb.AppendLine($"{currentTabs}\t\t\"Nested Fields\" :");
                            if (propType == typeof(IEnumerable) ||
                                propType.IsSubclassOf(typeof(IEnumerable)) ||
                                propType.GetInterfaces().Contains(typeof(IEnumerable)))
                            {
                                reflectionTypesDocumented.Add(propType);
                                var genericParams = propType.GetGenericArguments();
                                int j = genericParams.Count();
                                sb.AppendLine($"{currentTabs}\t\t[");
                                var multipleParams = (j > 1);
                                if (multipleParams)
                                { sb.AppendLine($"{currentTabs}\t\t{{"); }
                                foreach (var subtype in genericParams)
                                {
                                    if (subtype.IsSimpleType())
                                    { sb.Append($"{currentTabs}\"{subtype.Name}\""); }
                                    else
                                    { sb.Append(subtype.GetFieldsAsJson(ref reflectionTypesDocumented, ref propertyNamesEncountered, currentTabs + "\t\t")); }

                                    sb.AppendLine((j > 1) ? "," : String.Empty);
                                    j--;
                                }
                                if (multipleParams)
                                { sb.AppendLine($"{currentTabs}\t\t}}"); }
                                sb.AppendLine($"{currentTabs}\t\t]");
                            }
                            else
                            {
                                var subFields = propType.GetFieldsAsJson(ref reflectionTypesDocumented, ref propertyNamesEncountered, currentTabs + "\t");
                                sb.AppendLine($"{subFields}");
                            }
                        }
                        else if (propType.IsEnum)
                        {
                            sb.AppendLine(",");
                            sb.AppendLine($"{currentTabs}\t\t\"Enum Values\" : ");
                            sb.AppendLine($"{currentTabs}\t\t[");
                            foreach (var v in Enum.GetValues(propType))
                            {
                                var n = Enum.GetName(propType, v);
                                var underlyingValue = Convert.ChangeType(v, Enum.GetUnderlyingType(propType));
                                sb.AppendLine($"{currentTabs}\t\t\t\"{n}\" : \"{Convert.ToString(underlyingValue)}\"");
                            }
                            sb.AppendLine($"{currentTabs}\t\t]");
                        }
                        else
                        { sb.AppendLine(); }

                        sb.Append($"{currentTabs}\t}}");
                        sb.AppendLine((i > 1 ? "," : String.Empty));
                    }
                    i--;
                }
                sb.Append($"{currentTabs}}}");
            }

            return sb.ToString();
        }


        public static string GetPayload(this Type type, ref List<Type> reflectionTypesDocumented, string tabs = null)
        {
            if (type == typeof(IHttpActionResult) ||
                type.IsSubclassOf(typeof(IHttpActionResult)) ||
                type.GetInterfaces().Contains(typeof(IHttpActionResult)) ||
                type == typeof(Task<IHttpActionResult>) ||
                type.IsSubclassOf(typeof(Task<IHttpActionResult>)))
            {
                return String.Empty;
            }

            if (reflectionTypesDocumented.Contains(type))
            {
                return $"\"{type.Name}\"";
            }
            else
            {
                reflectionTypesDocumented.Add(type);
            }

            StringBuilder sb = new StringBuilder();
            var currentTabs = tabs;
            if (currentTabs == null) // not doing check for empty string as that has separate meaning in this case
            { currentTabs = String.Empty; }
            else
            { currentTabs = currentTabs + "\t"; }

            if (type.IsEnum)
            {
                var isReallyString = (type.GetAttributeValue((JsonConverterAttribute jca) => jca.ConverterType) == (typeof(StringEnumConverter)));
                if (isReallyString)
                { return $"\"{typeof(string).Name}\""; }

                return $"\"{Enum.GetUnderlyingType(type).Name}\"";
            }
            else if (type == typeof(IEnumerable) ||
               type.IsSubclassOf(typeof(IEnumerable)) ||
               type.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                if (type.IsGenericType)
                {
                    reflectionTypesDocumented.Add(type);
                    var genericParams = type.GetGenericArguments();
                    int i = genericParams.Count();
                    sb.AppendLine($"{currentTabs}[");
                    var multipleParams = (i > 1);
                    if (multipleParams)
                    { sb.AppendLine($"{currentTabs}\t{{"); }
                    foreach (var subtype in genericParams)
                    {
                        if (subtype.IsSimpleType())
                        { sb.Append($"{currentTabs}\"{subtype.Name}\""); }
                        else
                        { sb.Append(subtype.GetPayload(ref reflectionTypesDocumented, currentTabs)); }

                        sb.AppendLine((i > 1) ? ",": String.Empty);
                        i--;
                    }
                    if (multipleParams)
                    { sb.AppendLine($"{currentTabs}\t}}"); }
                    sb.Append($"{currentTabs}]");
                }
                else
                {
                    sb.Append($"{currentTabs}[ {{ \"Unknown\": \"{typeof(void).Name}\" }} ]");
                }
            }
            else
            {
                sb.AppendLine($"{currentTabs}{{");
                var properties = type.GetProperties()
                                     //.Where(pi => pi.GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null)
                                     .OrderBy(p => p.Name)
                                     .ToList();
                int i = properties.Count();
                foreach (var propInfo in properties)
                {
                    // Only record properties that are not ignored in json transform
                    if (propInfo.GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null)
                    {
                        sb.Append($"{currentTabs}\t\"{propInfo.Name}\" : ");
                        var propType = propInfo.PropertyType;
                        var nullableIndicator = String.Empty;
                        if (propType.IsSubClassOfGeneric(typeof(Nullable<>)))
                        {
                            var trueType = propType.GetGenericArguments()?.FirstOrDefault();
                            if (trueType != null)
                            {
                                nullableIndicator = "?";
                                propType = trueType;
                            }
                        }

                        if (!propType.IsSimpleType())
                        {
                            sb.AppendLine();
                            var subFields = propType.GetPayload(ref reflectionTypesDocumented, currentTabs);
                            sb.Append($"{subFields}");
                        }
                        else if (propType.IsEnum)
                        {
                            var isReallyString = (propType.GetAttributeValue((JsonConverterAttribute jca) => jca.ConverterType) == (typeof(StringEnumConverter)));
                            if (isReallyString)
                            { sb.Append($"\"{typeof(string).Name}\""); }
                            else
                            { sb.Append($"\"{Enum.GetUnderlyingType(propType).Name}\""); }
                        }
                        else
                        {
                            sb.Append($"\"{propType.Name + nullableIndicator}\"");
                        }
                        sb.AppendLine((i > 1 ? ",": String.Empty));
                    }
                    i--;
                }
                sb.Append($"{currentTabs}}}");
            }

            return sb.ToString();
        }

        public static bool IsSimpleType(this Type type)
        {
            return
                (type.IsValueType ||
                type.IsPrimitive ||
                new Type[] {
                typeof(String),
                typeof(Decimal),
                typeof(DateTime),
                typeof(DateTimeOffset),
                typeof(TimeSpan),
                typeof(Guid)
                }.Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object);
        }

        private static String GetVisibility(MethodInfo accessor)
        {
            if (accessor.IsPublic)
            { return "Public"; }
            else if (accessor.IsPrivate)
            { return "Private"; }
            else if (accessor.IsFamily)
            { return "Protected"; }
            else if (accessor.IsAssembly)
            { return "Internal/Friend"; }
            else
            { return "Protected Internal/Friend"; }
        }

        public static TValue GetAttributeValue<TAttribute, TValue>(
            this Type type,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
            where TValue : class
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true)
                          .FirstOrDefault() as TAttribute;
            if (att != null)
            { return valueSelector(att); }
            return null;
        }

        public static TValue GetAttributeValue<TAttribute, TValue>(
            this MethodInfo type,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true)
                          .FirstOrDefault() as TAttribute;

            if (att != null)
            { return valueSelector(att); }

            return default(TValue);
        }

        public static bool IsSubClassOfGeneric(this Type child, Type parent)
        {
            if (child == parent)
            { return false; }

            if (child.IsSubclassOf(parent))
            { return true; }

            var parameters = parent.GetGenericArguments();
            var isParameterLessGeneric = !(parameters != null && parameters.Length > 0 &&
                ((parameters[0].Attributes & TypeAttributes.BeforeFieldInit) == TypeAttributes.BeforeFieldInit));

            while (child != null && child != typeof(object))
            {
                var cur = GetFullTypeDefinition(child);
                if (parent == cur || (isParameterLessGeneric && cur.GetInterfaces().Select(i => GetFullTypeDefinition(i)).Contains(GetFullTypeDefinition(parent))))
                { return true; }
                else if (!isParameterLessGeneric)
                {
                    if (GetFullTypeDefinition(parent) == cur && !cur.IsInterface)
                    {
                        if (VerifyGenericArguments(GetFullTypeDefinition(parent), cur))
                        {
                            if (VerifyGenericArguments(parent, child))
                            { return true; }
                        }
                    }
                    else
                    {
                        foreach (var item in child.GetInterfaces().Where(i => GetFullTypeDefinition(parent) == GetFullTypeDefinition(i)))
                        {
                            if (VerifyGenericArguments(parent, item))
                            { return true; }
                        }
                    }
                }
                child = child.BaseType;
            }

            return false;
        }

        private static Type GetFullTypeDefinition(Type type)
        {
            return type.IsGenericType ? type.GetGenericTypeDefinition() : type;
        }

        private static bool VerifyGenericArguments(Type parent, Type child)
        {
            Type[] childArguments = child.GetGenericArguments();
            Type[] parentArguments = parent.GetGenericArguments();
            if (childArguments.Length == parentArguments.Length)
            {
                for (int i = 0; i < childArguments.Length; i++)
                {
                    if (childArguments[i].Assembly != parentArguments[i].Assembly || childArguments[i].Name != parentArguments[i].Name || childArguments[i].Namespace != parentArguments[i].Namespace)
                    {
                        if (!childArguments[i].IsSubclassOf(parentArguments[i]))
                        { return false; }
                    }
                }
            }
            return true;
        }
    }
}