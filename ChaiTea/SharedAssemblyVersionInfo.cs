﻿// Copyright (c) SBM Site Services, LLC.  All rights reserved. 

using System.Reflection;

#if !BUILD_GENERATED_VERSION
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: AssemblyInformationalVersion("2.0.0.0")]
#endif