﻿// Copyright (c) SBM Site Services, LLC.  All rights reserved.

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("SBM Site Services, LLC")]
[assembly: AssemblyCopyright("© SBM Site Services, LLC.  All rights reserved.")]
//[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyProduct("SBM Insite2")]
[assembly: AssemblyMetadata("Serviceable", "True")]
//[assembly: AssemblyMetadata("ProjectUrl", "")]
//[assembly: AssemblyMetadata("LicenseUrl", "")]
//[assembly: AssemblyMetadata("Tags", "")]
//[assembly: AssemblyMetadata("ReleaseNotes", "")]

#if NET40

namespace System.Reflection
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
    internal sealed class AssemblyMetadataAttribute : Attribute
    {
        public AssemblyMetadataAttribute(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }
    }
}

#endif