switch ($Env:LIFECYCLE_EVENT) {
  "ApplicationStop" {
    break
  }
  "DownloadBundle" {
    break
  }
  "BeforeInstall" {
    c:/windows/system32/inetsrv/appcmd.exe stop site 'Default Web Site'
    break
  }
  "Install" {
    break
  }
  "AfterInstall" {
    c:/windows/system32/inetsrv/appcmd.exe start site 'Default Web Site'
    break
  }
  "ApplicationStart" {
    break
  }
  "ValidateService" {
    break
  }
}
